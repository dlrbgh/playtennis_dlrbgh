<?php
if (!defined('_GNUBOARD_')) exit; // 개별 페이지 접근 불가

if(defined('G5_THEME_PATH')) {
    require_once(G5_THEME_PATH.'/head.php');
    return;
}

if (G5_IS_MOBILE) {
    include_once(G5_MOBILE_PATH.'/head.php');
    return;
}


include_once(G5_PATH.'/head.sub.php');
include_once(G5_LIB_PATH.'/latest.lib.php');
include_once(G5_LIB_PATH.'/outlogin.lib.php');
include_once(G5_LIB_PATH.'/poll.lib.php');
include_once(G5_LIB_PATH.'/visit.lib.php');
include_once(G5_LIB_PATH.'/connect.lib.php');
include_once(G5_LIB_PATH.'/popular.lib.php');

if ($is_guest) { 
 goto_url(G5_BBS_URL.'/login.php');
} 
?>

<div id="hd">
    <h1 id="hd_h1"><?php echo $g5['title'] ?></h1>

    <div id="skip_to_container"><a href="#container">본문 바로가기</a></div>

    <?php
    if(defined('_INDEX_')) { // index에서만 실행
        include G5_BBS_PATH.'/newwin.inc.php'; // 팝업레이어
    }
    ?>
    <div id="hd_wrapper">
        <div id="logo">
            <a href="<?php echo G5_URL ?>"><img src="<?php echo G5_IMG_URL ?>/logo.png" alt="<?php echo $config['cf_title']; ?>"></a>
        </div>

        <ul id="tnb">
            <li><a href="<?php echo G5_SADM_URL ?>/championship_list.php">대회관리</a></li>
            <li><a href="<?php echo G5_BBS_URL ?>/board.php?bo_table=tip">참가팁</a></li>
           	<li><a href="<?php echo G5_BBS_URL ?>/board.php?bo_table=notice">고객센터</a></li>
            <?php 
            	if($member["mb_id"] == ""){
            ?>
           		<li><a href="<?php echo G5_URL ?>/bbs/login.php">로그인</a></li>
           		<li><a href="<?php echo G5_URL ?>/myfair/register.php">회원가입</a></li>
            <?php		
            	}else{
            ?>
             	<li><a href="<?php echo G5_URL ?>/myfair/mypage.php">마이페이지</a></li>
            	<li><a href="<?php echo G5_URL ?>/bbs/logout.php">로그아웃</a></li>
            	
            	<?php if ($is_admin) {  ?>
            		<ul class="adm_nav">
            			<li ><a href="<?php echo G5_ADMIN_URL ?>">관리자</a></li>		
            			<li><a href="<?php echo G5_BBS_URL ?>/current_connect.php">접속자 <?php echo connect('theme/basic'); // 현재 접속자수, 테마의 스킨을 사용하려면 스킨을 theme/basic 과 같이 지정  ?></a></li>
           				<li><a href="<?php echo G5_BBS_URL ?>/new.php">새글</a></li>	
            		</ul>
	            <?php }  ?>
            <?php		
            	}
            ?>
        </ul>
    </div>

    <hr>
</div>
<!-- } 상단 끝 -->
<hr>

<?php if(defined('_INDEX_')) { ?>
<!-- main slide area -->
<div class="slide_hd">
	<div class="slide_wrapper">
	<!-- Owl Stylesheets -->
    <link rel="stylesheet" href="<?php echo G5_JS_URL ?>/owlcarousel/assets/owl.carousel.min.css">
    <link rel="stylesheet" href="<?php echo G5_JS_URL ?>/owlcarousel/assets/owl.theme.default.css">
    
    <!-- javascript -->
    <script src="<?php echo G5_JS_URL ?>/owlcarousel/owl.carousel.js"></script>
		<div class="slide">
	        <div class="owl-carousel owl-theme">
	        	
				<div class="item">
			    	<div class="slide_banner">
			    		 
			    		<div class="img_section">
			    			<a href="">
			    				<img height="385" src="/img/banner.png">
			    			</a>
			    		</div>
			    	</div>
			    </div>
			</div>
	    </div>
    

		<script>
	      $('.owl-carousel').owlCarousel({
			    items:1,
			    loop:true,
			    margin:15,
			    nav:true,
			    dots:true,
			    merge:true,
			    autoplay:true,
			    autoplayTimeout:2000,
			    autoplayHoverPause:true
	   		});
		</script>	
				
	    <!-- sch area-->
	    <div class="sch_area" style="height: 345px">
	    	<div class="hot_exhibition">
	    		<?php echo outlogin('basic'); // 외부 로그인, 테마의 스킨을 사용하려면 스킨을 theme/basic 과 같이 지정 ?>
	    		<ul>
	    			<?php 
						$today = date("Y-m-d",time());
						$sql = "select * from main_sub_exhibition where wr_id > '7' and counting = '2017' and sh_s_wr_datetime > '$today' order by wr_hit desc,sh_s_wr_datetime Limit 5";
						$result = sql_query($sql);
						$i = 1;
						while($r = sql_fetch_array($result)){
					?>
						<li class='ellip' ><a href="./myfair/exhibition_view.php?wr_id=<?php echo $r['wr_id'];?>"><span><?php echo $i;?></span><?php echo $r['sh_s_name'];?></a></li>
	    			<?php		
						$i++;
						}
					?>
	    		</ul>
	    	</div>
	    </div>
	    <!-- //sch area-->
	</div>
	
</div>
<!-- //main slide area -->
<?php } ?>


<?php if(defined('_INDEX_')) { ?>
	
<!-- main head banner area--
<div class="m_banner_section">
	<div class="hd_banner_area">
		<div class="hori_banner">
			<ul>
				<li><a href=""><span class="status">마감임박 전시회</span></a>
					<a href=""><span class="tit">2017 드론쇼 코리아 2017.1.19~21 벡스코 제1전시장 및 컨벤션홀</span></a>
					<a href=""><span class="apply">전시회 참가신청 바로가기</span></a>
				</li>
			</ul>
			<div class="page_nav">
				<span>1/1</span>
				<a href="">Left</a><a href="">Stop</a><a href="">Right</a>
			</div>
		</div>
	</div>
</div>
<!-- // main head banner area-->
<?php }else
	include_once(G5_PATH.'/inc/sub_top.inc');
?>

<!-- 콘텐츠 시작 { -->
<div id="wrapper">

