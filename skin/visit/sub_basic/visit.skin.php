<?php
if (!defined("_GNUBOARD_")) exit; // 개별 페이지 접근 불가

global $is_admin;

// add_stylesheet('css 구문', 출력순서); 숫자가 작을 수록 먼저 출력됨
add_stylesheet('<link rel="stylesheet" href="'.$visit_skin_url.'/style.css">', 0);
?>

<!-- 접속자집계 시작 { -->
<section id="visit">
    <div>
        <h2 class="sound_only">접속자집계</h2>
        <ul>
        	<li>
        		<div class="tit">TODAY</div>
        		<div><?php echo number_format($visit[1]) ?></div>
        	</li>
        	<li>
        		<div class="tit">TOTAL</div>
        		<div><?php echo number_format($visit[4]) ?></div>
        	</li>
        </ul>
    </div>
</section>
<!-- } 접속자집계 끝 -->