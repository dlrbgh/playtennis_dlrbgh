(function($){

	//$.fn.schedule = function(url,destination,params,kind, type) {
	$.fn.schedule = function(url,event) {

		$("#fade, #light").remove();

		$('<div id=\"light\" class=\"white_content\"></div><div id=\"fade\" class=\"black_overlay\"></div>').appendTo(document.body);
		$("#light, #fade").css("display", "block");		
		$("#fade").css("height", $(document).height());
		$.ajax({
			type: "POST",
			url: url,
			data: event,
			success: function(msg){
				modi = event; 
				$("#light").html(msg);				
				var l_left = ($("body").width() - $("#light").width())/2;
				//var l_top = ($("body").height() - $("#light").height())/2; 
				$("#light").css({'left' : l_left, 'top' : 200/*l_top*/});
			}
		});

	};  // end function

  
})(jQuery);

function win_resize(){
	var b_width = $("#content-box").width();
	var l_width = $(".content_list").width();	
	var v_width = b_width - l_width;
	if(v_width < 800){
		$(".content_view").css({'width' : 960});	
	}else{
		$(".content_view").css({'width' : v_width});
	}
}
