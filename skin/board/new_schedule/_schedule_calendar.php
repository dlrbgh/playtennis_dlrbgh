<?php
include_once("./_common.php");
// json_encode PHP4를 위한 함수....
if (!function_exists('json_encode'))
{
  function json_encode($a=false)
  {
    if (is_null($a)) return 'null';
    if ($a === false) return 'false';
    if ($a === true) return 'true';
    if (is_scalar($a))
    {
      if (is_float($a))
      {
        // Always use "." for floats.
        return floatval(str_replace(",", ".", strval($a)));
      }

      if (is_string($a))
      {
        static $jsonReplaces = array(array("\\", "/", "\n", "\t", "\r", "\b", "\f", '"'), array('\\\\', '\\/', '\\n', '\\t', '\\r', '\\b', '\\f', '\"'));
        return '"' . str_replace($jsonReplaces[0], $jsonReplaces[1], $a) . '"';
      }
      else
        return $a;
    }
    $isList = true;
    for ($i = 0, reset($a); $i < count($a); $i++, next($a))
    {
      if (key($a) !== $i)
      {
        $isList = false;
        break;
      }
    }
    $result = array();
    if ($isList)
    {
      foreach ($a as $v) $result[] = json_encode($v);
      return '[' . join(',', $result) . ']';
    }
    else
    {
      foreach ($a as $k => $v) $result[] = json_encode($k).':'.json_encode($v);
      return '{' . join(',', $result) . '}';
    }
  }
}

$list = array();

$sql = "select wr_subject as title, wr_id as id, trim(concat_ws(' ', wr_1, wr_4)) as start, trim(concat_ws(' ', wr_2, wr_5)) as end, wr_8 as className, '$bo_table' as bo_table, IF(wr_4 <> '', 0, 1) as allDay, wr_8 from $write_table where wr_8 != '' order by wr_1";

$result = sql_query($sql);
$i = 0;
while($row = sql_fetch_array($result)){	

	switch($row[wr_8]){

		case "year":			
			$s_year = date("Y", $start);
			$e_year = date("Y", $end);	
			for($t = $s_year; $t <= $e_year; $t++){
				$list[$i] = $row;
				
				if($list[$i][allDay] == "1") $list[$i][allDay] = true; else $list[$i][allDay] = false;		
				$list[$i][start] = $t.substr($row[start], 4);
				$list[$i][end] = $t.substr($row[end], 4);

				switch($list[$i][className]){
					case "#3DA4B7": $list[$i][className] = "importance1"; break;
					case "#B2CBD1": $list[$i][className] = "importance2"; break;
					case "#517ACC": $list[$i][className] = "importance3"; break;
					case "#D95B44": $list[$i][className] = "importance4"; break;
					case "#FF4E90": $list[$i][className] = "importance5"; break;
					case "#FF0000": $list[$i][className] = "importance6"; break;
					case "#ffbb00": $list[$i][className] = "importance7"; break;
					case "#A0A43E": $list[$i][className] = "importance8"; break;
					case "#C3CB0F": $list[$i][className] = "importance9"; break;
					default: $list[$i][className] = "importance1"; break;
				}
				$list[$i][editable] = false;

				$i++;
			}
		break;

		case "month":
			
			for($t = 0; $t < 3; $t++){
				$list[$i] = $row;				
				if($list[$i][allDay] == "1") $list[$i][allDay] = true; else $list[$i][allDay] = false;		
				$list[$i][start] = date("Y-m", mktime(0,0,0, date("n", $start) + $t, 1, date("Y", $start))).substr($row[start], 7);
				$list[$i][end] = date("Y-m", mktime(0,0,0, date("n", $start) + $t, 1, date("Y", $start))).substr($row[end], 7);

				switch($list[$i][className]){
					case "#3DA4B7": $list[$i][className] = "importance1"; break;
					case "#B2CBD1": $list[$i][className] = "importance2"; break;
					case "#517ACC": $list[$i][className] = "importance3"; break;
					case "#D95B44": $list[$i][className] = "importance4"; break;
					case "#FF4E90": $list[$i][className] = "importance5"; break;
					case "#FF0000": $list[$i][className] = "importance6"; break;
					case "#ffbb00": $list[$i][className] = "importance7"; break;
					case "#A0A43E": $list[$i][className] = "importance8"; break;
					case "#C3CB0F": $list[$i][className] = "importance9"; break;
					default: $list[$i][className] = "importance1"; break;
				}
				$list[$i][editable] = false;
				$i++;
			}		

		break;

		case "week":
			
			for($t = 0; $t < 7; $t++){
				
				$list[$i] = $row;						
				if($list[$i][allDay] == "1") $list[$i][allDay] = true; else $list[$i][allDay] = false;	

				$chk_week = date("w", strtotime($row[start]));
				$chk_week1 = date("w", strtotime($row[end]));

				if($chk_week > $chk_week1){ $k = $t + 1; }else{	$k = $t; }
								
				$list[$i][start] = date("Y-m-d", mktime(0,0,0, date("n", $start), date("j", $start) + $chk_week + $t*7, date("Y", $start))).substr($row[start], 10);
				$list[$i][end] = date("Y-m-d", mktime(0,0,0, date("n", $start), date("j", $start) + $chk_week1 + $k*7, date("Y", $start))).substr($row[end], 10);

				switch($list[$i][className]){
					case "#3DA4B7": $list[$i][className] = "importance1"; break;
					case "#B2CBD1": $list[$i][className] = "importance2"; break;
					case "#517ACC": $list[$i][className] = "importance3"; break;
					case "#D95B44": $list[$i][className] = "importance4"; break;
					case "#FF4E90": $list[$i][className] = "importance5"; break;
					case "#FF0000": $list[$i][className] = "importance6"; break;
					case "#ffbb00": $list[$i][className] = "importance7"; break;
					case "#A0A43E": $list[$i][className] = "importance8"; break;
					case "#C3CB0F": $list[$i][className] = "importance9"; break;
					default: $list[$i][className] = "importance1"; break;
				}
				$list[$i][editable] = false;
				$i++;
			}		

		break;
		
	}
	
};

$sql = "select wr_subject as title, wr_id as id, trim(concat_ws(' ', wr_1, wr_4)) as start, trim(concat_ws(' ', wr_2, wr_5)) as end, wr_9 as className, '$bo_table' as bo_table, IF(wr_4 <> '', 0, 1) as allDay from $write_table where wr_8 = '' and ( (unix_timestamp(wr_1) between '$start' and '$end' or  unix_timestamp(wr_2) between '$start' and '$end') or (unix_timestamp(wr_1) < '$start' and unix_timestamp(wr_2) > '$end') ) order by wr_1";


$result = sql_query($sql);
while($row = sql_fetch_array($result)){
	$list[$i] = $row;	
	if($list[$i][allDay] == "1") $list[$i][allDay] = true; else $list[$i][allDay] = false;
	
	switch($list[$i][className]){
		case "#3da4b7": $list[$i][className] = "importance1"; break;
		case "#b2cbd1": $list[$i][className] = "importance2"; break;
		case "#517acc": $list[$i][className] = "importance3"; break;
		case "#d95b44": $list[$i][className] = "importance4"; break;
		case "#ff4e90": $list[$i][className] = "importance5"; break;
		case "#ff0000": $list[$i][className] = "importance6"; break;
		case "#ffbb00": $list[$i][className] = "importance7"; break;
		case "#a0a43e": $list[$i][className] = "importance8"; break;
		case "#c3cb0f": $list[$i][className] = "importance9"; break;
		default: $list[$i][className] = "importance1"; break;
	}
	// if($list[$i][className] == "#3DA4B7"){
		// $list[$i][className] = "importance1";
	// }
	// if($list[$i][className] == "#B2CBD1"){
		// $list[$i][className] = "importance2";
	// }
	// if($list[$i][className] == "#517ACC"){
		// $list[$i][className] = "importance3";
	// }
	// if($list[$i][className] == "#D95B44"){
		// $list[$i][className] = "importance4";
	// }
	// if($list[$i][className] == "#FF4E90"){
		// $list[$i][className] = "importance5";
	// }
	// if($list[$i][className] == "#FF0000"){
		// $list[$i][className] = "importance6";
	// }
	// if($list[$i][className] == "#ffbb00"){
		// $list[$i][className] = "importance7";
	// }
	// if($list[$i][className] == "#A0A43E"){
		// $list[$i][className] = "importance8";
	// }
	// if($list[$i][className] == "#C3CB0F"){
		// $list[$i][className] = "importance9";
	// }
	$i++;
};

echo json_encode($list);
?>
