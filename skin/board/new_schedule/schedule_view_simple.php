<?php

include_once("./_common.php");

$wr_id = $id;

$write = array();
$write_table = "";
if ($bo_table) {
    $board = sql_fetch(" select * from {$g5['board_table']} where bo_table = '$bo_table' ");
    
    if ($board['bo_table']) {
        set_cookie("ck_bo_table", $board['bo_table'], 86400 * 1);
        $gr_id = $board['gr_id'];
        $write_table = $g5['write_prefix'] . $bo_table; // 게시판 테이블 전체이름
        //$comment_table = $g5['write_prefix'] . $bo_table . $g5['comment_suffix']; // 코멘트 테이블 전체이름
        if (isset($wr_id) && $wr_id)
            $write = sql_fetch(" select * from $write_table where wr_id = '$wr_id' ");
    }
}


$board_skin_url = str_replace("../../","", $board_skin_url);

$is_category = false;
$category_name = "";
if ($board[bo_use_category]) {
    $is_category = true;
    $category_name = $write[ca_name]; // 분류명
}

// 수정, 삭제 링크
$update_href = $delete_href = '';
// 로그인중이고 자신의 글이라면 또는 관리자라면 비밀번호를 묻지 않고 바로 수정, 삭제 가능
if (($member['mb_id'] && ($member['mb_id'] == $write['mb_id'])) || $is_admin) {
    $update_href = './write.php?w=u&amp;bo_table='.$bo_table.'&amp;wr_id='.$wr_id.'&amp;page='.$page.$qstr;
    $delete_href = './delete.php?bo_table='.$bo_table.'&amp;wr_id='.$wr_id.'&amp;page='.$page.urldecode($qstr);
    if ($is_admin)
    {
        set_session("ss_delete_token", $token = uniqid(time()));
        $delete_href ='./delete.php?bo_table='.$bo_table.'&amp;wr_id='.$wr_id.'&amp;token='.$token.'&amp;page='.$page.urldecode($qstr);
    }
}
else if (!$write['mb_id']) { // 회원이 쓴 글이 아니라면
    $update_href = './password.php?w=u&amp;bo_table='.$bo_table.'&amp;wr_id='.$wr_id.'&amp;page='.$page.$qstr;
    $delete_href = './password.php?w=d&amp;bo_table='.$bo_table.'&amp;wr_id='.$wr_id.'&amp;page='.$page.$qstr;
}


$view = get_view($write, $board, $board_skin_url, 255);

if (strstr($sfl, "subject"))
    $view[subject] = search_font($stx, $view[subject]);

$html = 0;
if (strstr($view[wr_option], "html1"))
    $html = 1;
else if (strstr($view[wr_option], "html2"))
    $html = 2;

$view[content] = conv_content($view[wr_content], $html);
if (strstr($sfl, "content"))
    $view[content] = search_font($stx, $view[content]);
$view[content] = preg_replace("/(\<img )([^\>]*)(\>)/i", "\\1 name='target_resize_image[]' onclick='image_window(this)' style='cursor:pointer;' \\2 \\3", $view[content]);

//$view[rich_content] = preg_replace("/{img\:([0-9]+)[:]?([^}]*)}/ie", "view_image(\$view, '\\1', '\\2')", $view[content]);
$view[rich_content] = preg_replace("/{이미지\:([0-9]+)[:]?([^}]*)}/ie", "view_image(\$view, '\\1', '\\2')", $view[content]);


$is_signature = false;
$signature = "";
if ($board[bo_use_signature] && $view[mb_id])
{
    $is_signature = true;
    $mb = get_member($view[mb_id]);
    $signature = $mb[mb_signature];

    //$signature = bad_tag_convert($signature);
    // 081022 : CSRF 보안 결함으로 인한 코드 수정
    $signature = conv_content($signature, 1);
}

switch($view[wr_3]){
	case "상": $grade = "<img src=\"$board_skin_url/img/icon_good.gif\" align=\"absmiddle\"> "; break;
	case "중": $grade = "<img src=\"$board_skin_url/img/icon_poor.gif\" align=\"absmiddle\"> "; break;
	case "하": $grade = ""; break;
	default: $grade = ""; break;
}
?>
<link rel="stylesheet" href="<?=$board_skin_url?>/css/list_form.css" type="text/css" />
<script language="JavaScript">
<!--
$(function() {
	$("#fade").click(function(){ $("#light, #fade").remove();  });
	$("#update_url").click(function(){		
		event_url = "<?=$board_skin_url?>/schedule_write.php";
		modi.w = "u";
		$(this).schedule(event_url, modi);			
	});
});	
//-->
</script>
<div class="content_view">
	<div class="content_Detail">
	
		<!--
		<div id="CM_loc_line"><span id="locationBar"><a href="/bbs/board.php?bo_table=<?=$bo_table?>" class="b"><?=$board[bo_subject]?></a></span></div>
		-->
		<div id="GE_font_wrap">
		<p id="GE_font"><br />
		</p>
		</div>
		
		<div id="GE_con" class="CT_ZONE_GE_con">
			<div class="btn_area">
				<?php if ($update_href) { echo "<span><a id='update_url' style='cursor:pointer;' class='btn_cancel' >수정</a></span>"; } ?>
				<?php if ($delete_href) { echo " <span><a href=\"$delete_href\" class=\"btn_admin\">삭제</a></span>"; } ?>
			</div>
			<h3 id="GE_con_tit"><?=$view[subject]?></h3>
			
			<div class="schedule_view">
				<ul>
					<li><span>약속장소</span>: <?=$view[wr_6]?></li>
				</ul>
			<div class="bline"><div>&nbsp;</div></div>	
			</div>
			

			<?php
			// 가변 파일
			$cnt1 = $cnt = 0;
			$file_content = "<div class=\"content_files\"><span class=\"info\">";
			for ($i=0; $i<count($view[file]); $i++) {
				if ($view[file][$i][source] && !$view[file][$i][view]) {
					$cnt++;
					$file_content .= "<span class=\"cp\"><img src='{$board_skin_url}/img/icon_file.gif' align=absmiddle><a href=\"javascript:file_download('{$view[file][$i][href]}', '{$view[file][$i][source]}');\" title='{$view[file][$i][content]}'>";
					$file_content .= "{$view[file][$i][source]}</a><em>|</em>{$view[file][$i][size]}";
					$file_content .= "<em>|</em>{$view[file][$i][download]}</span>";
				}
			}

			for ($i=1; $i<=$g4[link_count]; $i++) {
				if ($view[link][$i]) {
					$cnt1++;
					$file_content .= "<span class=\"cp\"><img src='{$board_skin_url}/img/icon_link.gif' align=absmiddle><a href=\"{$view[link_href][$i]}\" target=_blank>Link $i</a><em>|</em>";
					$file_content .= "{$view[link_hit][$i]}</span>";
				}
			}

			$file_content .= "</span></div><br/><br/>";
			if($cnt > 0 || $cnt1 > 0 ) echo $file_content;
			?>			
			<div id="content_text" class="smartOutput">
			<!-- 내용 출력 -->
			<?=$view[content];?>			
			
			<!-- 테러 태그 방지용 --></xml></xmp><a href=""></a><a href=''></a>
			</div>	
			
			

		</div>		
		

	</div>
</div>

