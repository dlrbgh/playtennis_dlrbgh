<?php
include_once("./_common.php");

?>


<?php
// json_encode PHP4를 위한 함수....
if (!function_exists('json_encode'))
{
  function json_encode($a=false)
  {
    if (is_null($a)) return 'null';
    if ($a === false) return 'false';
    if ($a === true) return 'true';
    if (is_scalar($a))
    {
      if (is_float($a))
      {
        // Always use "." for floats.
        return floatval(str_replace(",", ".", strval($a)));
      }

      if (is_string($a))
      {
        static $jsonReplaces = array(array("\\", "/", "\n", "\t", "\r", "\b", "\f", '"'), array('\\\\', '\\/', '\\n', '\\t', '\\r', '\\b', '\\f', '\"'));
        return '"' . str_replace($jsonReplaces[0], $jsonReplaces[1], $a) . '"';
      }
      else
        return $a;
    }
    $isList = true;
    for ($i = 0, reset($a); $i < count($a); $i++, next($a))
    {
      if (key($a) !== $i)
      {
        $isList = false;
        break;
      }
    }
    $result = array();
    if ($isList)
    {
      foreach ($a as $v) $result[] = json_encode($v);
      return '[' . join(',', $result) . ']';
    }
    else
    {
      foreach ($a as $k => $v) $result[] = json_encode($k).':'.json_encode($v);
      return '{' . join(',', $result) . '}';
    }
  }
}

$list = array();

$i = 0;

$sql = "select wr_subject as title, wr_id as id, trim(wr_1) as start, wr_3 as className, trim(wr_2) as end, wr_9 as className, '$bo_table' as bo_table, IF(wr_4 <> '', 0, 1) as allDay from $write_table where wr_8 = '' and wr_3 != '1' and ( (unix_timestamp(wr_1) between '$start' and '$end' or  unix_timestamp(wr_2) between '$start' and '$end') or (unix_timestamp(wr_1) < '$start' and unix_timestamp(wr_2) > '$end') ) order by wr_1";

$result = sql_query($sql);
while($row = sql_fetch_array($result)){
	$list[$i] = $row;	
	if($list[$i][allDay] == "1") $list[$i][allDay] = true; else $list[$i][allDay] = false;
	
	
	$started = str_replace('.','',$row['start']);
	$ended = str_replace('.','',$row['end']);
	
	$s_year = date("Y",strtotime($started)); 
	$e_year = date("Y",strtotime($ended)); 

	$s_day = date("d",strtotime($started)); 
	$e_day = date("d",strtotime($ended)); 
	
	$s_d = date("D",strtotime($started)); 
	$e_d = date("D",strtotime($ended)); 
	
	$s_e = date("M",strtotime($started)); 
	$e_e = date("M",strtotime($ended)); 
	
	//$list[$i]['start']  = $row['start'];
	//$list[$i]['end']  = $row['end'];
	
	$list[$i]['start'] = "$s_d $s_e $s_day $s_year 00:00:00 GMT+0900 (대한민국 표준시)";
	$list[$i]['end'] = "$e_d $e_e $e_day $e_year 00:00:00 GMT+0900 (대한민국 표준시)";
	
	switch($list[$i][className]){
		case "#3da4b7": $list[$i][className] = "importance1"; break;
		case "#b2cbd1": $list[$i][className] = "importance2"; break;
		case "#517acc": $list[$i][className] = "importance3"; break;
		case "#d95b44": $list[$i][className] = "importance4"; break;
		case "#ff4e90": $list[$i][className] = "importance5"; break;
		case "#ff0000": $list[$i][className] = "importance6"; break;
		case "#ffbb00": $list[$i][className] = "importance7"; break;
		case "#a0a43e": $list[$i][className] = "importance8"; break;
		case "#c3cb0f": $list[$i][className] = "importance9"; break;
		default: $list[$i][className] = "importance1"; break;
	}

	$i++;
};

$sql = "select wr_subject as title, wr_id as id, trim(wr_1) as start, wr_3 as className, trim(wr_2) as end, wr_9 as className, '$bo_table' as bo_table, IF(wr_4 <> '', 0, 1) as allDay from $write_table where wr_3 = '1' order by wr_1";

$result = sql_query($sql);
while($row = sql_fetch_array($result)){
	$list[$i] = $row;
	$list[$i]['test'] = $_POST;
	
	$started = str_replace('.','',$row['start']);
	$ended = str_replace('.','',$row['end']);
	
	$s_year = gmdate("Y", $start); 
	$e_year = gmdate("Y", $end); 

	$s_day = date("d",strtotime($started));
	$e_day = date("d",strtotime($ended));
	
	$s_d = date("D",strtotime($started)); 
	$e_d = date("D",strtotime($ended)); 
	
	$s_e = date("M",strtotime($started)); 
	$e_e = date("M",strtotime($ended)); 

	$list[$i]['start'] = "$s_d $s_e $s_day $s_year 00:00:00 GMT+0900 (대한민국 표준시)";
	$list[$i]['end'] = "$e_d $e_e $e_day $e_year 00:00:00 GMT+0900 (대한민국 표준시)";
	
	
	switch($list[$i][className]){
		case "#3da4b7": $list[$i][className] = "importance1"; break;
		case "#b2cbd1": $list[$i][className] = "importance2"; break;
		case "#517acc": $list[$i][className] = "importance3"; break;
		case "#d95b44": $list[$i][className] = "importance4"; break;
		case "#ff4e90": $list[$i][className] = "importance5"; break;
		case "#ff0000": $list[$i][className] = "importance6"; break;
		case "#ffbb00": $list[$i][className] = "importance7"; break;
		case "#a0a43e": $list[$i][className] = "importance8"; break;
		case "#c3cb0f": $list[$i][className] = "importance9"; break;
		default: $list[$i][className] = "importance1"; break;
	}
	
	$i++;
}
echo json_encode($list);
?>
