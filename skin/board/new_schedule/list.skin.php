<?php
if (!defined("_GNUBOARD_")) exit; // 개별 페이지 접근 불가 

$cate ="2";

?>

<link rel="stylesheet" href="<?php echo $board_skin_url;?>/css/list_form.css" type="text/css">
<link rel='stylesheet' type='text/css' href='<?php echo $board_skin_url;?>/css/fullcalendar.css' />
<link rel='stylesheet' type='text/css' href='<?php echo $board_skin_url;?>/tooltip.css' />
<script type='text/javascript' src='<?php echo $board_skin_url;?>/js/ui.core.js'></script>
<script type='text/javascript' src='<?php echo $board_skin_url;?>/js/ui.draggable.js'></script>
<script type='text/javascript' src='<?php echo $board_skin_url;?>/js/ui.resizable.js'></script>
<!-- <script type='text/javascript' src='<?php echo $board_skin_url;?>/js/calendar.js'></script> -->
<script type='text/javascript' src='<?php echo $board_skin_url;?>/js/fullcalendar.js'></script>
<script type='text/javascript' src='<?php echo $board_skin_url;?>/js/schedule.js'></script>
<script type='text/javascript' src='<?php echo $board_skin_url;?>/js/jquery.form.js'></script>
<script type='text/javascript'>

	$(document).ready(function() {
		
		win_resize();

		$('#calendar').fullCalendar({
			editable: false,			
			events: "<?php echo $board_skin_url;?>/schedule_calendar.php?mb_id=<?php echo $member['mb_id'];?>&bo_table=<?php echo $bo_table;?>",

			eventDrop: function(event, delta) { 
				event_url = "<?php echo $board_skin_url;?>/schedule_update.php";
				event.s_date = $.fullCalendar.formatDate(event.start, 'Y-m-d');
				event.e_date = $.fullCalendar.formatDate(event.end, 'Y-m-d');
				event.mode = "drop";
				$.post(event_url,event);
			},
			
			eventResize: function(event,dayDelta,minuteDelta,revertFunc) { 
				event_url = "<?php echo $board_skin_url;?>/schedule_update.php";
				event.s_date = $.fullCalendar.formatDate(event.start, 'Y-m-d');
				event.e_date = $.fullCalendar.formatDate(event.end, 'Y-m-d');
				event.mode = "resize";
				$.post(event_url,event);
			},

			<?php if ($write_href) { ?>
			dayClick: function(date, allDay, jsEvent, view) { // 일정등록		
				var chk_date = $.fullCalendar.formatDate(date, 'Y-m-d');
				event_url = "<?php echo $board_skin_url;?>/schedule_write.php";
				$(this).schedule(event_url, { bo_table: "<?php echo $bo_table?>", wr_1: chk_date, wr_2: chk_date, w:"" });	
			},
			<?php } ?>

			eventRender: function(event, element) {
				element.bind('click', function() {			
					event_url = "<?php echo $board_skin_url;?>/schedule_view.php";
					$(this).schedule(event_url, event);	
				});
			}

		});

	});
	
	window.onresize = win_resize;

</script>
<div class="tit sound_only">일정관리</div>

<div class="content_view">
	<div id="calendar"></div>
</div>
<!-- } 게시판 페이지 정보 및 버튼 끝 -->
<div class="clear"></div>