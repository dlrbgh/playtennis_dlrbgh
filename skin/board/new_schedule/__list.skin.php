<?php
if (!defined("_GNUBOARD_")) exit; // 개별 페이지 접근 불가 

$cate ="2"
?>



<link rel="stylesheet" href="<?=$board_skin_url?>/css/list_form.css" type="text/css">
<link rel='stylesheet' type='text/css' href='<?=$board_skin_url?>/css/fullcalendar.css' />
<link rel='stylesheet' type='text/css' href='<?=$board_skin_url?>/tooltip.css' />
<script type='text/javascript' src='<?=$board_skin_url?>/js/ui.core.js'></script>
<script type='text/javascript' src='<?=$board_skin_url?>/js/ui.draggable.js'></script>
<script type='text/javascript' src='<?=$board_skin_url?>/js/ui.resizable.js'></script>
<!-- <script type='text/javascript' src='<?=$board_skin_url?>/js/calendar.js'></script> -->
<script type='text/javascript' src='<?=$board_skin_url?>/js/fullcalendar.js'></script>
<script type='text/javascript' src='<?=$board_skin_url?>/js/schedule.js'></script>
<script type='text/javascript' src='<?=$board_skin_url?>/js/jquery.form.js'></script>
<script type='text/javascript'>

	$(document).ready(function() {
		
		win_resize();

		$('#calendar').fullCalendar({
			editable: false,			
			events: "<?=$board_skin_url?>/schedule_calendar.php?mb_id=<?=$member[mb_id]?>&bo_table=<?=$bo_table?>",

			eventDrop: function(event, delta) { 
				event_url = "<?=$board_skin_url?>/schedule_update.php";
				event.s_date = $.fullCalendar.formatDate(event.start, 'yyyy-MM-dd');
				event.e_date = $.fullCalendar.formatDate(event.end, 'yyyy-MM-dd');
				event.mode = "drop";
				$.post(event_url,event);
			},
			
			eventResize: function(event,dayDelta,minuteDelta,revertFunc) { 
				event_url = "<?=$board_skin_url?>/schedule_update.php";
				event.s_date = $.fullCalendar.formatDate(event.start, 'yyyy-MM-dd');
				event.e_date = $.fullCalendar.formatDate(event.end, 'yyyy-MM-dd');
				event.mode = "resize";
				$.post(event_url,event);
			},

			<?php if ($write_href) { ?>
			dayClick: function(date, allDay, jsEvent, view) { // 일정등록		
				var chk_date = $.fullCalendar.formatDate(date, 'yyyy-MM-dd');
				event_url = "<?=$board_skin_url?>/schedule_write.php";
				$(this).schedule(event_url, { bo_table: "<?=$bo_table?>", wr_1: chk_date, wr_2: chk_date, w:"" });	
			},
			<?php } ?>

			eventRender: function(event, element) {
				element.bind('click', function() {			
					event_url = "<?=$board_skin_url?>/schedule_view.php";
					$(this).schedule(event_url, event);	
				});
			}

		});

	});
	
	window.onresize = win_resize;

</script>
<?php include_once(G5_INT_PATH.'/inc/gtmc_int_leftmenu.inc');  ?>
<div class="tit" style="">일정관리</div>

<div class="content_view">
	<div id="calendar"></div>
</div>
<!-- } 게시판 페이지 정보 및 버튼 끝 -->

<div class="todayinfo_area">
<div class="tit_today">오늘의 일정(<?=date("Y.m.d");?>)</div>
	<div class="tbl tbl_frm01">
	<?php
	$sel_mon = sprintf("%02d",$month);
	$datess = date("Y-m-d");
	
	$query = "SELECT count(*) as cnt FROM $write_table WHERE wr_1 <= '$datess' and wr_2 >= '$datess' and wr_is_comment = '0'  ORDER BY wr_1,wr_id ASC";
	$result = sql_query($query);
	$cnt = sql_fetch_array($result);
	
	
	$query = "SELECT * FROM $write_table WHERE wr_1 <= '$datess' and wr_2 >= '$datess' and wr_is_comment = '0'  ORDER BY wr_1,wr_id ASC";
	$result = sql_query($query);
	while($r = sql_fetch_array($result)){
		switch($r[wr_9]){
			case "#3da4b7": $className = "today-color-1"; break;
			case "#b2cbd1": $className = "today-color-2"; break;
			case "#517acc": $className = "today-color-3"; break;
			case "#d95b44": $className = "today-color-4"; break;
			case "#ff4e90": $className = "today-color-5"; break;
			case "#ff0000": $className = "today-color-6"; break;
			case "#ffbb00": $className = "today-color-7"; break;
			case "#a0a43e": $className = "today-color-8"; break;
			case "#c3cb0f": $className = "today-color-9"; break;
			default: $className = "today-color-1"; break;
		}
	?>
		<table class="tbl-small <?=$className;?>">
			<tbody>
				<tr>
					<th style="width:50px">작성자</th>
					<td><?=$r['wr_name'];?> </td>
				</tr>
				<tr>
					<th style="width:50px">제목</th>
					<td><?=$r['wr_subject'];?></td>
				</tr>
				<tr>
					<th style="width:50px">기간</th>
					<td><?php echo date("Y.m.d",strtotime( $r[wr_1] ) )."~".date("Y.m.d",strtotime( $r[wr_2] ) );?></td>
				</tr>
			</tbody>
		</table>
	<?php 
		}
		if($cnt['cnt'] == 0){
	?>
		<table class="tbl-small ">
			<tbody>
				<tr>
					<th style="width:50px">오늘의 일정이 없습니다.</th>
				</tr>
			</tbody>
		</table>
	<?php
		}
	?>
	</div>
</div>