<?php
if (!defined('_GNUBOARD_')) exit; // 개별 페이지 접근 불가
// add_stylesheet('css 구문', 출력순서); 숫자가 작을 수록 먼저 출력됨
add_stylesheet('<link rel="stylesheet" href="'.$latest_skin_url.'/style.css">', 0);
include_once(G5_LIB_PATH.'/thumbnail.lib.php');
$thumb_width = 860;
$thumb_height = 430;

?>

<div class="slide">
    <div class="tit_subject sound_only">
    	올해 품질 목표
    </div>
    <ul class="bxslider">
	<?php
	for ($i=0; $i<count($list); $i++) {	
		$thumb = get_list_thumbnail($bo_table, $list[$i]['wr_id'], $thumb_width, $thumb_height);
	
		if($thumb['src']) {
			$thumb_url = $thumb['src'];
		} else {
			$thumb_url = $latest_skin_url."/img/no-image.gif";
		}
	?>
	      <li>
	      	<img src="<?php echo $thumb['src']?>" alt="<?php echo $list[$i]['subject']; ?>" />
	      		<div class="txt_area">
	      			<div class="tit"><?php echo $list[$i]['subject']; ?></div>
	    			<div class="sub_tit"><?php echo nl2br($list[$i]['wr_content']); ?></div>
	    		</div>
	      </li>
	<?php	}	?>
    </ul>
</div>
		

<script src="<?=$latest_skin_url?>/js/jquery.bxslider.js"></script>


<script>
	$('.bxslider').bxSlider({
  //mode: 'fade',
  pause: 5000,
  autoStart: true,
  auto: true,
  autoStart: true,
  autoDelay: 0,
  pager: true,
  loop: true,
  auto: true,
  controls: true
  
});
</script>




