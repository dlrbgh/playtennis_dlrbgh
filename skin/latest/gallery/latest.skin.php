<?php
if (!defined('_GNUBOARD_')) exit; // 개별 페이지 접근 불가

// add_stylesheet('css 구문', 출력순서); 숫자가 작을 수록 먼저 출력됨
add_stylesheet('<link rel="stylesheet" href="'.$latest_skin_url.'/style.css">', 0);

include_once(G5_LIB_PATH.'/thumbnail.lib.php');
$thumb_width = 167;
$thumb_height = 100;

?>

<!-- <?php echo $bo_subject; ?> 최신글 시작 { -->
<div class="lt gallery_area">
    <strong class="lt_title"><a href="<?php echo G5_BBS_URL ?>/board.php?bo_table=<?php echo $bo_table ?>"><?php echo $bo_subject; ?></a></strong>
    <ul class="latest_gallery">
    <?php
	for ($i=0; $i<count($list); $i++) {	
		$thumb = get_list_thumbnail($bo_table, $list[$i]['wr_id'], $thumb_width, $thumb_height);
	
		if($thumb['src']) {
			$thumb_url = $thumb['src'];
		} else {
			$thumb_url = $latest_skin_url."/img/no-image.gif";
		}
	?>
		<li>
			<a href="<?php echo $list[$i]['href']?>"><img src="<?php echo $thumb['src']?>" alt="" /></a>
			<div class="tit_area">
				<a href="<?php echo $list[$i]['href']?>"><?php echo $list[$i]['subject']; ?></a>
			</div>
		</li>
	<?php	}	?>
    <?php if (count($list) == 0) { //게시물이 없을 때  ?>
    <li>게시물이 없습니다.</li>
    <?php }  ?>
    </ul>
    <div class="lt_more"><a href="<?php echo G5_BBS_URL ?>/board.php?bo_table=<?php echo $bo_table ?>"><span class="sound_only"><?php echo $bo_subject ?></span>더보기</a></div>
</div>
<!-- } <?php echo $bo_subject; ?> 최신글 끝 -->








