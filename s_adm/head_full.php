<?php
if (!defined('_GNUBOARD_')) exit; // 개별 페이지 접근 불가

if(defined('G5_THEME_PATH')) {
    require_once(G5_THEME_PATH.'/head.php');
    return;
}

if (G5_IS_MOBILE) {
    include_once(G5_MOBILE_PATH.'/head.php');
    return;
}


include_once(G5_SADM_PATH.'/head.sub.php');
include_once(G5_LIB_PATH.'/latest.lib.php');
include_once(G5_LIB_PATH.'/outlogin.lib.php');
include_once(G5_LIB_PATH.'/poll.lib.php');
include_once(G5_LIB_PATH.'/visit.lib.php');
include_once(G5_LIB_PATH.'/connect.lib.php');
include_once(G5_LIB_PATH.'/popular.lib.php');
?>
<link rel="stylesheet" href="<?php echo G5_CSS_URL?>/font/flaticon.css">
<?php if (defined("_INDEX_")) {?>
<!-- 상단 시작 { -->
<div id="hd" >
    <h1 id="hd_h1"><?php echo $g5['title'] ?></h1>

    <div id="skip_to_container"><a href="#container">본문 바로가기</a></div>

    <?php
    if(defined('_INDEX_')) { // index에서만 실행
        include G5_BBS_PATH.'/newwin.inc.php'; // 팝업레이어
    }
    ?>

    <div id="hd_wrapper">
        <div id="logo">
            <!-- <a href="<?php echo G5_URL ?>"><img src="<?php echo G5_IMG_URL ?>/logo_horizon.png" alt="<?php echo $config['cf_title']; ?>"></a> -->        
        </div>

       <ul id="tnb">
            <?php if ($is_member) {  ?>
            <?php if ($is_admin) {  ?>
            <li><a href="<?php echo G5_ADMIN_URL ?>"><b>관리자</b></a></li>
        	<li><a href="<?php echo G5_BBS_URL ?>/current_connect.php">접속자 <?php echo connect(); // 현재 접속자수, 테마의 스킨을 사용하려면 스킨을 theme/basic 과 같이 지정  ?></a></li>
            <li><a href="<?php echo G5_BBS_URL ?>/new.php">새글</a></li>
            <li><a href="<?php echo G5_BBS_URL ?>/member_confirm.php?url=<?php echo G5_BBS_URL ?>/register_form.php">정보수정</a></li>
            <li><a href="<?php echo G5_BBS_URL ?>/logout.php">로그아웃</a></li>
            <?php }  ?>
            
            <?php } else {  ?>
            
            <?php }  ?>
            <li><a class="hd_qlink" href="<?php echo G5_SADM_URL?>/championship_list.php">대진표관리</a></li>
        </ul>
    </div>
</div>
<!-- } 상단 끝 -->
<?php } ?>
<hr>

<!-- 콘텐츠 시작 { -->
<div id="<?php if (!defined("_INDEX_")) echo "s_" ?>wrapper">
    <?php if (!defined("_INDEX_")){ ?>
	<style>
		html{box-sizing:border-box;}*,*:before,*:after{box-sizing:inherit}
	</style>
    <?php }?>
    <div id="m_container">
        
