<?php
$menu_cate2 ='2';
$menu_cate3 ='';
$menu_cate4 ='3';
$menu_cate5 ='1';
include_once('./_common.php');
include_once(G5_SADM_PATH.'/head.php');

$match = sql_fetch("select * from match_data where code = '$code'");

?>
<link rel="stylesheet" href="../dist/remodal.css">
<link rel="stylesheet" href="../dist/remodal-default-theme.css">

<!-- sub hd_section-->
<div class="tit_area">
	<div class="tit_subject">대회관리</div>
	<div class="tit"><?=$match['wr_name'] ?></div>
</div>
<!--//sub_hd_area -->

<!-- sub_hd_area -->
<?php include_once(G5_SADM_PATH.'/inc/sub_hd.inc');?>
<!--//sub_hd_area -->

<!-- !PAGE CONTENT! -->
<div>
	<?php
       	$i = 0;
        $sql = "select * from series_data where match_code='$code'";
		$series_result = sql_query($sql);
	?>
	<?php
		while($series = sql_fetch_array($series_result)){

	?>	<!-- 그룹명  -->
	<?php
		//점수 삽입
		$team_field = "team_data";

		if($series['division'] == "단체전"){
			$team_field = "team_event_data";
		}

    	$sql_team = "select count(wr_id) as cnt from $team_field where match_code='$code' and division='$series[division]' and series ='$series[series]' and series_sub = '$series[series_sub]'";
		$team_result = sql_query($sql_team);
		$r = sql_fetch_array($team_result);

    	$sql_teams = "select count(wr_id) as cnt from group_data where match_code='$code' and division='$series[division]' and series ='$series[series]' and series_sub = '$series[series_sub]'";
		$team_results = sql_query($sql_teams);
		$rs = sql_fetch_array($team_results);

    ?>
	<div class="sub_hd">
        <div class="l_area">
        	<div class="tit"><?php echo $series['division']." ".$series['series']." ".$series['series_sub'];?> - <?php echo $r['cnt'];?>명 - <?php echo $rs['cnt'];?>조</div>
        </div>
        <div class="r_area">
        	<div class="tit"><a onclick='window.open("./champ_matchtbl_print.php?code=<?php echo $code;?>", "champ_matchtbl", "width=1000,height=800");'><img class="ico_small" src="<?php echo G5_IMG_URL?>/common/svg/printer.svg" title="print">프린트</a></div>
        </div>
    </div>
    <!-- //그룹명  -->
	<div class="champ_group_edit" >
		<?php
           	$i = 0;
            $sql = "select * from group_data where match_code='$code' and division='$series[division]' and series='$series[series]' and series_sub='$series[series_sub]' and tournament	='L' order by num";
			$gourp_result = sql_query($sql);
		?>
		<?php while($group = sql_fetch_array($gourp_result)){?>
		
		<?php
			$mod = $i % 3;
			if($mod=="0"){
				if($i <> "0"){
				echo "</ul>";	
				}
			echo "<ul>";
		}
		?>
			<li style="min-height: 236px;">

				<div class="tit_area">
					<?php $gym_name = sql_fetch("select gym_name from gym_data where wr_id = '{$group['gym_code']}' ", true);  ?>

					<?php echo $group['num']+1?>조-<?php echo $gym_name['gym_name'] ?>
					<!-- <div class="r-btn-area">
						<a href="" class="btn add">공간 추가</a>
						<a href="" class="btn del">그룹 삭제</a>
					</div> -->
				</div>

				<div class="champ_group_table text-center" >
					<table>
	        			<thead>
	        				<tr>
	            				<th>클럽명</th>
	            				<th>지역</th>
	            				<th>참가자</th>
	            				<!-- <th width="80">편집</th> -->
	        				</tr>
	        			</thead>
	        			<tbody>
	        				<tr>
	        					<?php
                					if($group['team_1']!=""){
                					$team_sql = "select * from $team_field where match_code = '$code' and team_code = '$group[team_1]'";
									$team_result = sql_query($team_sql);
									$r = sql_fetch_array($team_result);
                				?>
	        					<td><?=$r['club']?>
	        					</td>
	        					<td>
	        						<?=$r['area_1'];?> / <?=$r['area_2'];?><br/>
	        					</td>
	        					<td><?=$r['team_1_name'];?><br>
	        						<?=$r['team_2_name'];?><br>
	        						<?php
			        					if($series['division'] == "단체전"){
									?>
									<?=$r['team_3_name'];?><br>
									<?=$r['team_4_name'];?><br>
									<?=$r['team_5_name'];?><br>
									<?=$r['team_6_name'];?><br>
									<?=$r['team_7_name'];?><br>
									<?=$r['team_8_name'];?><br>
									<?php
										}
			        				?>
	        					</td>
	        					<td>
		                            <!-- <div class="btn-group">
		                                <button class="" type="button" data-toggle="modal" onclick="" title="팀 수정하기"><i class="flaticon-edit"></i></button>
		                                <button class=" " type="button" onclick="" title="팀 삭제하기" ><i class="flaticon-remove "></i></button>
		                            </div> -->
		                        </td>
		                        <?php }?>
	        				</tr>

	        				<tr>
	        					<?php
                					if($group['team_2']!=""){
                					$team_sql = "select * from $team_field where match_code = '$code' and team_code = '$group[team_2]'";
									$team_result = sql_query($team_sql);
									$r = sql_fetch_array($team_result);
                				?>
	        					<td><?=$r['club']?>
	        					</td>
	        					<td>
	        						<?=$r['area_1'];?> / <?=$r['area_2'];?><br/>
	        					</td>
	        					<td><?=$r['team_1_name'];?><br>
	        						<?=$r['team_2_name'];?><br>
	        						<?php
			        					if($series['division'] == "단체전"){
									?>
									<?=$r['team_3_name'];?><br>
									<?=$r['team_4_name'];?><br>
									<?=$r['team_5_name'];?><br>
									<?=$r['team_6_name'];?><br>
									<?=$r['team_7_name'];?><br>
									<?=$r['team_8_name'];?><br>
									<?php
										}
			        				?>
	        					</td>
	        					<td>
		                            <!-- <div class="btn-group">
		                                <button class="" type="button" data-toggle="modal" onclick="" title="팀 수정하기"><i class="flaticon-edit"></i></button>
		                                <button class=" " type="button" onclick="" title="팀 삭제하기" ><i class="flaticon-remove "></i></button>
		                            </div> -->
		                        </td>
		                        <?php }?>
	        				</tr>

	        				<tr>
	        					<?php
                					if($group['team_3']!=""){
                					$team_sql = "select * from $team_field where match_code = '$code' and team_code = '$group[team_3]'";
									$team_result = sql_query($team_sql);
									$r = sql_fetch_array($team_result);
                				?>
	        					<td><?=$r['club']?>
	        					</td>
	        					<td>
	        						<?=$r['area_1'];?> / <?=$r['area_2'];?><br/>
	        					</td>
	        					<td><?=$r['team_1_name'];?><br>
	        						<?=$r['team_2_name'];?><br>
	        						<?php
			        					if($series['division'] == "단체전"){
									?>
									<?=$r['team_3_name'];?><br>
									<?=$r['team_4_name'];?><br>
									<?=$r['team_5_name'];?><br>
									<?=$r['team_6_name'];?><br>
									<?=$r['team_7_name'];?><br>
									<?=$r['team_8_name'];?><br>
									<?php
										}
			        				?>
	        					</td>
	        					<td>
		                            <!-- <div class="btn-group">
		                                <button class="" type="button" data-toggle="modal" onclick="" title="팀 수정하기"><i class="flaticon-edit"></i></button>
		                                <button class=" " type="button" onclick="" title="팀 삭제하기" ><i class="flaticon-remove "></i></button>
		                            </div> -->
		                        </td>
		                        <?php }?>
	        				</tr>
	        				<tr>
	        					<?php
                					if($group['team_4']!=""){
                					$team_sql = "select * from $team_field where match_code = '$code' and team_code = '$group[team_4]'";
									$team_result = sql_query($team_sql);
									$r = sql_fetch_array($team_result);
                				?>
	        					<td><?=$r['club']?>
	        					</td>
	        					<td>
	        						<?=$r['area_1'];?> / <?=$r['area_2'];?><br/>
	        					</td>
	        					<td><?=$r['team_1_name'];?> <br> <?=$r['team_2_name'];?></td>
	        					<td>
		                            <!-- <div class="btn-group">
		                                <button class="" type="button" data-toggle="modal" onclick="" title="팀 수정하기"><i class="flaticon-edit"></i></button>
		                                <button class=" " type="button" onclick="" title="팀 삭제하기" ><i class="flaticon-remove "></i></button>
		                            </div> -->
		                        </td>
		                        <?php }?>
	        				</tr>

	        			</tbody>
	                </table>
				</div>
			</li>
		<?php $i++;} ?>
	</div>
	<?php $i++;} ?>
	
	<div class="btn_area">
		<a class="btn btn02 fw-600" href="<?php echo G5_SADM_URL?>/championship_add_step3.php">수 정</a>
	</div>

</div>



<?php
include_once(G5_SADM_PATH.'/tail.php');
?>
