<?php
$menu_cate2 ='2';
$menu_cate3 ='2';
$menu_cate4 ='4';
$menu_cate5 ='3';
include_once('./_common.php');
include_once(G5_SADM_PATH.'/_head.php');

?>
<script src="<?php echo G5_SADM_URL?>/assets/js/autosave"></script>

<!-- sub_hd_area -->
<?php include_once(G5_SADM_PATH.'/inc/sub_hd.inc');?>

<div class="inline-tab relative">
	<ul class="tab_pd">
		<li class=""><a href="">2017-04-26</a></li>
		<li class="active"><a href="">2017-04-27</a></li>
	</ul>
</div>
<!--//sub_hd_area -->

<!-- !PAGE CONTENT! -->
<div>

	<div class="history_log_area">
		<ul>
			<li>
				[시간]구분 메시지(경기가 시작했습니다, 경기가 종료되었습니다. 점수가 수정되었습니다.)
				장소/경기번호 – 클럽명 선수명, 클럼명 선수명 결과점수(수정점수, 경시시작시는 공란) 클럽명 선수명, 클럽명 선수명
			</li>
			<li>
				[2017-05-19_14:45] 부자체육관-1코트-3번 경기 시작<br/>
				레이즈업 클럽 이상훈, 레이즈업 클럽 이우선 6 vs 0 네이버클럽 김과장, 카카오클럽 이대리
			</li>
			<li>
				<span class="warning">[2017-05-19_14:45] 부자체육관-1코트-3번 점수 수정</span> <span class="color6 fw-700">(수정사유 출력)</span><br/>
				레이즈업 클럽 이상훈, 레이즈업 클럽 이우선 6 vs 0 네이버클럽 김과장, 카카오클럽 이대리
			</li>
			<li>
				[2017-05-19_14:45] 부자체육관-1코트-3번 경기 종료<br/>
				레이즈업 클럽 이상훈, 레이즈업 클럽 이우선 6 vs 0 네이버클럽 김과장, 카카오클럽 이대리
			</li>
		</ul>
	</div>

</div>
<!-- End page content -->



<?php
include_once(G5_SADM_PATH.'/_tail.php');
?>
