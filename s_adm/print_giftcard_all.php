<?php require 'inc/config.php'; ?>
<?php require 'inc/views/template_head_start.php'; ?>
<?php require 'inc/views/template_head_end.php'; ?>

<style>
	table.table-scorecard{margin-bottom:40px;}
	table.table-scorecard tr td{vertical-align:middle;}
	table.table-scorecard thead th{padding: 20px 10px 12px;}
	table.table-scorecard tr td input{border:0px;text-align:center;font-size:70px;width:100%;}
	table.table-scorecard tr td .name-tit{font-size:18px;}
	.scorecard-sign span{border-bottom:1px solid #000;padding-bottom:5px;padding-right:200px;}
	.scorecard-title{font-size:25px;text-transform:uppercase;line-height:35px;}
	.block{padding:13px 0 10px 0;}
	.col-lg-6{height:522.5px;}
</style>

<?php
include_once('./_common.php');

include_once(G5_PATH.'/head.sub.php');

$match_code		 = $_REQUEST['match_code'];
$division	 	 = $_REQUEST['division'];

$sql = "select * from match_data where code = '$match_code'";
$result = sql_query($sql);
$rs = sql_fetch_array($result);

$sql = "select * from series_data where match_code = '$match_code' and division = '$division' order by wr_id";
$result = sql_query($sql);
while($r = sql_fetch_array($result)){
	
	$series_update = "update series_data set print = '1' where division = '$r[division]' and series = '$r[series]' and series_sub = '$r[series_sub]' and match_code = '$match_code'";
	sql_query($series_update);
	
	$group_sql = "select count(wr_id) as cnt from group_data where match_code = '$match_code' and division = '$division' and series = '$r[series]' and series_sub = '$r[series_sub]'";
	$group_result = sql_query($group_sql);
	$group = sql_fetch_array($group_result);
	
	if($group['cnt'] != 1){

		$game_sql = "select * from game_score_data where match_code = '$match_code' and division = '$division' and series = '$r[series]' and series_sub = '$r[series_sub]' and tournament = 'c'";
		$game_result = sql_query($game_sql);
		$game = sql_fetch_array($game_result);
		
		$sql_team1 = "select * from team_data where team_code = '$game[team_1_code]'";
		$team1_result = sql_query($sql_team1);
		
		$sql_team2 = "select * from team_data where team_code = '$game[team_2_code]'";
		$team2_result = sql_query($sql_team2);
		
		if($game['team_1_score'] > $game['team_2_score']){
			$champion = sql_fetch_array($team1_result);
			$champion_sub = sql_fetch_array($team2_result);
		}else{
			$champion = sql_fetch_array($team2_result);
			$champion_sub = sql_fetch_array($team1_result);
		}
		
		$champion_code = $champion['team_code'];
		$game_sql = "select * from game_score_data where match_code = '$match_code' and division = '$division' and series = '$r[series]' and series_sub = '$r[series_sub]' and (team_1_code = '$champion_code' or team_2_code = '$champion_code') and tournament = 'T' and tournament_count = '2'";
		$game_result = sql_query($game_sql);
		$third_array = array();
		$games = sql_fetch_array($game_result);
		if($games['team_1_score'] > $games['team_2_score'] ){
			$sql_team2 = "select * from team_data where team_code = '$games[team_2_code]'";
			$team2_result = sql_query($sql_team2);												
			$third_champion_sub = sql_fetch_array($team2_result);
		}else{
			$sql_team2 = "select * from team_data where team_code = '$games[team_1_code]'";
			$team2_result = sql_query($sql_team2);
			$third_champion_sub = sql_fetch_array($team2_result);
		}		
	?>
	<div class="col-lg-6 col-lg-offset-3">
	    <!-- Bordered Table -->
	    <div class="block">
	        <div class="block-header">
	        	<h2 class="text-center">상품 수령증</h2>
	        </div>
	        <div class="block-content">
	            <table class="table table-bordered table-scorecard">
	                <tbody>
	                    <tr>
	                        <td class="text-center" width="200px">
	                        	<div class="name-tit font-w600">대 회 명</div>
	                        </td>
	                        <td class="text-center">
	                        	<div class="name-tit"><?=$rs['wr_name']?></div>
	                        </td>
	                    </tr>
	                    <tr>
	                        <td class="text-center">
	                        	<div class="name-tit font-w600">급 수</div>
	                        </td>
	                        <td class="text-center">
	                        	<div class="name-tit"><?=$champion['division']?> <?=$champion['series']?><?=$champion['series_sub']?></div>
	                        </td>
	                    </tr>
	                    <tr>
	                        <td class="text-center">
	                        	<div class="name-tit font-w600">순 위</div>
	                        </td>
	                        <td class="text-center">
	                        	<div class="name-tit">1</div>
	                        </td>
	                    </tr>
	                    <tr>
	                        <td class="text-center">
	                        	<div class="name-tit font-w600">클 럽 명</div>
	                        </td>
	                        <td class="text-center">
	                        	<div class="name-tit"><?=$champion['club']?></div>
	                        </td>
	                    </tr>
	                    <tr>
	                        <td class="text-center">
	                        	<div class="name-tit font-w600">성 함</div>
	                        </td>
	                        <td class="text-center">
	                        	<div class="name-tit"><?=$champion['team_1_name']?> <?=$champion['team_2_name']?></div>
	                        </td>
	                    </tr>
					</tbody>
	            </table>
	            <div class="row">
	            	<div class="col-md-12">
	            		<div class="pull-left font-05 font-w600">
			            	<span>주관 : <?=$rs['organizer']?></span>
			            </div>
			            <div class="pull-right scorecard-sign">
			            	<span>확인(서명)</span>
			            </div>		
	            	</div>
	            </div>
	            <button id="print_button" name="print_button" class="btn btn-warning btn-block push-20-t push-20 hidden-print"  onclick="App.initHelper('print-page');"><i class="si si-printer"></i>상품수령증 인쇄</button>
	        </div>
	    </div>
	    <!-- END Bordered Table -->
	</div>
	
	<div class="col-lg-6 col-lg-offset-3">
	    <!-- Bordered Table -->
	    <div class="block">
	        <div class="block-header">
	        	<h2 class="text-center">상품 수령증</h2>
	        </div>
	        <div class="block-content">
	            <table class="table table-bordered table-scorecard">
	                <tbody>
	                    <tr>
	                        <td class="text-center" width="200px">
	                        	<div class="name-tit font-w600">대 회 명</div>
	                        </td>
	                        <td class="text-center">
	                        	<div class="name-tit"><?=$rs['wr_name']?></div>
	                        </td>
	                    </tr>
	                    <tr>
	                        <td class="text-center">
	                        	<div class="name-tit font-w600">급 수</div>
	                        </td>
	                        <td class="text-center">
	                        	<div class="name-tit"><?=$champion_sub['division']?> <?=$champion_sub['series']?><?=$champion_sub['series_sub']?></div>
	                        </td>
	                    </tr>
	                    <tr>
	                        <td class="text-center">
	                        	<div class="name-tit font-w600">순 위</div>
	                        </td>
	                        <td class="text-center">
	                        	<div class="name-tit">2</div>
	                        </td>
	                    </tr>
	                    <tr>
	                        <td class="text-center">
	                        	<div class="name-tit font-w600">클 럽 명</div>
	                        </td>
	                        <td class="text-center">
	                        	<div class="name-tit"><?=$champion_sub['club']?></div>
	                        </td>
	                    </tr>
	                    <tr>
	                        <td class="text-center">
	                        	<div class="name-tit font-w600">성 함</div>
	                        </td>
	                        <td class="text-center">
	                        	<div class="name-tit"><?=$champion_sub['team_1_name']?> <?=$champion_sub['team_2_name']?></div>
	                        </td>
	                    </tr>
					</tbody>
	            </table>
	            <div class="row">
	            	<div class="col-md-12">
	            		<div class="pull-left font-05 font-w600">
			            	<span>주관 : <?=$rs['organizer']?></span>
			            </div>
			            <div class="pull-right scorecard-sign">
			            	<span>확인(서명)</span>
			            </div>		
	            	</div>
	            </div>
	            <button id="print_button" name="print_button" class="btn btn-warning btn-block push-20-t push-20 hidden-print"  onclick="App.initHelper('print-page');"><i class="si si-printer"></i>상품수령증 인쇄</button>
	        </div>
	    </div>
	    <!-- END Bordered Table -->
	</div>
	
	<div class="col-lg-6 col-lg-offset-3">
	    <!-- Bordered Table -->
	    <div class="block">
	        <div class="block-header">
	        	<h2 class="text-center">상품 수령증</h2>
	        </div>
	        <div class="block-content">
	            <table class="table table-bordered table-scorecard">
	                <tbody>
	                    <tr>
	                        <td class="text-center" width="200px">
	                        	<div class="name-tit font-w600">대 회 명</div>
	                        </td>
	                        <td class="text-center">
	                        	<div class="name-tit"><?=$rs['wr_name']?></div>
	                        </td>
	                    </tr>
	                    <tr>
	                        <td class="text-center">
	                        	<div class="name-tit font-w600">급 수</div>
	                        </td>
	                        <td class="text-center">
	                        	<div class="name-tit"><?=$third_champion_sub['division']?> <?=$third_champion_sub['series']?><?=$third_champion_sub['series_sub']?></div>
	                        </td>
	                    </tr>
	                    <tr>
	                        <td class="text-center">
	                        	<div class="name-tit font-w600">순 위</div>
	                        </td>
	                        <td class="text-center">
	                        	<div class="name-tit">3</div>
	                        </td>
	                    </tr>
	                    <tr>
	                        <td class="text-center">
	                        	<div class="name-tit font-w600">클 럽 명</div>
	                        </td>
	                        <td class="text-center">
	                        	<div class="name-tit"><?=$third_champion_sub['club']?></div>
	                        </td>
	                    </tr>
	                    <tr>
	                        <td class="text-center">
	                        	<div class="name-tit font-w600">성 함</div>
	                        </td>
	                        <td class="text-center">
	                        	<div class="name-tit"><?=$third_champion_sub['team_1_name']?> <?=$third_champion_sub['team_2_name']?></div>
	                        </td>
	                    </tr>
					</tbody>
	            </table>
	            <div class="row">
	            	<div class="col-md-12">
	            		<div class="pull-left font-05 font-w600">
			            	<span>주관 : <?=$rs['organizer']?></span>
			            </div>
			            <div class="pull-right scorecard-sign">
			            	<span>확인(서명)</span>
			            </div>		
	            	</div>
	            </div>
	            <button id="print_button" name="print_button" class="btn btn-warning btn-block push-20-t push-20 hidden-print"  onclick="App.initHelper('print-page');"><i class="si si-printer"></i>상품수령증 인쇄</button>
	        </div>
	    </div>
	    <!-- END Bordered Table -->
	</div>
	
	<?php

		}else if($group['cnt'] == 1){
			$game_sql = "select * from team_data where match_code = '$match_code' and division = '$division' and series = '$r[series]' and series_sub = '$r[series_sub]' order by group_rank Limit 3";
			
			$game_result = sql_query($game_sql);
			$champion = array();
			while($game = sql_fetch_array($game_result)){
				$champion[] = $game;
			}
			
			$game_count_sql = "select count(wr_id) as cnt from game_score_data where team_1_score = '0' and team_2_score = '0' and match_code = '$code' and division = '$division' and series = '$r[series]' and series_sub = '$r[series_sub]' and tournament = 'L'";
			$game_count_result = sql_query($game_count_sql);
			$game_count = sql_fetch_array($game_count_result);
		?>
		
    	<?php
    		$i = 1;
    		foreach ($champion as $data) 	
			{
    	?>
    	
    	<div class="col-lg-6 col-lg-offset-3">
		    <!-- Bordered Table -->
		    <div class="block">
		        <div class="block-header">
		        	<h2 class="text-center">상품 수령증</h2>
		        </div>
		        <div class="block-content">
		            <table class="table table-bordered table-scorecard">
		                <tbody>
		                    <tr>
		                        <td class="text-center" width="200px">
		                        	<div class="name-tit font-w600">대 회 명</div>
		                        </td>
		                        <td class="text-center">
		                        	<div class="name-tit"><?=$rs['wr_name']?></div>
		                        </td>
		                    </tr>
		                    <tr>
		                        <td class="text-center">
		                        	<div class="name-tit font-w600">급 수</div>
		                        </td>
		                        <td class="text-center">
		                        	<div class="name-tit"><?=$data['division']?> <?=$data['series']?><?=$data['series_sub']?></div>
		                        </td>
		                    </tr>
		                    <tr>
		                        <td class="text-center">
		                        	<div class="name-tit font-w600">순 위</div>
		                        </td>
		                        <td class="text-center">
		                        	<div class="name-tit"><?=$i;?></div>
		                        </td>
		                    </tr>
		                    <tr>
		                        <td class="text-center">
		                        	<div class="name-tit font-w600">클 럽 명</div>
		                        </td>
		                        <td class="text-center">
		                        	<div class="name-tit"><?=$data['club']?></div>
		                        </td>
		                    </tr>
		                    <tr>
		                        <td class="text-center">
		                        	<div class="name-tit font-w600">성 함</div>
		                        </td>
		                        <td class="text-center">
		                        	<div class="name-tit"><?=$data['team_1_name']?> <?=$data['team_2_name']?></div>
		                        </td>
		                    </tr>
						</tbody>
		            </table>
		            <div class="row">
		            	<div class="col-md-12">
		            		<div class="pull-left font-05 font-w600">
				            	<span>주관 : <?=$rs['organizer']?></span>
				            </div>
				            <div class="pull-right scorecard-sign">
				            	<span>확인(서명)</span>
				            </div>		
		            	</div>
		            </div>
		            <button id="print_button" name="print_button" class="btn btn-warning btn-block push-20-t push-20 hidden-print"  onclick="App.initHelper('print-page');"><i class="si si-printer"></i>상품수령증 인쇄</button>
		        </div>
		    </div>
		    <!-- END Bordered Table -->
		</div>
    	<?php
    		$i++;		
    		}
		}
		
	}
?>


<script>
$(window).load(function() {
	$("#print_button").click();
});
</script>

<?php require 'inc/views/template_footer_start.php'; ?>



<?php require 'inc/views/template_footer_end.php'; ?>