<?php
$menu_cate2 ='2';
$menu_cate3 ='2';
include_once('./_common.php');
include_once(G5_PATH.'/_head.php');

?>
<script src="<?php echo G5_SADM_URL?>/assets/js/autosave"></script>
<!-- sub hd_section-->
	<div class="tit_area">
		<div class="tit_subject"><?php echo $menu_title; ?>(메인 카테고리영역)</div>
		<div class="tit">카테고리내 타이틀 경기종목설정</div>

	</div>
<!--//sub_hd_area -->
<style>
/* 디폴트 상태 */
input.select[type="radio"] + label,input.select[type="checkbox"] + label {display:block; background-color:#ffffff;cursor:pointer; }
/* 온상태일때 */
input.select[type="radio"]:checked + label {background-color:#292929}
input.select[type="radio"]:checked + label .subject, input.select[type="radio"]:checked + label .tit,
input.select[type="radio"]:checked + label .cost{color:#fff}


ul.select_group li{float:left;border:1px solid #ccc;background-color:#fff;width:100px;min-height:50px;margin-left:5px;position:relative}

ul.select_group li input.select[type="radio"],input.select[type="checkbox"] {display:none}
ul.select_group li input.select[type="radio"]:checked + label,
ul.select_group li input.select[type="checkbox"]:checked + label {background-color:#5c90d2;color:#fff}

ul.select_group li label .opt{padding:15px;min-height: 44px;text-align:center}



					</style>

<!-- !PAGE CONTENT! -->
<div>
	<!-- 대회정보 필수입력-->
	<div class="tbl_frm01">
		<table>
			<tbody>
				<tr>
					<th>경기구분</th>
					<td>
						<ul class="select_group">
							<li>
								<input id="radio" class="select" value="1" name="grad_sex[0]" type="radio">
								<label for="radio">
									<div class="opt">
										개인전
									</div>
								</label>
							</li>
							<li>
								<input id="radio1" class="select"  value="1" name="grad_sex[0]" type="radio">
								<label for="radio1">
									<div class="opt">
										단체전
									</div>
								</label>
							</li>
						</ul>
					</td>
					<th>성별</th>
					<td>
						<ul class="select_group">
							<li>
								<input id="radio" class="select" value="1" name="grad_sex[0]" type="radio">
								<label for="radio">
									<div class="opt">
										남자
									</div>
								</label>
							</li>
							<li>
								<input id="radio1" class="select"  value="1" name="grad_sex[0]" type="radio">
								<label for="radio1">
									<div class="opt">
										여자
									</div>
								</label>
							</li>
							<li>
								<input id="radio2" class="select"  value="3" name="grad_sex[0]" type="radio">
								<label for="radio2">
									<div class="opt">
										혼성
									</div>
								</label>
							</li>
						</ul>
					</td>
					<th>단/복식</th>
					<td>
						<ul class="select_group">
							<li>
								<input id="radio" class="select" value="1" name="grad_sex[0]" type="radio">
								<label for="radio">
									<div class="opt">
										단식
									</div>
								</label>
							</li>
							<li>
								<input id="radio1" class="select"  value="1" name="grad_sex[0]" type="radio">
								<label for="radio1">
									<div class="opt">
										복식
									</div>
								</label>
							</li>
						</ul>
					</td>
				</tr>
				<tr>
					<th>
						등급명
					</th>
					<td colspan="5">

						<ul class="champ_option_group">
							<li>
								<div>
									<div>1.개인or단체</div>
									<div>
										<ul class="select_group">
											<li>
												<input id="radio" class="select" value="1" name="grad_sex[0]" type="radio">
												<label for="radio">
													<div class="opt">
														개인전
													</div>
												</label>
											</li>
											<li>
												<input id="radio1" class="select"  value="1" name="grad_sex[0]" type="radio">
												<label for="radio1">
													<div class="opt">
														단체전
													</div>
												</label>
											</li>
										</ul>
									</div>
								</div>
							</li>
							<li>
								<div class="select_group">
							  	<div>2.성별</div>
									<div>
										<ul class="select_group">
											<li>
												<input id="radio" class="select" value="1" name="grad_sex[0]" type="radio">
												<label for="radio">
													<div class="opt">
														남자
													</div>
												</label>
											</li>
											<li>
												<input id="radio1" class="select"  value="1" name="grad_sex[0]" type="radio">
												<label for="radio1">
													<div class="opt">
														여자
													</div>
												</label>
											</li>
											<li>
												<input id="radio2" class="select"  value="3" name="grad_sex[0]" type="radio">
												<label for="radio2">
													<div class="opt">
														혼성
													</div>
												</label>
											</li>
										</ul>
									</div>
								</div>
							</li>
							<li>
								<div class="select_group">
							  	<div>3.단식 or 복식</div>
									<div>
										<ul class="select_group">
											<li>
												<input id="radio" class="select" value="1" name="grad_sex[0]" type="radio">
												<label for="radio">
													<div class="opt">
														단식
													</div>
												</label>
											</li>
											<li>
												<input id="radio1" class="select"  value="1" name="grad_sex[0]" type="radio">
												<label for="radio1">
													<div class="opt">
														복식
													</div>
												</label>
											</li>
										</ul>
									</div>
								</div>
							</li>
						</ul>
<style>
.champ_option_group li{float:left}
</style>
					</td>
				</tr>
				<tr>
					<th>등급명</th>
					<td><input name="" value="" id="" required="" class="frm_input required" size="25" maxlength="255" type="text"></td>
				</tr>
				<tr>
					<th>연령/선출</th>
					<td>
						<ul class="select_group">
							<li>
								<input id="radio" class="select" value="1" name="grad_sex[0]" type="radio">
								<label for="radio">
									<div class="opt">
										무관
									</div>
								</label>
							</li>
							<li>
								<input id="radio1" class="select"  value="1" name="" type="radio">
								<label for="radio1">
									<div class="opt">
										나이선택
									</div>
								</label>
							</li>
						</ul>
						<input type="text" size="5" class="frm_input text-center" />부터 <span>~</span> <input type="text" size=5" class="frm_input text-center" />까지
					</td>
				</tr>
				<tr>
					<th>등급</th>
					<td>
						<input name="" value="" id="" required="" class="frm_input " size="5" maxlength="5" >부터
						~
						<input name="" value="" id="" required="" class="frm_input " size="5" maxlength="5" >까지
						<br/>
                        합산등급 00점 이하
                       <input name="" value="" id="" required="" class="frm_input " size="5" maxlength="5" type="text">

                    </td>
				</tr>
				<tr>
					<th>선수출신</th>
					<td>
						<input name="" value="" id="" required="" class="frm_input " size="5" maxlength="5" type="text"> 인 이하
                    </td>
				</tr>

			</tbody>
		</table>
	</div>
	<!-- //대회정보 필수입력-->

	<div class="btn_area">
		<a class="btn btn01 fw-600" href="<?php echo G5_SADM_URL?>/championship_add.php">뒤 로</a>
		<a class="btn btn01 fw-600" href="<?php echo G5_SADM_URL?>/championship_add_step1.php">다 음</a>
	</div>

</div>
<!-- End page content -->




<?php
include_once(G5_PATH.'/_tail.php');
?>
