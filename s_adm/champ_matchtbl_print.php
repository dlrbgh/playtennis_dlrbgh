<?php
//include_once('./_common.php');
//include_once(G5_SADM_PATH.'/head.php');
include_once('./_common.php');
include_once('./head.sub.php');
$match = sql_fetch("select * from match_data where code = '$code'");

?>
<link rel="stylesheet" href="<?php echo G5_SADM_URL?>/assets/css/media_print.css" type="text/css" media="print">
<link rel="stylesheet" href="<?php echo G5_SADM_URL?>/assets/css/media_print.css" type="text/css">
<style>
	body{background-color:#fff}
</style>

<page size="A4" >
<!-- !PAGE CONTENT! -->
<div>
	<?php
       	$i = 0;
        $sql = "select * from series_data where match_code='$code'";
		$series_result = sql_query($sql);
	?>
	<?php
		while($series = sql_fetch_array($series_result)){

	?>	<!-- 그룹명  -->
	<?php
		//점수 삽입
		$team_field = "team_data";

		if($series['division'] == "단체전"){
			$team_field = "team_event_data";
		}

    	$sql_team = "select count(wr_id) as cnt from $team_field where match_code='$code' and division='$series[division]' and series ='$series[series]' and series_sub = '$series[series_sub]'";
		$team_result = sql_query($sql_team);
		$r = sql_fetch_array($team_result);

    	$sql_teams = "select count(wr_id) as cnt from group_data where match_code='$code' and division='$series[division]' and series ='$series[series]' and series_sub = '$series[series_sub]'";
		$team_results = sql_query($sql_teams);
		$rs = sql_fetch_array($team_results);

    ?>
	<div class="sub_hd">
        <div class="l_area">
        	<div class="tit"><?php echo $series['division']." ".$series['series']." ".$series['series_sub'];?> - <?php echo $r['cnt'];?>팀 - <?php echo $rs['cnt'];?>조</div>
        </div>
    </div>
    <!-- //그룹명  -->
	<?php if($series['division']=="단체전"){ ?>
		<div class="champ_group_edit half" >
	<?php
	}{ ?>
		<div class="champ_group_edit" >
	<?php
	}
	?>
		<?php
           	$i = 0;
            $sql = "select * from group_data where match_code='$code' and division='$series[division]' and series='$series[series]' and series_sub='$series[series_sub]' and tournament	='L' order by num";
			$gourp_result = sql_query($sql);
		?>
		<?php while($group = sql_fetch_array($gourp_result)){?>

		<?php
			if($series[division]=="단체전"){
				$mod = $i % 2;
				if($mod=="0"){
					if($i <> "0"){
					echo "</ul>";
					}
				echo "<ul>";
				}

			}else if($series[division]=="개인전"){
				$mod = $i % 3;
				if($mod=="0"){
					if($i <> "0"){
					echo "</ul>";
					}
				echo "<ul>";
				}
			}
		?>
			<li >

				<div class="tit_area">
					<?php $gym_name = sql_fetch("select gym_name from gym_data where wr_id = '{$group['gym_code']}' ", true);  ?>

					<?php echo $group['num']+1?>조 - <?php echo $gym_name['gym_name'] ?>
					<?php echo "-";?>
				</div>

				<div class="champ_group_table text-center" >
					<table>

	        			<tbody>

	        				<!-- //team1-->
	        				<?php
            					if($group['team_1']!=""){
            					$team_sql = "select * from $team_field where match_code = '$code' and team_code = '$group[team_1]'";
								$team_result = sql_query($team_sql);
								$r = sql_fetch_array($team_result);
            				?>

								<?php
				        			if($series['division'] == "개인전"){
								?>

								<tr>
		        					<td>
										<?=$r['area_1'];?> / <?=$r['area_2'];?><br/>
		        						<?=$r['area_1'];?> / <?=$r['team_2_area'];?>
		        					</td>
		        					<td>
				 						<?=$r['club']?><br/>
		        						<?=$r['team_2_club'];?>
									</td>
		        					<td>
		        						<?=$r['team_1_name'];?><br>
		        						<?=$r['team_2_name'];?><br>
		        					</td>
		        				</tr>
		        				<?php }else if($series['division'] == "단체전"){
								?>
		        				<tr>
		        					<td width="80">
				 					<?=$r['area_1'];?> / <?=$r['area_2'];?><br/>
				 					<?=$r['club']?>
		        					</td>
		        					<td>
		        						<?=$r['team_1_name'];?>, <?=$r['team_2_name'];?>, <?=$r['team_3_name'];?>, <?=$r['team_4_name'];?><br/>
										<?=$r['team_5_name'];?>, <?=$r['team_6_name'];?><?php if($r['team_7_name'] != ""){echo ",&nbsp;";} ?><?=$r['team_7_name'];?><?php if($r['team_8_name'] != ""){echo ",";} ?>
										<?=$r['team_8_name'];?><?php if($r['team_9_name'] != ""){echo ",";} ?>
										<?=$r['team_9_name'];?><?php if($r['team_10_name'] != ""){echo ",";} ?>
										<?=$r['team_10_name'];?>
									</td>
		        				</tr>

		        				<?php } ?>

							<?php }?>
							<!-- //team1-->

							<!-- //team2-->
	        				<?php
            					if($group['team_2']!=""){
            					$team_sql = "select * from $team_field where match_code = '$code' and team_code = '$group[team_2]'";
								$team_result = sql_query($team_sql);
								$r = sql_fetch_array($team_result);
            				?>

								<?php
				        			if($series['division'] == "개인전"){
								?>

								<tr>
		        					<td>
										<?=$r['area_1'];?> / <?=$r['area_2'];?><br/>
		        						<?=$r['area_1'];?> / <?=$r['team_2_area'];?>
		        					</td>
		        					<td>
				 						<?=$r['club']?><br/>
		        						<?=$r['team_2_club'];?>
									</td>
		        					<td>
		        						<?=$r['team_1_name'];?><br>
		        						<?=$r['team_2_name'];?><br>
		        					</td>
		        				</tr>
		        				<?php }else if($series['division'] == "단체전"){
								?>
		        				<tr>
		        					<td>
				 					<?=$r['area_1'];?> / <?=$r['area_2'];?><br/>
				 					<?=$r['club']?>
		        					</td>
		        					<td>
												<?=$r['team_1_name'];?>, <?=$r['team_2_name'];?>, <?=$r['team_3_name'];?>, <?=$r['team_4_name'];?><br/>
										<?=$r['team_5_name'];?>, <?=$r['team_6_name'];?><?php if($r['team_7_name'] != ""){echo ",&nbsp;";} ?><?=$r['team_7_name'];?><?php if($r['team_8_name'] != ""){echo ",";} ?>
										<?=$r['team_8_name'];?><?php if($r['team_9_name'] != ""){echo ",";} ?>
										<?=$r['team_9_name'];?><?php if($r['team_10_name'] != ""){echo ",";} ?>
										<?=$r['team_10_name'];?>
									</td>
		        				</tr>

		        				<?php } ?>

							<?php }?>
							<!-- //team2-->

							<!-- //team3-->
	        				<?php
            					if($group['team_3']!=""){
            					$team_sql = "select * from $team_field where match_code = '$code' and team_code = '$group[team_3]'";
								$team_result = sql_query($team_sql);
								$r = sql_fetch_array($team_result);
            				?>

								<?php
				        			if($series['division'] == "개인전"){
								?>

								<tr>
		        					<td>
										<?=$r['area_1'];?> / <?=$r['area_2'];?><br/>
		        						<?=$r['area_1'];?> / <?=$r['team_2_area'];?>
		        					</td>
		        					<td>
				 						<?=$r['club']?><br/>
		        						<?=$r['team_2_club'];?>
									</td>
		        					<td>
		        						<?=$r['team_1_name'];?><br>
		        						<?=$r['team_2_name'];?><br>
		        					</td>
		        				</tr>
		        				<?php }else if($series['division'] == "단체전"){
								?>
		        				<tr>
		        					<td>
				 					<?=$r['area_1'];?> / <?=$r['area_2'];?><br/>
				 					<?=$r['club']?>
		        					</td>
		        					<td>
												<?=$r['team_1_name'];?>, <?=$r['team_2_name'];?>, <?=$r['team_3_name'];?>, <?=$r['team_4_name'];?><br/>
										<?=$r['team_5_name'];?>, <?=$r['team_6_name'];?><?php if($r['team_7_name'] != ""){echo ",&nbsp;";} ?><?=$r['team_7_name'];?><?php if($r['team_8_name'] != ""){echo ",";} ?>
										<?=$r['team_8_name'];?><?php if($r['team_9_name'] != ""){echo ",";} ?>
										<?=$r['team_9_name'];?><?php if($r['team_10_name'] != ""){echo ",";} ?>
										<?=$r['team_10_name'];?>
									</td>
		        				</tr>

		        				<?php } ?>

							<?php }?>
							<!-- //team3-->
							<!-- //team3-->
	        				<?php
            					if($group['team_4']!=""){
            					$team_sql = "select * from $team_field where match_code = '$code' and team_code = '$group[team_4]'";
								$team_result = sql_query($team_sql);
								$r = sql_fetch_array($team_result);
            				?>

								<?php
				        			if($series['division'] == "개인전"){
								?>

								<tr>
		        					<td>
										<?=$r['area_1'];?> / <?=$r['area_2'];?><br/>
		        						<?=$r['area_1'];?> / <?=$r['team_2_area'];?>
		        					</td>
		        					<td>
				 						<?=$r['club']?><br/>
		        						<?=$r['team_2_club'];?>
									</td>
		        					<td>
		        						<?=$r['team_1_name'];?><br>
		        						<?=$r['team_2_name'];?><br>
		        					</td>
		        				</tr>
		        				<?php }else if($series['division'] == "단체전"){
								?>
		        				<tr>
		        					<td>
				 					<?=$r['area_1'];?> / <?=$r['area_2'];?><br/>
				 					<?=$r['club']?>
		        					</td>
		        					<td>
												<?=$r['team_1_name'];?>, <?=$r['team_2_name'];?>, <?=$r['team_3_name'];?>, <?=$r['team_4_name'];?><br/>
										<?=$r['team_5_name'];?>, <?=$r['team_6_name'];?><?php if($r['team_7_name'] != ""){echo ",&nbsp;";} ?><?=$r['team_7_name'];?><?php if($r['team_8_name'] != ""){echo ",";} ?>
										<?=$r['team_8_name'];?><?php if($r['team_9_name'] != ""){echo ",";} ?>
										<?=$r['team_9_name'];?><?php if($r['team_10_name'] != ""){echo ",";} ?>
										<?=$r['team_10_name'];?>
									</td>
		        				</tr>

		        				<?php } ?>

							<?php }?>
							<!-- //team4-->


	        			</tbody>
	                </table>
				</div>
			</li>
		<?php $i++;} ?>
	</div>
	<?php $i++;} ?>

</div>


</page>
<script>
	//window.print();
</script>
