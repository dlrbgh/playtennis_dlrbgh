<?php
$menu_cate2 ='2';
$menu_cate3 ='';
$menu_cate4 ='1';
$menu_cate5 ='2';
include_once('./_common.php');
include_once(G5_SADM_PATH.'/head.php');

$match = sql_fetch("select * from match_data where code = '$code'");

$map_result = sql_query("select * from match_gym_data where match_id = '$code'");

?>
  <link rel="stylesheet" href="../dist/remodal.css">
  <link rel="stylesheet" href="../dist/remodal-default-theme.css">

	
<!-- sub_hd_tab_area -->
	<?php include_once(G5_SADM_PATH.'/inc/sub_hd.inc');?>
<!--//sub_hd_tab_area -->

<!-- !PAGE CONTENT! -->
<div>
	<!-- 대회정보 필수입력-->
	<div class="tbl_style01 tbl_striped tbl_borderd champ_view_map">
		<table>
			<thead>
				<tr>
					<th>경기장 No</th>
					<th>경기장 정보</th>
				</tr>
			</thead>
			<tbody>
				<?php 
					$i = 1;
					while($r = sql_fetch_array($map_result)){ 
					$gym_data = sql_fetch("select * from gym_data where wr_id = '$r[gym_id]'");
				?>
				<tr>
					<td class="text-center" width="100">경기장<?php echo $i;?></td>
					<td>
						<!-- <img src="https://placeholdit.imgix.net/~text?txtsize=47&txt=1000%C3%97350&w=1000&h=350"> -->
						<div id="map" class="push-10-t" style="height: 300px;"></div>	

						<div class="tit">
							<?php echo $gym_data['gym_name']?> (<?php echo $gym_data['gym_address3']?>)
						</div>
					</td>
				</tr>
				<?php $i++; }?>
			</tbody>
		</table>
	</div>
	<!-- //대회정보 필수입력-->
	
	<div class="btn_area">
		<!-- 수정할때 출력-->
		<a class="btn btn02 delete fw-600" onclick="">수 정</a>
	</div>

</div>


<script type="text/javascript" src="//apis.daum.net/maps/maps3.js?apikey=85629d9f847388a273d6deba7dce3bbd&libraries=services"></script>

<script>
// 마커를 클릭하면 장소명을 표출할 인포윈도우 입니다
var infowindow = new daum.maps.InfoWindow({zIndex:1});
<?php 
$map_result = sql_query("select * from match_gym_data where match_id = '$code'");
	$i = 1;
	while($r = sql_fetch_array($map_result)){ 
	$gym_data = sql_fetch("select * from gym_data where wr_id = '$r[gym_id]'");
?>
var mapContainer = document.getElementById('map'), // 지도를 표시할 div 
    mapOption = {
        center: new daum.maps.LatLng(<?php echo $gym_data['lat'];?>, <?php echo $gym_data['lng'];?>), // 지도의 중심좌표
        level: 3 // 지도의 확대 레벨
    };  
<?php $i++; } ?>

// 지도를 생성합니다    
var map = new daum.maps.Map(mapContainer, mapOption); 

// 장소 검색 객체를 생성합니다
var ps = new daum.maps.services.Places(); 

// 키워드로 장소를 검색합니다

function search(){
	var search = document.getElementById('gym-map');
	ps.keywordSearch(search.value, placesSearchCB); 

}
// 키워드 검색 완료 시 호출되는 콜백함수 입니다
function placesSearchCB (status, data, pagination) {
    if (status === daum.maps.services.Status.OK) {

        // 검색된 장소 위치를 기준으로 지도 범위를 재설정하기위해
        // LatLngBounds 객체에 좌표를 추가합니다
        var bounds = new daum.maps.LatLngBounds();

        for (var i=0; i<data.places.length; i++) {
            displayMarker(data.places[i]);    
            bounds.extend(new daum.maps.LatLng(data.places[i].latitude, data.places[i].longitude));
        }       

        // 검색된 장소 위치를 기준으로 지도 범위를 재설정합니다
        map.setBounds(bounds);
    } 
}

// 지도에 마커를 표시하는 함수입니다
function displayMarker(place) {
    
    // 마커를 생성하고 지도에 표시합니다
    var marker = new daum.maps.Marker({
        map: map,
        position: new daum.maps.LatLng(place.latitude, place.longitude) 
    });
	console.log(place);
    // 마커에 클릭이벤트를 등록합니다
    daum.maps.event.addListener(marker, 'click', function() {
        // 마커를 클릭하면 장소명이 인포윈도우에 표출됩니다
        document.getElementById('gym-name').value = place.title;
        document.getElementById('gym-add3').value = place.address;
        document.getElementById('gym-add4').value = place.newAddress;

        document.getElementById('lat').value = place.latitude;
        document.getElementById('lng').value = place.longitude;

        infowindow.setContent('<div style="padding:5px;font-size:12px;">' + place.title + '</div>');
        infowindow.open(map, marker);
    });
}
</script>


<?php
include_once(G5_SADM_PATH.'/tail.php');
?>