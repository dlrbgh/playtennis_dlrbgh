<?php
$menu_cate2 ='2';
$menu_cate3 ='1';
include_once('./_common.php');
include_once(G5_SADM_PATH.'/head.php');

?>

<!-- sub hd_section-->
	<div class="tit_area">
		<div class="tit_subject">대회관리</div>
		<div class="tit">예정대회 리스트</div>
		<div class="r-btn-area">
			<ul>
				<li><a href="<?php echo G5_SADM_URL?>/championship_general_add.php" class="btn_default btn_large">일반대회 생성</a></li>
				<li><a href="<?php echo G5_SADM_URL?>/championship_add.php" class="btn_default btn_large">중계대회 생성</a></li>
			</ul>
		</div>
	</div>


	<!-- sub_hd_area -->
	<div class="sub_hd">
	    <div class="l_area">
	        <ul>
	        	<li><a href="./championship_list.php?division=&end_game=<?php echo $end_game;?>" class="btn_default <?php if($division == ""){ echo "active";}?>">전체</a></li>
	        	<li><a href="./championship_list.php?division=1&end_game=<?php echo $end_game;?>" class="btn_default <?php if($division == "1"){ echo "active";}?>">일반대회</a></li>
	        	<li><a href="./championship_list.php?division=2&end_game=<?php echo $end_game;?>" class="btn_default <?php if($division == "2"){ echo "active";}?>">중계대회</a></li>
	        </ul>
	        <ul>
	        	<li><a href="./championship_list.php?division=<?php echo $division;?>" class="btn_default <?php if($end_game == ""){ echo "active";}?>">전체</a></li>
	        	<li><a href="./championship_list.php?end_game=1&division=<?php echo $division;?>" class="btn_default <?php if($end_game == "1"){ echo "active";}?>">접수대기</a></li>
	        	<li><a href="./championship_list.php?end_game=2&division=<?php echo $division;?>" class="btn_default <?php if($end_game == "2"){ echo "active";}?>">접수중</a></li>
	        	<li><a href="./championship_list.php?end_game=3&division=<?php echo $division;?>" class="btn_default <?php if($end_game == "3"){ echo "active";}?>">접수마감</a></li>
	        </ul>

	    </div>

		<!-- 게시판 검색 시작 { -->
		<div class="r_area">
			<ul>
				<li></li>
			</ul>
		</div>
		<!-- } 게시판 검색 끝 -->
	</div>
	<!--//sub_hd_area -->
<div class="bo_hd">
    <div id="bo_list_total">
        <span class="color5"><img class="ico_small" src="http://test.raizup.kr/kesson04/img/common/svg/presentation.svg">Total </span>
        <span>4건</span> / 1 페이지
    </div>
	<div class="r_area">
		<ul>
			<li>
				<!-- 게시판 검색 시작 { -->
				<fieldset id="bo_sch">
				    <legend>게시물 검색</legend>
				    <form name="fsearch" method="get">
				    <input name="bo_table" value="site" type="hidden">
				    <input name="sca" value="" type="hidden">
				    <input name="sop" value="and" type="hidden">
				    <label for="sfl" class="sound_only">검색대상</label>
				    <select name="sfl" id="sfl">
				        <option value="wr_subject">대회명</option>
				        <option value="wr_content">대회장소</option>
				        <option value="wr_subject||wr_content">대회구분</option>
				        <option value="wr_name,1">상태</option>
				    </select>
				    <label for="stx" class="sound_only">검색어<strong class="sound_only"> 필수</strong></label>
				    <input name="stx" value="" required="" id="stx" class="frm_input required" size="40" maxlength="20" placeholder="항목을 입력해 주세요" type="text">
				    <input value="" class="btn_submit" type="submit">
				    </form>
				</fieldset>
				<!-- } 게시판 검색 끝 -->
			</li>
		</ul>
	</div>
</div>


<!-- !PAGE CONTENT! -->
<div>
	<div class="tbl_style01">
		<table>
			<thead>
				<tr>
					<th width="50">No</th>
					<th>대회명</th>
					<th width="190">일시및 장소</th>

					<th width="50">대회구분</th>
					<th>주최</th>
					<th width="40">담당자</th>
					<th>대진표</th>
					<th width="100">상태</th>
					<th width="40">수정</th>
				</tr>
			</thead>
			<tbody class="text-center">
				<!-- 대회정보-->
				<?php
					$i = 1;
					if($division != ""){
						$sqlsearch = "where division = '$division'";
					}
					if($end_game != ""){
						if($sqlsearch == ""){
							$sqlsearch = "where end_game = '$end_game'";
						}
						else{
							$sqlsearch .= "and end_game = '$end_game'";
						}

					}

					$sql = "select * from match_data $sqlsearch order by date1 desc";
					$result = sql_query($sql);
					while($r = sql_fetch_array($result)){
						if($r['scale'] == 1){
							$scale = "KTA";
						}
						if($r['scale'] == 2){
							$scale = "KATO";
						}
						if($r['scale'] == 3){
							$scale = "KATA";
						}
						if($r['scale'] == 4){
							$scale = "KASTA";
						}
						if($r['scale'] == 5){
							$scale = "LOCAL";
						}
						if($r['scale'] == 6){
							$scale = "비랭킹";
						}

				?>

				<tr>
					<td><?php echo $i;?></td>
					<?php
	           			$match_sql = "select * from match_gym_data where match_id='$r[code]' order by application_period,wr_id";
						$match_result = sql_query($match_sql);
						$match = sql_fetch_array($match_result);
            			$url = "currentstatus_table.php?code=$r[code]&gym_id=$match[gym_id]&application_period=$match[application_period]";
					?>
					<td class="text-left"><a href="<?php echo $url;?>"><?php echo $r['wr_name']?></a><a href="champ_view.php?code=<?php echo "$r[code]";?>"> (정보보기)</a></td>
					<td><?php echo $r['date1']."~".$r['date2']?>
						<div><?php echo $r['opening_place']; ?></div>
					</td>

					<td><?php echo $scale;?></td>
					<td><?php echo $r['organizer']?></td>
					<td></td>
					<td><a href="" class="btn_default" target="_blank">다운</a></td>
					<td class="text-center champ_status_sel">
                        <!-- 중계대회일경우 -->
                        <form action="match_end.php">
                        	<input name="code" value="<?=$r['code'];?>" type="hidden">
                            <select class="" id="" name="end_game">
                                <option value="">현재상태출력</option>
                                <option value="1" <?php if($r['end_game'] == "1") echo "selected";?>>접수대기</option>
                                <option value="2" <?php if($r['end_game'] == "2") echo "selected";?>>접수중</option>
                                <option value="3" <?php if($r['end_game'] == "3") echo "selected";?>>접수마감</option>
                                <option value="3" <?php if($r['end_game'] == "4") echo "selected";?>>대진공개</option>
                                <option value="3" <?php if($r['end_game'] == "5") echo "selected";?>>경기중</option>
                                <option value="5" <?php if($r['end_game'] == "6") echo "selected";?>>종료</option>
                            </select>
                             
							<select class="" id="" name="app_visible">
                                <option value="">현재상태출력</option>
                                <option value="1" <?php if($r['app_visible'] == "1") echo "selected";?>>비공개</option>
                                <option value="2" <?php if($r['app_visible'] == "2") echo "selected";?>>공개</option>
                            </select>
                            
							<button class="btn btn-default btn-xs">적용</button>
	                    </form>
	                    <!-- //일반대회일경우 -->
		            </td>
		            <style>
		            	.champ_status_sel select{padding:3px;display:block;margin-bottom:2px}
		            	.champ_status_sel button{display:block;width:100%}
		            	.btn-xs{}
		            </style>
		            <td class="text-center">
                            <div class="btn-group">
                            	<?php
                            		$url = "championship_add.php?code=$r[code]&wr_id=$r[wr_id]";
                            		if($r['step'] == 1){
                            		}
									if($r['step'] == 2){
                            			$url = "championship_add_step1.php?code=$r[code]";
                            		}
									if($r['step'] == 3){
                            			$url = "championship_add_step2.php?code=$r[code]";
                            		}
									if($r['step'] == 4){
                            			$url = "championship_add_step3.php?code=$r[code]";
                            		}
									if($r['step'] == 5){
                            			$url = "championship_add_step4.php?code=$r[code]";
                            		}
                            		if($r['step'] == 5){
                            			$match_sql = "select * from match_gym_data where match_id='$r[code]' order by application_period";
										$match_result = sql_query($match_sql);
										$match = sql_fetch_array($match_result);
                            			$url = "timetable_assignment.php?code=$r[code]&gym_id=$match[gym_id]&application_period=$match[application_period]";
										$url = "championship_add_step4.php?code=$r[code]";
                            		}
									if($r['step'] == 6){
                            			$match_sql = "select * from match_gym_data where match_id='$r[code]' order by application_period";
										$match_result = sql_query($match_sql);
										$match = sql_fetch_array($match_result);
                            			$url = "timetable_assignment.php?code=$r[code]&gym_id=$match[gym_id]&application_period=$match[application_period]";
                            			$url = "championship_add_step4.php?code=$r[code]";
                            		}
                            		$url = "championship_add.php?code=$r[code]";


                            	?>
                            	<?php
                            	if($member['mb_id'] == "admin" || $member['mb_id'] == "cockss" ){
		                		?>
                                <a class="btn btn-default" href="<?=$url;?>">수정</a><br>
                                <a class="btn btn-default bkg5" onclick="delete_data('<?php echo  $r['code'];?>');">삭제</a>
		                		<?php
		                		}
                            	?>
                            </div>
                        </td>
				</tr>
				<?php
					}
				?>
				<!-- //대회정보-->
			</tbody>
		</table>
	</div>


</div>

<script>
function delete_data(code){

var r = confirm("대회를 삭제하시겠습니까?");
if (r == true) {
    location.href = 'delete_data.php?code='+code;
} else {

}
}
</script>


<!-- End page content -->

<?php
include_once(G5_SADM_PATH.'/tail.php');
?>
