<?php
$menu_cate2 ='2';
$menu_cate3 ='2';
include_once('./_common.php');
include_once('./head.sub.php');

$game_code = $_REQUEST['game_code']

?>
<?php
$sql = "select a.*,b.division,b.series,b.series_sub,b.num from game_score_data as a inner join group_data as b 
		where a.code = '$game_code' and a.group_code = b.code group by a.game_increase order by a.game_increase asc";
$game_result = sql_query($sql);
$game_data = sql_fetch_array($game_result);
$team_field = "team_data";

if($game_data['division'] == "단체전"){
	$team_field = "team_event_data";	
}
if($team == "1"){
	$sql_team1 = "select * from $team_field where match_code = '{$game_data[match_code]}' and team_code = '$game_data[team_1_code]'";
}
if($team == "2"){
	$sql_team1 = "select * from $team_field where match_code = '{$game_data[match_code]}' and team_code = '$game_data[team_2_code]'";
}

$team1_result = sql_query($sql_team1);
$team1 = sql_fetch_array($team1_result);

if($game_data['division'] == "단체전"){
?>
<style>
.pop_container{padding:15px}
table {border-collapse: collapse;border-spacing: 0;}
.tbl_style03 table {margin:0 0 10px;width:100%;font-size:2em;min-width:400px}
.tbl_style03 table caption {padding:0;font-size:0;line-height:0;overflow:hidden}
.tbl_style03 table thead th {padding:7px 0;border-top:2px solid #000000;border-bottom:1px solid #000000;background:#fff;color:#777;font-size:0.8em;text-align:center;letter-spacing:-0.1em}
.tbl_style03 table thead a {color:#383838}
.tbl_style03 table thead th input {vertical-align:top} /* middle 로 하면 게시판 읽기에서 목록 사용시 체크박스 라인 깨짐 */
.tbl_style03 table tfoot th, .tbl_head01 tfoot td {padding:10px 0;border-top:1px solid #c1d1d5;border-bottom:1px solid #c1d1d5;background:#d7e0e2;text-align:center}
.tbl_style03 table tbody th {padding:13px 0;border-top:1px solid #e9e9e9;border-bottom:1px solid #e9e9e9;font-size:0.8em}
.tbl_style03 table td {text-align: center;padding:5px 3px;border-top:1px solid #e9e9e9;border-bottom:1px solid #e9e9e9;line-height:1.5em;word-break:break-all;font-size:0.8em;}
.tbl_style03 table td.match_point {color:#aaa;font-size:3em;}
.tbl_style03 table td.match_point input{width:80px;text-align:center;font-size:1.2em;border:1px solid #aaa;}
.tbl_style03 table td.match_point input:focus{background-color:#f2f5f9;border-color:#000}
.tbl_style03 table td.match_point span:first-child{padding-right:5px}
.tbl_style03 table td.match_point span:last-child{padding-left:5px}
.tbl_style03 table td.match_point .playing{font-size:16px;}


.btn_group a.score_btn{padding:0 5px;border:1px solid #ccc;display:block;cursor:pointer;text-decoration:none;margin-bottom:3px;color:#000;font-size:15px}
.btn_group a.score_btn:hover{background-color:#ccc;border-color:#bbb}
.btn_group a.score_btn:active{background-color:#14264a;color:#fff}

.hd_tit{margin-bottom:5px;margin-top:15px;font-weight:700;font-size:22px}
.btn_area input{width:100%;padding:5px;box-sizing:border-box;}
.btn_area .btn_submit{padding:15px;width:100%;display:block;margin-top:5px;background-color:#fafafa;color:#292929;border:1px solid #ccc;cursor:pointer}
.btn_area .btn_submit:hover{background-color:#14264a;color:#fff;font-weight:700}

.history_log_area {font-size:13px;margin-top:20px}
.history_log_area .tit{font-weight:700;margin-bottom:5px;font-size:22px}
.history_log_area ul{margin:0;padding:0;list-style-type:none;border:1px solid #ccc;padding:5px }
.history_log_area ul li{margin-bottom:3px}	
</style>


<form name="frmReview" action="insert_team_event_data.php" method="post" id="frmReview" >
	
<input type="hidden" name="game_code" value="<?php echo $game_code;?>" />
<input type="hidden" name="team_chk" value="<?php echo $team;?>" />
<input type="hidden" name="tournament" value="<?php echo $game_data['tournament'];?>" />
<div class="pop_container">
	<div class="tbl_style03 tbl_striped">
		<div class="hd_tit">
			<?php echo $team1['club']?>클럽  단체전 
		</div>
		
		<table>
			<thead>
				<tr><th>이름</th><th>1경기</th><th>2경기</th><th>3경기</th><th>미배정</th></tr>
			</thead>
			<tbody>
			<tr>
				<Td><?php echo $team1['team_1_name'];?></Td><td><input type="radio" checked=""  name="team_1[0]" value="1" /></td><td><input name="team_1[0]" type="radio" value="2" /></td><td><input name="team_1[0]" type="radio" value="3" /></td><td><input name="team_1[0]" type="radio" value="" /></td>
			</tr>
			<tr>
				<Td><?php echo $team1['team_2_name'];?></Td><td><input type="radio" checked=""  name="team_1[1]" value="1" /></td><td><input name="team_1[1]" type="radio" value="2" /></td><td><input name="team_1[1]" type="radio" value="3" /></td><td><input name="team_1[1]" type="radio" value="" /></td>
			</tr>
			<tr>
				<Td><?php echo $team1['team_3_name'];?></Td><td><input type="radio" name="team_1[2]" value="1" /></td><td><input name="team_1[2]" checked=""  type="radio" value="2" /></td><td><input name="team_1[2]" type="radio" value="3" /></td><td><input name="team_1[2]" type="radio" value="" /></td>
			</tr>
			<tr>
				<Td><?php echo $team1['team_4_name'];?></Td><td><input type="radio" name="team_1[3]" value="1" /></td><td><input name="team_1[3]" checked=""  type="radio" value="2" /></td><td><input name="team_1[3]" type="radio" value="3" /></td><td><input name="team_1[3]" type="radio" value="" /></td>
			</tr>
			<tr>
				<Td><?php echo $team1['team_5_name'];?></Td><td><input type="radio" name="team_1[4]" value="1" /></td><td><input name="team_1[4]" type="radio" value="2" /></td><td><input name="team_1[4]" checked=""  type="radio" value="3" /></td><td><input name="team_1[4]" type="radio" value="" /></td>
			</tr>
			<tr>
				<Td><?php echo $team1['team_6_name'];?></Td><td><input type="radio" name="team_1[5]" value="1" /></td><td><input name="team_1[5]" type="radio" value="2" /></td><td><input name="team_1[5]" checked=""  type="radio" value="3" /></td><td><input name="team_1[5]" type="radio" value="" /></td>
			</tr>
			<tr>
				<Td><?php echo $team1['team_7_name'];?></Td><td><input type="radio" name="team_1[6]" value="1" /></td><td><input name="team_1[6]" type="radio" value="2" /></td><td><input name="team_1[6]" type="radio" value="3" /></td><td><input name="team_1[6]" type="radio" checked=""  value="" /></td>
			</tr>
			<tr>
				<Td><?php echo $team1['team_8_name'];?></Td><td><input type="radio" name="team_1[7]" value="1" /></td><td><input name="team_1[7]" type="radio" value="2" /></td><td><input name="team_1[7]" type="radio" value="3" /></td><td><input name="team_1[7]" type="radio" checked=""  value="" /></td>
			</tr>
			<tr>
				<Td><?php echo $team1['team_9_name'];?></Td><td><input type="radio" name="team_1[8]" value="1" /></td><td><input name="team_1[8]" type="radio" value="2" /></td><td><input name="team_1[8]" type="radio" value="3" /></td><td><input name="team_1[8]" type="radio" checked=""  value="" /></td>
			</tr>

			</tbody>
		</table>
	</div>
	<div class="btn_area">
		<input class="btn_submit" type="submit" value="저장" />
	</div>
</div>

</form>
<?php	
}if($game_data['division']=="개인전"){
	if($game_data['tournament'] == "T"){
		if($game_data['tournament_count'] == "32"){
		}
		if($game_data['tournament_count'] == "16"){
			$sql = "select a.* from game_score_data as a 
				where tournament_count = '32' and a.match_code = '$game_data[match_code]' and a.division = '$game_data[division]' and a.series = '$game_data[series]' and a.series_sub = '$game_data[series_sub]' order by a.game_increase asc";
		}
		if($game_data['tournament_count'] == "8"){
			$sql = "select a.* from game_score_data as a 
				where tournament_count = '16' and a.match_code = '$game_data[match_code]' and a.division = '$game_data[division]' and a.series = '$game_data[series]' and a.series_sub = '$game_data[series_sub]' order by a.game_increase asc";
		}
		if($game_data['tournament_count'] == "4"){
			$sql = "select a.* from game_score_data as a 
				where tournament_count = '8' and a.match_code = '$game_data[match_code]' and a.division = '$game_data[division]' and a.series = '$game_data[series]' and a.series_sub = '$game_data[series_sub]' order by a.game_increase asc";
		}
		if($game_data['tournament_count'] == "2"){
			$sql = "select a.* from game_score_data as a 
				where tournament_count = '4' and a.match_code = '$game_data[match_code]' and a.division = '$game_data[division]' and a.series = '$game_data[series]' and a.series_sub = '$game_data[series_sub]' order by a.game_increase asc";
		}
		?>
		<form name="frmReview" action="insert_team_event_data.php" method="post" id="frmReview" >
			
		<input type="hidden" name="game_code" value="<?php echo $game_code;?>" />
		<input type="hidden" name="team_chk" value="<?php echo $team;?>" />
		<input type="hidden" name="tournament" value="<?php echo $game_data['tournament'];?>" />
		
		<table>
			<tr><td>이름</td><td>배정</td></tr>

		<?php
		$game_resulted = sql_query($sqled);
		while($game_dataed = sql_fetch_array($game_resulted)){
			$team_field = "team_data";
			
			if($game_data['division'] == "단체전"){
				$team_field = "team_event_data";	
			}
			if($game_dataed['team_1_score'] > $game_dataed['team_2_score']){
				$sql_team1 = "select * from $team_field where match_code = '{$game_dataed[match_code]}' and team_code = '$game_dataed[team_1_code]'";
			}
			if($game_dataed['team_2_score'] > $game_dataed['team_1_score']){
				$sql_team1 = "select * from $team_field where match_code = '{$game_dataed[match_code]}' and team_code = '$game_dataed[team_2_code]'";
			}
			$team1_result = sql_query($sql_team1);
			$team1 = sql_fetch_array($team1_result);
		?>
		<tr>
			<Td><?php echo $team1['club'];?><?php echo $team1['team_1_name'];?><?php echo $team1['team_2_name'];?></Td><td></td>
		</tr>
		<?php
		}
		?>
			</tr>
		</table>
		<?php		
	}
?>

<?php	
}
?>
<script>

$("form#frmReview").submit(function(event){
	var checked1 = 0;
	var checked2 = 0;
	var checked3 = 0;  
	
	$("input[type=radio]").each(function(index,elem){
			
		if($(elem).is(":checked")){
			
			if($(elem).val() == "1"){
				checked1++;
			}
			if($(elem).val() == "2"){
				checked2++;
			}
			if($(elem).val() == "3"){
				checked3++;
			}
		}	
	})
	
	if(checked1 != "2"){
		alert("1경기를 2명 선택해주세요");
		return false;
	}
	if(checked2 != "2"){
		alert("2경기를 2명 선택해주세요");
		return false;
	}
	if(checked3 != "2"){
		alert("3경기를 2명 선택해주세요");
		return false;
	}
	return true;

});
	
</script>