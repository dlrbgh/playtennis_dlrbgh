<?php
$menu_cate2 ='2';
$menu_cate3 ='2';
include_once('./_common.php');
include_once(G5_SADM_PATH.'/head.php');

$match = sql_fetch("select * from match_data where code = '$code'");

?>
  <link rel="stylesheet" href="../dist/remodal.css">
  <link rel="stylesheet" href="../dist/remodal-default-theme.css">

<!-- sub hd_section-->
	<div class="tit_area">
		<div class="tit_subject">대회관리</div>
		<div class="tit">대회명 출력</div>
	</div>
<!--//sub_hd_area -->
	
<!-- sub_hd_area -->
<div class="tab_hd tab_style01">
    <a class="" href="champ_view.php?code=<?php echo $code;?>">대회안내</a>
    <a class="active" href="champ_apply_view.php?code=<?php echo $code;?>">접수현황</a>
    <a class="" href="champ_matchtbl_view.php?code=<?php echo $code;?>">대진표</a>
    <a class="" href="champ_live_view.php?code=<?php echo $code;?>">대회운영</a>
    <a class="" href="champ_endrank_view.php?code=<?php echo $code;?>">경기결과</a>
</div>

<div class="sub_hd">
	<div class="l_area">
        <ul>
        	<li><a href="champ_apply_mbmer_list.php?code=<?php echo $code;?>&division=&series=&series_sub=" class="btn_default <?php if($division == ""){ echo "active"; };?>">전체</a></li>
        	<?php 
        		$sql = "select * from series_data where match_code = '$code' group by division";
				$result = sql_query($sql);
				while($r = sql_fetch_array($result)){
			?>
        	<li><a href="champ_apply_mbmer_list.php?code=<?php echo $code;?>&division=<?php echo $r['division'];?>&series=<?php echo $series;?>&series_sub=<?php echo $series_sub;?>" class="btn_default <?php if($r['division'] == $division){ echo "active"; };?>"><?php echo $r['division']?></a></li>
        	<?php		
				}
        	?>
        </ul>
        <ul>
        	<li><a href="champ_apply_mbmer_list.php?code=<?php echo $code;?>&division=<?php echo $division;?>&series=&series_sub=" class="btn_default <?php if($series == ""){ echo "active"; };?>">전체</a></li>
        	<?php 
        		$search = "";
				if($division != ""){
					$search .= " and division = '$division'";
				}
				if($series != ""){
					$search .= " and series = '$series'";
				}

				
        		$sql = "select * from series_data where match_code = '$code' $search  group by series";
				$result = sql_query($sql);
				while($r = sql_fetch_array($result)){
			?>
        	<li><a href="champ_apply_mbmer_list.php?code=<?php echo $code;?>&division=<?php echo $division;?>&series=<?php echo $r['series'];?>&series_sub=<?php echo $series_sub;?>" class="btn_default <?php if($r['series'] == $series){ echo "active"; };?>"><?php echo $r['series']?></a></li>
        	<?php		
				}
        	?>
        </ul>
         <ul>
        	<li><a href="champ_apply_mbmer_list.php?code=<?php echo $code;?>&" class="btn_default <?php if($series_sub == ""){ echo "active"; };?>">전체</a></li>
         	<?php 
        		$sql = "select * from series_data where match_code = '$code' $search group by series_sub";
				$result = sql_query($sql);
				while($r = sql_fetch_array($result)){
			?>
        	<li><a href="champ_apply_mbmer_list.php?code=<?php echo $code;?>&division=<?php echo $division;?>&series=<?php echo $series;?>&series_sub=<?php echo $r['series_sub'];?>" class="btn_default <?php if($r['series_sub'] == $series_sub){ echo "active"; };?>"><?php echo $r['series_sub']?></a></li>
        	<?php		
				}
        	?>
        </ul>
    </div>
</div>
<!--//sub_hd_area -->

<!-- !PAGE CONTENT! -->
<div>
	<!-- 접수현황 DB-->
	<div class="tbl_style01 tbl_borderd">
		
		<table>
			<thead>
				<tr>
					<th>No</th>
					<th>경기구분</th>
					<th>성별</th>
					<th>참가부</th>
					<th>지역</th>
					<th>소속클럽</th>
					<th>이름</th>
					<th>등급</th>
					<th>연령</th>
					<th>확인일</th>
					<th>입금확인</th>	
				</tr>
			</thead>
			<tbody class="text-center">
				<?php
					$i = 1;
					$search = "";
					if($division != ""){
						$search .= " and division = '$division'";
					}
					if($series != ""){
						$search .= " and series = '$series'";
					}
					if($series_sub != ""){
						$search .= " and series_sub = '$series_sub'";
					}
					
					if($division == "개인전"){						
						$result = sql_query("select * from team_data where match_code = '$code' $search");
					}
					if($division == "단체전"){
						$result = sql_query("select * from team_event_data where match_code = '$code' $search");
					}
					if($division == ""){
						$result = sql_query("select * from team_data where match_code = '$code' $search");
					}
					
					while($r = sql_fetch_array($result)){
				?>
				<tr>
					<td><?php echo $i;?></td>
					<td><a href=""><?php echo $r['division']?></a></td>
					<td><a href=""><?php echo $r['series']?></a></td>
					<td><a href=""><?php echo $r['series_sub']?></a></td>
					<td><a href=""><?php echo $r['area_1']?>-<?php echo $r['area_2']?></a></td>
					<td><a href=""><?php echo $r['club']?></a></td>
					<td><?php echo $r['team_1_name']?></td>
					<td><!-- <a href="">6등급</a> --></td>
					<td><!-- 34세 --></td>
					<td>
						<!-- <ul>
							<li>상태 로그 영역</li>
							<li>2017-04-23:11:30 <font class="color5">입금</font></li>
							<li>2017-04-23:11:30 <font class="color5">취소</font></li>
							
						</ul> -->
					</td>
								        
					<td>
						 <!-- <div class="dropdown">
						  <button onclick="statusChange()" class="dropbtn">현재상태출력</button>
						  <div id="statusChange" class="dropdown-content">
						    <a href="#">접수중</a>
						    <a href="#">입금대기</a>
						    <a href="#">입금완료</a>
						    <a href="#">취소</a>
						  </div>
						</div> -->
					</td>
				</tr>
				<?php		
				$i++;
					}
				?>
				<?php
					$search = "";
					if($division != ""){
						$search .= " and division = '$division'";
					}
					if($series != ""){
						$search .= " and series = '$series'";
					}
					if($series_sub != ""){
						$search .= " and series_sub = '$series_sub'";
					}
					
					if($division == ""){
						$result = sql_query("select * from team_event_data where match_code = '$code' $search");
					}
					
					while($r = sql_fetch_array($result)){
				?>
				<tr>
					<td><?php echo $i;?></td>
					<td><a href=""><?php echo $r['division']?></a></td>
					<td><a href=""><?php echo $r['series']?></a></td>
					<td><a href=""><?php echo $r['series_sub']?></a></td>
					<td><a href=""><?php echo $r['area_1']?>-<?php echo $r['area_2']?></a></td>
					<td><a href=""><?php echo $r['club']?></a></td>
					<td><?php echo $r['team_1_name']?></td>
					<td><!-- <a href="">6등급</a> --></td>
					<td><!-- 34세 --></td>
					<td>
						<!-- <ul>
							<li>상태 로그 영역</li>
							<li>2017-04-23:11:30 <font class="color5">입금</font></li>
							<li>2017-04-23:11:30 <font class="color5">취소</font></li>
							
						</ul> -->
					</td>
								        
					<td>
						 <!-- <div class="dropdown">
						  <button onclick="statusChange()" class="dropbtn">현재상태출력</button>
						  <div id="statusChange" class="dropdown-content">
						    <a href="#">접수중</a>
						    <a href="#">입금대기</a>
						    <a href="#">입금완료</a>
						    <a href="#">취소</a>
						  </div>
						</div> -->
					</td>
				</tr>
				<?php		
				$i++;
					}
				?>
			</tbody>
		</table>
	</div>
	<!-- //접수현황 DB-->
	
	<div class="btn_area">
		<!-- 수정할때 출력-->
		<a class="btn btn01 fw-600" onclick="">보 기</a>
	</div>

	

</div>




<?php
include_once(G5_SADM_PATH.'/tail.php');
?>