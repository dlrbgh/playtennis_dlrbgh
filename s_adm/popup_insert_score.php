<?php
$menu_cate2 ='2';
$menu_cate3 ='2';
include_once('./_common.php');
include_once('./head.sub.php');

$game_code = $_REQUEST['game_code']

?>
<?php

$sql = "select a.*,b.division,b.series,b.series_sub,b.num from game_score_data as a inner join group_data as b
		where a.code = '$game_code' and a.group_code = b.code group by a.game_increase order by a.game_increase asc";
$game_result = sql_query($sql);
$game_data = sql_fetch_array($game_result);

// print $sql;

if($game_data['division'] == "단체전"){
	$sql_team1 = "select * from team_event_data where
	match_code = '$game_data[match_code]' and team_code = '$game_data[team_1_event_code]'";
	$team1_result = sql_query($sql_team1);
	$team1 = sql_fetch_array($team1_result);

	$sql_team2 = "select * from team_event_data where
	match_code = '$game_data[match_code]' and team_code = '$game_data[team_2_event_code]'";
	$team2_result = sql_query($sql_team2);
	$team2 = sql_fetch_array($team2_result);

}else{
	$sql_team1 = "select * from team_data where match_code = '$game_data[match_code]' and team_code = '$game_data[team_1_code]'";
	$team1_result = sql_query($sql_team1);
	$team1 = sql_fetch_array($team1_result);

	$sql_team2 = "select * from team_data where match_code = '$game_data[match_code]' and team_code = '$game_data[team_2_code]'";
	$team2_result = sql_query($sql_team2);
	$team2 = sql_fetch_array($team2_result);
}
$modify = 0;
if($game_data['team_1_score'] != "0" && $game_data['team_2_score'] != "0" ){
	$modify = 1;
}

$action_url = "insert_game_score.php";
if($game_data['series_sub'] == "신인부"){
	$action_url = "insert_game_score_64.php";
}

$action_url = "insert_game_score_dlrbgh.php";

?>
<script src="<?php echo G5_JS_URL ?>/jquery-1.8.3.min.js"></script>
<script src="<?php echo G5_JS_URL ?>/jquery.menu.js?ver=<?php echo G5_JS_VER; ?>"></script>
<script src="<?php echo G5_JS_URL ?>/common.js?ver=<?php echo G5_JS_VER; ?>"></script>
<script src="<?php echo G5_JS_URL ?>/wrest.js?ver=<?php echo G5_JS_VER; ?>"></script>

<style>
.pop_container{padding:15px}
table {border-collapse: collapse;border-spacing: 0;}
.tbl_style03 table {margin:0 0 10px;width:100%;font-size:2em;min-width:400px}
.tbl_style03 table caption {padding:0;font-size:0;line-height:0;overflow:hidden}
.tbl_style03 table thead th {padding:7px 0;border-top:2px solid #000000;border-bottom:1px solid #000000;background:#fff;color:#777;font-size:0.8em;text-align:center;letter-spacing:-0.1em}
.tbl_style03 table thead a {color:#383838}
.tbl_style03 table thead th input {vertical-align:top} /* middle 로 하면 게시판 읽기에서 목록 사용시 체크박스 라인 깨짐 */
.tbl_style03 table tfoot th, .tbl_head01 tfoot td {padding:10px 0;border-top:1px solid #c1d1d5;border-bottom:1px solid #c1d1d5;background:#d7e0e2;text-align:center}
.tbl_style03 table tbody th {padding:13px 0;border-top:1px solid #e9e9e9;border-bottom:1px solid #e9e9e9;font-size:0.8em}
.tbl_style03 table td {text-align: center;padding:5px 3px;border-top:1px solid #e9e9e9;border-bottom:1px solid #e9e9e9;line-height:1.5em;word-break:break-all;font-size:0.8em;}
.tbl_style03 table td.match_point {color:#aaa;font-size:3em;}
.tbl_style03 table td.match_point input{width:80px;text-align:center;font-size:1.2em;border:1px solid #aaa;}
.tbl_style03 table td.match_point input:focus{background-color:#f2f5f9;border-color:#000}
.tbl_style03 table td.match_point span:first-child{padding-right:5px}
.tbl_style03 table td.match_point span:last-child{padding-left:5px}
.tbl_style03 table td.match_point .playing{font-size:16px;}


.btn_group a.score_btn{padding:0 5px;border:1px solid #ccc;display:block;cursor:pointer;text-decoration:none;margin-bottom:3px;color:#000;font-size:15px}
.btn_group a.score_btn:hover{background-color:#ccc;border-color:#bbb}
.btn_group a.score_btn:active{background-color:#14264a;color:#fff}

.hd_tit{margin-bottom:5px;margin-top:15px;font-weight:700;font-size:22px}
.btn_area input{width:100%;padding:5px;box-sizing:border-box;}
.btn_area .btn_submit{padding:15px;width:100%;display:block;margin-top:5px;background-color:#fafafa;color:#292929;border:1px solid #ccc;cursor:pointer}
.btn_area .btn_submit:hover{background-color:#14264a;color:#fff;font-weight:700}

.history_log_area {font-size:13px;margin-top:20px}
.history_log_area .tit{font-weight:700;margin-bottom:5px;font-size:22px}
.history_log_area ul{margin:0;padding:0;list-style-type:none;border:1px solid #ccc;padding:5px }
.history_log_area ul li{margin-bottom:3px}
</style>

<form name="frmReview" id="frmReview" action="<?php echo $action_url;?>" enctype="multipart/form-data" method="post" autocomplete="off" >

<input type="hidden" name="wr_id" value="<?=$game_data['wr_id'];?>">
<input type="hidden" name="division" value="<?=$game_data['division'];?>">
<input type="hidden" name="series" value="<?=$game_data['series'];?>">
<input type="hidden" name="series_sub" value="<?=$game_data['series_sub'];?>">
<input type="hidden" name="match_code" value="<?=$game_data['match_code'];?>">
<input type="hidden" name="tournament" value="<?=$game_data['tournament'];?>">
<input type="hidden" name="team_1_score_before" value="<?=$game_data['team_1_score'];?>">
<input type="hidden" name="team_2_score_before" value="<?=$game_data['team_2_score'];?>">
<input type="hidden" name="tournament_count" value="<?=$game_data['tournament_count'];?>">
<input type="hidden" name="modify" value="<?=$modify;?>">

<input type="hidden" name="team_1_dis" id="team_1_dis" value="">
<input type="hidden" name="team_2_dis" id="team_2_dis"  value="">





<div class="team_dropdown pop_container">
	<div class="hd_tit">
		<?php
			$g_sql = "select * from group_data where code = '$game_data[group_code]'";
			$g_result = sql_query($g_sql);
			$g = sql_fetch_array($g_result);
		?>
		<?php
			if($game_data['tournament'] ==  "L"){
		?>
				<?=$game_data['division'];?> <?=$game_data['series'];?><?=$game_data['series_sub'];?> <?=$g['num']+1;?>조 <?=$game_data['tournament_num'];?>경기

		<?php
			}
		?>
		<?php
			if($game_data['tournament'] ==  "T"){
		?>
				<?=$game_data['division'];?> <?=$game_data['series'];?><?=$game_data['series_sub'];?> <?=$game_data['tournament_count']*2?>강 <?=$game_data['tournament_num']+1;?>경기

		<?php
			}
		?>
		<?php
			if($game_data['tournament'] ==  "C"){
		?>
				<?=$game_data['division'];?> <?=$game_data['series'];?><?=$game_data['series_sub'];?> 결승

		<?php
			}
		?>
	</div>
<div class="tbl_style03 tbl_striped">
	<table>
		<thead>
			<tr>
				<th>클럽</th>
				<th>이름</th>
				<th>점수</th>
				<th>이름</th>
				<th>클럽</th>
			</tr>
		</thead>
		<tbody class="text-center">
			<tr>
				<td>
					<?php if($game_data['team_1_dis'] == "1"){ echo "<font color='red'>기권<br></font>"; }else if($game_data['team_1_dis'] == "2"){ echo "<font color='red'>실격<br></font>"; } ?>
					<?=$team1['club'];?><br>
					<?=$team1['club'];?>
				</td>
				<td>
					<?=$team1['team_1_name'];?><br>
					<?=$team1['team_2_name'];?>
				</td>
				<td class="match_point">
					<span><input type="text" maxlength="1" id="score_team_1_<?=$game_data['wr_id'];?>" name="team_1_score" value="<?php echo $game_data['team_1_score'];?>" ></span>:<span><input type="text" maxlength="1" id="score_team_1_<?=$game_data['wr_id'];?>" name="team_2_score" value="<?php echo $game_data['team_2_score'];?>"></span></td>
				<td>
					<?=$team2['team_1_name'];?><br>
					<?=$team2['team_2_name'];?>
				</td>
				<td>
					<?php if($game_data['team_2_dis'] == "1"){ echo "<font color='red'>기권<br></font>"; }else if($game_data['team_2_dis'] == "2"){ echo "<font color='red'>실격<br></font>"; } ?>
					<?=$team2['club'];?><br>
					<?=$team2['club'];?>
				</td>
			</tr>
			<tr>
				<td>

					<div class="btn_group">
					<?php
						if($game_data['division'] == "단체전"){
					?>
						<a class="score_btn" href="popup_team_event_set.php?game_code=<?php echo $game_code;?>&team=1">경기배정</a>
					<?php
						}
					?>
					<?php
						if($game_data['division'] == "개인전"){
					?>
						<a class="score_btn" href="popup_team_event_set.php?game_code=<?php echo $game_code;?>&team=1">경기배정</a>
					<?php
						}
					?>
					<a class="score_btn" onclick="set_dis('1','1')" >기권</a>
					<a class="score_btn" onclick="set_dis('2','1')" >실격</a>
					<a class="score_btn" onclick="set_dis('','1')" >기권취소</a>
					<a class="score_btn" onclick="set_dis('','1')" >실격취소</a>
					</div>
				</td>
				<td>
				</td>
				<td>
				</td>
				<td>
					<td>
						<div class="btn_group">
					<?php
						if($game_data['division'] == "단체전"){
					?>
						<a class="score_btn" href="popup_team_event_set.php?game_code=<?php echo $game_code;?>&team=2">경기배정</a>
					<?php
						}
					?>
					<?php
						if($game_data['division'] == "개인전"){
					?>
						<a class="score_btn" href="popup_team_event_set.php?game_code=<?php echo $game_code;?>&team=1">경기배정</a>
					<?php
						}
					?>
					<a class="score_btn" onclick="set_dis('1','2')" >기권</a>
					<a class="score_btn" onclick="set_dis('2','2')" >실격</a>
					<a class="score_btn" onclick="set_dis('','2')" >기권취소</a>
					<a class="score_btn" onclick="set_dis('','2')" >실격취소</a>
					</div>
				</td>
				</td>
			</tr>
		</tbody>
	</table>
</div>

<!--
	<div class="text-center pull-left timetable_teamA_pts">
    	<label class="remove-padding col-md-12_ control-label" for="use-court" id="data_team_1_<?=$game_data['wr_id'];?>">
    		<span class="font-s08"><?=$team1['club'];?></span>
    		<br/>
    		<span class="font-s08"><?=$team1['team_1_name'];?>/<?=$team1['team_2_name'];?></span>
    	</label>
    	<div class="">
            <input class="js-masked-taxid form-control text-center" type="text" id="score_team_1_<?=$game_data['wr_id'];?>" name="team_1_score" value="<?php if($game_data['team_1_score'] != 0 ) echo $game_data['team_1_score'];?>" placeholder="점수">
        </div>
    </div>
	<div class=" text-center text-center pull-right timetable_teamB_pts">
    	<label class="remove-padding col-md-12 control-label" for="use-court" id="data_team_2_<?=$game_data['wr_id'];?>">
    		<span class="font-s08"><?=$team2['club'];?></span>
    		<br/>
    		<span class="font-s08"><?=$team2['team_1_name'];?>/<?=$team2['team_2_name'];?></span>
    	</label>
    	<div class="">
            <input class="js-masked-taxid form-control text-center" type="text" id="score_team_1_<?=$game_data['wr_id'];?>" name="team_2_score" value="<?php if($game_data['team_2_score'] != 0 ) echo $game_data['team_2_score'];?>" placeholder="점수">
        </div>
    </div>
</div>
-->

	<div class="btn_area">
		<div id="score_button_<?=$game_data['wr_id'];?>">
		<?php
			if($game_data['team_1_code'] != "" && $game_data['team_2_code'] != ""){
		?>
		<?php
			if($game_data['team_1_score'] == "0" && $game_data['team_2_score'] == "0" ){
		?>
			<input type="hidden" name="modify" value="0" />
			<input type="hidden" name="tournament" value="<?php  echo $game_data['tournament'];?>" />
			<input type="hidden" name="tournament_count" value="<?php echo $game_data['tournament_count'];?>" />
			<button class="btn_submit" type="submit">적용</button>
		<?php
			}else{
		?>
			<input type="hidden" name="modify" value="1" />
			<input type="hidden" name="team_1_score_before" value="<?php if($game_data['team_1_score'] != 0 ) echo $game_data['team_1_score'];?>" />
			<input type="hidden" name="team_2_score_before" value="<?php if($game_data['team_2_score'] != 0 ) echo $game_data['team_2_score'];?>" />
			<input type="hidden" name="tournament" value="<?php  echo $game_data['tournament'];?>" />
			<input type="hidden" name="tournament_count" value="<?php echo $game_data['tournament_count'];?>" />
			<input type="text" name="wr_text" placeholder="수정사유를 입력해 주세요(필수)">
			<button class="btn_submit" type="submit">수정 적용</button>
		<?php
			}
			}
		?>

		</div>
	</div>
	<div class="history_log_area">
		<div class="tit">수정 및 업데이트 내역</div>
		<ul>
			<?php
				$sql = "select * from game_score_data_history where match_code = '$game_data[match_code]' and game_code = '$game_data[code]'";
				$result = sql_query($sql);
				while($r = sql_fetch_array($result)){
					$mem =  get_member($r['mb_id']);
			?>
				<li>업데이트 시간 출력 <?php echo $r['wr_text']?>(<?php echo $mem['mb_name']?>)</li>
			<?php
				}
			?>

		</ul>
	</div>
</form>

<script>
	function set_dis(dis,team){
		if(team == "1"){
			$('#team_1_dis').val(dis);
		}
		if(team == "2"){
			$('#team_2_dis').val(dis);
		}
		$( "#frmReview" ).submit();
	}
</script>
