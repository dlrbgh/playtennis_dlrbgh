<?php
$menu_cate2 ='2';
$menu_cate3 ='';
$menu_cate4 ='5';
include_once('./_common.php');
include_once(G5_SADM_PATH.'/head.php');

$match = sql_fetch("select * from match_data where code = '$code'");

?>
  <link rel="stylesheet" href="../dist/remodal.css">
  <link rel="stylesheet" href="../dist/remodal-default-theme.css">
	
<!-- sub_hd_area -->
<?php include_once(G5_SADM_PATH.'/inc/sub_hd.inc');?>


<div class="sub_hd">
	<div class="l_area">
        <!-- <ul>
        	<li><a href="" class="btn_default ">전체</a></li>
        	<li><a href="" class="btn_default active">남자개인</a></li>
        	<li><a href="" class="btn_default ">여자개인</a></li>
        	<li><a href="" class="btn_default ">혼복개인</a></li>
        	<li><a href="" class="btn_default ">단체전</a></li>
        </ul> -->
        <ul>
        	<!-- <li><a href="" class="btn_default ">전체</a></li>
        	<li><a href="" class="btn_default active">금배부</a></li>
        	<li><a href="" class="btn_default ">은배부</a></li>
        	<li><a href="" class="btn_default ">동배부</a></li> -->
        	<?php 
				$sql = "select * from series_data where match_code = '$code' group by division";
				$result = sql_query($sql);
				while($series = sql_fetch_array($result)){
					
			?>
			<li class="<?php if(strcmp(trim($division), trim($series['division'])) == "0"){echo "active";} ?>">
                <a class="btn_default " href="champ_endrank_view.php?division=<?=$series['division'];?>&code=<?php echo $code;?>"><?=$series['division']?></a>
            </li>
			<?php			
				}
			?>
        </ul>
        
    </div>
</div>
<!--//sub_hd_area -->

<!-- !PAGE CONTENT! -->
<div>
	<!-- 그림출력시  -->
	<div class="sub_hd">
        <div class="tit">시상표 그림 위치</div>
    </div>
    <!-- //그림출력시  -->
	<div class="champ_rank_result tbl_striped">
	
		<table>
			<thead>
				<tr>
					<th>등급</th>
					<th>1위</th>
					<th>2위</th>
					<!-- <th>3위</th> -->
					<th>상품권</th>
				</tr>
			</thead>
			<tbody class="text-center">
				<?php
            		$sql = "select * from series_data where match_code = '$code' and division = '$division' order by wr_id";
					
					$result = sql_query($sql);
					while($r = sql_fetch_array($result)){
						$group_sql = "select count(wr_id) as cnt from group_data where match_code = '$code' and division = '$division' and series = '$r[series]' and series_sub = '$r[series_sub]'";
						$group_result = sql_query($group_sql);
						$group = sql_fetch_array($group_result);
						
						if($group['cnt'] != 1){
							if($division == "단체전"){
								$game_sql = "select * from game_score_data where match_code = '$code' and division = '$division' and series = '$r[series]' and series_sub = '$r[series_sub]' and tournament = 'c'";
								$game_result = sql_query($game_sql);
								
								$chk1 = 0;
								$chk2 = 0;
								
								while($game = sql_fetch_array($game_result)){
									if($game['team_1_score'] > $game['team_2_score']){
										$chk1++;
										$team_1_code = $game['team_1_code'];
									}else{
										$chk2++;
										$team_2_code = $game['team_2_code'];
									}
								}
								
								$team_field = "team_data";
										
								if( $division  == "단체전" ){
									$team_field = "team_event_data";	
								}
															
								$sql_team1 = "select * from $team_field where team_code = '$team_1_code'";
								$team1_result = sql_query($sql_team1);
								
								$sql_team2 = "select * from $team_field where team_code = '$team_2_code'";
								$team2_result = sql_query($sql_team2);
								
								if($chk1 > $chk2){
									$champion = sql_fetch_array($team1_result);
									$champion_sub = sql_fetch_array($team2_result);
								}else{
									$champion = sql_fetch_array($team2_result);
									$champion_sub = sql_fetch_array($team1_result);
								}
							}
							else{
								$game_sql = "select * from game_score_data where match_code = '$code' and division = '$division' and series = '$r[series]' and series_sub = '$r[series_sub]' and tournament = 'c'";
								$game_result = sql_query($game_sql);
								$game = sql_fetch_array($game_result);
								
								$team_field = "team_data";
										
								if($r['division'] == "단체전"){
									$team_field = "team_event_data";	
								}
															
								$sql_team1 = "select * from $team_field where team_code = '$game[team_1_code]'";
								$team1_result = sql_query($sql_team1);
								
								$sql_team2 = "select * from $team_field where team_code = '$game[team_2_code]'";
								$team2_result = sql_query($sql_team2);
								
								if($game['team_1_score'] > $game['team_2_score']){
									$champion = sql_fetch_array($team1_result);
									$champion_sub = sql_fetch_array($team2_result);
								}else{
									$champion = sql_fetch_array($team2_result);
									$champion_sub = sql_fetch_array($team1_result);
								}
							}
							
							
							$champion_code = $champion['team_code'];
							$game_sql = "select * from game_score_data where match_code = '$code' and division = '$division' and series = '$r[series]' and series_sub = '$r[series_sub]' and (team_1_code = '$champion_code' or team_2_code = '$champion_code') and tournament = 'T' and tournament_count = '2'";
							$game_result = sql_query($game_sql);
							$third_array = array();
							$games = sql_fetch_array($game_result);
							if($games['team_1_score'] > $games['team_2_score'] ){
								$sql_team2 = "select * from $team_field where team_code = '$games[team_2_code]'";
								$team2_result = sql_query($sql_team2);												
								$third_champion_sub = sql_fetch_array($team2_result);
							}else{
								$sql_team2 = "select * from $team_field where team_code = '$games[team_1_code]'";
								$team2_result = sql_query($sql_team2);
								$third_champion_sub = sql_fetch_array($team2_result);
							}
							
						?>
						 <tr>
                        	<td><?=$r["series"]?> <?=$r["series_sub"]?></td>
                        	<?php
                        		if($game['team_1_score'] != "0"  && $game['team_2_score'] != "0" )
								{
                        	?>
                        	<td><?=$champion['club'];?><br/>
                        		<?=$champion['team_1_name']."/".$champion['team_2_name'];?>
                        	</td>
                        	<td><?=$champion_sub['club'];?><br/>
                        		<?=$champion_sub['team_1_name']."/".$champion_sub['team_2_name'];?>
                        	</td>
            					<?php if($r['print'] == 0){?>
                            	<td><a class="btn btn-warning btn-block" href="print_giftcard_row.php?division=<?php echo $division;?>&team_code_1=<?=$champion['team_code'];?>&team_code_2=<?=$champion_sub['team_code'];?>&team_code_3=<?=$third_champion_sub['team_code'];?>&match_code=<?=$code;?>&rank=2" target="blank">출력</a></td>
                            	<?php }else{ ?>
                            	<td><a class="btn btn-danger btn-block" href="print_giftcard_row.php?division=<?php echo $division;?>&team_code_1=<?=$champion['team_code'];?>&team_code_2=<?=$champion_sub['team_code'];?>&team_code_3=<?=$third_champion_sub['team_code'];?>&match_code=<?=$code;?>&rank=2" target="blank">출력됨</a></td>
                            	<?php }?>

                        	<!-- <td>
                        	<?php 
                        		echo $third_champion_sub['club']."<br>";
								echo $third_champion_sub['team_1_name']."/".$third_champion_sub['team_2_name'];
								echo "<br>";
                        	?>
                        	</td> -->
                        </tr>
						<?php
								}else{
									echo "<td></td><td></td><td></td>";
								}

							}							
						}
					?>
			</tbody>
		</table>
		
	</div>
	
	

	<div class="btn_area">
		<a class="btn btn02 fw-600" href="<?php echo G5_SADM_URL?>/championship_add_step3.php">수 정</a>
	</div>

</div>



<?php
include_once(G5_SADM_PATH.'/tail.php');
?>