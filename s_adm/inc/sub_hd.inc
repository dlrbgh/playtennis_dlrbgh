<?php
	// $match_sql = "select * from match_gym_data where match_id='$code' order by application_period";
	// $match_result = sql_query($match_sql);
	// $match = sql_fetch_array($match_result);
	// $url = "&gym_id=$match[gym_id]&application_period=$match[application_period]";
?>
<?php if($menu_cate2 == "2") { ?>
<!-- sub hd_section-->
	<div class="tit_area">
		<div class="tit_subject">대회관리</div>
		<div class="tit"><?php echo $match['wr_name'] ?></div>
	</div>
<!--//sub_hd_area -->

<div class="tab_hd tab_style01">
    <a <?php if($menu_cate4 == "1"){?> class="active"<?php }?> href="champ_view.php?code=<?php echo $code;?>">대회안내</a>
    <a <?php if($menu_cate4 == "2"){?> class="active"<?php }?> href="champ_apply_view.php?code=<?php echo $code;?>">접수현황</a>
    <a <?php if($menu_cate4 == "3"){?> class="active"<?php }?> href="champ_matchtbl_view.php?code=<?php echo $code;?>">대진표</a>
    <a <?php if($menu_cate4 == "4"){?> class="active"<?php }?> href="champ_operation_view.php?code=<?php echo $code.$url;?>">대회운영</a>
    <a <?php if($menu_cate4 == "5"){?> class="active"<?php }?> href="champ_endrank_view.php?code=<?php echo $code;?>">경기결과</a>
</div>
	<?php if($menu_cate4 == "1") { ?>
	<div class="sub_hd">
		<div class="l_area">
	        <ul>
	        	<li><a class="btn_default <?php if($menu_cate5 == "1") echo "active"; ?>" href="champ_view.php?code=<?php echo $code;?>">기본정보</a></li>
	        	<li><a class="btn_default <?php if($menu_cate5 == "2") echo "active"; ?>" href="champ_map_view.php?code=<?php echo $code;?>">기본정보</a></li>
            <li><a class="btn_default <?php if($menu_cate5 == "3") echo "active"; ?>" onclick="window.open('./popup/charger_assign.php?code=<?php echo $code;?>')">경기장담당자배정</a></li>
	        </ul>
	    </div>
	</div>
	<?php }elseif($menu_cate4 == "2") { ?>
	<div class="sub_hd">
		<div class="l_area">
	        <ul>
	        	<li><a class="btn_default <?php if($menu_cate5 == "1") echo "active"; ?>" href="champ_apply_view.php?code=<?php echo $code;?>">종목별</a></li>
	        	<li><a class="btn_default <?php if($menu_cate5 == "2") echo "active"; ?>" href="champ_applyarea_view.php?code=<?php echo $code;?>">지역별</a></li>
	        </ul>
	    </div>
	</div>

	<?php }elseif($menu_cate4 == "3") { ?>
	<div class="sub_hd">
		<div class="l_area">
	        <!-- <ul>
	        	<li><a class="btn_default <?php if($menu_cate5 == "1") echo "active"; ?>" href="champ_matchtbl_view.php?code=<?php echo $code;?>">종목별</a></li>
	        	<li><a class="btn_default <?php if($menu_cate5 == "2") echo "active"; ?>" href="champ_matchtbl_court_view.php?code=<?php echo $code;?>">코트별</a></li>
	        </ul> -->

	        <?php if($menu_cate5 == "1") { ?>
	        <ul>
        	<?php
      $sql_s = "select * from group_data where match_code = '$code' group by division, series, series_sub";
				$result = sql_query($sql_s);
        ?>
        <li>
          <a href="./champ_matchtbl_view.php?code=<?php echo $code;?>"
            class="btn_default <?php if($series_sub == ''){print "active";}?>">전체보기</a></li>
        <?php
				while($r = sql_fetch_array($result)){
			?>
	        	<li>
              <a href="./champ_matchtbl_view.php?code=<?php echo $code;?>&series_sub=<?php echo $r['series_sub'];?>"
                class="btn_default <?php if($r['series_sub'] == $series_sub){print "active";}?>">
                <?php echo substr($r['division'],0,6).$r['series'].$r['series_sub'];?></a></li>
	        	<?php } ?>
	        </ul>
	        <?php }elseif($menu_cate5 == "2") { ?>
	       	<ul>
	        	<li><a href="" class="btn_default active">장소이름 출력1</a></li>
	        	<li><a href="" class="btn_default ">장소이름 출력2</a></li>
	        </ul>
	        <?php } ?>
	    </div>
	</div>

	<?php }elseif($menu_cate4 == "4") { ?>
	<div class="sub_hd">
		<div class="l_area">
	        <ul>
	        	<li><a class="btn_default <?php if($menu_cate5 == "1") echo "active"; ?>" href="champ_operation_view.php">코트별</a></li>
	        	<!-- <li><a class="btn_default <?php if($menu_cate5 == "2") echo "active"; ?>" href="champ_status_rating_list.php">조별</a></li> -->
	        	<li><a class="btn_default <?php if($menu_cate5 == "2") echo "active"; ?>" href="currentstatus_rating.php">조별</a></li>
	        	<li><a class="btn_default <?php if($menu_cate5 == "3") echo "active"; ?>" href="champ_operation_history.php">히스토리</a></li>
	        </ul>

	        <?php if($menu_cate5 == "1") { ?>
	        <!-- 코트별 > 체육관 리스트-->
	        코트별 선택시 출력
	        <ul>
        	<?php
            	$sql_s = "select * from match_gym_data inner join gym_data where match_gym_data.match_id='$code' and gym_data.wr_id = match_gym_data.gym_id";
				$result = sql_query($sql_s);
				while($r = sql_fetch_array($result)){
			?>
	        	<li><a href="./champ_operation_view.php?code=<?php echo $code;?>&gym_id=<?php echo $r['gym_id'];?>&application_period=<?php echo $r['application_period'];?>" class="btn_default <?php if($r['gym_id'] == $gym_id){ echo 'active'; }?> "><?php echo $r['gym_name'];?></a></li>
	        	<?php } ?>
	        </ul>
	        <!-- //코트별 > 체육관 리스트-->
	        <?php }elseif($menu_cate5 == "2") { ?>
        	<!-- 조별 선택 출력 리스트-->
	        조별 선택시 출력
	        <ul>
	        	<li><a href="" class="btn_default active">남자개인</a></li>
	        	<li><a href="" class="btn_default ">여자개인</a></li>
	        	<li><a href="" class="btn_default ">혼복개인</a></li>
	        	<li><a href="" class="btn_default ">단체전</a></li>
	        </ul>

	        <ul>
	        	<li><a href="" class="btn_default active">개나리</a></li>
	        	<li><a href="" class="btn_default ">두달래</a></li>
	        	<li><a href="" class="btn_default ">꺄르르</a></li>
	        </ul>
	        <!-- //조별 선택 출력 리스트-->
        	<?php }elseif($menu_cate5 == "3") { ?>
	         <!-- 히스토리 선택 출력 리스트-->
	        히스토리 선택시 출력
	        <ul>
	        	<li><a href="" class="btn_default active">장소1</a></li>
	        	<li><a href="" class="btn_default ">장소2</a></li>
	        </ul>
	        <ul>
	        	<li><a href="" class="btn_default active">개나리</a></li>
	        	<li><a href="" class="btn_default ">달나래</a></li>
	        </ul>
	        <!-- //히스토리 선택 출력 리스트-->
	        <?php } ?>

	    </div>
	</div>
	<?php }elseif($menu_cate4 == "5") { ?>

	<?php }elseif($menu_cate4 == "6") { ?>
	<?php }?>
<?php } ?>
