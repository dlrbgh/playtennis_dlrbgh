<?php

include_once('./_common.php');

?>


<!-- 대회관리 -->


<!-- table collum fixed -->
 <div id="tblcol" class="timetbl-collum_h">
    <table id="" class="table table-time table-fixed remove-margin-b">
		<thead>
			<tr>
			<th class="tbl_tit">순서</th>
			<?php for ($c = 0; $c < $court; $c++) { ?>
				<th class="text-center" style=""><?php echo $c+1; ?>코트</th>
			<?php } ?>
			</tr>
		</thead>
	</table>
 	<div class="number">
   	 순서
   </div>
 </div>
 <!-- end table collum fixed -->  
 
 <!-- table row fixed-->
 <div id="tblrow" class='timetbl-row_v'>
    <table class="table table-time table-fixed">
    	<thead>
			<tr>
				<th class="tbl_tit">순서</th>
			</tr>
		</thead>
	    <tbody>
				<!--CLASS 적용
					term_section 짝수 텀에만 적용 -->
				<?php
				while(strtotime($day2) < strtotime($end_time)){
					if(strtotime($day2) < strtotime($breaktime_end_day) && strtotime($day2) >= strtotime($breaktime_start_day) ){
						$day = date("H:i", strtotime($time."+".$i." minutes")); 
						$day1 = date("H:i", strtotime($time."+".$i." minutes")); 
						$day2 = date("H:i", strtotime($day."+"."$game_time minutes")); 	
						echo "<tr>";
						?>
							<td class="text-center playtime">
								<span class="badge badge-timetable push-15"></span>
								<div>
								<?=$day;?>
									<div><i class="glyphicon glyphicon-time push-5-t push-5 text-gray"></i></div>
								<?=$day2;?>
								</div>
							</td>
							<?php
						echo "</tr>";
						$i=$i+$game_time;
					}else{
						
						$day = date("H:i", strtotime($time."+".$i." minutes")); 
						$day1 = date("H:i", strtotime($time."+".$i." minutes")); 
						$day2 = date("H:i", strtotime($day."+"."$game_time minutes")); 
						//echo $day."<br>"; 
						$i=$i+$game_time;
						echo "<tr>";
							?>
							<td class="text-center playtime">
								<span class="badge badge-timetable push-15"><?=$cnt;?></span>
								<div>
								<?=$day;?>
									<div><i class="glyphicon glyphicon-time push-5-t push-5 text-gray"></i></div>
								<?=$day2;?>
								</div>
							</td>
							<?php
						$cnt++;
						echo "</tr>\n";
						}
					}
				?>
		</tbody>
	</table>
  </div>
  <!-- end table row fixed-->
<!-- table thead scroll fixed -->
<script>
// 현재 스크롤바의 위치를 저장하는 변수 (px)
var currentScrollTop = 0;
     
// 비동기식 jQuery이므로 window load 후 jQuery를 실행해야 함
window.onload = function() {
    // 새로고침 했을 경우를 대비한 메소드 실행
    scrollController();
     
    // 스크롤을 하는 경우에만 실행됨
    $(window).on('scroll', function() {
        scrollController();
    });
}
// 메인 메뉴의 위치를 제어하는 함수
function scrollController() {
    currentScrollTop = $(window).scrollTop();
    if (currentScrollTop < 94) {
    	$('#tblcol .number').removeClass('on');
        $('#tblcol').css('top', 94-(currentScrollTop));
        if ($('#tblcol').hasClass('fixed')) {
            $('#tblcol').removeClass('fixed');
            
            //$('#tblcol .number').addClass('fixed');
            $('#tblcol .number').css('top', 0);
            $('#tblcol .number').removeClass('on');
            
        }
    } else {
        if (!$('#tblcol').hasClass('fixed')) {
            $('#tblcol').css('top', 0);
            $('#tblcol').addClass('fixed');
            
            //$('#tblcol .number').removeClass('fixed');
            $('#tblcol .number').css('top', 0);
            $('#tblcol .number').addClass('on');
        }
    }
}
</script> 
<!-- end table thead scroll fixed -->
<!-- table row / col 스크롤  -->
<script>
/* @Note: not sure e.pageX will work in IE8 */
(function(window){
  
  /* A full compatability script from MDN: */
  var supportPageOffset = window.pageXOffset !== undefined;
  var isCSS1Compat = ((document.compatMode || "") === "CSS1Compat");
 
  /* Set up some variables  */
  var tblcol = document.getElementById("tblcol");
  var tblrow = document.getElementById("tblrow"); 
  /* Add an event to the window.onscroll event */
  window.addEventListener("scroll", function(e) {  
    
    /* A full compatability script from MDN for gathering the x and y values of scroll: */
    var x = supportPageOffset ? window.pageXOffset : isCSS1Compat ? document.documentElement.scrollLeft : document.body.scrollLeft;
	var y = supportPageOffset ? window.pageYOffset : isCSS1Compat ? document.documentElement.scrollTop : document.body.scrollTop;
 
    tblcol.style.left = -x + 0 + "px";
    tblrow.style.top = -y + 94 + "px";
  });
  
})(window);
</script>
<!-- end table row / col 스크롤  -->



<?php
$game_time = $match['match_time'];
$court = $match['use_court'];

$time = $match['championship_start_day'] ; 
$day = $match['championship_start_day'] ;
$end_time = $match['championship_end_day'];

$breaktime_start_day = $match['breaktime_start_day'];
$breaktime_end_day = $match['breaktime_end_day'];

//$end_time = "12:00";

$c=0;
$i=0;
//$sql = "select * from game_score_data where match_code='1' order by Full_League desc,term desc";

//$sql = "select * from game_score_data where match_code='1' and division='혼복' order by term desc,wr_id asc";
//경기 정보를 호출한다.
$sql = "select a.*,b.division,b.series,b.series_sub,b.num from game_score_data as a inner join group_data as b 
		where a.match_code='$code' and a.game_date = '$application_period' and a.gym_code = '$gym_id' and a.game_assign='1' and a.group_code = b.code group by a.game_increase order by a.game_increase asc";

$gourp_result = sql_query($sql);
//좌축 순서를 카운트한다.
$cnt = 1;
//미배정 경기 일경우 건너뛴다. 0이면 호출 1이면 스킵
$no_count = 0;
//auto_increase를 카운트한다.
$auto_increase = 1;
$day2 = "";
?>

<!-- Page Content timetable -->
<div class="content_" style="background-color:#fff;">
	<div class="block">
		<div class="block-header" style="background-color:#f5f5f5">
            <div class="row">
				<div class="col-md-12">
					<!-- 등록된 체육관 -->
					<?php 
						$match_sql = "select * from match_gym_data as a inner join gym_data as b where a.match_id='$code' and a.gym_id = b.wr_id group by gym_id order by gym_id,application_period";
						$match_result = sql_query($match_sql);
						while($match = sql_fetch_array($match_result)){
					?>
						<div class="push-10-r" style="display:inline-block">
							<div class="push-10 font-w700"><?=$match['gym_name'];?></div>
			            	<?php
								$matchs_sql = "select * from match_gym_data as a inner join gym_data as b where a.match_id='$code' and a.gym_id = b.wr_id and a.gym_id = $match[gym_id] order by gym_id,application_period";
								$matchs_result = sql_query($matchs_sql);
								while($matchs = sql_fetch_array($matchs_result)){
								?>
			            			<a style="display:inline-block;" href="currentstatus_table_full.php?code=<?=$code?>&gym_id=<?=$matchs['gym_id'];?>&application_period=<?=$matchs['application_period'];?>"><button class="btn btn-info <?php if($gym_id == $matchs['gym_id'] & $application_period == $matchs['application_period']) echo "active";?>"><?=$matchs['application_period'];?></button></a>
								<?php
								}
			            	?>
			            </div>
					<?php
						}
					
					?>
		            <!-- end 등록된 체육관 -->
            	</div>
            </div>
        </div>
		
		
		<!-- 타임테이블 -->
		<div class="block-content_">
			<div class="row">
				<div class="col-xs-12">
					<div class="table-responsive_">
						<table id="" class="table table-time">
							<thead>
								<tr>
									<th class="text-center tbl_tit">순서</th>
								<?php for ($c = 0; $c < $court; $c++) { ?>
									<th class="text-center"><?php echo $c+1; ?>코트</th>
								<?php } ?>
								</tr>
							</thead>
							<tbody>
									<?php
									while(strtotime($day2) < strtotime($end_time)){
										
										
										if(strtotime($day2) < strtotime($breaktime_end_day) && strtotime($day2) >= strtotime($breaktime_start_day) ){
											$day = date("H:i", strtotime($time."+".$i." minutes")); 
											$day1 = date("H:i", strtotime($time."+".$i." minutes")); 
											$day2 = date("H:i", strtotime($day."+"."$game_time minutes")); 	
											echo "<tr>";
											?>
												<td class="text-center playtime">
													<span class="badge badge-timetable push-15"></span>
													<div>
													<?=$day;?>
														<div><i class="glyphicon glyphicon-time push-5-t push-5 text-gray"></i></div>
													<?=$day2;?>
													</div>
												</td>
												<?php
												for($j=0;$j<$court;$j++){
												?>
													<td>
														<a class="text-black">
															<div class="text-center font-w700 free_space">
																
															</div>
														</a>
													</td>
												<?php
												}
											echo "</tr>";
											$i=$i+$game_time;
										}else{
											
											$day = date("H:i", strtotime($time."+".$i." minutes")); 
											$day1 = date("H:i", strtotime($time."+".$i." minutes")); 
											$day2 = date("H:i", strtotime($day."+"."$game_time minutes")); 
											
											//echo $day."<br>"; 
											$i=$i+$game_time;
											echo "<tr>";
											for($j=0;$j<$court+1;$j++){
												if($j == 0){
												?>
												<td class="text-center playtime">
													<span class="badge badge-timetable push-15"><?=$cnt;?></span>
													<div>
													<?=$day;?>
														<div><i class="glyphicon glyphicon-time push-5-t push-5 text-gray"></i></div>
													<?=$day2;?>
													</div>
												</td>
												
												<?php
												}
												else {
													if($no_count == 0){
														$group = sql_fetch_array($gourp_result);
													}
													if($j == $group['game_court']){
													$no_count = 0;
													$sql_team1 = "select * from team_data where match_code = '$code' and team_code = '$group[team_1_code]'";
													$team1_result = sql_query($sql_team1);
													$team1 = sql_fetch_array($team1_result);
													
													$sql_team2 = "select * from team_data where match_code = '$code' and team_code = '$group[team_2_code]'";
													$team2_result = sql_query($sql_team2);
													$team2 = sql_fetch_array($team2_result);
													
													$division = $group['division'];
													$division = trim($division);
													
													?>
													<td class="<?php if($group['tournament'] ==  "T") echo "tournament_on_t";?><?php if($group['tournament'] ==  "C") echo "tournament_on_c";?>">
														<?php if($division == "혼복"){?>
															<div class="btn-group text-center group_area team_play " role="group">
															<a class="dropdown-toggle text-black" data-toggle="dropdown" aria-expanded="false" href="">
															<div class="team_play_bg">
																<?=$group['division'];?>&nbsp;<?=$group['series'];?><?=$group['series_sub'];?> <?php if($group['tournament'] ==  "L") echo "<font color='green'>$group[tournament]</font>";?><?php if($group['tournament'] ==  "T") echo "<font color='red'>$group[tournament]</font>";?><?php if($group['tournament'] ==  "C") echo "<font color='red'>$group[tournament]</font>";?><?php if($group['tournament_count'] != 0){echo $group['tournament_count']*2;}?><br/>
															</div>
														<?php }?>
														<?php if($division == "남복"){?>
															<div class="btn-group text-center group_area male_play " role="group">
															<a class="dropdown-toggle text-black" data-toggle="dropdown" aria-expanded="false" href="">
															
															<div class="team_play_bg">
																<?=$group['division'];?>&nbsp;<?=$group['series'];?><?=$group['series_sub'];?> <?php if($group['tournament'] ==  "L") echo "<font color='green'>$group[tournament]</font>";?><?php if($group['tournament'] ==  "T") echo "<font color='red'>$group[tournament]</font>";?><?php if($group['tournament'] ==  "C") echo "<font color='red'>$group[tournament]</font>";?><?php if($group['tournament_count'] != 0){echo $group['tournament_count']*2;}?><br/>
															</div>
														<?php }?>
														<?php if($division == "여복"){?>
															<div class="btn-group text-center group_area female_play " role="group">
															<a class="dropdown-toggle text-black" data-toggle="dropdown" aria-expanded="false" href="">
															
															<div class="team_play_bg">
																<?=$group['division'];?>&nbsp;<?=$group['series'];?><?=$group['series_sub'];?> <?php if($group['tournament'] ==  "L") echo "<font color='green'>$group[tournament]</font>";?><?php if($group['tournament'] ==  "T") echo "<font color='red'>$group[tournament]</font>";?><?php if($group['tournament'] ==  "C") echo "<font color='red'>$group[tournament]</font>";?><?php if($group['tournament_count'] != 0){echo $group['tournament_count']*2;}?><br/>
															</div>
														<?php }?>
															
															<!-- 경기가 종료되었을때 class 추가 end_matchA , end_matchB -->
															<?php if($group['team_2_score'] == "0" && $group['team_1_score'] == "0" || ($group['team_2_score'] == 0 && $group['team_1_score'] == 0)){?>
																<div id="score_class_<?=$group['wr_id'];?>" class="club_area">	
															<?php }else {?>
																<?php if($group['team_1_score'] > $group['team_2_score'] ){?>
																	<div id="score_class_<?=$group['wr_id'];?>" class="club_area end_matchA">															
																<?php }else{?>
																	<div id="score_class_<?=$group['wr_id'];?>" class="club_area end_matchB">	
																<?php }?>
															<?php }?>
																<div class="teamA_area">
																	<font class="font-w700 font-s08"><?=$team1['club'];?> </font>
																	<br/><font class="font-s08"><?=$team1['team_1_name'];?>/<?=$team1['team_2_name'];?></font>
																</div>
																<div class="scoreboard_area">
																	<div class="win_score">
																		<div id="score_session_<?=$group['wr_id'];?>">
																			<div class="a_score"><?=$group['team_1_score'];?></div>
																			<div class="b_score"><?=$group['team_2_score'];?></div>
																		</div>
																	</div>
																</div>
																<div class="teamB_area">
																	<font class="font-w700 font-s08"><?=$team2['club'];?></font>
																	<br/><font class="font-s08"><?=$team2['team_1_name'];?>/<?=$team2['team_2_name'];?></font>
																</div>
																<div class="font-s05">
																	<?php 
																		if($group['tournament'] != "L" ){
														                	if($group['scorecard_print'] == 0 ){
														                	?>
																			<!-- print 안됨 -->																	

														                	<?php
														                		}else{
														                	?>
														                	<!-- print 됨 -->
														                	<i class="fa fa-circle text-success print"></i>	
														                	<?php
														                		}
																			}
													                	?>
																	<span style="">#<?=$group['game_code'];?></span>
																	<!-- 경기중일때 표시 --> 
																	<?php if($cnt == 0){?>
																		<i class="fa fa-circle text-red playing"></i>									
																	<?php }?>
																</div>
															</div>
															
															</a>
															
															<ul class="dropdown-menu">
																<form name="frmReview" id="frmReview" enctype="multipart/form-data" method="post" autocomplete="off" >
																	<input type="hidden" name="wr_id" value="<?=$group['wr_id'];?>"> 
																	<input type="hidden" name="division" value="<?=$group['division'];?>"> 
																	<input type="hidden" name="series" value="<?=$group['series'];?>"> 
																	<input type="hidden" name="series_sub" value="<?=$group['series_sub'];?>"> 
																	<input type="hidden" name="match_code" value="<?=$code;?>"> 

												                    <div class="team_dropdown">
												                    	<div class="tit">
												                    		<?php 
																				$g_sql = "select * from group_data where code = '$group[group_code]'";
																				$g_result = sql_query($g_sql);
																				$g = sql_fetch_array($g_result);	
																						
												                    		?>
												                    		<?=$group['division'];?> <?=$group['series'];?><?=$group['series_sub'];?> <?=$g['num']+1;?>그룹 <?=$group['tournament_num'];?>경기
												                    	</div>
												                    	<div class="text-center pull-left timetable_teamA_pts">
												                    		
													                    	<label class="remove-padding col-md-12_ control-label" for="use-court">
													                    		<span class="font-s08"><?=$team1['club'];?></span>
													                    		<br/>
													                    		<span class="font-s08"><?=$team1['team_1_name'];?>/<?=$team1['team_2_name'];?></span>
													                    		</label>
													                    	<div class="">
													                            <input class="js-masked-taxid form-control text-center" type="text" id="use-court" name="team_1_score" value="<?php if($group['team_1_score'] != 0 ) echo $group['team_1_score'];?>" placeholder="점수">
													                        </div>
													                    </div>
												                    	<div class=" text-center text-center pull-right timetable_teamB_pts">
													                    	<label class="remove-padding col-md-12 control-label" for="use-court">
													                    		<span class="font-s08"><?=$team2['club'];?></span>
													                    		<br/>
													                    		<span class="font-s08"><?=$team2['team_1_name'];?>/<?=$team2['team_2_name'];?></span>
													                    		</label>
													                    	<div class="">
													                            <input class="js-masked-taxid form-control text-center" type="text" id="use-court" name="team_2_score" value="<?php if($group['team_2_score'] != 0 ) echo $group['team_2_score'];?>" placeholder="점수">
													                        </div>
													                    </div>
													                </div>
													                <div class="col-md-12 text-center timetable_pts_apply">
													                	<div id="score_button_<?=$group['wr_id'];?>">
																		<?php
													                		if($group['team_1_code'] != "" && $group['team_2_code'] != ""){
													                	?>
													                	<?php		
													                		if($group['team_1_score'] == "0" && $group['team_2_score'] == "0" ){
													                	?>
													                		<input type="hidden" name="modify" value="0" />
													                		<input type="hidden" name="tournament" value="<?php  echo $group['tournament'];?>" />
													                		<input type="hidden" name="tournament_count" value="<?php echo $group['tournament_count'];?>" />
													                		<button class="btn btn-info btn-block push-5" type="submit">적용</button>
													                	<?php
													                		}else{
													                	?>
													                		<input type="hidden" name="modify" value="1" />
													                		<input type="hidden" name="team_1_score_before" value="<?php if($group['team_1_score'] != 0 ) echo $group['team_1_score'];?>" />
													                		<input type="hidden" name="team_2_score_before" value="<?php if($group['team_2_score'] != 0 ) echo $group['team_2_score'];?>" />
													                		<input type="hidden" name="tournament" value="<?php  echo $group['tournament'];?>" />
													                		<input type="hidden" name="tournament_count" value="<?php echo $group['tournament_count'];?>" />

													                		<button class="btn btn-danger btn-block push-5" type="submit">수정 적용</button>
													                	<?php	
													                		}
																			}
													                	?>													                	
													                	
																	</form>
																		<?php 
													                		if($group['scorecard_print'] == 0 ){
													                	?>
													                	<!-- 토너먼트 시드 배치가 되었을때 출력버튼 생성-->
													                	<button class="btn btn-warning btn-block"  onclick=""><i class="si si-printer"></i> <a href="scorecards_tm.php?wr_id=<?=$group['wr_id'];?>" class="text-white" target="_blank">채점표 인쇄</a></button>
													                	<!-- end 토너먼트 시드 배치가 되었을때 출력버튼 생성-->
													                	<?php
													                		}else{
													                	?>
													                	<button class="btn btn-danger btn-block"  onclick=""><i class="si si-printer"></i> <a href="scorecards_tm.php?wr_id=<?=$group['wr_id'];?>" class="text-white" target="_blank">채점표 인쇄됨</a></button>
													                	<?php
													                		}
													                	?>
													                </div>
											                </ul>
														</div>
													</td>
													<?php
													}else if($group['game_court'] == null){
														?>
														<td>
															<a href="#" class="text-black"  >
																<div class="text-center font-w700 free_space">
																</div>
															</a>
														</td>
														
														<?php
													}else{
														$no_count = 1;
														?>
														<td>
															<a class="text-black"  >
																<div onclick="pop_not_assigned('<?=$j;?>','<?=$day;?>','<?=$match_array[0]['application_period'];?>','<?=$auto_increase;?>');" class="text-center font-w700 free_space">
																	경기 배정
																</div>
															</a>
														</td>
														<?php
													}
													
												?>
												<?php
												$auto_increase++;
												}
												$c++;
												
											}
											if($group['wr_id'] == ""){
												break;
											}
											echo "</tr>\n";
											$cnt++;
											}
											
										}
									?>
								
							</tbody>
						</table>
					</div>
				</div>
				
			</div>
		</div>
		<!-- END 타임테이블 -->
	</div>
    
</div>
<!-- END Page Content -->

	



<script>
$("form#frmReview").submit(function(event){       
    //disable the default form submission
    event.preventDefault();
    var fd = new FormData($(this)[0]); 
    $.ajax({
        url: "insert_game_score.php",
        type: "POST",
        dataType:'json',
        data: fd,
        async: false,
        cache: false,
        contentType: false,
        processData: false,
        success:  function(data){
        	console.log(data);
        	if(data[0]['result'] == "false"){
	   			$('#fail_button').click();
	   		}
	   		if(data[0]['result'] == "success"){
		    	var str = '';
		    	var i = 1;
				str =  '';
				if(Number(data[0]['team_1_score']) != 0 &&  Number(data[0]['team_2_score']) != 0){
					if(Number(data[0]['team_1_score']) > Number(data[0]['team_2_score']) ){
						//console.log("aaaa");
						$('#score_class_'+data[0]['wr_id']).removeClass();
						$('#score_class_'+data[0]['wr_id']).addClass("club_area end_matchA");
					}else{
						//console.log("bbb");
						$('#score_class_'+data[0]['wr_id']).removeClass();
						$('#score_class_'+data[0]['wr_id']).addClass("club_area end_matchB");
					}
				}else{
					$('#score_class_'+data[0]['wr_id']).removeClass();
					$('#score_class_'+data[0]['wr_id']).addClass("club_area");
				}
				
				str = '<div class="a_score">'+data[0]['team_1_score']+'</div>'
					+'<div class="b_score">'+data[0]['team_2_score']+'</div>';
				$('#score_session_'+data[0]['wr_id']).html(str);
				str = '';
				
				$('#score_button_'+data[0]['wr_id']).html("<input type='hidden' name='modify' value='1' /><input type='hidden' name='team_1_score_before' value='"+data[0]['team_1_score']+"' /><input type='hidden' name='team_2_score_before' value='"+data[0]['team_2_score']+"' /><input type='hidden' name='tournament' value='"+data[0]['tournament']+"' /><input type='hidden' name='tournament_count' value='"+data[0]['tournament_count']+"' /><button class='btn btn-danger btn-block' type='submit'>수정 적용</button>");
	   			if(data[0]['tournament_create'] == "T32"){
	   				//alert("32강 토너먼트가 생성되엇습니다.");
	   				window.open('<?php echo G5_URL?>/s_adm/scorecards_tm.php?tournament=T32&division='+data[0]['division']+'&series='+data[0]['series']+'&series_sub='+data[0]['series_sub']+'&match_code=<?=$code;?>&gym_id=<?=$gym_id;?>&application_period=<?=$application_period;?>', '_blank');
	   			}
	   			if(data[0]['tournament_create'] == "T16"){
	   				//alert("16강 토너먼트가 생성되엇습니다.");
	   				window.open('<?php echo G5_URL?>/s_adm/scorecards_tm.php?tournament=T16&division='+data[0]['division']+'&series='+data[0]['series']+'&series_sub='+data[0]['series_sub']+'&match_code=<?=$code;?>&gym_id=<?=$gym_id;?>&application_period=<?=$application_period;?>', '_blank');
	   			}
	   			if(data[0]['tournament_create'] == "T8"){
	   				//alert("8강 토너먼트가 생성되엇습니다.");
	   				window.open('<?php echo G5_URL?>/s_adm/scorecards_tm.php?tournament=T8&division='+data[0]['division']+'&series='+data[0]['series']+'&series_sub='+data[0]['series_sub']+'&match_code=<?=$code;?>&gym_id=<?=$gym_id;?>&application_period=<?=$application_period;?>', '_blank');
	   			}
	   			if(data[0]['tournament_create'] == "T4"){
	   				//alert("4강 토너먼트가 생성되엇습니다.");
	   				window.open('<?php echo G5_URL?>/s_adm/scorecards_tm.php?tournament=T4&division='+data[0]['division']+'&series='+data[0]['series']+'&series_sub='+data[0]['series_sub']+'&match_code=<?=$code;?>&gym_id=<?=$gym_id;?>&application_period=<?=$application_period;?>', '_blank');
	   			}
	   			if(data[0]['tournament_create'] == "C"){
	   				//alert("결승이 생성되엇습니다.");
	   				window.open('<?php echo G5_URL?>/s_adm/scorecards_tm.php?tournament=C&division='+data[0]['division']+'&series='+data[0]['series']+'&series_sub='+data[0]['series_sub']+'&match_code=<?=$code;?>&gym_id=<?=$gym_id;?>&application_period=<?=$application_period;?>', '_blank');
	   			}
	   		}
        }
    });
    return false;
});


        
</script>



<?php require 'inc/views/template_footer_end.php'; ?>