<?php
$menu_cate2 ='2';
$menu_cate3 ='2';
include_once('./_common.php');
include_once(G5_SADM_PATH.'/_head.php');

$increase_num = 0;


?>
<script src="<?php echo G5_SADM_URL?>/assets/js/autosave"></script>
<!-- sub hd_section-->
	<div class="tit_area">
		<div class="tit_subject">대회관리</div>
		<div class="tit">Step.3 : 경기종목설정 &nbsp;<a id="grad_add" class="btn01"><i class="flaticon-add"></i> 추가</a></div>
		<div class="r-btn-area">
			<ul>
				<li><a href="" class="btn_default">초기화</a></li>
				<li><a href="" class="btn_default">저장하기</a></li>
				<li><a href="" class="btn_default">불러오기</a></li>
			</ul>
		</div>
	</div>
<!--//sub_hd_area -->

<!-- !PAGE CONTENT! -->
<form class="form-horizontal push-10-t push-10" action="grade_write.php" method="post" onsubmit="return true;">
<input type="hidden" name="wr_id" value="<?php echo $wr_id;?>" />
<input type="hidden" name="match_code" value="<?php echo $code;?>" />

<div id="grad_div">

	<!-- 대회정보 필수입력-->
	<?php
    	$sql = "select * from grade_data where match_code = '$code'";
		$result = sql_query($sql);
		while($r = sql_fetch_array($result)){
	?>
	<input type="hidden" name="grad_wr_id[<?php echo $increase_num;?>]" value="<?php echo $r['wr_id'];?>" />

	<div id="grad_set_area" class="tbl_frm01">
		<div class="champ_section">

			<div class="tit">대회정보1 <a href="" class=""><i class="flaticon-cancel-1 color5"></i></a></div>
			<ul class="champ_option_group">
				<li>
					<div>
						<div class="tit">1.개인or단체</div>
						<div>
							<ul class="select_group">
								<li>
									<label>
									<input class="select" value="1" <?php if($r['grad_division'] == "1"){ echo "checked"; } ?>  name="grad_division[<?php echo $increase_num;?>]" type="radio">
										<div class="opt">
											개인전
										</div>
									</label>
								</li>
								<li>
									<label>
									<input class="select"  value="2" <?php if($r['grad_division'] == "2"){ echo "checked"; } ?>  name="grad_division[<?php echo $increase_num;?>]" type="radio">
										<div class="opt">
											단체전
										</div>
									</label>
								</li>
							</ul>
						</div>
					</div>
				</li>
				<li>
					<div class="select_group">
						<div class="tit">2.성별</div>
						<div>
							<ul class="select_group">
								<li>
									<label>
									<input class="select" value="1"  <?php if($r['grad_sex'] == "1"){ echo "checked"; } ?>  name="grad_sex[<?php echo $increase_num;?>]" type="radio">
										<div class="opt">
											남자
										</div>
									</label>
								</li>
								<li>
									<label>
									<input class="select"  value="2" <?php if($r['grad_sex'] == "2"){ echo "checked"; } ?> name="grad_sex[<?php echo $increase_num;?>]" type="radio">
										<div class="opt">
											여자
										</div>
									</label>
								</li>
								<li>
									<label>
									<input class="select"  value="3" <?php if($r['grad_sex'] == "3"){ echo "checked"; } ?>  name="grad_sex[<?php echo $increase_num;?>]" type="radio">
										<div class="opt">
											혼성
										</div>
									</label>
								</li>
							</ul>
						</div>
					</div>
				</li>
				<li>
					<div class="select_group">
						<div class="tit">3.단식 or 복식</div>
						<div>
							<ul class="select_group">
								<li>
									<label >
									<input class="select" value="1" <?php if($r['game_rules'] == "1"){ echo "checked"; } ?> name="game_rules[<?php echo $increase_num;?>]" type="radio">
										<div class="opt">
											단식
										</div>
									</label>
								</li>
								<li>
									<label >
									<input class="select"  value="2" <?php if($r['game_rules'] == "1"){ echo "checked"; } ?> name="game_rules[<?php echo $increase_num;?>]" type="radio">
										<div class="opt">
											복식
										</div>
									</label>
								</li>
							</ul>
						</div>
					</div>
				</li>
			</ul>

			<div class="champ_group_ect">
				<ul>
					<li><input id="rating_valance" value="1" <?php if($r['grad_pay_chk'] == "1"){ echo "checked"; } ?> name="grad_pay_chk[<?php echo $increase_num;?>]" type="checkbox"><label for="rating_valance">&nbsp;선수출신과 함께 대회 참가시 출전등급 0.5 이하까지 참가 가능</label></li>
					<li>참가비용 : <input id="rating_valance" name="grad_pay[]" value="<?php echo $r['grad_pay'] ; ?>"  type="text" class="frm_input" size="8"> 원 <span class="tip">미입력시 금액 출력 안함</span></li>
				</ul>
			</div>


			<table>

				<tbody>
					<tr>
						<td colspan="4" class="tit_area">종목명 : <input name="grad_title[]" value="<?php echo $r['grad_title'];?>" id="" required="" class="frm_input " size="25" maxlength="255" type="text"> <a href="" class=""><i class="flaticon-cancel-1 color5"></i></a></td>
					</tr>
					<tr>
						<th>연령</th>
						<td>
							<input type="checkbox" id="all_ages" <?php if($r['grad_age_chk'] == "1"){ echo "checked"; } ?> name="grad_age_chk[<?php echo $increase_num;?>]" value="1" /><label for="all_ages">&nbsp;무관</label>&nbsp;&nbsp;
							<select name="grad_age_low[]">
								<option value="">선택</option>
								<?php
									for($i = 1920; $i <= date('Y') ;$i++){
								?>
								<option value="<?php echo $i;?>" <?php if($r['grad_age_low'] == "$i"){ echo "selected"; } ?>><?php echo $i;?></option>
								<?php
									}
								?>
							</select>
							부터 <span>~</span>
							<select name="grad_age_high[]">
								<option value="">선택</option>
								<?php
									for($i = 1920; $i <= date('Y') ;$i++){
								?>
								<option value="<?php echo $i;?>" <?php if($r['grad_age_high'] == "$i"){ echo "selected"; } ?>><?php echo $i;?></option>
								<?php
									}
								?>
							</select>
							까지
						</td>
						<th>등급</th>
						<td>
							<div>
								<select name="grad_low[]">
									<?php
										for($i = 0; $i <= 6 ;$i= $i+0.5){
									?>
									<option value="<?php echo $i;?>" <?php if($r['grad_low'] == "$i"){ echo "selected"; } ?>><?php echo $i;?></option>
									<?php
										}
									?>
								</select>
								부터 ~
								<select name="grad_high[]">
									<?php
										for($i = 0; $i <= 6 ;$i= $i+0.5){
									?>
									<option value="<?php echo $i;?>" <?php if($r['grad_high'] == "$i"){ echo "selected"; } ?>><?php echo $i;?></option>
									<?php
										}
									?>
								</select>
								까지
							/ 합산등급
							<select name="grad_average[]" >
								<option value="">선택</option>
								<?php
									for($i = 0; $i <= 12 ;$i= $i+0.5){
								?>
								<option value="<?php echo $i;?>" <?php if($r['grad_average'] == "$i"){ echo "selected"; } ?>><?php echo $i;?></option>
								<?php
									}
								?>
							</select>
							점 이하

							</div>
	            		</td>
					</tr>

					<tr>
						<th>선수출신</th>
						<td>
							<input name="grad_player[]"  value="<?php echo $r['grad_player'] ; ?>" id="" class="frm_input " size="5" maxlength="5" type="text"> 인 이하 <span class="tip">숫자를 입력해 주세요</span>
	                    </td>
	                    <th>비고 </th>
						<td>
							<input name="grad_notice[]" value="<?php echo $r['grad_notice'] ; ?>" id="" class="frm_input " size="30" maxlength="30" type="text"><span class="tip">기타사항 입력</span>
	                   </td>
					</tr>
				</tbody>

			</table>
		</div>


	</div>
	<?php
       		$increase_num++;
			}
        ?>
	<!-- //대회정보 필수입력-->
	<div id="grad_set_area" class="tbl_frm01">
		<div class="champ_section">

			<div class="tit">대회정보1 <a href="" class=""><i class="flaticon-cancel-1 color5"></i></a></div>
			<ul class="champ_option_group">
				<li>
					<div>
						<div class="tit">1.개인or단체</div>
						<div>
							<ul class="select_group">
								<li>
									<label>
									<input class="select" value="1" <?php if($r['grad_division'] == "1"){ echo "checked"; } ?>  name="grad_division[<?php echo $increase_num;?>]" type="radio">
										<div class="opt">
											개인전
										</div>
									</label>
								</li>
								<li>
									<label>
									<input class="select"  value="2" <?php if($r['grad_division'] == "2"){ echo "checked"; } ?>  name="grad_division[<?php echo $increase_num;?>]" type="radio">
										<div class="opt">
											단체전
										</div>
									</label>
								</li>
							</ul>
						</div>
					</div>
				</li>
				<li>
					<div class="select_group">
						<div class="tit">2.성별</div>
						<div>
							<ul class="select_group">
								<li>
									<label>
									<input class="select" value="1"  <?php if($r['grad_sex'] == "1"){ echo "checked"; } ?>  name="grad_sex[<?php echo $increase_num;?>]" type="radio">
										<div class="opt">
											남자
										</div>
									</label>
								</li>
								<li>
									<label>
									<input class="select"  value="2" <?php if($r['grad_sex'] == "2"){ echo "checked"; } ?> name="grad_sex[<?php echo $increase_num;?>]" type="radio">
										<div class="opt">
											여자
										</div>
									</label>
								</li>
								<li>
									<label>
									<input class="select"  value="3" <?php if($r['grad_sex'] == "3"){ echo "checked"; } ?>  name="grad_sex[<?php echo $increase_num;?>]" type="radio">
										<div class="opt">
											혼성
										</div>
									</label>
								</li>
							</ul>
						</div>
					</div>
				</li>
				<li>
					<div class="select_group">
						<div class="tit">3.단식 or 복식</div>
						<div>
							<ul class="select_group">
								<li>
									<label >
									<input class="select" value="1" <?php if($r['game_rules'] == "1"){ echo "checked"; } ?> name="game_rules[<?php echo $increase_num;?>]" type="radio">
										<div class="opt">
											단식
										</div>
									</label>
								</li>
								<li>
									<label >
									<input class="select"  value="2" <?php if($r['game_rules'] == "1"){ echo "checked"; } ?> name="game_rules[<?php echo $increase_num;?>]" type="radio">
										<div class="opt">
											복식
										</div>
									</label>
								</li>
							</ul>
						</div>
					</div>
				</li>
			</ul>

			<div class="champ_group_ect">
				<ul>
					<li><input id="rating_valance" value="1" <?php if($r['grad_pay_chk'] == "1"){ echo "checked"; } ?> name="grad_pay_chk[<?php echo $increase_num;?>]" type="checkbox"><label for="rating_valance">&nbsp;선수출신과 함께 대회 참가시 출전등급 0.5 이하까지 참가 가능</label></li>
					<li>참가비용 : <input id="rating_valance" name="grad_pay[]" value="<?php echo $r['grad_pay'] ; ?>"  type="text" class="frm_input" size="8"> 원 <span class="tip">미입력시 금액 출력 안함</span></li>
				</ul>
			</div>
			<table class="mb-20">

				<tbody>
					<tr>
						<td colspan="4" class="tit_area">종목명 : <input name="grad_title[]" value="<?php echo $r['grad_title'];?>" id="" class="frm_input " size="25" maxlength="255" type="text"> <a href="" class=""><i class="flaticon-cancel-1 color5"></i></a></td>
					</tr>
					<tr>
						<th>연령</th>
						<td>
							<input type="checkbox" id="all_ages" <?php if($r['grad_age_chk'] == "1"){ echo "checked"; } ?> name="grad_age_chk[<?php echo $increase_num;?>]" value="1" /><label for="all_ages">&nbsp;무관</label>&nbsp;&nbsp;
							<select name="grad_age_low[]">
								<option value="">선택</option>
								<?php
									for($i = 1920; $i <= date('Y') ;$i++){
								?>
								<option value="<?php echo $i;?>" <?php if($r['grad_age_low'] == "$i"){ echo "selected"; } ?>><?php echo $i;?></option>
								<?php
									}
								?>
							</select>
							부터 <span>~</span>
							<select name="grad_age_high[]">
								<option value="">선택</option>
								<?php
									for($i = 1920; $i <= date('Y') ;$i++){
								?>
								<option value="<?php echo $i;?>" <?php if($r['grad_age_high'] == "$i"){ echo "selected"; } ?>><?php echo $i;?></option>
								<?php
									}
								?>
							</select>
							까지
						</td>
						<th>등급</th>
						<td>
							<div>
								<select name="grad_low[]">
									<?php
										for($i = 0; $i <= 6 ;$i= $i+0.5){
									?>
									<option value="<?php echo $i;?>" <?php if($r['grad_low'] == "$i"){ echo "selected"; } ?>><?php echo $i;?></option>
									<?php
										}
									?>
								</select>
								부터 ~
								<select name="grad_high[]">
									<?php
										for($i = 0; $i <= 6 ;$i= $i+0.5){
									?>
									<option value="<?php echo $i;?>" <?php if($r['grad_high'] == "$i"){ echo "selected"; } ?>><?php echo $i;?></option>
									<?php
										}
									?>
								</select>
								까지
							/ 합산등급
							<select name="grad_average[]" >
								<option value="">선택</option>
								<?php
									for($i = 0; $i <= 12 ;$i= $i+0.5){
								?>
								<option value="<?php echo $i;?>" <?php if($r['grad_average'] == "$i"){ echo "selected"; } ?>><?php echo $i;?></option>
								<?php
									}
								?>
							</select>
							점 이하

							</div>
	            		</td>
					</tr>

					<tr>
						<th>선수출신</th>
						<td>
							<input name="grad_player[]"  value="<?php echo $r['grad_player'] ; ?>" id="" class="frm_input " size="5" maxlength="5" type="text"> 인 이하 <span class="tip">숫자를 입력해 주세요</span>
	                    </td>
	                    <th>비고 </th>
						<td>
							<input name="grad_notice[]" value="<?php echo $r['grad_notice'] ; ?>" id="" class="frm_input " size="30" maxlength="30" type="text"><span class="tip">기타사항 입력</span>
	                   </td>
					</tr>
				</tbody>
			</table>
			<div class="text-center place_date_add">
				<a >
					<span class="color5">+ 종목명 추가</span>
				</a>
			</div>

		</div>

		<?php $increase_num++; ?>

	</div>

</div>

	<div class="btn_area">
		<a class="btn btn01 fw-600" href="<?php echo G5_SADM_URL?>/championship_add_step1.php?code=<?php echo $code;?>">뒤 로</a>
		<a class="btn btn01 fw-600" href="<?php echo G5_SADM_URL?>/championship_add_step3.php?code=<?php echo $code;?>">다 음(skip)</a>
		<input type="submit" class="btn btn01 fw-600" value="다음" />
	</div>
</form>
<!-- End page content -->
<script>
	var increase_num = <?php echo $increase_num;?>;
	$("#grad_add").click(function(){

		var th = $("#grad_set_area").clone();
		console.log(th.find('input'));
		th.find('input').each(function(index){

			if(index == 0){
				$(this).attr("name","grad_division["+increase_num+"]");
			}
			if(index == 1)
				$(this).attr("name","grad_division["+increase_num+"]");
			if(index == 2)
				$(this).attr("name","grad_sex["+increase_num+"]");
			if(index == 3)
				$(this).attr("name","grad_sex["+increase_num+"]");
			if(index == 4)
				$(this).attr("name","grad_sex["+increase_num+"]");
			if(index == 5)
				$(this).attr("name","game_rules["+increase_num+"]");
			if(index == 6)
				$(this).attr("name","game_rules["+increase_num+"]");
			if(index == 7)
				$(this).attr("name","grad_pay_chk["+increase_num+"]");
			if(index == 10)
				$(this).attr("name","grad_age_chk["+increase_num+"]");


			$(this).attr("value","");
		});
		$("#grad_div").append(th);
		increase_num++;
	});
</script>



<?php
include_once(G5_SADM_PATH.'/_tail.php');
?>
