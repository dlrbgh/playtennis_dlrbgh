<?php
include_once('../common.php');
if (!defined('_GNUBOARD_')) exit; // 개별 페이지 접근 불가
include_once(G5_AOS_PATH.'/head.php');
$menu_cate2 ='competition_view';
$menu_cate3 ='7';
?>

<?php

?>
<!-- sub nav -->
<?php include_once('./app_sub_nav.php'); ?>
<!-- end sub nav -->
<?php
	$defaultQstr = "competition_time_tbl.php?c={$c}&d={$d}&s={$s}&ss={$ss}&gi={$gi}&t={$t}";
	$gi = isset($gi) && $gi!='' ? $gi : 0;
	$t = isset($t) && $t!='' ? $t : "L";


//
	// print $d;
?>


<!-- Contents Area -->
<div class="pop_container">
	<?php if(false &&! $competition['opening_date']){?>
		<div class="empty_waiting">
			<div class="img_area">
				<img src="<?php echo G5_IMG_URL.'/common/intro_logo1.png';?>">
			</div>
			<div class="cmt ani02">
				경기가 준비중입니다.
			</div>
		</div>
	<?php }else {?>
	<!-- 탭-코트-->
	<section class="section2">
		<div class="pop_hd">
			<div class="tit">경기 선택</div>
		</div>
		<div class="content ">
			<div class="btn-group" role="group">

				<ul class="btn-list list2">
					<?php
						$group_sql = "select division, series, series_sub from group_data where match_code = '{$c}'
						group by division, series, series_sub
						order by division, series, series_sub
						";
						$groups = sql_query($group_sql);
						$is_first = $d!='';
						$active_class = 'class="active"';

						$index = 0;
						while($row = sql_fetch_array($groups)){
							if($d==''){
								$d = $row['division'];
								$s = $row['series'];
								$ss = $row['series_sub'];
							}
							$groupqstr = "?c={$c}&d={$row['division']}&s={$row['series']}&ss={$row['series_sub']}&gi={$index}&t=L";

							?>
							<li><a <?=($index."" == $gi? $active_class : "")?> href="<?=$groupqstr?>">
								<?=$row['division']?> <?=$row['series']?> <?=$row['series_sub']?></a></li>
						<?php
						$index++;
					}?>
				</ul>
	        </div>
	    </div>
	</section>
	<!-- //탭-코트-->
	<?php if($d != ''){?>
	<section>
		<div class="content">
			<div class="con_tit_area clear">
				<div class="tit mb-5 fw-700">
					<?=$competition['wr_name']?>
				</div>
				<div class="tit">
					<?=$competition['date1']?> ~ <?=$competition['date2']?> <?=$competition['gym_name']?>
				</div>
				<div class="r-area">
					<ul>
						<li class="color5 fw-700"></li>
					</ul>
				</div>
			</div>

			<div class="tbl_style01 tbl_striped mb-20">
				<table>
					<thead>
						<tr>
							<th>순위</th>
							<th>클럽</th>
							<th>선수</th>
						</tr>
					</thead>
					<tbody class="text-center">
						<?php


              $get_match_result_sql = "select * from
              game_score_data
              where
              match_code = '{$c}'
              and division = '{$d}'
              and series = '{$s}'
              and series_sub = '{$ss}'
              and tournament = 'C'";


              $match_result;

              $winner;
              $loser;
              if($d == '단체전'){
                  $match_result_list = sql_query($get_match_result_sql);
                  $team_1_win_count = 0;
                  $team_2_win_count = 0;
                  while($row = sql_fetch_array($match_result_list)){

                    if($row['team_1_score'] > $row['team_2_score']){
                      $team_1_win_count ++;
                    }else{
                      $team_2_win_count ++;
                    }
                  }

                  if($team_1_win_count > $team_2_win_count){
                    $winner = sql_fetch("select * from team_event_data where team_code = '".$match_result_list["team_1_code"]."'", true);
                    $loser = sql_fetch("select * from team_event_data where team_code = '".$match_result_list["team_2_code"]."'", true);

                  }else{
                    $winner = sql_fetch("select * from team_event_data where team_code = '".$match_result_list["team_2_code"]."'", true);
                    $loser = sql_fetch("select * from team_event_data where team_code = '".$match_result_list["team_1_code"]."'", true);
                  }
                  print $team_1_win_count." ".$team_2_win_count;
                  print_r("select * from team_event_data where team_code = '".$match_result_list["team_2_code"]."'<br>");
                  print_r("select * from team_event_data where team_code = '".$match_result_list["team_1_code"]."'");

              }else{
                  $match_result_list = sql_fetch($get_match_result_sql);

                  if($match_result_list['team_1_score'] > $match_result_list['team_2_score']){

                    $winner = sql_fetch("select * from team_data where team_code = '".$match_result_list["team_1_code"]."'");
                    $loser = sql_fetch("select * from team_data where team_code = '".$match_result_list["team_2_code"]."'");

                  }else{
                    $winner = sql_fetch("select * from team_data where team_code = '".$match_result_list["team_2_code"]."'");
                    $loser = sql_fetch("select * from team_data where team_code = '".$match_result_list["team_1_code"]."'");
                  }
              }


              $match_result [0][club] = array($winner['club'], $winner['team_2_club']);
              $match_result [0][member] = array($winner['team_1_name'], $winner['team_2_name']);
              $match_result [1][club] = array($loser['club'], $loser['team_2_club']);
              $match_result [1][member] = array($loser['team_1_name'], $loser['team_2_name']);
						 ?>
						<tr>
							<td>우승</td>
							<td><?php print join('<br>', $match_result[0]['club'])?></td>
							<td><?php print join('<br>', $match_result[0]['member'])?></td>
						</tr>
						<tr>
							<td>준우승</td>
              <td><?php print join('<br>', $match_result[1]['club'])?></td>
							<td><?php print join('<br>', $match_result[1]['member'])?></td>
						</tr>
					</tbody>
				</table>
			</div>


		</div>
	</section>

	<!--//토너먼트 대진표 -->
<!--
	<section>
		<div class="content">
			<div class="con_tit_area clear">
				<div class="tit">본선경기 결과</div>
				<div class="r-area">
					<ul>
						<li class="color5 fw-700"></li>
					</ul>
				</div>
			</div>

			<div class="tab_group1 mb-0">
				<?php
					$tournament_sql = "select
									wr_id
							    ,team_1_code
							    ,(select concat(club,' ',team_1_name, ' ',team_2_name ) from team_data where team_data.team_code = team_1_code ) as team_1
							    ,team_1_score
							    ,team_2_code
							    ,(select concat(club,' ',team_1_name, ' ',team_2_name ) from team_data where team_data.team_code = team_2_code ) as team_2
							    ,team_2_score
							    ,concat(game_court,'코트') as game_court
							    ,game_increase
							    ,tournament
							    ,tournament_count
									,case when tournament_count = 0 then '결승'
									 else concat(tournament_count * 2,'강') end as count_for_display
							    ,tournament_num
							    ,end_game
									,case when end_game = 'Y' then '종료'
									else '경기중' end as end_game_display
									,date_format(wr_datetime,get_format(date,'iso')) as datetime_display
									,(select gym_name from gym_data where wr_id = gym_code) as gym_name
							from game_score_data
							where match_code = '{$c}'
							and division = '{$d}'
							and series = '{$s}'
							and series_sub = '{$ss}'
							and tournament != 'L'
							and game_assign = '1' order by wr_id desc ";
							// print $tournament_sql;
							$tournament = sql_query($tournament_sql, true);
							$rount_index = "";
							$round_html;
							$rount_detail = "";
							while($row = sql_fetch_array($tournament)){
								if($round_index == ""){
									$round_index = $row['count_for_display'];
									$round_html .="<a class=\"round_tab active\" data-for=\"{$row['count_for_display']}\">{$row['count_for_display']}</a>";
								}else if($round_index != $row['count_for_display']){
									$round_index = $row['count_for_display'];
									$round_html .="<a class=\"round_tab\" data-for=\"{$round_index}\">{$round_index}</a>";
								}
								$tournament_list[$row['count_for_display']]['body'][] = $row;
							}
							print $round_html;
				?>
        	</div>

			<div class="mb-20">
				<?= tournament_list_html_render($tournament_list)?>
			</div>
		</div>
	</section> -->
	<?php }?>

	<!-- //본선 토너먼트 ㅇ그룹 경기 결과-->
	<script>
		$('.round_tab').click(function(event){
			$('.round_tab.active').removeClass('active');
			$(this).addClass('active');
			var target = $(this).data('for');
			$('.tournament_list').hide();
			$('#'+target).show();
		})
	</script>
	<?php

		function cmp($a, $b){
			return ($b[3]-0) -($a[3]-0);
		}

			function tournament_list_html_render($list){
				$wrapper = '<ul class="tournament_list {:is_active}" id="{:id}"><div class="status_banner">{:header}</div>{:list}</ul>';
				$html = '';
				$round = '';
				$tmp_html = '';
				$tmp_wrapper = '';
				$list_stack = '';
				$tmp_list_template = $list_template;
				$is_first = true;
				$is_active = 'active';
				foreach ($list as $key => $value) {
					$round = $key;
					$ttt = 0;
					$list_stack = '';
					$tmp_list_template = $list_template;
					foreach($list[$round]['body'] as $match_key => $match_value){
						$list_stack .= set_list_template($match_value);
					}
					$tmp_wrapper = $wrapper;
					$tmp_wrapper = str_replace("{:is_active}", $is_active, $tmp_wrapper);
					$tmp_wrapper = str_replace("{:id}", "{$key}", $tmp_wrapper);
					$tmp_wrapper = str_replace("{:header}", "{$key} 토너먼트 상태", $tmp_wrapper);
					$html .=str_replace("{:list}", $list_stack, $tmp_wrapper);
					$is_active = "hide";
		 		}
				return $html;

			}
			function set_list_template($data){
				$list_template = '<li>
					<div class="tournament_match">
						<div class="tournament_hd">
							<span>{:round} {:court} {:match}</span>
							<span class="r-side-area"></span>
						</div>
						<div class="tournament_content">
							<div class="l-area">
								<div class="teamA">{:club1} - {:team_1_name_1}</div>
								<div class="teamA">{:club1} - {:team_1_name_2}</div>
							</div>
							<div class="tournament_point">
								{:status}
							</div>
							<div class="r-area">
								<div class="teamB">{:team_2_name_1} - {:club2}</div>
								<div class="teamB">{:team_2_name_2} - {:club2}</div>
							</div>
						</div>
						<div class="tournament_ft">
							<span>{:gym_name}</span>
							<span class="r-side-area">{:date}</span>
						</div>
					</div>
				</li>';
				// print_r( $data);
				//0			1				2
				//club name1, name2

				$team_1 = explode(' ', $data['team_1']);
				$team_2 = explode(' ', $data['team_2']);

				$list_template = str_replace('{:club1}',$team_1[0], $list_template);
				$list_template = str_replace('{:team_1_name_1}',$team_1[1], $list_template);
				$list_template = str_replace('{:team_1_name_2}',$team_1[2], $list_template);
				$list_template = str_replace('{:club2}',$team_2[0], $list_template);
				$list_template = str_replace('{:team_2_name_1}',$team_2[1], $list_template);
				$list_template = str_replace('{:team_2_name_2}',$team_2[2], $list_template);
				$list_template = str_replace('{:court}',$data['game_court'], $list_template);
				$list_template = str_replace('{:round}',$data['count_for_display'], $list_template);
				$list_template = str_replace('{:gym_name}',$data['gym_name'], $list_template);
				$list_template = str_replace('{:date}',$data['datetime_display'], $list_template);
				// $list_template = str_replace('{:status}',$data['end_game_display'], $list_template);
				// $list_template = str_replace('',$team_1[0], $list_template);
				// $list_template = str_replace('',$team_1[0], $list_template);


				return $list_template;
			}
	 ?>
	 <?php }?>
</div>
<!-- end Contents Area -->

<?php
include_once(G5_AOS_PATH.'/tail.php');
?>
