<?php
include_once('../common.php');
if (!defined('_GNUBOARD_')) exit; // 개별 페이지 접근 불가


$is_history_back= true;

include_once(G5_AOS_PATH.'/head.php');
//$menu_cate2 ='competition_view';
$menu_cate3 ='6';

$sql = "select a.*,b.division,b.series,b.series_sub,b.num from game_score_data as a inner join group_data as b
		where a.code = '$game_code' and a.group_code = b.code group by a.game_increase order by a.game_increase asc";
$game_result = sql_query($sql);
$game_data = sql_fetch_array($game_result);




if($game_data['division'] == "단체전"){
	$sql_team1 = "select * from team_data where match_code = '$game_data[match_code]' and team_code = '$game_data[team_1_event_code]'";
	$team1_result = sql_query($sql_team1);
	$team1 = sql_fetch_array($team1_result);

	$sql_team2 = "select * from team_data where match_code = '$game_data[match_code]' and team_code = '$game_data[team_2_event_code]'";
	$team2_result = sql_query($sql_team2);
	$team2 = sql_fetch_array($team2_result);
}else{
	$sql_team1 = "select * from team_data where match_code = '$game_data[match_code]' and team_code = '$game_data[team_1_code]'";
	$team1_result = sql_query($sql_team1);
	$team1 = sql_fetch_array($team1_result);

	$sql_team2 = "select * from team_data where match_code = '$game_data[match_code]' and team_code = '$game_data[team_2_code]'";
	$team2_result = sql_query($sql_team2);
	$team2 = sql_fetch_array($team2_result);
}

// if($game_data['team_1_score'] != "0" && $game_data['team_2_score'] != "0" ){
// 	$modify = 1;
// 	print ">>>>>>>>>>>>>>>>>>>>";
// }

if($game_data['end_game'] == 'Y'){
	$modify = 1;
}



$action_url = "insert_game_score_dlrbgh.php";
// if($game_data['series_sub'] == "신인부"){
// 	$action_url = "insert_game_score_64.php";
// }


?>

<!-- sub nav -->
<?php include_once('./app_sub_nav.php'); ?>
<!-- end sub nav -->

<!-- Contents Area -->
<div class="pop_container pt-50 insert_score_area">
	<section>
		<div class="content">
			<form name="frmReview" id="frmReview" action="<?php echo $action_url;?>" enctype="multipart/form-data" method="post" autocomplete="off" >
<input type="hidden" name="wr_id" value="<?=$game_data['wr_id'];?>">
<input type="hidden" name="division" value="<?=$game_data['division'];?>">
<input type="hidden" name="series" value="<?=$game_data['series'];?>">
<input type="hidden" name="series_sub" value="<?=$game_data['series_sub'];?>">
<input type="hidden" name="match_code" value="<?=$game_data['match_code'];?>">
<input type="hidden" name="tournament" value="<?=$game_data['tournament'];?>">
<input type="hidden" name="group_code" value="<?php print $game_data['group_code']?>">
<input type="hidden" name="tournament_count" value="<?=$game_data['tournament_count'];?>">
<!-- <input type="hidden" name="team_1_dis" id="team_1_dis" value="">
<input type="hidden" name="team_2_dis" id="team_2_dis"  value=""> -->
<input type="hidden" name="team_1_score_before" value="<?=$game_data['team_1_score'];?>">
<input type="hidden" name="team_2_score_before" value="<?=$game_data['team_2_score'];?>">
<input type="hidden" name="team_1_dis" id="team_1_dis" value="<?=$game_data['team_1_dis'] ?>">
<input type="hidden" name="team_2_dis" id="team_2_dis"  value="<?=$game_data['team_2_dis'] ?>">
<input type="hidden" name="team_1_dis_before" value="<?=$game_data['team_1_dis'] ?>">
<input type="hidden" name="team_2_dis_before" value="<?=$game_data['team_2_dis'] ?>">
<input type="hidden" name="modify" value="<?=$modify;?>">
<input type="hidden" name="rd" value="<?=$rd;?>">

<div class="team_dropdown">
	<div class="hd_tit">
		<?php
			$g_sql = "select * from group_data where code = '$game_data[group_code]'";
			$g_result = sql_query($g_sql);
			$g = sql_fetch_array($g_result);
		?>
		<?php
			if($game_data['tournament'] ==  "L"){
		?>
				<?=$game_data['division'];?> <?=$game_data['series'];?><?=$game_data['series_sub'];?> <?=$g['num']+1;?>조 <?=$game_data['tournament_num'];?>경기

		<?php
			}
		?>
		<?php
			if($game_data['tournament'] ==  "T"){
		?>
				<?=$game_data['division'];?> <?=$game_data['series'];?><?=$game_data['series_sub'];?> <?=$game_data['tournament_count']*2?>강 <?=$game_data['tournament_num']+1;?>경기

		<?php
			}
		?>
		<?php
			if($game_data['tournament'] ==  "C"){
		?>
				<?=$game_data['division'];?> <?=$game_data['series'];?><?=$game_data['series_sub'];?> 결승

		<?php
			}
		?>
	</div>
<div class="tbl_style03 tbl_striped">
	<table>
		<thead>
			<tr>
				<th>클럽</th>
				<th>이름</th>
				<th>점수</th>
				<th>이름</th>
				<th>클럽</th>
			</tr>
		</thead>
		<tbody class="text-center">
			<tr>
				<td>
					<?php if($game_data['team_1_dis'] == "1"){ echo "<font color='red'>기권<br></font>"; }else if($game_data['team_1_dis'] == "2"){ echo "<font color='red'>실격<br></font>"; }else if($game_data['team_1_dis'] == "3"){ echo "<font color='red'>경기안함<br></font>"; } ?>
					<?=$team1['club'];?><br>
					<?=$team1['club'];?>
				</td>
				<td>
					<?=$team1['team_1_name'];?><br>
					<?=$team1['team_2_name'];?>
				</td>
				<td class="match_point">
					<span><input type="tel" autofocus="autofocus" maxlength="1" id="score_team_1_<?=$game_data['wr_id'];?>"
						name="team_1_score"
						value="<?php if($game_data['team_1_score']=="0" && $game_data['team_2_score']=="0"){echo ""; }else {echo $game_data['team_1_score'];}?>" placeholder="0"></span>
					:
					<span><input type="tel" maxlength="1" id="score_team_1_<?=$game_data['wr_id'];?>" name="team_2_score" value="<?php if($game_data['team_1_score']=="0" && $game_data['team_2_score']=="0"){echo"";}else {echo $game_data['team_2_score'];}?>" placeholder="0"></span>
				</td>
				<td>
					<?=$team2['team_1_name'];?><br>
					<?=$team2['team_2_name'];?>
				</td>
				<td>
					<?php if($game_data['team_2_dis'] == "1"){ echo "<font color='red'>기권<br></font>"; }else if($game_data['team_2_dis'] == "2"){ echo "<font color='red'>실격<br></font>";} else if($game_data['team_1_dis'] == "3"){ echo "<font color='red'>경기안함<br></font>"; } ?>
					<?=$team2['club'];?><br>
					<?=$team2['club'];?>
				</td>
			</tr>
			<tr>
				<td colspan="2">

					<div class="btn_group">
					<?php
						if($game_data['division'] == "단체전"){
					?>
						<a class="score_btn" href="popup_team_event_set.php?game_code=<?php echo $game_code;?>&team=1&rd=<?=urlencode($rd)?>">경기배정</a>
					<?php
						}
					?>
					<?php
						if($game_data['division'] == "개인전"){
					?>
						<!-- <a class="score_btn" href="popup_team_event_set.php?game_code=<?php echo $game_code;?>&team=1&rd=<?=urlencode($rd)?>">경기배정</a> -->
					<?php
						}
					?>
				<?php
					// print_r($game_data);
				 ?>

					<?php if($game_data['team_1_dis'] == "0" && $game_data['team_2_dis'] == "0"){?>
						<a class="score_btn" onclick="set_dis('1','1')" >기권</a>
						<a class="score_btn" onclick="set_dis('2','1')" >실격</a>
					<?php }else{
							if($game_data['team_1_dis'] == "1"){?>
							<a class="score_btn bkg6" onclick="set_dis('','1')" >기권취소</a>
						<?php }else if($game_data['team_1_dis'] == "2"){?>
							<a class="score_btn bkg6" onclick="set_dis('','1')" >실격취소</a>
						<?php }
						}?>
					</div>
				</td>

				<td>
					<div class="btn_group">
						<?php if($game_data['team_2_dis'] == "0" && $game_data['team_1_dis'] == "0"){?>
							<a class="score_btn" onclick="set_dis('3','1')" >경기안함</a>
						<?php }else if($game_data['team_1_dis'] == "3" ){?>
								<a class="score_btn bkg6" onclick="set_dis('','1')" >경기안함 취소</a>
						<?php }?>
					</div>
				</td>

				<td colspan="2">
						<div class="btn_group">
					<?php
						if($game_data['division'] == "단체전"){
					?>
						<a class="score_btn" href="popup_team_event_set.php?game_code=<?php echo $game_code;?>&team=2&rd=<?=urlencode($rd)?>">경기배정</a>
					<?php
						}
					?>
					<?php
						if($game_data['division'] == "개인전"){
					?>
						<!-- <a class="score_btn" href="popup_team_event_set.php?game_code=<?php echo $game_code;?>&team=1&rd=<?=urlencode($rd)?>">경기배정</a> -->
					<?php
						}
					?>
					<?php if($game_data['team_2_dis'] == "0" && $game_data['team_1_dis'] == "0"){?>
					<a class="score_btn" onclick="set_dis('1','2')" >기권</a>
					<a class="score_btn" onclick="set_dis('2','2')" >실격</a>
					<?php }else{
							if($game_data['team_2_dis'] == "1"){?>
					<a class="score_btn bkg6" onclick="set_dis('','2')" >기권취소</a>
				<?php }else if($game_data['team_2_dis'] == "2"){?>
					<a class="score_btn bkg6" onclick="set_dis('','2')" >실격취소</a>
				<?php }
				}?>
					</div>
				</td>

			</tr>
		</tbody>
	</table>
</div>

<!--
	<div class="text-center pull-left timetable_teamA_pts">
    	<label class="remove-padding col-md-12_ control-label" for="use-court" id="data_team_1_<?=$game_data['wr_id'];?>">
    		<span class="font-s08"><?=$team1['club'];?></span>
    		<br/>
    		<span class="font-s08"><?=$team1['team_1_name'];?>/<?=$team1['team_2_name'];?></span>
    	</label>
    	<div class="">
            <input class="js-masked-taxid form-control text-center" type="text" id="score_team_1_<?=$game_data['wr_id'];?>" name="team_1_score" value="<?php if($game_data['team_1_score'] != 0 ) echo $game_data['team_1_score'];?>" placeholder="점수">
        </div>
    </div>
	<div class=" text-center text-center pull-right timetable_teamB_pts">
    	<label class="remove-padding col-md-12 control-label" for="use-court" id="data_team_2_<?=$game_data['wr_id'];?>">
    		<span class="font-s08"><?=$team2['club'];?></span>
    		<br/>
    		<span class="font-s08"><?=$team2['team_1_name'];?>/<?=$team2['team_2_name'];?></span>
    	</label>
    	<div class="">
            <input class="js-masked-taxid form-control text-center" type="text" id="score_team_1_<?=$game_data['wr_id'];?>" name="team_2_score" value="<?php if($game_data['team_2_score'] != 0 ) echo $game_data['team_2_score'];?>" placeholder="점수">
        </div>
    </div>
</div>
-->

	<div class="btn_area">
		<!-- <?php print_r($game_data)?> -->
		<div id="score_button_<?=$game_data['wr_id'];?>">
		<?php
			//5-31 일단 모두 입력되지않아도 해야되므로
			// if($game_data['team_1_code'] != "" && $game_data['team_2_code'] != ""){
		?>
		<?php
			if($game_data['team_1_score'] == "0" && $game_data['team_2_score'] == "0" ){
		?>
			<input type="hidden" name="modify" value="0" />
			<input type="hidden" name="tournament" value="<?php  echo $game_data['tournament'];?>" />
			<input type="hidden" name="tournament_count" value="<?php echo $game_data['tournament_count'];?>" />
			<button class="btn_submit" type="submit">적용</button>
		<?php
			}else{
		?>
			<input type="hidden" name="modify" value="1" />
			<input type="hidden" name="team_1_score_before" value="<?php if($game_data['team_1_score'] != 0 ) echo $game_data['team_1_score'];?>" />
			<input type="hidden" name="team_2_score_before" value="<?php if($game_data['team_2_score'] != 0 ) echo $game_data['team_2_score'];?>" />
			<input type="hidden" name="tournament" value="<?php  echo $game_data['tournament'];?>" />
			<input type="hidden" name="tournament_count" value="<?php echo $game_data['tournament_count'];?>" />

			<!-- <a class="score_btn" onclick="" >지각</a>
		 	<a class="score_btn" onclick="" >불참</a>
		 	<a class="score_btn" onclick="" >오기</a>
		 	<a class="score_btn" onclick="" >기타</a> -->
			<?php if($game_data['end_game'] == 'Y'){?>
				<select class="full-width form-control" required name="wr_text" id="reason_select">
					<option value="">사유를 선택해주세요</option>
					<option>지각</option>
					<option>불참</option>
					<option>오기</option>
					<option value="3">기타</option>
				</select>

				<script>
					$('#reason_select').change(function(){
						if($(this).val() == 3){
								$(this).after('<input type="text" required name="wr_text" id="reason_text" placeholder="수정사유를 입력해 주세요(필수)">');
						}else{
							$('#reason_text').remove();
						}
					})
				</script>
			<?php }?>

			<button class="btn_submit" type="submit">수정 적용</button>
		<?php
			}
		?>

		</div>
	</div>
	<?php
	$sql = "select * from game_score_data_history where match_code = '$game_data[match_code]' and game_code = '$game_data[code]' ORDER BY `wr_datetime` desc";
	$result = sql_query($sql);
	if($result->num_rows > 0){?>
		<div class="history_log_area">
			<div class="tit">수정 및 업데이트 내역</div>
				<ul>
					<?php while($r = sql_fetch_array($result)){
							$mem = get_member($r['mb_id']);?>
						<li><?php echo "[".$r['wr_datetime']."]".$r['wr_text']?>(<?php echo $mem['mb_name']?>)</li>
					<?php }?>
				</ul>
			</div>
	<?php }?>

</form>
	</div>
	</section>

</div>
<!-- end Contents Area -->

<script>
	function set_dis(dis,team){
		if(team == "1"){
			$('#team_1_dis').val(dis);
		}
		if(team == "2"){
			$('#team_2_dis').val(dis);
		}
		$( "#frmReview" ).submit();
	}
</script>
<?php
include_once(G5_AOS_PATH.'/tail.php');
?>
