<?php

  include_once('../../common.php');

  $result_raw['page'] = array();
  $result_raw['list_data'] = array();
  $start = $a * $limit;
  // $a = $a * $limit;
  if($division == 'total'){
    $result_raw['page'] = array('total'=>sql_fetch("select count(*) as total from team_data where match_code = '{$c}'")['total'], 'cur'=>$a);
    $sql = "select * from team_data where match_code = '{$c}' limit $start, $limit";
    $result = sql_query($sql);
    while($row = sql_fetch_array($result)){
      $result_raw['list_data'][] = $row;
    }
    $result_raw['sql'] = $sql;
  }else{

    $result_raw['page'] = array('total'=>sql_fetch("select count(*) as total from
                              team_data
                              where match_code = '{$c}'
                              and division = '{$division}'
                              and series = '{$series}'
                              and series_sub = '{$series_sub}'")['total'], 'cur'=>$a);
    $sql = "select * from team_data where match_code = '{$c}'
    and division = '{$division}' and series = '{$series}'and series_sub = '{$series_sub}'
     limit $start, $limit";
    $result = sql_query($sql, true);
    while($row = sql_fetch_array($result)){
      $result_raw['list_data'][] = $row;
    }
    $result_raw['sql'] = $sql;
  }
  die(json_encode($result_raw));

 ?>
