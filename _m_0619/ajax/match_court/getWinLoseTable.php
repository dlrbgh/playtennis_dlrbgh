<?php

  include_once('../../../common.php');


    $is_league_end = "SELECT
          end_game , count(*) as count
  FROM
      game_score_data
          JOIN
      group_data ON game_score_data.group_code = group_data.code
  WHERE
      game_score_data.match_code = '{$c}'
          AND gym_code = ${gym_id}
          AND game_score_data.game_court = ${court_id}
          AND team_1_code <> ''
          and game_score_data.tournament = 'L'
          and end_game = 'N'
          GROUP BY end_game
  ORDER BY game_score_data.division , game_score_data.series , game_score_data.series_sub , group_data.num";
  $count_n = sql_fetch($is_league_end)['count'];
  $tournament = "and game_score_data.tournament = 'L'";
  if($count_n == 0){
    die();
    // $tournament = "and game_score_data.tournament != 'L'";
  }

  $sql = "select
	division,series, series_sub, group_data.num
	,(select concat(club,' ', team_1_name, ' ',team_2_name,' ', team_league_point,' ',team_league_lose_count) from team_data where group_data.team_1 = team_data.team_code)as team_1
  ,(select concat(club,' ', team_1_name, ' ',team_2_name,' ', team_league_point,' ',team_league_lose_count) from team_data where group_data.team_2 = team_data.team_code)as team_2
  ,(select concat(club,' ', team_1_name, ' ',team_2_name,' ', team_league_point,' ',team_league_lose_count) from team_data where group_data.team_3 = team_data.team_code) as team_3
  from group_data
  join (select code as match_code , gym_id from match_data join match_gym_data
  on match_data.code = match_gym_data.match_id) match_gym
  on group_data.match_code = match_gym.match_code
  where match_gym.match_code = '{$c}' and gym_id =  ${gym_id} and game_court = ${court_id}
  {$tournament}
  order by division, series, series_sub, num";

  // print $sql;

  $result = sql_query($sql);
  $json_result = array();
  while($row = sql_fetch_array($result)){
    $json_result [] = $row;
  }

die(json_encode($json_result));
 ?>
