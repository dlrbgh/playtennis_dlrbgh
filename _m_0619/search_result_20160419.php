
<?php require 'inc/config.php'; ?>
<?php require 'inc/views/template_head_start.php'; ?>
<?php
include_once('../common.php');

$search_type	= $_REQUEST['search_type'];
$search_name	= $_REQUEST['search_name'];
$user_code		= $_REQUEST['user_code'];


?>
<!-- Page JS Plugins CSS -->
<link rel="stylesheet" href="<?php echo $one->assets_folder; ?>/js/plugins/slick/slick.min.css">
<link rel="stylesheet" href="<?php echo $one->assets_folder; ?>/js/plugins/slick/slick-theme.min.css">
<link rel="stylesheet" href="<?php echo $one->assets_folder; ?>/js/plugins/select2/select2.min.css">
<link rel="stylesheet" href="<?php echo $one->assets_folder; ?>/js/plugins/select2/select2-bootstrap.min.css">
<?php require 'inc/views/template_head_end.php'; ?>
<?php
if($search_type == "1"){
	$fav_sql = "select team_code from team_data where team_1_name LIKE '%$search_name%' or team_2_name LIKE '%$search_name%'";
	$fav_result = sql_query($fav_sql);
}
else if($search_type == "2"){
	$fav_sql = "select * from match_data where app_visible = '1' and wr_name LIKE '%$search_name%'";
	$fav_result = sql_query($fav_sql);
}
$search_count = 0;
if($search_type == "1"){
	while($fav = sql_fetch_array($fav_result)){
		$game_sql = "select * from game_score_data where team_1_code = '$fav[team_code]' or team_2_code = '$fav[team_code]'";
		$game_result = sql_query($game_sql);
		while($game = sql_fetch_array($game_result)){
			$match_sql = "select * from match_data where code = '$game[match_code]'";
			$match_result = sql_query($match_sql);
			$match = sql_fetch_array($match_result);
			if($match['app_visible'] == '1'){
				$search_count++;	
			}
					
		}
	}	
}
if($search_type == "2"){
	while($fav = sql_fetch_array($fav_result)){
		$search_count++;		
	}	
}
?>

<!-- end 현재 위치 -->
	
	<!-- Contents Area -->
	<div class="content">
	
		<!-- <div class="btn-group btn-group-justified push-10">
			 <?php
				if($search_type == "1"){
			?>
				<div class="btn-group">
		            <button class="btn btn-lg btn-white active" type="button">이름</button>
		        </div>
		        <div class="btn-group">
		            <button class="btn btn-lg btn-white" type="button">대회명</button>
		        </div>
			<?php		
				}else{
			?>
				<div class="btn-group">
		            <button class="btn btn-lg btn-white " type="button">이름</button>
		        </div>
		        <div class="btn-group">
		            <button class="btn btn-lg btn-white active" type="button">대회명</button>
		        </div>
			<?php		
				}
			?>
	        
		</div> -->
	    
	    <!-- 검색 결과 -->
	    <div class="text-muted push-10">
	    	<span class="font-w700 text-base"><?=$search_name;?></span>(으)로 검색한 결과 <span class="font-w700 text-base"><?=$search_count;?>개</span> 검색됨
	    </div>
	    <!-- end 결과 -->
	    
	    <!-- 접수현황 -->
	    <?php
			if($search_type == "1"){
				$fav_sql = "select * from team_data where team_1_name LIKE '%$search_name%' or team_2_name LIKE '%$search_name%'";
				$fav_result = sql_query($fav_sql);
			
		?>
	    <div class="row">
	    	<div class="col-lg-6">
	        	<div class="block">
			        <div class="block-content_">
			            <table class="js-table-sections table table-mobile">
			                <!-- 그룹 경기 조회 타이틀 -->
			                <tbody class="js-table-sections-header open">
			                    <tr>
			                        <td class="font-w600" width="85%">중계예정</td>
			                        <td class="text-center">
			                            <i class="fa fa-angle-right"></i>
			                        </td>
			                    </tr>
			                </tbody>
			                <!-- end 그룹 경기 조회 타이틀 -->
			                <tbody>
			                    <!-- 참가 선수 정보 및 대진 점수 -->
			                    <tr>
			                    	<td colspan="2" class="text-center">
			                    		<div class="row tournament_match">
			                    			<?php
													
											while($fav = sql_fetch_array($fav_result)){
												if($search_type == "1"){
				                    				$game_sql = "select * from game_score_data where team_1_code = '$fav[team_code]' or team_2_code = '$fav[team_code]'";
													$game_result = sql_query($game_sql);
													
												}	
												while($game = sql_fetch_array($game_result)){
													$sql_team1 = "select * from team_data where team_code = '$game[team_1_code]'";
													$team1_result = sql_query($sql_team1);
													$team1 = sql_fetch_array($team1_result);
													
													$sql_team2 = "select * from team_data where team_code = '$game[team_2_code]'";
													$team2_result = sql_query($sql_team2);
													$team2 = sql_fetch_array($team2_result);
													
													$sql_gym = "select * from gym_data where wr_id = '$game[gym_code]'";
													$gym_result = sql_query($sql_gym);
													$gym = sql_fetch_array($gym_result);
													
													$sql_match = "select * from match_data where app_visible = '1' and code = '$game[match_code]'";
													$match_result = sql_query($sql_match);
													$match = sql_fetch_array($match_result);
													if($match['app_visible'] == '1'){
												?>
												
			                    			<!-- 참가선수 정보 -->
			                    			<div class="col-xs-12 col-sm-6 col-md-6">
									            <div class="block block-rounded">
									                <div class="block-options push-10-t push-10-r">
										                <!-- <li>
										                    <a href="">이동</a>
										                </li> -->
										            </div>
									                <div class="tournament_header">
									                    <h3 class="block-title"><?=$match['wr_name']?></h3>
									                </div>
									                <div class="tournament_content clearfix">
									                	<div class="pull-left text-center push-5-l">
															<?php
									                			$favor_sql = "select count(*) as cnt from app_user_favor where user_code = '$user_code' and match_code = '$game[match_code]' and team_code = '$game[team_1_code]'";
									                			
									                			$favor_result = sql_query($favor_sql);
																$favor = sql_fetch_array($favor_result);
																if($favor['cnt'] == 0){
															?>
															
									                    		<button class="btn_fav" onclick="favor('<?=$user_code?>','<?=$game['team_1_code']?>','<?=$game['match_code']?>')">
									                    	<?php
																}else{
															?>
																<button class="btn_fav active" onclick="favor('<?=$user_code?>','<?=$game['team_1_code']?>','<?=$game['match_code']?>')">
															<?php	
																}
									                		?>										                    	
									                			<div class="left-star">
											                		<i class="fa fa-star"></i>
											                	</div>
										                        <div class="font-w600"><?=$team1['club'];?></div>
										                        <div class=""><?=$team1['team_1_name'];?> <?=$team1['team_2_name'];?></div>
										                    </button>
									                    </div>
									                	<div class="tournament_point">
									                		
									                		<span class="left_team"><?=$game['team_1_score']?><div class="status">
									                		<?php if($game['team_1_score'] != 0){?>
									                			<!--기권-->
									                		<?php }?>
									                			<!--승리/기권/입력x--></div></span>
									                		<span>:</span>
									                		<span class="right_team"><?=$game['team_2_score']?><div class="status">
									                		<?php if($game['team_2_score'] != 0){?>
									                			<!--승리-->
									                		<?php }?><!--승리/기권/입력x--></div></span>
									                    </div>
									                    <div class="pull-right text-center push-5-r">
									                		<?php
									                			$favor_sql = "select count(*) as cnt from app_user_favor where user_code = '$user_code' and match_code = '$game[match_code]' and team_code = '$game[team_2_code]'";
									                			$favor_result = sql_query($favor_sql);
																$favor = sql_fetch_array($favor_result);
																if($favor['cnt'] == 0){
															?>
															
									                    		<button class="btn_fav" onclick="favor('<?=$user_code?>','<?=$game['team_2_code']?>','<?=$game['match_code']?>')">
									                    	<?php
																}else{
															?>
																<button class="btn_fav active" onclick="favor('<?=$user_code?>','<?=$game['team_2_code']?>','<?=$game['match_code']?>')">
															<?php	
																}
									                		?>
										                		<div class="right-star">
											                		<i class="fa fa-star"></i>
											                	</div>
										                        <div class="font-w600"><?=$team2['club'];?></div>
										                        <div class=""><?=$team2['team_1_name'];?> <?=$team2['team_2_name'];?></div>
									                        </button>
									                    </div>
									                </div>
									               	<div class="coat_tit">
									                	<span class="pull-left"><?=$gym['gym_name'];?></span> <span class="pull-right"><?=$game['game_date'];?> <?=$game['game_time'];?> <?=$game['game_court'];?>코트 <?=$game['court_array_num'];?>번 경기</span>
									                </div>
									            </div>
									            
									                
									        </div>
									        <?php		
												}
											?>
									        <!-- end 참가선수 정보 -->
											<?php } }  ?>	        
			                    		</div>
			                    	</td>
			                    </tr>
			                    <!-- end 참가 선수 정보 및 대진 점수 -->
			                    
			                </tbody>
			                
			            </table>
			        </div>
			    </div>
	        	
	        </div>
	    </div>
	    <?php } ?>
	    <?php
			if($search_type == "2"){
				$fav_sql = "select * from match_data where app_visible = '1' and wr_name LIKE '%$search_name%'";
				$fav_result = sql_query($fav_sql);
			
		?>
	    <div class="row">
	    	<div class="col-lg-6">
	        	<div class="block">
	        		
			        <div class="block-content_">
			        	<?php 
			        		if($search_type == "2"){
								$fav_sql = "select * from match_data where app_visible = '1' and  wr_name LIKE '%$search_name%'";
								$fav_result = sql_query($fav_sql);
							}	
						?>
			            <table class="js-table-sections table table-mobile">
			                <!-- 그룹 경기 조회 타이틀 -->
			                <tbody class="js-table-sections-header open">
			                    <tr>
			                        <td class="font-w600" width="85%">중계종료</td>
			                        <td class="text-center">
			                            <i class="fa fa-angle-right"></i>
			                        </td>
			                    </tr>
			                </tbody>
			                <!-- end 그룹 경기 조회 타이틀 -->
			                <tbody>
			                    <!-- 대회 정보 -->
			                    <tr>
			                    	<td colspan="2" class="text-center">
			                    		<div class="row tournament_match">
			                    			<?php
													
											while($fav = sql_fetch_array($fav_result)){
												$match_gym = "select * from match_gym_data where match_id = '$fav[code]'";
												$match_result = sql_query($match_gym);
												$match = sql_fetch_array($match_result);
												
												$gym_query = "select * from gym_data where wr_id = '$match[gym_id]'";
												$gym_result = sql_query($gym_query);
												$gym = sql_fetch_array($gym_result);
												
											?>
			                    			<div class="col-xs-12 col-sm-6 col-md-6">
									            <div class="block block-rounded">
									                <div class="block-options push-10-t push-10-r">
										                <li>
										                    <a href="championship_info.php?wr_id=<?=$fav['wr_id'];?>&user_code=<?=$user_code;?>">이동</a>
										                </li>
										            </div>
									                <div class="tournament_header">
									                    <h3 class="block-title"><?=$fav['wr_name'];?></h3>
									                </div>
									                <div class="coat_tit">
									                	<span class="pull-left"><?=$gym['gym_name']?></span> <span class="pull-right"><?=$match['championship_start_day']?> ~ <?=$match['championship_end_day']?></span>
									                </div>
									            </div>
									        </div>
									        <?php } ?>	        
			                    		</div>
			                    	</td>
			                    </tr>
			                    <!-- end 대회 정보 -->
			                    
			                </tbody>
			                
			            </table>
			        </div>
			    </div>
	        	
	        </div>
	    </div>
	    <?php } ?>
	</div>
<!-- end Contents Area -->


<?php require 'inc/views/base_footer.php'; ?>
<?php require 'inc/views/template_footer_start.php'; ?>
<script>
function favor(user_code,team_code,code){
	$.ajax({
		type: 'post',
		url:'./favorite_add.php',
		dataType:'json',
		data: {user_code:user_code,team_code:team_code,code:code},
		success:function(data){
			console.log(data);
			location.reload(true);
			// var str = '';
			// for(var sub in data){
				// console.log(sub);
				// str += '<option value='+data[sub]['series_sub']+'>'+data[sub]['series_sub']+'</option>';
			// }
			// console.log(str);
			
	   	}
    });
}

</script>

<!-- Page JS Plugins -->
<script src="<?php echo $one->assets_folder; ?>/js/plugins/select2/select2.full.min.js"></script>
<script src="<?php echo $one->assets_folder; ?>/js/plugins/datatables/jquery.dataTables.min.js"></script>


<!-- Page JS Code -->
<script src="<?php echo $one->assets_folder; ?>/js/pages/base_tables_datatables.js"></script>
<script>
    jQuery(function(){
        // Init page helpers (BS Datepicker + BS Datetimepicker + BS Colorpicker + BS Maxlength + Select2 + Masked Input + Range Sliders + Tags Inputs plugins)
        App.initHelpers(['table-tools','datepicker', 'datetimepicker', 'colorpicker', 'maxlength', 'select2', 'masked-inputs', 'rangeslider', 'tags-inputs']);
    });
</script>

<?php require 'inc/views/template_footer_end.php'; ?>

