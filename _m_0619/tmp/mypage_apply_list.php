<?php
include_once('../common.php');
if (!defined('_GNUBOARD_')) exit; // 개별 페이지 접근 불가
include_once(G5_AOS_PATH.'/head.php');
$menu_cate2 ='mypage';
$menu_cate3 ='2';
?>

<!-- sub nav -->
<?php include_once('./app_sub_nav.php'); ?>
<!-- end sub nav -->
<?php


	$compatition_list_sql = "
			SELECT * ,
			CASE
				WHEN date_format(period1, '%Y') = date_format(period2, '%Y')
				THEN concat(date_format(period1, '%Y-%m-%d'),' ~ ', date_format(period2, '%m-%d'))
				ELSE concat(date_format(period1, '%Y-%m-%d'),' ~ ', date_format(period2, '%Y-%m-%d'))
			END AS period
			FROM match_data";
	$competition_list = sql_query($compatition_list_sql);?>



<div class="sub_container">

	<!-- 접수중 대회  -->
	<section>
		<div class="hd">
			<div class="tit"><i class="flaticon-heart"></i>&nbsp;접수중 대회</div>
		</div>
		<!-- list board -->
		<div class="bo_list_ul">
			<ul id="competition_list">
				<?php while($row = sql_fetch_array($competition_list)){?>
				<li>
					<a href="<?=G5_AOS_URL?>/competition_view.php?c=<?=$row['code']?>" class="list_a">
						<div class="champ_status">
							<span class="champ_ico champ_ico6"></span>
							<span class="limit_status">접수중</span>
						</div>
						<div class="list_r_area">
							<span class="btn01">결제대기</span>
							<span class="btn02">결제완료</span>
						</div>
						<div class="subject">
							<div class="m_subject"><?=$row['wr_name']?></div>
							<div class="s_subject"><?=$row['period']?>&nbsp;&nbsp;<?=$row['opening_place']?></div>
						</div>
					</a>
				</li>
				<?php }?>
			</ul>
		</div>
		<!-- //list board -->
	</section>
	<!-- //접수중 대회  -->

	<!-- 접수 완료 대회  -->
	<!-- 접수기간이 종료 되었을때 접수한 대회는 참가 대회로 이동-->
	<section>
		<div class="hd">
			<div class="tit"><i class="flaticon-heart"></i>&nbsp;참가 대회</div>
		</div>
		<!-- list board -->
		<div class="bo_list_ul">
			<ul id="competition_list">
				<?php while($row = sql_fetch_array($competition_list)){?>
				<li>
					<a href="<?=G5_AOS_URL?>/competition_view.php?c=<?=$row['code']?>" class="list_a">
						<div class="champ_status">
							<span class="champ_ico champ_ico6"></span>
							<span class="limit_status">접수중</span>
						</div>
						<span class="list_r_area">
							<i class="flaticon-right-arrow"></i>
						</span>
						<div class="subject">
							<div class="m_subject"><?=$row['wr_name']?></div>
							<div class="s_subject"><?=$row['period']?>&nbsp;&nbsp;<?=$row['opening_place']?></div>
						</div>
					</a>
				</li>
				<?php }?>
			</ul>
			<div class="quick_view">
				<a href="" id="more">
					더보기
				</a>
			</div>
		</div>
		<!-- //list board -->
	</section>
	<!-- //접수 완료 대회  -->

</div>
<!-- champ_list -->

<script>

	var more = {
		index : 0
		,limit : 15
		,request:function(event){
			event.preventDefault();
	        $.ajax({
	          url:g5_url+'/m/test.php'
	          ,type:'post'
						,data:{index:more.index}
	          ,dataType:'json'
	          ,cache:false
	          ,success:more.append})
	          }
	,append:function(data){
	  var _html = '';
	  for(var i in data){
			_html += '<li>'
			_html += '<a href="./competition_view.php?c='+data[i].code+'" class="list_a">'
			_html += '<div class="champ_status">'
			_html += '<span class="champ_ico champ_ico6"></span>'
			_html += '<span class="limit_status">접수중</span>'
			_html += '</div>'
			_html += '<span class="list_r_area">'
			_html += '<i class="flaticon-right-arrow"></i>'
			_html += '</span>'
			_html += '<div class="subject">'
			_html += '<div class="m_subject">'+data[i].wr_name+'</div>'
			_html += '<div class="s_subject">'+data[i].period+'&nbsp;&nbsp;'+data[i].opening_place+'</div>'
			_html += '</div>'
			_html += '</a>'
			_html += '</li>'
	  }
	  if(data.length<more.limit){
	    _html +='<li>목록이 더 없습니다.</li>';
	    $('#more').hide();
	  }
	  $('#competition_list').append(_html);
		more.index ++;
	}}

	$('#more').click(more.request);
</script>
<?php
include_once(G5_AOS_PATH.'/tail.php');
?>
