<?php
/**
 * config.php
 *
 * Author: pixelcave
 *
 * Global configuration file
 *
 */

// Include Template class
require 'classes/Template.php';

// Create a new Template Object
$one                               = new Template('CockPlay', '1.0', 'assets'); // Name, version and assets folder's name

// Global Meta Data
$one->author                       = 'CockPlay';
$one->robots                       = 'noindex, nofollow';
$one->title                        = '대진표 솔루션';
$one->description                  = '대진표 솔루션';

// Global Included Files (eg useful for adding different sidebars or headers per page)
$one->inc_side_overlay             = 'base_side_overlay.php';
$one->inc_sidebar                  = 'base_sidebar.php';
$one->inc_header                   = 'base_header.php';

// Global Color Theme
$one->theme                        = 'ios';       // '' for default theme or 'amethyst', 'city', 'flat', 'modern', 'smooth'

// Global Cookies
$one->cookies                      = false;    // True: Remembers active color theme between pages (when set through color theme list), False: Disables cookies

// Global Body Background Image
$one->body_bg                      = '';       // eg 'assets/img/photos/photo10@2x.jpg' Useful for login/lockscreen pages

// Global Header Options
$one->l_header_fixed               = true;     // True: Fixed Header, False: Static Header

// Global Sidebar Options
$one->l_sidebar_position           = 'left';   // 'left': Left Sidebar and right Side Overlay, 'right;: Flipped position
$one->l_sidebar_mini               = true;    // True: Mini Sidebar Mode (> 991px), False: Disable mini mode
$one->l_sidebar_visible_desktop    = true;     // True: Visible Sidebar (> 991px), False: Hidden Sidebar (> 991px)
$one->l_sidebar_visible_mobile     = false;    // True: Visible Sidebar (< 992px), False: Hidden Sidebar (< 992px)

// Global Side Overlay Options
$one->l_side_overlay_hoverable     = false;    // True: Side Overlay hover mode (> 991px), False: Disable hover mode
$one->l_side_overlay_visible       = false;    // True: Visible Side Overlay, False: Hidden Side Overlay

// Global Sidebar and Side Overlay Custom Scrolling
$one->l_side_scroll                = true;     // True: Enable custom scrolling (> 991px), False: Disable it (native scrolling)

// Global Active Page (it will get compared with the url of each menu link to make the link active and set up main menu accordingly)
$one->main_nav_active              = basename($_SERVER['PHP_SELF']);

// Global Main Menu
$one->main_nav                     = array(
    
    /*cock 추가*/
	array(
        'name'  => '<span class="sidebar-mini-hide">Home</span>',
        'icon'  => 'si si-speedometer',
        'url'   => 'championship_list.php'
    ),
    array(
        'name'  => '<span class="sidebar-mini-hide">대회정보</span>',
        'type'  => 'heading'
    ),
    array(
        'name'  => '<span class="sidebar-mini-hide">대회진행정보</span>',
        'icon'  => 'si si-rocket',
        'url'   => 'championship_list.php'
    ),
    array(
        'name'  => '<span class="sidebar-mini-hide">대회결과</span>',
        'icon'  => 'si si-rocket',
        'url'   => 'gym_management.php'
    ),
    array(
        'name'  => '<span class="sidebar-mini-hide">관심경기</span>',
        'icon'  => 'si si-rocket',
        'url'   => 'gym_management.php'
    ),
	array(
        'name'  => '<span class="sidebar-mini-hide">Cock</span>',
        'type'  => 'heading'
    ),
    array(
        'name'  => '<span class="sidebar-mini-hide">공지사항</span>',
        'icon'  => 'si si-rocket',
        'url'   => 'manager_list.php'
    ),
	array(
        'name'  => '<span class="sidebar-mini-hide">지인에게 알리기</span>',
        'icon'  => 'si si-rocket',
        'url'   => 'cock_analytics.php'
    ),
);