<?php
/**
 * base_footer.php
 *
 * Author: pixelcave
 *
 * The footer of each page (Backend)
 *
 */
?>

<style>
/*푸터 뒤로가기*/
.footer_btn_area{height:70px;}
.footer_btn_area ul{position:fixed;bottom:0;right:0;list-style:none;z-index:999;}
.footer_btn_area ul li{float:left}
.footer_btn{margin:0 5px;width:42px;height:42px;display:inline-block;}
</style>
<div class="footer_btn_area">
	<ul>
		<li>
			<a href="<?php G5_URL ?>/m/index.php?wr_id=<?=$wr_id?>&user_code=<?=$user_code;?>&user_club=<?=$user_club;?>&user_name=<?=$user_name;?>" onclick="" class="footer_btn">
				<img class="img-responsive" src="<?php G5_URL?>/m/assets/img/home_icon.png" />
			</a>
		</li>
		<li>
			<a href="javascript:history.go(0)" onclick="" class="footer_btn">
				<img class="img-responsive" src="<?php G5_URL?>/m/assets/img/refresh_icon.png" />
			</a>
		</li>
		<li>
			<a href="javascript:history.go(-1)" class="footer_btn">
				<img class="img-responsive" src="<?php G5_URL?>/m/assets/img/back_icon.png" />
			</a>		
		</li>
	</ul>
</div>

    </main>
    <!-- END Main Container -->
</div>
<!-- END Page Container -->

<script type="text/javascript" src="http://wcs.naver.net/wcslog.js"></script>
<script type="text/javascript">
if(!wcs_add) var wcs_add = {};
wcs_add["wa"] = "806aaead95de6c";
wcs_do();
</script>

