<?php 

function android_push($pushtoken,$message,$title,$push){
	$andorid_array = array();
	$andorid_array[] = $pushtoken;
		
	// URL to POST to
	$gcm_url = 'https://android.googleapis.com/gcm/send';
	
	// data to be posted
	$fields = array(
		'registration_ids'  => $andorid_array,
		'data'              => array( "message" => $message,"title" => $title, "push" => "$push" ),
	);
	
	// headers for the request
	$headers = array( 
	    'Authorization:key=AIzaSyBSwFTUalt1RBrfITM0MbyQ5Ac3Q21YVGc',
	    'Content-Type: application/json'
	);
	
	$curl_handle = curl_init();
	
	// set CURL optionsa
	curl_setopt($curl_handle, CURLOPT_URL, 'https://android.googleapis.com/gcm/send');
	curl_setopt($curl_handle, CURLOPT_HTTPHEADER,  $headers);
	curl_setopt($curl_handle, CURLOPT_POST,    true);
	curl_setopt($curl_handle, CURLOPT_RETURNTRANSFER, true);
	curl_setopt($curl_handle, CURLOPT_SSL_VERIFYPEER, false);
	curl_setopt($curl_handle, CURLOPT_POSTFIELDS,json_encode($fields));
	$response = curl_exec($curl_handle);
	
	curl_close($curl_handle);
}

function ios_push($deviceToken,$message,$push){
	// 개발용
	// $apnsHost = 'gateway.sandbox.push.apple.com';
	// $apnsCert = 'apns-dev.pem';
	 
	// 실서비스용
	$apnsHost = 'gateway.push.apple.com';
	$apnsCert = 'apns.pem';
	 
	$apnsPort = 2195;
	 
	$payload = array('aps' => array('alert' => $message, 'badge' => 0, 'sound' => 'default', 'push' => $push));
	$payload = json_encode($payload);
	 
	$streamContext = stream_context_create();
	stream_context_set_option($streamContext, 'ssl', 'local_cert', $apnsCert);
	 
	$apns = stream_socket_client('ssl://'.$apnsHost.':'.$apnsPort, $error, $errorString, 2, STREAM_CLIENT_CONNECT, $streamContext);
	 
	if($apns)
	{
	  $apnsMessage = chr(0).chr(0).chr(32).pack('H*', str_replace(' ', '', $deviceToken)).chr(0).chr(strlen($payload)).$payload;
	  fwrite($apns, $apnsMessage);
	  fclose($apns);
	}
}

function seed_push($wr_id,$match_code){
	$group_code_sql  = "select * from game_score_data where wr_id = '$wr_id' and match_code = '$match_code'";
	$group_code_result = sql_query($group_code_sql);
	$group_code = sql_fetch_array($group_code_result);
	
	/**
	 * 퓌쉬전송 시작
	 */
	 
	//관심 경기 푸쉬 전송 이전 경기가 시작되면 푸쉬를 전송한다.
	$three_cournt = $group_code['court_array_num']+3;
	$two_cournt = $group_code['court_array_num']+2;
	$one_cournt = $group_code['court_array_num']+1;
	$gym_code = $group_code['gym_code'];
	$game_date = $group_code['game_date'];
	$game_court = $group_code['game_court'];
	$court_array_num = $group_code['court_array_num'];
	
	$three_push_sql = "select * from game_score_data where match_code = '$match_code' and game_court = '$game_court' and court_array_num = '$one_cournt' and gym_code = '$gym_code' and game_date = '$game_date'";
	
	$three_push_result = sql_query($three_push_sql);
	$three_push_code = sql_fetch_array($three_push_result);
	
	$ios_array = array();
	$andorid_array = array();
	
	$team_1_code = $three_push_code['team_1_code'];
	$team_2_code = $three_push_code['team_2_code'];
	$game_time 	 = $three_push_code['game_time'];
	$push_court  = $three_push_code['game_court'];
	
	//아이폰 푸쉬전송
	
	// ";
	$push_user_sqls = "select * from app_user_favor where match_code = '$match_code' and ( team_code = '$team_1_code' or team_code = '$team_2_code' )";
	
	$push_user_result = sql_query($push_user_sqls);
	while($push_user_code = sql_fetch_array($push_user_result)){
		$users_code  = $push_user_code['user_code'];
		
		$team_sql = "select * from team_data where match_code = '$match_code' and team_code = '$push_user_code[team_code]'";
		$team_result = sql_query($team_sql);
		$team = sql_fetch_array($team_result);
		
		$user_sql = "select * from app_user_token where user_code = '$users_code'";
		
		
		$user_result = sql_query($user_sql);
		while($code = sql_fetch_array($user_result)){
			if($code['device'] == "iphone"){
				$ios_array = $code['pushtoken'];
				//아이폰 푸쉬 전송
				$deviceToken = $ios_array; // 디바이스토큰ID
				$message = "$team[club]"."클럽 $team[team_1_name]"."/"."$team[team_2_name]"."팀의 경기가  $push_court"."코트에서 시작됩니다.";		
				ios_push($ios_array,$message,1);
				
			}
			if($code['device'] == "android"){
				$title = "콕선생 관심경기 알림";
				$message = "$team[club]"."클럽 $team[team_1_name]"."/"."$team[team_2_name]"."팀의 경기가  $push_court"."코트에서 시작됩니다.";		
				android_push($code['pushtoken'],$title,$message,1);
			}
		}
	}
	
	//나의 경기 푸쉬 전송
	//-3경기시 푸쉬전송
	//아이폰 푸쉬전송
	$two_push_sql = "select * from game_score_data where match_code = '$match_code' and game_court = '$game_court' and court_array_num = '$three_cournt' and gym_code = '$gym_code' and game_date = '$game_date'";
	$two_push_result = sql_query($two_push_sql);
	$two_push_code = sql_fetch_array($two_push_result);
	
	$ios_array = array();
	$andorid_array = array();
	
	$team_1_code = $two_push_code['team_1_code'];
	$team_2_code = $two_push_code['team_2_code'];
	$game_court  = $two_push_code['game_court'];
	
	$sql_team1 = "select * from team_data where team_code = '$team_1_code' and match_code = '$match_code'";
	$team1_result = sql_query($sql_team1);
	$team1 = sql_fetch_array($team1_result);
	
	$sql_team2 = "select * from team_data where team_code = '$team_2_code' and match_code = '$match_code'";
	$team2_result = sql_query($sql_team2);
	$team2 = sql_fetch_array($team2_result);
	
	$user_sql = "select * from app_user_token where user_club = '$team1[club]' and user_club != '' and user_name = '$team1[team_1_name]' and user_name != ''";
	$user_result = sql_query($user_sql);
	
	while($code = sql_fetch_array($user_result)){
		if($code['device'] == "iphone"){
			$ios_array = $code['pushtoken'];
			//아이폰 푸쉬 전송
			$deviceToken = $ios_array; // 디바이스토큰ID
			$message = "$team1[team_1_name]님의 경기가 2경기 남았습니다. $game_court"."번 코트에서 준비해주시기 바랍니다.";		
			ios_push($ios_array,$message,1);
			
		}
		if($code['device'] == "android"){
			$message = "$team1[team_1_name]님의 경기가 2경기 남았습니다. $game_court"."번 코트에서 준비해주시기 바랍니다.";		
			$title = "콕선생 나의경기 알림";
			android_push($code['pushtoken'],$title,$message,1);
			// URL to POST to
		}
	}
	
	$user_sql = "select * from app_user_token where user_club = '$team1[club]' and user_club != '' and user_name = '$team1[team_2_name]' and user_name != ''";
	$user_result = sql_query($user_sql);
	
	while($code = sql_fetch_array($user_result)){
		if($code['device'] == "iphone"){
			$ios_array = $code['pushtoken'];
			//아이폰 푸쉬 전송
			$deviceToken = $ios_array; // 디바이스토큰ID
			$message = "$team1[team_2_name]님의 경기가 2경기 남았습니다. $game_court"."번 코트에서 준비해주시기 바랍니다.";		
			ios_push($ios_array,$message,1);
		}
		if($code['device'] == "android"){		
			$message = "$team1[team_2_name]님의 경기가 2경기 남았습니다. $game_court"."번 코트에서 준비해주시기 바랍니다.";		
			$title = "콕선생 나의경기 알림";
			android_push($code['pushtoken'],$title,$message,1);
		}
	}
	
	$user_sql = "select * from app_user_token where user_club = '$team2[club]' and user_club != '' and user_name = '$team2[team_1_name]' and user_name != ''";
	$user_result = sql_query($user_sql);
	while($code = sql_fetch_array($user_result)){
		if($code['device'] == "iphone"){
			$ios_array = $code['pushtoken'];
			//아이폰 푸쉬 전송
			$deviceToken = $ios_array; // 디바이스토큰ID
			$message = "$team2[team_1_name]님의 경기가 2경기 남았습니다. $game_court"."번 코트에서 준비해주시기 바랍니다.";		
			ios_push($ios_array,$message,1);
			
		}
		if($code['device'] == "android"){		
			$message = "$team2[team_1_name]님의 경기가 2경기 남았습니다. $game_court"."번 코트에서 준비해주시기 바랍니다.";		
			$title = "콕선생 나의경기 알림";
			android_push($code['pushtoken'],$title,$message,1);
	
		}
	}
	
	$user_sql = "select * from app_user_token where user_club = '$team2[club]'  and user_club != '' and user_name = '$team2[team_2_name]'  and user_name != ''";
	$user_result = sql_query($user_sql);
	while($code = sql_fetch_array($user_result)){
		if($code['device'] == "iphone"){
			$ios_array = $code['pushtoken'];
			//아이폰 푸쉬 전송
			$deviceToken = $ios_array; // 디바이스토큰ID
			$message = "$team2[team_2_name]님의 경기가 2경기 남았습니다. $game_court"."번 코트에서 준비해주시기 바랍니다.";		
			ios_push($ios_array,$message,1);
		}
		if($code['device'] == "android"){		
			$message = "$team2[team_2_name]님의 경기가 2경기 남았습니다. $game_court"."번 코트에서 준비해주시기 바랍니다.";		
			$title = "콕선생 나의경기 알림";
			android_push($code['pushtoken'],$title,$message,1);
			
		}
	}
	
	
	//-3경기시 푸쉬전송
	//아이폰 푸쉬전송
	$two_push_sql = "select * from game_score_data where match_code = '$match_code' and game_court = '$game_court' and court_array_num = '$two_cournt' and gym_code = '$gym_code' and game_date = '$game_date'";
	$two_push_result = sql_query($two_push_sql);
	$two_push_code = sql_fetch_array($two_push_result);
	
	$ios_array = array();
	$andorid_array = array();
	
	$team_1_code = $two_push_code['team_1_code'];
	$team_2_code = $two_push_code['team_2_code'];
	$game_court  = $two_push_code['game_court'];
	
	$sql_team1 = "select * from team_data where team_code = '$team_1_code' and match_code = '$match_code'";
	$team1_result = sql_query($sql_team1);
	$team1 = sql_fetch_array($team1_result);
	
	$sql_team2 = "select * from team_data where team_code = '$team_2_code' and match_code = '$match_code'";
	$team2_result = sql_query($sql_team2);
	$team2 = sql_fetch_array($team2_result);
	
	$user_sql = "select * from app_user_token where user_club = '$team1[club]' and user_club != '' and user_name = '$team1[team_1_name]' and user_name != ''";
	$user_result = sql_query($user_sql);
	
	while($code = sql_fetch_array($user_result)){
		if($code['device'] == "iphone"){
			$ios_array = $code['pushtoken'];
			//아이폰 푸쉬 전송
			$deviceToken = $ios_array; // 디바이스토큰ID
			$message = "$team1[team_1_name]님의 경기가 20분 이내에 시작합니다. $game_court"."번 코트에서 준비해주시기 바랍니다.";		
			ios_push($ios_array,$message,1);
			
		}
		if($code['device'] == "android"){		
			$message = "$team1[team_1_name]님의 경기가 20분 이내에 시작합니다. $game_court"."번 코트에서 준비해주시기 바랍니다.";		
			$title = "콕선생 나의경기 알림";
			android_push($code['pushtoken'],$title,$message,1);
		}
	}
	
	$user_sql = "select * from app_user_token where user_club = '$team1[club]' and user_club != '' and user_name = '$team1[team_2_name]' and user_name != ''";
	$user_result = sql_query($user_sql);
	
	while($code = sql_fetch_array($user_result)){
		if($code['device'] == "iphone"){
			$ios_array = $code['pushtoken'];
			//아이폰 푸쉬 전송
			$deviceToken = $ios_array; // 디바이스토큰ID
			$message = "$team1[team_2_name]님의 경기가 20분 이내에 시작합니다. $game_court"."번 코트에서 준비해주시기 바랍니다.";		
			ios_push($ios_array,$message,1);
			
		}
		if($code['device'] == "android"){		
			$message = "$team1[team_2_name]님의 경기가 20분 이내에 시작합니다. $game_court"."번 코트에서 준비해주시기 바랍니다.";		
			$title = "콕선생 나의경기 알림";
			android_push($code['pushtoken'],$title,$message,1);
		}
	}
	
	$user_sql = "select * from app_user_token where user_club = '$team2[club]' and user_club != '' and user_name = '$team2[team_1_name]' and user_name != ''";
	$user_result = sql_query($user_sql);
	while($code = sql_fetch_array($user_result)){
		if($code['device'] == "iphone"){
			$ios_array = $code['pushtoken'];
			//아이폰 푸쉬 전송
			$deviceToken = $ios_array; // 디바이스토큰ID
			$message = "$team2[team_1_name]님의 경기가 20분 이내에 시작합니다. $game_court"."번 코트에서 준비해주시기 바랍니다.";		
			ios_push($ios_array,$message,1);
		}
		if($code['device'] == "android"){
			$andorid_array = array();
			$andorid_array[] = $code['pushtoken'];
			
			$message = "$team2[team_1_name]님의 경기가 20분 이내에 시작합니다. $game_court"."번 코트에서 준비해주시기 바랍니다.";		
			$title = "콕선생 나의경기 알림";
			android_push($code['pushtoken'],$title,$message,1);
		}
	}
	
	$user_sql = "select * from app_user_token where user_club = '$team2[club]' and user_club != '' and user_name = '$team2[team_2_name]' and user_name != ''";
	$user_result = sql_query($user_sql);
	while($code = sql_fetch_array($user_result)){
		if($code['device'] == "iphone"){
			$ios_array = $code['pushtoken'];
			//아이폰 푸쉬 전송
			$deviceToken = $ios_array; // 디바이스토큰ID
			$message = "$team2[team_2_name]님의 경기가 20분 이내에 시작합니다. $game_court"."번 코트에서 준비해주시기 바랍니다.";		
			 ios_push($ios_array,$message,1);
		}
		if($code['device'] == "android"){
			$andorid_array = array();
			$andorid_array[] = $code['pushtoken'];
			
			$message = "$team2[team_2_name]님의 경기가 20분 이내에 시작합니다. $game_court"."번 코트에서 준비해주시기 바랍니다.";		
			$title = "콕선생 나의경기 알림";
			android_push($code['pushtoken'],$title,$message,1);
		}
	}
}


/**
 * 퓌쉬전송 완료 수정 및 토너먼트 입력 개발
 */
?>