<?php
include_once('../common.php');

$wr_id 		= $_REQUEST['wr_id'];
$division   = $_REQUEST['division'];
$user_code	= $_REQUEST['user_code'];
$act = 'champ_info_match'
?>
<?php require 'inc/config.php'; ?>
<?php require 'inc/views/template_head_start.php'; ?>

<!-- Page JS Plugins CSS -->
<link rel="stylesheet" href="<?php echo $one->assets_folder; ?>/js/plugins/slick/slick.min.css">
<link rel="stylesheet" href="<?php echo $one->assets_folder; ?>/js/plugins/slick/slick-theme.min.css">
<link rel="stylesheet" href="<?php echo $one->assets_folder; ?>/js/plugins/select2/select2.min.css">
<link rel="stylesheet" href="<?php echo $one->assets_folder; ?>/js/plugins/select2/select2-bootstrap.min.css">
<?php require 'inc/views/template_head_end.php'; ?>

<?php 
	$sql = "select * from match_data where wr_id = '$wr_id' order by wr_id desc";
	$result = sql_query($sql);
	$i = 1;
	$r = sql_fetch_array($result);
    $code = $r['code'];
	
	$sql = "select * from match_gym_data where match_id = '$code' group by gym_id";
	$result = sql_query($sql);
	$gym_id = sql_fetch_array($result);
?>

<!-- sub nav -->
<?php include_once('./app_sub_nav.php'); ?>
<!-- end sub nav -->

<!-- Contents Area -->
	
	<div class="content" style="margin-top: 42px;">
	
		<div class="btn-group btn-group-justified push-10">
	        <div class="btn-group">
	            <a href="champ_info_match.php?wr_id=<?=$wr_id;?>&user_club=<?=$user_club;?>&user_name=<?=$user_name;?>&user_code=<?=$user_code;?>&division=<?=$division;?>"><button class="btn btn-lg btn-white active" type="button">급수별 편성표</button></a>
	        </div>
	        <div class="btn-group">
	            <a href="champ_info_match_time.php?wr_id=<?=$wr_id;?>&user_club=<?=$user_club;?>&user_name=<?=$user_name;?>&user_code=<?=$user_code;?>&gym_court=<?=$gym_id['wr_id']?>&application_period=<?=$gym_id['application_period']?>&gym_game=1"><button class="btn btn-lg btn-white" type="button">대진 시간표</button></a>
	        </div>
		</div>
		
		<div class="row">
			<div class="col-md-6">
				<!-- 1일차 기본 선택  -->
				<div class="champ-data push-10">
					<?php 
						$sql = "select * from series_data where match_code = '$code' group by division";
						$result = sql_query($sql);
						while($series = sql_fetch_array($result)){
					?>
					
					
					<a class="btn2 <?php if(strcmp(trim($division), trim($series['division'])) == "0") echo "active"; ?>" href="champ_info_match.php?wr_id=<?=$wr_id?>&user_club=<?=$user_club;?>&user_name=<?=$user_name;?>&user_code=<?=$user_code;?>&division=<?=$series['division'];?>"><?=$series['division'];?></a>
					<!-- <a class="btn2 <?php if($division == "여복") echo "active"; ?>" href="champ_info_match.php?wr_id=<?=$wr_id?>&user_code=<?=$user_code;?>&division=여복">여자 복식</a>
					<a class="btn2 <?php if($division == "혼복") echo "active"; ?>" href="champ_info_match.php?wr_id=<?=$wr_id?>&user_code=<?=$user_code;?>&division=혼복">혼합 복식</a> -->					
					<?php			
						}
					?>
				</div>
			</div>
		</div>
		
	    <div class="row">
	        <div class="col-lg-6">
	            <!-- 급수별 접수 현황 Table -->
	            <div class="block">
	                <div class="block-content remove-padding">
	                    <table class="table table-vcenter champ_result table-mobile">
	                        <thead>
	                            <tr>
	                                <th class="text-center" style="">연령</th>
	                                <th colspan="2" class="text-center" style="">급수</th>
	                            </tr>
	                        </thead>
	                        <tbody>
	                        	<?php 
									$a_sum = 0;
									$b_sum = 0;
									$c_sum = 0;
									
	                        		$series_sql = "select series from series_data where match_code = '$r[code]' and division = '$division'   group by series  order by series";
	                        		$series_result = sql_query($series_sql);
									while($series = sql_fetch_array($series_result)){
		                        		$series_sub_sql = "select series_sub from series_data where match_code = '$r[code]' and division = '$division' and series = '$series[series]'  group by series_sub  order by series_sub";
		                        		$series_sub_result = sql_query($series_sub_sql);
		                        		$i = 0;
										while($series_sub = sql_fetch_array($series_sub_result)){
										if($i == 0){
											$cnt_sql = "select count(wr_id) as cnt from series_data where match_code = '$r[code]' and division = '$division' and series = '$series[series]' group by series_sub";
			                        		$cnt_result = sql_query($cnt_sql);			                        	
				                        	$cnt = 0;
											while($sub = sql_fetch_array($cnt_result)){
												$cnt++;
											}		
												
											
									?>
										<tr class="text-center">
			                        		<td rowspan="<?=$cnt;?>"><?=$series['series'];?></td>
			                                <td><?=$series_sub['series_sub'];?></td>
			                                <td><a href="champ_info_match_view.php?wr_id=<?=$wr_id?>&user_code=<?=$user_code;?>&division=<?=$division?>&series=<?=$series['series']?>&user_club=<?=$user_club;?>&user_name=<?=$user_name;?>&series_sub=<?=$series_sub['series_sub']?>">조회</a></td>
			                                
		                            	</tr>
									<?php
										}else{
									?>
										<tr class="text-center">
			                                <td><?=$series_sub['series_sub'];?></td>
			                                <td><a href="champ_info_match_view.php?wr_id=<?=$wr_id?>&user_code=<?=$user_code;?>&division=<?=$division?>&series=<?=$series['series']?>&user_club=<?=$user_club;?>&user_name=<?=$user_name;?>&series_sub=<?=$series_sub['series_sub']?>">조회</a></td>
			                                
		                            	</tr>
									<?php
										}
										$i++;	
									?>

								<?php
										}
									}
	                        	?>
	                        </tbody>
	                    </table>
	                </div>
	            </div>
	            <!-- end 급수별 접수 현황 Table -->
	        </div>
	    </div>
	    
	</div>
<!-- end Contents Area -->


<?php require 'inc/views/base_footer.php'; ?>
<?php require 'inc/views/template_footer_start.php'; ?>



<!-- Page JS Plugins -->
<script src="<?php echo $one->assets_folder; ?>/js/plugins/select2/select2.full.min.js"></script>
<script src="<?php echo $one->assets_folder; ?>/js/plugins/datatables/jquery.dataTables.min.js"></script>


<!-- Page JS Code -->
<script src="<?php echo $one->assets_folder; ?>/js/pages/base_tables_datatables.js"></script>
<script>
    jQuery(function(){
        // Init page helpers (BS Datepicker + BS Datetimepicker + BS Colorpicker + BS Maxlength + Select2 + Masked Input + Range Sliders + Tags Inputs plugins)
        App.initHelpers(['table-tools','datepicker', 'datetimepicker', 'colorpicker', 'maxlength', 'select2', 'masked-inputs', 'rangeslider', 'tags-inputs']);
    });
</script>

<?php require 'inc/views/template_footer_end.php'; ?>

