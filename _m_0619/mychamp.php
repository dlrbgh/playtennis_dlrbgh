<?php
include_once('../common.php');

$user_code	= $_REQUEST['user_code'];
$club	= $_REQUEST['user_club'];
$name	= $_REQUEST['user_name'];

?>
<?php require 'inc/config.php'; ?>
<?php require 'inc/views/template_head_start.php'; ?>


<?php require 'inc/views/template_head_end.php'; ?>
	<!-- Contents Area -->

	<div class="content">

		<?php
			$gym_sql = "select * from team_data as a inner join match_data as b where a.club = '$club' and (a.team_1_name = '$name' or a.team_2_name = '$name') and a.match_code = b.code and b.end_game > '2'  group by code";
			$gym_result = sql_query($gym_sql);
			while($gym = sql_fetch_array($gym_result)){
		?>
	    <!-- 접수현황 -->
	    <div class="row">
	    	<div class="col-lg-6">
	        	<div class="block">
			            <table class="table table-mobile">
			                <!-- 그룹 경기 조회 타이틀 -->
			                <tbody class="js-table-sections-header open">
			                    <tr>
			                        <td class="font-w600" width="85%"><?=$gym['wr_name'];?></td>
			                    </tr>
			                </tbody>
			                <!-- end 그룹 경기 조회 타이틀 -->
			                <tbody>
			                	<?php
			                		$team_sql = "select * from team_data as a where a.club = '$club' and (a.team_1_name = '$name' or a.team_2_name = '$name') and a.match_code = '$gym[code]'";
									$team_result = sql_query($team_sql);
									while($team = sql_fetch_array($team_result)){
										$match_gym_sql = "select * from game_score_data as a inner join gym_data as b where (a.team_1_code = '$team[team_code]' or a.team_2_code = '$team[team_code]') and a.match_code = '$team[match_code]' and a.gym_code = b.wr_id group by a.game_date";
										$match_gym_result = sql_query($match_gym_sql);
										$match_gym = sql_fetch_array($match_gym_result);
			                	?>
			                    <tr>
			                    	<td class="mychamp_list">
			                    		<div class="title">
			                    			<span class=""><?=$team['division'];?> <?=$team['series'];?> <?=$team['series_sub'];?></span><span class="push-10-l">(<?=$match_gym['gym_name']?>)</span>
			                    			<span class="pull-right"><a href="champ_info_match_view.php?wr_id=<?=$gym['wr_id']?>&user_code=<?=$user_code;?>&user_club=<?=$club;?>&user_name=<?=$name;?>&division=<?=$team['division']?>&series=<?=$team['series']?>&series_sub=<?=$team['series_sub']?>">바로가기</a></span>
			                    		</div>

			                    		<div class="row tournament_match">
			                    			<!-- 참가선수 정보 -->
			                    			<?php
							            		$score_sql = "select * from game_score_data where (team_1_code = '$team[team_code]' or team_2_code = '$team[team_code]') and match_code = '$team[match_code]' order by game_increase";
												$score_result = sql_query($score_sql);
												while($score = sql_fetch_array($score_result)){

													$sql_team1 = "select * from team_data where team_code = '$score[team_1_code]'";
													$team1_result = sql_query($sql_team1);
													$team1 = sql_fetch_array($team1_result);

													$sql_team2 = "select * from team_data where team_code = '$score[team_2_code]'";
													$team2_result = sql_query($sql_team2);
													$team2 = sql_fetch_array($team2_result);
								            ?>
			                    			<div class="col-xs-12 col-sm-6 col-md-6">
									            <div class="block block-rounded">
									                <div class="block-options push-10-t push-10-r">
										                <li>
										                   <?=$score['game_date'];?>
										                </li>
										            </div>
										            <div class="tournament_header">
									                    <h3 class="block-title">
									                    	<span><?=$score['game_court'];?>코트 <?=$score['court_array_num'];?>번 경기</span>
									                    	<span class="push-10-l"><?=$score['game_time'];?></span>
								                    	   	<?php if($score['tournament'] == "T"){?>
										                    <span class="push-10-l">토너먼트<?=$score['tournament_count']*2;?>강</span>
											               	<?php }?>
											               	<?php if($score['tournament'] == "C"){?>
										                    <span class="push-10-l">결승</span>
											               	<?php }?>
											                <span class="pull-right"></span>
									                    </h3>
									                </div>
									                <div class="tournament_content clearfix">
									                	<div class="pull-left text-center push-5-l">
															    <div class="font-w600"><?=$team1['club'];?></div>
										                        <div class=""><?=$team1['team_1_name'];?> <?=$team1['team_2_name'];?></div>
										                </div>
									                	<div class="tournament_point">
									                		<span class="left_team <?php if($score['team_1_score'] > $score['team_2_score']) echo "win";?>"><?=$score['team_1_score'];?></span>
									                		<span>vs</span>
									                		<span class="right_team <?php if($score['team_1_score'] < $score['team_2_score']) echo "win";?>"><?=$score['team_2_score'];?></span>
									                    </div>
									                    <div class="pull-right text-center push-5-r">
									                		    <div class="font-w600"><?=$team2['club'];?></div>
										                        <div class=""><?=$team2['team_1_name'];?> <?=$team2['team_2_name'];?></div>
									                    </div>
									                </div>

									            </div>
									        </div>
									        <!-- end 참가선수 정보 -->
									         <?php } ?>
										</div>
			                    	</td>
			                    </tr>
			                    <?php } ?>
			                </tbody>
			            </table>
			    </div>

	        </div>
	    </div>
	<?php } ?>
	</div>
<!-- end Contents Area -->


<?php require 'inc/views/base_footer.php'; ?>
<?php require 'inc/views/template_footer_start.php'; ?>



<?php require 'inc/views/template_footer_end.php'; ?>
