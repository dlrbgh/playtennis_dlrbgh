<?php
include_once('../common.php');
if (!defined('_GNUBOARD_')) exit; // 개별 페이지 접근 불가
include_once(G5_AOS_PATH.'/head.php');
$menu_cate2 ='competition_view';
$menu_cate3 ='7';

$code = $c;

?>


<!-- sub nav -->
<?php include_once('./app_sub_nav.php'); ?>
<!-- end sub nav -->

<!-- Contents Area -->
<div class="pop_container">

<!-- -남자마스터2부 -->
	<section>
		<div class="content">
			<div class="con_tit_area clear">
				<div class="tit mb-5 fw-700">
					<?=$competition['wr_name']?> - 남자 - 마스터 2부

				</div>
				<div class="tit">
				</div>
				<div class="r-area">
					<ul>
						<li class="color5 fw-700"></li>
					</ul>
				</div>
			</div>

			<div class="tbl_style01 tbl_striped mb-20">
				<table>
					<thead>
						<tr>
							<th>순위</th>
							<th>선수</th>
							<th>선수</th>
						</tr>
					</thead>
					<tbody class="text-center">
						<tr>
							<td>우승</td>
							<td>(삼척)이정권</td>
							<td>(삼척)이성철</td>
						</tr>
						<tr>
							<td>준우승</td>
							<td>(화천)이진수</td>
							<td>(화천)정용진</td>
						</tr>
						<tr>
							<td>3위</td>
							<td>(원주)임영순</td>
							<td>(원주)위효복</td>
						</tr>
						<tr>
							<td> </td>
							<td>(정선)박상현</td>
							<td>(정선)전재만</td>
						</tr>
						
					</tbody>
				</table>
			</div>
		</div>
	</section>

<!--개인남자신인1부-->
	<section>
		<div class="content">
			<div class="con_tit_area clear">
				<div class="tit mb-5 fw-700">
					<?=$competition['wr_name']?> - 남자 - 신인1부

				</div>
				<div class="tit">
				</div>
				<div class="r-area">
					<ul>
						<li class="color5 fw-700"></li>
					</ul>
				</div>
			</div>

			<div class="tbl_style01 tbl_striped mb-20">
				<table>
					<thead>
						<tr>
							<th>순위</th>
							<th>선수</th>
							<th>선수</th>
						</tr>
					</thead>
					<tbody class="text-center">
						<tr>
							<td>우승</td>
							<td>(화천)여상훈</td>
							<td>(화천)장귀태</td>
						</tr>
						<tr>
							<td>준우승</td>
							<td>(고성)이재만</td>
							<td>(고성)어봉갑</td>
						</tr>
						<tr>
							<td>3위</td>
							<td>(양구)금오수</td>
							<td>(양구)김용대</td>
						</tr>
						<tr>
							<td> </td>
							<td>(철원)신동표</td>
							<td>(철원)이주성</td>
						</tr>
						
					</tbody>
				</table>
			</div>
		</div>
	</section>
	
<!--개인남자신인2부-->
	<section>
		<div class="content">
			<div class="con_tit_area clear">
				<div class="tit mb-5 fw-700">
					<?=$competition['wr_name']?> - 남자 - 신인 2부

				</div>
				<div class="tit">
				</div>
				<div class="r-area">
					<ul>
						<li class="color5 fw-700"></li>
					</ul>
				</div>
			</div>

			<div class="tbl_style01 tbl_striped mb-20">
				<table>
					<thead>
						<tr>
							<th>순위</th>
							<th>선수</th>
							<th>선수</th>
						</tr>
					</thead>
					<tbody class="text-center">
						<tr>
							<td>우승</td>
							<td>(화천)장시석</td>
							<td>(화천)송제승</td>
						</tr>
						<tr>
							<td>준우승</td>
							<td>(속초)최지훈</td>
							<td>(속초)조성현</td>
						</tr>
						<tr>
							<td>3위</td>
							<td>(화천)최해규</td>
							<td>(화천)이기홍</td>
						</tr>
						<tr>
							<td> </td>
							<td>조광욱</td>
							<td>한철주</td>
						</tr>
						
					</tbody>
				</table>
			</div>
		</div>
	</section>	

<!-- -남자어르신부 -->
	<section>
		<div class="content">
			<div class="con_tit_area clear">
				<div class="tit mb-5 fw-700">
					<?=$competition['wr_name']?> - 남자 - 어르신부

				</div>
				<div class="tit">
				</div>
				<div class="r-area">
					<ul>
						<li class="color5 fw-700"></li>
					</ul>
				</div>
			</div>

			<div class="tbl_style01 tbl_striped mb-20">
				<table>
					<thead>
						<tr>
							<th>순위</th>
							<th>선수</th>
							<th>선수</th>
						</tr>
					</thead>
					<tbody class="text-center">
						<tr>
							<td>우승</td>
							<td>(춘천)이동우</td>
							<td>(춘천)전영환</td>
						</tr>
						<tr>
							<td>준우승</td>
							<td>(속초)유세현</td>
							<td>(속초)정익섭</td>
						</tr>
						<tr>
							<td>3위</td>
							<td>(춘천)김국진</td>
							<td>(춘천)이연식</td>
						</tr>
						<tr>
							<td> </td>
							<td>(속초)심석건</td>
							<td>(속초)최임식</td>
						</tr>
						
					</tbody>
				</table>
			</div>
		</div>
	</section>

<!--개인오픈부-->
	<section>
		<div class="content">
			<div class="con_tit_area clear">
				<div class="tit mb-5 fw-700">
					<?=$competition['wr_name']?> - 남자 - 오픈부

				</div>
				<div class="tit">
				</div>
				<div class="r-area">
					<ul>
						<li class="color5 fw-700"></li>
					</ul>
				</div>
			</div>

			<div class="tbl_style01 tbl_striped mb-20">
				<table>
					<thead>
						<tr>
							<th>순위</th>
							<th>선수</th>
							<th>선수</th>
						</tr>
					</thead>
					<tbody class="text-center">
						<tr>
							<td>우승</td>
							<td>(홍천)김민준</td>
							<td>(홍천)윤충식</td>
						</tr>
						<tr>
							<td>준우승</td>
							<td>(원주)김용래</td>
							<td>(원주)남상철</td>
						</tr>
						<tr>
							<td>3위</td>
							<td>(화천)박정규</td>
							<td>(화천)윤치영</td>
						</tr>
						<tr>
							<td> </td>
							<td>(춘천)홍양석</td>
							<td>(춘천)박나원</td>
						</tr>
						
					</tbody>
				</table>
			</div>
		</div>
	</section>

<!-- -남자챌린저1부 -->
	<section>
		<div class="content">
			<div class="con_tit_area clear">
				<div class="tit mb-5 fw-700">
					<?=$competition['wr_name']?> - 남자 - 챌린저1부

				</div>
				<div class="tit">
				</div>
				<div class="r-area">
					<ul>
						<li class="color5 fw-700"></li>
					</ul>
				</div>
			</div>

			<div class="tbl_style01 tbl_striped mb-20">
				<table>
					<thead>
						<tr>
							<th>순위</th>
							<th>선수</th>
							<th>선수</th>
						</tr>
					</thead>
					<tbody class="text-center">
						<tr>
							<td>우승</td>
							<td>(철원)정승일</td>
							<td>(철원)김양현</td>
						</tr>
						<tr>
							<td>준우승</td>
							<td>(속초)강창희</td>
							<td>(속초)이강명</td>
						</tr>
						<tr>
							<td>3위</td>
							<td></td>
							<td></td>
						</tr>
						<tr>
							<td> </td>
							<td></td>
							<td></td>
						</tr>
						
					</tbody>
				</table>
			</div>
		</div>
	</section>

<!--개인남자챌린저2부-->
	<section>
		<div class="content">
			<div class="con_tit_area clear">
				<div class="tit mb-5 fw-700">
					<?=$competition['wr_name']?> - 남자 - 챌린저2부

				</div>
				<div class="tit">
				</div>
				<div class="r-area">
					<ul>
						<li class="color5 fw-700"></li>
					</ul>
				</div>
			</div>

			<div class="tbl_style01 tbl_striped mb-20">
				<table>
					<thead>
						<tr>
							<th>순위</th>
							<th>선수</th>
							<th>선수</th>
						</tr>
					</thead>
					<tbody class="text-center">
						<tr>
							<td>우승</td>
							<td>(인제)전성주</td>
							<td>(인제)최창수</td>
						</tr>
						<tr>
							<td>준우승</td>
							<td>(화천)윤병대</td>
							<td>(화천)이선달</td>
						</tr>
						<tr>
							<td>3위</td>
							<td>(춘천)송영한</td>
							<td>(춘천)안홍익</td>
						</tr>
						<tr>
							<td> </td>
							<td>(정선)이규진</td>
							<td>(정선)최종주</td>
						</tr>
						
					</tbody>
				</table>
			</div>
		</div>
	</section>

<!-- 여자 마스터부-->
	<section>
		<div class="content">
			<div class="con_tit_area clear">
				<div class="tit mb-5 fw-700">
					<?=$competition['wr_name']?> - 여자 - 마스터부
				</div>
				<div class="tit">
				</div>
				<div class="r-area">
					<ul>
						<li class="color5 fw-700"></li>
					</ul>
				</div>
			</div>

			<div class="tbl_style01 tbl_striped mb-20">
				<table>
					<thead>
						<tr>
							<th>순위</th>
							<th>선수</th>
							<th>선수</th>
						</tr>
					</thead>
					<tbody class="text-center">
						<tr>
							<td>우승</td>
							<td>(화천)채희금</td>
							<td>(양구)현현숙</td>
						</tr>
						<tr>
							<td>준우승</td>
							<td>(원주)신금순</td>
							<td>(원주)허찬화</td>
						</tr>
						<tr>
							<td>3위</td>
							<td>(홍천)황경옥</td>
							<td>(홍천)김달옥</td>
						</tr>
						<tr>
							<td> </td>
							<td>(속초)권성란</td>
							<td>(속초)김유진</td>
						</tr>
						
					</tbody>
				</table>
			</div>
		</div>
	</section>

<!-- 여자 신인1부-->
	<section>
		<div class="content">
			<div class="con_tit_area clear">
				<div class="tit mb-5 fw-700">
					<?=$competition['wr_name']?> - 여자 - 신인1부
				</div>
				<div class="tit">
				</div>
				<div class="r-area">
					<ul>
						<li class="color5 fw-700"></li>
					</ul>
				</div>
			</div>

			<div class="tbl_style01 tbl_striped mb-20">
				<table>
					<thead>
						<tr>
							<th>순위</th>
							<th>선수</th>
							<th>선수</th>
						</tr>
					</thead>
					<tbody class="text-center">
						<tr>
							<td>우승</td>
							<td>(홍천)이암숙</td>
							<td>(홍천)권명예</td>
						</tr>
						<tr>
							<td>준우승</td>
							<td>(고성)김미경</td>
							<td>(고성)노은숙</td>
						</tr>
						<tr>
							<td>3위</td>
							<td>(화천)정기화</td>
							<td>(화천)이기정</td>
						</tr>
						<tr>
							<td> </td>
							<td>(원주)김숙진</td>
							<td>(원주)진명숙</td>
						</tr>
						
					</tbody>
				</table>
			</div>
		</div>
	</section>

<!-- 여자 신인2부-->
	<section>
		<div class="content">
			<div class="con_tit_area clear">
				<div class="tit mb-5 fw-700">
					<?=$competition['wr_name']?> - 여자 - 신인2부
				</div>
				<div class="tit">
				</div>
				<div class="r-area">
					<ul>
						<li class="color5 fw-700"></li>
					</ul>
				</div>
			</div>

			<div class="tbl_style01 tbl_striped mb-20">
				<table>
					<thead>
						<tr>
							<th>순위</th>
							<th>선수</th>
							<th>선수</th>
						</tr>
					</thead>
					<tbody class="text-center">
						<tr>
							<td>우승</td>
							<td>(고성)김혜경</td>
							<td>(고성)송은주</td>
						</tr>
						<tr>
							<td>준우승</td>
							<td>(원주)채옥경</td>
							<td>(원주)정명옥</td>
						</tr>
						<tr>
							<td>3위</td>
							<td>(삼척)김애희</td>
							<td>(삼척)김은진</td>
						</tr>
						<tr>
							<td> </td>
							<td>(정선)정춘희</td>
							<td>(정선)박정숙</td>
						</tr>
						
					</tbody>
				</table>
			</div>
		</div>
	</section>

<!-- 여자 챌린저1부-->
	<section>
		<div class="content">
			<div class="con_tit_area clear">
				<div class="tit mb-5 fw-700">
					<?=$competition['wr_name']?> - 여자 - 챌린저 1부
				</div>
				<div class="tit">
				</div>
				<div class="r-area">
					<ul>
						<li class="color5 fw-700"></li>
					</ul>
				</div>
			</div>

			<div class="tbl_style01 tbl_striped mb-20">
				<table>
					<thead>
						<tr>
							<th>순위</th>
							<th>선수</th>
							<th>선수</th>
						</tr>
					</thead>
					<tbody class="text-center">
						<tr>
							<td>우승</td>
							<td>(원주)김애숙</td>
							<td>(원주)박현숙</td>
						</tr>
						<tr>
							<td>준우승</td>
							<td>(속초)정순희</td>
							<td>(속초)김미자</td>
						</tr>
						<tr>
							<td>3위</td>
							<td></td>
							<td></td>
						</tr>
						<tr>
							<td> </td>
							<td></td>
							<td></td>
						</tr>
						
					</tbody>
				</table>
			</div>
		</div>
	</section>

<!-- 여자 챌린저2부-->
	<section>
		<div class="content">
			<div class="con_tit_area clear">
				<div class="tit mb-5 fw-700">
					<?=$competition['wr_name']?> - 여자 - 챌린저 2부
				</div>
				<div class="tit">
				</div>
				<div class="r-area">
					<ul>
						<li class="color5 fw-700"></li>
					</ul>
				</div>
			</div>

			<div class="tbl_style01 tbl_striped mb-20">
				<table>
					<thead>
						<tr>
							<th>순위</th>
							<th>선수</th>
							<th>선수</th>
						</tr>
					</thead>
					<tbody class="text-center">
						<tr>
							<td>우승</td>
							<td>(화천)박금자</td>
							<td>(화천)윤경숙</td>
						</tr>
						<tr>
							<td>준우승</td>
							<td>(정선)김옥춘</td>
							<td>(정선)홍미라</td>
						</tr>
						<tr>
							<td>3위</td>
							<td></td>
							<td></td>
						</tr>
						<tr>
							<td> </td>
							<td></td>
							<td></td>
						</tr>
						
					</tbody>
				</table>
			</div>
		</div>
	</section>	

<!-- end Contents Area -->

<?php
include_once(G5_AOS_PATH.'/tail.php');
?>
