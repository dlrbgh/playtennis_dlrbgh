<?php
include_once('../common.php');

$wr_id = $_REQUEST['wr_id'];
$gym_court = $_REQUEST['gym_court'];
$gym_game = $_REQUEST['gym_game'];
$user_code	= $_REQUEST['user_code'];

$time_court = $_REQUEST['time_court'];
$time_game = $_REQUEST['time_game'];
$act = 'champ_info_match'
?>
<?php require 'inc/config.php'; ?>
<?php require 'inc/views/template_head_start.php'; ?>

<!-- Page JS Plugins CSS -->
<link rel="stylesheet" href="<?php echo $one->assets_folder; ?>/js/plugins/slick/slick.min.css">
<link rel="stylesheet" href="<?php echo $one->assets_folder; ?>/js/plugins/slick/slick-theme.min.css">
<link rel="stylesheet" href="<?php echo $one->assets_folder; ?>/js/plugins/select2/select2.min.css">
<link rel="stylesheet" href="<?php echo $one->assets_folder; ?>/js/plugins/select2/select2-bootstrap.min.css">
<?php require 'inc/views/template_head_end.php'; ?>

<?php 
	$sql = "select * from match_data where wr_id = '$wr_id' order by wr_id desc";
	$result = sql_query($sql);
	$i = 1;
	$r = sql_fetch_array($result);
    $code = $r['code'];
?>

<!-- sub nav -->
<?php include_once('./app_sub_nav.php'); ?>
<!-- end sub nav -->

<!-- Contents Area -->
	
	<div class="content" style="margin-top: 42px;">
	
		<div class="btn-group btn-group-justified push-10">
	        <div class="btn-group">
	            <a href="champ_info_match.php?wr_id=<?=$wr_id;?>&user_code=<?=$user_code;?>"><button class="btn btn-lg btn-white " type="button">급수별 편성표</button></a>
	        </div>
	        <div class="btn-group">
	            <a href="champ_info_match_time.php?wr_id=<?=$wr_id;?>&user_code=<?=$user_code;?>"><button class="btn btn-lg btn-white active" type="button">대진 시간표</button></a>
	        </div>
		</div>
		 
	    <!-- 대진 시간표 조회 -->
	    <div class="row">
	    	<!-- 조회(코트별 조회) -->
	    	<div class="col-lg-6">
				<div class="block block-themed block-rounded">
	                <div class="block-header bg-smooth-dark" style="padding:13px 20px;">
		                    <ul class="block-options">
		                        <li>
	                                <button type="button" data-toggle="block-option" data-action="content_toggle"><i class="si si-arrow-up"></i></button>
		                        </li>
		                    </ul>
	                    <h3 class="block-title">코트별 조회</h3>
	                </div>
	                <div class="block-content">
	                    <!-- 체육관 조회 -->
	                    <form class="form-horizontal" action="champ_info_match_time.php" method="get" onsubmit="return true;">
	                    	<input type="hidden" name="wr_id" value="<?=$wr_id;?>" />
	                    	<input type="hidden" name="user_code" value="<?=$user_code;?>" />
							<div class="form-group push-10">
				                <div class="col-md-12">
				                    <select class="js-select2 form-control" id="gym_court" name="gym_court" style="width:100%;height:44px;" data-placeholder="체육관 선택">
				                        <option>체육관을 선택해 주세요</option>
				                	<?php
				                		$sql = "select * from match_gym_data where match_id = '$code' group by gym_id";
										$result = sql_query($sql);
										while($r = sql_fetch_array($result)){
											$gym_sql = "select * from gym_data where wr_id = '$r[gym_id]'";
											$gym_result = sql_query($gym_sql);
											$gym = sql_fetch_array($gym_result);
									?>
				                        <option <?php if($gym_court == $r['wr_id']) echo "selected";?> value="<?=$r['wr_id'];?>"><?=$gym['gym_name'];?></option>
									<?php		
										}
												
				                	?>
				                    </select>
				                </div>
				            </div>
							<div class="form-group push-10">
				                <div class="col-md-12">
				                	
				                    <select class="js-select2 form-control" id="gym_game" name="gym_game" style="width:100%;height:44px;" data-placeholder="체육관 선택">
				                        <option>코트를 선택해 주세요</option>
				                        <?php
				                		if($gym_court != ""){
				                			$sql = "select * from match_gym_data where match_id = '$code' and wr_id = '$gym_court'";
											$result = sql_query($sql);
											$r = sql_fetch_array($result);
											for($i = 0 ; $i < $r['use_court'];$i++){
					                	?>
					                		<option <?php if($gym_game == $i) echo "selected";?> value='<?=$i;?>'><?=$i;?>코트</option>
					                	<?php
											}
					                		}
					                	?>
				                    </select>
				                </div>
				            </div>
				            <div class="form-group push-10">
				            	<div class="col-md-12">
	                            	<button  type="submit" class="btn btn-block btn-mobile push-10" >검 색</button>
	                            </div>
				            </div>
				        </form>
			            <!-- end 체육관 조회 -->
	                </div>
	            </div>
			</div>
	    	<!-- end 조회(코트별 조회) -->
	    	
	    	
	    	<!-- 조회(시간별 조회) -->
	    	<div class="col-lg-6">
				<div class="block block-themed block-rounded">
	                <div class="block-header bg-smooth-dark" style="padding:13px 20px;">
		                    <ul class="block-options">
		                        <li>
	                                <button type="button" data-toggle="block-option" data-action="content_toggle"><i class="si si-arrow-up"></i></button>
		                        </li>
		                    </ul>
	                    <h3 class="block-title">시간별 조회</h3>
	                </div>
	                <div class="block-content">
	                    <!-- 체육관 조회 -->
						<form class="form-horizontal" action="champ_info_match_time.php" method="get" onsubmit="return true;">
							<input type="hidden" name="wr_id" value="<?=$wr_id;?>" />
							<input type="hidden" name="user_code" value="<?=$user_code;?>" />
							<div class="form-group push-10">
				                <div class="col-md-12">
				                    <select class="js-select2 form-control" id="time_court" name="time_court" style="width:100%;height:44px;" data-placeholder="체육관 선택">
				                        <option>체육관을 선택해 주세요</option>
				                	<?php
				                		$sql = "select * from match_gym_data where match_id = '$code'";
										$result = sql_query($sql);
										while($r = sql_fetch_array($result)){
											$gym_sql = "select * from gym_data where wr_id = '$r[gym_id]'";
											$gym_result = sql_query($gym_sql);
											$gym = sql_fetch_array($gym_result);
									?>
				                        <option <?php if($time_court == $r['wr_id']) echo "selected";?> value="<?=$r['wr_id'];?>"><?=$gym['gym_name'];?> <?=$r['application_period'];?></option>
									<?php		
										}
												
				                	?>
				                    </select>
				                </div>
				            </div>
			                <div class="form-group push-10">
			                    <div class="col-md-8">
			                        <select class="js-select2 form-control" id="time_game" name="time_game" style="width:100%;height:44px;" data-placeholder="체육관 선택">
				                        <option>시간을 선택해 주세요</option>
				                        <?php
				                        	if($time_court != ""){
				                			$sqls = "select * from match_gym_data where match_id = '$code' and wr_id = '$time_court'";
											$results = sql_query($sqls);
											$rs = sql_fetch_array($results);
											
				                			$sql = "select * from game_score_data where match_code = '$code' and game_date = '$rs[application_period]' and gym_code = '$rs[gym_id]' group by game_time";
											
											$result = sql_query($sql);
											while($r = sql_fetch_array($result)){
					                	?>
					                		<option  <?php if($time_game == $r['game_time']) echo "selected";?> value='<?=$r['game_time'];?>'><?=$r['game_time'];?></option>
					                	<?php
											}
											}
					                	?>
				                    </select>
			                    </div>
			                </div>
			                <div class="form-group push-10">
				            	<div class="col-md-12">
	                            	<button  type="submit" class="btn btn-block btn-mobile push-10" >검 색</button>
	                            </div>
				            </div>
			            </form>
			            <!-- end 시간 조회 -->
	                </div>
	            </div>
			</div>
	    	<!-- end 조회(시간별 조회) -->
	    </div>
	    <!-- end 대진 시간표 조회 -->
	    
	    <!-- 접수현황 -->
	    <div class="row">
	        
	        <div class="col-lg-6">
	        	<div class="block">
			        <div class="block-content_">
			            <table class="js-table-sections table table-mobile">
			                <!-- 그룹 경기 조회 타이틀 -->
			                <tbody class="js-table-sections-header open">
			                    <tr>
			                        <td class="font-w600" width="85%"><?=$gym['gym_name'];?> 검색결과 </td>
			                        <td class="text-center">
			                            <i class="fa fa-angle-right"></i>
			                        </td>
			                    </tr>
			                </tbody>
			                <!-- end 그룹 경기 조회 타이틀 -->
			                <tbody>
			                    
			                    <!-- 참가 선수 정보 및 대진 점수 -->
			                    <tr>
			                    	<td colspan="2" class="text-center">
			                    		<div class="row tournament_match">
											<?php	
												if($gym_court != ""){
													$sql = "select * from match_gym_data where match_id = '$code' and wr_id = '$gym_court'";
													$result = sql_query($sql);
													$gym_id = sql_fetch_array($result);
													
													$sql = "select * from game_score_data where match_code = '$code' and gym_code = '$gym_id[gym_id]' and game_court = '$gym_game' order by game_increase";
													$result = sql_query($sql);
													
												
												}
												if($time_court != ""){
													$sql = "select * from match_gym_data where match_id = '$code' and wr_id = '$time_court'";
													$result = sql_query($sql);
													$gym_id = sql_fetch_array($result);
													
													$sql = "select * from game_score_data where match_code = '$code' and gym_code = '$gym_id[gym_id]' and game_time = '$time_game' order by game_increase";
																										
													
													$result = sql_query($sql);
												
												}
												$count = 1;
												while($score = sql_fetch_array($result)){
													$sql_team1 = "select * from team_data where team_code = '$score[team_1_code]'";
													$team1_result = sql_query($sql_team1);
													$team1 = sql_fetch_array($team1_result);
													
													$sql_team2 = "select * from team_data where team_code = '$score[team_2_code]'";
													$team2_result = sql_query($sql_team2);
													$team2 = sql_fetch_array($team2_result);
													
													$sql_gym = "select * from gym_data where wr_id = '$score[gym_code]'";
													$gym_result = sql_query($sql_gym);
													$gym = sql_fetch_array($gym_result);
													
												?>
			                    			<div class="col-xs-12 col-sm-6 col-md-6">
									            <a class="block block-rounded" href="javascript:void(0)">
									                <div class="block-options push-10-t push-10-r">
										                <li>
										                    <?=$score['game_code'];?>
										                </li>
										            </div>
									                <div class="tournament_header">
										               	<?php if($score['tournament'] == "L"){?>
									                    <h3 class="block-title">조별리그</h3>
										               	<?php }?>
										               	<?php if($score['tournament'] == "T"){?>
									                    <h3 class="block-title">토너먼트</h3>
										               	<?php }?>
									                </div>
									                <div class="tournament_content clearfix">
									                	<div class="pull-left text-center push-5-l">
									                    	<?php
									                			$favor_sql = "select count(*) as cnt from app_user_favor where user_code = '$user_code' and match_code = '$code' and team_code = '$score[team_1_code]'";
									                			$favor_result = sql_query($favor_sql);
																$favor = sql_fetch_array($favor_result);
																if($favor['cnt'] == 0){
															?>
															
									                    		<button class="btn_fav" onclick="favor('<?=$user_code?>','<?=$score['team_1_code']?>')">
									                    	<?php
																}else{
															?>
																<button class="btn_fav active" onclick="favor('<?=$user_code?>','<?=$score['team_1_code']?>')">
															<?php	
																}
									                		?>
										                    	<div class="left-star">
											                		<i class="fa fa-star"></i>
											                	</div>
										                        <div class="font-w600"><?=$team1['club'];?></div>
										                        <div class=""><?=$team1['team_1_name'];?>/<?=$team1['team_2_name'];?></div>
										                    </button>
									                    </div>
									                	<div class="tournament_point">
									                		<span class="left_team"><?=$score['team_1_score'];?><div class="status"><?php if($score['team_1_score'] > $score['team_2_score']) echo "승리";?><!--승리/기권/입력x--></div></span>
									                		<span>:</span>
									                		<span class="right_team"><?=$score['team_2_score'];?><div class="status"><?php if($score['team_1_score'] < $score['team_2_score']) echo "승리";?><!--승리/기권/입력x--></div></span>
									                    </div>
									                    <div class="pull-right text-center push-5-r">
									                		<?php
									                			$favor_sql = "select count(*) as cnt from app_user_favor where user_code = '$user_code' and match_code = '$code' and team_code = '$score[team_2_code]'";
									                			$favor_result = sql_query($favor_sql);
																$favor = sql_fetch_array($favor_result);
																if($favor['cnt'] == 0){
															?>
															
									                    		<button class="btn_fav" onclick="favor('<?=$user_code?>','<?=$score['team_2_code']?>')">
									                    	<?php
																}else{
															?>
																<button class="btn_fav active" onclick="favor('<?=$user_code?>','<?=$score['team_2_code']?>')">
															<?php	
																}
									                		?>
										                		<div class="right-star">
											                		<i class="fa fa-star"></i>
											                	</div>
										                        <div class="font-w600"><?=$team2['club'];?></div>
										                        <div class=""><?=$team2['team_1_name'];?>/<?=$team2['team_2_name'];?></div>
									                        </button>
									                    </div>
									                </div>
									               <div class="coat_tit">
														<?php if($gym_court != ""){?>
									                	<span class="pull-left"><?=$gym['gym_name'];?></span> <span class="pull-right"><?=$score['game_date'];?> <?=$score['game_time'];?> <?=$score['game_court'];?>코트 <?=$count;?>번 경기</span>
									                	<?php }?>
									                	<?php if($time_court != ""){?>
									                	<span class="pull-left"><?=$gym['gym_name'];?></span> <span class="pull-right"><?=$score['game_date'];?> <?=$score['game_time'];?> <?=$score['game_court'];?>코트 <?=$score['court_array_num'];?>번 경기</span>
									                	<?php }?>									                
									                </div>
									            </a>
									        </div>
									        <!-- end 참가선수 정보 -->
											<?php $count++; } ?>	        
			                    		</div>
			                    	</td>
			                    </tr>
			                    <!-- end 참가 선수 정보 및 대진 점수 -->
			                    
			                </tbody>
			                
			            </table>
			        </div>
			    </div>
	        	
	        </div>
	    </div>
	</div>
<!-- end Contents Area -->



<?php require 'inc/views/base_footer.php'; ?>
<?php require 'inc/views/template_footer_start.php'; ?>


<script>
$( "#gym_court" ).change(function () {
	$('#gym_game').html("<option>잠시만 기다려주세요</option>");
	var wr_id = $( this ).val();    
	$.ajax({
		type: 'post',
		url:'./gym_court_data.php',
		dataType:'json',
		data: {wr_id:wr_id, code:'<?php echo $code;?>'},
		success:function(data){
			var str = '';
			$('#gym_game').html("");
			str += '<option>코트를 선택해주세요</option>';
			for(var i = 1 ; i <= data[0]['use_court']; i++){
				str += '<option value='+i+'>'+i+'코트</option>';
			}
			$('#gym_game').append(str);
	   	}
    });
});

$( "#time_court" ).change(function () {
	$('#time_game').html("<option>잠시만 기다려주세요</option>");
	var wr_id = $( this ).val();    
	$.ajax({
		type: 'post',
		url:'./time_court_data.php',
		dataType:'json',
		data: {wr_id:wr_id, code:'<?php echo $code;?>'},
		success:function(data){
			$('#time_game').html("");
			var str = '';
			for(var i = 0 ; i < data.length; i++){
				str += '<option value='+data[i]['game_time']+'>'+data[i]['game_time']+'</option>';
			}
			console.log(str);
			$('#time_game').append(str);
	   	}
    });
});
function favor(user_code,team_code){
	$.ajax({
		type: 'post',
		url:'./favorite_add.php',
		dataType:'json',
		data: {user_code:user_code,team_code:team_code,code:'<?php echo $code;?>'},
		success:function(data){
			console.log(data);
			location.reload(true);
			// var str = '';
			// for(var sub in data){
				// console.log(sub);
				// str += '<option value='+data[sub]['series_sub']+'>'+data[sub]['series_sub']+'</option>';
			// }
			// console.log(str);
			
	   	}
    });
}
</script>
<!-- Page JS Plugins -->
<script src="<?php echo $one->assets_folder; ?>/js/plugins/select2/select2.full.min.js"></script>
<script src="<?php echo $one->assets_folder; ?>/js/plugins/datatables/jquery.dataTables.min.js"></script>


<!-- Page JS Code -->
<script src="<?php echo $one->assets_folder; ?>/js/pages/base_tables_datatables.js"></script>
<script>
    jQuery(function(){
        // Init page helpers (BS Datepicker + BS Datetimepicker + BS Colorpicker + BS Maxlength + Select2 + Masked Input + Range Sliders + Tags Inputs plugins)
        App.initHelpers(['table-tools','datepicker', 'datetimepicker', 'colorpicker', 'maxlength', 'select2', 'masked-inputs', 'rangeslider', 'tags-inputs']);
    });
</script>

<?php require 'inc/views/template_footer_end.php'; ?>

