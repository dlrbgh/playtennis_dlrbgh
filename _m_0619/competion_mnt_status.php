<?php
include_once('../common.php');
if (!defined('_GNUBOARD_')) exit; // 개별 페이지 접근 불가
include_once(G5_AOS_PATH.'/head.php');
$menu_cate2 ='management';
$menu_cate3 ='1';
?>

<!-- sub nav -->
<?php include_once('./app_sub_nav.php'); ?>
<!-- end sub nav -->
<?php

$gym_sql = "select gym_data.*, date_format(gym_data.wr_datetime, '%Y.%m.%d') as format_wr_datetime
,use_court
from gym_data join match_data join match_gym_data
on gym_data.wr_id = match_gym_data.gym_id and match_gym_data.match_id = match_data.code
";
$gym = sql_query($gym_sql);
$date_data;
$gym_data;


while ($row = sql_fetch_array($gym)) {
	$date_data[$row['format_wr_datetime']] = $row;
	$gym_data[$row['format_wr_datetime']][$row['wr_id']] = $row;
}

 ?>

<!-- Contents Area -->
<div class="pop_container">
	<section>
		<div class="pop_hd">
			대회선택
		</div>
		<div class="content">
			<select class="full-width form-control" id="match_list" class="" name="m">
				<option value="">경기를 선택해주세요</option>
				<?php
					while($row = sql_fetch_array($match_result)){?>
						<option <?=$c == $row['code'] ? 'selected="selected"' : ""?> value="<?=$row['code']?>"><?=$row['wr_name']?></option>
					<?php }?>
			</select>
		</div>
	</section>
	<script>$('#match_list').change(function(){window.location.href='?c='+$(this).val();});</script>
	<?php
	if (count($date_data) > 1) {?>
		<section>
			<div class="pop_hd">
				<div class="tit">
					날짜 선택
				</div>
			</div>
			<div class="content">
				<div id="court_list" class="btn-group" role="group">
				<ul class="btn-list">
		<?php
				foreach($date_data as $date){?>
					<li><a href="?test=test"><?=$date['format_wr_datetime']?></a></li>
				<?php }?>
				</ul>
			</div>
			</div>
		</section>
		<?php }else{
			print_r($date_date);
			// $dt = array_keys($date_data)[0];
		}?>
	<section>
		<!--
		<div class="tab_group1 mb-0">
			<a class="active" href="competion_mnt_status.php">진행현황</a>
			<a class="" href="competion_mnt_list.php">경기관리</a>
		</div>
		-->
		<div class="match_manager_area">

				<!-- <ul>
					<li>
						<a href="competion_mnt_list.php">
						<div class="ground_area">ㅇㅇ 체육관 담당 sad 담당 담당 담당  길어도 안밀려요</div>
						<div class="m_name">홍길동, 홍명동, 홍가동, 홍나동</div>
						</a>
					</li>
					<li class="active">
						<a href="competion_mnt_status.php">
						<div class="ground_area">ㅇㅇ 체육관 담당 sad 담당 담당 담당 담당 담당 엄청</div>
						<div class="m_name">홍길동, 홍명동, 홍가동, 홍나동</div>
						</a>
					</li>

				</ul> -->
		</div>
	</section>
	<section>

		<div class="content">
			<div class="tbl_match_status">
				<div class="con_tit_area clear">
						<div class="tit">코트상황</div>
						<div class="r-area">
							<ul>
								<li class="color5 fw-700">2017-04-30:13:15:20</li>
							</ul>
						</div>
					</div>
					<table>
						<thead>
							<tr>
								<th width="50">구분</th>
								<th>01</th>
								<th>02</th>
								<th>03</th>
								<th>04</th>
								<th>50</th>
								<th>06</th>
								<th>07</th>
								<th>08</th>
								<th>09</th>
								<th>10</th>
								<th>11</th>
								<th>12</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<th>1코트</th>
								<td class="end"></td>
								<td class="end"></td>
								<td class=""></td>
							</tr>
							<tr>
								<th>2코트</th>
								<td class="end"></td>
								<td class="end"></td>
								<td class="end"></td>
								<td class="end"></td>
								<td class="end"></td>
								<td class="end"></td>
								<td class="end"></td>
								<td class="end"></td>
								<td class="playing ani"></td>
								<td class=""></td>
							</tr>
							<tr>
								<th>1</th>
								<td class="end"></td>
								<td class="end"></td>
								<td class="end"></td>
								<td class="end"></td>
								<td class="end"></td>
								<td class="end"></td>
								<td class="playing ani"></td>
								<td class=""></td>
							</tr>
							<tr>
								<th>2</th>
								<td class="end"></td>
								<td class="end"></td>
								<td class="end"></td>
								<td class="end"></td>
								<td class="end"></td>
								<td class="end"></td>
								<td class="end"></td>
								<td class="playing ani"></td>
								<td class=""></td>
								<td class=""></td>
								<td class=""></td>
								<td class=""></td>

							</tr>
						</tbody>
					</table>


				</div>
		</div>
	</section>
</div>
<!-- end Contents Area -->


<?php
include_once(G5_AOS_PATH.'/tail.php');
?>
