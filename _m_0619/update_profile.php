<?php
define('_INDEX_', true);
include_once('../common.php');
if (!defined('_GNUBOARD_')) exit; // 개별 페이지 접근 불가

include_once(G5_AOS_PATH.'/head.php');

$menu_cate2 ='mypage';
$menu_cate3 ='3';

$mb_id = get_session('ss_mb_id');
$mb = get_member($mb_id);


$area1 = sql_query("select * from area1");
$area2 = sql_query("select * from area2");
$club = sql_query("select * from club_list");

$mb_grade = sql_query("select * from g5_write_member where wr_1='{$mb['mb_2']}' and wr_2='{$mb['mb_3']}' and wr_3='{$mb['mb_name']}'");

$area2_sorted;
while($row = sql_fetch_array($area2)){
	$area2_sorted[$row['area1id']][] = $row;
}

$club_sorted;

while($row = sql_fetch_array($club)){
	$club_sorted[$row['area1id']][$row['area2id']][] = $row;
}
?>

<!-- sub nav -->
<?php include_once('./app_sub_nav.php'); ?>
<!-- end sub nav -->
<style>
	body{background-color:#fff}
</style>



<div class="sub_container">
	<!--
	<div style="text-align:center;font-size:16px;font-weight:700;padding:10px 0 20px">
		프로필 정보 입력
	</div>
	-->
	<form class="" action="<?=G5_BBS_URL?>/register_form_update.php" method="post">
				<input type="hidden" name="w" value="u">
				<input type="hidden" name="mb_id" value="<?=get_session('ss_mb_id')?>">
				<input type="hidden" name="redirect" value="<?=G5_URL."/m/update_profile.php"?>"/>
	<!-- 회원정보수정 -->
	<div class="profile_update">
		<div class="tip">
			원활한 플레이테니스 어플리케이션 사용을 위하여 회원님의 추가 정보 입력이 필요합니다.
		</div>
		<div class="tbl_frm02">

			<table>
				<tbody>
					<tr>
						<th width="90">이름</th>
						<td><input type="text" name="mb_name" id="mb_name" value="<?=$mb['mb_name']?>" placeholder="이름을 입력하세요" /></td>
					</tr>
					<tr>
						<th>지역1</th>
						<td>
							<select class="full-width form-control2" id="mb_1" name="mb_1">
								<option value = "">도를 선택해주세요</option>
								<?php $area1 = sql_query("select * from area1");
								 while($row = sql_fetch_array($area1)){?>
									 <option
									 	<?=$row['id'] == $mb['mb_1'] ? 'selected="selected"' : ""?>
										 value="<?=$row['id']?>"><?=$row['area1']?></option>
								 <?php }?>
							</select>
						</td>
					</tr>
					<tr>
						<th>지역2</th>
						<td>
							<select class="full-width form-control2" id="mb_2" name="mb_2">
								<option value = "">시를 선택해주세요</option>
								<?php $area2 = sql_query("select * from area2 where area1id = ". $mb['mb_1'] );
								 while($row = sql_fetch_array($area2)){?>
									 <option
									 	<?=$row['id'] == $mb['mb_2'] ? 'selected="selected"' : ""?>
										 value="<?=$row['id']?>"><?=$row['area2']?></option>
								 <?php }?>
							</select>

						</td>
					</tr>
					<tr>
						<th>소속클럽</th>
						<td><select class="full-width form-control2" id="mb_3" name="mb_3">
							<option value="">클럽을 선택해주세요</option>
							<?php $clublist = sql_query("select * from club_list where area1id = ". $mb['mb_1']." and area2id = ".$mb['mb_2'] );
							 while($row = sql_fetch_array($clublist)){?>
								 <option
									<?=$row['id'] == $mb['mb_3'] ? 'selected="selected"' : ""?>
									 value="<?=$row['id']?>"><?=$row['club']?></option>
							 <?php }?>
						</select></td>
					</tr>
					<tr>
						<th>휴대폰</th>
						<td><input type="tel" name="mb_tel" value="<?=$mb['mb_tel']?>" placeholder="휴대폰번호를 입력하세요" /></td>
					</tr>
					<tr>
						<th>출생년도</th>
						<td><input type="tel" name="mb_4" value="<?=$mb['mb_4']?>" placeholder="주민등록상 출생년도를 입력하세요" /></td>
					</tr>
					<tr>
						<th>등급</th>
						<td><?=$mb['mb_5']?></td>
					</tr>
					<tr>
						<th>랭킹</th>
						<td><?=$mb['mb_6']?></td>
					</tr>
				</tbody>
			</table>
		</div>
		<div class="con_btn_area">
			<button type="submit">적 용</a>
		</div>
	<style>
		/*회원가입 콘텐츠 버튼 */
.con_btn_area{position:relative;width:100%;display:block;margin-bottom:10px}
.con_btn_area button{background-color:#14264a;color:#fff;padding:20px 0;display:block;text-align:center;width:100%;border:1px solid #14264a}
.con_btn_area button:active{background-color:#bdbdbd;color:#000}
	</style>
	</div>
	<!--
	<section>
		<div class="quick_view">
			<a href="clearSession.php" class="btn_default">
				로그아웃하기
			</a>
		</div>
	</section>
	<br /><br /><br />
	
	<div class="ft_area">
		<div class="btn_area">
			<button type="submit">적 용</a>
		</div>
	</div>-->
	</form>
	<!-- //회원정보수정 -->
</div>


<!-- champ_list -->
<script type="text/javascript">

var area2 =<?=json_encode($area2_sorted);?>;
var club =<?=json_encode($club_sorted);?>;
$('#mb_1').change(function(){
	var area1 = $(this).val();
	var type = '시';
	if(area1 == '') type = '도';
	var _html = '<option value="">'+type+'를 선택해주세요</option>';
	var target = area2[$(this).val()];
	for(var i in target){
		_html += '<option value="'+target[i].id+'">'+target[i].area2+'</option>';
	}
	$('#mb_2').empty().append(_html);
	$('#mb_3').empty().append('<option value="">'+type+'를 선택해주세요</option>');
});
$('#mb_2').change(function(){
	var area1 = $('#mb_1').val();
	var area2 = $(this).val();
	var type = '클럽을';
	if(area1 == '') type = '도를';
	if(area2 == '') type = '시를';
	var _html = '<option value="">'+type+' 선택해주세요</option>';
	var target = club[area1][area2];
	console.log(target);
	for(var i in target){
		_html += '<option value="'+target[i].id+'">'+target[i].club+'</option>';
	}
	$('#mb_3').empty().append(_html);
});
$('#mb_4').keyup(maxLengthCheck)
function maxLengthCheck(event){
	var object = event.currentTarget;
   if (object.value.length > object.maxLength){
    object.value = object.value.slice(0, object.maxLength);
   }
}
	function verify(){
		// $('.alert').remove();
		var name = document.getElementById('mb_name')
		var mb_1 = document.getElementById('mb_1')
		var mb_2 = document.getElementById('mb_2')
		var mb_3 = document.getElementById('mb_3')
		var mb_tel = document.getElementById('mb_tel')
		var mb_4 = document.getElementById('mb_4')

				$('.border_color5').removeClass('border_color5');
		if(name.value == ''){
			$(name).addClass('border_color5').attr('placeholder', '이름이 입력되지 않았습니다').focus()
			return false;
		}else if(mb_1.value == ''){
			$(mb_1).addClass('border_color5').focus()
			return false;
		}else if(mb_2.value == ''){
			$(mb_2).addClass('border_color5').focus()
			return false;
		}else if(mb_3.value == ''){
			$(mb_3).addClass('border_color5').attr('placeholder', '클럽명이 입력되지않았습니다').focus()
			return false;
		}else if(mb_tel.value == ''){
			$(mb_tel).addClass('border_color5').attr('placeholder', '휴대폰번호를 입력해주세요').focus()
			return false;
		}else if(mb_4.value == '' || mb_4.value<1900){
			$(mb_4).addClass('border_color5').attr('placeholder', '연도가 올바르지않습니다').focus()
			return false;
		}
		return true;
	}
</script>


<?php
include_once(G5_AOS_PATH.'/tail.php');
?>
