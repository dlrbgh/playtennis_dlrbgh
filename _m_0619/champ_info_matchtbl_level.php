<?php
include_once('../common.php');

$wr_id 		= $_REQUEST['wr_id'];
$division   = $_REQUEST['division'];
$series 	= $_REQUEST['series'];
$series_sub = $_REQUEST['series_sub'];
$user_code	= $_REQUEST['user_code'];

?>
<?php require 'inc/config.php'; ?>
<?php require 'inc/views/template_head_start.php'; ?>

<!-- Page JS Plugins CSS -->
<link rel="stylesheet" href="<?php echo $one->assets_folder; ?>/js/plugins/slick/slick.min.css">
<link rel="stylesheet" href="<?php echo $one->assets_folder; ?>/js/plugins/slick/slick-theme.min.css">
<link rel="stylesheet" href="<?php echo $one->assets_folder; ?>/js/plugins/select2/select2.min.css">
<link rel="stylesheet" href="<?php echo $one->assets_folder; ?>/js/plugins/select2/select2-bootstrap.min.css">
<?php require 'inc/views/template_head_end.php'; ?>

<?php 
	$sql = "select * from match_data where wr_id = '$wr_id' order by wr_id desc";
	$result = sql_query($sql);
	$i = 1;
	$r = sql_fetch_array($result);
    $code = $r['code'];
?>

<div class="menu_snav top_nav" id="home_tabs"  style="top: 0;">
    <a href="index.php?user_code=<?=$user_code;?>">홈</a></li> 
    <a class="" href="championship_info.php?wr_id=<?=$wr_id?>&user_code=<?=$user_code;?>">정보</a> 
    <a class="" href="champ_info_apply.php?wr_id=<?=$wr_id?>&user_code=<?=$user_code;?>">접수현황</a>
    <a class="active" href="champ_info_matchtbl_level.php?wr_id=<?=$wr_id?>&user_code=<?=$user_code;?>">대진표</a> 
    <a href="champ_info_match.php?wr_id=<?=$wr_id?>&user_code=<?=$user_code;?>">경기진행</a> 
    <a href="champ_info_endmatch.php?wr_id=<?=$wr_id?>&user_code=<?=$user_code;?>">결과</a> 
</div>
<!-- Contents Area -->
	
	<div class="content" style="margin-top: 42px;">
	
		<div class="btn-group btn-group-justified push-10">
	        <div class="btn-group">
	            <a href="champ_info_matchtbl_level.php?wr_id=<?=$wr_id;?>&user_code=<?=$user_code;?>"><button class="btn btn-lg btn-white active" type="button">급수별 편성표</button></a>
	        </div>
	        <div class="btn-group">
	            <a href="champ_info_matchtbl_time.php?wr_id=<?=$wr_id;?>&user_code=<?=$user_code;?>"><button class="btn btn-lg btn-white" type="button">대진 시간표</button></a>
	        </div>
		</div>
	    
	    <!-- 대진 시간표 조회 -->
	    <div class="row">
	    	<!-- 조회(급수별 조회) -->
	    	<div class="col-lg-6">
				<div class="block block-themed block-rounded">
	                <div class="block-header bg-smooth-dark" style="padding:13px 20px;">
		                    <ul class="block-options">
		                        <li>
	                                <button type="button" data-toggle="block-option" data-action="content_toggle"><i class="si si-arrow-up"></i></button>
		                        </li>
		                    </ul>
	                    <h3 class="block-title">급수별 조회</h3>
	                </div>
	                <div class="block-content">
	                    <!-- 체육관 조회 -->
						<form class="form-horizontal" action="champ_info_matchtbl_level.php" method="get" onsubmit="return true;">
							<input type="hidden" name="wr_id" value="<?=$wr_id;?>" />
							<input type="hidden" name="user_code" value="<?=$user_code;?>" />
							<div class="form-group push-10">
				                <div class="col-md-12">
								   	<select class="js-select2 form-control" id="division" name="division" style="width:30%;height:44px;display: inline-block;" data-placeholder="체육관 선택">
							            <option value="">성별</option>
							            <option <?php if($division == "혼복") echo "selected";?> value="혼복">혼복</option>
							            <option <?php if($division == "남복") echo "selected";?> value="남복">남복</option>
							            <option <?php if($division == "여복") echo "selected";?> value="여복">여복</option>
							        </select>
							        <select class="js-select2 form-control" id="series" name="series" style="width:30%;height:44px;display: inline-block;" data-placeholder="체육관 선택">
							            <option>연령</option>
							           <?php
							           
				                		if($division != ""){
											$sql = "select series from series_data where match_code = '$code' and division = '$division' group by series";
											echo $sql;
											$query = sql_query($sql);
											while($r = sql_fetch_array($query))
											{
					                	?>
											<option <?php if($r['series'] == $series) echo "selected";?> value='<?=$r['series']?>'><?=$r['series']?></option>
					                	<?php
											}
					                		}
					                	?>

							        </select>
							        <select class="js-select2 form-control" id="series_sub" name="series_sub" style="width:30%;height:44px;display: inline-block;" data-placeholder="체육관 선택">
							            <option>급수</option>
							            <?php
				                		if($series != ""){
											$sql = "select series_sub from series_data where match_code = '$code' and division = '$division' and series = '$series' group by series_sub";
											echo $sql;
											$query = sql_query($sql);
											while($r = sql_fetch_array($query))
											{
					                	?>
											<option <?php if($r['series_sub'] == $series_sub) echo "selected";?> value='<?=$r['series_sub']?>'><?=$r['series_sub']?></option>
					                	<?php
											}
					                		}
					                	?>
							        </select>
				                </div>
				            </div>
				            <div class="form-group push-10">
				            	<div class="col-md-12">
	                            	<button type="submit" class="btn btn-block btn-mobile push-10">검 색</button>
	                            </div>
				            </div>
			            </form>
			            <!-- end 체육관 조회 -->
	                </div>
	            </div>
			</div>
	    	<!-- end 조회(급수별 조회) -->
	    </div>
	    <!-- end 대진 시간표 조회 -->
	    
	    <!-- 접수현황 -->
	    <div class="row">
	    	<!-- 토너먼트 대진표 출력 -->
	    	<div class="col-md-6">
		    	<div class="block block-themed block-rounded">
			    	<div class="block-header bg-smooth-dark" style="padding:13px 20px;">
		                    <ul class="block-options">
		                        <li>
		                            <button type="button" data-toggle="block-option" data-action="content_toggle"><i class="si si-arrow-up"></i></button>
		                        </li>
		                    </ul>
		                <h3 class="block-title"><?=$division;?> / <?=$series;?> / <?=$series_sub;?></h3>
		            </div>
		            <?php
		        	$sql = "select count(*) as cnt from group_data where match_code = '$code' and division = '$division' and series = '$series' and series_sub = '$series_sub' and tournament = 'L'";
					$result = sql_query($sql);
					$r = sql_fetch_array($result);
					if($r['cnt'] > 1){
					?>
			    	<div class="block-content col-lg-6 push-10">
			    		<img class="img-responsive" src="assets/img/tournament/<?=$r['cnt'];?>.gif">
			    		<div class="text-center push-20 push-20-t">
			    			<h5>위 토너먼트 그림은 예선전 순위에따라 배정됩니다</h5>
			    		</div>
			        </div>
				    <?php		
						}				
					?>
		        </div>
		    </div>
	        <!-- end 토너먼트 대진표 출력 -->
	        
	        <?php
        			$sql = "select * from group_data where match_code = '$code' and division = '$division' and series = '$series' and series_sub = '$series_sub' and tournament = 'L' order by num asc";
					$result = sql_query($sql);
					while($r = sql_fetch_array($result)){
			?>
	        <!-- <?=$r['num']+1?> 그룹 -->
	        <div class="col-lg-6">
	        	<div class="block">
			        <div class="block-content_">
			        	<table class="js-table-sections table table-mobile">
			                <!-- 그룹 경기 조회 타이틀 -->
			                <tbody class="js-table-sections-header ">
			                    <tr>
			                        <td class="font-w600" width="85%"><?=$r['num']+1?>그룹</td>
			                        <td class="text-center">
			                            <i class="fa fa-angle-right"></i>
			                        </td>
			                    </tr>
			                </tbody>
			                <!-- end 그룹 경기 조회 타이틀 -->
			                <tbody>
			                    <!-- 경기 결과표 출력 -->
			                    <tr >
			                    	<td colspan="2" class="remove-border">
			                    		<?
				                    		$sql_team1 = "select * from team_data where team_code = '$r[team_1]'";
											$team1_result = sql_query($sql_team1);
											$team1 = sql_fetch_array($team1_result);
											
											$sql_team2 = "select * from team_data where team_code = '$r[team_2]'";
											$team2_result = sql_query($sql_team2);
											$team2 = sql_fetch_array($team2_result);
											
											$sql_team3 = "select * from team_data where team_code = '$r[team_3]'";
											$team3_result = sql_query($sql_team3);
											$team3 = sql_fetch_array($team3_result);
											
											$sql_team4 = "select * from team_data where team_code = '$r[team_4]'";
											$team4_result = sql_query($sql_team4);
											$team4 = sql_fetch_array($team4_result);
											
											$sql_team5 = "select * from team_data where team_code = '$r[team_5]'";
											$team5_result = sql_query($sql_team5);
											$team5 = sql_fetch_array($team5_result);
											
		                    			?>
			                    		<table class="table champ_result">
				                    		<thead>
				                    			<th class="text-center">클럽명</th>
				                    			<th class="text-center">지역</th>
				                    			<th class="text-center">참가자</th>
				                    		</thead>
				                    		<tbody>
				                    			<tr>
				                    				<td><?=$team1['club'];?></td>
				                    				<td><?=$team1['area_1'];?></td>
				                    				<td><?=$team1['team_1_name'];?> / <?=$team1['team_2_name'];?></td>
				                    			</tr>
				                    			<tr>
				                    				<td><?=$team2['club'];?></td>
				                    				<td><?=$team2['area_1'];?></td>
				                    				<td><?=$team2['team_1_name'];?> / <?=$team2['team_2_name'];?></td>
				                    			</tr>
				                    			<?php if($team3['club'] != ""){?>
				                    			<tr>
				                    				<td><?=$team3['club'];?></td>
				                    				<td><?=$team3['area_1'];?></td>
				                    				<td><?=$team3['team_1_name'];?> / <?=$team3['team_2_name'];?></td>
				                    			</tr>
				                    			<?php }?>
				                    			<?php if($team4['club'] != ""){?>
				                    			<tr>
				                    				<td><?=$team4['club'];?></td>
				                    				<td><?=$team4['area_1'];?></td>
				                    				<td><?=$team4['team_1_name'];?> / <?=$team4['team_2_name'];?></td>
				                    			</tr>
				                    			<?php }?>
				                    			<?php if($team5['club'] != ""){?>
				                    			<tr>
				                    				<td><?=$team5['club'];?></td>
				                    				<td><?=$team5['area_1'];?></td>
				                    				<td><?=$team5['team_1_name'];?> / <?=$team5['team_2_name'];?></td>
				                    			</tr>
				                    			<?php }?>

				                    		</tbody>
				                    	</table>
				                    </td>
			                    </tr>
			                    <!-- end 경기 결과표 출력 --> 
			                    
			                </tbody>
			                
			            </table>
			        </div>
			    </div>
	        </div>
	        <!-- end <?=$r['num']+1?> 그룹 -->
			<?php } ?>	        
	        
	    </div>
	</div>
<!-- end Contents Area -->


<?php require 'inc/views/base_footer.php'; ?>
<?php require 'inc/views/template_footer_start.php'; ?>
<script>
$( "#division" ).change(function () {
	$('#series').html("<option>잠시만 기다려주세요</option>");
	$('#series_sub').html("<option></option>");
	var division = $( this ).val();
	console.log('./series_array_push.php?division='+division+'&code=<?php echo $code;?>');
	$.ajax({
		type: 'post',
		url:'./series_array_push.php',
		dataType:'json',
		data: {division:division, code:'<?php echo $code;?>'},
		success:function(data){
			//console.log(data[0]);
			$('#series').html("<option>연령</option>");
			var str = '';
			for(var sub in data){
				console.log(sub);
				str += '<option value='+data[sub]['series']+'>'+data[sub]['series']+'</option>';
			}
			console.log(str);
			$('#series').append(str);
	   	}
    });
});
$( "#series" ).change(function () {
	var division = $( "#division" ).val();
	var series = $( "#series" ).val();
	$('#series_sub').html("<option>잠시만 기다려주세요</option>");
	console.log('./series_sub_array_push.php?division='+division+'&code=<?php echo $code;?>');
	$.ajax({
		type: 'post',
		url:'./series_sub_array_push.php',
		dataType:'json',
		data: {division:division,series:series, code:'<?php echo $code;?>'},
		success:function(data){
			//console.log(data);
			$('#series_sub').html("<option>급수</option>");
			var str = '';
			for(var sub in data){
				console.log(sub);
				str += '<option value='+data[sub]['series_sub']+'>'+data[sub]['series_sub']+'</option>';
			}
			console.log(str);
			$('#series_sub').append(str);
	   	}
    });
});
</script>

<!-- Page JS Plugins -->
<script src="<?php echo $one->assets_folder; ?>/js/plugins/select2/select2.full.min.js"></script>
<script src="<?php echo $one->assets_folder; ?>/js/plugins/datatables/jquery.dataTables.min.js"></script>


<!-- Page JS Code -->
<script src="<?php echo $one->assets_folder; ?>/js/pages/base_tables_datatables.js"></script>
<script>
    jQuery(function(){
        App.initHelpers(['table-tools']);
    });
</script>

<?php require 'inc/views/template_footer_end.php'; ?>

