<?php
include_once('../common.php');
if (!defined('_GNUBOARD_')) exit; // 개별 페이지 접근 불가
include_once(G5_AOS_PATH.'/head.php');
$menu_cate2 ='competition_view';
$menu_cate3 ='7';

$code = $c;

?>


<!-- sub nav -->
<?php include_once('./app_sub_nav.php'); ?>
<!-- end sub nav -->

<!-- Contents Area -->
<div class="pop_container">

<!-- -남자마스터2부 -->
	<section>
		<div class="content">
			<div class="con_tit_area clear">
				<div class="tit mb-5 fw-700">
					<?=$competition['wr_name']?> - 남자 - 마스터 1부

				</div>
				<div class="tit">
				</div>
				<div class="r-area">
					<ul>
						<li class="color5 fw-700"></li>
					</ul>
				</div>
			</div>

			<div class="tbl_style01 tbl_striped mb-20">
				<table>
					<thead>
						<tr>
							<th>순위</th>
							<th>클럽</th>
							<th>선수</th>
						</tr>
					</thead>
					<tbody class="text-center">
						<tr>
							<td>우승</td>
							<td>강릉</td>
							<td>강릉시</td>
						</tr>
						<tr>
							<td>준우승</td>
							<td>양구</td>
							<td>양구시</td>
						</tr>
						<tr>
							<td>3위</td>
							<td>화천</td>
							<td>화천군</td>
						</tr>
						<tr>
							<td> </td>
							<td>홍천</td>
							<td>홍천군</td>
						</tr>
						
					</tbody>
				</table>
			</div>
		</div>
	</section>

<!--개인남자신인1부-->
	<section>
		<div class="content">
			<div class="con_tit_area clear">
				<div class="tit mb-5 fw-700">
					<?=$competition['wr_name']?> - 남자 - 마스터2부

				</div>
				<div class="tit">
				</div>
				<div class="r-area">
					<ul>
						<li class="color5 fw-700"></li>
					</ul>
				</div>
			</div>

			<div class="tbl_style01 tbl_striped mb-20">
				<table>
					<thead>
						<tr>
							<th>순위</th>
							<th>클럽</th>
							<th>선수</th>
						</tr>
					</thead>
					<tbody class="text-center">
						<tr>
							<td>우승</td>
							<td>철원</td>
							<td>철원A</td>
						</tr>
						<tr>
							<td>준우승</td>
							<td>정선</td>
							<td>정선군</td>
						</tr>
						<tr>
							<td>3위</td>
							<td>철원</td>
							<td>철원B</td>
						</tr>
						<tr>
							<td> </td>
							<td>인제</td>
							<td>인제군</td>
						</tr>
						
					</tbody>
				</table>
			</div>
		</div>
	</section>
	
<!--개인남자신인1부-->
	<section>
		<div class="content">
			<div class="con_tit_area clear">
				<div class="tit mb-5 fw-700">
					<?=$competition['wr_name']?> - 남자 - 신인 1부

				</div>
				<div class="tit">
				</div>
				<div class="r-area">
					<ul>
						<li class="color5 fw-700"></li>
					</ul>
				</div>
			</div>

			<div class="tbl_style01 tbl_striped mb-20">
				<table>
					<thead>
						<tr>
							<th>순위</th>
							<th>클럽</th>
							<th>선수</th>
						</tr>
					</thead>
					<tbody class="text-center">
						<tr>
							<td>우승</td>
							<td>춘천</td>
							<td>춘천시</td>
						</tr>
						<tr>
							<td>준우승</td>
							<td>양구</td>
							<td>양구군</td>
						</tr>
						<tr>
							<td>3위</td>
							<td>화천</td>
							<td>화천군</td>
						</tr>
						<tr>
							<td> </td>
							<td>홍천</td>
							<td>홍천군</td>
						</tr>
						
					</tbody>
				</table>
			</div>
		</div>
	</section>	

<!-- -남자어르신부 -->
	<section>
		<div class="content">
			<div class="con_tit_area clear">
				<div class="tit mb-5 fw-700">
					<?=$competition['wr_name']?> - 남자 - 신인2부

				</div>
				<div class="tit">
				</div>
				<div class="r-area">
					<ul>
						<li class="color5 fw-700"></li>
					</ul>
				</div>
			</div>

			<div class="tbl_style01 tbl_striped mb-20">
				<table>
					<thead>
						<tr>
							<th>순위</th>
							<th>클럽</th>
							<th>선수</th>
						</tr>
					</thead>
					<tbody class="text-center">
						<tr>
							<td>우승</td>
							<td>철원</td>
							<td>철원군</td>
						</tr>
						<tr>
							<td>준우승</td>
							<td>정선</td>
							<td>정선군</td>
						</tr>
						<tr>
							<td>3위</td>
							<td>철원</td>
							<td>철원B</td>
						</tr>
						<tr>
							<td> </td>
							<td>인제</td>
							<td>인제군</td>
						</tr>
						
					</tbody>
				</table>
			</div>
		</div>
	</section>

<!--개인오픈부-->
	<section>
		<div class="content">
			<div class="con_tit_area clear">
				<div class="tit mb-5 fw-700">
					<?=$competition['wr_name']?> - 남자 - 어르신1부

				</div>
				<div class="tit">
				</div>
				<div class="r-area">
					<ul>
						<li class="color5 fw-700"></li>
					</ul>
				</div>
			</div>

			<div class="tbl_style01 tbl_striped mb-20">
				<table>
					<thead>
						<tr>
							<th>순위</th>
							<th>클럽</th>
							<th>선수</th>
						</tr>
					</thead>
					<tbody class="text-center">
						<tr>
							<td>우승</td>
							<td>춘천</td>
							<td>춘천시</td>
						</tr>
						<tr>
							<td>준우승</td>
							<td>강릉</td>
							<td>강릉시</td>
						</tr>
						<tr>
							<td>3위</td>
							<td>원주</td>
							<td>원주시</td>
						</tr>
						<tr>
							<td> </td>
							<td>속초</td>
							<td>속초시</td>
						</tr>
						
					</tbody>
				</table>
			</div>
		</div>
	</section>

<!-- -남자챌린저1부 -->
	<section>
		<div class="content">
			<div class="con_tit_area clear">
				<div class="tit mb-5 fw-700">
					<?=$competition['wr_name']?> - 남자 - 어르신2부

				</div>
				<div class="tit">
				</div>
				<div class="r-area">
					<ul>
						<li class="color5 fw-700"></li>
					</ul>
				</div>
			</div>

			<div class="tbl_style01 tbl_striped mb-20">
				<table>
					<thead>
						<tr>
							<th>순위</th>
							<th>클럽</th>
							<th>선수</th>
						</tr>
					</thead>
					<tbody class="text-center">
						<tr>
							<td>우승</td>
							<td>영월</td>
							<td>영월군</td>
						</tr>
						<tr>
							<td>준우승</td>
							<td>횡성</td>
							<td>횡성군</td>
						</tr>
						<tr>
							<td>3위</td>
							<td>삼척</td>
							<td>삼척시</td>
						</tr>
						<tr>
							<td> </td>
							<td>철원</td>
							<td>철원군</td>
						</tr>
						
					</tbody>
				</table>
			</div>
		</div>
	</section>


<!-- 여자 마스터부-->
	<section>
		<div class="content">
			<div class="con_tit_area clear">
				<div class="tit mb-5 fw-700">
					<?=$competition['wr_name']?> - 남자 - 챌린저 1부
				</div>
				<div class="tit">
				</div>
				<div class="r-area">
					<ul>
						<li class="color5 fw-700"></li>
					</ul>
				</div>
			</div>

			<div class="tbl_style01 tbl_striped mb-20">
				<table>
					<thead>
						<tr>
							<th>순위</th>
							<th>클럽</th>
							<th>선수</th>
						</tr>
					</thead>
					<tbody class="text-center">
						<tr>
							<td>우승</td>
							<td>화천</td>
							<td>화천군</td>
						</tr>
						<tr>
							<td>준우승</td>
							<td>춘천</td>
							<td>춘천시</td>
						</tr>
						<tr>
							<td>3위</td>
							<td>속초</td>
							<td>속초시</td>
						</tr>
						<tr>
							<td> </td>
							<td>원주</td>
							<td>원주시</td>
						</tr>
						
					</tbody>
				</table>
			</div>
		</div>
	</section>

<!-- 여자 신인1부-->
	<section>
		<div class="content">
			<div class="con_tit_area clear">
				<div class="tit mb-5 fw-700">
					<?=$competition['wr_name']?> - 남자 - 챌린저 2부
				</div>
				<div class="tit">
				</div>
				<div class="r-area">
					<ul>
						<li class="color5 fw-700"></li>
					</ul>
				</div>
			</div>

			<div class="tbl_style01 tbl_striped mb-20">
				<table>
					<thead>
						<tr>
							<th>순위</th>
							<th>클럽</th>
							<th>선수</th>
						</tr>
					</thead>
					<tbody class="text-center">
						<tr>
							<td>우승</td>
							<td>철원</td>
							<td>철원군</td>
						</tr>
						<tr>
							<td>준우승</td>
							<td>영월</td>
							<td>영월군</td>
						</tr>
						<tr>
							<td>3위</td>
							<td>횡성</td>
							<td>횡성군</td>
						</tr>
						<tr>
							<td> </td>
							<td>삼척시</td>
							<td>삼척시</td>
						</tr>
						
					</tbody>
				</table>
			</div>
		</div>
	</section>

<!-- 여자 신인2부-->
	<section>
		<div class="content">
			<div class="con_tit_area clear">
				<div class="tit mb-5 fw-700">
					<?=$competition['wr_name']?> - 여자 - 마스터1부
				</div>
				<div class="tit">
				</div>
				<div class="r-area">
					<ul>
						<li class="color5 fw-700"></li>
					</ul>
				</div>
			</div>

			<div class="tbl_style01 tbl_striped mb-20">
				<table>
					<thead>
						<tr>
							<th>순위</th>
							<th>클럽</th>
							<th>선수</th>
						</tr>
					</thead>
					<tbody class="text-center">
						<tr>
							<td>우승</td>
							<td>원주</td>
							<td>원주시</td>
						</tr>
						<tr>
							<td>준우승</td>
							<td>춘천</td>
							<td>춘천시</td>
						</tr>
						<tr>
							<td>3위</td>
							<td>양구</td>
							<td>양구군</td>
						</tr>
						<tr>
							<td> </td>
							<td>홍천</td>
							<td>홍천군</td>
						</tr>
						
					</tbody>
				</table>
			</div>
		</div>
	</section>

<!-- 여자 챌린저1부-->
	<section>
		<div class="content">
			<div class="con_tit_area clear">
				<div class="tit mb-5 fw-700">
					<?=$competition['wr_name']?> - 여자 - 마스터2부
				</div>
				<div class="tit">
				</div>
				<div class="r-area">
					<ul>
						<li class="color5 fw-700"></li>
					</ul>
				</div>
			</div>

			<div class="tbl_style01 tbl_striped mb-20">
				<table>
					<thead>
						<tr>
							<th>순위</th>
							<th>클럽</th>
							<th>선수</th>
						</tr>
					</thead>
					<tbody class="text-center">
						<tr>
							<td>우승</td>
							<td>인제</td>
							<td>인제군</td>
						</tr>
						<tr>
							<td>준우승</td>
							<td>철원</td>
							<td>철원군</td>
						</tr>
						<tr>
							<td>3위</td>
							<td></td>
							<td></td>
						</tr>
						<tr>
							<td> </td>
							<td></td>
							<td></td>
						</tr>
						
					</tbody>
				</table>
			</div>
		</div>
	</section>

<!-- 여자 챌린저2부-->
	<section>
		<div class="content">
			<div class="con_tit_area clear">
				<div class="tit mb-5 fw-700">
					<?=$competition['wr_name']?> - 여자 - 챌린저 1부
				</div>
				<div class="tit">
				</div>
				<div class="r-area">
					<ul>
						<li class="color5 fw-700"></li>
					</ul>
				</div>
			</div>

			<div class="tbl_style01 tbl_striped mb-20">
				<table>
					<thead>
						<tr>
							<th>순위</th>
							<th>클럽</th>
							<th>선수</th>
						</tr>
					</thead>
					<tbody class="text-center">
						<tr>
							<td>우승</td>
							<td>화천</td>
							<td>화천군</td>
						</tr>
						<tr>
							<td>준우승</td>
							<td>춘천</td>
							<td>춘천시</td>
						</tr>
						<tr>
							<td>3위</td>
							<td>원주</td>
							<td>원주시</td>
						</tr>
						<tr>
							<td> </td>
							<td>홍천</td>
							<td>홍천시</td>
						</tr>
						
					</tbody>
				</table>
			</div>
		</div>
	</section>	

<!-- 여자 챌린저2부-->
	<section>
		<div class="content">
			<div class="con_tit_area clear">
				<div class="tit mb-5 fw-700">
					<?=$competition['wr_name']?> - 여자 - 챌린저 2부
				</div>
				<div class="tit">
				</div>
				<div class="r-area">
					<ul>
						<li class="color5 fw-700"></li>
					</ul>
				</div>
			</div>

			<div class="tbl_style01 tbl_striped mb-20">
				<table>
					<thead>
						<tr>
							<th>순위</th>
							<th>클럽</th>
							<th>선수</th>
						</tr>
					</thead>
					<tbody class="text-center">
						<tr>
							<td>우승</td>
							<td>삼척</td>
							<td>삼척시</td>
						</tr>
						<tr>
							<td>준우승</td>
							<td>정선</td>
							<td>정선군</td>
						</tr>
						<tr>
							<td>3위</td>
							<td>고성</td>
							<td>고성군</td>
						</tr>
						<tr>
							<td> </td>
							<td>태백</td>
							<td>태백시</td>
						</tr>
						
					</tbody>
				</table>
			</div>
		</div>
	</section>	
<!-- end Contents Area -->

<?php
include_once(G5_AOS_PATH.'/tail.php');
?>
