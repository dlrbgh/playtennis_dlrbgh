<?php
include_once('../common.php');
if (!defined('_GNUBOARD_')) exit; // 개별 페이지 접근 불가
include_once(G5_AOS_PATH.'/head.php');

$menu_cate2 = "competition_view";
$menu_cate3 = "1";

$mb_id = get_session('ss_mb_id');
?>

<!-- sub nav -->
<?php include_once('./app_sub_nav.php'); ?>
<!-- end sub nav -->

<?php
$match_code = $c;
$favor = sql_fetch("SELECT * FROM favorite_match WHERE mb_id = '{$mb_id}' AND match_code = '{$match_code}'");

$display_none = "style=\"display:none;\"";
?>

<!-- Contents Area -->
<div class="pop_container"<?= ($competition['division'] == 1 ? "style=\"padding-top:55px\"" : "")?>>
	<!-- 대회 기본정보-->
	<section>
		<div class="pop_hd">
			<div class="tit">대회 기본정보</div>
			<div class="r-btn-area">
				<ul>
					<!-- 상태확인후 보였다 안보였다-->
					<li><a href="" id="add_favo" class="add_favo favor"
						<?=(isset($favor['favorite_code'])? $display_none : "" )?>>즐겨찾기 추가</a></li>

					<li><a href="" id="remove_favo" data-favor_code = "<?=$favor['favorite_code']?>" class="remove_favo favor"

						<?=(!isset($favor['favorite_code'])? $display_none : "" )?>>즐겨찾기 제거</a></li>

				</ul>

			</div>
		</div>
		<div class="content">
			<ul class="info-list">
				<?php
				//얘네정보는 app_sub_nav에 있음
				$match_category_index = 1;
				$match_category = array($match_category_index++=>"KTA", $match_category_index++=>"KATO"
				, $match_category_index++=>"KATA", $match_category_index++=>"KASTA", $match_category_index++=>"LOCAL"
				, $match_category_index++=>"비랭킹");
				 ?>
				<?php if($competition['scale']){?>
					<li><span>대회종류</span><?=$match_category[$competition['scale']]?></li><?php }?>
				<?php if($competition['wr_name']){?>
					<li><span>대회이름</span><?=$competition['wr_name']?></li><?php }?>
				<?php if($competition['opening_place']){?>
					<li><span>대회지역</span><?=$competition['opening_place']?></li><?php }?>
				<?php if($competition['date2']){?>
					<li><span>대회기간</span><?=$competition['date2']?> ~ <?=$competition['date2']?></li><?php }?>
				<?php if($competition['supervision']){?>
					<li><span>주관기관</span><?=$competition['supervision']?></li><?php }?>
				<!-- <li><span>공유하기</span>
					<a href=""><img src="https://placeholdit.imgix.net/~text?txtsize=6&txt=50%C3%9750&w=50&h=50"></a>
					<a href=""><img src="https://placeholdit.imgix.net/~text?txtsize=6&txt=50%C3%9750&w=50&h=50"></a>
					<a href=""><img src="https://placeholdit.imgix.net/~text?txtsize=6&txt=50%C3%9750&w=50&h=50"></a>
					<a href=""><img src="https://placeholdit.imgix.net/~text?txtsize=6&txt=50%C3%9750&w=50&h=50"></a>
					<a href=""><img src="https://placeholdit.imgix.net/~text?txtsize=6&txt=50%C3%9750&w=50&h=50"></a>
				</li> -->
			</ul>
		</div>
	</section>
	<!-- //대회 기본정보-->

	<!-- 대회 기본정보-->
<!-- <?=nl2br($competition['text1'])?>		 -->
	<?php if($competition['text1'] != ''){?>

	<section>
		<div class="pop_hd">
			<div class="tit">대회요강</div>
		</div>
		<div class="content">

	<?=nl2br($competition['text1'])?>

		</div>
	</section>
	<?php  }?>
	<!-- //대회 기본정보-->
</div>
<!-- end Contents Area -->
<script>
	$('.favor').click(function(event){
		event.preventDefault();
		var state;
		var type;
		var favor_code = $(this).data('favor_code');
		var match_code = '<?=$c?>';
		if($(this).attr('id') == 'add_favo'){
			state = '#remove_favo';
			type = 'add';
		}else if($(this).attr('id') == 'remove_favo'){
			state = '#add_favo';
			type = 'remove';
		}
		$.ajax({
			url:g5_url+"/m/ajax/favorite_add.php"
			,data:{type:type, favor_code : favor_code, match_code:match_code}
			,dataType:'JSON'
			,type:'POST'
			,cache:false
			,success:function(data){
				console.dir(data.sql);
				if(data.status == 'ok'){
					$('.favor').hide();
					if(data.favor_code){
						$(state).data('favor_code', data.favor_code);
					}
					$(state).show();
				}else{
					//todo
				}

			}
		})
	})

</script>

<?php
include_once(G5_AOS_PATH.'/tail.php');
?>
