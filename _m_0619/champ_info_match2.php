<?php
include_once('../common.php');

$wr_id 		= $_REQUEST['wr_id'];
$division   = $_REQUEST['division'];
$series 	= $_REQUEST['series'];
$series_sub = $_REQUEST['series_sub'];
$user_code	= $_REQUEST['user_code'];

?>
<?php require 'inc/config.php'; ?>
<?php require 'inc/views/template_head_start.php'; ?>

<!-- Page JS Plugins CSS -->
<link rel="stylesheet" href="<?php echo $one->assets_folder; ?>/js/plugins/slick/slick.min.css">
<link rel="stylesheet" href="<?php echo $one->assets_folder; ?>/js/plugins/slick/slick-theme.min.css">
<link rel="stylesheet" href="<?php echo $one->assets_folder; ?>/js/plugins/select2/select2.min.css">
<link rel="stylesheet" href="<?php echo $one->assets_folder; ?>/js/plugins/select2/select2-bootstrap.min.css">
<?php require 'inc/views/template_head_end.php'; ?>

<?php 
	$sql = "select * from match_data where wr_id = '$wr_id' order by wr_id desc";
	$result = sql_query($sql);
	$i = 1;
	$r = sql_fetch_array($result);
    $code = $r['code'];
?>

<div class="menu_snav top_nav" id="home_tabs"  style="top: 0;">
    <a href="index.php?user_code=<?=$user_code;?>">홈</a></li> 
    <a class="" href="championship_info.php?wr_id=<?=$wr_id?>&user_code=<?=$user_code;?>">정보</a> 
    <a class="" href="champ_info_apply.php?wr_id=<?=$wr_id?>&user_code=<?=$user_code;?>">접수현황</a>
    <a class="" href="champ_info_matchtbl_level.php?wr_id=<?=$wr_id?>&user_code=<?=$user_code;?>">대진표</a> 
    <a class="active" href="champ_info_match.php?wr_id=<?=$wr_id?>&user_code=<?=$user_code;?>">경기진행</a> 
    <a href="champ_info_endmatch.php?wr_id=<?=$wr_id?>&user_code=<?=$user_code;?>">결과</a> 
</div>

<!-- Contents Area -->
	
	<div class="content" style="margin-top: 42px;">
	
		<div class="btn-group btn-group-justified push-10">
	        <div class="btn-group">
	            <a href="champ_info_match.php?wr_id=<?=$wr_id;?>&user_code=<?=$user_code;?>"><button class="btn btn-lg btn-white active" type="button">급수별 편성표</button></a>
	        </div>
	        <div class="btn-group">
	            <a href="champ_info_match_time.php?wr_id=<?=$wr_id;?>&user_code=<?=$user_code;?>"><button class="btn btn-lg btn-white" type="button">대진 시간표</button></a>
	        </div>
		</div>
		 <div class="row">
	   	<div class="col-lg-6">
				<div class="block block-themed block-rounded">
	                <div class="block-header bg-smooth-dark" style="padding:13px 20px;">
		                    <ul class="block-options">
		                        <li>
	                                <button type="button" data-toggle="block-option" data-action="content_toggle"><i class="si si-arrow-up"></i></button>
		                        </li>
		                    </ul>
	                    <h3 class="block-title">급수별 조회</h3>
	                </div>
	                <div class="block-content">
	                    <!-- 체육관 조회 -->
						<form class="form-horizontal" action="champ_info_match.php" method="get" onsubmit="return true;">
							<input type="hidden" name="wr_id" value="<?=$wr_id;?>" />
							<input type="hidden" name="user_code" value="<?=$user_code;?>" />
							<div class="form-group push-10">
				                <div class="col-md-12">
								   	<select class="js-select2 form-control" id="division" name="division" style="width:30%;height:44px;display: inline-block;" data-placeholder="체육관 선택">
							            <option value="">성별</option>
							            <option <?php if($division == "혼복") echo "selected";?> value="혼복">혼복</option>
							            <option <?php if($division == "남복") echo "selected";?> value="남복">남복</option>
							            <option <?php if($division == "여복") echo "selected";?> value="여복">여복</option>
							        </select>
							        <select class="js-select2 form-control" id="series" name="series" style="width:30%;height:44px;display: inline-block;" data-placeholder="체육관 선택">
							            <option>연령</option>
							           <?php
							           
				                		if($division != ""){
											$sql = "select series from series_data where match_code = '$code' and division = '$division' group by series";
											echo $sql;
											$query = sql_query($sql);
											while($r = sql_fetch_array($query))
											{
					                	?>
											<option <?php if($r['series'] == $series) echo "selected";?> value='<?=$r['series']?>'><?=$r['series']?></option>
					                	<?php
											}
					                		}
					                	?>

							        </select>
							        <select class="js-select2 form-control" id="series_sub" name="series_sub" style="width:30%;height:44px;display: inline-block;" data-placeholder="체육관 선택">
							            <option>급수</option>
							            <?php
				                		if($series != ""){
											$sql = "select series_sub from series_data where match_code = '$code' and division = '$division' and series = '$series' group by series_sub";
											echo $sql;
											$query = sql_query($sql);
											while($r = sql_fetch_array($query))
											{
					                	?>
											<option <?php if($r['series_sub'] == $series_sub) echo "selected";?> value='<?=$r['series_sub']?>'><?=$r['series_sub']?></option>
					                	<?php
											}
					                		}
					                	?>
							        </select>
				                </div>
				            </div>
				            <div class="form-group push-10">
				            	<div class="col-md-12">
	                            	<button type="submit" class="btn btn-block btn-mobile push-10">검 색</button>
	                            </div>
				            </div>
			            </form>
			            <!-- end 체육관 조회 -->
	                </div>
	            </div>
			</div>
		</div>
	    
	    <!-- 접수현황 -->
	    <div class="row">
	    	<!-- 토너먼트 대진표 출력 -->
	        	<?php
		        	$sql = "select count(*) as cnt from group_data where match_code = '$code' and division = '$division' and series = '$series' and series_sub = '$series_sub' and tournament = 'L'";
					$result = sql_query($sql);
					$r = sql_fetch_array($result);
					if($r['cnt'] > 1){
				?>
	    	<div class="col-lg-6 push-10 bg-white" style="padding-bottom:10px;">
	    		<img class="img-responsive" src="assets/img/tournament/<?=$r['cnt'];?>.gif">
		        <div class="text-center push-10-t">
	    			<h5>위 토너먼트는 예선전 순위에따라 배정됩니다</h5>
	    		</div>
	        </div>
	        
	        	<?php		
					}				
				?>


	        <!-- end 토너먼트 대진표 출력 -->
	        <?php
        			$sql = "select * from group_data where match_code = '$code' and division = '$division' and series = '$series' and series_sub = '$series_sub' and tournament = 'L' order by num asc";
					$result = sql_query($sql);
					while($r = sql_fetch_array($result)){
			?>
	        <div class="col-lg-6">
	        	<div class="block">
			        <div class="block-content_">
			        	<table class="js-table-sections__ table table-mobile">
			                <!-- 그룹 경기 조회 타이틀 -->
			                <tbody class="js-table-sections-header open">
			                    <tr>
			                        <td class="font-w600" width="85%">리그 <?=$r['num']+1;?>그룹 대진 보기</td>
			                        <td class="text-center">
			                            <i class="fa fa-angle-right"></i>
			                        </td>
			                    </tr>
			                </tbody>
			                <!-- end 그룹 경기 조회 타이틀 -->
			                <tbody >
			                    <!-- 경기 결과표 출력 -->
			                    <tr>
			                    	<td colspan="2" class="remove-border">
			                    		<table class="table champ_result">
				                    		<thead>
				                    			<th class="text-center">순위</th>
				                    			<th class="text-center">클럽</th>
				                    			<th class="text-center">선수</th>
				                    			<th class="text-center">승</th>
				                    			<th class="text-center">패</th>
				                    			<th class="text-center">득점</th>
				                    			<th class="text-center">실점</th>
				                    			<th class="text-center">득실</th>
				                    		</thead>
				                    		<tbody>
				                    			<?php 
				                    			$rank_sql = "select * from team_data where (team_code = '$r[team_1]' or team_code = '$r[team_2]' or team_code = '$r[team_3]' or team_code = '$r[team_4]' or team_code = '$r[team_5]') and match_code = '$code' and division = '$division' and series = '$series' and series_sub = '$series_sub' order by group_rank";
												$rank_result = sql_query($rank_sql);
												while($rank = sql_fetch_array($rank_result)){
												?>
				                    			<tr>
				                    				<td><?=$rank['group_rank'];?></td>
				                    				<td><?=$rank['club'];?></td>
				                    				<td><?=$rank['team_1_name'];?><br/><?=$rank['team_2_name'];?></td>
				                    				<td><?=$rank['team_league_point'];?></td>
				                    				<td><?=$rank['team_league_lose_count'];?></td>
				                    				<td><?=$rank['team_total_match_point'];?></td>
				                    				<td><?=$rank['team_total_match_minus_point'];?></td>
				                    				<td><?=$rank['team_total_match_point'] - $rank['team_total_match_minus_point'];?></td>
				                    			</tr>
				                    			<?php
												}
				                    			?>
				                    		</tbody>
				                    	</table>
				                    	<div class="push-10-t font-w700">순위 선정 기준 : 다승 > 승률 > 득실차 > 다득점</div>
			                    	</td>
			                    </tr>
			                    <!-- end 경기 결과표 출력 --> 
			                    
			                    <!-- 참가 선수 정보 및 대진 점수 -->
			                    <tr>
			                    	<td colspan="2" class="text-center">
			                    		<div class="row tournament_match">
			                    			<?php 
				                    			$score_sql = "select * from game_score_data where group_code = '$r[code]' and match_code = '$code' and division = '$division' and series = '$series' and series_sub = '$series_sub' and tournament = 'L' order by wr_id desc";
												$score_result = sql_query($score_sql);
												$i = 1;
												while($score = sql_fetch_array($score_result)){
													
													$sql_team1 = "select * from team_data where team_code = '$score[team_1_code]'";
													$team1_result = sql_query($sql_team1);
													$team1 = sql_fetch_array($team1_result);
													
													$sql_team2 = "select * from team_data where team_code = '$score[team_2_code]'";
													$team2_result = sql_query($sql_team2);
													$team2 = sql_fetch_array($team2_result);
													
													$sql_gym = "select * from gym_data where wr_id = '$score[gym_code]'";
													$gym_result = sql_query($sql_gym);
													$gym = sql_fetch_array($gym_result);
													
												?>
											<!-- 참가선수 정보 -->
			                    			<div class="col-xs-12 col-sm-6 col-md-6">
									            <a class="block block-rounded" href="javascript:void(0)">
									                <div class="block-options push-10-t push-10-r">
										                <li>
										                    <?=$score['game_code'];?>
										                </li>
										            </div>
									                <div class="tournament_header">
									                    <h3 class="block-title">조별리그 <?=$i;?>경기</h3>
									                </div>
									                <div class="tournament_content clearfix">
									                	<div class="pull-left text-center push-5-l">
									                    	<?php
									                			$favor_sql = "select count(*) as cnt from app_user_favor where user_code = '$user_code' and match_code = '$code' and team_code = '$score[team_1_code]'";
									                			$favor_result = sql_query($favor_sql);
																$favor = sql_fetch_array($favor_result);
																if($favor['cnt'] == 0){
															?>
															
									                    		<button class="btn_fav" onclick="favor('<?=$user_code?>','<?=$score['team_1_code']?>')">
									                    	<?php
																}else{
															?>
																<button class="btn_fav active" onclick="favor('<?=$user_code?>','<?=$score['team_1_code']?>')">
															<?php	
																}
									                		?>
										                    	<div class="left-star">
											                		<i class="fa fa-star"></i>
											                	</div>
										                        <div class="font-w600"><?=$team1['club'];?></div>
										                        <div class=""><?=$team1['team_1_name'];?> <?=$team1['team_2_name'];?></div>
										                    </button>
									                    </div>
									                	<div class="tournament_point">
									                		<span class="left_team <?php if($game['team_1_score'] > $game['team_2_score']) echo "win";?>"><?=$game['team_1_score'];?>31</span>
									                		<span>vs</span>
									                		<span class="right_team <?php if($game['team_1_score'] < $game['team_2_score']) echo "win";?>"><?=$game['team_2_score'];?>29</span>
									                    </div>
									                    <div class="pull-right text-center push-5-r">
									                    	<?php
									                			$favor_sql = "select count(*) as cnt from app_user_favor where user_code = '$user_code' and match_code = '$code' and team_code = '$score[team_2_code]'";
									                			$favor_result = sql_query($favor_sql);
																$favor = sql_fetch_array($favor_result);
																if($favor['cnt'] == 0){
															?>
															
									                    		<button class="btn_fav" onclick="favor('<?=$user_code?>','<?=$score['team_2_code']?>')">
									                    	<?php
																}else{
															?>
																<button class="btn_fav active" onclick="favor('<?=$user_code?>','<?=$score['team_2_code']?>')">
															<?php	
																}
									                		?>
										                		<div class="right-star">
											                		<i class="fa fa-star"></i>
											                	</div>
										                        <div class="font-w600"><?=$team2['club'];?></div>
										                        <div class=""><?=$team2['team_1_name'];?> <?=$team2['team_2_name'];?></div>
									                        </button>
									                    </div>
									                </div>
									                <div class="coat_tit">
									                	<span class="pull-left"><?=$gym['gym_name'];?></span><span class="pull-right"><?=$score['game_date'];?> <?=$score['game_time'];?> <?=$score['game_court'];?>코트 <?=$score['court_array_num'];?>번 경기</span>
									                </div>
									            </a>
									        </div>
									        <!-- end 참가선수 정보 -->				                    			
									        <?php
									        	$i++;
												}
				                    		?>			                    			
			                    		</div>
			                    	</td>
			                    </tr>
			                    <!-- end 참가 선수 정보 및 대진 점수 -->
			                    
			                </tbody>
			                
			            </table>
		            </div>
	            </div>
            </div>
            <?php
					}
        	?>
            
            <!-- 리그전 종합 순위 -->
            <div class="col-lg-6">
	        	<div class="block">
			        <div class="block-content_">
			        	<table class="js-table-sections__ table table-mobile">
			                <!-- 그룹 경기 조회 타이틀 -->
			                <tbody class="js-table-sections-header open">
			                    <tr>
			                        <td class="font-w600" width="85%">토너먼트 시드배정 순위</td>
			                        <td class="text-center">
			                            <i class="fa fa-angle-right"></i>
			                        </td>
			                    </tr>
			                </tbody>
			                <!-- end 그룹 경기 조회 타이틀 -->
			                <tbody>
			                    <!-- 경기 결과표 출력 -->
			                    <tr>
			                    	<td colspan="2" class="remove-border">
			                    		<table class="table champ_result">
				                    		<thead>
				                    			<th class="text-center">순위</th>
				                    			<th class="text-center">클럽</th>
				                    			<th class="text-center">선수</th>
				                    			<th class="text-center">승</th>
				                    			<th class="text-center">패</th>
				                    			<th class="text-center">득점</th>
				                    			<th class="text-center">실점</th>
				                    			<th class="text-center">득실</th>
				                    			<th class="text-center">그룹순위</th>
				                    		</thead>
				                    		<tbody class="text-center" >
				                    			<?php 
				                    			$score_sql = "select * from team_data where match_code = '$code' and division = '$division' and series = '$series' and series_sub = '$series_sub'  ORDER BY group_rank,team_league_point desc ,winning_rate desc	,gains_losses_point desc, team_total_match_point DESC";
												$score_result = sql_query($score_sql);
												$i = 1;
												while($score = sql_fetch_array($score_result)){
													
													$gourp_sql = "select num from group_data where (team_1  = '$score[team_code]' or team_2  = '$score[team_code]' or team_3  = '$score[team_code]' or team_4  = '$score[team_code]'  or team_5  = '$score[team_code]') and match_code = '$code' and division = '$division' and series = '$series' and series_sub = '$series_sub' and tournament = 'L' order by num asc";
													$group_result = sql_query($gourp_sql);
													$group = sql_fetch_array($group_result);
												?>
				                    			<tr>
				                    				<td><?=$i;?></td>
				                    				<td><?=$score['club'];?></td>
				                    				<td><?=$score['team_1_name'];?><br/><?=$score['team_2_name'];?></td>
				                    				<td><?=$score['team_league_point'];?></td>
				                    				<td><?=$score['team_league_lose_count'];?></td>
				                    				<td><?=$score['team_total_match_point'];?></td>
				                    				<td><?=$score['team_total_match_minus_point'];?></td>
				                    				<td><?=$score['gains_losses_point'];?></td>
				                    				<td><?=$group['num']+1?>그룹<?=$score['group_rank'];?>위</td>
				                    			</tr>
				                    			<?php $i++; } ?>
				                    			
				                    		</tbody>
				                    	</table>
				                    	<div class="push-10-t font-w700">순위 선정 기준 : 다승 > 승률 > 득실차 > 다득점</div>
			                    	</td>
			                    </tr>
			                    <!-- end 경기 결과표 출력 --> 
			                    
			                    <!-- 참가 선수 정보 및 대진 점수 -->
			                    <tr>
			                    	<td colspan="2" class="text-center">
			                    		<div class="row tournament_match">
			                    			<?php 
				                    			$score_sql = "select * from game_score_data where group_code = '$r[code]' and match_code = '$code' and division = '$division' and series = '$series' and series_sub = '$series_sub' and tournament = 'L' order by wr_id desc";
												$score_result = sql_query($score_sql);
												$i = 1;
												while($score = sql_fetch_array($score_result)){
													
													$sql_team1 = "select * from team_data where team_code = '$score[team_1_code]'";
													$team1_result = sql_query($sql_team1);
													$team1 = sql_fetch_array($team1_result);
													
													$sql_team2 = "select * from team_data where team_code = '$score[team_2_code]'";
													$team2_result = sql_query($sql_team2);
													$team2 = sql_fetch_array($team2_result);
													
													$sql_gym = "select * from gym_data where wr_id = '$score[gym_code]'";
													$gym_result = sql_query($sql_gym);
													$gym = sql_fetch_array($gym_result);
													
												?>
											<!-- 참가선수 정보 -->
			                    			<div class="col-xs-12 col-sm-6 col-md-6">
									            <a class="block block-rounded" href="javascript:void(0)">
									                <div class="block-options push-10-t push-10-r">
										                <li>
										                    <?=$score['game_code'];?>
										                </li>
										            </div>
									                <div class="tournament_header">
									                    <h3 class="block-title">조별리그 <?=$i;?>경기</h3>
									                </div>
									                <div class="tournament_content clearfix">
									                	<div class="pull-left text-center push-5-l">
									                    	<?php
									                			$favor_sql = "select count(*) as cnt from app_user_favor where user_code = '$user_code' and match_code = '$code' and team_code = '$score[team_1_code]'";
									                			$favor_result = sql_query($favor_sql);
																$favor = sql_fetch_array($favor_result);
																if($favor['cnt'] == 0){
															?>
															
									                    		<button class="btn_fav" onclick="favor('<?=$user_code?>','<?=$score['team_1_code']?>')">
									                    	<?php
																}else{
															?>
																<button class="btn_fav active" onclick="favor('<?=$user_code?>','<?=$score['team_1_code']?>')">
															<?php	
																}
									                		?>
										                    	<div class="left-star">
											                		<i class="fa fa-star"></i>
											                	</div>
										                        <div class="font-w600"><?=$team1['club'];?></div>
										                        <div class=""><?=$team1['team_1_name'];?> <?=$team1['team_2_name'];?></div>
										                    </button>
									                    </div>
									                	<div class="tournament_point">
									                		<span class="left_team"><?=$score['team_1_score'];?><div class="status"><?php if($score['team_1_score'] > $score['team_2_score']) echo "승리";?><!--승리/기권/입력x--></div></span>
									                		<span>vs</span>
									                		<span class="right_team"><?=$score['team_2_score'];?><div class="status"><?php if($score['team_1_score'] < $score['team_2_score']) echo "승리";?><!--승리/기권/입력x--></div></span>
									                    </div>
									                    <div class="pull-right text-center push-5-r">
									                    	<?php
									                			$favor_sql = "select count(*) as cnt from app_user_favor where user_code = '$user_code' and match_code = '$code' and team_code = '$score[team_2_code]'";
									                			$favor_result = sql_query($favor_sql);
																$favor = sql_fetch_array($favor_result);
																if($favor['cnt'] == 0){
															?>
															
									                    		<button class="btn_fav" onclick="favor('<?=$user_code?>','<?=$score['team_2_code']?>')">
									                    	<?php
																}else{
															?>
																<button class="btn_fav active" onclick="favor('<?=$user_code?>','<?=$score['team_2_code']?>')">
															<?php	
																}
									                		?>
										                		<div class="right-star">
											                		<i class="fa fa-star"></i>
											                	</div>
										                        <div class="font-w600"><?=$team2['club'];?></div>
										                        <div class=""><?=$team2['team_1_name'];?> <?=$team2['team_2_name'];?></div>
									                        </button>
									                    </div>
									                </div>
									                <div class="coat_tit">
									                	<span class="pull-left"><?=$gym['gym_name'];?></span><span class="pull-right"><?=$score['game_date'];?> <?=$score['game_time'];?> <?=$score['game_court'];?>코트 <?=$score['court_array_num'];?>번 경기</span>
									                </div>
									            </a>
									        </div>
									        <!-- end 참가선수 정보 -->				                    			
									        <?php
									        	$i++;
												}
				                    		?>			                    			
			                    		</div>
			                    	</td>
			                    </tr>
			                    <!-- end 참가 선수 정보 및 대진 점수 -->
			                    
			                </tbody>
			                
			            </table>
		            </div>
	            </div>
            </div>
            <!-- end 리그전 종합 순위 -->
            <?php
				$group_sql = "select count(*) as cnt from group_data where match_code = '$code' and division = '$division' and series = '$series' and series_sub = '$series_sub'";
				$group_result = sql_query($group_sql);
            	$group = sql_fetch_array($group_result);
				if($group['cnt'] > 1){
			?>
			
            <div class="col-lg-6">
	        	<div class="block">
			        <div class="block-content_">
			            
						<table class="js-table-sections__ table table-mobile">
			                <!-- 그룹 경기 조회 타이틀 -->
			                <tbody class="js-table-sections-header open">
			                    <tr>
			                        <td class="font-w600" width="85%">토너먼트 대진 보기</td>
			                        <td class="text-center">
			                            <i class="fa fa-angle-right"></i>
			                        </td>
			                    </tr>
			                </tbody>
			                <!-- end 그룹 경기 조회 타이틀 -->
			                <tbody>
			                    
			                    <!-- 참가 선수 정보 및 대진 점수 -->
			                    <tr>
			                    	<td colspan="2" class="text-center">
			                    		<div class="row tournament_match">
			                    			<?php 
				                    			$score_sql = "select * from game_score_data where match_code = '$code' and division = '$division' and series = '$series' and series_sub = '$series_sub' and tournament != 'L' and game_assign = '1' order by wr_id desc";
												
												$score_result = sql_query($score_sql);
												$i = 1;
												while($score = sql_fetch_array($score_result)){
													
													$sql_team1 = "select * from team_data where team_code = '$score[team_1_code]'";
													$team1_result = sql_query($sql_team1);
													$team1 = sql_fetch_array($team1_result);
													
													$sql_team2 = "select * from team_data where team_code = '$score[team_2_code]'";
													$team2_result = sql_query($sql_team2);
													$team2 = sql_fetch_array($team2_result);
													
													$sql_gym = "select * from gym_data where wr_id = '$score[gym_code]'";
													$gym_result = sql_query($sql_gym);
													$gym = sql_fetch_array($gym_result);
													
												?>
											<!-- 참가선수 정보 -->
			                    			<div class="col-xs-12 col-sm-6 col-md-6">
									            <a class="block block-rounded" href="javascript:void(0)">
									                <div class="block-options push-10-t push-10-r">
										                <li>
										                    <?=$score['game_code'];?>
										                </li>
										            </div>
									                <div class="tournament_header">
										               	<?php if($score['tournament'] == "T"){?>
									                    <h3 class="block-title">토너먼트 <?=$score['tournament_count']*2;?>강</h3>
										               	<?php }?>
										               	<?php if($score['tournament'] == "C"){?>
									                    <h3 class="block-title">결승</h3>
										               	<?php }?>
									                </div>
									                <div class="tournament_content clearfix">
									                	
									                	<div class="pull-left text-center push-5-l">
									                		<?php
									                			$favor_sql = "select count(*) as cnt from app_user_favor where user_code = '$user_code' and match_code = '$code' and team_code = '$score[team_1_code]'";
									                			$favor_result = sql_query($favor_sql);
																$favor = sql_fetch_array($favor_result);
																if($favor['cnt'] == 0){
															?>
															
									                    		<button class="btn_fav" onclick="favor('<?=$user_code?>','<?=$score['team_1_code']?>')">
									                    	<?php
																}else{
															?>
																<button class="btn_fav active" onclick="favor('<?=$user_code?>','<?=$score['team_1_code']?>')">
															<?php	
																}
									                		?>
										                    	<div class="left-star">
											                		<i class="fa fa-star"></i>
											                	</div>
										                        <div class="font-w600"><?=$team1['club'];?></div>
										                        <div class=""><?=$team1['team_1_name'];?> <?=$team1['team_2_name'];?></div>
										                    </button>
									                    </div>
									                	<div class="tournament_point">
									                		<span class="left_team"><?=$score['team_1_score'];?><div class="status"><?php if($score['team_1_score'] > $score['team_2_score']) echo "승리";?><!--승리/기권/입력x--></div></span>
									                		<span>:</span>
									                		<span class="right_team"><?=$score['team_2_score'];?><div class="status"><?php if($score['team_1_score'] < $score['team_2_score']) echo "승리";?><!--승리/기권/입력x--></div></span>
									                    </div>
									                    <div class="pull-right text-center push-5-r">
															<?php
									                			$favor_sql = "select count(*) as cnt from app_user_favor where user_code = '$user_code' and match_code = '$code' and team_code = '$score[team_2_code]'";
									                			$favor_result = sql_query($favor_sql);
																$favor = sql_fetch_array($favor_result);
																if($favor['cnt'] == 0){
															?>
															
									                    		<button class="btn_fav" onclick="favor('<?=$user_code?>','<?=$score['team_1_code']?>')">
									                    	<?php
																}else{
															?>
																<button class="btn_fav active" onclick="favor('<?=$user_code?>','<?=$score['team_1_code']?>')">
															<?php	
																}
									                		?>										                		
									                			<div class="right-star">
											                		<i class="fa fa-star"></i>
											                	</div>
										                        <div class="font-w600"><?=$team2['club'];?></div>
										                        <div class=""><?=$team2['team_1_name'];?> <?=$team2['team_2_name'];?></div>
									                        </button>
									                    </div>
									                </div>
									                <div class="coat_tit">
									                	<span class="pull-left"><?=$gym['gym_name'];?></span> <span class="pull-right"><?=$score['game_date'];?> <?=$score['game_time'];?> <?=$score['game_court'];?>코트 <?=$score['court_array_num'];?>번 경기</span>
									                </div>
									            </a>
									        </div>
									        <!-- end 참가선수 정보 -->				                    			
									        <?php
									        	$i++;
												}
				                    		?>			                    			
			                    		</div>
			                    	</td>
			                    </tr>
			                    <!-- end 참가 선수 정보 및 대진 점수 -->
			                </tbody>
			            </table>
			        </div>
			    </div>
	        </div>
	        <?php		
				}
            ?>
	        
	    </div>
	</div>
<!-- end Contents Area -->


<?php require 'inc/views/base_footer.php'; ?>
<?php require 'inc/views/template_footer_start.php'; ?>

<script>
$( "#division" ).change(function () {
	$('#series').html("<option>잠시만 기다려주세요</option>");
	$('#series_sub').html("<option></option>");
	var division = $( this ).val();
	console.log('./series_array_push.php?division='+division+'&code=<?php echo $code;?>');
	$.ajax({
		type: 'post',
		url:'./series_array_push.php',
		dataType:'json',
		data: {division:division, code:'<?php echo $code;?>'},
		success:function(data){
			//console.log(data[0]);
			$('#series').html("<option>연령</option>");
			var str = '';
			for(var sub in data){
				console.log(sub);
				str += '<option value='+data[sub]['series']+'>'+data[sub]['series']+'</option>';
			}
			console.log(str);
			$('#series').append(str);
	   	}
    });
});
$( "#series" ).change(function () {
	var division = $( "#division" ).val();
	var series = $( "#series" ).val();
	$('#series_sub').html("<option>잠시만 기다려주세요</option>");
	console.log('./series_sub_array_push.php?division='+division+'&code=<?php echo $code;?>');
	$.ajax({
		type: 'post',
		url:'./series_sub_array_push.php',
		dataType:'json',
		data: {division:division,series:series, code:'<?php echo $code;?>'},
		success:function(data){
			//console.log(data);
			$('#series_sub').html("<option>급수</option>");
			var str = '';
			for(var sub in data){
				console.log(sub);
				str += '<option value='+data[sub]['series_sub']+'>'+data[sub]['series_sub']+'</option>';
			}
			console.log(str);
			$('#series_sub').append(str);
	   	}
    });
});
function favor(user_code,team_code){
	$.ajax({
		type: 'post',
		url:'./favorite_add.php',
		dataType:'json',
		data: {user_code:user_code,team_code:team_code,code:'<?php echo $code;?>'},
		success:function(data){
			console.log(data);
			location.reload(true);
			// var str = '';
			// for(var sub in data){
				// console.log(sub);
				// str += '<option value='+data[sub]['series_sub']+'>'+data[sub]['series_sub']+'</option>';
			// }
			// console.log(str);
			
	   	}
    });
}
</script>


<!-- Page JS Plugins -->
<script src="<?php echo $one->assets_folder; ?>/js/plugins/select2/select2.full.min.js"></script>
<script src="<?php echo $one->assets_folder; ?>/js/plugins/datatables/jquery.dataTables.min.js"></script>


<!-- Page JS Code -->
<script src="<?php echo $one->assets_folder; ?>/js/pages/base_tables_datatables.js"></script>
<script>
    jQuery(function(){
        // Init page helpers (BS Datepicker + BS Datetimepicker + BS Colorpicker + BS Maxlength + Select2 + Masked Input + Range Sliders + Tags Inputs plugins)
        App.initHelpers(['table-tools','datepicker', 'datetimepicker', 'colorpicker', 'maxlength', 'select2', 'masked-inputs', 'rangeslider', 'tags-inputs']);
    });
</script>

<?php require 'inc/views/template_footer_end.php'; ?>

