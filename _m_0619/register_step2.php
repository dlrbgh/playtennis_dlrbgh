
<?php
define('_INDEX_', true);
include_once('../common.php');
if (!defined('_GNUBOARD_')) exit; // 개별 페이지 접근 불가
include_once(G5_AOS_PATH.'/head.php');



$area1 = sql_query("select * from area1");
$area2 = sql_query("select * from area2");
$club = sql_query("select * from club_list");

$area2_sorted;
while($row = sql_fetch_array($area2)){
	$area2_sorted[$row['area1id']][] = $row;
}

$club_sorted;

while($row = sql_fetch_array($club)){
	$club_sorted[$row['area1id']][$row['area2id']][] = $row;
}

?>
<style>
	body{background-color:#fff}
</style>
<div class="login_hd_area">
	<!-- <div class="hd_lnb"><a href=""><i class="flaticon-left-arrow"></i></a></div> -->
	<a class="title" >추가정보 입력</a>
	<!-- <div class="hd_right"><a></a></div> -->
</div>

<!-- 회원가입 -->
<div class="login_container">

	<div class="tip">
		원활한 플레이테니스 어플리케이션 사용을 위하여 회원님의 추가 정보 입력이 필요합니다.
	</div>
	<div class="tbl_frm02">
		<form class="" action="./update_member_step2.php" method="post" onsubmit="return verify()">
			<input type="hidden" name="w" value="u">
			<input type="hidden" name="mb_id" value="<?php echo get_session('ss_mb_id')?>">
		<table>
			<tbody>
				<tr>
					<th width="90">이름</th>
					<td><input type="text" id="mb_name" name="mb_name" value="" placeholder="이름을 입력하세요" /></td>
				</tr>
				<tr>
					<th>지역1</th>
					<td>

						<select class="full-width form-control2" id="mb_1" name="mb_1">
							<option value = "">도를 선택해주세요</option>
							<?php $area1 = sql_query("select * from area1", true);
							 while($row = sql_fetch_array($area1)){?>
								 <option value="<?=$row['id']?>"><?=$row['area1']?></option>
							 <?php }?>
						</select>
					</td>
				</tr>
				<tr>
					<th>지역2</th>
					<td>
						<select class="full-width form-control2" id="mb_2" name="mb_2">
							<option value = "">도를 선택해주세요</option>
						</select>
					</td>
				</tr>
				<tr>
					<th>소속클럽</th>

					<td>
						<select class="full-width form-control2" id="mb_3" name="mb_3">
							<option value="">도를 선택해주세요</option>
						</select>
						<!-- <input type="text" name="mb_3" id="mb_3" placeholder="클럽명을 입력하세요" /> -->
					</td>
				</tr>
				<tr>
					<th>휴대폰</th>
					<td><input type="tel" name="mb_tel" id="mb_tel" placeholder="휴대폰번호를 입력하세요" /></td>
				</tr>
				<tr>
					<th>출생년도</th>
					<td><input type="number" name="mb_4" id="mb_4" maxlength="4" data-maxlength="4"  placeholder="주민등록상 출생년도를 입력하세요" /></td>
				</tr>
			</tbody>
		</table>

	</div>
	<div class="con_btn_area">
		<button type="submit">완료</a>
	</div>
	<style>
		/*회원가입 콘텐츠 버튼 */
.con_btn_area{position:relative;width:100%;display:block}
.con_btn_area button{background-color:#14264a;color:#fff;padding:20px 0;display:block;text-align:center;width:100%;border:1px solid #14264a}
.con_btn_area button:active{background-color:#bdbdbd;color:#000}
	</style>
</div>
<!-- //회원가입 -->


<!--
<div class="ft_area">
	<div class="btn_area">
		<button type="submit" name="button" >완료</button>
	</div>
</div>
-->
</form>
<script>


var area2 =<?=json_encode($area2_sorted);?>;
var club =<?=json_encode($club_sorted);?>;
$('#mb_1').change(function(){
	var area1 = $(this).val();
	var type = '시';
	if(area1 == '') type = '도';
	var _html = '<option value="">'+type+'를 선택해주세요</option>';
	var target = area2[$(this).val()];
	for(var i in target){
		_html += '<option value="'+target[i].id+'">'+target[i].area2+'</option>';
	}
	$('#mb_2').empty().append(_html);
	$('#mb_3').empty().append('<option value="">'+type+'를 선택해주세요</option>');
});
$('#mb_2').change(function(){
	var area1 = $('#mb_1').val();
	var area2 = $(this).val();
	var type = '클럽을';
	if(area1 == '') type = '도를';
	if(area2 == '') type = '시를';
	var _html = '<option value="">'+type+' 선택해주세요</option>';
	var target = club[area1][area2];
	for(var i in target){
		_html += '<option value="'+target[i].id+'">'+target[i].club+'</option>';
	}
	$('#mb_3').empty().append(_html);
});
$('#mb_4').keyup(maxLengthCheck)
function maxLengthCheck(event){
	var object = event.currentTarget;
   if (object.value.length > object.maxLength){
    object.value = object.value.slice(0, object.maxLength);
   }
}

	function verify(){
		$('.alert').remove();
		var name = document.getElementById('mb_name')
		var mb_1 = document.getElementById('mb_1')
		var mb_2 = document.getElementById('mb_2')
		var mb_3 = document.getElementById('mb_3')
		var mb_tel = document.getElementById('mb_tel')
		var mb_4 = document.getElementById('mb_4')

		$('.border_color5').removeClass('border_color5');

		if(name.value == ''){
			$(name).addClass('border_color5').attr('placeholder', '이름이 입력되지 않았습니다').focus()

			// setAlert(name, '이름이 입력되지 않았습니다');
			return false;
		}else if(mb_1.value == ''){
			$(mb_1).addClass('border_color5').focus()
			return false;
		}else if(mb_2.value == ''){
			$(mb_2).addClass('border_color5').focus()
			return false;
		}else if(mb_3.value == ''){
			$(mb_3).addClass('border_color5').attr('placeholder', '클럽명이 입력되지않았습니다').focus()
			return false;
		}else if(mb_tel.value == ''){
			$(mb_tel).addClass('border_color5').attr('placeholder', '휴대폰번호를 입력해주세요').focus()
			return false;
		}else if(mb_4.value == '' || mb_4.value<1900){
			$(mb_4).addClass('border_color5').attr('placeholder', '연도가 올바르지않습니다').focus()
			return false;
		}
		return true;
	}
	// function setAlert(obj, msg){
	// 	$(obj).parent().parent().after('<tr class="alert"><td colspan="2" class="text-center">'+msg+'</td></tr>');
	// 	$(obj).focus();
	// }



</script>
<?php
include_once(G5_AOS_PATH.'/tail.php');
?>
