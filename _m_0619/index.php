<?php
include_once('../common.php');
if (!defined('_GNUBOARD_')) exit; // 개별 페이지 접근 불가
include_once(G5_AOS_PATH.'/head.php');

  if(get_session('ss_mb_id', $mb_id) == ''){
    set_session('ss_mb_id', '');
    unset($_COOKIE['tennis']);
    setcookie('forlogin',null,-1);
    header('Location:index.php');
		goto_url('./index2.php');
  }

  if(get_session('emailLogined') ==  'emailLogined'){
      //이메일로그인했을때 생성된 토큰정보와 멤버의 정보를 갖다주기위해서 생성
      $mb = get_session('mb');
      $sql = "select mb_id, mb_email, mb_name
      ,mb_1, mb_2, mb_3
      , (select area1 from area1 where id = mb_1) as area1
      , (select area2 from area2 where id = mb_2) as area2
      , (select club from club_list where id = mb_3) as club
      , mb_4, mb_5
      from g5_member where mb_id= '{$mb['mb_id']}'";
      $mb_pick = sql_fetch($sql);
      $token = $mb['mb_10'];
      set_session('emailLogined','');
      ?>
      <script>
      try {
        window.Android.get_Member_info('<?php print json_encode($mb_pick);?>', '<?php print $token?>');
      } catch (e) {
        console.log('its web');
      } finally {

      }
      </script>
      <?php
  }

?>
<!-- <a href="clearSession.php">efefefeeef</a> -->
<div class="m_hd_area" >

	<div class="hd_lnb">
		<a href="" id="lnbMenu" class="menu"><i>메뉴</i></a>
	</div>
	<a class="logo" href="<?php echo G5_AOS_URL?>"><img src="<?php echo G5_AOS_URL?>/assets/img/logo2x.png" style="width:100%"></a>
	<div class="hd_right">

      <a
      <?php
        // $charge_list =
        // sql_query("SELECT * FROM match_gym_data
        // where date_format(application_period ,get_format(date, 'ISO')) = '{$today}'
        // and (charger1 = '{$mb_id}' or charger2 = '{$mb_id}'
        // or charger3 = '{$mb_id}' or charger4 = '{$mb_id}'
        // or charger5 = '{$mb_id}')");
        //날짜는나중에 입력하자
        $is_hidden = true;
        if($is_admin){
          $is_hidden = false;
        }else{
          $today = gmdate('Y-m-d');
          $mb_id = get_session('ss_mb_id');
          $sql = "select * from match_data where code in (SELECT match_id FROM match_gym_data
          where (charger1 = '{$mb_id}' or charger2 = '{$mb_id}'
          or charger3 = '{$mb_id}' or charger4 = '{$mb_id}'
          or charger5 = '{$mb_id}') ) and app_visible = 2";
          $charge_list = sql_query($sql);
          if($charge_list->num_rows > 0){
            $is_hidden = false;
          }
        }

      if($is_hidden) print "style=\"visibility  : hidden;\"";?>
      href="<?php echo G5_AOS_URL?>/competion_mnt_list.php"  id="management" class="calander"><i>일정</i></a>
      <?php
      // print $sql;?>
  </div>
</div>

<div class="m_container">


    <!-- <a href="clearSession.php">로그아웃</a> -->
	<!-- 관심대회 -->
  <?php
    $favorSql = "
        SELECT * ,
        case when now() between period1 and period2 then '접수중'
            when now() < period1 then ''
             else '접수종료' end as is_accept
        ,CASE
          WHEN date_format(date1, '%Y') = date_format(date2, '%Y')
          THEN concat(date_format(date1, '%Y-%m-%d'),' ~ ', date_format(date2, '%m-%d'))
          ELSE concat(date_format(date1, '%Y-%m-%d'),' ~ ', date_format(date2, '%Y-%m-%d'))
        END AS period
        FROM match_data join favorite_match
        on match_data.code = favorite_match.match_code

         where favorite_match.mb_id = '".get_session('ss_mb_id')."' and app_visible = 2
         order by date1 desc
         ";
        //  print $favorSql;
    $favor_list = sql_query($favorSql);

    if($favor_list ->num_rows > 0){?>

	<section>
		<div class="hd">
			<div class="tit"><i class="ico ico-star"></i>관심대회</div>
			<div class="r-btn-area">
				<ul>
					<li><a href="<?=G5_AOS_URL?>/competition_favor_list.php" class="more_info">더보기</a></li>
				</ul>
			</div>
		</div>
		<!-- list board -->

		<div class="bo_list_ul">

			<ul>

				 <!-- <li>
					<a href="<?=G5_AOS_URL?>/mypage_history.php" class="list_a">
						마이페이지(LNB_마이페이지)
					</a>
				</li>
				<li>
					<a href="<?=G5_AOS_URL?>/competion_mnt_status.php" class="list_a">
						경기진행현황(LNB_매니저용_점수입력)
					</a>
				</li>
				<li>
					<a href="<?=G5_AOS_URL?>/competition_mypage.php" class="list_a">
						단체전 선수배정
					</a>
				</li>
				<li>
					<a href="<?=G5_AOS_URL?>/popup_insert_score.php" class="list_a">
						점수입력 팝업
					</a>
				</li> -->

   				<?php while($row = sql_fetch_array($favor_list)){?>
   				<li>
   					<a href="<?=G5_AOS_URL?>/competition_view.php?c=<?=$row['code']?>" class="list_a">
   						<div class="champ_status">
   							<span class="champ_ico champ_ico<?=$row['scale']?>"></span>
   							<!-- <span class="limit_status"><?=$row['is_accept']?></span> -->
   						</div>
   						<span class="list_r_area">
   							<i class="flaticon-right-arrow"></i>
   						</span>
   						<div class="subject">
   							<div class="m_subject"><?=$row['wr_name']?></div>
   							<div class="s_subject"><?=$row['period']?>&nbsp;&nbsp;<?=$row['opening_place']?></div>
   						</div>
   					</a>
   				</li>
   				<?php }?>
			</ul>
			<div class="quick_view">
				<a href="<?php echo G5_AOS_URL?>/competition_favor_list.php">
					관심대회 리스트 바로가기
				</a>
			</div>

		</div>
		<!-- //list board -->
	</section>

  <?php }?>
	<!-- //관심대회 -->


	<!-- 중계대회 -->
	<section>
		<div class="hd">
			<div class="tit"><i class="ico ico-star"></i>중계대회</div>
			<div class="r-btn-area">
				<ul>
					<li><a href="<?=G5_AOS_URL?>/competition_relay_list.php" class="more_info">더보기</a></li>
				</ul>
			</div>
		</div>
		<!-- list board -->
		<div class="bo_list_ul">
      <?php
				$compatition_all_list_sql = "
						SELECT * ,
            case when now() between period1 and period2 then '접수중'
                when now() < period1 then ''
							   else '접수종료' end as is_accept
						,CASE
							WHEN date_format(date1, '%Y') = date_format(date2, '%Y')
							THEN concat(date_format(date1, '%Y-%m-%d'),' ~ ', date_format(date2, '%m-%d'))
							ELSE concat(date_format(date1, '%Y-%m-%d'),' ~ ', date_format(date2, '%Y-%m-%d'))
						END AS period
						FROM match_data where division = 2  and app_visible = 2 order by date1 desc limit 0, 5" ;
				$competition_all_list = sql_query($compatition_all_list_sql);?>
			<ul>
        <?php if($competition_all_list->num_rows < 1){?>
          <li style="padding:14px" class="text-center"><a href="#" >등록된 중계대회가 없습니다.</a></li>

        <?php }else {
              while($row = sql_fetch_array($competition_all_list)){?>
				<li>
					<a href="<?=G5_AOS_URL?>/competition_view.php?c=<?=$row['code']?>" class="list_a">
						<div class="champ_status">
							<span class="champ_ico champ_ico<?=$row['scale']?>"></span>
							<!-- <span class="limit_status"><?=$row['is_accept']?></span> -->
						</div>
						<span class="list_r_area">
							<i class="flaticon-right-arrow"></i>
						</span>
						<div class="subject">
							<div class="m_subject"><?=$row['wr_name']?></div>
							<div class="s_subject"><?=$row['period']?>&nbsp;&nbsp;<?=$row['opening_place']?></div>
						</div>
					</a>
				</li>
				<?php }?>
        <?php }?>
			</ul>
			<div class="quick_view">
				<a href="<?php echo G5_AOS_URL?>/competition_relay_list.php">
					중계대회 리스트 바로가기
				</a>
			</div>

		</div>
		<!-- //list board -->
	</section>
	<!-- //중계대회 -->

	<!-- 전체대회일정 -->
	<section>
		<div class="hd">
			<div class="tit"><i class="ico ico-star"></i>전체대회일정</div>
			<div class="r-btn-area">
				<ul>
					<li><a href="<?=G5_AOS_URL?>/competition_list.php" class="more_info">더보기</a></li>
				</ul>
			</div>
		</div>
		<!-- list board -->
		<div class="bo_list_ul">
			<?php
				$compatition_all_list_sql = "
						SELECT * ,
            case when now() between period1 and period2 then '접수중'
                when now() < period1 then ''
							   else '접수종료' end as is_accept
						,CASE
							WHEN date_format(date1, '%Y') = date_format(date2, '%Y')
							THEN concat(date_format(date1, '%Y-%m-%d'),' ~ ', date_format(date2, '%m-%d'))
							ELSE concat(date_format(date1, '%Y-%m-%d'),' ~ ', date_format(date2, '%Y-%m-%d'))
						END AS period
						FROM match_data where  app_visible = 2 order by date1 desc limit 0, 5";
				$competition_all_list = sql_query($compatition_all_list_sql, true);?>
			<ul>
        <?php if($competition_all_list->num_rows < 1){?>
          <li style="padding:14px" class="text-center"><a href="#" class="text-center">등록된 대회정보가 없습니다.</a></li>

        <?php }else {
              while($row = sql_fetch_array($competition_all_list)){?>
				<li>
					<a href="<?=G5_AOS_URL?>/competition_view.php?c=<?=$row['code']?>" class="list_a">
						<div class="champ_status">
							<span class="champ_ico champ_ico<?=$row['scale']?>"></span>
							<!-- <span class="limit_status"><?=$row['is_accept']?></span> -->
						</div>
						<span class="list_r_area">
							<i class="flaticon-right-arrow"></i>
						</span>
						<div class="subject">
							<div class="m_subject"><?=$row['wr_name']?></div>
							<div class="s_subject"><?=$row['period']?>&nbsp;&nbsp;<?=$row['opening_place']?></div>
						</div>
					</a>
				</li>
				<?php }?>
      <?php }?>
			</ul>
			<div class="quick_view">
				<a href="<?php echo G5_AOS_URL?>/competition_list.php">
					전체대회일정 리스트 바로가기
				</a>
			</div>

		</div>
		<!-- //list board -->
	</section>
	<!-- //전체대회일정 -->

	<!-- calander section-->
	<section>

		<div id="calendar">

		</div>
	</section>
	<!-- //calander section-->
</div>

<!-- champ_list -->

<!-- <script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/jquery/1.9.0/jquery.js"></script> -->

<script src='<?=G5_JS_URL?>/node_modules/fullcalendar/node_modules/moment/min/moment.min.js'></script>
<script src='<?=G5_JS_URL?>/node_modules/fullcalendar/dist/fullcalendar.js'></script>
<script src='<?=G5_JS_URL?>/node_modules/fullcalendar/dist/locale/ko.js'></script>
<script>
$(document).ready(function() {


    try {
      // console.log('{"name":"<?=get_session('ss_mb_name')?>","email":"<?=get_session('ss_mb_email')?>","addr1":"<?=get_session('addr1')?>","addr2":"<?=get_session('addr2')?>","club":"<?=get_session('club')?>"}');
      // window.Android.setUserInfo();
    } catch (e) {
      console.log(e);
    } finally {

    }


    // page is now ready, initialize the calendar...
    //
    // $('#calendar').fullCalendar({
		// 	aspectRatio: 5,
		// 	height:'auto',
		// 	header: {
		// 		left: '',
		// 		center: 'title',
		// 		right: 'month,agendaWeek'
		// 	}
		// 	, views: {
    //     month: { // name of view
    //         titleFormat: 'YYYY. MM'
    //         // other view-specific options here
    //     }
    // }
    //     // put your options and callbacks here
    // })

});
$('div.bo_list_ul ul a, #management').click(function(event){
	event.preventDefault();
	var link = $(this).attr('href');

	try {
										//link, title
										// console.log(link);
		window.Android.openLayer(link, '타이틀명');
	} catch (e) {
		console.log('web');
		location.href=link;
	} finally {

	}
});
$('#lnbMenu').click(function(event){
	event.preventDefault();
	try {
		window.Android.openLnb();
    console.log('openlng')
	} catch (e) {
		console.log('it\'s web');
	} finally {

	}
})

</script>


<?php
// include_once(G5_AOS_PATH.'/tail.php');
?>
