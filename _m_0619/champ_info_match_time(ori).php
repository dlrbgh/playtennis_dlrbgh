<?php
include_once('../common.php');

$wr_id = $_REQUEST['wr_id'];
$gym_court = $_REQUEST['gym_court'];
$gym_game = $_REQUEST['gym_game'];
$user_code	= $_REQUEST['user_code'];
$application_period = $_REQUEST['application_period'];

$time_court = $_REQUEST['time_court'];
$time_game = $_REQUEST['time_game'];
$act = 'champ_info_match'
?>
<?php require 'inc/config.php'; ?>
<?php require 'inc/views/template_head_start.php'; ?>

<!-- Page JS Plugins CSS -->
<link rel="stylesheet" href="<?php echo $one->assets_folder; ?>/js/plugins/slick/slick.min.css">
<link rel="stylesheet" href="<?php echo $one->assets_folder; ?>/js/plugins/slick/slick-theme.min.css">
<link rel="stylesheet" href="<?php echo $one->assets_folder; ?>/js/plugins/select2/select2.min.css">
<link rel="stylesheet" href="<?php echo $one->assets_folder; ?>/js/plugins/select2/select2-bootstrap.min.css">
<?php require 'inc/views/template_head_end.php'; ?>

<?php 
	$sql = "select * from match_data where wr_id = '$wr_id' order by wr_id desc";
	$result = sql_query($sql);
	$i = 1;
	$r = sql_fetch_array($result);
    $code = $r['code'];
	
    $sql = "select * from match_gym_data where match_id = '$code' and application_period = '$application_period' and  group by gym_id";
	$result = sql_query($sql);
	$gym_id = sql_fetch_array($result);
		
?>

<!-- sub nav -->
<?php include_once('./app_sub_nav.php'); ?>
<!-- end sub nav -->

<!-- Contents Area -->
	
	<div class="content" style="margin-top: 42px;">
	
		<div class="btn-group btn-group-justified push-10">
			<?php
				$sql = "select * from series_data where match_code = '$code' group by division";
				$result = sql_query($sql);
				$series = sql_fetch_array($result);
		    ?>
	        <div class="btn-group">
	            <a href="champ_info_match.php?wr_id=<?=$wr_id;?>&user_code=<?=$user_code;?>&user_club=<?=$user_club;?>&user_name=<?=$user_name;?>&division=<?=$series['division']?>"><button class="btn btn-lg btn-white " type="button">급수별 편성표</button></a>
	        </div>
	        <div class="btn-group">
	            <a href="champ_info_match_time.php?wr_id=<?=$wr_id;?>&user_code=<?=$user_code;?>&user_club=<?=$user_club;?>&user_name=<?=$user_name;?>&gym_court=<?=$gym_id['wr_id']?>&application_period=<?=$gym_id['application_period']?>&gym_game=1"><button class="btn active btn-lg btn-white" type="button">대진 시간표</button></a>
	        </div>
		</div>
		 
		<div class="row">
			<div class="col-md-6">
				<!-- 1일차 기본 선택  -->
				<?php  
					$sql = "select *,a.wr_id from match_gym_data as a join gym_data as b where a.gym_id = b.wr_id and  match_id = '$code' group by gym_id";
					$result = sql_query($sql);
					while($gym_data = sql_fetch_array($result)){
						$sqls = "select * from match_gym_data as a join gym_data as b where a.gym_id = b.wr_id and  match_id = '$code' and gym_id = '$gym_data[gym_id]'";
						$results = sql_query($sqls);
				?>
				
				<!-- <div class="champ-data push-10">
					<span class="push-10-r">춘천봄내체육관</span>
					<button class="active btn2">4월30일</button>
					<button class="btn2">5월1일</button>
				</div> -->				
				<div class="champ-data push-10">
					<span class="push-10-r"><?=$gym_data['gym_name']?></span>
					<?php while($gym_period = sql_fetch_array($results)){?>
						<a class="btn2 <?php if($application_period == $gym_period['application_period']) echo "active";?>" href="champ_info_match_time.php?wr_id=<?=$wr_id;?>&user_code=<?=$user_code;?>&user_club=<?=$user_club;?>&user_name=<?=$user_name;?>&gym_court=<?=$gym_data['wr_id']?>&application_period=<?=$gym_period['application_period']?>&gym_game=1"><?=$gym_period['application_period']?></a>
					<?php }?>
				</div>
				<?php		
					}
				?>
				<style>
					
				</style>
			</div>
		</div>
		<div class="row push-10">
			<div class="col-md-6">
				<span class="push-10-r">코트를 선택하시면 시간표가 출력됩니다.</span>
				<ul class="m_court_list">
					<?php  
						$sql = "select * from match_gym_data where match_id = '$code' and application_period='$application_period'";
						$result = sql_query($sql);
						$gym_data = sql_fetch_array($result);
					?>
					
					<?php for ($i = 1; $i <= $gym_data['use_court'] ; $i++) { ?>
					<li><a class="<?php if($gym_game == $i) echo "active";?>" href="champ_info_match_time.php?wr_id=<?=$wr_id;?>&user_code=<?=$user_code;?>&user_club=<?=$user_club;?>&user_name=<?=$user_name;?>e&gym_court=<?=$gym_court?>&application_period=<?=$application_period;?>&gym_game=<?=$i;?>" ><?php echo $i; ?></a></li>
					<?php } ?>	
				</ul>
			</div>
		</div>
		<!-- 접수현황 -->
	    <div class="row">
	        <div class="col-lg-12">
	        	<div class="block">
			        <div class="block-content_">
			            <table class="js-table-sections__ table table-mobile">
			                <!-- 그룹 경기 조회 타이틀 -->
			                <tbody class="js-table-sections-header open">
			                    <tr>
			                        <td class="font-w600" width="85%"><?=$gym_game;?> 코트 대진시간표 </td>
			                        <td class="text-center">
			                            <i class="fa fa-angle-right"></i>
			                        </td>
			                    </tr>
			                </tbody>
			                <!-- end 그룹 경기 조회 타이틀 -->
			                <tbody>
			                    
			                    <!-- 참가 선수 정보 및 대진 점수 -->
			                    <tr>
			                    	<td colspan="2" class="text-center">
			                    		<div class="row tournament_match">
											<?php	
												if($gym_court != ""){
													$sql = "select * from match_gym_data where match_id = '$code' and wr_id = '$gym_court'";
													$result = sql_query($sql);
													$gym_id = sql_fetch_array($result);
													
													$sql = "select * from game_score_data where match_code = '$code' and gym_code = '$gym_id[gym_id]' and game_court = '$gym_game' and game_date = '$application_period'  order by game_increase";
													$result = sql_query($sql);
													
												
												}
												$count = 1;
												while($score = sql_fetch_array($result)){
													$sql_team1 = "select * from team_data where team_code = '$score[team_1_code]'";
													$team1_result = sql_query($sql_team1);
													$team1 = sql_fetch_array($team1_result);
													
													$sql_team2 = "select * from team_data where team_code = '$score[team_2_code]'";
													$team2_result = sql_query($sql_team2);
													$team2 = sql_fetch_array($team2_result);
													
													$sql_gym = "select * from gym_data where wr_id = '$score[gym_code]'";
													$gym_result = sql_query($sql_gym);
													$gym = sql_fetch_array($gym_result);
													
												?>
			                    			<div class="col-xs-12 col-sm-6 col-md-6">
									            <a class="block block-rounded" href="javascript:void(0)">
									                <div class="block-options push-10-t push-10-r">
										                <li>
										                    <?=$score['game_code'];?>
										                </li>
										            </div>
									                <div class="tournament_header">
										               	<?php if($score['tournament'] == "L"){?>
									                    <span class="font-w700"><?=$score['division'];?> <?=$score['series'];?><?=$score['series_sub'];?> 리그전</span>
									                    	<?php if($gym_court != ""){?>
										                	<span class="font-w700"><?=$score['game_court'];?>코트 <?=$score['court_array_num'];?>경기</span>
										                	<span><?=$score['game_time'];?> </span>
										                	<?php }?>
										                	<?php if($time_court != ""){?>
										                	<span class="pull-left"><?=$gym['gym_name'];?></span> <span class="pull-right"><?=$score['game_date'];?> <?=$score['game_time'];?> <?=$score['game_court'];?>코트 <?=$score['court_array_num'];?>번 경기</span>
										                	<?php }?>
									                    <?php }?>
										               	<?php if($score['tournament'] == "T"){?>
									                    <span class="font-w700"><?=$score['division'];?> <?=$score['series'];?><?=$score['series_sub'];?> 토너먼트</span>
									                    	<?php if($gym_court != ""){?>
										                	<span class="font-w700"><?=$score['game_court'];?>코트 <?=$score['court_array_num'];?>경기</span>
										                	<span><?=$score['game_time'];?> </span>
										                	<?php }?>
										                	<?php if($time_court != ""){?>
										                	<span class="pull-left"><?=$gym['gym_name'];?></span> <span class="pull-right"><?=$score['game_date'];?> <?=$score['game_time'];?> <?=$score['game_court'];?>코트 <?=$score['court_array_num'];?>번 경기</span>
										                	<?php }?>
										               	<?php }?>
										               	<?php if($score['tournament'] == "C"){?>
									                    <span class="font-w700"><?=$score['division'];?> <?=$score['series'];?><?=$score['series_sub'];?> 결승</span>
									                    	<?php if($gym_court != ""){?>
										                	<span class="font-w700"><?=$score['game_court'];?>코트 <?=$score['court_array_num'];?>경기</span>
										                	<span><?=$score['game_time'];?> </span>
										                	<?php }?>
										                	<?php if($time_court != ""){?>
										                	<span class="pull-left"><?=$gym['gym_name'];?></span> <span class="pull-right"><?=$score['game_date'];?> <?=$score['game_time'];?> <?=$score['game_court'];?>코트 <?=$score['court_array_num'];?>번 경기</span>
										                	<?php }?>
										               	<?php }?>
									                </div>
									                <div class="tournament_content clearfix">
									                	<div class="pull-left text-center push-5-l">
									                    	<?php
									                			$favor_sql = "select count(*) as cnt from app_user_favor where user_code = '$user_code' and match_code = '$code' and team_code = '$score[team_1_code]'";
									                			$favor_result = sql_query($favor_sql);
																$favor = sql_fetch_array($favor_result);
																if($favor['cnt'] == 0){
															?>
															
									                    		<button class="btn_fav" onclick="favor('<?=$user_code?>','<?=$score['team_1_code']?>')">
									                    	<?php
																}else{
															?>
																<button class="btn_fav active" onclick="favor('<?=$user_code?>','<?=$score['team_1_code']?>')">
															<?php	
																}
									                		?>
										                    	<div class="left-star">
											                		<i class="fa fa-star"></i>
											                	</div>
										                        <div class="font-w600"><?=$team1['club'];?></div>
										                        <div class=""><?=$team1['team_1_name'];?>/<?=$team1['team_2_name'];?></div>
										                    </button>
									                    </div>
									                	<div class="tournament_point">
									                		<span class="left_team <?php if($score['team_1_score'] > $score['team_2_score']) echo "win";?>"><?=$score['team_1_score'];?></span>
									                		<span>vs</span>
									                		<span class="right_team <?php if($score['team_1_score'] < $score['team_2_score']) echo "win";?>"><?=$score['team_2_score'];?></span>
									                    </div>
									                    <div class="pull-right text-center push-5-r">
									                		<?php
									                			$favor_sql = "select count(*) as cnt from app_user_favor where user_code = '$user_code' and match_code = '$code' and team_code = '$score[team_2_code]'";
									                			$favor_result = sql_query($favor_sql);
																$favor = sql_fetch_array($favor_result);
																if($favor['cnt'] == 0){
															?>
															
									                    		<button class="btn_fav" onclick="favor('<?=$user_code?>','<?=$score['team_2_code']?>')">
									                    	<?php
																}else{
															?>
																<button class="btn_fav active" onclick="favor('<?=$user_code?>','<?=$score['team_2_code']?>')">
															<?php	
																}
									                		?>
										                		<div class="right-star">
											                		<i class="fa fa-star"></i>
											                	</div>
										                        <div class="font-w600"><?=$team2['club'];?></div>
										                        <div class=""><?=$team2['team_1_name'];?>/<?=$team2['team_2_name'];?></div>
									                        </button>
									                    </div>
									                </div>
									               <div class="coat_tit">
														<?php if($gym_court != ""){?>
									                	<span class="pull-left"><?=$gym['gym_name'];?></span> <span class="pull-right"><?=$score['game_date'];?></span>
									                	<?php }?>
									                	<?php if($time_court != ""){?>
									                	<span class="pull-left"><?=$gym['gym_name'];?></span> <span class="pull-right"><?=$score['game_date'];?> </span>
									                	<?php }?>									                
									                </div>
									            </a>
									        </div>
									        <!-- end 참가선수 정보 -->
											<?php $count++; } ?>	        
			                    		</div>
			                    	</td>
			                    </tr>
			                    <!-- end 참가 선수 정보 및 대진 점수 -->
			                    
			                </tbody>
			                
			            </table>
			        </div>
			    </div>
	        	
	        </div>
	    </div>
	</div>
<!-- end Contents Area -->



<?php require 'inc/views/base_footer.php'; ?>
<?php require 'inc/views/template_footer_start.php'; ?>


<script>
$( "#gym_court" ).change(function () {
	$('#gym_game').html("<option>잠시만 기다려주세요</option>");
	var wr_id = $( this ).val();    
	$.ajax({
		type: 'post',
		url:'./gym_court_data.php',
		dataType:'json',
		data: {wr_id:wr_id, code:'<?php echo $code;?>'},
		success:function(data){
			var str = '';
			$('#gym_game').html("");
			str += '<option>코트를 선택해주세요</option>';
			for(var i = 1 ; i <= data[0]['use_court']; i++){
				str += '<option value='+i+'>'+i+'코트</option>';
			}
			$('#gym_game').append(str);
	   	}
    });
});

$( "#time_court" ).change(function () {
	$('#time_game').html("<option>잠시만 기다려주세요</option>");
	var wr_id = $( this ).val();    
	$.ajax({
		type: 'post',
		url:'./time_court_data.php',
		dataType:'json',
		data: {wr_id:wr_id, code:'<?php echo $code;?>'},
		success:function(data){
			$('#time_game').html("");
			var str = '';
			for(var i = 0 ; i < data.length; i++){
				str += '<option value='+data[i]['game_time']+'>'+data[i]['game_time']+'</option>';
			}
			console.log(str);
			$('#time_game').append(str);
	   	}
    });
});
function favor(user_code,team_code){
	$.ajax({
		type: 'post',
		url:'./favorite_add.php',
		dataType:'json',
		data: {user_code:user_code,team_code:team_code,code:'<?php echo $code;?>'},
		success:function(data){
			console.log(data);
			location.reload(true);
			if(data[0]["result"] == "success"){
				window.AJInterface.callAndroid();
			}else if(data[0]["result"] == "cancel"){
				window.AJInterface.cancelAndroid();
			}
			// var str = '';
			// for(var sub in data){
				// console.log(sub);
				// str += '<option value='+data[sub]['series_sub']+'>'+data[sub]['series_sub']+'</option>';
			// }
			// console.log(str);
			
	   	}
    });
}
</script>
<!-- Page JS Plugins -->
<script src="<?php echo $one->assets_folder; ?>/js/plugins/select2/select2.full.min.js"></script>
<script src="<?php echo $one->assets_folder; ?>/js/plugins/datatables/jquery.dataTables.min.js"></script>


<!-- Page JS Code -->
<script src="<?php echo $one->assets_folder; ?>/js/pages/base_tables_datatables.js"></script>
<script>
    jQuery(function(){
        // Init page helpers (BS Datepicker + BS Datetimepicker + BS Colorpicker + BS Maxlength + Select2 + Masked Input + Range Sliders + Tags Inputs plugins)
        App.initHelpers(['table-tools','datepicker', 'datetimepicker', 'colorpicker', 'maxlength', 'select2', 'masked-inputs', 'rangeslider', 'tags-inputs']);
    });
</script>

<?php require 'inc/views/template_footer_end.php'; ?>

