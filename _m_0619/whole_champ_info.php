<?php
include_once('../common.php');

$wr_id = $_REQUEST['wr_id'];
$user_code	= $_REQUEST['user_code'];
$act = 'championship_info';
?>
<?php require 'inc/config.php'; ?>
<?php require 'inc/views/template_head_start.php'; ?>

<!-- Page JS Plugins CSS -->
<link rel="stylesheet" href="<?php echo $one->assets_folder; ?>/js/plugins/slick/slick.min.css">
<link rel="stylesheet" href="<?php echo $one->assets_folder; ?>/js/plugins/slick/slick-theme.min.css">

<?php require 'inc/views/template_head_end.php'; ?>
<?php 
	$sql = "select * from match_data where wr_id = '$wr_id' order by wr_id desc";
	$result = sql_query($sql);
	$i = 1;
	$r = sql_fetch_array($result);
	$code = $r['code'];
	$img_sql = "select * from g5_board_file where wr_id = '$wr_id' and bf_no = '2'";
	$img_result = sql_query($img_sql);
	$img = sql_fetch_array($img_result);
?>

<div class="menu_snav top_nav" id="home_tabs"  style="top: 0;">
	<a class="active"  href="index.php?user_code=<?=$user_code;?>&user_club=<?=$user_club;?>&user_name=<?=$user_name;?>">대회일정 안내</a>
	<a href="match_end.php?user_code=<?=$user_code;?>&user_club=<?=$user_club;?>&user_name=<?=$user_name;?>">배드민턴 뉴스</a>
</div>

<!-- Contents Area -->

	<div class="content" style="margin-top: 42px;">
	    <div class="row push-20">
	        <div class="col-lg-12">
	            <!-- 경기 일정 상세정보 출력 -->
	            <div class="block">
	                <div class="block-content remove-padding">
	                    <table class="table ">
	                        <tbody>
		                            <tr>
		                                <td width="76px" class="text-right">
										<?php if($r['scale'] == 1){echo "전국 대회";}else if($r['scale'] == 2){echo "지역 대회";}else if($r['scale'] == 3){echo "연합회 대회";}else if($r['scale'] == 4){echo "동호회 대회";}?>
										</td>
		                                <td><?=$r['wr_name'];?></td>
		                            </tr>
		                            <tr>
		                                <td class="text-right">대회기간</td>
		                                <td><?=$r['date1'];?> ~ <?=$r['date2'];?></td>
		                            </tr>
		                            <tr>
		                                <td class="text-right">접수기간</td>
		                                <td><?=$r['period1'];?> ~ <?=$r['period2'];?></td>
		                            </tr>
		                            <tr>
		                                <td class="text-right">대회지역</td>
		                                <td><?=$r['place'];?></td>
		                            </tr>
		                            
		                            <tr>
		                                <td class="text-right">주&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;관</td>
		                                <td><?=$r['organizer'];?></td>
		                            </tr>
		                            <tr>
		                                <td class="text-right">링크</td>
		                                <td><a href="http://<?=$r['link'];?>"><?=$r['link'];?></a></td>
		                            </tr>
		                             <tr>
		                                <td colspan="2">
		                                	<div class="font-w700 push-10">추가정보</div>
		                                	<?=$r['wr_content'];?>
		                                </td>
		                            </tr>
		                            <tr>
		                            	<td colspan="2">
		                            		<img class="img-responsive" src="../data/file/match/<?=$img['bf_file']?>" />
		                            	</td>
		                            </tr>
		                            
                        	        
	                        </tbody>
	                    </table>
	                </div>
	            </div>
	            <!-- end 경기 일정 상세정보 출력 -->
	        </div>
	    </div>
	</div>
	<!-- end Contents Area -->


<?php require 'inc/views/base_footer.php'; ?>
<?php require 'inc/views/template_footer_start.php'; ?>


<!-- Page JS Plugins -->
<script src="<?php echo $one->assets_folder; ?>/js/plugins/datatables/jquery.dataTables.min.js"></script>

<!-- Page JS Code -->
<script src="<?php echo $one->assets_folder; ?>/js/pages/base_tables_datatables.js"></script>
<?php require 'inc/views/template_footer_end.php'; ?>

