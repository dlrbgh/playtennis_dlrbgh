<?php
define('_INDEX_', true);
include_once('../common.php');
if (!defined('_GNUBOARD_')) exit; // 개별 페이지 접근 불가
include_once(G5_AOS_PATH.'/head.php');



$token = md5(uniqid(rand(), true));
set_session("ss_token", $token);
set_session("ss_cert_no",   "");
set_session("ss_cert_hash", "");
set_session("ss_cert_type", "");


?>
<style>
body{background-color:#fff}
</style>
<div class="login_hd_area">
	<div class="hd_lnb"><a href="javascript:history.go(-1)" id="back"><i class="flaticon-left-arrow"></i></a></div>
	<a class="title" href="<?php echo G5_AOS_URL?>">회원가입</a>
	<div class="hd_right"><a>&nbsp;</a></div>
</div>

<!-- 회원가입 -->
<form class="" action="../bbs/register_form_update.php" method="post" onsubmit="return verify()">
<div class="login_container">
	<?php
	if($msg != ''){?>
		<div class="warning"><?=$msg?></div>
	<?php }?>
	<div class="tbl_frm02">
		<table>
			<tbody>
				<tr>
					<th width="90">이메일</th>
					<td><input type="email" id="mb_id" name="mb_id" value="<?=$mb_id?>" placeholder="이메일 주소를 입력하세요" value="" /></td>
				</tr>

				<tr>
					<th>비밀번호</th>
					<td><input type="password" id="mb_password" name="mb_password" placeholder="비밀번호 입력하세요"  value=""/></td>
				</tr>
				<tr>
					<th>비밀번호 확인</th>
					<td><input type="password" id="mb_password_re" name="mb_password_re" placeholder="재확인 비밀번호 입력하세요"  value=""/></td>
				</tr>
			</tbody>
			<!-- <a href="register_step2.php" class="btn_default">다음</a> -->
		</table>
	</div>
	<!-- <a href="register_step2.php" class="btn_default">다음</a> --><div class="register_area">
		<div class="tip mb-10"class="text-center" style="text-align:center">
			<a href="./playtennis_private_policy.html" id="policy">개인정보 취급 방침</a>
		</div>

	</div>
	<div class="con_btn_area">
		<button type="submit">회원가입</a>
	</div>
	<style>
		/*회원가입 콘텐츠 버튼 */
.con_btn_area{position:relative;width:100%;display:block}
.con_btn_area button{background-color:#14264a;color:#fff;padding:20px 0;display:block;text-align:center;width:100%;border:1px solid #14264a}
.con_btn_area button:active{background-color:#bdbdbd;color:#000}
	</style>
</div>
<!-- //회원가입 -->

<div class="ft_area">
	<!-- <div class="ft_hd text-center">
		<a href="">비밀번호 찾기</a>
	</div> 
	<div class="btn_area">
		<button type="submit" name="button" >회원가입</button>
	</div>
	-->
</div>
</form>

<script type="text/javascript">

	$('#policy').click(function(event){
		event.preventDefault();
		try {
			window.Android.openLayer('http://koreatennis.com/m/playtennis_private_policy.html', '플레이테니스 개인정보 취급 방침');
		} catch (e) {
			console.log('itsweb')
		} finally {

		}
	})



function verify(){
	var mb_id = $('#mb_id').val();
	var mb_password = $('#mb_password').val();
	var mb_password_re = $('#mb_password_re').val();
	var msg = $('#msg');
	// return false;
	console.log(mb_id+' '+mb_password);
	$('input').removeClass('border_color5')
	if(mb_id == ''){
		$('#mb_id').attr('placeholder', '이메일주소를 입력하세요').addClass('border_color5')
		$('#mb_id').focus();
		return false;
	}else if(mb_password == ''){
		$('#mb_password').attr('placeholder', '비밀번호를 입력하세요').addClass('border_color5')
		$('#mb_password').focus();
		// $(msg).find('td').text('비밀번호를 입력하세요');
		return false;
	}else if(mb_password_re == ''){
		$('#mb_password_re').attr('placeholder', '재확인 비밀번호를 입력하세요').addClass('border_color5')
		$('#mb_password_re').focus();
		// $(msg).find('td').text('비밀번호를 입력하세요');
		return false;
	}
	return true;

}
</script>
<?php
include_once(G5_AOS_PATH.'/tail.php');
?>
