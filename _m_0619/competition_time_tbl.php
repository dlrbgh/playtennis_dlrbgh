<?php
include_once('../common.php');
if (!defined('_GNUBOARD_')) exit; // 개별 페이지 접근 불가
include_once(G5_AOS_PATH.'/head.php');
$menu_cate2 ='competition_view';
$menu_cate3 ='5';

include_once ('../s_adm/lib/tennis.common.php');
include_once ('./common_functions/Drawer.php');

$t = $t == '' ? $t = 'L' : $t;
$gp = $gp == '' ? 'A' : $gp;
?>

<!-- sub nav -->
<?php include_once('./app_sub_nav.php'); ?>
<!-- end sub nav -->
<div class="pop_container">

	<section class="section2">
			<!-- <div class="pop_hd">
				<div class="tit">그룹 선택</div>
			</div> -->
			<div class="content ">
				<div class="btn-group" role="group">
					<ul class="btn-list list2">
						<?php
							$series_sql = "select * from series_data where match_code='$c'";
							$series_list = sql_query($series_sql);
							while($row = sql_fetch_array($series_list)){
								if($d == '') {
									$d = $row['division'];
									$s= $row['series'];
									if($ss == '') $ss = $row['series_sub'];
								}
								?>
								<li><a href="?c=<?=$c?>&d=<?=$row['division']?>&s=<?=$row['series']?>&ss=<?=$row['series_sub']?>&t=<?=$t?>"
									<?=$d==$row['division'] && $s==$row['series'] && $ss==$row['series_sub'] ? 'class="active"':"" ?>>
								<?=$row['division']?> <?=$row['series']?> <?=$row['series_sub']?></a></li>
					<?php }?>
					</ul>
			</div>
			<div class="btn-group" role="group">
				<ul class="btn-list list2">
					<li><a <?=$t == 'L' ? 'class="active"' : ''?> href="?c=<?=$c?>&d=<?=$d?>&s=<?=$s?>&ss=<?=$ss?>&t=L">예선</a></li>
					<li><a <?=$t == 'T' ? 'class="active"' : ''?> href="?c=<?=$c?>&d=<?=$d?>&s=<?=$s?>&ss=<?=$ss?>&t=T">본선</a></li>
				</ul>
			</div>
				<?php if($t == 'L'){?>
					<ul class="btn-list">
						<?php
						$group_info_sql =
						"select * from group_data where
						 match_code = '{$c}'
						 and division = '{$d}' and series = '{$s}' and series_sub = '{$ss}' and tournament = 'L'
						 order by num";
						$group_info_result = sql_query($group_info_sql);
						if($group_info_result->num_rows >1){?>
							<li><a <?php if($gp=='A') print 'class="active"';?>
								href="?c=<?=$c?>&d=<?=$d?>&s=<?=$s?>&ss=<?=$ss?>&gp=A">전체</a></li>
							<?php
								$group_info_sql =
								"select * from group_data where
								match_code = '{$c}'
								and division = '{$d}' and series = '{$s}' and series_sub = '{$ss}' and tournament = 'L'
								order by num";
								$group_info_result = sql_query($group_info_sql);
								while($group = sql_fetch_array($group_info_result)){
									$group_num = $group['num'];
									if($gp==''){$gp = $group['num'];}
									?>
									<li><a <?php if($gp==$group_num) print 'class="active"';?>
										href="?c=<?=$c?>&d=<?=$d?>&s=<?=$s?>&ss=<?=$ss?>&t=<?=$t?>&gp=<?=$group_num?>"><?=$group_num+1?>조</a></li>
								<?php }
						}
						?>
					</ul>
			<?php }?>
		</div>
	</section>


	<section>
	  <div class="match_sts_area">
	  <?php

	    if($t == 'L'){
				if($gp == 'A'){
					$game_progress_sql = "
			    select end_game, count(*) as cnt from game_score_data
			    where
			    match_code = '{$c}'
			    and division = '{$d}'
			    and series = '{$s}'
			    and series_sub = '{$ss}'
					and tournament = 'L'
			    group by end_game";
				}else{
					$game_progress_sql = "
			    select end_game, count(*) as cnt from game_score_data
			    where
			    match_code = '{$c}'
			    and group_code = (select code from group_data
														where match_code = '{$c}' and division = '{$d}' and series = '{$s}' and series_sub = '{$ss}' and num = $gp)
					and tournament = 'L'
			    group by end_game";
				}
			}else{
				$game_progress_sql = "
				select end_game, count(*) as cnt from game_score_data
				where
				match_code = '{$c}'
				and division = '{$d}'
				and series = '{$s}'
				and series_sub = '{$ss}'
				and tournament <> 'L'

				group by end_game";
			}


	    // print $game_progress_sql;
	    $game_progress = sql_query($game_progress_sql);
	    $game;
	    $total_game = 0;
	    $game = array('N'=>0, 'Y'=>0);
	    while($row = sql_fetch_array($game_progress)){
	      $game[$row['end_game']] = $row['cnt'];
	      $total_game = $total_game+ ($row['cnt']-0);
	    }
	    // print_r($game);
	    $end_game_percentage = $game['Y']/$total_game * 100;
	    // $end_game_percentage = 40;
	    // print $end_game_percentage;
	    // print $total_game." ".$game['Y'];
			$sentence = "진행중";
			if($end_game_percentage == 100){
				$sentence = "종료";
			}

	   ?>
   <div id="progress" class="progress_bar " style="width:<?php print $end_game_percentage?>%;">
		<div class="ani_progress "></div>
   </div>
   <div class="progress_cnt">
   	<?php print $game['Y'];?>/<?php print $total_game;?><?php print $sentence?>
   </div>


	 </div>

	</section>

	<?php if($t == 'L'){?>
	<!-- 리그전일때 -->
	<section <?=$t == 'T' ? "style=display:none;" : ""?>>
		<?php
	 	$i = 0;
		$sql = "select * from series_data where match_code='$c'";
			$series_result = sql_query($sql);
		?>
		<?php
			//while($series = sql_fetch_array($series_result)){

		?>	<!-- 그룹명	-->
		<?php
			//점수 삽입
			$team_field = "team_data";

			if($d == "단체전"){
				$team_field = "team_event_data";
			}

			$sql_team = "select count(wr_id) as cnt from $team_field where match_code='$c'
			and division='{$d}'
			and series ='{$s}'
			and series_sub = '{$ss}'";
			$team_result = sql_query($sql_team);
			$r = sql_fetch_array($team_result);

			$sql_teams = "select count(wr_id) as cnt from group_data where match_code='$c'
			and division='{$d}'
			and series ='{$s}'
			and series_sub = '{$ss}'";
			// print $sql_teams;
			$team_results = sql_query($sql_teams);
			$rs = sql_fetch_array($team_results);
			// print_r($rs);
			?>
				<?php
					$i = 0;
					$sql = "select * from group_data where match_code='$c'
					and division='{$d}'
					and series='{$s}'
					and series_sub='{$ss}' and tournament	='L'";

					if($gp != 'A'){
						$sql .=" and num = {$gp} ";
					}
					$sql .=" order by num";
					// print $sql;
					$gourp_result = sql_query($sql);
				while($group = sql_fetch_array($gourp_result)){
					$group_code = $group['code'];
					// print $group_code;
					$gym_name = sql_fetch("select gym_name from gym_data where wr_id = (SELECT gym_code FROM game_score_data where group_code = '{$group_code}' group by gym_code limit 1)", true);
					$game_status = is_on($group_code);
					?>

					<section>
						<div class="content">
							<div class="con_tit_area clear">
								<div class="tit">
									<?=$group['division']?> <?=$group['series']?> <?=$group['series_sub']?>-<?php echo $group['num']+1?>조
								</div>
								<div class="r-area">
									<ul>
										<li class="color5 fw-700"><?=$gym_name['gym_name']?>
											<?php
												$game_court_result = sql_fetch("select game_court from game_score_data
												where match_code = '{$c}' and group_code = '{$group['code']}'
												and is_on = 'Y'");
												if(count($game_court_result) > 0){
													$game_court = $game_court_result['game_court']."코트";
												}
											 ?>
											<?=$game_court?></li>
									</ul>
								</div>
							</div>
							<div class="tbl_style01 tbl_striped mb-20">
								<table>
									<?php
										$team_index = 1;
										$team_result_data = array();
										while($group['team_'.$team_index] != ''){
											$group_team_code = $group['team_'.$team_index];
											$team_sql = "select * from $team_field
											where match_code = '$c'
											and team_code = '{$group_team_code}'
											order by group_rank
											";
											// print $team_sql;
											$team_result = sql_query($team_sql);
											$r = sql_fetch_array($team_result);
											$team_result_data[] =$r; $team_index++;}
									?>
											<thead>
												<tr>
														<th>
															<?php
															if($game_status['status']==3){
																echo "순위";
															}else{
																echo "번호";
															}
															?>

														</th>
														<th>클럽</th>
														<th>선수</th>
														<th>승</th>
														<th>패</th>
												</tr>
											</thead>
											<tbody class="text-center">


													<?php


														usort($team_result_data, "cmp");
														// $ranking  = 1;
														foreach($team_result_data as $key => $value){

															$go_tournament = '';
															if($game_status['status'] == 3 && $ranking == 1){
																// $go_tournament = '<br />토너먼트 진출';
															}
													?>
														<tr>

														<td class="text-center cuttent_rating_sts">

															<?php

															if($value['group_rank'] > 0 && $value['group_rank'] < 3){
																print $value['group_rank']."위";
															}else{
																print "-";
															}
															// if($game_status['status']==3){
															// 	echo "위";
															// }else{
															// 	echo "";
															// }
															?>
															<?=$go_tournament?>
														</td>
														<td><?=$value['area_2'] ?>-<?=$value['club']?></td>
														<td>
															<?=$value['team_1_name'];?>(<?=$value['team_1_grade'];?>)<br>
															<?=$value['team_2_name'];?>(<?=$value['team_2_grade'];?>)<br>
															<?php if($d == "단체전"){ ?>
																<?=$value['team_3_name'];?>(<?=$value['team_3_grade'];?>)<br>
																<?=$value['team_4_name'];?>(<?=$value['team_4_grade'];?>)<br>
																<?=$value['team_5_name'];?>(<?=$value['team_5_grade'];?>)<br>
																<?=$value['team_6_name'];?>(<?=$value['team_6_grade'];?>)<br>
																<?php if($value['team_7_name']!=""){echo $value['team_7_name'];echo "(";echo $value['team_7_grade'];echo ")";}?><br>
																<?php if($value['team_8_name']!=""){echo $value['team_8_name'];echo "(";echo $value['team_8_grade'];echo ")";}?><br>
																<?php } ?>
														</td>
														<td><?=$value['team_league_point']?>승</td>
														<td><?=$value['team_league_lose_count']?>패</td>
													</tr>
													<?php $team_index++;}?>
											</tbody>
										</table>
							</div>
							<?php if($gp != 'A'){?>

							<div class="con_tit_area clear">
								<div class="tit">경기결과</div>
							</div>
							<div class="tbl_style02 tbl_striped">
								<table>
									<thead>
										<tr>
											<!-- <th>경기</th> -->
											<th>이름</th>
											<th>점수</th>
											<th>이름</th>
										</tr>
									</thead>
									<tbody>
										<?php
											$game_score_sql = "select * from game_score_data
											where group_code = '{$group_code}' and tournament = 'L' order by court_array_num desc, wr_id desc";
											// print ">>>>>>".$game_score_sql;
											$game_score = sql_query($game_score_sql);
											while($row = sql_fetch_array($game_score)){
												if($group['division'] == '단체전'){
													$team_left = $row['team_1_code'];
													$team_right = $row['team_2_code'];
													$team_1_info = sql_fetch("select * from team_data where team_code = '{$row['team_1_event_code']}'");
													$team_2_info = sql_fetch("select * from team_data where team_code = '{$row['team_2_event_code']}'");
													print_r($team_1_info);
												}else{
													$team_1_info = sql_fetch("select * from team_data where team_code = '{$row['team_1_code']}'");
													$team_2_info = sql_fetch("select * from team_data where team_code = '{$row['team_2_code']}'");
												}
												?>
												<tr>
													<!-- <td class="text-center"><?=$game_result_list[$row['code']]?><br>경기</td> -->

													<td class="text-center">

														<div class="match_member_area">
														<?php
														if($row['division'] == "단체전"){
															if($row['team_1_event_code'] == ""){
															$team_left_name = sql_fetch("select * from team_event_data where team_code='$team_left'");
															?>
																<?=$team_left_name['area_2']."-".$team_left_name['club']?>
															<?php
																}else{
															?>
															<div class="team_area">
																<div class="club"><?= $team_1_info['area_2']."-".$team_1_info['club']?></div>
																<div class="name">
																	<?php if($team_1_info['team_1_name']!=""){
																		echo $team_1_info['team_1_name'];echo "(";echo $team_left_name['team_1_grade'];echo ")";}?>
																</div>
																<div class="name">
																	<?php if($team_1_info['team_2_name']!=""){
																		echo $team_1_info['team_2_name'];echo "(";echo $team_left_name['team_2_grade'];echo ")";}?>
																</div>
															</div>
															<?php }
															}else{
															?>
															<div class="team_area">
																<div class="club"><?= $team_1_info['area_2']."-".$team_1_info['club']?></div>
																<div class="name">
																	<?php if($team_1_info['team_1_name']!=""){
																		echo $team_1_info['team_1_name'];echo "(";echo $team_1_info['team_1_grade'];echo ")";}?>
																</div>
															</div>
															<div class="team_area">
																<div class="club"><?= $team_1_info['area_2']."-".$team_1_info['team_2_club'];?></div>
																<div class="name">
																	<?php if($team_1_info['team_2_name']!=""){
																		echo $team_1_info['team_2_name'];echo "(";echo $team_1_info['team_2_grade'];echo ")";}?>
																</div>
															</div>
															<?php }?>
														</div>
													</td>

													<td class="match_point text-center">
															<?php if($row['team_1_dis'] == "1"){
																print '<div class="skip point_box"><span>ㅡ</span>&nbsp;&nbsp;&nbsp;&nbsp;<span>승</span></div>';
															}else if($row['team_2_dis'] == "1"){
																print '<div class="skip point_box"><span>승</span>&nbsp;&nbsp;&nbsp;&nbsp;<span>ㅡ</span></div>';
															}else if($row['team_1_dis'] == "2"){
																print '<div class="skip point_box"><span>실격</span><span>&nbsp;&nbsp;&nbsp;&nbsp;ㅡ&nbsp;&nbsp;&nbsp;&nbsp;</span></div>';
															}else if($row['team_2_dis'] == "2"){
																print '<div class="skip point_box"><span>&nbsp;&nbsp;&nbsp;&nbsp;-&nbsp;&nbsp;&nbsp;&nbsp;</span><span>실격</span></div>';
															}else if($row['team_1_dis'] == "3"){
																print '<div class="skip"><span>경기안함</span></div>';
															}else{
															 if($row['is_on'] == 'N' && $row['end_game'] == 'N'){?>
															<!-- <div><span></span>&nbsp;준비중&nbsp;<span></span></div> -->
															<div class="">
																<span style="font-size:16px">준비중</span>
															</div>
														<?php }else if($row['end_game'] == 'Y'){?>
															<div class="point_box"><span class="win"><?=$row['team_1_score']?></span>&nbsp;&nbsp;<span><?=$row['team_2_score']?></span></div>
														<?php }else if($row['is_on'] == 'Y' && $row['game_start_time'] != ''){?>
															<div class="playing ani01 color6">
																<?php print $row['game_court']?>코트 진행중
															</div>
														<?php }

														}?>
														<!-- <?php if($row['is_on'] == 'N' && $row['end_game'] == 'N'){?>
															<span></span>
															준비중
															<span></span>
														<?php }else if($row['end_game'] == 'Y'){?>
															<?=$row['team_1_score']?> : <?=$row['team_2_score']?>
														<?php }else if($row['is_on'] == 'Y' && $row['game_start_time'] != ''){?>
															<span></span>
															진행중
															<span></span>
														<?php }?> -->
													</td>
													<td class="text-center">
														<div class="match_member_area">
														<?php
														if($row['division'] == "단체전"){
															if($row['team_1_event_code'] == ""){
															$team_right_name = sql_fetch("select * from team_event_data where team_code='$team_right'");
															?>
																<?=$team_right_name['area_2']."-".$team_right_name['club']?>
															<?php
																}else{
															?>
															<div class="team_area">
																<div class="club"><?= $team_2_info['area_2']."-".$team_2_info['club']?></div>
																<div class="name">
																	<?php if($team_2_info['team_1_name']!=""){
																		echo $team_2_info['team_1_name'];echo "(";echo $team_right_name['team_1_grade'];echo ")";}?>
																</div>
																<div class="name">
																	<?php if($team_2_info['team_2_name']!=""){
																		echo $team_2_info['team_2_name'];echo "(";echo $team_right_name['team_2_grade'];echo ")";}?>
																</div>
															</div>
															<?php }
															}else{
															?>
															<div class="team_area">
																<div class="club"><?= $team_2_info['area_2']."-".$team_2_info['club']?></div>
																<div class="name">
																	<?php if($team_2_info['team_1_name']!=""){
																		echo $team_2_info['team_1_name'];echo "(";echo $team_2_info['team_1_grade'];echo ")";}?>
																</div>
															</div>
															<div class="team_area">
																<div class="club"><?= $team_2_info['area_2']."-".$team_2_info['team_2_club'];?></div>
																<div class="name">
																	<?php if($team_2_info['team_2_name']!=""){
																		echo $team_2_info['team_2_name'];echo "(";echo $team_2_info['team_2_grade'];echo ")";}?>
																</div>
															</div>
															<?php }?>
														</div>

													</td>
												</tr>
											<?php }?>
									</tbody>
								</table>
							</div>
							<?php }?>
						</div>
					</section>
			</section>
			<?php $i++;} ?>

			<?php } else if($t == 'T'){?>
			<section <?=$t == 'L' ? 'style="display:none"':""?>>
				<div class="content">

				<div class="con_tit_area clear">
					<div class="tit">본선진행 중계</div>
					<div class="r-area">
						<ul>
							<!-- <li class="color5 fw-700">8강 진행중</li> -->
						</ul>
					</div>
				</div>
				<div class="tab_group1 mb-0">
					<div id="table" style="overflow-x:scroll; width:100%; overflow:scroll;"></div>
					<script type="text/javascript" src="<?php print G5_JS_URL?>/tournament/jquery.bracket.min.js"></script>
					<link rel="stylesheet" type="text/css" href="<?php print G5_JS_URL?>/tournament/jquery.bracket.min.css" />
					<script>

					var singleElimination = {
					  "teams": [
					    <?php
								$groupcount_result = sql_fetch("  select count(*) * 2  as count from group_data where
									match_code = '{$c}' and division = '{$d}' and series = '{$s}' and series_sub='{$ss}'
								");
								$groupcount = $groupcount_result['count'];
							 print get_rounds_example($groupcount)?>
					  ],

						<?php
							//빈칸을 다 만들어주고
							//경기를 불러와서 넣어줘야함

							$finish = true;
							$round = 2;
							while ($finish){
								$tmp = $groupcount - $round ;
								if($tmp > $round){
									$round *= 2;
								}else{
									$finish = false;
								}
							}
							// print ">>>>>>>>>>>".$round;
							$empty_score;
							$finish = true;
							while($finish){
								for($i = 0; $i < $round; $i++){
									$empty_score[$round][$i] =array(0,0);
								}
								$round = $round/2;
								// if($round == )
								if($round == 1){
									$finish = false;
								}
							}
							// print_r($empty_score);
							$score_list = "select team_1_score, team_2_score, tournament_count, tournament_num from game_score_data

							where match_code = '{$c}' and division = '{$d}' and series = '{$s}' and series_sub = '{$ss}'
							order by tournament_count, tournament_num";
							// print $score_list;
							$score_result = sql_query($score_list, true);
							while($score = sql_fetch_array($score_result)){
								$empty_score[$score['tournament_count']][$score['tournament_num']]
									= array($score['team_1_score'], $score['team_2_score']);
							}
						 ?>
						"results":
						[
							<?php
								$is_first = false;
								foreach($empty_score as $round=>$score){
									if($is_first){
										print ",";
									}else{
										$is_first = true;
									}
									print "[";
									for($i = 0; $i < $round; $i++){
										if($i >0){print ",";}
										print "[".join(',',$score[$i])."]";
									}
									if($round == 0){
										print "[".join(',',$score[0])."]";
									}
									print "]";
								}

							?>
						]

					}


					// These are modified by the sliders
					var resizeParameters = {
					  teamWidth: 222222,
					  scoreWidth: 20,
					  matchMargin: 133330,
					  roundMargin: 50,
					  init: singleElimination
					};

					function updateResizeDemo() {
						$('#table').bracket({init:singleElimination})
					}
					$(updateResizeDemo)
					</script>
				</div>
				 <div class="tab_group1 mb-0">
				<?php

				$round_sql = "select tournament_count,
									case when tournament_count = 0 then '결승'
									else concat(tournament_count*2, '강') end as round from game_score_data
									where match_code = '{$c}' and tournament <> 'L'
									and division='{$d}'
									and series ='{$s}'
									and series_sub = '{$ss}'
									group by tournament_count order by tournament_count desc";
					$round = sql_query($round_sql);
					while($row = sql_fetch_array($round)){
						if($rd == '') $rd = $row['tournament_count'];
						$round_list[] = $row;
						?>
						<a href="?c=<?=$c?>&d=<?=$d?>&s=<?=$s?>&ss=<?=$ss?>&t=T&rd=<?=$row['tournament_count']?>" class="round_tab
							<?=($rd == $row['tournament_count'] ? 'active' : '')?>"
							 data-for="<?=$row['tournament_count']?>" >
							 <?=$row['round']?></a>
					<?php }?>

				 </div>

				 <div class="mb-20">
					 <?php
					 $this_round = '';
					 $round_for_state = '';
					 	foreach($round_list as $key => $value){
							if($this_round == '' || $this_round != $value['round']){
								$this_round = $value['round'];
							}
							?>
							<ul class="tournament_list active" id="<?=$value['tournament_count']?>"
								<?=$rd == $value['tournament_count'] ? "" : 'style="display:none"'?>
								>
								<?php
									$tournament_sql = "
									select *
									from game_score_data
									where match_code = '{$c}' and tournament <> 'L'
									and division='{$d}'
									and series ='{$s}'
									and series_sub = '{$ss}'
									and tournament_count = {$value['tournament_count']}
									order by tournament_num";
									$tournament_round = sql_query($tournament_sql, true);
									// print $tournament_sql;

									 ?>
									<div class="status_banner"><?=$value['round']?> 토너먼트</div>
									<?php while($row = sql_fetch_array($tournament_round)){
										if($d == '단체전'){
											// print "select * from team_data where team_code = '{$row['team_1_event_code']}'";
											// print ">>>>".$row['team_1_event_code']."<br />";
											// print ">>>>".$row['team_1_code'];
											if($row['team_1_event_code'] == ''){
												$team_1_info = sql_fetch("select * from team_event_data where team_code = '{$row['team_1_code']}'");
											}else {
												$team_1_info = sql_fetch("select * from team_data where team_code = '{$row['team_1_event_code']}'");
											}
											if($row['team_2_event_code'] == ''){
												$team_2_info = sql_fetch("select * from team_event_data where team_code = '{$row['team_2_code']}'");
											}else{
												$team_2_info = sql_fetch("select * from team_data where team_code = '{$row['team_2_event_code']}'");
											}
											// $team_1_info = sql_fetch("select * from team_data where team_code = '{$row['team_1_event_code']}'");
											// $team_2_info = sql_fetch("select * from team_data where team_code = '{$row['team_2_event_code']}'");
										}else{
											$team_1_info = sql_fetch("select * from team_data where team_code = '{$row['team_1_code']}'");
											$team_2_info = sql_fetch("select * from team_data where team_code = '{$row['team_2_code']}'");
										}
										?>
										<li>
											<div class="tournament_match">
												<div class="tournament_hd">
													<span><?=$value['round']?> <?=$row['game_court']?>코트 </span>
													<span class="r-side-area"></span>
												</div>
												<div class="tournament_content">
													<div class="l-area">
													<?php if($row['team_1_code'] == ''){?>
														<div class="teamA"><?php print $row['assigned_group_name1'];?></div>
														<?php }else{?>
														<div class="teamA"><?= $team_1_info['club']?> - <?= $team_1_info['team_1_name']?></div>
														<div class="teamA"><?= $team_1_info['club']?> - <?= $team_1_info['team_2_name']?></div>
														<?php if($debug){?><div class="teamA"><?php print $row['assigned_group_name1'];?></div> <?php }?>
														<?php }?>
													</div>

												<div class="tournament_point">
													<div class="result_point">


														<?php if($row['team_1_dis']=="1"){?>
														<div class="point_box">
															<div class="skip"><span>기권</span><span>&nbsp;&nbsp;&nbsp;&nbsp;<?=$row['team_2_score']?>&nbsp;&nbsp;&nbsp;&nbsp;</span></div>
														</div>
													<?php }else if($row['team_1_dis']=="2"){?>
														<div class="point_box">
															<div class="skip"><span>실격</span><span style="width:50px">&nbsp;&nbsp;&nbsp;&nbsp;<?=$row['team_2_score']?>&nbsp;&nbsp;&nbsp;&nbsp;</span></div>
														</div>
													<?php }else if($row['team_1_dis']=="3"){?>
																			<div class="skip"><span>경기안함</span></div>
													<?php }else if($row['team_2_dis'] == 1){?>
														<div class="point_box">

															<div class="skip"><span>&nbsp;&nbsp;&nbsp;&nbsp;<?=$row['team_1_score']?>&nbsp;&nbsp;&nbsp;&nbsp;</span><span>기권</span></div>
														</div>
													<?php }else if($row['team_2_dis']=="2"){?>
														<div class="point_box">
															<div class="skip"><span>&nbsp;&nbsp;&nbsp;&nbsp;<?=$row['team_1_score']?>&nbsp;&nbsp;&nbsp;&nbsp;</span><span>실격</span></div>
														</div>
													<?php }else if($row['team_2_dis']=="3"){?>
														<span>경기안함</span>
													<?php }else if($row['is_on'] == 'N' && $row['end_game'] == 'N'){?>
															<div class="">
																<span style="font-size:16px">준비중</span>
															</div>
														<?php }else if($row['end_game'] == 'Y'){?>
															<div><span class="win"><?=$row['team_1_score']?></span>&nbsp;:&nbsp;<span><?=$row['team_2_score']?></span></div>
														<?php }else if($row['is_on'] == 'Y' && $row['game_start_time'] != ''){?>
															<div class="playing ani01">진행중</div>
														<?php }?>





													</div>
												</div>
												<div class="r-area">
													<?php if($row['team_2_code'] == ''){?>
														<div class="teamB"><?php print $row['assigned_group_name2'];?></div>
														<?php }else{?>
															<div class="teamB"><?= $team_2_info['team_2_name']?> - <?= $team_2_info['club']?></div>
															<div class="teamB"><?= $team_2_info['team_1_name']?> - <?= $team_2_info['club']?></div>
															<?php if($debug){?><div class="teamB"><?php print $row['assigned_group_name2'];?></div> <?php }?>
														<?php }?>
												</div>
												</div>
												<div class="tournament_ft">
													<span>
														<?php

														$gym_info = sql_fetch("select gym_name from gym_data where wr_id = {$row['gym_code']}",true);
														print $gym_info['gym_name'];
														?>
													</span>
										 			<span class="r-side-area"><?=$row['game_date']?></span>
										 		</div>
											</div>
										</li>
									<?php }?>
							</ul>
						<?php }?>
			 </div>

			 </div>
			</section>
<?php }?>
</div>
<?php

function cmp($a, $b){
	// if($a)
	return $a['group_rank'] - $b['group_rank'];
}

function is_on($group_code, $tournament_sql = " and tournament = 'L'"){

	$game_state_array = array(1=>'예정', 2=>'진행중', 3=>'종료');
	$sql = "select end_game, is_on, ifnull(game_start_time,'') as game_start_time
	,  ifnull(game_end_time,'') as game_end_time from game_score_data where group_code = '{$group_code}' $tournament_sql";
	// print $sql;
	$result = sql_query($sql,true);
	$game = $result->num_rows;
	$end_game = array('N'=>0, 'Y'=>0);
	$is_on  = array('N'=>0, 'Y'=>0);
	$game_start_time;
	$game_end_time;
	while($row = sql_fetch_array($result)){
		$end_game[$row['end_game']] =$end_game[$row['end_game']] +1;
		$is_on[$row['is_on']] = $is_on[$row['is_on']] +1;
		$game_start_time[$row['game_start_time']]
		= $game_start_time[$row['game_start_time']] == '' ? 1 :  ($game_start_time[$row['game_start_time']]-0 )+1;

	}
	if($end_game['N'] == $game){
		if($game_start_time[''] == $game){
			return array('status'=>1, 'status_display'=>$game_state_array[1]);
		}else if($game_start_time[''] < $game){
			return array('status'=>2, 'status_display'=>$game_state_array[2]);
		}
	}
	if($end_game['Y'] == $game) {
		return array('status'=>3, 'status_display'=>$game_state_array[3]);
	}
}
 ?>
<?php
include_once(G5_AOS_PATH.'/tail.php');
?>
