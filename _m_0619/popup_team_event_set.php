<?php
define('_INDEX_', true);
include_once('../common.php');
if (!defined('_GNUBOARD_')) exit; // 개별 페이지 접근 불가
include_once(G5_AOS_PATH.'/head.php');
//$menu_cate2 ='competition_view';
$menu_cate3 ='6';




?>
<?php
$sql = "select a.*,b.division,b.series,b.series_sub,b.num from game_score_data as a inner join group_data as b
		where a.code = '$game_code' and a.group_code = b.code group by a.game_increase order by a.game_increase asc";
$game_result = sql_query($sql);
$game_data = sql_fetch_array($game_result);
// print_r($game_data);
$team_field = "team_data";

if($game_data['division'] == "단체전"){
	$team_field = "team_event_data";
}
if($team == "1"){
	$sql_team1 = "select * from $team_field where match_code = '{$game_data[match_code]}' and team_code = '$game_data[team_1_code]'";
}
if($team == "2"){
	$sql_team1 = "select * from $team_field where match_code = '{$game_data[match_code]}' and team_code = '$game_data[team_2_code]'";
}


// print $sql_team1;
$team1_result = sql_query($sql_team1);
$team1 = sql_fetch_array($team1_result);

?>
<!-- sub nav -->
<?php include_once('./app_sub_nav.php'); ?>
<!-- end sub nav -->


<!-- Contents Area -->
<div class="pop_container pt-50">

	<section>
		<div class="pop_hd">
			<div class="tit"><?php echo $team1['club']?>클럽  단체전 </div>
		</div>


		<form name="frmReview" action="insert_team_event_data.php" method="post" id="frmReview" >

		<input type="hidden" name="game_code" value="<?php echo $game_code;?>" />
		<input type="hidden" name="team_chk" value="<?php echo $team;?>" />
		<input type="hidden" name="tournament" value="<?php echo $game_data['tournament'];?>" />
		<input type="hidden" name="rd" value="<?php echo $rd;?>" />

			<div class="tbl_style02 tbl_striped">
				<table>
					<thead>
						<tr>
							<th>이름</th>
							<th>1경기</th>
							<th>2경기</th>
							<th>3경기</th>
							<th>미배정</th>
						</tr>
					</thead>
					<tbody class="text-center">
						<tr>
							<Td><?php echo $team1['team_1_name'];?></Td><td><input type="radio" name="team_1[0]" value="1" /></td><td><input name="team_1[0]" type="radio" value="2" /></td><td><input name="team_1[0]" type="radio" value="3" /></td><td><input name="team_1[0]" type="radio" checked="" value="" /></td>
						</tr>
						<tr>
							<Td><?php echo $team1['team_2_name'];?></Td><td><input type="radio" name="team_1[1]" value="1" /></td><td><input name="team_1[1]" type="radio" value="2" /></td><td><input name="team_1[1]" type="radio" value="3" /></td><td><input name="team_1[1]" type="radio" checked="" value="" /></td>
						</tr>
						<tr>
							<Td><?php echo $team1['team_3_name'];?></Td><td><input type="radio" name="team_1[2]" value="1" /></td><td><input name="team_1[2]" type="radio" value="2" /></td><td><input name="team_1[2]" type="radio" value="3" /></td><td><input name="team_1[2]" type="radio" checked="" value="" /></td>
						</tr>
						<tr>
							<Td><?php echo $team1['team_4_name'];?></Td><td><input type="radio" name="team_1[3]" value="1" /></td><td><input name="team_1[3]" type="radio" value="2" /></td><td><input name="team_1[3]" type="radio" value="3" /></td><td><input name="team_1[3]" type="radio" checked="" value="" /></td>
						</tr>
						<tr>
							<Td><?php echo $team1['team_5_name'];?></Td><td><input type="radio" name="team_1[4]" value="1" /></td><td><input name="team_1[4]" type="radio" value="2" /></td><td><input name="team_1[4]" type="radio" value="3" /></td><td><input name="team_1[4]" type="radio" checked="" value="" /></td>
						</tr>
						<tr>
							<Td><?php echo $team1['team_6_name'];?></Td><td><input type="radio" name="team_1[5]" value="1" /></td><td><input name="team_1[5]" type="radio" value="2" /></td><td><input name="team_1[5]" type="radio" value="3" /></td><td><input name="team_1[5]" type="radio" checked="" value="" /></td>
						</tr>
						<tr>
							<Td><?php echo $team1['team_7_name'];?></Td><td><input type="radio" name="team_1[6]" value="1" /></td><td><input name="team_1[6]" type="radio" value="2" /></td><td><input name="team_1[6]" type="radio" value="3" /></td><td><input name="team_1[6]" type="radio" checked="" value="" /></td>
						</tr>
						<tr>
							<Td><?php echo $team1['team_8_name'];?></Td><td><input type="radio" name="team_1[7]" value="1" /></td><td><input name="team_1[7]" type="radio" value="2" /></td><td><input name="team_1[7]" type="radio" value="3" /></td><td><input name="team_1[7]" type="radio" checked="" value="" /></td>
						</tr>
					</tbody>
				</table>
			</div>

			<div class="ft_area">
				<div class="btn_area">
					<input type="submit" value="적용">
				</div>
			</div>
		</form>
	</section>
</div>
<!-- end Contents Area -->
<script>

$("form#frmReview").submit(function(event){
	var checked1 = 0;
	var checked2 = 0;
	var checked3 = 0;

	$("input[type=radio]").each(function(index,elem){

		if($(elem).is(":checked")){

			if($(elem).val() == "1"){
				checked1++;
			}
			if($(elem).val() == "2"){
				checked2++;
			}
			if($(elem).val() == "3"){
				checked3++;
			}
		}
	})

	if(checked1 != "2"){
		alert("1경기를 2명 선택해주세요");
		return false;
	}
	if(checked2 != "2"){
		alert("2경기를 2명 선택해주세요");
		return false;
	}
	if(checked3 != "2"){
		alert("3경기를 2명 선택해주세요");
		return false;
	}
	return true;

});

</script>

<?php
include_once(G5_AOS_PATH.'/tail.php');
?>
