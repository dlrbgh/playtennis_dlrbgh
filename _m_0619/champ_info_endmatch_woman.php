<?php
include_once('../common.php');

$wr_id = $_REQUEST['wr_id'];
$user_code	= $_REQUEST['user_code'];
$act = 'champ_info_endmatch'
?>
<?php require 'inc/config.php'; ?>
<?php require 'inc/views/template_head_start.php'; ?>


<?php require 'inc/views/template_head_end.php'; ?>

<?php 
	$sql = "select * from match_data where wr_id = '$wr_id' order by wr_id desc";
	$result = sql_query($sql);
	$i = 1;
	$r = sql_fetch_array($result);
    $code = $r['code'];
?>

<!-- sub nav -->
<?php include_once('./app_sub_nav.php'); ?>
<!-- end sub nav -->

<!-- Contents Area -->
	<div class="content" style="margin-top: 42px;">
<!-- 카테고리 -->
		<div class="btn-group btn-group-justified push-10">
	        <div class="btn-group">
	            <a href="champ_info_endmatch.php?wr_id=<?=$wr_id;?>&user_code=<?=$user_code;?>"><button class="btn btn-lg btn-white remove-padding " type="button">남자복식</button></a>
	        </div>
	        <div class="btn-group">
	            <a href="champ_info_endmatch_woman.php?wr_id=<?=$wr_id;?>&user_code=<?=$user_code;?>"><button class="btn btn-lg btn-white remove-padding active" type="button">여자복식</button></a>
	        </div>
	        <div class="btn-group">
	            <a href="champ_info_endmatch_mix.php?wr_id=<?=$wr_id;?>&user_code=<?=$user_code;?>"><button class="btn btn-lg btn-white remove-padding" type="button">혼합복식</button></a>
	        </div>
	        <!--
	        <div class="btn-group">
	            <a href="champ_info_endmatch_point.php?wr_id=<?=$wr_id;?>"><button class="btn btn-lg btn-white remove-padding" type="button">단체전</button></a>
	        </div>
	        -->
		</div>
	    <!-- end 카테고리 -->
	    <!-- 접수현황 -->
	    <div class="row push-20">
	        <div class="col-lg-12">
	            <!-- 경기 결과 우승자 출력 -->
	            <div class="block">
	                <div class="block-content remove-padding">
	                    <table class="table  champ_result table-mobile">
	                        <thead>
	                        	<th class="text-center" width="20%">급수</th>
	                        	<th class="text-center">1위</th>
	                        	<th class="text-center">2위</th>
	                        	<th class="text-center">3위</th>
	                        </thead>
	                        <tbody class="text-center">
		                            <?php
			                		$sql = "select * from series_data where match_code = '$code' and division = '여복' order by wr_id";
									
									$result = sql_query($sql);
									while($r = sql_fetch_array($result)){
										$group_sql = "select count(wr_id) as cnt from group_data where match_code = '$code' and division = '여복' and series = '$r[series]' and series_sub = '$r[series_sub]'";
										$group_result = sql_query($group_sql);
										$group = sql_fetch_array($group_result);
										
										if($group['cnt'] != 1){

											$game_sql = "select * from game_score_data where match_code = '$code' and division = '여복' and series = '$r[series]' and series_sub = '$r[series_sub]' and tournament = 'c'";
											$game_result = sql_query($game_sql);
											$game = sql_fetch_array($game_result);
											
											$sql_team1 = "select * from team_data where team_code = '$game[team_1_code]'";
											$team1_result = sql_query($sql_team1);
											
											$sql_team2 = "select * from team_data where team_code = '$game[team_2_code]'";
											$team2_result = sql_query($sql_team2);
											
											if($game['team_1_score'] > $game['team_2_score']){
												$champion = sql_fetch_array($team1_result);
												$champion_sub = sql_fetch_array($team2_result);
											}else{
												$champion = sql_fetch_array($team2_result);
												$champion_sub = sql_fetch_array($team1_result);
											}
											$champion_code = $champion['team_code'];
											$game_sql = "select * from game_score_data where match_code = '$code' and division = '여복' and series = '$r[series]' and series_sub = '$r[series_sub]' and (team_1_code = '$champion_code' or team_2_code = '$champion_code') and tournament = 'T' and tournament_count = '2'";
											$game_result = sql_query($game_sql);
											$third_array = array();
											$games = sql_fetch_array($game_result);
											if($games['team_1_score'] > $games['team_2_score'] ){
												$sql_team2 = "select * from team_data where team_code = '$games[team_2_code]'";
												$team2_result = sql_query($sql_team2);												
												$third_champion_sub = sql_fetch_array($team2_result);
											}else{
												$sql_team2 = "select * from team_data where team_code = '$games[team_1_code]'";
												$team2_result = sql_query($sql_team2);
												$third_champion_sub = sql_fetch_array($team2_result);
											}
											
										?>
										 <tr>
			                            	<td><?=$r["series"]?> <?=$r["series_sub"]?></td>
			                            	<?php
			                            		if($game['team_1_score'] != "0"  && $game['team_2_score'] != "0" )
												{
			                            	?>
			                            	<td><?=$champion['club'];?><br/>
			                            		<?=$champion['team_1_name']."/".$champion['team_2_name'];?>
			                            	</td>
			                            	<td><?=$champion_sub['club'];?><br/>
			                            		<?=$champion_sub['team_1_name']."/".$champion_sub['team_2_name'];?>
			                            	</td>
			                            	<td>
			                            	<?php 
			                            		echo $third_champion_sub['club']."<br>";
												echo $third_champion_sub['team_1_name']."/".$third_champion_sub['team_2_name'];
												echo "<br>";
			                            	?>
			                            	</td>
			                            </tr>
										<?php
												}else{
													echo "<td></td><td></td><td></td>";
												}
	
											}else if($group['cnt'] == 1){
												if($code == "CPTB6WY01GA" && $r[series] == '40' && $r[series_sub] == 'B' ){
													echo "<td>40 B</td><td>DMZ클럽<br>남선희/이정미 </td><td>용하<br>우효림/최미숙</td><td></td>";
												}
												else{
													
												
												$game_sql = "select * from team_data where match_code = '$code' and division = '여복' and series = '$r[series]' and series_sub = '$r[series_sub]' order by group_rank Limit 3";
												
												$game_result = sql_query($game_sql);
												$champion = array();
												while($game = sql_fetch_array($game_result)){
													$champion[] = $game;
												}
												$game_count_sql = "select count(wr_id) as cnt from game_score_data where team_1_score = '0' and team_2_score = '0' and match_code = '$code' and division = '여복' and series = '$r[series]' and series_sub = '$r[series_sub]' and tournament = 'L'";
												$game_count_result = sql_query($game_count_sql);
												$game_count = sql_fetch_array($game_count_result);
											?>
											<tr>
			                            	<td><?=$r["series"]?> <?=$r["series_sub"]?></td>
			                            	<?php
			                            		if($game_count['cnt'] == 0)
												{
				                            	?>
				                            	<td><?=$champion[0]['club'];?><br/>
				                            		<?=$champion[0]['team_1_name']."/".$champion[0]['team_2_name'];?>
				                            	</td>
				                            	<td><?=$champion[1]['club'];?><br/>
				                            		<?=$champion[1]['team_1_name']."/".$champion[1]['team_2_name'];?>
				                            	</td>
				                            	<td>
				                            	<?php 
				                            		echo $champion[2]['club']."<br>";
													echo $champion[2]['team_1_name']."/".$champion[2]['team_2_name'];
													echo "<br>";
				                            	?>

				                            	</td>
				                            	</tr>
												<?php	
												}else{
													echo "<td></td><td></td><td></td>";
												}
												}
											}
											
										}
									?>
		                        </tbody>
	                    </table>
	                </div>
	            </div>
	            <!-- end 경기 결과 우승자 출력 -->
	        </div>
	        
	    </div>
	</div>
<!-- end Contents Area -->


<?php require 'inc/views/base_footer.php'; ?>
<?php require 'inc/views/template_footer_start.php'; ?>







<?php require 'inc/views/template_footer_end.php'; ?>

