<?php
include_once('../common.php');
if (!defined('_GNUBOARD_')) exit; // 개별 페이지 접근 불가
include_once(G5_AOS_PATH.'/head.php');
?>

<!-- sub nav -->
<?php include_once('./app_sub_nav.php'); ?>
<!-- end sub nav -->


<!-- Contents Area -->
<div class="pop_container">
	
	<section>
		<div class="tab_group1 mb-0">
			<a class="" href="">진행현황</a>
			<a class="active" href="">경기관리</a>
		</div>
		<div class="content">
			<div class="tbl_style02 tbl_striped">
			<div class="con_tit_area clear">
				<div class="tit">리그전 현황</div>
			</div>
			<table>
				<thead>
					<tr>
						<th>경기</th>
						<th>클럽</th>
						<th>이름</th>
						<th>점수</th>
						<th>이름</th>
						<th>클럽</th>
					</tr>
				</thead>
				<tbody class="text-center">
					<tr>
						<td>1경기</td>
						<td>
							레이즈업<br>
							레이즈업
						</td>
						<td>
							이우선<br>
							이상훈
						</td>
						<td class="match_point"><span>6</span>&nbsp;:&nbsp;<span>0</span></td>
						<td>
							이우선<br>
							이상훈
						</td>
						<td>
							레이즈업<br>
							레이즈업
						</td>
					</tr>
					<tr>
						<td>2경기</td>
						<td>
							레이즈업<br>
							레이즈업
						</td>
						<td>
							이우선<br>
							이상훈
						</td>
						<td class="match_point">
							<div class="champ_sts_change">
								<!-- 점수입력관리자 기능 (경기시작 > 점수입력 >점수입력 선택 수정팝업 ) -->
	                    		<a href="" class="btn">경기시작</a><!-- 담당자 일 경우 준비중 대체-->
	                    		<a href="" class="btn bkg6">점수입력</a><!-- 담당자 일 경우 진행중 대체-->
	                    		
	                    		<div>
	                    			<div><span></span>&nbsp;-&nbsp;<span></span></div><!-- 준비중-->
	                    			<div class="playing ani01">진행중</div><!-- 진행중 -->
	                    			<div><a href=""><span class="win">6</span>&nbsp;:&nbsp;<span>0</span></a></div><!-- 종료 -->
	                    		</div>
	                    	</div>
							<style>
							.match_point .champ_sts_change .win{color:#ff0000;}
							.match_point .btn{margin:auto;display:block;padding:9px 0;font-size: 16px;text-align:center;width:80px;line-height:1em;border:2px solid #14264a;border-radius:3px}
							</style>
						</td>
						<td>
							이우선<br>
							이상훈
						</td>
						<td>
							레이즈업<br>
							레이즈업
						</td>
					</tr>
				</tbody>
			</table>
		</div>
		</div>
	</section>
</div>
<!-- end Contents Area -->


<?php
include_once(G5_AOS_PATH.'/tail.php');
?>
