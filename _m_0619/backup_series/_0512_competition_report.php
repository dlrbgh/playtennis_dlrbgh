<?php
include_once('../common.php');
if (!defined('_GNUBOARD_')) exit; // 개별 페이지 접근 불가
include_once(G5_AOS_PATH.'/head.php');
$menu_cate2 ='competition_view';
$menu_cate3 ='7';
?>

<!-- sub nav -->
<?php include_once('./app_sub_nav.php'); ?>
<!-- end sub nav -->
<?php
	$defaultQstr = "competition_time_tbl.php?c={$c}&d={$d}&s={$s}&ss={$ss}&gi={$gi}&t={$t}";
	$gi = isset($gi) && $gi!='' ? $gi : 0;
	$t = isset($t) && $t!='' ? $t : "L";
?>


<!-- Contents Area -->
<div class="pop_container">

	<!-- 탭-코트-->
	<section class="section2">
		<div class="pop_hd">
			<div class="tit">경기 선택</div>
		</div>
		<div class="content ">
			<div class="btn-group" role="group">

				<ul class="btn-list list3">
					<?php
						$group_sql = "select division, series, series_sub from group_data where match_code = '{$c}'
						group by division, series, series_sub
						order by division, series, series_sub
						";
						$groups = sql_query($group_sql);
						$is_first = $d!='';
						$active_class = 'class="active"';

						$index = 0;
						while($row = sql_fetch_array($groups)){
							$groupqstr = "?c={$c}&d={$row['division']}&s={$row['series']}&ss={$row['series_sub']}&gi={$index}&t=L";
							?>
							<li><a <?=($index."" == $gi? $active_class : "")?> href="<?=$groupqstr?>">
								<?=$row['division']?> <?=$row['series']?> <?=$row['series_sub']?></a></li>

						<?php $index++;
					}?>
				</ul>
	        </div>
	    </div>
	</section>
	<!-- //탭-코트-->
	<section>
		<div class="content">
			<div class="con_tit_area clear">
				<div class="tit mb-5 fw-700">
					플레이테니스배 강원도 춘천시 테니스 대회
				</div>
				<div class="tit">
					2017.04.18 ~ 20 춘천 송암 체육관
				</div>
				<div class="r-area">
					<ul>
						<li class="color5 fw-700"></li>
					</ul>
				</div>
			</div>

			<div class="tbl_style01 tbl_striped mb-20">
				<table>
					<thead>
						<tr>
							<th>순위</th>
							<th>클럽</th>
							<th>선수</th>
						</tr>
					</thead>
					<tbody class="text-center">
						<tr>
							<td>우승</td>
							<td>
								레이즈업<br>
								레이즈업
							</td>
							<td>
								이우선<br>
								이상훈
							</td>
						</tr>
						<tr>
							<td>준우승</td>
							<td>
								레이즈업<br>
								레이즈업
							</td>
							<td>
								이우선<br>
								이상훈
							</td>
						</tr>

					</tbody>
				</table>
			</div>


		</div>
	</section>

	<!-- 본선 토너먼트 ㅇ그룹 경기 결과-->
	<section>
		<div class="content">
			<div class="con_tit_area clear">
				<div class="tit">본선경기 결과</div>
				<div class="r-area">
					<ul>
						<li class="color5 fw-700"></li>
					</ul>
				</div>
			</div>

			<div class="tab_group1 mb-0">
    			<a class="active" href="">128강</a>
    			<a class="" href="">64강</a>
    			<a href="">32강</a>
    			<a href="">16강</a>
    			<a href="">8강</a>
    			<a href="">준결승</a>
    			<a href="">결승</a>
        	</div>

			<div class="mb-20">
				<ul class="tournament_list">
					 <div class="status_banner">
					 	3그룹 단식 토너먼트 64강 종료
					 </div>

					<li>
						<div class="tournament_match">
				            <div class="tournament_hd">
			                    <span>64강 4코트 5경기</span>
			                    <span class="r-side-area"></span>
			                </div>

			                <div class="tournament_content">
			                	<div class="l-area">
			                        <div class="teamA">자연별곡 - 이우선가</div>
			                        <div class="teamA">자연자연 - 홍길동</div>
			                    </div>
			                    <div class="tournament_point">
			                    	<!--
			                    	<a href="">점수입력</a>
			                    	-->
			                    	<div class="result_point"><span>6</span>&nbsp;:&nbsp;<span>0</span></div>

			                    </div>
			                    <div class="r-area">
			                        <div class="teamB">정겨운 - 정겨정겨</div>
			                        <div class="teamB">어경민 - 잠자는바나나</div>
			                    </div>
			                </div>

			                <div class="tournament_ft">
			                	<span>송암체육관</span>
			                	<span class="r-side-area">이히2017-10-17</span>
			                </div>
				        </div>
					</li>
				</ul>

			</div>
		</div>
	</section>
	<!-- //본선 토너먼트 ㅇ그룹 경기 결과-->

</div>
<!-- end Contents Area -->


<?php
include_once(G5_AOS_PATH.'/tail.php');
?>
