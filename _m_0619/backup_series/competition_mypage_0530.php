<?php
include_once('../common.php');
if (!defined('_GNUBOARD_')) exit; // 개별 페이지 접근 불가
include_once(G5_AOS_PATH.'/head.php');
$menu_cate2 ='competition_view';
$menu_cate3 ='6';

$mb = get_member($member['mb_id']);

$match_data = sql_fetch("select * from match_data where code = '$c'");

$area_1s =sql_fetch("select area1 from area1 where id = '$mb[mb_1]'");
$area_1 = trim($area_1s['area1']);

$area_2s =sql_fetch("select area2 from area2 where id = '$mb[mb_2]'");
$area_2 = trim($area_2s['area2']);

$club_lists =sql_fetch("select club from club_list where id = '$mb[mb_3]'");
$club = trim($club_lists['club']);

?>

<!-- sub nav -->
<?php include_once('./app_sub_nav.php'); ?>
<!-- end sub nav -->


<!-- Contents Area -->
<div class="pop_container">

	<section>
		<div class="pop_hd">
			<div class="tit"><?=$mb['mb_name']?>님 경기 정보</div>
		</div>
		<div class="content">
			<?php
				$mystatusSql = "
				select division, series, series_sub, club
				,case when team_1_name = '{$mb['mb_name']}' then team_2_name
				else team_1_name end as coPlayer
				from team_data
				where
				match_code = '{$c}'
				and ( team_1_name = '{$mb['mb_name']}'
				or team_2_name = '{$mb['mb_name']}')
				and area_1 like '%{$area_1}%'
				and area_2 like '%{$area_2}%'
				and club like '%{$club}%'";
				// print $mystatusSql;
				$status_result = sql_query($mystatusSql);
				$row = sql_fetch_array($status_result);
			 ?>

			<div class="con_tit_area clear">
				<div class="tit mb-15 fw-700">
					<!-- 복식 3그룹 1조 예선 -->
					<?php echo $row['division']?> <?php echo $row['series']?> <?php echo $row['series_sub']?>
				</div>
				<!-- <div class="tit color5">
					2017.04.18 ~ 20 화교일 춘천 송암 체육관
					<br/>
					1코트 1번째경기 예정
				</div> -->
			</div>

			<!-- 단체전 선수배정-->
			<!-- <div class="group_match_area">
				<ul>
					<li>단체전1경기 <span class="color5">선수를 배정하세요</span> <a href="popup_team_event_set.php" target="_blank">선수배정</a></li>
				</ul>
			</div> -->
			<!-- //단체전 선수배정-->
		</div>


	</section>


<style>
.group_match_area ul li{position:relative;display:block;line-height:20px}
.group_match_area ul li a{position:absolute;right:0;padding:5px 15px;border:1px solid #ccc;bottom:-3px;width:100px;text-align:center}
.group_match_area ul li a:active{background-color:#14264a;color:#fff}
.group_match_area ul li span{font-size:13px}
</style>


<?php
	$team_code = sql_fetch("SELECT team_code FROM team_data	where  match_code = '{$c}' and area_1 LIKE '{$area_1}' and area_2 LIKE '$area_2' and club LIKE '{$club}' and team_1_name LIKE '{$mb['mb_name']}'");

	$tornament_sql = "select * from game_score_data
	where match_code = '$c' and tournament <> 'L'
	and (team_1_code = '$team_code[team_code]' or team_2_code  = '$team_code[team_code]' )";

	$tournament_result = sql_query($tornament_sql);

	if($tournament_result->num_rows>1){
 ?>
	<section>
		<div class="content">

			<div class="tbl_style02 tbl_striped">
				<div class="con_tit_area clear">
					<div class="tit">토너먼트</div>
					<div class="r-area">
						<ul>
							<li class="color5 fw-700">진행중</li>
						</ul>
					</div>
				</div>
				<table>
					<thead>
						<tr>
							<th>경기</th>
							<th>클럽</th>
							<th>이름</th>
							<th>점수</th>
							<th>이름</th>
							<th>클럽</th>
						</tr>
					</thead>
					<tbody class="text-center">
						<?php
							while($row = sql_fetch_array($tournament_result)){
								// print_r($row);
						?>

							<?php }
						 ?>
						<!-- <tr>
							<td>16강</td>
							<td>
								레이즈업<br>
								레이즈업
							</td>
							<td>
								이우선<br>
								이상훈
							</td>
							<td class="match_point"><span>6</span>&nbsp;:&nbsp;<span>0</span></td>
							<td>
								이우선<br>
								이상훈
							</td>
							<td>
								레이즈업<br>
								레이즈업
							</td>
						</tr>
						<tr>
							<td>32강</td>
							<td>
								레이즈업<br>
								레이즈업
							</td>
							<td>
								이우선<br>
								이상훈
							</td>
							<td class="match_point"><span>6</span>&nbsp;:&nbsp;<span>0</span></td>
							<td>
								이우선<br>
								이상훈
							</td>
							<td>
								레이즈업<br>
								레이즈업
							</td>
						</tr> -->

					</tbody>
				</table>
			</div>
		</div>
	</section>

	<?php }?>
	<section>
		<div class="content">
			<!-- 리그전 결과-->
			<div class="tbl_style01 tbl_striped">
				<div class="con_tit_area clear">
					<div class="tit">리그전</div>
					<div class="r-area">
						<ul>
							<li class="color5 fw-700">진행중 or 토너먼트 진출 or 종료</li>
						</ul>
					</div>
				</div>
				<table>
					<thead>
						<tr>
							<th>순위</th>
							<th>클럽</th>
							<th>선수</th>
							<th>승</th>
							<th>패</th>
						</tr>
					</thead>
					<tbody class="text-center">
						<?php $my_league_status = "select
						  (select concat(club,' ', team_1_name, ' ',team_2_name,' ', team_league_point,' ',team_league_lose_count) from team_data where team_1 = team_data.team_code) as team_1
						  ,(select concat(club,' ', team_1_name, ' ',team_2_name,' ', team_league_point,' ',team_league_lose_count) from team_data where team_2 = team_data.team_code) as team_2
						  ,(select concat(club,' ', team_1_name, ' ',team_2_name,' ', team_league_point,' ',team_league_lose_count) from team_data where team_3 = team_data.team_code) as team_3
						  ,game_court

						  from group_data where code = (

						  SELECT code FROM tennis2.group_data

						  where team_1 =

						  (SELECT team_code FROM tennis2.team_data
						  where match_code = '{$c}'
						  and area_1 = '{$area_1}'
						  and area_2 = '{$area_2}'
						  and club = '{$club}'
						  and team_1_name = '{$mb['mb_name']}')
						  or team_2  =
						  (SELECT team_code FROM tennis2.team_data
						  where match_code = '{$c}'
						  and area_1 = '{$area_1}'
						  and area_2 = '{$area_2}'
						  and club = '{$club}'
						  and team_1_name = '{$mb['mb_name']}')
						  )";
						  $my_league_status_result = sql_query($my_league_status);
							while($row = sql_fetch_array($my_league_status_result)){
							$sorted = array(explode(' ',$row['team_1']),explode(' ',$row['team_2']),explode(' ',$row['team_3']));

							usort($sorted, "cmp"); ?>
							<?php

							for($index = 0; $index < count($sorted); $index++){?>
								<tr>
									<td class="text-center"><?=$index+1?>위</td>
									<td class="text-center">
										<?=$sorted[$index][0]?><br/>
										<?=$sorted[$index][0]?>
									</td>
									<td class="my_team_sts">
										<?=$sorted[$index][1]?><br/>
										<?=$sorted[$index][2]?>
									</td>
									<td class="text-center">
										<?=$sorted[$index][3]?>승
									</td>
									<td class="text-center">
										<?=$sorted[$index][4]?>패
									</td>
								</tr>
							<?php	}?>

						<?php }?>
					</tbody>
				</table>
			</div>
			<!-- //리그전 결과-->

			<!-- 리그전 상세 -->
			<div class="tbl_style02 tbl_striped">
				<div class="con_tit_area clear">
					<div class="tit">리그전 현황</div>
				</div>
				<table>
					<thead>
						<tr>
							<th>경기</th>
							<th>클럽</th>
							<th>이름</th>
							<th>점수</th>
							<th>이름</th>
							<th>클럽</th>
						</tr>
					</thead>
					<tbody class="text-center">
						<?php
							$league_status_sql = "

						select
           game_score_data.division,
					    game_score_data.series,
					    game_score_data.series_sub,
					    group_data.code as group_code,
					    group_data.num, team_1_code,
					    (select concat(club,'-', team_1_name,'-',team_2_name) from team_data where team_code = team_1_code) as team_1,
					    (select concat(club,'-', team_1_name,'-',team_2_name) from team_data where team_code = team_2_code) as team_2,
					    team_1_score,
					    team_2_score,
					    end_game

							from
							(select * from game_score_data where tournament = 'L') game_score_data
							join
          group_data
          on game_score_data.group_code = group_data.code

          where group_data.code = (

							SELECT code FROM tennis2.group_data

							where
							 (team_1 =

							(SELECT team_code FROM tennis2.team_data
							where match_code = '{$c}'
						  and area_1 = '{$area_1}'
						  and area_2 = '{$area_2}'
						  and club = '{$club}'
						  and team_1_name = '{$mb['mb_name']}')
							or team_2  =
							(SELECT team_code FROM tennis2.team_data
							where match_code = '{$c}'
						  and area_1 = '{$area_1}'
						  and area_2 = '{$area_2}'
						  and club = '{$club}'
						  and team_1_name = '{$mb['mb_name']}')
							)						)
                            ";


						 ?>
						 <?php
						 $matchProcess = sql_query($league_status_sql);
					while($match_row = sql_fetch_array($matchProcess)){
 						// 	$match_row = sql_fetch_array($matchProcess);
 							$team1 = explode('-',$match_row['team_1']);
 							$team2 = explode('-',$match_row['team_2']);
 							?>
 							<tr>
 								<td class="text-center"><?=$j+1?><br/>경기</td>
 								<td class="text-center"><?=$team1['0']?><br/><?=$team1['0']?></td>
 								<td class="text-center"><?=$team1['1']?><br/><?=$team1['2']?></td>
 								<td class="match_point text-center"><span><?=$match_row['team_1_score']?></span>&nbsp;:&nbsp;<span><?=$match_row['team_2_score']?></span></td>
 								<td class="text-center"><?=$team2['1']?><br/><?=$team2['2']?></td>
 								<td class="text-center"><?=$team2['0']?><br/><?=$team2['0']?></td>
 							</tr>
 							<?php }?>
					</tbody>
				</table>
			</div>
			<!-- //리그전 상세 -->

		</div>
	</section>



</div>
<!-- end Contents Area -->


<?php
function cmp($a, $b){
	return ($b[3]-0) -($a[3]-0);
}
include_once(G5_AOS_PATH.'/tail.php');
?>
