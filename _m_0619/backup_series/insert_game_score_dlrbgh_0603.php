<?php
include_once('../common.php');
include_once('./lib/common.lib.php');

$rows = array();

$wr_id 				= $_REQUEST['wr_id'];
$division			= $_REQUEST['division'];
$series 			= $_REQUEST['series'];
$series_sub			= $_REQUEST['series_sub'];
$match_code			= $_REQUEST['match_code'];

$team_1_dis			= $_REQUEST['team_1_dis'];
$team_2_dis			= $_REQUEST['team_2_dis'];


//수정적용임
//이전소스에서는 이게 1이면 전부다 뒤집었음
$modify				= $_REQUEST['modify'];

$tournament			= $_REQUEST['tournament'];
$tournament_count	= $_REQUEST['tournament_count'];

$team_1_score		= (int)$_REQUEST['team_1_score'];
$team_2_score		= (int)$_REQUEST['team_2_score'];

$team_1_score_before	= $_REQUEST['team_1_score_before'];
$team_2_score_before	= $_REQUEST['team_2_score_before'];
$require_url	= $_REQUEST['rd'];


$is_league = ($tournament == 'L' ? true : false);


$target_table = "team_data";

if($division == "단체전"){
	$target_table = "team_event_data";
}


print_r($_POST);

if($wr_id == ""){
	$rows = array();
	$rows[]['result'] = "false";
	echo json_encode($rows);
	return;
}

if($division == ""){
	$rows = array();
	$rows[]['result'] = "false";
	echo json_encode($rows);
	return;
}
if($series == ""){
	$rows = array();
	$rows[]['result'] = "false";
	echo json_encode($rows);
	return;
}
if($match_code == ""){
	$rows = array();
	$rows[]['result'] = "false";
	echo json_encode($rows);
	return;
}
if($modify == ""){
	$rows = array();
	$rows[]['result'] = "false";
	echo json_encode($rows);
	return;
}

if($tournament == ""){
	$rows = array();
	$rows[]['result'] = "false";
	echo json_encode($rows);
	return;
}

if($tournament_count == ""){
	$rows = array();
	$rows[]['result'] = "false";
	echo json_encode($rows);
	return;
}

//점수 삽입
$target_table = "team_data";
if($division == "단체전"){
	$target_table = "team_event_data";
}

if($division == '단체전'){
	$event = '_event';
}


//실력/기권 입력
if($team_1_dis > 0 || $team_2_dis > 0){
	print ">>>";
  make_dis($team_1_dis, $team_2_dis, $wr_id, $match_code);}

//점수입력
score_update($team_1_score, $team_2_score, $team_1_dis, $team_2_dis, $wr_id, $match_code);
if($is_league){
  //토너먼트는 갱신없음
  update_league_point($team_1_score, $team_2_score, $wr_id,$target_table, $d);
}

//재수정시 수정사유입력
if($wr_text != ""){
  $group_code_sql  = "select * from game_score_data where wr_id = '$wr_id' and match_code = '$match_code'";
  $group_code_result = sql_query($group_code_sql);
  $group_code = sql_fetch_array($group_code_result);
	regist_change_score_history($team_1_score_before, $team_2_score_before
	, $team_1_dis_before, $team_2_dis_before, $wr_text
	, $match_code, $group_code['code'], $member['mb_id'], $_SERVER['REMOTE_ADDR']);
  // regist_change_score_history($wr_text, $group_code['code'], $match_code, $member['mb_id'], $_SERVER['REMOTE_ADDR']);
}


//수정되면 대진표 새로짜기기능있었음 ->지금필요없지

//그다음은 득실차에의해 1,2위 설정하는게있었음 ->흠.. 이건 있다가.. ㅠㅠㅠ

//종료확인코드
//각 리그/토너먼트 라운드별로 종료되면 그 다음 토너먼트/상위토너먼트에 팀과 조를 기록함
// $is_league = true;

if($is_league){
			//리그끝났나
    if(is_this_league_and_group_finish($wr_id, $match_code, $group_code)){
      //1.그럼 랭킹을 정해주고
      $first_second = write_this_group_ranking($match_code, $group_code, $target_table);
				$group_number = sql_fetch("select num+1 as group_number from
				group_data where code = '{$group_code}'")['group_number'];

				foreach($first_second as $ranking => $team){
					// 그룹코드로 찾지않는다

					$assigned_group_name = "{$group_number}조 {$ranking}위";
					$sql = "select * from game_score_data
					where division = '{$division}' and series = '{$series}' and series_sub = '{$series_sub}'
					and tournament <> 'L' and match_code = '{$match_code}'
					and (assigned_group_name1 = '{$assigned_group_name}' or assigned_group_name2 = '{$assigned_group_name}')";


					$result = sql_fetch($sql);
					if($result['assigned_group_name1'] == $assigned_group_name){
						$team_code_assign_clause = " team_1{$event}_code = '{$team['team_code']}'";
					}else if($result['assigned_group_name2'] == $assigned_group_name){
						$team_code_assign_clause = " team_2{$event}_code = '{$team['team_code']}'";
					}
	      //2. 토너먼트에 집어넣어야지
					$team_code_assign_league_from_tournament_sql =
					"update game_score_data set $team_code_assign_clause where ";
					if($division =='단체전'){
						//여기 그룹코드는 이전 그룹이랑 다릉코드이므로
						//그룹코드가아니라 division, series, series_sub, tournament 로 찾아서 넣어야됨
						$team_code_assign_league_from_tournament_sql .=
						"match_code = '{$match_code}'
						and division = '{$division}' and series = '{$series}' and series_sub = '{$series_sub}'
						and tournament <> 'L'
						and (assigned_group_name1 = '{$assigned_group_name}' or assigned_group_name2 = '{$assigned_group_name}')";
					}else{
						//하나만 넣으면되니까
					 $team_code_assign_league_from_tournament_sql .= " wr_id = {$result['wr_id']}";
				 }
					// print "<br />".$team_code_assign_league_from_tournament_sql."<br />";
					sql_query($team_code_assign_league_from_tournament_sql, true);
				}
    }
}else{
  //토너먼트경기일때
	//1. 개인전 -> 바로 갖고와서 빈칸에 넣는다
	//2. 단체전 -> 전부다 끝났을때 비교해서 그위에 집어넣는다

	if($division == '단체전'){

		// if(is_tournament_finish($)){
		//
		// }
	}


}


//리그가 다 끝났는지 확인하거나 "그룹이 다 끝났는지 확인" 하면 바로바로 넣을수있다

//토너먼트도 스코어입력이되면 바로 올리자 점수입력은 위에서하니까


// goto_url($rd);








// echo "1111111111111<br /><br /><br /><br />aaa";
// echo "<br>team1 : ";
// echo $team_1_dis;
// echo "<br>team2 : ";
// echo $team_2_dis;

function make_dis($team_1_dis, $team_2_dis, $wr_id, $match_code){
  if($team_1_dis == "3"){
    $score_cluase = " team_1_score 	= '0',team_2_score 	= '0' ";
    $dis_clause = " team_1_dis 	= '$team_1_dis' ";
	// echo $score_cluase;
  }else if($team_1_dis == "1"){
    $score_cluase = " team_1_score 	= '0',team_2_score 	= '6' ";
    $dis_clause = " team_1_dis 	= '$team_1_dis' ";
	// echo "1팀 기권"	;
  }else if($team_1_dis == "2"){
    $score_cluase = " team_1_score 	= '0',team_2_score 	= '6' ";
    $dis_clause = " team_2_dis 	= '$team_2_dis' ";
// echo "1팀 실격"	;
  }else if($team_2_dis == "1"){
    $score_cluase = " team_1_score 	= '6',team_2_score 	= '0' ";
    $dis_clause = " team_2_dis 	= '$team_2_dis' ";
// echo "2팀 기권"	;
  }else if($team_2_dis == "2"){
    $score_cluase = " team_1_score 	= '6',team_2_score 	= '0' ";
    $dis_clause = " team_2_dis 	= '$team_2_dis' ";
// echo "2팀 실격"	;
  }

  $sql = " update game_score_data set {$score_cluase}, end_game = 'Y',{$dis_clause}
      where wr_id = '$wr_id' and match_code = '$match_code'";
  sql_query($sql);
}
function score_update($team_1_score, $team_2_score, $team_1_dis, $team_2_dis, $wr_id, $match_code){
  $score_update_sql = " update game_score_data set
      team_1_score 	= '$team_1_score',
      team_2_score 	= '$team_2_score',
      end_game 		= 'Y',
				is_on = 'N',
				game_end_time = now(),
      team_1_dis 	= '$team_1_dis',
      team_2_dis 	= '$team_2_dis'
      where wr_id = '$wr_id' and match_code = '$match_code'";
  sql_query($score_update_sql);

}
function update_league_point($team_1_score, $team_2_score, $wr_id, $target_table, $d){
    $league_point_cluase = "";
    $win_team_code; $lose_team_code;
			if($d == '단체전'){
				$event = '_event';
			}
    if($team_1_score > $team_2_score){

        $win_team_code = " ( select team_1{$event}_code from game_score_data where wr_id = {$wr_id})";
        $lose_team_code = " ( select team_2{$event}_code from game_score_data where wr_id = {$wr_id})";
    }else{
      $win_team_code = " ( select team_2{$event}_code from game_score_data where wr_id = {$wr_id})";
      $lose_team_code = " ( select team_1{$event}_code from game_score_data where wr_id = {$wr_id})";
    }

    $winner_sql = "update $target_table set team_league_point = team_league_point + 1  where team_code = {$win_team_code}";
    $loser_sql = "update $target_table set team_league_lose_count = team_league_lose_count + 1 where team_code = {$lose_team_code}";

    sql_query($winner_sql);
    sql_query($loser_sql);
}
function regist_change_score_history($team_1_score, $team_2_score
												, $team_1_dis, $team_2_dis
												, $wr_text, $group_code, $match_code, $mb_id, $ip){
  	$sql = " insert into game_score_data_history
        set
        wr_text 	= '{$wr_text}',
        game_code 	= '{$group_code}',
        match_code = '{$match_code}',
        wr_datetime = '".G5_TIME_YMDHIS."',
        mb_id = '{$mb_id}',
        wr_ip = '{$ip}',
					past_team_1_score = '{$team_1_score_before}',
					past_team_2_score = '{$team_2_score_before}',
					past_team_1_dis = '{$team_1_dis}',
					past_team_2_dis = '{$team_2_dis}'";
					print "<br />".$sql;
  	sql_query($sql);
}
function is_this_league_and_group_finish($wr_id, $match_code, $group_code){
  $sql =" select count(*) as count from game_score_data where
  match_code = '{$match_code}'
  and group_code = '{$group_code}'
  and end_game = 'N'
  and  game_code = (select game_code from game_score_data where wr_id = {$wr_id})";
  // print $sql;
  $count_not_finished = sql_fetch($sql)['count'];
  return $count_not_finished == 0;
}
function write_this_group_ranking($match_code, $group_code, $target_table){

  $first_and_second;

  $group_data = sql_fetch("select team_1, team_2, team_3, team_4, team_5
					from group_data where match_code = '{$match_code}' and code = '{$group_code}'", true);

	// print_r($group_data);
  $sql =" select * from $target_table  where
  (team_code = '{$group_data['team_1']}' or team_code ='{$group_data['team_2']}'
  or team_code ='{$group_data['team_3']}' or team_code = '{$group_data['team_4']}'
  or team_code = '{$group_data['team_5']}')
  order by team_league_point, team_league_lose_count desc limit 0, 2";

	// print $sql;
  $group_list = sql_query($sql);
  $ranking = count($group_list->num_rows)+1;
  while($row = sql_fetch_array($group_list)){
    $set_ranking_sql = "update $target_table set group_rank = {$ranking} where wr_id = {$row['wr_id']}";
    sql_query($set_ranking_sql);
    if($ranking < 3){
      $first_and_second[$ranking] = $row;
    }
    $ranking --;
  }
  return $first_and_second;
}
?>
