<?php
include_once('../common.php');
if (!defined('_GNUBOARD_')) exit; // 개별 페이지 접근 불가
include_once(G5_AOS_PATH.'/head.php');
$menu_cate2 ='management';
$menu_cate3 ='2';
?>

<!-- sub nav -->
<?php include_once('./app_sub_nav.php'); ?>
<!-- end sub nav -->




<!-- Contents Area -->
<div class="pop_container">
	오늘경기있는 체육관 리스트
	<section>
		<ul>
			<li></li>
		</ul>
	</section>

	<section>
		<!--
		<div class="tab_group1 mb-0">
			<a class="active" href="competion_mnt_status.php">진행현황</a>
			<a class="" href="competion_mnt_list.php">경기관리</a>
		</div>
		-->

		<div class="match_manager_area">
			<ul>
				<li>
					<a href="competion_mnt_list.php">
					<div class="ground_area">10월01일 ㅇㅇ 체육관 </div>
					<div class="m_name">홍길동, 매니저 접속권한별 출력내용 다름(본인/참여자 모두)</div>
					</a>
				</li>
				<li class="active">
					<a href="competion_mnt_status.php">
					<div class="ground_area">날짜 - 체육관이름</div>
					<div class="m_name">홍길동, 홍명동, 홍가동, 홍나동</div>
					</a>
				</li>

			</ul>
		</div>
	</section>
	<!-- 본선 토너먼트 ㅇ그룹 경기 -->
	<section>
		<div class="content">
			<div class="con_tit_area clear">
				<div class="tit">본선진행 점수</div>
				<div class="r-area">
					<ul>
						<li class="color5 fw-700">현재시간출력(시:분:초) 8강 진행중</li>
					</ul>
				</div>
			</div>

			<div class="tab_group1 mb-0">
    			<a class="active" href="">128강</a>
    			<a class="" href="">64강</a>
    			<a href="">32강</a>
    			<a href="">16강</a>
    			<a href="">8강</a>
    			<a href="">준결승</a>
    			<a href="">결승</a>
        	</div>

			<div class="mb-20">
				<ul class="tournament_list">
					 <div class="status_banner">
					 	3그룹 단식 토너먼트 64강 종료
					 </div>
					<li>
						<div class="tournament_match">
				            <div class="tournament_hd">
			                    <span>64강 4코트 5경기</span>
			                    <span class="r-side-area"></span>
			                </div>

			                <div class="tournament_content">
			                	<div class="l-area">
			                        <div class="teamA">자연별곡 - 이우선가</div>
			                        <div class="teamA">자연자연 - 홍길동</div>
			                    </div>
			                    <div class="tournament_point">
			                    	<!-- 점수입력관리자 기능 (경기시작 > 점수입력 >점수입력 선택 수정팝업 )
			                    		<a href="" class="btn">경기시작</a>
			                    		<a href="" class="btn bkg6">점수입력</a>
			                    		 -->
									<a href="" class="btn">경기시작</a>
								</div>
			                    <div class="r-area">
			                        <div class="teamB">정겨운 - 정겨정겨</div>
			                        <div class="teamB">어경민 - 잠자는바나나</div>
			                    </div>
			                </div>

			                <div class="tournament_ft">
			                	<span>송암체육관</span>
			                	<span class="r-side-area">이히2017-10-17</span>
			                </div>
				        </div>
					</li>
					<li>
						<div class="tournament_match">
				            <div class="tournament_hd">
			                    <span>64강 4코트 5경기</span>
			                    <span class="r-side-area"></span>
			                </div>

			                <div class="tournament_content">
			                	<div class="l-area">
			                        <div class="teamA">자연별곡 - 이우선가</div>
			                        <div class="teamA">자연자연 - 홍길동</div>
			                    </div>
			                    <div class="tournament_point">
			                    	<a href="popup_insert_score.php" target="_blank" class="btn bkg6">점수입력</a>

			                    </div>
			                    <div class="r-area">
			                        <div class="teamB">정겨운 - 정겨정겨</div>
			                        <div class="teamB">어경민 - 잠자는바나나</div>
			                    </div>
			                </div>

			                <div class="tournament_ft">
			                	<span>송암체육관</span>
			                	<span class="r-side-area">이히2017-10-17</span>
			                </div>
				        </div>
					</li>
					<li>
						<div class="tournament_match">
				            <div class="tournament_hd">
			                    <span>64강 4코트 5경기</span>
			                    <span class="r-side-area"></span>
			                </div>

			                <div class="tournament_content">
			                	<div class="l-area">
			                        <div class="teamA">자연별곡 - 이우선가</div>
			                        <div class="teamA">자연자연 - 홍길동</div>
			                    </div>
			                    <div class="tournament_point">
			                    	<a href="popup_insert_score.php" target="_blank" class="btn bkg6">점수입력</a>
								</div>
			                    <div class="r-area">
			                        <div class="teamB">정겨운 - 정겨정겨</div>
			                        <div class="teamB">어경민 - 잠자는바나나</div>
			                    </div>
			                </div>

			                <div class="tournament_ft">
			                	<span>송암체육관</span>
			                	<span class="r-side-area">이히2017-10-17</span>
			                </div>
				        </div>
					</li>
					<li>
						<div class="tournament_match">
				            <div class="tournament_hd">
			                    <span>64강 4코트 5경기</span>
			                    <span class="r-side-area"></span>
			                </div>

			                <div class="tournament_content">
			                	<div class="l-area">
			                        <div class="teamA">자연별곡 - 이우선가</div>
			                        <div class="teamA">자연자연 - 홍길동</div>
			                    </div>
			                    <div class="tournament_point">
			                    	<!--
			                    	<a href="">점수입력</a>
			                    	-->
			                    	<div class="result_point">
			                    		<div><a href=""><span class="win">6</span>&nbsp;:&nbsp;<span>0</span></a></div><!-- 종료 -->
			                    	</div>

			                    </div>
			                    <div class="r-area">
			                        <div class="teamB">정겨운 - 정겨정겨</div>
			                        <div class="teamB">어경민 - 잠자는바나나</div>
			                    </div>
			                </div>

			                <div class="tournament_ft">
			                	<span>송암체육관</span>
			                	<span class="r-side-area">이히2017-10-17</span>
			                </div>
				        </div>
					</li>
				</ul>

			</div>
		</div>
	</section>
	<!-- //본선 토너먼트 ㅇ그룹 경기 -->

	<section>
		<div id="court_list" class="btn-group" role="group">
				<ul class="btn-list">
					<li><a class="active" href="?c=413780A6K1IUFNDAO&amp;dt=2017.05.12&amp;g=84&amp;ct=1">1코트</a></li>
					<li><a href="?c=413780A6K1IUFNDAO&amp;dt=2017.05.12&amp;g=84&amp;ct=2">2코트</a></li>
					<li><a href="?c=413780A6K1IUFNDAO&amp;dt=2017.05.12&amp;g=84&amp;ct=3">3코트</a></li>
					<li><a href="?c=413780A6K1IUFNDAO&amp;dt=2017.05.12&amp;g=84&amp;ct=4">4코트</a></li>
				</ul>
	        </div>
	</section>

	<!-- 리그전 -->
	<section>

		<div class="content">

			<div class="tbl_style02 tbl_striped">
				<div class="con_tit_area clear">
					<div class="tit">리그전 점수</div>
					<div class="r-area">
						<ul>
							<li class="color5 fw-700">2017-04-30:13:15:20</li>
						</ul>
					</div>
				</div>
				<table>
					<thead>
						<tr>
							<th>경기</th>
							<th>클럽</th>
							<th>이름</th>
							<th>점수</th>
							<th>이름</th>
							<th>클럽</th>
						</tr>
					</thead>
					<tbody class="text-center">
						<tr>
							<td>1경기</td>
							<td>
								레이즈업<br>
								레이즈업
							</td>
							<td>
								이우선<br>
								이상훈
							</td>
							<td class="match_point"><span>6</span>&nbsp;:&nbsp;<span>0</span></td>
							<td>
								이우선<br>
								이상훈
							</td>
							<td>
								레이즈업<br>
								레이즈업
							</td>
						</tr>
						<tr>
							<td>2경기</td>
							<td>
								레이즈업<br>
								레이즈업
							</td>
							<td>
								이우선<br>
								이상훈
							</td>
							<td class="match_point">
								<div class="champ_sts_change">
									<!-- 점수입력관리자 기능 (경기시작 > 점수입력 >점수입력 선택 수정팝업 ) -->
		                    		<a href="" class="btn">경기시작</a><!-- 담당자 일 경우 준비중 대체-->
		                    		<a href="popup_insert_score.php" target="_blank" class="btn bkg6">점수입력</a><!-- 담당자 일 경우 진행중 대체-->
		                    		<div><a href=""><span class="win">6</span>&nbsp;:&nbsp;<span>0</span></a></div><!-- 종료 -->
		                    	</div>
								<style>
								.match_point .champ_sts_change .win,.result_point .win{color:#ff0000;}

								.match_point .btn{margin:auto;display:block;padding:9px 0;font-size: 16px;text-align:center;width:80px;line-height:1em;border:2px solid #14264a;border-radius:3px}
								</style>
							</td>
							<td>
								이우선<br>
								이상훈
							</td>
							<td>
								레이즈업<br>
								레이즈업
							</td>
						</tr>
					</tbody>
				</table>
			</div>
		</div>
	</section>
	<!-- 리그전 -->

</div>
<!-- end Contents Area -->


<?php
include_once(G5_AOS_PATH.'/tail.php');
?>
