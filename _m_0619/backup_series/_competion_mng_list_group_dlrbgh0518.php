<?php
include_once('../common.php');
if (!defined('_GNUBOARD_')) exit; // 개별 페이지 접근 불가
include_once(G5_AOS_PATH.'/head.php');
$menu_cate2 ='management';
$menu_cate3 ='2';
?>

<!-- sub nav -->
<?php include_once('./app_sub_nav.php'); ?>
<!-- end sub nav -->
<?php

	$match_sql = "select * from match_data";
	$match_result = sql_query($match_sql);

 ?>

<?php

	$gym_sql = "select gym_data.*, date_format(gym_data.wr_datetime, '%Y.%m.%d') as format_wr_datetime
	,use_court
	from gym_data join match_data join match_gym_data
	on gym_data.wr_id = match_gym_data.gym_id and match_gym_data.match_id = match_data.code
	where match_data.code = '{$c}'";
	$gym = sql_query($gym_sql);
	$date_data;
	$gym_data;


	while ($row = sql_fetch_array($gym)) {
		$date_data[$row['format_wr_datetime']] = $row;
		$gym_data[$row['format_wr_datetime']][$row['wr_id']] = $row;
	}

 ?>
<!-- Contents Area -->
<div class="pop_container">
	<?php if(false && ! $competition['opening_date']){?>
		<div class="empty_waiting">
			<div class="img_area">
				<img src="<?php echo G5_IMG_URL.'/common/intro_logo1.png';?>">
			</div>
			<div class="cmt ani02">
				경기가 준비중입니다.
			</div>
		</div>
	<?php }else {?>

			<section>
				<div class="pop_hd">
					대회선택
				</div>
				<div class="content">
					<select class="full-width form-control" id="match_list" class="" name="m">
						<option value="">경기를 선택해주세요</option>
						<?php
							while($row = sql_fetch_array($match_result)){?>
								<option <?=$c == $row['code'] ? 'selected="selected"' : ""?> value="<?=$row['code']?>"><?=$row['wr_name']?></option>
							<?php }?>
					</select>
				</div>
			</section>
			<script>$('#match_list').change(function(){window.location.href='?c='+$(this).val();});</script>

			<?php
			if (count($date_data) > 1) {?>
				<section>
					<div class="pop_hd">
						<div class="tit">
							날짜 선택
						</div>
					</div>
					<div class="content">
						<div id="court_list" class="btn-group" role="group">
						<ul class="btn-list">
				<?php
						foreach($date_data as $date){?>
							<li><a href="?test=test"><?=$date['format_wr_datetime']?></a></li>
						<?php }?>
						</ul>
					</div>
					</div>
				</section>
				<?php }else{
					$dt = array_keys($date_data)[0];
				}?>
	<!-- 탭-코트-->
	<section class="section2">
		<div class="pop_hd">
			<div class="tit">경기장 선택</div>
		</div>
		<div class="content ">

			<select class="full-width form-control" id="gym">
				<option value="">경기장 선택</option>
				<?php

					foreach($gym_data[$dt] as $gym_key => $value){?>
						<option value="<?=$value['wr_id']?>"
							<?=  $g == '' || $g == $value['wr_id'] ? "selected=\"selected\"" :""?>>
							<?=$value['gym_name']?></option>
					<?php
					if($g == '') $g = $value['wr_id'];
				}?>

			</select>
			<div id="court_list" class="btn-group" role="group">
				<ul class="btn-list">
					<?php
					$court = $gym_data[$dt][$g]['use_court'];
					if($ct == ''){$ct = 1;}
					for ($i=0; $i < $court; $i++) {?>
						<li><a <?=$ct ==  $i+1? 'class="active"':""?>
							href="?c=<?=$c?>&dt=<?=$dt?>&g=<?=$g?>&ct=<?=$i+1?>">
							<?=$i+1?>코트</a></li>
					<?php

					}?>
				</ul>
	        </div>
		</div>
	</section>
	<!-- //탭-코트-->
	<?php
		if($dt != '' && $g != ''  && $ct != ''){

			$redirectUrl = urlencode (G5_AOS_URL."/competion_mnt_list.php?c=$c&dt=$dt&g=$g&ct=$ct");

			$matchProcessSql = "SELECT
			game_score_data.wr_id as game_score_id,
				group_data.code as group_code,
				group_data.num, team_1_code,
				(select concat(club,'-', team_1_name,'-',team_2_name) from team_data where team_code = team_1_code) as team_1,
				(select concat(club,'-', team_1_name,'-',team_2_name) from team_data where team_code = team_2_code) as team_2,
				team_1_score,
				team_2_score,
				end_game,
				game_start_time,
				game_end_time,
				is_on
		FROM
				game_score_data
						JOIN
				group_data ON game_score_data.group_code = group_data.code
		WHERE
				game_score_data.match_code = '{$c}'
				and group_data.game_court = {$ct}
				AND team_1_code <> ''
		ORDER BY group_code, num";
			$matchProcess = sql_query($matchProcessSql, true);
			$matchIndex = 0;
			$match_group_code = '';
			$match_group_text = '';


			while($match_row = sql_fetch_array($matchProcess)){
				$matchIndex++;
				if($match_group_code == '' || $match_group_code != $match_row['group_code'] ){?>

					<?php if($match_group_code != '' && $match_group_code != $match_row['group_code']){?>

									</tbody></table></div></div></section>
					<?php }?>
					<section>
						<div class="content">
						<div class="con_tit_area clear">
							<div class="tit">경기결과</div>
						</div>
						<div class="tbl_style02 tbl_striped">
							<table>
								<thead>
									<tr>
										<th>경기</th>
										<th>클럽</th>
										<th>이름</th>
										<th>점수</th>
										<th>이름</th>
										<th>클럽</th>
									</tr>
								</thead>
								<tbody>
				<?php
				$match_group_code = $match_row['group_code'];
			} ?>


								<?php
									$team1 = explode('-',$match_row['team_1']);
									$team2 = explode('-',$match_row['team_2']);
									?>
									<tr>
										<td class="text-center"><?=$matchIndex?><br/>경기</td>
										<td class="text-center"><?=$team1['0']?><br/><?=$team1['0']?></td>
										<td class="text-center"><?=$team1['1']?><br/><?=$team1['2']?></td>
										<td class="match_point text-center">
											<?php
												if($match_row['end_game'] == 'N'){
													if($match_row['is_on'] == 'N'){?>
														<div class="tournament_point">
															<a href="process/start_game.php?c=<?=$c?>&dt=<?=$dt?>&g=<?=$g?>&ct=<?=$ct?>&gid=<?=$match_row['game_score_id']?>&rd=<?=$redirectUrl?>" class="btn">경기시작</a>
														</div>
													<?php }else{?>
														<div class="tournament_point">
			                	<a href="popup_insert_score.php?gid=<?=$match_row['game_score_id']?>&rd=<?=$redirectUrl?>" target="_blank" class="btn bkg6">점수입력</a>
			                </div>
													<?php }
												}else{?>
														<span><?=$match_row['team_1_score']?></span>&nbsp;:&nbsp;<span><?=$match_row['team_2_score']?></span>
												<?php }?>

										</td>
										<td class="text-center"><?=$team2['1']?><br/><?=$team2['2']?></td>
										<td class="text-center"><?=$team2['0']?><br/><?=$team2['0']?></td>
									</tr>

								<?php }?>

		<?php }?>

	<?php }?>

</div>
<!-- end Contents Area -->

<script>
$('#gym').change(function(event){
	window.location.href='?c=<?=$c?>&d='+encodeURIComponent('<?=$d?>')+'&g='+$(this).val();
});

</script>
<?php
include_once(G5_AOS_PATH.'/tail.php');
?>
