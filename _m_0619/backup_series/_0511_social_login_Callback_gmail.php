<?php



include_once('../common.php');
  include_once(G5_LIB_PATH.'/register.lib.php');


  if(get_session('ss_mb_id') != ''){
      goto_url('./index.php');
  }


$path = __DIR__."/../lib/google-plus-api-client-master";
// require_once $path."/vendor/autoload.php";

// require_once $path.'/src/Google_Client.php';
// require_once $path.'src/contrib/Google_Oauth2Service.php';

require_once $path.'/src/Google_Client.php';
// print $path.'/src/Google_Client.php';
require_once $path.'/src/contrib/Google_Oauth2Service.php';




/*
/*
 * Copyright 2011 Google Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
session_start();
$client = new Google_Client();
$client->setApplicationName("Google UserInfo PHP Starter Application");
// Visit https://code.google.com/apis/console?api=plus to generate your
// oauth2_client_id, oauth2_client_secret, and to register your oauth2_redirect_uri.
$client->setClientId('37351104984-mi2pp9a2km3tbejk7af9r9cnkh8b8pen.apps.googleusercontent.com');
$client->setClientSecret('3Yh00sPvaBCmbFVeGG4b0YEy');
$client->setRedirectUri('http://test.raizup.kr/tennis2/m/social_login_callback_gmail.php');
$client->setScopes(array('https://www.googleapis.com/auth/userinfo.email','https://www.googleapis.com/auth/userinfo.profile'));
// $client->setDeveloperKey('insert_your_developer_key');


// print_r($_SESSION);
$oauth2 = new Google_Oauth2Service($client);
if (isset($_GET['code'])) {
  $client->authenticate($_GET['code']);
  $_SESSION['access_token'] = $client->getAccessToken();
  $redirect = 'http://' . $_SERVER['HTTP_HOST'] . $_SERVER['PHP_SELF'];
  header('Location: ' . filter_var($redirect, FILTER_SANITIZE_URL));
  return;
}
if (isset($_SESSION['access_token'])) {
 $client->setAccessToken($_SESSION['access_token']);
 $html = file_get_contents("https://www.googleapis.com/oauth2/v1/userinfo?alt=json&access_token=ya29.GltABEI3DGACP2AIa8uJivupNHy6i0OTCfUKPCWXmr8UKQIBTUht8sAjPKPpUX5iwzzkjqdloR4uNTX5ZLpWmIUCdhe0rEleWL2LGw8v6JyV7QWi1t2q-TYW26jF");
 print $html;
}
if (isset($_REQUEST['logout'])) {
  unset($_SESSION['access_token']);
  $client->revokeToken();
}
if ($client->getAccessToken()) {
  $user = $oauth2->userinfo->get();
  // These fields are currently filtered through the PHP sanitize filters.
  // See http://www.php.net/manual/en/filter.filters.sanitize.php
  $email = filter_var($user['email'], FILTER_SANITIZE_EMAIL);
  $img = filter_var($user['picture'], FILTER_VALIDATE_URL);
  $personMarkup = "$email<div><img src='$img?sz=50'></div>";
  // The access token may have been updated lazily.
  $_SESSION['token'] = $client->getAccessToken();

} else {
  $authUrl = $client->createAuthUrl();
}
?>
