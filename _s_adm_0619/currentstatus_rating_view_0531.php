<?php
$menu_cate2 ='2';
$menu_cate3 ='2';
include_once('./_common.php');
include_once(G5_SADM_PATH.'/_head.php');

?>
<script src="<?php echo G5_SADM_URL?>/assets/js/autosave"></script>
<!-- sub hd_section-->
	<div class="tit_area">
		<div class="tit_subject"><?php echo $menu_title; ?>(메인 카테고리영역)</div>
		<div class="tit">경기상황별 자세히보는 화면 뷰페이지</div>
		<div class="r-btn-area">
			<ul>
				<li><a href="" class="btn_default">대진 상황표 이동</a></li>
			</ul>
		</div>
	</div>
<!--//sub_hd_area -->

<!-- !PAGE CONTENT! -->
<div>

	
	<!-- 그룹명  -->
	<div class="sub_hd">
        <div class="tit"><?php echo $division." ".$series." ".$series_sub;?> - 30명 - 10조</div>
    </div>
    <!-- //그룹명  -->

<style>
.champ_match_result{margin-bottom:20px}
.champ_match_result ul li{float:left;width:32%;margin-right:2%;margin-bottom:15px;position:relative;}
.champ_match_result ul li:nth-child(3n+3){margin-right:0%;}
.champ_match_result ul li .tit_area{background-color:#d9edf7;padding:10px 15px;font-weight:600;border:1px solid #000;border-bottom:1px solid #000}
.champ_match_result ul li .tit_area .r-btn-area{position:absolute;right:0;top:0;padding:15px;font-size:11px;font-family:'dotum'}

</style>
	<div class="sub_hd">
        <div class="tit">토너먼트 대진표 출력-토너먼트 배정시 이름 출력</div>
        <div style="background-color:#ccc;height:500px">
        	<iframe src="http://www.aropupu.fi/bracket/" style="width:100%;height:100%"></iframe>
        </div>
    </div>


	<div class="champ_match_result">
		<ul>
			<?php 
	           	$sql = "select * from group_data where match_code='$code' and division='$division' and series='$series' and series_sub='$series_sub' and tournament	='L' order by num";
				$gourp_result = sql_query($sql);
				$i = 0;
				$j = 1;
				
			?>
			
			<?php while($group = sql_fetch_array($gourp_result)){ ?>
			<li style="min-height: 460px;">				
				<div class="tit_area">
					<span style="font-size:18px">1조</span>
					코트: ㅇㅇ체육관 ㅇ코트 ㅇ경기
				</div>
				
				<div class="text-center">
					<style>
						
					
						.current_rating_tbl{margin-bottom:20px}
						.current_rating_tbl thead tr th{font-weight:300;color:#ccc;border:1px solid #ccc;padding:5px}
						.current_rating_tbl tbody tr td{font-weight:300;color:#000;border:1px solid #ccc;}
						.current_rating_tbl tbody tr th{font-weight:300;color:#000;border:1px solid #ccc;padding:5px;position:relative}
						.current_rating_tbl tbody tr:nth-child(1) td:nth-child(2),
						.current_rating_tbl tbody tr:nth-child(2) td:nth-child(3),
						.current_rating_tbl tbody tr:nth-child(3) td:nth-child(4),
						.current_rating_tbl tbody tr:nth-child(4) td:nth-child(5),
						.current_rating_tbl tbody tr:nth-child(5) td:nth-child(6){background:url('http://cockss.com/assets/img/tournament/slash.png');background-size: 100% 100%;}
						.current_rating_tbl tbody tr td{font-weight:300;color:rgba(000,000,000,0.5)}
						.current_rating_tbl tbody tr td span:first-child{padding-right:10px}
						.current_rating_tbl tbody tr td span:last-child{padding-left:10px}
						
						.current_rating_tbl tbody tr td.win{color:rgba(000,000,000,1);}
						.current_rating_tbl tbody tr td.win span:first-child{color:red;font-weight:600;}
						.current_rating_tbl tbody tr td.win span:last-child{color:#000}
						
						.current_rating_tbl tbody tr.winner1 th:after{content:"1위";position:absolute;left:0;top:0;background-color:red;padding:3px;border-radius:50px;color:#fff;font-size:10px;}
						.current_rating_tbl tbody tr.winner1 {background:rgba(232,24,12,0.1)}
						
						.current_rating_tbl tbody tr.winner2 th:after{content:"2위";position:absolute;left:0;top:0;background-color:#e8117f ;padding:3px;border-radius:50px;color:#fff;font-size:10px;}
						.current_rating_tbl tbody tr.winner2 {background:rgba(000,000,000,0.05)}
						
					</style>
					<div class="current_rating_tbl">
						<table>
	            			<thead>
	            				<tr>
	            				<th>VIEW =></th>
	            				<?php
	            					if($group['team_1']!=""){
	                					$team_sql = "select * from team_data where match_code = '$code' and team_code = '$group[team_1]'";
										$team_result = sql_query($team_sql);
										$r = sql_fetch_array($team_result);
									?>
	                				<th class="text-center"><?=$r['club'];?><br/><span class="font-s08"><?=$r['team_1_name'];?>,<?=$r['team_2_name'];?></span></th>
	                				<?php }	?>
	                				<?php
	            					if($group['team_2']!=""){
	                					$team_sql = "select * from team_data where match_code = '$code' and team_code = '$group[team_2]'";
										$team_result = sql_query($team_sql);
										$r = sql_fetch_array($team_result);
									?>
	                				<th class="text-center"><?=$r['club'];?><br/><span class="font-s08"><?=$r['team_1_name'];?>,<?=$r['team_2_name'];?></span></th>
	                				<?php }	?>
	                				<?php
	            					if($group['team_3']!=""){
	                					$team_sql = "select * from team_data where match_code = '$code' and team_code = '$group[team_3]'";
										$team_result = sql_query($team_sql);
										$r = sql_fetch_array($team_result);
									?>
	                				<th class="text-center"><?=$r['club'];?><br/><span class="font-s08"><?=$r['team_1_name'];?>,<?=$r['team_2_name'];?></span></th>
	                				<?php }	?>
	                				<?php
	            					if($group['team_4']!=""){
	                					$team_sql = "select * from team_data where match_code = '$code' and team_code = '$group[team_4]'";
										$team_result = sql_query($team_sql);
										$r = sql_fetch_array($team_result);
									?>
	                				<th class="text-center"><?=$r['club'];?><br/><span class="font-s08"><?=$r['team_1_name'];?>,<?=$r['team_2_name'];?></span></th>
	                				<?php }	?>
	                				<?php
	            					if($group['team_5']!=""){
	                					$team_sql = "select * from team_data where match_code = '$code' and team_code = '$group[team_5]'";
										$team_result = sql_query($team_sql);
										$r = sql_fetch_array($team_result);
									?>
	                				<th class="text-center"><?=$r['club'];?><br/><span class="font-s08"><?=$r['team_1_name'];?>,<?=$r['team_2_name'];?></span></th>
	                				<?php }	?>
	            				<tr>
	            				</tr>
	            				<!-- <tr>
	                				<th class="text-center">춘천클럽<br><span class="font-s08">전선희, 도미현</span></th>
	                				<th class="text-center">나래<br><span class="font-s08">김명미, 신은미</span></th>
	                				<th class="text-center">신남<br><span class="font-s08">차미옥, 박미향</span></th>
	                				<th class="text-center">한마음<br><span class="font-s08">김영옥, 이미경</span></th>
	            				</tr> -->
	            			</thead>
							<tbody class="text-center">
								<?php
                					if($group['team_1']!=""){
                    					$team_sql = "select * from team_data where match_code = '$code' and team_code = '$group[team_1]'";
										$team_result = sql_query($team_sql);
										$r = sql_fetch_array($team_result);
										
										$score_sql = "select * from game_score_data where match_code = '$code' and tournament = 'L' and (team_1_code = '$group[team_1]' or team_2_code = '$group[team_1]') and (team_1_code = '$group[team_2]' or team_2_code = '$group[team_2]')";
										$score_result = sql_query($score_sql);
										$score1 = sql_fetch_array($score_result);

										$score_sql = "select * from game_score_data where match_code = '$code' and tournament = 'L' and (team_1_code = '$group[team_1]' or team_2_code = '$group[team_1]') and (team_1_code = '$group[team_3]' or team_2_code = '$group[team_3]')";
										$score_result = sql_query($score_sql);
										$score2 = sql_fetch_array($score_result);
										
										if($group['team_4']!=""){
											$score_sql = "select * from game_score_data where match_code = '$code' and tournament = 'L' and (team_1_code = '$group[team_1]' or team_2_code = '$group[team_1]') and (team_1_code = '$group[team_4]' or team_2_code = '$group[team_4]')";
											$score_result = sql_query($score_sql);
											$score3 = sql_fetch_array($score_result);
										}
										if($group['team_5']!=""){
											$score_sql = "select * from game_score_data where match_code = '$code' and tournament = 'L' and (team_1_code = '$group[team_1]' or team_2_code = '$group[team_1]') and (team_1_code = '$group[team_5]' or team_2_code = '$group[team_5]')";
											$score_result = sql_query($score_sql);
											$score4 = sql_fetch_array($score_result);
										}
								?>
                				<tr>
                					<td class="font-w700 teamA_winner"><?=$r['club'];?><br/><span class="font-s08"><?=$r['team_1_name'];?>, <?=$r['team_2_name'];?></span></td>
                					<td class="text-center table_slash"></td>
                					
                					<?php if($score1['team_1_code'] == $group['team_1']){ ?>
                					<td class="text-center"><h3><?=$score1['team_1_score']?>&nbsp;:&nbsp;<?=$score1['team_2_score']?></h3></td>
                					<?php }?>
                					<?php if($score1['team_2_code'] == $group['team_1']){ ?>
                					<td class="text-center"><h3><?=$score1['team_2_score']?>&nbsp;:&nbsp;<?=$score1['team_1_score']?></h3></td>
                					<?php }?>
                					
                					<?php if($score2['team_1_code'] == $group['team_1']){ ?>
                					<td class="text-center"><h3><?=$score2['team_1_score']?>&nbsp;:&nbsp;<?=$score2['team_2_score']?></h3></td>
                					<?php }?>
                					<?php if($score2['team_2_code'] == $group['team_1']){ ?>
                					<td class="text-center"><h3><?=$score2['team_2_score']?>&nbsp;:&nbsp;<?=$score2['team_1_score']?></h3></td>
                					<?php }?>
                					
                					<?php if($group['team_4']!=""){ ?>
                    					<?php if($score3['team_1_code'] == $group['team_1']){?>
                    					<td class="text-center"><h3><?=$score3['team_1_score']?>&nbsp;:&nbsp;<?=$score3['team_2_score']?></h3></td>
                    					<?php }?>
                    					<?php if($score3['team_2_code'] == $group['team_1']){?>
                    					<td class="text-center"><h3><?=$score3['team_2_score']?>&nbsp;:&nbsp;<?=$score3['team_1_score']?></h3></td>
                    					<?php }?>
                					<?php }	?>
                					
                					<?php if($group['team_5']!=""){?>
                    					<?php if($score4['team_1_code'] == $group['team_1']){ ?>
                    					<td class="text-center"><h3><?=$score4['team_1_score']?>&nbsp;:&nbsp;<?=$score4['team_2_score']?></h3></td>
                    					<?php }?>
                    					<?php if($score4['team_2_code'] == $group['team_1']){ ?>
                    					<td class="text-center"><h3><?=$score4['team_2_score']?>&nbsp;:&nbsp;<?=$score4['team_1_score']?></h3></td>
                    					<?php }?>
                					<?php }	?>
                				</tr>
                				<?php
									}
                				?>
								<?php
                					if($group['team_2']!=""){
                    					$team_sql = "select * from team_data where match_code = '$code' and team_code = '$group[team_2]'";
										$team_result = sql_query($team_sql);
										$r = sql_fetch_array($team_result);
										
										$score_sql = "select * from game_score_data where match_code = '$code' and tournament = 'L' and (team_1_code = '$group[team_2]' or team_2_code = '$group[team_2]') and (team_1_code = '$group[team_1]' or team_2_code = '$group[team_1]')";
										$score_result = sql_query($score_sql);
										$score1 = sql_fetch_array($score_result);
										
										$score_sql = "select * from game_score_data where match_code = '$code' and tournament = 'L' and (team_1_code = '$group[team_2]' or team_2_code = '$group[team_2]') and (team_1_code = '$group[team_3]' or team_2_code = '$group[team_3]')";
										$score_result = sql_query($score_sql);
										$score2 = sql_fetch_array($score_result);
										
										if($group['team_4']!=""){
											$score_sql = "select * from game_score_data where match_code = '$code' and tournament = 'L' and (team_1_code = '$group[team_2]' or team_2_code = '$group[team_2]') and (team_1_code = '$group[team_4]' or team_2_code = '$group[team_4]')";
											$score_result = sql_query($score_sql);
											$score3 = sql_fetch_array($score_result);
										}
										if($group['team_5']!=""){
											$score_sql = "select * from game_score_data where match_code = '$code' and tournament = 'L' and (team_1_code = '$group[team_2]' or team_2_code = '$group[team_2]') and (team_1_code = '$group[team_5]' or team_2_code = '$group[team_5]')";
											$score_result = sql_query($score_sql);
											$score4 = sql_fetch_array($score_result);
										}					
								?>
                				<tr>
                					<td class="font-w700 teamA_winner"><?=$r['club'];?><br/><span class="font-s08"><?=$r['team_1_name'];?>, <?=$r['team_2_name'];?></span></td>
                					<?php if($score1['team_1_code'] == $group['team_2']){?>
                					<td class="text-center"><h3><?=$score1['team_1_score']?>&nbsp;:&nbsp;<?=$score1['team_2_score']?></h3></td>
                					<?php }?>
                					<?php if($score1['team_2_code'] == $group['team_2']){?>
                					<td class="text-center"><h3><?=$score1['team_2_score']?>&nbsp;:&nbsp;<?=$score1['team_1_score']?></h3></td>
                					<?php }?>
                					
                					<td class="text-center table_slash"></td>
                					
									<?php if($score2['team_1_code'] == $group['team_2']){?>
                					<td class="text-center"><h3><?=$score2['team_1_score']?>&nbsp;:&nbsp;<?=$score2['team_2_score']?></h3></td>
                					<?php }?>
                					<?php if($score2['team_2_code'] == $group['team_2']){?>
                					<td class="text-center"><h3><?=$score2['team_2_score']?>&nbsp;:&nbsp;<?=$score2['team_1_score']?></h3></td>
                					<?php }?>		                    					
                					
                					
                					<?php if($group['team_4']!=""){?>
                    					<?php if($score3['team_1_code'] == $group['team_2']){?>
                    					<td class="text-center"><h3><?=$score3['team_1_score']?>&nbsp;:&nbsp;<?=$score3['team_2_score']?></h3></td>
                    					<?php }?>
                    					<?php if($score3['team_2_code'] == $group['team_2']){?>
                    					<td class="text-center"><h3><?=$score3['team_2_score']?>&nbsp;:&nbsp;<?=$score3['team_1_score']?></h3></td>
                    					<?php }?>
                					<?php }	?>
                					
                					<?php if($group['team_5']!=""){?>
                    					<?php if($score4['team_1_code'] == $group['team_2']){?>
                    					<td class="text-center"><h3><?=$score4['team_1_score']?>&nbsp;:&nbsp;<?=$score4['team_2_score']?></h3></td>
                    					<?php }?>
                    					<?php if($score4['team_2_code'] == $group['team_2']){?>
                    					<td class="text-center"><h3><?=$score4['team_2_score']?>&nbsp;:&nbsp;<?=$score4['team_1_score']?></h3></td>
                    					<?php }?>
                					<?php }	?>
                				</tr>
                				<?php
									}
                				?>
								<?php
                					if($group['team_3']!=""){
                    					$team_sql = "select * from team_data where match_code = '$code' and team_code = '$group[team_3]'";
										$team_result = sql_query($team_sql);
										$r = sql_fetch_array($team_result);
										
										$score_sql = "select * from game_score_data where match_code = '$code' and tournament = 'L' and (team_1_code = '$group[team_3]' or team_2_code = '$group[team_3]') and (team_1_code = '$group[team_1]' or team_2_code = '$group[team_1]')";
										$score_result = sql_query($score_sql);
										$score1 = sql_fetch_array($score_result);
										 
										$score_sql = "select * from game_score_data where match_code = '$code' and tournament = 'L' and (team_1_code = '$group[team_3]' or team_2_code = '$group[team_3]') and (team_1_code = '$group[team_2]' or team_2_code = '$group[team_2]')";
										$score_result = sql_query($score_sql);
										$score2 = sql_fetch_array($score_result);
										
										if($group['team_4']!=""){
											$score_sql = "select * from game_score_data where match_code = '$code' and tournament = 'L' and (team_1_code = '$group[team_3]' or team_2_code = '$group[team_3]') and (team_1_code = '$group[team_4]' or team_2_code = '$group[team_4]')";
											$score_result = sql_query($score_sql);
											$score3 = sql_fetch_array($score_result);
										}
										if($group['team_5']!=""){
											$score_sql = "select * from game_score_data where match_code = '$code' and tournament = 'L'  and (team_1_code = '$group[team_3]' or team_2_code = '$group[team_3]') and (team_1_code = '$group[team_5]' or team_2_code = '$group[team_5]')";
											$score_result = sql_query($score_sql);
											$score4 = sql_fetch_array($score_result);
										}
								?>
                				<tr>
                					<td class="font-w700 teamA_winner"><?=$r['club'];?><br/><span class="font-s08"><?=$r['team_1_name'];?>, <?=$r['team_2_name'];?></span></td>
                					
                					<?php if($score1['team_1_code'] == $group['team_3']){?>
                					<td class="text-center"><h3><?=$score1['team_1_score']?>&nbsp;:&nbsp;<?=$score1['team_2_score']?></h3></td>
                					<?php }?>
                					<?php if($score1['team_2_code'] == $group['team_3']){?>
                					<td class="text-center"><h3><?=$score1['team_2_score']?>&nbsp;:&nbsp;<?=$score1['team_1_score']?></h3></td>
                					<?php }?>
                					
									<?php if($score2['team_1_code'] == $group['team_3']){?>
                					<td class="text-center"><h3><?=$score2['team_1_score']?>&nbsp;:&nbsp;<?=$score2['team_2_score']?></h3></td>
                					<?php }?>
                					<?php if($score2['team_2_code'] == $group['team_3']){?>
                					<td class="text-center"><h3><?=$score2['team_2_score']?>&nbsp;:&nbsp;<?=$score2['team_1_score']?></h3></td>
                					<?php }?>		                    					
                					<td class="text-center table_slash"></td>
                					<?php if($group['team_4']!=""){?>
                    					<?php if($score3['team_1_code'] == $group['team_3']){?>
                    					<td class="text-center"><h3><?=$score3['team_1_score']?>&nbsp;:&nbsp;<?=$score3['team_2_score']?></h3></td>
                    					<?php }?>
                    					<?php if($score3['team_2_code'] == $group['team_3']){?>
                    					<td class="text-center"><h3><?=$score3['team_2_score']?>&nbsp;:&nbsp;<?=$score3['team_1_score']?></h3></td>
                    					<?php }?>
                					<?php }	?>
                					<?php if($group['team_5']!=""){?>
                    					<?php if($score4['team_1_code'] == $group['team_3']){?>
                    					<td class="text-center"><h3><?=$score4['team_1_score']?>&nbsp;:&nbsp;<?=$score4['team_2_score']?></h3></td>
                    					<?php }?>
                    					<?php if($score4['team_2_code'] == $group['team_3']){?>
                    					<td class="text-center"><h3><?=$score4['team_2_score']?>&nbsp;:&nbsp;<?=$score4['team_1_score']?></h3></td>
                    					<?php }?>
                					<?php }	?>
                				</tr>
                				<?php
									}
                				?>

								<?php
                					if($group['team_4']!=""){
                    					$team_sql = "select * from team_data where match_code = '$code' and team_code = '$group[team_4]'";
										$team_result = sql_query($team_sql);
										$r = sql_fetch_array($team_result);
										
										$score_sql = "select * from game_score_data where match_code = '$code' and tournament = 'L' and (team_1_code = '$group[team_4]' or team_2_code = '$group[team_4]') and (team_1_code = '$group[team_1]' or team_2_code = '$group[team_1]')";
										$score_result = sql_query($score_sql);
										$score1 = sql_fetch_array($score_result);
										
										$score_sql = "select * from game_score_data where match_code = '$code' and tournament = 'L' and (team_1_code = '$group[team_4]' or team_2_code = '$group[team_4]') and (team_1_code = '$group[team_2]' or team_2_code = '$group[team_2]')";
										$score_result = sql_query($score_sql);
										$score2 = sql_fetch_array($score_result);
										
										if($group['team_4']!=""){
											$score_sql = "select * from game_score_data where match_code = '$code' and tournament = 'L' and (team_1_code = '$group[team_4]' or team_2_code = '$group[team_4]') and (team_1_code = '$group[team_3]' or team_2_code = '$group[team_3]')";
											$score_result = sql_query($score_sql);
											$score3 = sql_fetch_array($score_result);
										}
										if($group['team_5']!=""){
											$score_sql = "select * from game_score_data where match_code = '$code' and tournament = 'L' and (team_1_code = '$group[team_4]' or team_2_code = '$group[team_4]') and (team_1_code = '$group[team_5]' or team_2_code = '$group[team_5]')";
											$score_result = sql_query($score_sql);
											$score4 = sql_fetch_array($score_result);
										}
								?>
                				<tr>
                					<td class="font-w700 teamA_winner"><?=$r['club'];?><br/><span class="font-s08"><?=$r['team_1_name'];?>, <?=$r['team_2_name'];?></span></td>
                					
                					<?php if($score1['team_1_code'] == $group['team_4']){?>
                					<td class="text-center"><h3><?=$score1['team_1_score']?>&nbsp;:&nbsp;<?=$score1['team_2_score']?></h3></td>
                					<?php }?>
                					<?php if($score1['team_2_code'] == $group['team_4']){?>
                					<td class="text-center"><h3><?=$score1['team_2_score']?>&nbsp;:&nbsp;<?=$score1['team_1_score']?></h3></td>
                					<?php }?>
                					
									<?php if($score2['team_1_code'] == $group['team_4']){?>
                					<td class="text-center"><h3><?=$score2['team_1_score']?>&nbsp;:&nbsp;<?=$score2['team_2_score']?></h3></td>
                					<?php }?>
                					<?php if($score2['team_2_code'] == $group['team_4']){?>
                					<td class="text-center"><h3><?=$score2['team_2_score']?>&nbsp;:&nbsp;<?=$score2['team_1_score']?></h3></td>
                					<?php }?>		                    					

									<?php if($score3['team_1_code'] == $group['team_4']){?>
                					<td class="text-center"><h3><?=$score3['team_1_score']?>&nbsp;:&nbsp;<?=$score3['team_2_score']?></h3></td>
                					<?php }?>
                					<?php if($score3['team_2_code'] == $group['team_4']){?>
                					<td class="text-center"><h3><?=$score3['team_2_score']?>&nbsp;:&nbsp;<?=$score3['team_1_score']?></h3></td>
                					<?php }?>		                    					

                					<?php if($group['team_4']!=""){?>
                					<td class="text-center table_slash"></td>
                					<?php }	?>
                					
                					<?php if($group['team_5']!=""){?>
                    					<?php if($score4['team_1_code'] == $group['team_5']){?>
                    					<td class="text-center"><h3><?=$score4['team_1_score']?>&nbsp;:&nbsp;<?=$score4['team_2_score']?></h3></td>
                    					<?php }?>
                    					<?php if($score4['team_2_code'] == $group['team_5']){?>
                    					<td class="text-center"><h3><?=$score4['team_2_score']?>&nbsp;:&nbsp;<?=$score4['team_1_score']?></h3></td>
                    					<?php }?>
                					<?php }	?>
                				</tr>
                				<?php
									}
                				?>
								<?php
                					if($group['team_5']!=""){
                    					$team_sql = "select * from team_data where match_code = '$code' and team_code = '$group[team_5]'";
										$team_result = sql_query($team_sql);
										$r = sql_fetch_array($team_result);
										
										$score_sql = "select * from game_score_data where match_code = '$code' and tournament = 'L' and (team_1_code = '$group[team_5]' or team_2_code = '$group[team_5]') and (team_1_code = '$group[team_1]' or team_2_code = '$group[team_1]')";
										$score_result = sql_query($score_sql);
										$score1 = sql_fetch_array($score_result);
										
										$score_sql = "select * from game_score_data where match_code = '$code' and tournament = 'L' and (team_1_code = '$group[team_5]' or team_2_code = '$group[team_5]') and (team_1_code = '$group[team_2]' or team_2_code = '$group[team_2]')";
										$score_result = sql_query($score_sql);
										$score2 = sql_fetch_array($score_result);
										
										if($group['team_4']!=""){
											$score_sql = "select * from game_score_data where match_code = '$code' and tournament = 'L' and (team_1_code = '$group[team_5]' or team_2_code = '$group[team_5]') and (team_1_code = '$group[team_3]' or team_2_code = '$group[team_3]')";
											$score_result = sql_query($score_sql);
											$score3 = sql_fetch_array($score_result);
										}
										if($group['team_5']!=""){
											$score_sql = "select * from game_score_data where match_code = '$code' and tournament = 'L' and (team_1_code = '$group[team_5]' or team_2_code = '$group[team_5]') and (team_1_code = '$group[team_4]' or team_2_code = '$group[team_4]')";
											$score_result = sql_query($score_sql);
											$score4 = sql_fetch_array($score_result);
										}
								?>
                				<tr>
                					<td class="font-w700 teamA_winner"><?=$r['club'];?><br/><span class="font-s08"><?=$r['team_1_name'];?>, <?=$r['team_2_name'];?></span></td>		                    					
                					<?php if($score1['team_1_code'] == $group['team_5']){?>
                					<td class="text-center"><h3><?=$score1['team_1_score']?>&nbsp;:&nbsp;<?=$score1['team_2_score']?></h3></td>
                					<?php }?>
                					<?php if($score1['team_2_code'] == $group['team_5']){?>
                					<td class="text-center"><h3><?=$score1['team_2_score']?>&nbsp;:&nbsp;<?=$score1['team_1_score']?></h3></td>
                					<?php }?>
                					
									<?php if($score2['team_1_code'] == $group['team_5']){?>
                					<td class="text-center"><h3><?=$score2['team_1_score']?>&nbsp;:&nbsp;<?=$score2['team_2_score']?></h3></td>
                					<?php }?>
                					<?php if($score2['team_2_code'] == $group['team_5']){?>
                					<td class="text-center"><h3><?=$score2['team_2_score']?>&nbsp;:&nbsp;<?=$score2['team_1_score']?></h3></td>
                					<?php }?>		    
                					      
									<?php if($score3['team_1_code'] == $group['team_5']){?>
                					<td class="text-center"><h3><?=$score3['team_1_score']?>&nbsp;:&nbsp;<?=$score3['team_2_score']?></h3></td>
                					<?php }?>
                					<?php if($score3['team_2_code'] == $group['team_5']){?>
                					<td class="text-center"><h3><?=$score3['team_2_score']?>&nbsp;:&nbsp;<?=$score3['team_1_score']?></h3></td>
                					<?php }?>		          

                					<?php if($group['team_4']!=""){?>
                    					<?php if($score4['team_1_code'] == $group['team_5']){?>
                    					<td class="text-center"><h3><?=$score4['team_1_score']?>&nbsp;:&nbsp;<?=$score4['team_2_score']?></h3></td>
                    					<?php }?>
                    					<?php if($score4['team_2_code'] == $group['team_5']){?>
                    					<td class="text-center"><h3><?=$score4['team_2_score']?>&nbsp;:&nbsp;<?=$score4['team_1_score']?></h3></td>
                    					<?php }?>
                					<?php }	?>
                					
                					<?php if($group['team_5']!=""){?>
                					<td class="text-center table_slash"></td>
                					<?php }	?>
                					

                				</tr>
                				<?php
									}
                				?>
								<!-- <tr>
									<th class="font-w700 ">춘천클럽<br><span class="font-s08">전선희, 도미현</span></th>
									<td class="text-center "></td>
									<td class="text-center "><span>25</span>:<span>31</span></td>
									<td class="text-center win" ><span>25</span>:<span>31</span></td>
									<td class="text-center"><span>25</span>:<span>31</span></td>
								</tr>
							    <tr class="winner1">
									<th class="font-w700 win " style="background-color:#d9edf7;color:#000"나래<br><span class="font-s08">김명미, 신은미</span></th>
			    					<td class="text-center win"><span>25</span>:<span>31</span></td>
									<td class="text-center "></td>
									<td class="text-center win"><span>25</span>:<span>31</span></td>
									<td class="text-center win"><span>25</span>:<span>31</span></td>
								</tr>
								<tr class="winner2">
									<th class="font-w700 teamA_winner">신남<br><span class="font-s08">차미옥, 박미향</span></th>
									<td class="text-center"><span>25</span>:<span>31</span></td>
			    					<td class="text-center"><span>25</span>:<span>31</span></td>
									<td class="text-center "></td>
			    					<td class="text-center win"><span>25</span>:<span>31</span></td>
								<tr>
									<th class="font-w700 teamA_winner">한마음<br><span class="font-s08">김영옥, 이미경</span></th>
			    					<td class="text-center win"><span>25</span>:<span>31</span></td>
			    					<td class="text-center"><span>25</span>:<span>31</span></td>
			    					<td class="text-center"><span>25</span>:<span>31</span></td>
									<td class="text-center "></td>
								</tr> -->
	        				</tbody>
	                	</table>
	                </div>
	                <style>
	                	.rank_summary_tbl{border:1px solid #000;}
	                	.rank_summary_tbl thead tr th{color:#000;border:1px solid #ccc;padding:10px 5px;border-bottom:1px solid #000;background-color:#d9edf7}
						.rank_summary_tbl tbody tr{}
						.rank_summary_tbl tbody tr td{font-weight:300;border:1px solid #ccc;padding:5px 5px;line-height:22px;}
						.rank_summary_tbl tbody tr th{font-weight:300;border:1px solid #ccc;padding:5px;position:relative}

					</style>
	                <div class="rank_summary_tbl">
	                	<table>
	                		<thead>
	                			<tr>
	                				<th>순위</th>
	                				<th>클럽명 - 선수명</th>
	                				
	                				<th>승</th>
	                				<th>패</th>
	                			</tr>
	                		</thead>
	                		<tbody>
	                			<?php 
	                    			$group['division'];
                					$score_sql = "select * from team_data where (team_code  = '$group[team_1]' or team_code  = '$group[team_2]' or team_code  = '$group[team_3]' or team_code  = '$group[team_4]'  or team_code  = '$group[team_5]') and match_code = '$code' and division = '$division' and series = '$series' and series_sub = '$series_sub'  ORDER BY group_rank,team_league_point desc ,winning_rate	,gains_losses_point desc, team_total_match_point DESC";
									$score_result = sql_query($score_sql);
									$i = 1;
									while($score = sql_fetch_array($score_result)){
										
										$gourp_sql = "select num from group_data where (team_1  = '$score[team_code]' or team_2  = '$score[team_code]' or team_3  = '$score[team_code]' or team_4  = '$score[team_code]'  or team_5  = '$score[team_code]') and match_code = '$code' and division = '$division' and series = '$series' and series_sub = '$series_sub' and tournament = 'L' order by num asc";
										$group_result = sql_query($gourp_sql);
										$group = sql_fetch_array($group_result);
									?>
	                    			<tr class="winner1">
	                    				<td><?=$i;?></td>
	                    				<td><?=$score['club'];?><Br><?=$score['team_1_name'];?><?=$score['team_2_name'];?></td>
	                    				<td><?=$score['team_league_point'];?></td>
	                    				<td><?=$score['team_league_lose_count'];?></td>
	                    			</tr>
	                    			<?php $i++; } ?>
	                			<!-- <tr class="winner1">
	                				<td>1위</td>
	                				<td>
	                					<div>도깨비클럽 - 이우선</div>
	                					<div>사랑별클럽 - 이상훈</div>
	                				</td>
	                				<td>2</td>
	                				<td>1</td>
	                			</tr>
	                			<tr class="winner1">
	                				<th>2위</th>
	                				<td>
	                					<div>도깨비클럽 - 이우선</div>
	                					<div>사랑별클럽 - 이상훈</div>
	                				</td>
	                				<td>2</td>
	                				<td>1</td>
	                			</tr>
	                			<tr class="winner1">
	                				<td>3위</td>
	                				<td>
	                					<div>도깨비클럽 - 이우선</div>
	                					<div>사랑별클럽 - 이상훈</div>
	                				</td>
	                				<td>2</td>
	                				<td>1</td>
	                			</tr> -->
	                		</tbody>
	                	</table>
	                	
	                </div>
				</div>
			</li>
			
			<!-- <li>
				<div class="tit_area">
					1조
					코트: ㅇㅇ체육관 ㅇ코트 ㅇ경기
					
				</div>
				
				<div class="champ_group_table text-center">
					<table class="table table-vcenter">
            			<thead>
            				<tr>
            					<th></th>
                				<th class="text-center">춘천클럽<br><span class="font-s08">전선희, 도미현</span></th>
                				<th class="text-center">나래<br><span class="font-s08">김명미, 신은미</span></th>
                				<th class="text-center">신남<br><span class="font-s08">차미옥, 박미향</span></th>
                				<th class="text-center">한마음<br><span class="font-s08">김영옥, 이미경</span></th>
            				</tr>
            			</thead>
						<tbody class="text-center">
							<tr>
								<td class="font-w700 teamA_winner">춘천클럽<br><span class="font-s08">전선희, 도미현</span></td>
								<td class="text-center table_slash"></td>
								<td class="text-center"><h3>25&nbsp;:&nbsp;31</h3></td>
								<td class="text-center" style="background: #ededed;"><h3>31&nbsp;:&nbsp;22</h3></td>
								<td class="text-center"><h3>26&nbsp;:&nbsp;31</h3></td>
							</tr>
						    <tr>
								<td class="font-w700 teamA_winner">나래<br><span class="font-s08">김명미, 신은미</span></td>
		    					<td class="text-center"><h3>31&nbsp;:&nbsp;25</h3></td>
								<td class="text-center table_slash"></td>
								<td class="text-center"><h3>31&nbsp;:&nbsp;13</h3></td>
								<td class="text-center"><h3>31&nbsp;:&nbsp;29</h3></td>
							</tr>
							<tr>
								<td class="font-w700 teamA_winner">신남<br><span class="font-s08">차미옥, 박미향</span></td>
								<td class="text-center"><h3>22&nbsp;:&nbsp;31</h3></td>
		    					<td class="text-center"><h3>13&nbsp;:&nbsp;31</h3></td>
								<td class="text-center table_slash"></td>
		    					<td class="text-center"><h3>31&nbsp;:&nbsp;29</h3></td>
							<tr>
								<td class="font-w700 teamA_winner">한마음<br><span class="font-s08">김영옥, 이미경</span></td>
		    					<td class="text-center"><h3>31&nbsp;:&nbsp;26</h3></td>
		    					<td class="text-center"><h3>29&nbsp;:&nbsp;31</h3></td>
		    					<td class="text-center"><h3>29&nbsp;:&nbsp;31</h3></td>
								<td class="text-center table_slash"></td>
							</tr>
        				</tbody>
                	</table>
				</div>
			</li>
			
			<li>
				<div class="tit_area">
					1조
					코트: ㅇㅇ체육관 ㅇ코트 ㅇ경기
					
				</div>
				
				<div class="champ_group_table text-center">
					<table class="table table-vcenter">
            			<thead>
            				<tr>
            					<th></th>
                				<th class="text-center">춘천클럽<br><span class="font-s08">전선희, 도미현</span></th>
                				<th class="text-center">나래<br><span class="font-s08">김명미, 신은미</span></th>
                				<th class="text-center">신남<br><span class="font-s08">차미옥, 박미향</span></th>
                				<th class="text-center">한마음<br><span class="font-s08">김영옥, 이미경</span></th>
            				</tr>
            			</thead>
						<tbody class="text-center">
							<tr>
								<td class="font-w700 teamA_winner">춘천클럽<br><span class="font-s08">전선희, 도미현</span></td>
								<td class="text-center table_slash"></td>
								<td class="text-center"><h3>25&nbsp;:&nbsp;31</h3></td>
								<td class="text-center" style="background: #ededed;"><h3>31&nbsp;:&nbsp;22</h3></td>
								<td class="text-center"><h3>26&nbsp;:&nbsp;31</h3></td>
							</tr>
						    <tr>
								<td class="font-w700 teamA_winner">나래<br><span class="font-s08">김명미, 신은미</span></td>
		    					<td class="text-center"><h3>31&nbsp;:&nbsp;25</h3></td>
								<td class="text-center table_slash"></td>
								<td class="text-center"><h3>31&nbsp;:&nbsp;13</h3></td>
								<td class="text-center"><h3>31&nbsp;:&nbsp;29</h3></td>
							</tr>
							<tr>
								<td class="font-w700 teamA_winner">신남<br><span class="font-s08">차미옥, 박미향</span></td>
								<td class="text-center"><h3>22&nbsp;:&nbsp;31</h3></td>
		    					<td class="text-center"><h3>13&nbsp;:&nbsp;31</h3></td>
								<td class="text-center table_slash"></td>
		    					<td class="text-center"><h3>31&nbsp;:&nbsp;29</h3></td>
							<tr>
								<td class="font-w700 teamA_winner">한마음<br><span class="font-s08">김영옥, 이미경</span></td>
		    					<td class="text-center"><h3>31&nbsp;:&nbsp;26</h3></td>
		    					<td class="text-center"><h3>29&nbsp;:&nbsp;31</h3></td>
		    					<td class="text-center"><h3>29&nbsp;:&nbsp;31</h3></td>
								<td class="text-center table_slash"></td>
							</tr>
        				</tbody>
                	</table>
				</div>
			</li> -->
			<?php } ?>
		</ul>	
		
	</div>
	

	<div class="btn_area">
		<a class="btn btn01 fw-600" href="javascript:history.go(-1)">목록으로</a>
		<a class="btn btn01 fw-600" href="<?php echo G5_SADM_URL?>/championship_add_step3.php">대진 상황표 이동</a>
	</div>

</div>
<!-- End page content -->



<?php
include_once(G5_SADM_PATH.'/_tail.php');
?>
