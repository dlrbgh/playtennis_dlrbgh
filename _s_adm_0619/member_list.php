<?php
$menu_cate2 ='3';
$menu_cate3 ='1';

include_once('./_common.php');
include_once(G5_SADM_PATH.'/_head.php');

?>
<script src="<?php echo G5_SADM_URL?>/assets/js/autosave"></script>
<!-- sub hd_section-->
	<div class="tit_area">
		<div class="tit_subject">회원 관리</div>
		<div class="tit">회원 리스트</div>
		<div class="r-btn-area">
			<ul>
				<li><a href="<?php echo G5_SADM_URL?>/gym_update.php" class="btn_default btn_large">회원 등록</a></li>
			</ul>
		</div>
	</div>
<!--//sub hd_section -->

<!-- sub_hd_area -->
<div class="sub_hd">
    <div class="l_area">
        <ul>
        	<li><a href="" class="btn_default">전체회원</a></li>
        	<li><a href="" class="btn_default active">관리자회원</a></li>
        	<li><a href="" class="btn_default">일반회원</a></li>
        </ul>
        <ul>
        	<li><a href="" class="btn_default">등급전체</a></li>
        	<li><a href="" class="btn_default active">시/도 협회</a></li>
        	<li><a href="" class="btn_default">경기관리자</a></li>
        	<li><a href="" class="btn_default">시/군구 협회</a></li>
        	<li><a href="" class="btn_default">클럽장</a></li>
        	<li><a href="" class="btn_default">일반회원</a></li>
        </ul>
    </div>

	<!-- 우측 시작 { -->
	<div class="r_area">
		<ul>
			<li></li>
		</ul>
	</div>
	<!-- } 우측 끝 -->
</div>
<!--//sub_hd_area -->

<!-- !PAGE CONTENT! -->
<div>
	<div class="champ_rank_result tbl_striped">
	
		<table>
			<thead>
				<tr>
					<th>No</th>
					<th>명칭</th>
					<th>광역시/도</th>
					<th>시/군/구</th>
					<th>상세주소</th>
					<th>코트수</th>
					<th>위치</th>
					<th>관리</th>
				</tr>
			</thead>
			<tbody class="text-center">
				<tr>
					<td>1</td>
					<td>송암 야외 체육관 1</td>
					<td>강원도</td>
					<td>춘천</td>
					<td>강원도 춘천시 우두동 동부 송암 체육관</td>
					<td>10</td>
					<td><a href="" target="_blank">지도보기</a></td>
					<td><a href="" class="btn_default bkg5">삭제</a><a href="gym_update.php" class="btn_default bkg6">수정</a></td>
				</tr>
			</tbody>
		</table>
		
	</div>
	
	<nav class="pg_wrap">
		<span class="pg"><span class="sound_only">열린</span><strong class="pg_current">1</strong><span class="sound_only">페이지</span>
			<a href="" class="pg_page">2<span class="sound_only">페이지</span></a>
			<a href="" class="pg_page">3<span class="sound_only">페이지</span></a>
			<a href="" class="pg_page pg_end">맨끝</a>
		</span>
	</nav>
</div>
<!-- End page content -->



<?php
include_once(G5_SADM_PATH.'/_tail.php');
?>
