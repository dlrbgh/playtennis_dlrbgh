<?php
$menu_cate2 ='2';
$menu_cate3 ='2';
include_once('./_common.php');
include_once(G5_SADM_PATH.'/_head.php');
$code = $_REQUEST['wr_id'];

?>
<script src="<?php echo G5_SADM_URL?>/assets/js/autosave"></script>
<!-- sub hd_section-->
	<div class="tit_area">
		<div class="tit_subject">대회관리</div>
		<div class="tit">급수별 편성표 상세정보 </div>
		<div class="r-btn-area">
			<ul>
				<li><a href="" class="btn_default">저장하기</a></li>
				<li><a href="" class="btn_default">불러오기</a></li>
			</ul>
		</div>
	</div>
<!--//sub_hd_area -->

<!-- !PAGE CONTENT! -->
<div>

	<?php
		$team_field = "team_data";
		
		if($division == "단체전"){
			$team_field = "team_event_data";	
		}
		
	    $sql_team = "select count(wr_id) as cnt from $team_field where match_code='$code' and division='$division' and series ='$series' and series_sub ='$series_sub'";
		$team_result = sql_query($sql_team);
		$r = sql_fetch_array($team_result);
		
    	$sql_teams = "select count(wr_id) as cnt from $team_field where match_code='$code' and division='$division' and series ='$series' and series_sub ='$series_sub' and group_assign	= '0'";
		$team_results = sql_query($sql_teams);
		$rs = sql_fetch_array($team_results);
		
		$sql_team = "select count(wr_id) as cnt from group_data where match_code='$code' and division='$division' and series ='$series' and series_sub ='$series_sub'";
		$team_result = sql_query($sql_team);
		$group = sql_fetch_array($team_result);
	
	?>
	<!-- 그룹명  -->
	<div class="sub_hd">
        <div class="tit"><?php echo $division." ".$series." ".$series_sub;?> - <?php echo $r['cnt'];?>명 - <?php echo $group['cnt'];?>조</div>
    </div>
    <!-- //그룹명  -->
	<div class="champ_group_edit">
		<ul>
			<?php 
	           	$i = 0;
	            $sql = "select * from group_data where match_code='$code' and division='$division' and series='$series' and series_sub='$series_sub' order by num";
				$gourp_result = sql_query($sql);
			?>
			<?php while($group = sql_fetch_array($gourp_result)){?>
			<li style="min-height: 236px;">
				
				<div class="tit_area">
					<?php echo $group['num']+1;?>조
					<!-- <div class="r-btn-area">
						<a href="" class="btn add">공간 추가</a>
						<a href="" class="btn del">그룹 삭제</a>
					</div> -->
				</div>
				
				<div class="champ_group_table text-center">
					<table>
	        			<thead>
	        				<tr>
	            				<th>클럽명</th>
	            				<th>지역</th>
	            				<th>참가자</th>
	            				<!-- <th width="80">편집</th> -->
	        				</tr>
	        			</thead>
	        			<tbody>
	        				<tr>
	        					<?php
                					if($group['team_1']!=""){
                					if($division == "개인전"){
	                					$team_sql = "select * from team_data where match_code = '$code' and team_code = '$group[team_1]'";
										$team_result = sql_query($team_sql);
										$r = sql_fetch_array($team_result);
                					}
									if($division == "단체전"){
										$sql_team = "select *  from team_event_data where match_code = '$code' and team_code = '$group[team_1]'";
										$team_result = sql_query($sql_team);
										$r = sql_fetch_array($team_result);
                					}
                				?>
	        					<td><?=$r['club']?>
	        					</td>
	        					<td>
	        						<?=$r['area_1'];?> / <?=$r['area_2'];?><br/>
	        					</td>
	        					<td><?=$r['team_1_name'];?><br>
	        						<?=$r['team_2_name'];?><br>
	        						<?php
			        					if($group['division'] == "단체전"){
									?>
									<?=$r['team_3_name'];?><br> 
									<?=$r['team_4_name'];?><br> 
									<?=$r['team_5_name'];?><br> 
									<?=$r['team_6_name'];?><br> 
									<?=$r['team_7_name'];?><br> 
									<?=$r['team_8_name'];?><br> 
									<?php
										}
			        				?>
	        					</td>
	        					<td>
		                            <!-- <div class="btn-group">
		                                <button class="" type="button" data-toggle="modal" onclick="" title="팀 수정하기"><i class="flaticon-edit"></i></button>
		                                <button class=" " type="button" onclick="" title="팀 삭제하기" ><i class="flaticon-remove "></i></button>
		                            </div> -->
		                        </td>
		                        <?php }?>
	        				</tr>
	        						                    						                    					
	        				<tr>
	        					<?php
                					if($group['team_2']!=""){
                					if($division == "개인전"){
	                					$team_sql = "select * from team_data where match_code = '$code' and team_code = '$group[team_2]'";
										$team_result = sql_query($team_sql);
										$r = sql_fetch_array($team_result);
                					}
									if($division == "단체전"){
										$sql_team = "select *  from team_event_data where match_code = '$code' and team_code = '$group[team_2]'";
										$team_result = sql_query($sql_team);
										$r = sql_fetch_array($team_result);
                					}
                				?>
	        					<td><?=$r['club']?>
	        					</td>
	        					<td>
	        						<?=$r['area_1'];?> / <?=$r['area_2'];?><br/>
	        					</td>
	        					<td><?=$r['team_1_name'];?><br>
	        						<?=$r['team_2_name'];?><br>
	        						<?php
			        					if($group['division'] == "단체전"){
									?>
									<?=$r['team_3_name'];?><br> 
									<?=$r['team_4_name'];?><br> 
									<?=$r['team_5_name'];?><br> 
									<?=$r['team_6_name'];?><br> 
									<?=$r['team_7_name'];?><br> 
									<?=$r['team_8_name'];?><br> 
									<?php
										}
			        				?>
	        					</td>
	        					<td>
		                            <!-- <div class="btn-group">
		                                <button class="" type="button" data-toggle="modal" onclick="" title="팀 수정하기"><i class="flaticon-edit"></i></button>
		                                <button class=" " type="button" onclick="" title="팀 삭제하기" ><i class="flaticon-remove "></i></button>
		                            </div> -->
		                        </td>
		                        <?php }?>
	        				</tr>
	        						                    						                    					
	        				<tr>
	        					<?php
                					if($group['team_3']!=""){
									if($division == "개인전"){
	                					$team_sql = "select * from team_data where match_code = '$code' and team_code = '$group[team_3]'";
										$team_result = sql_query($team_sql);
										$r = sql_fetch_array($team_result);
                					}
									if($division == "단체전"){
										$sql_team = "select *  from team_event_data where match_code = '$code' and team_code = '$group[team_3]'";
										$team_result = sql_query($sql_team);
										$r = sql_fetch_array($team_result);
                					}
                				?>
	        					<td><?=$r['club']?>
	        					</td>
	        					<td>
	        						<?=$r['area_1'];?> / <?=$r['area_2'];?><br/>
	        					</td>
	        					<td><?=$r['team_1_name'];?><br>
	        						<?=$r['team_2_name'];?><br>
	        						<?php
			        					if($group['division'] == "단체전"){
									?>
									<?=$r['team_3_name'];?><br> 
									<?=$r['team_4_name'];?><br> 
									<?=$r['team_5_name'];?><br> 
									<?=$r['team_6_name'];?><br> 
									<?=$r['team_7_name'];?><br> 
									<?=$r['team_8_name'];?><br> 
									<?php
										}
			        				?>
	        					</td>
	        					<td>
		                            <!-- <div class="btn-group">
		                                <button class="" type="button" data-toggle="modal" onclick="" title="팀 수정하기"><i class="flaticon-edit"></i></button>
		                                <button class=" " type="button" onclick="" title="팀 삭제하기" ><i class="flaticon-remove "></i></button>
		                            </div> -->
		                        </td>
		                        <?php }?>
	        				</tr>
	        				<tr>
	        					<?php
                					if($group['team_4']!=""){
                					if($division == "개인전"){
	                					$team_sql = "select * from team_data where match_code = '$code' and team_code = '$group[team_4]'";
										$team_result = sql_query($team_sql);
										$r = sql_fetch_array($team_result);
                					}
									if($division == "단체전"){
										$sql_team = "select *  from team_event_data where match_code = '$code' and team_code = '$group[team_4]'";
										$team_result = sql_query($sql_team);
										$r = sql_fetch_array($team_result);
                					}
                				?>
	        					<td><?=$r['club']?>
	        					</td>
	        					<td>
	        						<?=$r['area_1'];?> / <?=$r['area_2'];?><br/>
	        					</td>
	        					<td><?=$r['team_1_name'];?> <br> <?=$r['team_2_name'];?></td>
	        					<td>
		                            <!-- <div class="btn-group">
		                                <button class="" type="button" data-toggle="modal" onclick="" title="팀 수정하기"><i class="flaticon-edit"></i></button>
		                                <button class=" " type="button" onclick="" title="팀 삭제하기" ><i class="flaticon-remove "></i></button>
		                            </div> -->
		                        </td>
		                        <?php }?>
	        				</tr>
	        			</tbody>
	                </table>
				</div>
			</li>
			<?php $i++;} ?>
		</ul>		
	</div>
	
	

	<div class="btn_area">
		<a class="btn btn01 fw-600" href="javascript:history.go(-1)">뒤 로</a>
		<a class="btn btn02 fw-600" href="<?php echo G5_SADM_URL?>/championship_add_step3.php">저 장</a>
	</div>

</div>
<!-- End page content -->



<?php
include_once(G5_SADM_PATH.'/_tail.php');
?>
