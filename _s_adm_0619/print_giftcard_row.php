<?php
include_once('./_common.php');

include_once(G5_PATH.'/head.sub.php');

$match_code		 = $_REQUEST['match_code'];
$team_code_1 	 = $_REQUEST['team_code_1'];
$team_code_2 	 = $_REQUEST['team_code_2'];
$team_code_3 	 = $_REQUEST['team_code_3'];
$rank			 = $_REQUEST['rank'];

$sql = "select * from match_data where code = '$match_code'";
$result = sql_query($sql);
$r = sql_fetch_array($result);

$team_field = "team_data";
		
if($division == "단체전"){
	$team_field = "team_event_data";	
}

$sql_team1 = "select * from $team_field where team_code = '$team_code_1'";
$team1_result = sql_query($sql_team1);
$team1 = sql_fetch_array($team1_result);

$sql_team2 = "select * from $team_field where team_code = '$team_code_2'";
$team2_result = sql_query($sql_team2);
$team2 = sql_fetch_array($team2_result);

$series_update = "update series_data set print = '1' where division = '$team1[division]' and series = '$team1[series]' and series_sub = '$team1[series_sub]' and match_code = '$match_code'";
sql_query($series_update);

?>

<style>
	body{height:410px;}
	table.table-scorecard{margin-bottom:40px;}
	table.table-scorecard tr td{vertical-align:middle;}
	table.table-scorecard thead th{padding: 20px 10px 12px;}
	table.table-scorecard tr td input{border:0px;text-align:center;font-size:70px;width:100%;}
	table.table-scorecard tr td .name-tit{font-size:18px;}
	.scorecard-sign span{border-bottom:1px solid #000;padding-bottom:5px;padding-right:200px;}
	.scorecard-title{font-size:25px;text-transform:uppercase;line-height:35px;}
	.block{padding:13px 0 10px 0}
	.col-lg-6{height:522.5px;}
</style>

<div class="col-lg-6 col-lg-offset-3">
    <!-- Bordered Table -->
    <div class="block" style="">
        <div class="block-header">
        	<h2 class="text-center">상품 수령증</h2>
        </div>
        <div class="block-content">
            <table class="table table-bordered table-scorecard">
                <tbody>
                    <tr>
                        <td class="text-center" width="200px">
                        	<div class="name-tit font-w600">대 회 명</div>
                        </td>
                        <td class="text-center">
                        	<div class="name-tit"><?=$r['wr_name']?></div>
                        </td>
                    </tr>
                    <tr>
                        <td class="text-center">
                        	<div class="name-tit font-w600">급 수</div>
                        </td>
                        <td class="text-center">
                        	<div class="name-tit"><?=$team1['division']?> <?=$team1['series']?><?=$team1['series_sub']?></div>
                        </td>
                    </tr>
                    <tr>
                        <td class="text-center">
                        	<div class="name-tit font-w600">순 위</div>
                        </td>
                        <td class="text-center">
                        	<div class="name-tit">1</div>
                        </td>
                    </tr>
                    <tr>
                        <td class="text-center">
                        	<div class="name-tit font-w600">클 럽 명</div>
                        </td>
                        <td class="text-center">
                        	<div class="name-tit"><?=$team1['club']?></div>
                        </td>
                    </tr>
                    <tr>
                        <td class="text-center">
                        	<div class="name-tit font-w600">성 함</div>
                        </td>
                        <td class="text-center">
                        	<div class="name-tit"><?=$team1['team_1_name']?> <?=$team1['team_2_name']?></div>
                        </td>
                    </tr>
				</tbody>
            </table>
            <div class="row">
            	<div class="col-md-12">
            		<div class="pull-left font-05 font-w600">
		            	<span>주관 : <?=$r['organizer']?></span>
		            </div>
		            <div class="pull-right scorecard-sign">
		            	<span>확인(서명)</span>
		            </div>		
            	</div>
            </div>
            <button id="print_button" name="print_button" class="btn btn-warning btn-block push-20-t push-20 hidden-print"  onclick="App.initHelper('print-page');"><i class="si si-printer"></i>상품수령증 인쇄</button>
        </div>
    </div>
    <!-- END Bordered Table -->
</div>
<div class="col-lg-6 col-lg-offset-3 ">
    <!-- Bordered Table -->
    <div class="block">
        <div class="block-header">
        	<h2 class="text-center">상품 수령증</h2>
        </div>
        <div class="block-content">
            <table class="table table-bordered table-scorecard">
                <tbody>
                    <tr>
                        <td class="text-center" width="200px">
                        	<div class="name-tit font-w600">대 회 명</div>
                        </td>
                        <td class="text-center">
                        	<div class="name-tit"><?=$r['wr_name']?></div>
                        </td>
                    </tr>
                    <tr>
                        <td class="text-center">
                        	<div class="name-tit font-w600">급 수</div>
                        </td>
                        <td class="text-center">
                        	<div class="name-tit"><?=$team2['division']?> <?=$team2['series']?><?=$team2['series_sub']?></div>
                        </td>
                    </tr>
                    <tr>
                        <td class="text-center">
                        	<div class="name-tit font-w600">순 위</div>
                        </td>
                        <td class="text-center">
                        	<div class="name-tit">2</div>
                        </td>
                    </tr>
                    <tr>
                        <td class="text-center">
                        	<div class="name-tit font-w600">클 럽 명</div>
                        </td>
                        <td class="text-center">
                        	<div class="name-tit"><?=$team2['club']?></div>
                        </td>
                    </tr>
                    <tr>
                        <td class="text-center">
                        	<div class="name-tit font-w600">성 함</div>
                        </td>
                        <td class="text-center">
                        	<div class="name-tit"><?=$team2['team_1_name']?> <?=$team2['team_2_name']?></div>
                        </td>
                    </tr>
				</tbody>
            </table>
            <div class="row">
            	<div class="col-md-12">
            		<div class="pull-left font-05 font-w600">
		            	<span>주관 : <?=$r['organizer']?></span>
		            </div>
		            <div class="pull-right scorecard-sign">
		            	<span>확인(서명)</span>
		            </div>		
            	</div>
            </div>
            <button id="print_button" name="print_button" class="btn btn-warning btn-block push-20-t push-20 hidden-print"  onclick="App.initHelper('print-page');"><i class="si si-printer"></i>상품수령증 인쇄</button>
        </div>
    </div>
    <!-- END Bordered Table -->
</div>

<script>
$(window).load(function() {
	$("#print_button").click();
});
</script>