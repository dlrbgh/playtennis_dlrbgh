<?php
$menu_cate2 ='2';
$menu_cate3 ='2';
include_once('./_common.php');
include_once(G5_SADM_PATH.'/_head.php');

?>
<script src="<?php echo G5_SADM_URL?>/assets/js/autosave"></script>
<!-- sub hd_section-->
	<div class="tit_area">
		<div class="tit_subject">대회관리</div>
		<div class="tit">급수별 편성표&nbsp;</div>
		<div class="r-btn-area">
			<ul>
				<li><a href="" class="btn_default">저장하기</a></li>
				<li><a href="" class="btn_default">불러오기</a></li>
			</ul>
		</div>
	</div>
<!--//sub_hd_area -->

<!-- !PAGE CONTENT! -->
<div>

	<div class="bo_cate_area">
	    <!-- 게시판 카테고리 시작 { -->
	    <nav id="tab_group2">
	        <h2>급수별 편성</h2>
	        <ul id="bo_cate_ul">
	            <li><a href="" id="bo_cate_on">전체</a></li>
	        	<?php
                	$sql_s = "select * from match_gym_data inner join gym_data where match_gym_data.match_id='$code' and gym_data.wr_id = match_gym_data.gym_id group by gym_id order by match_gym_data.wr_id";
					$result = sql_query($sql_s);
					while($r = sql_fetch_array($result)){
				?>
	            <li><a href="./championship_add_step5.php?code=<?php echo $code;?>&gym_id=<?php echo $r['gym_id'];?>&application_period=<?php echo $application_period;?>" id="<?php if($r['gym_id'] == $gym_id){ echo "bo_cate_on"; } ?>"><?php echo $r['gym_name'];?></a></li>
			    <?php } ?>
				<!--
	            <li><a href="">남성</a></li>
	            <li><a href="">여성</a></li>
	            <li><a href="">혼합</a></li>
	            -->
	    </nav>
	    <!-- } 게시판 카테고리 끝 -->
	</div>

	<div class="tbl_style01 tbl_borderd">
		<table>
			<thead>
				<tr>
					<th width="50">No</th>
					<th>그룹</th>
					<th>팀수</th>
					<th>배정현황</th>
					<th></th>
				</tr>
			</thead>
			<tbody class="text-center">
				<?php
                	$i = 0;
                	$sql = "select * from series_data where match_code='$code' GROUP BY division,series,series_sub";
					$gourp_result = sql_query($sql);
				?>
				<?php while($group = sql_fetch_array($gourp_result)){?>
					 <?php
					 	$team_field = "team_data";

						if($group['division'] == "단체전"){
							$team_field = "team_event_data";
						}


						$sql_team = "select count(wr_id) as cnt from $team_field where match_code='$code' and division='$group[division]' and series ='$group[series]' and series_sub ='$series_sub'";
						$team_result = sql_query($sql_team);
						$r = sql_fetch_array($team_result);

	        $sql_teams = "select count(wr_id) as cnt from $team_field where match_code='$code' and division='$group[division]' and series ='$group[series]' and series_sub ='$group[series_sub]' and group_assign	= '0'";
						$team_results = sql_query($sql_teams);
						$rs = sql_fetch_array($team_results);
						print $sql_teams;

						$sql_team = "select count(wr_id) as cnt from $team_field where match_code='$code' and division='$group[division]' and series ='$group[series]' and series_sub ='$group[series_sub]'";
						$team_result = sql_query($sql_team);
						$r = sql_fetch_array($team_result);

	                ?>
				<tr>
					<td>1</td>
					<td>
						<a href=""><?php echo $group['division'];?> - <?php echo $group['series'];?> - <?php echo $group['series_sub'];?>(<?=$r[cnt];?>팀)</a>
					</td>

					<td>
						<?=$r[cnt];?>팀
					</td>
					<td>
						배정(<?=$r[cnt];?>팀) / <font class="color5">미배정(<?=$rs[cnt];?>팀)</font>
					</td>
					<td>
						<a href="championship_add_step5_view.php?division=<?php echo $group['division'];?>&series=<?php echo $group['series'];?>&series_sub=<?php echo $group['series_sub'];?>&wr_id=<?php echo $code;?>" class="btn_default">보기</a>
					</td>
				</tr>
				<?php } ?>
				<!-- <tr>
					<td>1</td>
					<td>
						<a href="">개인전 - 여자 - 단식(30팀)</a>
					</td>
					<td>챌린저</td>

					<td>
						30팀
					</td>
					<td>
						배정(29팀) / <font class="color5">미배정(1팀)</font>
					</td>
					<td>
						<a href="" class="btn_default">보기</a>
					</td>
				</tr> -->
			</tbody>
		</table>
	</div>

	<?php
		$match_sql = "select * from match_gym_data where match_id='$code' order by application_period";
		$match_result = sql_query($match_sql);
		$match = sql_fetch_array($match_result);
		$url = "&gym_id=$match[gym_id]&application_period=$match[application_period]";
	?>
	<div class="btn_area">
		<a class="btn btn01 fw-600" href="javascript:history.go(-1)">뒤 로</a>
		<a class="btn btn01 fw-600" href="<?php echo G5_SADM_URL?>/timetable_assignment.php?code=<?php echo $code.$url;?>">다 음(skip)</a>
		<a class="btn btn01 fw-600" href="<?php echo G5_SADM_URL?>/step5_seires.php?code=<?=$code;?>">대진표 생성</a>
	</div>

</div>
<!-- End page content -->



<?php
include_once(G5_SADM_PATH.'/_tail.php');
?>
