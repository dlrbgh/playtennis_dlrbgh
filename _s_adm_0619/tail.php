<?php
if (!defined('_GNUBOARD_')) exit; // 개별 페이지 접근 불가

if(defined('G5_THEME_PATH')) {
    require_once(G5_THEME_PATH.'/tail.php');
    return;
}

if (G5_IS_MOBILE) {
    include_once(G5_MOBILE_PATH.'/tail.php');
    return;
}
?>

    </div>
		<?php
		    if(!defined('_INDEX_')) { // sub에서만 실행
			include_once(G5_INC_PATH."/sub_tail.inc");
		}?>    
</div>

<!-- } 콘텐츠 끝 -->

<hr>

<?php
    if(defined('_INDEX_')) { // index에서만 실행
    ?>
    
<!-- main 하단 시작 { -->
<div id="ft">
    <div>
    	<!-- <span id="ft_tit">서울사무실.</span>
        <p>
        	<span><b>주소</b> (06053) 서울시 강남구 언주로 133길 20</span><br/>
            <span><b>전화</b> 02)3441-3100 </span><span><b>팩스</b> 02)3443-5201</span>
            Copyright &copy; 2001-2013 <?php echo $default['de_admin_company_name']; ?>. All Rights Reserved.
        </p> -->
        <!--<a href="#" id="ft_totop">상단으로</a>-->
        <!-- <a href="<?php echo G5_URL; ?>/" id="ft_logo"><img src="<?php echo G5_IMG_URL; ?>/logo.png" alt="처음으로"></a> -->
    </div>
    
</div>
<?php }
    ?>
<?php
if ($config['cf_analytics']) {
    echo $config['cf_analytics'];
}
?>

<!-- } main 하단 끝 -->



<?php
include_once(G5_SADM_PATH."/tail.sub.php");
?>
