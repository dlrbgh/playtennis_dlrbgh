<?php
//그룹 생성
function create_group($code,$division,$series,$series_sub,$i){
	$team = createRandomPassword();
	$sql = "insert into group_data set
			match_code	= '$code',
			code = '$team',
			num = '$i',
			division 	= '$division',
			series 	= '$series',
			series_sub 	= '$series_sub',
			tournament = 'L',
			wr_datetime = '".G5_TIME_YMDHIS."',
			mb_id = '$member[mb_id]',
			wr_ip = '{$_SERVER['REMOTE_ADDR']}',
			wr_last = '".G5_TIME_YMDHIS."'";
			print $sql;
			print "<br>\n";
			sql_query($sql);
}
function create_group_auto_num($code,$division,$series,$series_sub){
	$team = createRandomPassword();
	$sql = "insert into group_data set
			match_code	= '$code',
			code = '$team',
			num =
			(select ifnull(num , 0) from (	select max(num) +1 as num
				from group_data
				where match_code = '{$code}'
				and division='{$division}'
				and series = '{$series}'
				and series_sub = '{$series_sub}') a
			),
			gym_code = (select * from (	select gym_code
				from group_data
				where match_code = '{$code}'
				and division='{$division}'
				and series = '{$series}'
				and series_sub = '{$series_sub}') a
				group by gym_code limit 0, 1
			)
			,division 	= '$division',
			series 	= '$series',
			series_sub 	= '$series_sub',
			tournament = 'L',
			wr_datetime = '".G5_TIME_YMDHIS."',
			mb_id = '$member[mb_id]',
			wr_ip = '{$_SERVER['REMOTE_ADDR']}',
			wr_last = '".G5_TIME_YMDHIS."'";

			// print $sql;
			sql_query($sql);
}

//급수 정보 생성
function create_series($code,$division,$series,$series_sub,$color){
	$sql = "insert into series_data set
			division 	= '$division',
			series 	= '$series',
			series_sub 	= '$series_sub',
			match_code	= '$code',
			color = '$color',
			wr_datetime = '".G5_TIME_YMDHIS."',
			mb_id = '$member[mb_id]',
			wr_ip = '{$_SERVER['REMOTE_ADDR']}',
			wr_last = '".G5_TIME_YMDHIS."'";
	sql_query($sql);
}
//경기 생성
function create_game(
		$code,
		$group,
		$tournament, /* tournament 여부 L, T, C*/
		$tournament_count, /* tournament 강(round) 수*/
		$tournament_num, /* 같은 tournament  round 내의 정렬값*/
		$NO_Full_League,
		$tournament_array_num,
		$assigned_group_name1 = '', /* 배정받은 조 1 이름*/
		$assigned_group_name2 = '' /* 배정받은 조 2 이름*/
	){
	$team = createRandomPassword();
	$sql = "insert into game_score_data set
			match_code	= '$code',
			group_code = '$group[code]',
			code = '$team',
			term = '$NO_Full_League',
			tournament	= '$tournament',
			tournament_count	= '$tournament_count',
			tournament_num = '$tournament_num',
			tournament_array_num = '$tournament_array_num',
			Full_League = '$NO_Full_League',
			division 	= '$group[division]',
			series	 	= '$group[series]',
			series_sub	= '$group[series_sub]',
			wr_datetime = '".G5_TIME_YMDHIS."',
			mb_id = '$member[mb_id]',
			wr_last = '".G5_TIME_YMDHIS."',
			assigned_group_name1 = '{$assigned_group_name1}',
			assigned_group_name2 = '{$assigned_group_name2}'
			";
	// echo $sql."<br>";
	sql_query($sql);
	//단체전일 경우 동일한 경기를 2개 더 생성한다.
	if($group['division'] == "단체전"){
		$team = createRandomPassword();
		$sql = "insert into game_score_data set
				match_code	= '$code',
				group_code = '$group[code]',
				code = '$team',
				term = '$NO_Full_League',
				tournament	= '$tournament',
				tournament_count	= '$tournament_count',
				tournament_num = '$tournament_num',
				tournament_array_num = '$tournament_array_num',
				Full_League = '$NO_Full_League',
				division 	= '$group[division]',
				series	 	= '$group[series]',
				series_sub	= '$group[series_sub]',
				wr_datetime = '".G5_TIME_YMDHIS."',
				mb_id = '$member[mb_id]',
				wr_last = '".G5_TIME_YMDHIS."',
				assigned_group_name1 = '{$assigned_group_name1}',
				assigned_group_name2 = '{$assigned_group_name2}'
				";
		sql_query($sql);
		$team = createRandomPassword();
		$sql = "insert into game_score_data set
				match_code	= '$code',
				group_code = '$group[code]',
				code = '$team',
				term = '$NO_Full_League',
				tournament	= '$tournament',
				tournament_count	= '$tournament_count',
				tournament_num = '$tournament_num',
				tournament_array_num = '$tournament_array_num',
				Full_League = '$NO_Full_League',
				division 	= '$group[division]',
				series	 	= '$group[series]',
				series_sub	= '$group[series_sub]',
				wr_datetime = '".G5_TIME_YMDHIS."',
				mb_id = '$member[mb_id]',
				wr_last = '".G5_TIME_YMDHIS."',
				assigned_group_name1 = '{$assigned_group_name1}',
				assigned_group_name2 = '{$assigned_group_name2}'";
		sql_query($sql);
	}
}
//리그 경기 생성
function create_league_game($code,$group,$team_1,$team_2,$tournament,$tournament_count,$tournament_num,$NO_Full_League,$term){
	if($group[division] == "개인전"){
		$team = createRandomPassword();
		$sql = "insert into game_score_data set
			team_1_code = '$team_1',
			team_2_code = '$team_2',
			match_code	= '$code',
			group_code = '$group[code]',
			code = '$team',
			term = '$term',
			tournament	= 'L',
			tournament_count = '0',
			tournament_num = '3',
			Full_League = '$NO_Full_League',
			division 	= '$group[division]',
			series	 	= '$group[series]',
			series_sub	= '$group[series_sub]',
			wr_datetime = '".G5_TIME_YMDHIS."',
			mb_id = '$member[mb_id]',
			wr_last = '".G5_TIME_YMDHIS."'";
			// print $sql;
		sql_query($sql);
	}

	//단체전일 경우 동일한 팀이 배정되지 않는 경기를 3경기 생성한다.
	if($group[division] == "단체전"){
		$team = createRandomPassword();
		$sql = "insert into game_score_data set
			team_1_code = '$team_1',
			team_2_code = '$team_2',
			match_code	= '$code',
			group_code = '$group[code]',
			code = '$team',
			term = '$term',
			tournament	= 'L',
			tournament_count = '0',
			tournament_num = '3',
			Full_League = '$NO_Full_League',
			division 	= '$group[division]',
			series	 	= '$group[series]',
			series_sub	= '$group[series_sub]',
			wr_datetime = '".G5_TIME_YMDHIS."',
			mb_id = '$member[mb_id]',
			wr_last = '".G5_TIME_YMDHIS."'";
		sql_query($sql);
		$team = createRandomPassword();
		$sql = "insert into game_score_data set
			team_1_code = '$team_1',
			team_2_code = '$team_2',
			match_code	= '$code',
			group_code = '$group[code]',
			code = '$team',
			term = '$term',
			tournament	= 'L',
			tournament_count = '0',
			tournament_num = '3',
			Full_League = '$NO_Full_League',
			division 	= '$group[division]',
			series	 	= '$group[series]',
			series_sub	= '$group[series_sub]',
			wr_datetime = '".G5_TIME_YMDHIS."',
			mb_id = '$member[mb_id]',
			wr_last = '".G5_TIME_YMDHIS."'";
		sql_query($sql);
		$team = createRandomPassword();
		$sql = "insert into game_score_data set
			team_1_code = '$team_1',
			team_2_code = '$team_2',
			match_code	= '$code',
			group_code = '$group[code]',
			code = '$team',
			term = '$term',
			tournament	= 'L',
			tournament_count = '0',
			tournament_num = '3',
			Full_League = '$NO_Full_League',
			division 	= '$group[division]',
			series	 	= '$group[series]',
			series_sub	= '$group[series_sub]',
			wr_datetime = '".G5_TIME_YMDHIS."',
			mb_id = '$member[mb_id]',
			wr_last = '".G5_TIME_YMDHIS."'";
		sql_query($sql);
	}
}
//ios_push
function ios_push($deviceToken,$message){

	// 개발용
	// $apnsHost = 'gateway.sandbox.push.apple.com';
	// $apnsCert = 'apns-dev.pem';

	// 실서비스용
	$apnsHost = 'gateway.push.apple.com';
	$apnsCert = 'apns.pem';

	$apnsPort = 2195;

	$payload = array('aps' => array('alert' => $message, 'badge' => 0, 'sound' => 'default'));
	$payload = json_encode($payload);

	$streamContext = stream_context_create();
	stream_context_set_option($streamContext, 'ssl', 'local_cert', $apnsCert);

	$apns = stream_socket_client('ssl://'.$apnsHost.':'.$apnsPort, $error, $errorString, 2, STREAM_CLIENT_CONNECT, $streamContext);

	if($apns)
	{
	  $apnsMessage = chr(0).chr(0).chr(32).pack('H*', str_replace(' ', '', $deviceToken)).chr(0).chr(strlen($payload)).$payload;
	  fwrite($apns, $apnsMessage);
	  fclose($apns);
	}
}
//안드로이드 푸쉬 GCM대응버전 Firebase기반으로 다시 만들어야함
function android_push($andorid_array,$message){
	$title = "콕선생 관심경기 알림";

	// URL to POST to
	$gcm_url = 'https://android.googleapis.com/gcm/send';

	// data to be posted
	$fields = array(
		'registration_ids'  => $andorid_array,
		'data'              => array( "message" => $message,"title" => $title ),
	);

	// headers for the request
	$headers = array(
	    'Authorization:key=AIzaSyBSwFTUalt1RBrfITM0MbyQ5Ac3Q21YVGc',
	    'Content-Type: application/json'
	);

	$curl_handle = curl_init();

	// set CURL optionsa
	curl_setopt($curl_handle, CURLOPT_URL, 'https://android.googleapis.com/gcm/send');
	curl_setopt($curl_handle, CURLOPT_HTTPHEADER,  $headers);
	curl_setopt($curl_handle, CURLOPT_POST,    true);
	curl_setopt($curl_handle, CURLOPT_RETURNTRANSFER, true);
	curl_setopt($curl_handle, CURLOPT_SSL_VERIFYPEER, false);
	curl_setopt($curl_handle, CURLOPT_POSTFIELDS,json_encode($fields));
	$response = curl_exec($curl_handle);

	curl_close($curl_handle);

}
//토너먼트 내에 1,2팀 경기 입력
function insert_two_team($team_1_code,$team_2_code,$wr_ids,$match_code){
	$sql = " update game_score_data
		    set
		         team_1_code = '$team_1_code',
		         team_2_code = '$team_2_code'

		    where wr_id = '$wr_ids' and match_code = '$match_code'";
	sql_query($sql);
}
//토너먼트 내에 1팀 경기 입력
function insert_team_1(){
	$sql = " update game_score_data
            set
                 team_1_code = '$team_code'
            where wr_id = '$wr_ids' and match_code = '$match_code'";

	sql_query($sql);
}
//토너먼트 내에 2팀 경기 입력
function insert_team_2($team_code,$wr_ids,$match_code){
	$sql = " update game_score_data
            set
                 team_2_code = '$team_code'
            where wr_id = '$wr_ids' and match_code = '$match_code'";

	sql_query($sql);
}
//토너먼트 승자 검사(단체전 포함)
function team_win_code($team){
	if($team['division'] == "단체전"){
		$sql = "select * from game_score_data where match_code = '$team[match_code]' and tournament_array_num = '$team[tournament_array_num]' and division = '$team[division]' and series = '$team[series]' and series_sub = '$team[series_sub]'";
		$result = sql_query($sql);
		$chk1 = 0;
		$chk2 = 0;
		while($team = sql_fetch_array($result)){
			if($team['team_1_score'] > $team['team_2_score']){
				$chk1++;
				$team_2_code = $team['team_1_code'];
			}else{
				$chk2++;
				$team_1_code = $team['team_2_code'];
			}
		}
		if($chk1 > $chk2){
			return $team_2_code;
		}
		if($chk2 > $chk1){
			return $team_1_code;
		}
	}else{
		if($team['team_1_score'] > $team['team_2_score']){
			$team_1_code = $team['team_1_code'];
		}else{
			$team_1_code = $team['team_2_code'];
		}
		return $team_1_code;
	}

}
//종료된 경기 숫자 검사
function get_tourament_N_count($tournament_count,$match_code,$division,$series,$series_sub){
	$sql = "select count(wr_id) as cnt from game_score_data where tournament = 'T' and tournament_count = '$tournament_count' and end_game = 'N' and match_code = '$match_code' and division = '$division' and series = '$series' and series_sub = '$series_sub'";
	$result = sql_query($sql);
	$r = sql_fetch_array($result);
	$cnt = $r['cnt'];
	return $cnt;
}
//종료되지 않는 숫자 검사
function get_tourament_Y_count($tournament_count,$match_code,$division,$series,$series_sub){
	$sql = "select count(wr_id) as cnt from game_score_data where tournament = 'T' and tournament_count = '$tournament_count' and end_game = 'N' and match_code = '$match_code' and division = '$division' and series = '$series' and series_sub = '$series_sub'";
	$result = sql_query($sql);
	$r = sql_fetch_array($result);
	$cnt = $r['cnt'];
	return $cnt;
}

//결승전 세팅
function set_championship($match_code,$division,$series,$series_sub){
	$gravity = 1;
	if($division == "단체전")
		$gravity = 3;

	$sql = "select count(wr_id) as cnt from game_score_data where tournament = 'T' and tournament_count = '2' and end_game = 'Y' and match_code = '$match_code' and division = '$division' and series = '$series' and series_sub = '$series_sub'";
	$result = sql_query($sql);
	$r = sql_fetch_array($result);
	if($r['cnt'] == 2*$gravity){
		$sql = "select count(wr_id) as cnt from game_score_data where tournament = 'C' and tournament_count = '0' and end_game = 'N' and match_code = '$match_code' and division = '$division' and series = '$series' and series_sub = '$series_sub'";
		$result = sql_query($sql);
		$r = sql_fetch_array($result);
		if($r['cnt'] == 1*$gravity){
			$tournament_create = 'C';
			$group_sql = "select * from game_score_data where tournament = 'T' and tournament_count = '2' and division = '$division' and series = '$series' and series_sub = '$series_sub' and match_code = '$match_code'";
			$result = sql_query($group_sql);
			//검색된 순위대로 그룹 랭크를 매긴다.
			$cnt = 0;
			$team_array = array();
			$team_1_code = 0;
			$team_2_code = 0;
			while($team = sql_fetch_array($result)){

				if($cnt == 0){
					$team_1_code = team_win_code($team);
				}else{
					$team_2_code = team_win_code($team);
				}
				$cnt++;
			}
			$sqls = "select * from game_score_data where tournament = 'C' and tournament_count = '0' and team_1_score = '' and team_2_score = '' and match_code = '$match_code' and division = '$division' and series = '$series' and series_sub = '$series_sub'";
			$result = sql_query($sqls);
			$r = sql_fetch_array($result);
			$tournament_array_num = $r['tournament_array_num'];
			if($division == "단체전"){
				$sql = " update game_score_data
	            set
	                 team_1_code = '$team_1_code',
	                 team_2_code = '$team_2_code'
	            where tournament_array_num = '$tournament_array_num' and match_code = '$match_code'";
				sql_query($sql);
			}else{
				$wr_ids  = $r['wr_id'];
				$sql = " update game_score_data
	            set
	                 team_1_code = '$team_1_code',
	                 team_2_code = '$team_2_code'
	            where wr_id = '$wr_ids' and match_code = '$match_code'";
				sql_query($sql);
			}
			//insert_two_team($team_1_code,$team_2_code,$r[wr_id],$match_code);
		}
	}
	echo $tournament_create;
}

//그룹내 랭크 상태를 불러오기 위한 함수
function get_team_rank($team_field,$match_code,$division,$series,$series_sub){
	$group_sql = "select * from $team_field where match_code = '$match_code' and division = '$division' and series = '$series' and series_sub = '$series_sub' and (group_rank = '1' or group_rank = '2') ORDER BY group_rank,team_league_point desc ,winning_rate desc	,gains_losses_point desc, team_total_match_point DESC";
	$result = sql_query($group_sql);
	//검색된 순위대로 그룹 랭크를 매긴다.
	$cnt = 0;
	$team_array = array();
	while($team = sql_fetch_array($result)){
		$team_array[$cnt] = $team;
		$cnt++;
	}
	return $team_array;
}

//dlrbgh 0608
// 배정되지않은 팀 숫자
function get_not_assigned_team_count($c, $d, $s, $ss){
	$targetTable  = ($d == '단체전' ? 'team_event_data' : 'team_data');
	$sql = "select count(*) as count
	from $targetTable
	where match_code = '{$c}'
	and division = '{$d}'
	and series = '{$s}'
	and series_sub = '{$ss}'
	and group_assign = 0";
	$not_assigned_count = sql_fetch($sql);
	// print $sql;
	return $not_assigned_count['count'];
}

//미배정된 팀을 가져온
function get_not_assigned_team($group_id, $d){
	$targetTable  = ($d == '단체전' ? 'team_event_data' : 'team_data');
	$sql = "select *
	from $targetTable
	where match_code = (select match_code from group_data where wr_id = {$group_id})
	and division = (select division from group_data where wr_id = {$group_id})
	and series = (select series from group_data where wr_id = {$group_id})
	and series_sub = (select series_sub from group_data where wr_id = {$group_id})
	and group_assign = 0";
	// print $sql;
	$not_assigned_list = sql_query($sql);
	$return;
	while($row = sql_fetch_array($not_assigned_list)){
		$return['data'][] = $row;
	}
	return $return;
}

//division series, series_sub code로 미배정된 팀을 갖고온다 
function get_not_assigned_team_by_dsss($d,$s, $ss, $c){
	$targetTable  = ($d == '단체전' ? 'team_event_data' : 'team_data');
	$sql = "select *
	from $targetTable
	where match_code = '{$c}'
	and division = '{$d}'
	and series = '{$s}'
	and series_sub = '{$ss}'
	and group_assign = 0";
	// print $sql;
	$not_assigned_list = sql_query($sql);
	$return;
	while($row = sql_fetch_array($not_assigned_list)){
		$return['data'][] = $row;
	}
	return $return;
}

// 그룹에 개인전을 집어 넣음 
function assign_team_to_group($groupid, $team_codes){

//배정뒤의 변화를 알기위해
	$group_info_before_assign = sql_fetch("select * from group_data where wr_id = {$groupid}");

	$start_index = get_empty_space($groupid);
	if($start_index <1){
		return array("status"=>"failure", "code"=>-1);
	}
	if($start_index > 3){
		return array("status"=>"failure", "code"=>0);
	}
	// print $start_index ." ".count($team_codes)."\n";
	if(4 - $start_index < count($team_codes)){
		return array("status"=>"failure", "code"=>1);
	}
	foreach($team_codes as $key => $code){
		$sql="update group_data set team_{$start_index} = '{$code}' where wr_id = {$groupid}";
		sql_query($sql);
		$team_assign_true = "update team_data set group_assign = 1 where team_code = '{$code}'";
		sql_query($team_assign_true);
		$start_index ++;
	}

	$group_info_after_assign = sql_fetch("select * from group_data where wr_id = {$groupid}");

	make_league_game_by_changing_group_team($group_info_before_assign, $group_info_after_assign);

	return array("status"=>"OK");
}
//그룹에 단체전을 집어넣는
function assign_team_event_to_group($groupid, $team_codes){

$group_info_before_assign = sql_fetch("select * from group_data where wr_id = {$groupid}");

	$start_index = get_empty_space($groupid);
	if($start_index <1){
		return array("status"=>"failure", "code"=>-1);
	}
	if($start_index > 3){
		return array("status"=>"failure", "code"=>0);
	}
	// print $start_index ." ".count($team_codes)."\n";
	if(4 - $start_index < count($team_codes)){
		return array("status"=>"failure", "code"=>1);
	}
	foreach($team_codes as $key => $code){
		$sql="update group_data set team_{$start_index} = '{$code}' where wr_id = {$groupid}";
		sql_query($sql);
		$team_assign_true = "update team_event_data set group_assign = 1 where team_code = '{$code}'";
		// print $team_assign_true;
		sql_query($team_assign_true);
		$start_index ++;
	}
	return array("status"=>"OK");
	$group_info_after_assign = sql_fetch("select * from group_data where wr_id = {$groupid}");

	make_league_game_by_changing_group_team_event($group_info_before_assign, $group_info_after_assign);
}


//옆으로 늘어선 컬럼중에 빈칸인 것을 갖고온
function get_empty_space($groupid){
	$sql = "select team_1, team_2, team_3, team_4, team_5 from group_data where wr_id = {$groupid}";
	$return = sql_fetch($sql);
	// print $sql."\n";

	for($i = 1; $i < 6; $i++){
		if($return['team_'.$i] == ''){
			return $i;
		}
	}
	return -1;
}
// 그룹 삭제
function delete_group_by_id($group_id){
	//하나도 없으면 새로 생성될 그룹에 참조할 그룹이 없기때문에
	//지웠을때 0이되면 지우지않는걸로
	//어차피 free할때 데이터를 다 지웠으므로 삭제만 막으면된다/
	$group_count_sql = "select count(*) as cnt from group_data where
	match_code =  (select match_code from group_data where wr_id = {$group_id})
	and division = (select division from group_data where wr_id = {$group_id})
	and series = (select series from group_data where wr_id = {$group_id})
	and series_sub = (select series_sub from group_data where wr_id = {$group_id})";

	$group_count = sql_fetch($group_count_sql);
	// print $group_count_sql;
	// print_r($group_count);
	if($group_count['cnt'] > 1){
		$sql = "delete from group_data where wr_id = {$group_id}";
		// print ">>>>";
	}else{
		// print '삭제안해';
		$sql ="update group_data set team_1 = '',team_2 = '',team_3 = '',team_4 = '',team_5 = ''
		where wr_id = {$group_id}";
	}
	// print $sql;
	sql_query($sql);
}

//그룹에 배정된 팀들을 제거한
function free_assigned_team_group( $group_id, $d){


	$group_info_before_assign = sql_fetch("select * from group_data where wr_id = {$groupid}");

	$targetTable  = ($d == '단체전' ? 'team_event_data' : 'team_data');

	$group_info_sql = "select match_code as c, division as d, series as s, series_sub as ss
	,team_1, team_2, team_3, team_4, team_5
	from group_data where wr_id = '{$group_id}'";
	$group_info = sql_fetch($group_info_sql);

	$team_code_list;



	if($group_info['team_1'] != '') {
		if($team_code_list != ''){
			$team_code_list .= ", ";
		}
		$team_code_list .= "'{$group_info['team_1']}'";
	}if($group_info['team_2'] != '') {
		if($team_code_list != ''){
			$team_code_list .= ", ";
		}
		$team_code_list .= "'{$group_info['team_2']}'";
	}if($group_info['team_3'] != '') {
		if($team_code_list != ''){
			$team_code_list .= ", ";
		}
		$team_code_list .= "'{$group_info['team_3']}'";
	}if($group_info['team_4'] != '') {
		if($team_code_list != ''){
			$team_code_list .= ", ";
		}
		$team_code_list .= "'{$group_info['team_4']}'";
	}if($group_info['team_5'] != '') {
		if($team_code_list != ''){
			$team_code_list .= ", ";
		}
		$team_code_list .= "'{$group_info['team_5']}'";
	}

	if($team_code_list != ""){

		$free = "update {$targetTable} set group_assign = 0 where
		team_code in ({$team_code_list})";

		sql_query($free);
	// print $free;
	}

	$group_info_after_assign = sql_fetch("select * from group_data where wr_id = {$groupid}");
	if($d == '단체전'){
		make_league_game_by_changing_group_team_event($group_info_before_assign, $group_info_after_assign);
	}else{
		make_league_game_by_changing_group_team($group_info_before_assign, $group_info_after_assign);
	}
}

//팀 정보 수
function update_team_data($team_code, $area1s, $area2s, $clubs, $names){

	// print_r($names);

	//뒤부터빼내므로
	$area1 = array_pop($area1s);
	$area2 = array_pop($area2s);
	$club = array_pop($clubs);
	$name = array_pop($names);
	$sql = "update team_data set team_2_area = '{$area2}' , team_2_club = '{$club}', team_2_name = '{$name}' where team_code = '{$team_code}'";
	// print $sql;
	// print_r($names);
	sql_query($sql, true);
	$area1 = array_pop($area1s);
	$area2 = array_pop($area2s);
	$club = array_pop($clubs);
	$name = array_pop($names);

$sql = "update team_data set area_1 = '{$area1}', area_2 = '{$area2}'
, club = '{$club}', team_1_name = '{$name}' where team_code = '{$team_code}'";
// print $sql;
	sql_query($sql, true);

	// while($area1s){
	// 	$area1 = array_pop($area1s);
	// 	$area2 = array_pop($area2s);
	// 	$club = array_pop($clubs);
	// 	$name = array_pop($names);
	// 	// $sql = "update team_data set area_1 = '{$area1}' , area_2 = '{$area_2}', club = '{$club}', team_1_name = "
	// }
}
// 단체전수
function update_team_event_data($team_code, $area1, $area2, $club, $names){

	$name_clause = "";
	// print_r( $names);
	for($i = 0; $i < count($names); $i++){

		$tmp = $i +1;
		$name_clause .=", team_{$tmp}_name = '{$names[$i]}'";
	}

		$sql = "update team_event_data set
						area_1 = '{$area1}'
						,area_2 = '{$area2}'
						,club = '{$club}'
						{$name_clause}
		where team_code = '{$team_code}'";
		sql_query($sql, true);
}
// 팀을 미배정상태로 만든다(개인전)
function make_not_assign_team($groupid, $team_code){

$group_info_before_assign = sql_fetch("select * from group_data where wr_id = {$groupid}");

// print_r($group_info_before_assign);

	$sql ="select team_1, team_2, team_3, team_4, team_5 from group_data where wr_id = {$groupid}";
	$group = sql_fetch($sql);


	$update_group ;
	foreach($group as $team => $code){
		if($code != $team_code){
			$update_group[] = $code;
		}
	}
	$count = 1;
	for($i = 0; $i < 5; $i++){
		if($i != 0){ $team_clause .=", ";}
		$team_clause .= " team_{$count} = '{$update_group[$i]}'";
		$count ++;
	}

	$group_update = "update group_data set {$team_clause} where wr_id = {$groupid}";
	sql_query($group_update);
	$sql = "update team_data set group_assign = 0 where team_code = '{$team_code}'";
	sql_query($sql, true);

		$group_info_after_assign = sql_fetch("select * from group_data where wr_id = {$groupid}");

		make_league_game_by_changing_group_team($group_info_before_assign, $group_info_after_assign);
}
//그룹에 들어간 게임을 삭제(단체전) 
function make_not_assign_team_event($groupid, $team_code){

$group_info_before_assign = sql_fetch("select * from group_data where wr_id = {$groupid}");

	$sql ="select team_1, team_2, team_3, team_4, team_5 from group_data where wr_id = {$groupid}";
	$group = sql_fetch($sql);

	$update_group ;
	foreach($group as $team => $code){
		if($code != $team_code){
			$update_group[] = $code;
		}
	}
	$count = 1;
	for($i = 0; $i < 5; $i++){
		if($i != 0){ $team_clause .=", ";}
		$team_clause .= " team_{$count} = '{$update_group[$i]}'";
		$count ++;
	}

	$group_update = "update group_data set {$team_clause} where wr_id = {$groupid}";
	sql_query($group_update);
	$sql = "update team_event_data set group_assign = 0 where team_code = '{$team_code}'";
	sql_query($sql, true);


	$group_info_after_assign = sql_fetch("select * from group_data where wr_id = {$groupid}");

	make_league_game_by_changing_group_team_event($group_info_before_assign, $group_info_after_assign);
}
//팀을그룹에 추
function insert_team_data($code,$d, $s, $ss, $area1s, $area2s, $clubs, $names, $grades){

	//뒤부터빼내므로
	$area1_2 = array_pop($area1s);
	$area2_2 = array_pop($area2s);
	$club_2 = array_pop($clubs);
	$name_2 = array_pop($names);
	$grades_2 = array_pop($grades);

	$area1_1 = array_pop($area1s);
	$area2_1 = array_pop($area2s);
	$club_1 = array_pop($clubs);
	$name_1 = array_pop($names);
	$grades_1 = array_pop($grades);

	$team = createRandomPassword();
	$sql = "insert into team_data set
			 division 		='{$d}',
			 series			='{$s}',
			 series_sub		='{$ss}',
			 area_1			='{$area1_1}',
			 area_2			='{$area2_1}',
			 club				='{$club_1}',
			 team_1_name		='{$name_1}',
			 team_1_grade		='{$grades_1}',
			 team_2_area 		='{$area2_2}',
			 team_2_club    	='{$club_2}',
			 team_2_name		='{$name_2}',
			 team_2_grade		='{$grades_2}',
			 team_code		='{$team}',
			 match_code		='{$code}'";
			 sql_query($sql);
			//  print $sql;
}

//단체전팀 추
function insert_team_event_data($code,$d, $s, $ss, $area1, $area2, $club, $names, $grades){
	$team = createRandomPassword();
	$sql = "insert into team_event_data set
			 division 		='{$d}',
			 series			='{$s}',
			 series_sub		='{$ss}',
			 area_1			='{$area1}',
			 area_2			='{$area2}',
			 club				='{$club}',
			 team_1_name		='{$names[0]}',
			 team_1_grade		='{$grades[0]}',
			 team_2_area 		='$names[1]',
			 team_2_club    	='$value10',
			 team_2_name		='$names[1]',
			 team_2_grade		='{$grades[1]}',
			 team_3_name		='$names[2]',
			 team_3_grade		='{$grades[2]}',
			 team_4_name		='$names[3]',
			 team_4_grade		='{$grades[3]}',
			 team_5_name		='$names[4]',
			 team_5_grade		='{$grades[4]}',
			 team_6_name		='$names[5]',
			 team_6_grade		='{$grades[5]}',
			 team_7_name		='$names[6]',
			 team_7_grade		='{$grades[6]}',
			 team_8_name		='$names[7]',
			 team_8_grade		='{$grades[7]}',
			 team_code		='$team',
			 match_code		='{$code}'";
			//  print $sql;
			 sql_query($sql, true);
}


//수정된 조의 게임을 삭제, 재생성
//단체전은 4개짜리 게임까지 만들어지게되있음
//													바뀌기전							변경
function make_league_game_by_changing_group_team($group_info_before_assign, $group_info_after_assign){
	$code = $group_info_before_assign['match_code'];
	$index = 1;
	$before;
	for($i = 0; $i < 5; $i++){
		if($group_info_before_assign['team_'.$index] !=''){
			$before[] = $group_info_before_assign['team_'.$index];
		}
		$index ++;
	}


	$index = 1;
	$after;
	for($i = 0; $i < 5; $i++){
		if($group_info_after_assign['team_'.$index] !=''){
			$after[] = $group_info_after_assign['team_'.$index];
		}

		$index ++;
	}

	// 3팀에서 하나빼고 3개를 바로넣는건 불가능하므로 갯수가같은데 다를일은 없음
	if(count($before) != count($after)){
		// 우선 리그게임 다삭제
		delete_league_game($group_info_before_assign,$before);
		if(count($after) == 2){
			$term++;
			create_league_game($code,$group_info_after_assign
			,$group_info_after_assign['team_1'],$group_info_after_assign['team_2'],"L",0,3,$NO_Full_League,$term);
		}else if(count($after) == 3){
			$term++;
			create_league_game($code,$group_info_after_assign
			,$group_info_after_assign['team_1'],$group_info_after_assign['team_2'],"L",0,3,$NO_Full_League,$term);
			$term++;
			create_league_game($code,$group_info_after_assign
			,$group_info_after_assign['team_2'],$group_info_after_assign['team_3'],"L",0,2,$NO_Full_League,$term);
			$term++;
			create_league_game($code,$group_info_after_assign
			,$group_info_after_assign['team_1'],$group_info_after_assign['team_3'],"L",0,1,$NO_Full_League,$term);
		}
		//group assign 체크
		foreach ($after as $key=>$tema_code){
			sql_query("update team_data set group_assign = 1 where team_code = '{$tema_code}' ");
		}
		update_assign_data($group_info_after_assign);
	}
}


//수정된 조의 게임을 삭제, 재생성 
//단체전은 4개짜리 게임까지 만들어지게되있음
//													바뀌기전							변경
function make_league_game_by_changing_group_team_event($group_info_before_assign, $group_info_after_assign){
	$code = $group_info_before_assign['match_code'];
	$index = 1;
	$before;
	for($i = 0; $i < 5; $i++){
		if($group_info_before_assign['team_'.$index] !=''){
			$before[] = $group_info_before_assign['team_'.$index];
		}
		$index ++;
	}


	$index = 1;
	$after;
	for($i = 0; $i < 5; $i++){
		if($group_info_after_assign['team_'.$index] !=''){
			$after[] = $group_info_after_assign['team_'.$index];
		}

		$index ++;
	}

	// 3팀에서 하나빼고 3개를 바로넣는건 불가능하므로 갯수가같은데 다를일은 없음
	if(count($before) != count($after)){
		delete_league_game($group_info_before_assign,$before);
		// 우선 리그게임 다삭제
		if(count($after) == 2){
			for($i = 0; $i < 3; $i++){
				$term++;
				create_league_game($code,$group_info_after_assign
				,$group_info_after_assign['team_1'],$group_info_after_assign['team_2'],"L",0,3,$NO_Full_League,$term);
			}
		}else if(count($after) == 3){
			for($i = 0; $i < 3; $i++){
				$term++;
				create_league_game($code,$group_info_after_assign
				,$group_info_after_assign['team_1'],$group_info_after_assign['team_2'],"L",0,3,$NO_Full_League,$term);
				$term++;
				create_league_game($code,$group_info_after_assign
				,$group_info_after_assign['team_2'],$group_info_after_assign['team_3'],"L",0,2,$NO_Full_League,$term);
				$term++;
				create_league_game($code,$group_info_after_assign
				,$group_info_after_assign['team_1'],$group_info_after_assign['team_3'],"L",0,1,$NO_Full_League,$term);
			}
		}
		//group assign 체크
		foreach ($after as $key=>$tema_code){
			sql_query("update team_event_data set group_assign = 1 where team_code = '{$tema_code}' ");
		}
		update_assign_data($group_info_after_assign);
	}
}


//배정된 게임 수
function update_assign_data($groupinfo){

	$match_code = $groupinfo['match_code'];
	$group_id = $groupinfo['wr_id'];
	$group_code = $groupinfo['code'];

	$match_gym_data = sql_fetch("select * from match_gym_data where gym_id = (select gym_code from group_data where wr_id = {$group_id})");


	$gamedate = $match_gym_data['application_period'];
	$gym_code = $match_gym_data['gym_id'];

	$sql = "update game_score_data set game_date = '{$gamedate}', gym_code = {$gym_code}
	where group_data = '{$group_code}'";

}

//리그게임을 삭제한
function delete_league_game($group, $team_code){
	for($i = 0; $i < count($team_code) ; $i++){
		$code = $team_code[$i];
		$sql = "delete from game_score_data
		where
		match_code = '{$c}'
		and division = '{$d}'
		and series = '{$s}'
		and series_sub = '{$ss}'
		and tournament = 'L'
		and is_on <> 'N'
		and team_1_code = '{$code}' or team_2_code = '{$code}'";
		sql_query($sql);

	}
}



// 2017.06.12 dlrbgh
//drawer 정보 배열
function get_drawer($group_count){
	// print $group_count;
	// $total_round = $group_count *2;

	//							tournament_count
	//												tournament_num
	$group[2] = array(0=>array(0=>array(1=>'1조1위', 2=>'1조2위')) ) ;

	$group[4] = array(2=>array(0=>array(1=>'1조1위' ,2=>'2조2위')
														,1=>array(1=>'2조1위' ,2=>'1조2위')));
	$group[6] = array(2=>array(0=>array(1=>'1조1위' ,2=>'')
														,1=>array(1=>'2조1위' ,2=>''))
									, 4=>array(0=>array(1=>'2조2위', 2=>'3조2위')
														,1=>array(1=>'3조1위', 2=>'1조2위')));
	$group[8] = array(4=>array(0=>array(1=>'1조1위' ,2=>'2조2위')
														,1=>array(1=>'4조1위' ,2=>'3조2위')
														,2=>array(1=>'3조1위' ,2=>'4조2위')
														,3=>array(1=>'2조1위' ,2=>'1조2위')));
	$group[10] = array(4=>array(0=>array(1=>'1조1위', 2=>'')
														, 1=>array(1=>'5조1위', 2=>'4조1위')
														// , 2=>array(1=>'', 2=>'')
														, 2=>array(1=>'3조1위', 2=>'4조2위')
														// , 4=>array(1=>'', 2=>'')
														, 3=>array(1=>'2조1위', 2=>''))
										,8=>array(0=>array(1=>'2조2위', 2=>'3조2위')
															,1=>''
															,2=>''
															,3=>''
															,4=>''
															,5=>array(1=>'5조2위', 2=>'1조2위')));

	$group[12] = array(4=>array(0=>array(1=>'1조1위', 2=>'')
														, 1=>array(1=>'4조1위', 2=>'')
														, 2=>array(1=>'3조1위', 2=>'')
														, 3=>array(1=>'2조1위', 2=>''))
									, 8=>array(0=>array(1=>'2조2위', 2=>'6조2위')
														, 1=>array(1=>'5조1위', 2=>'3조2위')
														, 2=>array(1=>'4조2위', 2=>'6조1위')
														, 3=>array(1=>'5조2위', 2=>'1조2위'))
	);

	//아래애들이 갯수가 더 많으면
	//위에꺼 순서대로 붙여줘야한다;;
	//아래랑 위 14강 표를 볼것
	//주석처리한것대로하면 [14][4][0] 두번째는
	//										[14][8][0]이
	//										[14][4][1] 두번째는
	//										[14][8][1]이 자리잡게딘다.
	$group[14] = array(4=>array(0=>array(1=>'1조1위', 2=>'')
													 	, 1=>array(1=>'', 2=>'')
													 	, 2=>array(1=>'', 2=>'')
														, 3=>array(1=>'2조1위', 2=>''))
									, 8=>array(0=>array(1=>'2조2위', 2=>'7조2위')
													, 1=>array(1=>'5조1위', 2=>'6조2위')
													, 2=>array(1=>'3조2위', 2=>'4조1위')
													, 3=>array(1=>'3조1위', 2=>'4조2위')
													, 4=>array(1=>'6조1위', 2=>'5조2위')
													, 5=>array(1=>'7조1위', 2=>'1조2위'))
	);

	// $group[14] = array(4=>array(0=>array(1=>'1조1위', 2=>'')
	// 													, 1=>array(1=>'2조1위', 2=>''))
	// 								, 8=>array(0=>array(1=>'2조2위', 2=>'7조2위')
	// 												, 1=>array(1=>'7조1위', 2=>'1조2위')
	// 												, 2=>array(1=>'5조1위', 2=>'6조2위')
	// 												, 3=>array(1=>'3조2위', 2=>'4조1위')
	// 												, 4=>array(1=>'3조1위', 2=>'4조2위')
	// 												, 5=>array(1=>'6조1위', 2=>'5조2위'))
	// );
	$group[16] = array(8=>array(0=>array(1=>'1조1위', 2=>'2조2위')
										, 1=>array(1=>'8조1위', 2=>'7조2위')
										, 2=>array(1=>'5조1위', 2=>'6조2위')
										, 3=>array(1=>'4조1위', 2=>'3조2위')
										, 4=>array(1=>'3조1위', 2=>'4조2위')
										, 5=>array(1=>'5조2위', 2=>'6조1위')
										, 6=>array(1=>'7조1위', 2=>'8조2위')
										, 7=>array(1=>'2조1위', 2=>'1조2위'))
	);


	//dldntjs
	$group[18] = array(8=>array(0=>array(1=>'1조1위', 2=>'')
													 	, 1=>array(1=>'9조1위', 2=>'8조1위')
														, 2=>array(1=>'5조1위', 2=>'6조2위')
														, 3=>array(1=>'3조2위', 2=>'4조1위')
														, 4=>array(1=>'3조1위', 2=>'4조2위')
														, 5=>array(1=>'5조2위', 2=>'6조1위')
														, 6=>array(1=>'7조1위', 2=>'8조2위')
														, 7=>array(1=>'', 2=>'2조1위'))
									, 16=>array(0=>array(1=>'2조2위', 2=>'7조2위')
														, 1=>array(1=>'9조2위', 2=>'1조2위'))

	);

	$group[20] = array(8=>array(0=>array(1=>'1조1위', 2=>'')
													 	, 1=>array(1=>'9조1위', 2=>'8조1위')
														, 2=>array(1=>'5조1위', 2=>'7조2위')
														, 3=>array(1=>'', 2=>'4조1위')
														, 4=>array(1=>'3조1위', 2=>'')
														, 5=>array(1=>'8조2위', 2=>'6조1위')
														, 6=>array(1=>'7조1위', 2=>'10조1위')
														, 7=>array(1=>'', 2=>'2조1위'))
									, 16=>array(0=>array(1=>'2조2위', 2=>'10조2위')
														, 1=>array(1=>'6조2위', 2=>'3조2위')
														, 2=>array(1=>'4조2위', 2=>'5조2위')
														, 3=>array(1=>'9조2위', 2=>'1조2위'))

	);

	$group[22] = array(8=>array(0=>array(1=>'1조1위', 2=>'')
													 	, 1=>array(1=>'9조1위', 2=>'8조1위')
														, 2=>array(1=>'5조1위', 2=>'')
														, 3=>array(1=>'', 2=>'4조1위')
														, 4=>array(1=>'3조1위', 2=>'')
														, 5=>array(1=>'', 2=>'6조1위')
														, 6=>array(1=>'7조1위', 2=>'10조1위')
														, 7=>array(1=>'', 2=>'2조1위'))
									, 16=>array(0=>array(1=>'2조2위', 2=>'10조2위')
														, 1=>array(1=>'7조2위', 2=>'6조2위')
														, 2=>array(1=>'11조2위', 2=>'3조2위')
														, 3=>array(1=>'4조2위', 2=>'5조2위')
														, 4=>array(1=>'11조1위', 2=>'8조2위')
														, 5=>array(1=>'9조2위', 2=>'1조2위'))

	);

	$group[24] = array(8=>array(0=>array(1=>'1조1위', 2=>'')
													 	, 1=>array(1=>'', 2=>'8조1위')
														, 2=>array(1=>'5조1위', 2=>'')
														, 3=>array(1=>'', 2=>'4조1위')
														, 4=>array(1=>'3조1위', 2=>'')
														, 5=>array(1=>'', 2=>'6조1위')
														, 6=>array(1=>'7조1위', 2=>'')
														, 7=>array(1=>'', 2=>'2조1위'))
									, 16=>array(0=>array(1=>'2조2위', 2=>'10조2위')
														, 1=>array(1=>'9조1위', 2=>'7조2위')
														, 2=>array(1=>'6조2위', 2=>'12조1위')
														, 3=>array(1=>'11조2위', 2=>'3조2위')
														, 4=>array(1=>'4조2위', 2=>'12조2위')
														, 5=>array(1=>'11조1위', 2=>'5조2위')
														, 6=>array(1=>'8조2위', 2=>'10조1위')
														, 7=>array(1=>'9조2위', 2=>'1조2위'))

	);

	$group[26] = array(8=>array(0=>array(1=>'1조1위', 2=>'')
													 	, 1=>array(1=>'', 2=>'')
														, 2=>array(1=>'5조1위', 2=>'')
														, 3=>array(1=>'', 2=>'4조1위')
														, 4=>array(1=>'3조1위', 2=>'')
														, 5=>array(1=>'', 2=>'6조1위')
														, 6=>array(1=>'', 2=>'')
														, 7=>array(1=>'', 2=>'2조1위'))
									, 16=>array(0=>array(1=>'2조2위', 2=>'10조2위')
														, 1=>array(1=>'9조1위', 2=>'7조2위')
														, 2=>array(1=>'6조2위', 2=>'8조1위')
														, 3=>array(1=>'11조2위', 2=>'12조1위')
														, 4=>array(1=>'13조1위', 2=>'3조2위')
														, 5=>array(1=>'4조2위', 2=>'13조2위')
														, 6=>array(1=>'11조1위', 2=>'12조2위')
														, 7=>array(1=>'7조1위', 2=>'5조2위')
														, 8=>array(1=>'8조2위', 2=>'10조1위')
														, 9=>array(1=>'9조2위', 2=>'1조2위'))

	);

	$group[28] = array(8=>array(0=>array(1=>'1조1위', 2=>'')
													 	, 1=>array(1=>'', 2=>'')
														, 2=>array(1=>'', 2=>'')
														, 3=>array(1=>'', 2=>'4조1위')
														, 4=>array(1=>'3조1위', 2=>'')
														, 5=>array(1=>'', 2=>'')
														, 6=>array(1=>'', 2=>'')
														, 7=>array(1=>'', 2=>'2조1위'))
									, 16=>array(0=>array(1=>'2조2위', 2=>'10조2위')
														, 1=>array(1=>'9조1위', 2=>'7조2위')
														, 2=>array(1=>'6조2위', 2=>'8조1위')
														, 3=>array(1=>'5조1위', 2=>'11조2위')
														, 4=>array(1=>'14조2위', 2=>'12조1위')
														, 5=>array(1=>'13조1위', 2=>'3조2위')
														, 6=>array(1=>'4조2위', 2=>'14조1위')
														, 7=>array(1=>'11조1위', 2=>'13조2위')
														, 8=>array(1=>'12조2위', 2=>'6조1위')
														, 9=>array(1=>'7조1위', 2=>'5조2위')
														, 10=>array(1=>'8조2위', 2=>'10조1위')
														, 11=>array(1=>'9조2위', 2=>'1조2위'))

	);

	$group[30] = array(8=>array(0=>array(1=>'1조1위', 2=>'')
													 	, 1=>array(1=>'', 2=>'')
														, 2=>array(1=>'', 2=>'')
														, 3=>array(1=>'', 2=>'')
														, 4=>array(1=>'', 2=>'')
														, 5=>array(1=>'', 2=>'')
														, 6=>array(1=>'', 2=>'')
														, 7=>array(1=>'', 2=>'2조1위'))
									, 16=>array(0=>array(1=>'2조2위', 2=>'15조2위')
														, 1=>array(1=>'9조1위', 2=>'10조2위')
														, 2=>array(1=>'7조2위', 2=>'8조1위')
														, 3=>array(1=>'5조1위', 2=>'6조2위')
														, 4=>array(1=>'11조2위', 2=>'12조1위')
														, 5=>array(1=>'13조1위', 2=>'14조2위')
														, 6=>array(1=>'3조2위', 2=>'4조1위')
														, 7=>array(1=>'3조1위', 2=>'4조2위')
														, 8=>array(1=>'13조2위', 2=>'14조1위')
														, 9=>array(1=>'11조1위', 2=>'12조2위')
														, 10=>array(1=>'5조2위', 2=>'6조1위')
														, 11=>array(1=>'7조2위', 2=>'8조2위')
														, 12=>array(1=>'9조2위', 2=>'10조1위')
														, 13=>array(1=>'15조1위', 2=>'1조2위'))

	);

	$group[32] = array(16=>array(0=>array(1=>'1조1위', 2=>'2조2위')
														, 1=>array(1=>'15조2위', 2=>'16조1위')
														, 2=>array(1=>'9조1위', 2=>'10조2위')
														, 3=>array(1=>'7조2위', 2=>'8조1위')
														, 4=>array(1=>'5조1위', 2=>'6조2위')
														, 5=>array(1=>'11조2위', 2=>'12조1위')
														, 6=>array(1=>'13조1위', 2=>'14조2위')
														, 7=>array(1=>'3조2위', 2=>'4조1위')
														, 8=>array(1=>'3조1위', 2=>'4조2위')
														, 9=>array(1=>'13조2위', 2=>'14조1위')
														, 10=>array(1=>'11조1위', 2=>'12조2위')
														, 11=>array(1=>'5조2위', 2=>'6조1위')
														, 12=>array(1=>'7조1위', 2=>'8조2위')
														, 13=>array(1=>'9조2위', 2=>'10조1위')
														, 14=>array(1=>'15조1위', 2=>'16조2위')
														, 15=>array(1=>'1조2위', 2=>'2조1위'))

	);



		return $group[$group_count];

}
?>

