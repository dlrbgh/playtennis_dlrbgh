<?php
$menu_cate2 ='2';
$menu_cate3 ='';
$menu_cate4 ='3';
$menu_cate5 ='2';
include_once('./_common.php');
include_once(G5_SADM_PATH.'/head.php');

$match = sql_fetch("select * from match_data where code = '$code'");

?>
  <link rel="stylesheet" href="../dist/remodal.css">
  <link rel="stylesheet" href="../dist/remodal-default-theme.css">

<!-- sub hd_section-->
	<div class="tit_area">
		<div class="tit_subject">대회관리</div>
		<div class="tit">대회명 출력 (뷰 페이지 편집기능 없음_편집은 수정 페이지에서 진행)</div>
	</div>
<!--//sub_hd_area -->
	
<!-- sub_hd_area -->
<?php include_once(G5_SADM_PATH.'/inc/sub_hd.inc');?>
<!--//sub_hd_area -->

<!-- !PAGE CONTENT! -->
<div>
	<div class="tbl">
		<div class="inline-tab relative">
			<ul class="tab_pd">
				<li class=""><a href="">2017-04-26</a></li>
				<li class="active"><a href="">2017-04-27</a></li>
			</ul>
		</div>
		
	</div>
	
	<div style="width:1161px"><!-- 155px*칼럼수 값 + 76px-->
		<!-- champ_edit area-->
		<div class="champ_edit">
			<?php
			
				$sst = "( Case WHEN a.team_4 = '' Then 2 WHEN a.team_5 = '' Then 3 ELSE 1 End)";
				$group_count_sql = "select count(*) as cnt from group_data as a inner join series_data as b where a.match_code = '$code' and b.match_code = '$code' and b.gym_code = '$gym_id' and a.division = b.division and a.series = b.series  order by  $sst";
				$group_count_result = sql_query($group_count_sql);
				$group_count = sql_fetch_array($group_count_result);

				$sqls = "select count(*) as cnt from game_score_data as a inner join group_data as b 
						where a.match_code='$code' and a.tournament != 'L' and a.game_date = '$application_period' and a.gym_code = '$gym_id' and a.group_code = b.code";
				$group_results = sql_query($sqls);
				$group_counts = sql_fetch_array($group_results);
			?>
			<!-- court_game_count -->
				<div class="chmap_time_count">
				<ul>
					<?php
					for($j=1;$j<=ceil($group_count['cnt']/$court)+1 ;$j++){
					?>
					<li style="height: 249px"><?php echo $j;?>경기</li>
					<?php } ?>
					<?php
					for(;$j<=ceil($group_count['cnt']/$court)+1+ceil($group_counts['cnt']/$court)+4 ;$j++){
					?>
					<li><?php echo $j;?>경기</li>
					<?php } ?>
					
					
				</ul>
				</div>
			<!-- court_game_count -->
			
			<!-- full_court_container -->
			<div class="table-time"  >
				<!-- court_count_column-->
				<ul class="match_court">
					<?php
					
					for($j=1;$j<=$court;$j++){
						echo "<li style='width:159px;' >$j 코트</li>";
					}
					?>
				</ul>
				<!-- //court_count_column-->
				
				<ul class="team_edit_area">
					<!-- 숫자 court  -->
					<?php
					$cnt = 1;
					for($j=1;$j<=$court;$j++){
					$sst = "( Case WHEN a.team_4 = '' Then 2 WHEN a.team_5 = '' Then 3 ELSE 1 End)";
					$sql = "select *,a.wr_id as wr_ids from group_data as a inner join series_data as b where a.match_code = '$code' and a.game_court='$j' and b.match_code = '$code' and b.gym_code = '$gym_id' and a.division = b.division and a.series = b.series  order by  $sst";
					
					$group_result = sql_query($sql);
					?>

					<li>
						<!-- 0court 0group-->
						<ul id="A<?php echo $j;?>" style="width:155px; min-height:<?php echo ceil($group_count['cnt']/$court)*244+271;?>px;">
							<!-- 0court 0game-->
							<?php while($group = sql_fetch_array($group_result)){ ?>
							<li id="<?php echo $group['wr_ids'];?>" >
								<div class="btn-group text-center group_area male_play " role="group"> 
									<input type="hidden" name="wr_id[]" value="<?php echo $group['wr_ids'];?>" />
									<input type="hidden" name="game_court[]" value="<?php echo $group['game_court'];?>" />									
									<input type="hidden" name="tournament[]" value="L" />									
									<input type="hidden" name="court_array_num[]" value="<?php echo $group['court_array_num'];?>" />									
										<div class="team_play_bg">
											<?=$group['division'];?><?=$group['series'];?><?=$group['tournament'];?><?=$group['num']+1;?>조
											<div class="champ_code">
												<span>#<?php echo $group['wr_ids'];?></span>
												<!-- 경기중일때 표시 
												<i class="fa fa-circle text-red playing"></i>
												-->									
											</div>
										</div>
										<!-- 경기가 종료되었을때 class 추가 end_matchA , end_matchB -->
										<?php
											$game_sql = "select * from game_score_data where group_code = '{$group[code]}' and match_code = '$code' and tournament = 'L'";
											$game_result = sql_query($game_sql);
											$game = sql_fetch_array($game_result);
																												
											$sql_team1 = "select * from team_data where match_code = '$code' and team_code = '$group[team_1]'";
											$team1_result = sql_query($sql_team1);
											$team1 = sql_fetch_array($team1_result);
											
											$sql_team2 = "select * from team_data where match_code = '$code' and team_code = '$group[team_2]'";
											$team2_result = sql_query($sql_team2);
											$team2 = sql_fetch_array($team2_result);

											$sql_team3 = "select * from team_data where match_code = '$code' and team_code = '$group[team_3]'";
											$team3_result = sql_query($sql_team3);
											$team3 = sql_fetch_array($team3_result);
											
											$sql_team4 = "select * from team_data where match_code = '$code' and team_code = '$group[team_4]'";
											$team4_result = sql_query($sql_team4);
											$team4 = sql_fetch_array($team4_result);
																							
										?>
										
										<div class="club_area ">
											<div class="team_area">
												<div class="club_name"><?php echo $team1['club'];?></div>
												<div class="name"><?php echo $team1['team_1_name']."/".$team1['team_2_name'];?></div>
											</div>
											<div class="scoreboard_area">
												<div class="win_score">
												</div>
											</div>
											<!--<div class="scoreboard_area">
												<div class="win_score">
													<div id="score_session_18371">
														<div class="a_score">31</div>
														<div class="b_score">0</div>
													</div>
												</div>
											</div>
											-->
											<div class="team_area">
												<div class="club_name"><?php echo $team2['club'];?></div>
												<div class="name"><?php echo $team2['team_1_name']."/".$team2['team_2_name'];?></div>
											</div>
											<div class="team_area">
												<div class="club_name"><?php echo $team3['club'];?></div>
												<div class="name"><?php echo $team3['team_1_name']."/".$team3['team_2_name'];?></div>
											</div>
											<div class="team_area">
												<div class="club_name"><?php echo $team4['club'];?>&nbsp;</div>
												<div class="name"><?php echo $team4['team_1_name']."/".$team4['team_2_name'];?></div>
											</div>
										</div>
								</div>
							</li>
							<?php
							$cnt++;
							}
							?>
							<!-- // 0 court 0game-->
						</ul>
						
						<!-- //0court 0group-->
					</li>
					<script>
					Sortable.create(A<?php echo $j;?>, {
					  animation: 0,
					  chosenClass: "ui-state-chosen",
					  connectWith: "155",
					  group: {
					    name: "shared",
					    revertClone: false,
					  },
					  sort: true,
					  	onStart: function (/**Event*/evt, ui) {
					  		var itemEl = evt.item; 
					  		ui.placeholder.height(ui.item.height());

					  		//console.log(itemEl.id);
						},
					
						// Element dragging ended
						onEnd: function (/**Event*/evt) {
							var itemEl = evt.item;  
							evt.oldIndex;  // element's old index within parent
							evt.newIndex;  // element's new index within parent
							console.log(itemEl.id);
							$("#"+itemEl.id).addClass("ui-state-droped");
						},
						onAdd: function (/**Event*/evt) {
							var itemEl = evt.item;  // dragged HTMLElement
							
							var to_court = evt.to.id.replace("A", "");

							
							console.log( $("#"+itemEl.id).find("input[name='game_court[]']").val(to_court));
							var data = $("#"+itemEl.id).data("court");
							evt.from;  // previous list
							// + indexes from onEnd
						}
					});
					</script>

					<?php
					}
					?>
					<!-- //숫자 court  -->
				</ul>

				<ul class="team_edit_area">
					<!-- 숫자 court  -->
					<?php
					$sqls = "select count(*) as cnt from game_score_data as a inner join group_data as b 
							where a.match_code='$code' and a.tournament != 'L' and a.game_date = '$application_period' and a.gym_code = '$gym_id' and a.group_code = b.code";
					$group_results = sql_query($sqls);
					$group_counts = sql_fetch_array($group_results);

					for($j=1;$j<=$court;$j++){
						$sqlED = "select a.*,b.division,b.series,b.series_sub,b.num from game_score_data as a inner join group_data as b 
								where a.match_code='$code' and a.tournament != 'L' and a.game_court = '$j' and a.gym_code = '$gym_id' and a.group_code = b.code order by a.court_array_num asc";
						echo $sql;
						$group_resultS = sql_query($sqlED);
					?>

					<li>
						<!-- 0court 0group-->
						<ul id="B<?php echo $j;?>" style="width:155px; min-height:<?php echo ceil($group_counts['cnt']/$court+5)*168+198;?>px;">
							<!-- 0court 0game-->
							<?php while($grouped = sql_fetch_array($group_resultS)){ ?>
							<li id="<?php echo $grouped['wr_id'];?>" >
								<div class="btn-group text-center group_area male_play " role="group"> 
									<input type="hidden" name="wr_id[]" value="<?php echo $grouped['wr_id'];?>" />
									<input type="hidden" name="game_court[]" value="<?php echo $grouped['game_court'];?>" />									
									<input type="hidden" name="tournament[]" value="T" />									
									<input type="hidden" name="court_array_num[]" value="<?php echo $grouped['court_array_num'];?>" />									
										<div class="team_play_bg">
											<?php echo $grouped['division'];?><?php echo $group['series'];?><?php echo  $grouped['tournament'];?><?php echo $grouped['tournament_count']*2;?>
											<div class="champ_code">
												<span>#<?php echo $grouped['wr_id'];?></span>
												<!-- 경기중일때 표시 
												<i class="fa fa-circle text-red playing"></i>
												-->									
											</div>
										</div>
										<!-- 경기가 종료되었을때 class 추가 end_matchA , end_matchB -->
										<?php
											$sql_team1 = "select * from team_data where match_code = '$code' and team_code = '$grouped[team_1_code]'";
											$team1_result = sql_query($sql_team1);
											$team1 = sql_fetch_array($team1_result);
											
											$sql_team2 = "select * from team_data where match_code = '$code' and team_code = '$grouped[team_2_code]'";
											$team2_result = sql_query($sql_team2);
											$team2 = sql_fetch_array($team2_result);
												
										?>
										
										<div class="club_area ">
											<div class="team_area">
												<div class="club_name"><?php echo $team1['club'];?></div>
												<div class="name"><?php echo $team1['team_1_name']."/".$team1['team_2_name'];?></div>
											</div>
											<div class="scoreboard_area">
												<div class="win_score">
												</div>
											</div>
											<div class="team_area">
												<div class="club_name"><?php echo $team2['club'];?></div>
												<div class="name"><?php echo $team2['team_1_name']."/".$team2['team_2_name'];?></div>
											</div>
										</div>
								</div>
							</li>
							<?php
							}
							?>
							<!-- // 0 court 0game-->
						</ul>
						
						<!-- //0court 0group-->
					</li>
					<script>
					Sortable.create(B<?php echo $j;?>, {
					  animation: 100,
					  chosenClass: "ui-state-chosen",
					  connectWith: "155",
					  group: {
					    name: "shared",
					    revertClone: false,
					  },
					  sort: true,
					  	onStart: function (/**Event*/evt, ui) {
					  		var itemEl = evt.item; 
					  		//ui.placeholder.height(ui.item.height());
						},
					
						// Element dragging ended
						onEnd: function (/**Event*/evt) {
							var itemEl = evt.item;  
							evt.oldIndex;  // element's old index within parent
							evt.newIndex;  // element's new index within parent
							console.log(itemEl.id);
							$("#"+itemEl.id).addClass("ui-state-droped");
						},
						onAdd: function (/**Event*/evt) {
							var itemEl = evt.item;  // dragged HTMLElement
							
							var to_court = evt.to.id.replace("A", "");
							var to_court = evt.to.id.replace("B", "");
							console.log(to_court);
							console.log( $("#"+itemEl.id).find("input[name='game_court[]']").val(to_court));
							console.log($("#"+itemEl.id).find("input[name='game_court[]']").val());
							var data = $("#"+itemEl.id).data("court");
							// + indexes from onEnd
						}
					});
					</script>

					<?php
					}
					?>
					<!-- //숫자 court  -->
				</ul>
			</div>	
			<!-- //full_court_container -->
		</div>
		<!-- //champ_edit area-->
	</div>

	<div class="btn_area">
		<a class="btn btn02 fw-600" href="<?php echo G5_SADM_URL?>/championship_add_step3.php">수 정</a>
	</div>

</div>



<?php
include_once(G5_SADM_PATH.'/tail.php');
?>