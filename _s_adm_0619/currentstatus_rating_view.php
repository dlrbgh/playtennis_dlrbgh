<?php
$menu_cate2 ='2';
$menu_cate3 ='2';
include_once('./_common.php');
include_once(G5_SADM_PATH.'/_head.php');

include_once (G5_SADM_PATH.'/lib/tennis.common.php');
include_once (G5_AOS_PATH.'/common_functions/Drawer.php');



$team_field = "team_data";

if($series['division'] == "단체전"){
	$team_field = "team_event_data";
}

$sql_team = "select count(wr_id) as cnt from $team_field where match_code='$code' and division='$series[division]' and series ='$series[series]' and series_sub = '$series[series_sub]'";
$team_result = sql_query($sql_team);
$r = sql_fetch_array($team_result);

$sql_teams = "select count(wr_id) as cnt from group_data where match_code='$code' and division='$series[division]' and series ='$series[series]' and series_sub = '$series[series_sub]'";
$team_results = sql_query($sql_teams);
$rs = sql_fetch_array($team_results);

$sql_team = "select count(wr_id) as cnt from $team_field where match_code='$code' and division='$series[division]' and series ='$series[series]' and series_sub = '$series[series_sub]'";
$team_result = sql_query($sql_team);
$r = sql_fetch_array($team_result);
		
?>

<!-- sub hd_section-->
	<div class="tit_area">
		<div class="tit_subject"><?php echo $menu_title; ?>(메인 카테고리영역)</div>
		<div class="tit">경기상황별 자세히보는 화면 뷰페이지</div>
		<div class="r-btn-area">
			<ul>
				<li><a href="" class="btn_default">대진 상황표 이동</a></li>
			</ul>
		</div>
	</div>
<!--//sub_hd_area -->

<!-- !PAGE CONTENT! -->
<div>
	
	<!-- 그룹명  -->
	<div class="sub_hd">
        <div class="l_area">
        	<div class="tit"><?php echo $division." ".$series." ".$series_sub;?> - <?php echo $r['cnt'];?>명 - <?php echo $rs['cnt'];?>조</div>
        </div>
        <div class="r_area">
        	<!-- 
        		참가조 추가 : 추가로 조 추가
        		참가팀 추가 : 참가팀 등록
        		미배정팀 (00팀) : 배정이 안된팀은 미배정팀으로 이동
        		-->
        	<ul>
        		<li><div class="tit"><a onclick='window.open("./tournament_map.php?code=<?php echo $code;?>&division=<?php echo $division;?>&series=<?php echo $series;?>&series_sub=<?php echo $series_sub;?>&gym_id=<?php echo $series['gym_id'];?>", "tournament_map", "width=1000,height=800");'><img class="ico_small" src="<?php echo G5_IMG_URL?>/common/svg/printer.svg" title="print">토너먼트</a></div></li>
        		<li><div class="tit"><a onclick='window.open("./champ_matchtbl_print.php?code=<?php echo $code;?>", "champ_matchtbl", "width=1000,height=800");'><img class="ico_small" src="<?php echo G5_IMG_URL?>/common/svg/printer.svg" title="print">대진표</a></div></li>
        	</ul>
        </div>
    </div>
    <!-- //그룹명  -->

	<div class="sub_hd">
        <!--<div class="tit"><?php echo $division." ".$series." ".$series_sub;?>토너먼트 대진표</div>-->
        <div >
        	<div id="table" style=" width:100%;"></div>
				<script type="text/javascript" src="<?php print G5_JS_URL?>/tournament/jquery.bracket.min.js"></script>
				<link rel="stylesheet" type="text/css" href="<?php print G5_JS_URL?>/tournament/jquery.bracket.min.css" />
				<script>
				var singleElimination = {
				  "teams": [
				    <?php
							$groupResult = get_group_code_and_count($code, $division, $series, $series_sub);
							$groupCount = $groupResult['cnt'] * 2;
							$tournament = get_drawer('', $groupCount);
							$rounds = get_rounds($groupCount);
							// print_r($tournament);
				    $is_first = false;
				    for($i = 0; $i < count($tournament[$rounds]); $i++){
				      if($is_first){
				        print ",";
				      }else{
				        $is_first = true;
				      }
				      //array 일 경우 조에관한 데이터가 들어간것이므로
				      if(is_array($tournament[$rounds][$i])){
				        print "['".$tournament[$rounds][$i][0]."',null]";
				      //밑으로 새끼친애들은 그 이후의 강(라운드)*2 에 저장되어있다
				      //tournament 의 해당값은 int로 강*2의 key값이다
				      }else{
				        print "['".$tournament[$rounds*2][$tournament[$rounds][$i]][0]."', '".$tournament[$rounds*2][$tournament[$rounds][$i]][1]."']";
				      }
				    }
				    ?>
				  ]
				}
				$('#table').bracket({init:singleElimination})
				</script>	
        </div>
    </div>

	<div class="champ_match_result">
		<ul>
			<?php 
	           	$sql = "select * from group_data where match_code='$code' and division='$division' and series='$series' and series_sub='$series_sub' and tournament	='L' order by num";
				$gourp_result = sql_query($sql);
				$i = 0;
				$j = 1;
				
			?>
			
			<?php while($group = sql_fetch_array($gourp_result)){ ?>
			
			
			<li style="min-height: 460px;">				
				<?php $gym_name = sql_fetch("select gym_name from gym_data where wr_id = '{$group['gym_code']}' ", true);  ?>
				<div class="tit_area">
					<span style="font-size:18px"><?=$group['num']+1;?>조</span>
					<?php echo $gym_name['gym_name'];  //여기에 코트 및 경기순서 들어갈 자리 

					$team_field = "team_data";
					if($division == "단체전"){
						$team_field = "team_event_data";
					}
										?> 
				</div>

				
				<div class="text-center">
					<div class="current_rating_tbl">
						<table>
	            			<thead>
	            				<tr>
	            				<th>구분</th>
	            				<?php
	            					if($group['team_1']!=""){
	                					$team_sql = "select * from $team_field where match_code='$code' and team_code='{$group['team_1']}'";
										$team_result = sql_query($team_sql);
										$r = sql_fetch_array($team_result);
									?>
	                				<th class="text-center"><?=$r['club'];?><br/><span class="font-s08"><?=$r['team_1_name'];?>,<?=$r['team_2_name'];?></span></th>
	                				<?php }	?>
	                				<?php
	            					if($group['team_2']!=""){
	                					$team_sql = "select * from $team_field where match_code='$code' and team_code='{$group['team_2']}'";
										$team_result = sql_query($team_sql);
										$r = sql_fetch_array($team_result);
									?>
	                				<th class="text-center"><?=$r['club'];?><br/><span class="font-s08"><?=$r['team_1_name'];?>,<?=$r['team_2_name'];?></span></th>
	                				<?php }	?>
	                				<?php
	            					if($group['team_3']!=""){
	                					$team_sql = "select * from $team_field where match_code='$code' and team_code='{$group['team_3']}'";
										$team_result = sql_query($team_sql);
										$r = sql_fetch_array($team_result);
									?>
	                				<th class="text-center"><?=$r['club'];?><br/><span class="font-s08"><?=$r['team_1_name'];?>,<?=$r['team_2_name'];?></span></th>
	                				<?php }	?>
	                				<?php
	            					if($group['team_4']!=""){
	                					$team_sql = "select * from team_data where match_code = '$code' and team_code = '$group[team_4]'";
										$team_result = sql_query($team_sql);
										$r = sql_fetch_array($team_result);
									?>
	                				<th class="text-center"><?=$r['club'];?><br/><span class="font-s08"><?=$r['team_1_name'];?>,<?=$r['team_2_name'];?></span></th>
	                				<?php }	?>
	                				<?php
	            					if($group['team_5']!=""){
	                					$team_sql = "select * from team_data where match_code = '$code' and team_code = '$group[team_5]'";
										$team_result = sql_query($team_sql);
										$r = sql_fetch_array($team_result);
									?>
	                				<th class="text-center"><?=$r['club'];?><br/><span class="font-s08"><?=$r['team_1_name'];?>,<?=$r['team_2_name'];?></span></th>
	                				<?php }	?>
	            				<tr>
	            				</tr>
	            			</thead>
							<tbody class="text-center">
								<?php
                					if($group['team_1']!=""){
                    					$team_sql = "select * from $team_field where match_code = '$code' and team_code = '$group[team_1]'";
										$team_result = sql_query($team_sql);
										$r = sql_fetch_array($team_result);
										
										$score_sql = "select * from game_score_data where match_code = '$code' and tournament = 'L' and (team_1_code = '$group[team_1]' or team_2_code = '$group[team_1]') and (team_1_code = '$group[team_2]' or team_2_code = '$group[team_2]')";
										$score_result = sql_query($score_sql);
										$score1 = sql_fetch_array($score_result);

										$score_sql = "select * from game_score_data where match_code = '$code' and tournament = 'L' and (team_1_code = '$group[team_1]' or team_2_code = '$group[team_1]') and (team_1_code = '$group[team_3]' or team_2_code = '$group[team_3]')";
										$score_result = sql_query($score_sql);
										$score2 = sql_fetch_array($score_result);
										
										if($group['team_4']!=""){
											$score_sql = "select * from game_score_data where match_code = '$code' and tournament = 'L' and (team_1_code = '$group[team_1]' or team_2_code = '$group[team_1]') and (team_1_code = '$group[team_4]' or team_2_code = '$group[team_4]')";
											$score_result = sql_query($score_sql);
											$score3 = sql_fetch_array($score_result);
										}
										if($group['team_5']!=""){
											$score_sql = "select * from game_score_data where match_code = '$code' and tournament = 'L' and (team_1_code = '$group[team_1]' or team_2_code = '$group[team_1]') and (team_1_code = '$group[team_5]' or team_2_code = '$group[team_5]')";
											$score_result = sql_query($score_sql);
											$score4 = sql_fetch_array($score_result);
										}
								?>
                				<tr>
                					<td class="font-w700 teamA_winner"><?=$r['club'];?><br/><span class="font-s08"><?=$r['team_1_name'];?>, <?=$r['team_2_name'];?></span></td>
                					<td class="text-center table_slash"></td>
                					
                					<?php if($score1['team_1_code'] == $group['team_1']){ ?>
                					<td class="text-center"><h3><?=$score1['team_1_score']?>&nbsp;:&nbsp;<?=$score1['team_2_score']?></h3></td>
                					<?php }?>
                					<?php if($score1['team_2_code'] == $group['team_1']){ ?>
                					<td class="text-center"><h3><?=$score1['team_2_score']?>&nbsp;:&nbsp;<?=$score1['team_1_score']?></h3></td>
                					<?php }?>
                					
                					<?php if($score2['team_1_code'] == $group['team_1']){ ?>
                					<td class="text-center"><h3><?=$score2['team_1_score']?>&nbsp;:&nbsp;<?=$score2['team_2_score']?></h3></td>
                					<?php }?>
                					<?php if($score2['team_2_code'] == $group['team_1']){ ?>
                					<td class="text-center"><h3><?=$score2['team_2_score']?>&nbsp;:&nbsp;<?=$score2['team_1_score']?></h3></td>
                					<?php }?>
                					
                					<?php if($group['team_4']!=""){ ?>
                    					<?php if($score3['team_1_code'] == $group['team_1']){?>
                    					<td class="text-center"><h3><?=$score3['team_1_score']?>&nbsp;:&nbsp;<?=$score3['team_2_score']?></h3></td>
                    					<?php }?>
                    					<?php if($score3['team_2_code'] == $group['team_1']){?>
                    					<td class="text-center"><h3><?=$score3['team_2_score']?>&nbsp;:&nbsp;<?=$score3['team_1_score']?></h3></td>
                    					<?php }?>
                					<?php }	?>
                					
                					<?php if($group['team_5']!=""){?>
                    					<?php if($score4['team_1_code'] == $group['team_1']){ ?>
                    					<td class="text-center"><h3><?=$score4['team_1_score']?>&nbsp;:&nbsp;<?=$score4['team_2_score']?></h3></td>
                    					<?php }?>
                    					<?php if($score4['team_2_code'] == $group['team_1']){ ?>
                    					<td class="text-center"><h3><?=$score4['team_2_score']?>&nbsp;:&nbsp;<?=$score4['team_1_score']?></h3></td>
                    					<?php }?>
                					<?php }	?>
                				</tr>
                				<?php
									}
                				?>
								<?php
                					if($group['team_2']!=""){
                    					$team_sql = "select * from $team_field where match_code = '$code' and team_code = '$group[team_2]'";
										$team_result = sql_query($team_sql);
										$r = sql_fetch_array($team_result);
										
										$score_sql = "select * from game_score_data where match_code = '$code' and tournament = 'L' and (team_1_code = '$group[team_2]' or team_2_code = '$group[team_2]') and (team_1_code = '$group[team_1]' or team_2_code = '$group[team_1]')";
										$score_result = sql_query($score_sql);
										$score1 = sql_fetch_array($score_result);
										
										$score_sql = "select * from game_score_data where match_code = '$code' and tournament = 'L' and (team_1_code = '$group[team_2]' or team_2_code = '$group[team_2]') and (team_1_code = '$group[team_3]' or team_2_code = '$group[team_3]')";
										$score_result = sql_query($score_sql);
										$score2 = sql_fetch_array($score_result);
										
										if($group['team_4']!=""){
											$score_sql = "select * from game_score_data where match_code = '$code' and tournament = 'L' and (team_1_code = '$group[team_2]' or team_2_code = '$group[team_2]') and (team_1_code = '$group[team_4]' or team_2_code = '$group[team_4]')";
											$score_result = sql_query($score_sql);
											$score3 = sql_fetch_array($score_result);
										}
										if($group['team_5']!=""){
											$score_sql = "select * from game_score_data where match_code = '$code' and tournament = 'L' and (team_1_code = '$group[team_2]' or team_2_code = '$group[team_2]') and (team_1_code = '$group[team_5]' or team_2_code = '$group[team_5]')";
											$score_result = sql_query($score_sql);
											$score4 = sql_fetch_array($score_result);
										}					
								?>
                				<tr>
                					<td class="font-w700 teamA_winner"><?=$r['club'];?><br/><span class="font-s08"><?=$r['team_1_name'];?>, <?=$r['team_2_name'];?></span></td>
                					<?php if($score1['team_1_code'] == $group['team_2']){?>
                					<td class="text-center"><h3><?=$score1['team_1_score']?>&nbsp;:&nbsp;<?=$score1['team_2_score']?></h3></td>
                					<?php }?>
                					<?php if($score1['team_2_code'] == $group['team_2']){?>
                					<td class="text-center"><h3><?=$score1['team_2_score']?>&nbsp;:&nbsp;<?=$score1['team_1_score']?></h3></td>
                					<?php }?>
                					
                					<td class="text-center table_slash"></td>
                					
									<?php if($score2['team_1_code'] == $group['team_2']){?>
                					<td class="text-center"><h3><?=$score2['team_1_score']?>&nbsp;:&nbsp;<?=$score2['team_2_score']?></h3></td>
                					<?php }?>
                					<?php if($score2['team_2_code'] == $group['team_2']){?>
                					<td class="text-center"><h3><?=$score2['team_2_score']?>&nbsp;:&nbsp;<?=$score2['team_1_score']?></h3></td>
                					<?php }?>		                    					
                					
                					
                					<?php if($group['team_4']!=""){?>
                    					<?php if($score3['team_1_code'] == $group['team_2']){?>
                    					<td class="text-center"><h3><?=$score3['team_1_score']?>&nbsp;:&nbsp;<?=$score3['team_2_score']?></h3></td>
                    					<?php }?>
                    					<?php if($score3['team_2_code'] == $group['team_2']){?>
                    					<td class="text-center"><h3><?=$score3['team_2_score']?>&nbsp;:&nbsp;<?=$score3['team_1_score']?></h3></td>
                    					<?php }?>
                					<?php }	?>
                					
                					<?php if($group['team_5']!=""){?>
                    					<?php if($score4['team_1_code'] == $group['team_2']){?>
                    					<td class="text-center"><h3><?=$score4['team_1_score']?>&nbsp;:&nbsp;<?=$score4['team_2_score']?></h3></td>
                    					<?php }?>
                    					<?php if($score4['team_2_code'] == $group['team_2']){?>
                    					<td class="text-center"><h3><?=$score4['team_2_score']?>&nbsp;:&nbsp;<?=$score4['team_1_score']?></h3></td>
                    					<?php }?>
                					<?php }	?>
                				</tr>
                				<?php
									}
                				?>
								<?php
                					if($group['team_3']!=""){
                    					$team_sql = "select * from $team_field where match_code = '$code' and team_code = '$group[team_3]'";
										$team_result = sql_query($team_sql);
										$r = sql_fetch_array($team_result);
										
										$score_sql = "select * from game_score_data where match_code = '$code' and tournament = 'L' and (team_1_code = '$group[team_3]' or team_2_code = '$group[team_3]') and (team_1_code = '$group[team_1]' or team_2_code = '$group[team_1]')";
										$score_result = sql_query($score_sql);
										$score1 = sql_fetch_array($score_result);
										 
										$score_sql = "select * from game_score_data where match_code = '$code' and tournament = 'L' and (team_1_code = '$group[team_3]' or team_2_code = '$group[team_3]') and (team_1_code = '$group[team_2]' or team_2_code = '$group[team_2]')";
										$score_result = sql_query($score_sql);
										$score2 = sql_fetch_array($score_result);
										
										if($group['team_4']!=""){
											$score_sql = "select * from game_score_data where match_code = '$code' and tournament = 'L' and (team_1_code = '$group[team_3]' or team_2_code = '$group[team_3]') and (team_1_code = '$group[team_4]' or team_2_code = '$group[team_4]')";
											$score_result = sql_query($score_sql);
											$score3 = sql_fetch_array($score_result);
										}
										if($group['team_5']!=""){
											$score_sql = "select * from game_score_data where match_code = '$code' and tournament = 'L'  and (team_1_code = '$group[team_3]' or team_2_code = '$group[team_3]') and (team_1_code = '$group[team_5]' or team_2_code = '$group[team_5]')";
											$score_result = sql_query($score_sql);
											$score4 = sql_fetch_array($score_result);
										}
								?>
                				<tr>
                					<td class="font-w700 teamA_winner"><?=$r['club'];?><br/><span class="font-s08"><?=$r['team_1_name'];?>, <?=$r['team_2_name'];?></span></td>
                					
                					<?php if($score1['team_1_code'] == $group['team_3']){?>
                					<td class="text-center"><h3><?=$score1['team_1_score']?>&nbsp;:&nbsp;<?=$score1['team_2_score']?></h3></td>
                					<?php }?>
                					<?php if($score1['team_2_code'] == $group['team_3']){?>
                					<td class="text-center"><h3><?=$score1['team_2_score']?>&nbsp;:&nbsp;<?=$score1['team_1_score']?></h3></td>
                					<?php }?>
                					
									<?php if($score2['team_1_code'] == $group['team_3']){?>
                					<td class="text-center"><h3><?=$score2['team_1_score']?>&nbsp;:&nbsp;<?=$score2['team_2_score']?></h3></td>
                					<?php }?>
                					<?php if($score2['team_2_code'] == $group['team_3']){?>
                					<td class="text-center"><h3><?=$score2['team_2_score']?>&nbsp;:&nbsp;<?=$score2['team_1_score']?></h3></td>
                					<?php }?>		                    					
                					<td class="text-center table_slash"></td>
                					<?php if($group['team_4']!=""){?>
                    					<?php if($score3['team_1_code'] == $group['team_3']){?>
                    					<td class="text-center"><h3><?=$score3['team_1_score']?>&nbsp;:&nbsp;<?=$score3['team_2_score']?></h3></td>
                    					<?php }?>
                    					<?php if($score3['team_2_code'] == $group['team_3']){?>
                    					<td class="text-center"><h3><?=$score3['team_2_score']?>&nbsp;:&nbsp;<?=$score3['team_1_score']?></h3></td>
                    					<?php }?>
                					<?php }	?>
                					<?php if($group['team_5']!=""){?>
                    					<?php if($score4['team_1_code'] == $group['team_3']){?>
                    					<td class="text-center"><h3><?=$score4['team_1_score']?>&nbsp;:&nbsp;<?=$score4['team_2_score']?></h3></td>
                    					<?php }?>
                    					<?php if($score4['team_2_code'] == $group['team_3']){?>
                    					<td class="text-center"><h3><?=$score4['team_2_score']?>&nbsp;:&nbsp;<?=$score4['team_1_score']?></h3></td>
                    					<?php }?>
                					<?php }	?>
                				</tr>
                				<?php
									}
                				?>

								<?php
                					if($group['team_4']!=""){
                    					$team_sql = "select * from team_data where match_code = '$code' and team_code = '$group[team_4]'";
										$team_result = sql_query($team_sql);
										$r = sql_fetch_array($team_result);
										
										$score_sql = "select * from game_score_data where match_code = '$code' and tournament = 'L' and (team_1_code = '$group[team_4]' or team_2_code = '$group[team_4]') and (team_1_code = '$group[team_1]' or team_2_code = '$group[team_1]')";
										$score_result = sql_query($score_sql);
										$score1 = sql_fetch_array($score_result);
										
										$score_sql = "select * from game_score_data where match_code = '$code' and tournament = 'L' and (team_1_code = '$group[team_4]' or team_2_code = '$group[team_4]') and (team_1_code = '$group[team_2]' or team_2_code = '$group[team_2]')";
										$score_result = sql_query($score_sql);
										$score2 = sql_fetch_array($score_result);
										
										if($group['team_4']!=""){
											$score_sql = "select * from game_score_data where match_code = '$code' and tournament = 'L' and (team_1_code = '$group[team_4]' or team_2_code = '$group[team_4]') and (team_1_code = '$group[team_3]' or team_2_code = '$group[team_3]')";
											$score_result = sql_query($score_sql);
											$score3 = sql_fetch_array($score_result);
										}
										if($group['team_5']!=""){
											$score_sql = "select * from game_score_data where match_code = '$code' and tournament = 'L' and (team_1_code = '$group[team_4]' or team_2_code = '$group[team_4]') and (team_1_code = '$group[team_5]' or team_2_code = '$group[team_5]')";
											$score_result = sql_query($score_sql);
											$score4 = sql_fetch_array($score_result);
										}
								?>
                				<tr>
                					<td class="font-w700 teamA_winner"><?=$r['club'];?><br/><span class="font-s08"><?=$r['team_1_name'];?>, <?=$r['team_2_name'];?></span></td>
                					
                					<?php if($score1['team_1_code'] == $group['team_4']){?>
                					<td class="text-center"><h3><?=$score1['team_1_score']?>&nbsp;:&nbsp;<?=$score1['team_2_score']?></h3></td>
                					<?php }?>
                					<?php if($score1['team_2_code'] == $group['team_4']){?>
                					<td class="text-center"><h3><?=$score1['team_2_score']?>&nbsp;:&nbsp;<?=$score1['team_1_score']?></h3></td>
                					<?php }?>
                					
									<?php if($score2['team_1_code'] == $group['team_4']){?>
                					<td class="text-center"><h3><?=$score2['team_1_score']?>&nbsp;:&nbsp;<?=$score2['team_2_score']?></h3></td>
                					<?php }?>
                					<?php if($score2['team_2_code'] == $group['team_4']){?>
                					<td class="text-center"><h3><?=$score2['team_2_score']?>&nbsp;:&nbsp;<?=$score2['team_1_score']?></h3></td>
                					<?php }?>		                    					

									<?php if($score3['team_1_code'] == $group['team_4']){?>
                					<td class="text-center"><h3><?=$score3['team_1_score']?>&nbsp;:&nbsp;<?=$score3['team_2_score']?></h3></td>
                					<?php }?>
                					<?php if($score3['team_2_code'] == $group['team_4']){?>
                					<td class="text-center"><h3><?=$score3['team_2_score']?>&nbsp;:&nbsp;<?=$score3['team_1_score']?></h3></td>
                					<?php }?>		                    					

                					<?php if($group['team_4']!=""){?>
                					<td class="text-center table_slash"></td>
                					<?php }	?>
                					
                					<?php if($group['team_5']!=""){?>
                    					<?php if($score4['team_1_code'] == $group['team_5']){?>
                    					<td class="text-center"><h3><?=$score4['team_1_score']?>&nbsp;:&nbsp;<?=$score4['team_2_score']?></h3></td>
                    					<?php }?>
                    					<?php if($score4['team_2_code'] == $group['team_5']){?>
                    					<td class="text-center"><h3><?=$score4['team_2_score']?>&nbsp;:&nbsp;<?=$score4['team_1_score']?></h3></td>
                    					<?php }?>
                					<?php }	?>
                				</tr>
                				<?php
									}
                				?>
								<?php
                					if($group['team_5']!=""){
                    					$team_sql = "select * from team_data where match_code = '$code' and team_code = '$group[team_5]'";
										$team_result = sql_query($team_sql);
										$r = sql_fetch_array($team_result);
										
										$score_sql = "select * from game_score_data where match_code = '$code' and tournament = 'L' and (team_1_code = '$group[team_5]' or team_2_code = '$group[team_5]') and (team_1_code = '$group[team_1]' or team_2_code = '$group[team_1]')";
										$score_result = sql_query($score_sql);
										$score1 = sql_fetch_array($score_result);
										
										$score_sql = "select * from game_score_data where match_code = '$code' and tournament = 'L' and (team_1_code = '$group[team_5]' or team_2_code = '$group[team_5]') and (team_1_code = '$group[team_2]' or team_2_code = '$group[team_2]')";
										$score_result = sql_query($score_sql);
										$score2 = sql_fetch_array($score_result);
										
										if($group['team_4']!=""){
											$score_sql = "select * from game_score_data where match_code = '$code' and tournament = 'L' and (team_1_code = '$group[team_5]' or team_2_code = '$group[team_5]') and (team_1_code = '$group[team_3]' or team_2_code = '$group[team_3]')";
											$score_result = sql_query($score_sql);
											$score3 = sql_fetch_array($score_result);
										}
										if($group['team_5']!=""){
											$score_sql = "select * from game_score_data where match_code = '$code' and tournament = 'L' and (team_1_code = '$group[team_5]' or team_2_code = '$group[team_5]') and (team_1_code = '$group[team_4]' or team_2_code = '$group[team_4]')";
											$score_result = sql_query($score_sql);
											$score4 = sql_fetch_array($score_result);
										}
								?>
                				<tr>
                					<td class="font-w700 teamA_winner"><?=$r['club'];?><br/><span class="font-s08"><?=$r['team_1_name'];?>, <?=$r['team_2_name'];?></span></td>		                    					
                					<?php if($score1['team_1_code'] == $group['team_5']){?>
                					<td class="text-center"><h3><?=$score1['team_1_score']?>&nbsp;:&nbsp;<?=$score1['team_2_score']?></h3></td>
                					<?php }?>
                					<?php if($score1['team_2_code'] == $group['team_5']){?>
                					<td class="text-center"><h3><?=$score1['team_2_score']?>&nbsp;:&nbsp;<?=$score1['team_1_score']?></h3></td>
                					<?php }?>
                					
									<?php if($score2['team_1_code'] == $group['team_5']){?>
                					<td class="text-center"><h3><?=$score2['team_1_score']?>&nbsp;:&nbsp;<?=$score2['team_2_score']?></h3></td>
                					<?php }?>
                					<?php if($score2['team_2_code'] == $group['team_5']){?>
                					<td class="text-center"><h3><?=$score2['team_2_score']?>&nbsp;:&nbsp;<?=$score2['team_1_score']?></h3></td>
                					<?php }?>		    
                					      
									<?php if($score3['team_1_code'] == $group['team_5']){?>
                					<td class="text-center"><h3><?=$score3['team_1_score']?>&nbsp;:&nbsp;<?=$score3['team_2_score']?></h3></td>
                					<?php }?>
                					<?php if($score3['team_2_code'] == $group['team_5']){?>
                					<td class="text-center"><h3><?=$score3['team_2_score']?>&nbsp;:&nbsp;<?=$score3['team_1_score']?></h3></td>
                					<?php }?>		          

                					<?php if($group['team_4']!=""){?>
                    					<?php if($score4['team_1_code'] == $group['team_5']){?>
                    					<td class="text-center"><h3><?=$score4['team_1_score']?>&nbsp;:&nbsp;<?=$score4['team_2_score']?></h3></td>
                    					<?php }?>
                    					<?php if($score4['team_2_code'] == $group['team_5']){?>
                    					<td class="text-center"><h3><?=$score4['team_2_score']?>&nbsp;:&nbsp;<?=$score4['team_1_score']?></h3></td>
                    					<?php }?>
                					<?php }	?>
                					
                					<?php if($group['team_5']!=""){?>
                					<td class="text-center table_slash"></td>
                					<?php }	?>
                					

                				</tr>
                				<?php
									}
                				?>
								<!-- 
							    <tr class="winner1">
									<th class="font-w700 win " style="background-color:#d9edf7;color:#000"나래<br><span class="font-s08">김명미, 신은미</span></th>
			    					<td class="text-center win"><span>25</span>:<span>31</span></td>
									<td class="text-center "></td>
									<td class="text-center win"><span>25</span>:<span>31</span></td>
									<td class="text-center win"><span>25</span>:<span>31</span></td>
								</tr>
								<tr class="winner2">
									<th class="font-w700 teamA_winner">신남<br><span class="font-s08">차미옥, 박미향</span></th>
									<td class="text-center"><span>25</span>:<span>31</span></td>
			    					<td class="text-center"><span>25</span>:<span>31</span></td>
									<td class="text-center "></td>
			    					<td class="text-center win"><span>25</span>:<span>31</span></td>
								<tr>
									<th class="font-w700 teamA_winner">한마음<br><span class="font-s08">김영옥, 이미경</span></th>
			    					<td class="text-center win"><span>25</span>:<span>31</span></td>
			    					<td class="text-center"><span>25</span>:<span>31</span></td>
			    					<td class="text-center"><span>25</span>:<span>31</span></td>
									<td class="text-center "></td>
								</tr>-->
	        				</tbody>
	                	</table>
	                </div>
	                
	                <div class="rank_summary_tbl">
	                	<table>
	                		<thead>
	                			<tr>
	                				<th>순위</th>
	                				<th>클럽명 - 선수명</th>
	                				
	                				<th>승</th>
	                				<th>패</th>
	                			</tr>
	                		</thead>
	                		<tbody>
	                			<?php 
	                    			$group['division'];
                					$score_sql = "select * from team_data where (team_code  = '$group[team_1]' or team_code  = '$group[team_2]' or team_code  = '$group[team_3]' or team_code  = '$group[team_4]'  or team_code  = '$group[team_5]') and match_code = '$code' and division = '$division' and series = '$series' and series_sub = '$series_sub'  ORDER BY group_rank,team_league_point desc ,winning_rate	,gains_losses_point desc, team_total_match_point DESC";
									$score_result = sql_query($score_sql);
									$i = 1;
									while($score = sql_fetch_array($score_result)){
										
										$gourp_sql = "select num from group_data where (team_1  = '$score[team_code]' or team_2  = '$score[team_code]' or team_3  = '$score[team_code]' or team_4  = '$score[team_code]'  or team_5  = '$score[team_code]') and match_code = '$code' and division = '$division' and series = '$series' and series_sub = '$series_sub' and tournament = 'L' order by num asc";
										$group_result = sql_query($gourp_sql);
										$group = sql_fetch_array($group_result);
									?>
	                    			<tr class="winner1">
	                    				<td><?=$i;?></td>
	                    				<td><?=$score['club'];?><Br><?=$score['team_1_name'];?><?=$score['team_2_name'];?></td>
	                    				<td><?=$score['team_league_point'];?></td>
	                    				<td><?=$score['team_league_lose_count'];?></td>
	                    			</tr>
	                    			<?php $i++; } ?>
	                		</tbody>
	                	</table>
	                	
	                </div>
				</div>
			</li>
			<?php $cnt++;}  ?>
		</ul>	
		
	</div>
	

	<div class="btn_area">
		<a class="btn btn01 fw-600" href="javascript:history.go(-1)">목록으로</a>
		<a class="btn btn01 fw-600" href="<?php echo G5_SADM_URL?>/championship_add_step3.php">대진 상황표 이동</a>
	</div>

</div>
<!-- End page content -->



<?php
include_once(G5_SADM_PATH.'/_tail.php');
?>
