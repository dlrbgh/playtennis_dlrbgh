<?php
include_once('./_common.php');
include_once('./head.sub.php');

include_once (G5_SADM_PATH.'/lib/tennis.common.php');
include_once (G5_AOS_PATH.'/common_functions/Drawer.php');
?>

<style>
	body {background: rgb(204,204,204);}
	page {
	 background: white;
	 display: block;
	 margin: 0 auto;
	 margin-bottom: 0.5cm;
	 /*box-shadow: 0 0 0.5cm rgba(0,0,0,0.5);*/
	 
	}
	page[size="A4"] {  
	  width: 21cm;
	  height: 29.7cm; 
	}
	page[size="A4"][layout="portrait"] {
	  width: 29.7cm;
	  height: 21cm;  
	}
	page[size="A3"] {
	  width: 29.7cm;
	  height: 42cm;
	}
	page[size="A3"][layout="portrait"] {
	  width: 42cm;
	  height: 29.7cm;  
	}
	page[size="A5"] {
	  width: 14.8cm;
	  height: 21cm;
	}
	page[size="A5"][layout="portrait"] {
	  width: 21cm;
	  height: 14.8cm;  
	}
	
	@media print {
	  body, page {
	    margin: 0;
	    box-shadow: 0;
	    -webkit-print-color-adjust: exact;
	  -moz-background-inline-policy /*배경이미지 적용할때*/
	  }
	  
	  
	  
	  
	}
/*print*/	

</style>	


<script type="text/javascript" src="<?php print G5_JS_URL?>/tournament/jquery.bracket.min.print.js"></script>
<link rel="stylesheet" type="text/css" href="<?php print G5_JS_URL?>/tournament/jquery.bracket.min.print.css" />

<page size="A4_">
	<!-- 그룹명  -->
	<div class="sub_hd">
        <div class="tit"><?php echo $division." ".$series." ".$series_sub;?>토너먼트 대진표<!-- - <?php echo $r['cnt'];?>명 - <?php echo $rs['cnt'];?>조--></div>
    </div>
    <!-- //그룹명  -->	
		<div>
		        <div>
		        	<div id="tournament_map" style=" width:100%;"></div>
				</div>
				<!--<script>
			var singleElimination = {
			  "teams": [
			    <?php
						$groupResult = get_group_code_and_count($code, $division, $series, $series_sub);
						$groupCount = $groupResult['cnt'] * 2;
						$tournament = get_drawer('', $groupCount);
						$rounds = get_rounds($groupCount);
						// print_r($tournament);
			    $is_first = false;
			    for($i = 0; $i < count($tournament[$rounds]); $i++){
			      if($is_first){
			        print ",";
			      }else{
			        $is_first = true;
			      }
			      //array 일 경우 조에관한 데이터가 들어간것이므로
			      if(is_array($tournament[$rounds][$i])){
			        print "['".$tournament[$rounds][$i][0]."',null]";
			      //밑으로 새끼친애들은 그 이후의 강(라운드)*2 에 저장되어있다
			      //tournament 의 해당값은 int로 강*2의 key값이다
			      }else{
			        print "['".$tournament[$rounds*2][$tournament[$rounds][$i]][0]."', '".$tournament[$rounds*2][$tournament[$rounds][$i]][1]."']";
			      }
			    }
			    ?>
			  ]
			}
			$('#tournament_map').bracket({init:singleElimination})
			</script>-->
			<script>
					var singleElimination = {
					  "teams": [
					    <?php



								$groupcount = sql_fetch("  select count(*) * 2  as count from group_data where
									match_code = '{$c}' and division = '{$d}' and series = '{$s}' and series_sub='{$ss}'
								")['count'];
								// // print " select count(*) * 2  as count from group_data where
								// 	match_code = '{$c}' and division = '{$d}' and series = '{$s}' and series_sub='{$ss}'
								// ";
								// print $groupcount;
							 print get_rounds_example($groupcount)?>
					  ]
					}
					// $('#table').bracket({init:singleElimination})


					// These are modified by the sliders
					var resizeParameters = {
					  teamWidth: 222222,
					  scoreWidth: 20,
					  matchMargin: 133330,
					  roundMargin: 50,
					  init: singleElimination
					};

					function updateResizeDemo() {
						$('#tournament_map').bracket({init:singleElimination})
					}
					$(updateResizeDemo)
					</script>
		</div>
</page>
<!--
<script>
	window.print();
</script>
-->