<?php
$menu_cate2 ='2';
$menu_cate3 ='';
$menu_cate4 ='1';
$menu_cate5 ='1';
include_once('./_common.php');
include_once(G5_SADM_PATH.'/head.php');

$match = sql_fetch("select * from match_data where code = '$code'");

?>
  <link rel="stylesheet" href="../dist/remodal.css">
  <link rel="stylesheet" href="../dist/remodal-default-theme.css">

<!-- sub hd_section-->
	<div class="tit_area">
		<div class="tit_subject">대회관리</div>
		<div class="tit">대회명 출력</div>
	</div>
<!--//sub_hd_area -->

<!-- sub_hd_tab_area -->
	<?php include_once(G5_SADM_PATH.'/inc/sub_hd.inc');?>
<!--//sub_hd_tab_area -->

<!-- !PAGE CONTENT! -->
<div>
	<!-- 대회정보 필수입력-->
	<div class="tbl_style01 tbl_striped tbl_borderd">
		<table>
			<thead>
				<tr>
					<th>항목</th>
					<th>내용</th>
				</tr>
			</thead>
			<tbody>
				<tr>
					<td width="100">대회종류</td>
					<td>
						<?php
							if($match['scale'] == 1){ echo "KTA"; }
							if($match['scale'] == 2){ echo "KATO"; }
							if($match['scale'] == 3){ echo "KATA"; }
							if($match['scale'] == 4){ echo "KASTA"; }
							if($match['scale'] == 5){ echo "LOCAL"; }
							if($match['scale'] == 6){ echo "비랭킹"; }
						?>
					</td>
				</tr>
				<tr>
					<td>대회이름</td>
					<td><?php echo $match['wr_name'] ?></td>
				</tr>
				<tr>
					<td>대회기간</td>
					<td><?php echo $match['date1'] ?>~<?php echo $match['date2'] ?></td>
				</tr>
				<tr>
					<td>접수기간</td>
					<td><?php echo $match['period1'] ?>~<?php echo $match['period1'] ?></td>
				</tr>
				<tr>
					<td>주최기관</td>
					<td><?php echo $match['organizer'] ?></td>
				</tr>
				<tr>
					<td>주관기관</td>
					<td><?php echo $match['supervision'] ?></td>
				</tr>
				<tr>
					<td>대회지역</td>
					<td><?php echo $match['area_1']; echo $match['area_2'] ?></td>
				</tr>
				<tr>
					<td>대회홈페이지</td>
					<td><?php echo $match['homepage'] ?></td>
				</tr>
				<tr>
					<td>대회장소</td>
					<td><?php echo $match['place'] ?></td>
				</tr>
				<tr>
					<td>추가정보</td>
					<td><?php echo nl2br($match['text1']); ?></td>
				</tr>
			</tbody>
		</table>
	</div>
	<!-- //대회정보 필수입력-->

	<div class="btn_area">
		<!-- 수정할때 출력-->
		<a class="btn btn02 delete fw-600" onclick="">수 정</a>
    <a href="create_tournament_game.php?code=<?=$code?>">토너먼트 생성</a>
	</div>

</div>




<?php
include_once(G5_SADM_PATH.'/tail.php');
?>
