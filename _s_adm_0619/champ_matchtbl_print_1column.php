<?php
//include_once('./_common.php');
//include_once(G5_SADM_PATH.'/head.php');
include_once('./_common.php');
include_once('./head.sub.php');
$match = sql_fetch("select * from match_data where code = '$code'");

?>
<link rel="stylesheet" href="<?php echo G5_SADM_URL?>/assets/css/media_print.css" type="text/css" media="print">
<style>
	/*body {background: rgb(204,204,204);}*/
	page {
	 background: white;
	 display: block;
	 margin: 0 auto;
	 margin-bottom: 0.5cm;
	 /*box-shadow: 0 0 0.5cm rgba(0,0,0,0.5);*/
	 
	}
	page[size="A4"] {  
	  width: 21cm;
	  height: 29.7cm; 
	}
	page[size="A4"][layout="portrait"] {
	  width: 29.7cm;
	  height: 21cm;  
	}
	page[size="A3"] {
	  width: 29.7cm;
	  height: 42cm;
	}
	page[size="A3"][layout="portrait"] {
	  width: 42cm;
	  height: 29.7cm;  
	}
	page[size="A5"] {
	  width: 14.8cm;
	  height: 21cm;
	}
	page[size="A5"][layout="portrait"] {
	  width: 21cm;
	  height: 14.8cm;  
	}
	
	@media print {
	  body, page {
	    margin: 0;
	    box-shadow: 0;
	    -webkit-print-color-adjust: exact;
	  -moz-background-inline-policy /*배경이미지 적용할때*/
	  }
	  
	}

	
/*print*/	
.sub_hd .tit{font-size:22px;}
.champ_group_edit{page-break-after: always;}
.champ_group_edit ul li{/*page-break-after: always;*/}
.champ_group_edit ul li{float:left;width:100%;margin-right:2%;border:1px solid #ccc;margin-bottom:15px;position:relative;}
.champ_group_edit ul li:nth-child(3n+3){margin-right:0%;}

.champ_group_edit ul li .tit_area{border-bottom:2px solid #000;padding:15px;font-weight:600;font-size:14px}
.champ_group_edit ul li .tit_area .r-btn-area{position:absolute;right:0;top:0;padding:15px;font-size:11px;font-family:'dotum'}
.champ_group_edit ul li .tit_area .r-btn-area a.btn{margin-right:5px;border:1px solid;padding:5px 10px;border-radius:5px;}
.champ_group_edit ul li .tit_area .r-btn-area a.btn:last-child{margin-right:0}
.champ_group_edit ul li .tit_area .r-btn-area a.btn.add{border-color:#34a263;background-color:#46c37b;color:#fff}
.champ_group_edit ul li .tit_area .r-btn-area a.btn.del{border-color:#c54736;background-color:#d26a5c;color:#fff}


.champ_group_table {padding:15px;}
.champ_group_table table thead tr th{border-bottom:2px solid #ccc;padding:5px 0;}
.champ_group_table table tbody tr td{border-bottom:1px solid #ccc;padding:5px;}
.champ_group_table table tbody tr:last-child td{border-bottom:none}
	
</style>

<page size="A4" >

<!-- !PAGE CONTENT! -->
<div class="page">
	<?php
       	$i = 0;
        $sql = "select * from series_data where match_code='$code'";
		$series_result = sql_query($sql);
	?>
	<?php
		while($series = sql_fetch_array($series_result)){

	?>	<!-- 그룹명  -->
	<?php
		//점수 삽입
		$team_field = "team_data";

		if($series['division'] == "단체전"){
			$team_field = "team_event_data";
		}

    	$sql_team = "select count(wr_id) as cnt from $team_field where match_code='$code' and division='$series[division]' and series ='$series[series]' and series_sub = '$series[series_sub]'";
		$team_result = sql_query($sql_team);
		$r = sql_fetch_array($team_result);

    	$sql_teams = "select count(wr_id) as cnt from group_data where match_code='$code' and division='$series[division]' and series ='$series[series]' and series_sub = '$series[series_sub]'";
		$team_results = sql_query($sql_teams);
		$rs = sql_fetch_array($team_results);

    ?>
	<div class="sub_hd">
        <div class="l_area">
        	<div class="tit"><?php echo $series['division']." ".$series['series']." ".$series['series_sub'];?> - <?php echo $r['cnt'];?>명 - <?php echo $rs['cnt'];?>조</div>
        </div>
    </div>
    <!-- //그룹명  -->
	<div class="champ_group_edit" >
		<ul>
			<?php
	           	$i = 0;
	            $sql = "select * from group_data where match_code='$code' and division='$series[division]' and series='$series[series]' and series_sub='$series[series_sub]' and tournament	='L' order by num";
				$gourp_result = sql_query($sql);
			?>
			
			<?php while($group = sql_fetch_array($gourp_result)){?>
			<li style="min-height: 236ㅁpx;">
				<div class="tit_area">
					<?php $gym_name = sql_fetch("select gym_name from gym_data where wr_id = '{$group['gym_code']}' ", true);  ?>

					<?php echo $group['num']+1?>조-<?php echo $gym_name['gym_name'] ?>
				</div>

				<div class="champ_group_table text-center" >
					<table>
	        			<thead>
	        				<tr>
	            				<th colspan="3">1번</th>
	            				<th colspan="3">2번</th>
	            				<th colspan="3">3번</th>
	        				</tr>
	        			</thead>
	        			<tbody>
	        				<tr>
	        					<?php
                					if($group['team_1']!=""){
                					$team_sql = "select * from $team_field where match_code = '$code' and team_code = '$group[team_1]'";
									$team_result = sql_query($team_sql);
									$r = sql_fetch_array($team_result);
                				?>
	        					<td><?=$r['club']?>
	        					</td>
	        					<td>
	        						<?=$r['area_1'];?> / <?=$r['area_2'];?><br/>
	        					</td>
	        					<td><?=$r['team_1_name'];?><br>
	        						<?=$r['team_2_name'];?><br>
	        						<?php
			        					if($series['division'] == "단체전"){
									?>
									<?=$r['team_3_name'];?><br>
									<?=$r['team_4_name'];?><br>
									<?=$r['team_5_name'];?><br>
									<?=$r['team_6_name'];?><br>
									<?=$r['team_7_name'];?><br>
									<?=$r['team_8_name'];?><br>
									<?php
										}
			        				?>
	        					</td>
	        					
		                        <?php }?>
		                        
		                        <?php
                					if($group['team_2']!=""){
                					$team_sql = "select * from $team_field where match_code = '$code' and team_code = '$group[team_2]'";
									$team_result = sql_query($team_sql);
									$r = sql_fetch_array($team_result);
                				?>
	        					<td><?=$r['club']?>
	        					</td>
	        					<td>
	        						<?=$r['area_1'];?> / <?=$r['area_2'];?><br/>
	        					</td>
	        					<td><?=$r['team_1_name'];?><br>
	        						<?=$r['team_2_name'];?><br>
	        						<?php
			        					if($series['division'] == "단체전"){
									?>
									<?=$r['team_3_name'];?><br>
									<?=$r['team_4_name'];?><br>
									<?=$r['team_5_name'];?><br>
									<?=$r['team_6_name'];?><br>
									<?=$r['team_7_name'];?><br>
									<?=$r['team_8_name'];?><br>
									<?php
										}
			        				?>
	        					</td>
	        					
		                        <?php }?>
		                        
		                        <?php
                					if($group['team_3']!=""){
                					$team_sql = "select * from $team_field where match_code = '$code' and team_code = '$group[team_3]'";
									$team_result = sql_query($team_sql);
									$r = sql_fetch_array($team_result);
                				?>
	        					<td><?=$r['club']?>
	        					</td>
	        					<td>
	        						<?=$r['area_1'];?> / <?=$r['area_2'];?><br/>
	        					</td>
	        					<td><?=$r['team_1_name'];?><br>
	        						<?=$r['team_2_name'];?><br>
	        						<?php
			        					if($series['division'] == "단체전"){
									?>
									<?=$r['team_3_name'];?><br>
									<?=$r['team_4_name'];?><br>
									<?=$r['team_5_name'];?><br>
									<?=$r['team_6_name'];?><br>
									<?=$r['team_7_name'];?><br>
									<?=$r['team_8_name'];?><br>
									<?php
										}
			        				?>
	        					</td>
	        					
		                        <?php }?>
	        				</tr>

	        				<!--<tr>
	        					<?php
                					if($group['team_2']!=""){
                					$team_sql = "select * from $team_field where match_code = '$code' and team_code = '$group[team_2]'";
									$team_result = sql_query($team_sql);
									$r = sql_fetch_array($team_result);
                				?>
	        					<td><?=$r['club']?>
	        					</td>
	        					<td>
	        						<?=$r['area_1'];?> / <?=$r['area_2'];?><br/>
	        					</td>
	        					<td><?=$r['team_1_name'];?><br>
	        						<?=$r['team_2_name'];?><br>
	        						<?php
			        					if($series['division'] == "단체전"){
									?>
									<?=$r['team_3_name'];?><br>
									<?=$r['team_4_name'];?><br>
									<?=$r['team_5_name'];?><br>
									<?=$r['team_6_name'];?><br>
									<?=$r['team_7_name'];?><br>
									<?=$r['team_8_name'];?><br>
									<?php
										}
			        				?>
	        					</td>
	        					
		                        <?php }?>
	        				</tr>

	        				<tr>
	        					<?php
                					if($group['team_3']!=""){
                					$team_sql = "select * from $team_field where match_code = '$code' and team_code = '$group[team_3]'";
									$team_result = sql_query($team_sql);
									$r = sql_fetch_array($team_result);
                				?>
	        					<td><?=$r['club']?>
	        					</td>
	        					<td>
	        						<?=$r['area_1'];?> / <?=$r['area_2'];?><br/>
	        					</td>
	        					<td><?=$r['team_1_name'];?><br>
	        						<?=$r['team_2_name'];?><br>
	        						<?php
			        					if($series['division'] == "단체전"){
									?>
									<?=$r['team_3_name'];?><br>
									<?=$r['team_4_name'];?><br>
									<?=$r['team_5_name'];?><br>
									<?=$r['team_6_name'];?><br>
									<?=$r['team_7_name'];?><br>
									<?=$r['team_8_name'];?><br>
									<?php
										}
			        				?>
	        					</td>
	        					
		                        <?php }?>
	        				</tr>
	        				<!--<tr>
	        					<?php
                					if($group['team_4']!=""){
                					$team_sql = "select * from $team_field where match_code = '$code' and team_code = '$group[team_4]'";
									$team_result = sql_query($team_sql);
									$r = sql_fetch_array($team_result);
                				?>
	        					<td><?=$r['club']?>
	        					</td>
	        					<td>
	        						<?=$r['area_1'];?> / <?=$r['area_2'];?><br/>
	        					</td>
	        					<td><?=$r['team_1_name'];?> <br> <?=$r['team_2_name'];?></td>
	        					
		                        <?php }?>
	        				</tr>
							-->
	        			</tbody>
	                </table>
				</div>
			</li>
			
			<?php $i++;} ?>
			
		</ul>
	</div>
		<?php $i++;} ?>

</div>


</page>
<script>
	//window.print();
</script>


