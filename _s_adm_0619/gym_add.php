<?php
$menu_cate2 ='4';
$menu_cate3 ='1';
include_once('./_common.php');
include_once(G5_SADM_PATH.'/_head.php');

?>
<?php
$wr_id = $_REQUEST['wr_id'];
$sql = "select * from gym_data where wr_id = '$wr_id'";
$result = sql_query($sql);
$gym = sql_fetch_array($result);

?>

<script src="<?php echo G5_SADM_URL?>/assets/js/autosave"></script>
<!-- sub hd_section-->
	<div class="tit_area">
		<div class="tit_subject">경기장 관리</div>
		<div class="tit">경기장 글쓰기</div>
	</div>
<!--//sub hd_section -->


<!-- !PAGE CONTENT! -->
<form class="form-horizontal push-10-t push-10" action="gym_update.php" method="post" onsubmit="return true;">

<div>
	<div class="tbl_frm01 ">
	
		<table>
			<thead>
				<tr>
					<th width="100">항목</th>
					<th>내용</th>
				</tr>
			</thead>
			<tbody >
				<tr>
					<td>지역</td>
					<td>
                        <div class="col-md-5">
                        	광역시/도 <input type="text" name="gym-add1" id="gym-add1" value="<?=$gym['gym_address1'];?>"/>
                        </div>
                        <div class="col-md-5">	
                        	시/군/구 <input type="text" name="gym-add2" id="gym-add2" value="<?=$gym['gym_address2'];?>" />
                        </div>
					</td>
				</tr>
				<input type="hidden" id="lat" name="lat" value="<?=$gym['lat'];?>" />
	            <input type="hidden" id="lng" name="lng" value="<?=$gym['lng'];?>" />
				<tr>
					<td>지도검색</td>
					<td>
						<form class="js-form-search push-10" action="base_comp_maps.php" method="post" onsubmit="return false;" >
						<div class="input-group">
                            <input class="js-search-address form-control" value="코엑스" id="gym-map" name="gym-map" placeholder="Search.. (eg: 장충체육관)" type="text">
                            <div class="input-group-btn">
                                <button class="btn btn-default" onclick="search();" type="button"><i class="fa fa-search"></i>검색</button>
                            </div>
                        </div>
                         </form>
						<div>
							<div id="map" class="push-10-t" style="height: 300px;"></div>
						</div>
					</td>
				</tr>
				<tr>
					<td>명칭</td>
					<td><input class="form-control" type="text" id="gym-name" value="<?=$gym['gym_name'];?>"  name="gym-name" placeholder="지도검색 마크를 선택하시면 자동으로 입력됩니다."></td>
				</tr>
				<tr>
					<td>상세주소</td>
					<td><input class="form-control" type="text" id="gym-add3" value="<?=$gym['gym_address3'];?>"  name="gym-add3" placeholder="지도검색 마크를 선택하시면 자동으로 입력됩니다." ></td>
				</tr>
				<tr>
					<td>코트수</td>
					<td><input type="text" id="gym_courts" value="<?=$gym['gym_courts'];?>"  name="gym_courts" class="frm_input required" size="50" maxlength="255" type="text"></td>
				</tr>
			</tbody>
		</table>
		
	</div>
	
	<div class="btn_area">
		<a class="btn btn01 fw-600" href="javascript:history.go(-1)">뒤 로</a>
		<input type="submit" class="btn btn01 fw-600" value="등 록">
	</div>
</div>
</form>
<!-- End page content -->

<script type="text/javascript" src="//apis.daum.net/maps/maps3.js?apikey=85629d9f847388a273d6deba7dce3bbd&libraries=services"></script>

<script>
// 마커를 클릭하면 장소명을 표출할 인포윈도우 입니다
var infowindow = new daum.maps.InfoWindow({zIndex:1});

var mapContainer = document.getElementById('map'), // 지도를 표시할 div 
    mapOption = {
        center: new daum.maps.LatLng(37.566826, 126.9786567), // 지도의 중심좌표
        level: 3 // 지도의 확대 레벨
    };  

// 지도를 생성합니다    
var map = new daum.maps.Map(mapContainer, mapOption); 

// 장소 검색 객체를 생성합니다
var ps = new daum.maps.services.Places(); 

// 키워드로 장소를 검색합니다

function search(){
	var search = document.getElementById('gym-map');
	ps.keywordSearch(search.value, placesSearchCB); 

}
// 키워드 검색 완료 시 호출되는 콜백함수 입니다
function placesSearchCB (status, data, pagination) {
    if (status === daum.maps.services.Status.OK) {

        // 검색된 장소 위치를 기준으로 지도 범위를 재설정하기위해
        // LatLngBounds 객체에 좌표를 추가합니다
        var bounds = new daum.maps.LatLngBounds();

        for (var i=0; i<data.places.length; i++) {
            displayMarker(data.places[i]);    
            bounds.extend(new daum.maps.LatLng(data.places[i].latitude, data.places[i].longitude));
        }       

        // 검색된 장소 위치를 기준으로 지도 범위를 재설정합니다
        map.setBounds(bounds);
    } 
}

// 지도에 마커를 표시하는 함수입니다
function displayMarker(place) {
    
    // 마커를 생성하고 지도에 표시합니다
    var marker = new daum.maps.Marker({
        map: map,
        position: new daum.maps.LatLng(place.latitude, place.longitude) 
    });
	console.log(place);
    // 마커에 클릭이벤트를 등록합니다
    daum.maps.event.addListener(marker, 'click', function() {
        // 마커를 클릭하면 장소명이 인포윈도우에 표출됩니다
        document.getElementById('gym-name').value = place.title;
        document.getElementById('gym-add3').value = place.address;
        document.getElementById('gym-add4').value = place.newAddress;

        document.getElementById('lat').value = place.latitude;
        document.getElementById('lng').value = place.longitude;

        infowindow.setContent('<div style="padding:5px;font-size:12px;">' + place.title + '</div>');
        infowindow.open(map, marker);
    });
}
</script>

<?php
include_once(G5_SADM_PATH.'/_tail.php');
?>
