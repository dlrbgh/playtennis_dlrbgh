<?php
$menu_cate2 ='2';
$menu_cate3 ='2';
include_once('./_common.php');
include_once(G5_SADM_PATH.'/head.php');

$main_point = sql_fetch("select * from main_points where match_code = '$code'");
?>

  <link rel="stylesheet" href="../dist/remodal.css">
  <link rel="stylesheet" href="../dist/remodal-default-theme.css">
<script src="<?php echo G5_SADM_URL?>/assets/js/autosave"></script>
<!-- sub hd_section-->
	<div class="tit_area">
		<div class="tit_subject">대회관리</div>
		<div class="tit">Step.2 : 대회장소 등록  <a href="#modal" class="btn_default">장소추가</a></div>

	</div>
<!--//sub_hd_area -->

<!-- !PAGE CONTENT! -->
<form action="championship_update_main_point.php" method="post" onsubmit="return true;" enctype="multipart/form-data">
<div>
	<input type="hidden" name="match_code" value="<?php echo $code;?>" />
	<input type="hidden" name="wr_id" value="<?php echo $main_point['wr_id'];?>" />
	<!-- 대회정보 필수입력-->
	<ul id="place_add_list"  class="champ_map_list">
		<?php
			$increse = 0;
			$charger_increse = 0;
		?>
		<?php
        	$sql_s = "select * from match_gym_data inner join gym_data where match_gym_data.match_id='$code' and gym_data.wr_id = match_gym_data.gym_id";
			$result = sql_query($sql_s);


			while($r = sql_fetch_array($result)){
		?>
		<li id="gym_div_<?php echo $increse;?>" >
			<input type="hidden" name="chk[]" value="<?php echo $increse;?>"></input>
			<input type="hidden" name="wr_id[]" value="<?php echo $r['wr_id'];?>"></input>
			<input type="hidden" name="chk2[]" value="<?php echo $charger_increse;?>"></input>

			<p>
				<span>장소명 :</span><?php echo $r['gym_name']?><a href="">&nbsp;<i class="flaticon-placeholder"></i></a><br/>
				<span>주소 :</span><?php echo $r['gym_address3']?><br/>
			</p>

			<div class="text-center place_date_add">
				<a id="gym_plus<?php echo $increse;?>" onclick="gym_time_plus('<?php echo $increse;?>','<?php echo $r['wr_id'];?>','<?php echo $r['gym_courts'];?>')"  >
					<span class="color5">일정을 등록하지 않으면 경기기간 동안 해당 체육관을 사용하는 것으로 설정됩니다.</span>
					+ 경기일정 추가
				</a>
			</div>
			<div class="match_day">
				<ul>
					<li>
						경기일 : <input class="frm_input text-center" id="application-period1" required name="application-period[]" value="<?php echo $r['application_period']?>" placeholder="시작일" type="text">
						&nbsp;/&nbsp;사용코트수 :
						<select required id="use-court" name="use-court[]">
							<?php
								for($i = 1 ; $i <= $r['gym_courts'] ;$i++){
							?>
							<option <?php if($r['use_court'] == $i){ echo "selected"; }?> value="<?php echo $i;?>"><?php echo $i;?></option>
							<?php
								}
							?>
						</select>
						<!-- &nbsp;/&nbsp;담당자1 : <input type="text" name="charger[]" value="<?php echo $r2['charger'];?>" /> -->
            <br/>
						&nbsp;/&nbsp;담당자1 : <input type="text" name="charger1[]" value="<?php echo $r['charger1'];?>" />
						&nbsp;/&nbsp;담당자2 : <input type="text" name="charger2[]" value="<?php echo $r['charger2'];?>" />
						&nbsp;/&nbsp;담당자3 : <input type="text" name="charger3[]" value="<?php echo $r['charger3'];?>" />
						&nbsp;/&nbsp;담당자4 : <input type="text" name="charger4[]" value="<?php echo $r['charger4'];?>" />
						&nbsp;/&nbsp;담당자5 : <input type="text" name="charger5[]" value="<?php echo $r['charger5'];?>" />
					</li>
					<li id="gym_plus_place<?php echo $increse;?>">
					</li>
				</ul>
			</div>
			<div class="text-center">
				<a onclick="delete_gym(<?php echo $increse;?>)" class="btn_default bkg5">삭제</a>
			</div>
		</li>
		<?php } ?>

		<!-- 경기장 장소 추가 버튼 -->
		<li>
			<div class="text-center place_date_add">
				<a href="#modal">
					<span class="color5">+ 경기장 장소 추가</span>
				</a>
			</div>
		</li>
		<!-- //경기장 장소 추가 버튼 -->
	</ul>
	<!-- //대회정보 필수입력-->

	<div class="btn_area">
		<a class="btn btn01 fw-600" href="<?php echo G5_SADM_URL?>/championship_add.php?code=<?php echo $code;?>">뒤 로</a>
		<a class="btn btn01 fw-600" href="<?php echo G5_SADM_URL?>/championship_add_step2.php?code=<?php echo $code;?>">다 음(skip)</a>
		<input class="btn btn01 fw-600" value="다 음" type="submit">
	</div>
</div>
</form>


<div class="remodal" data-remodal-id="modal" role="dialog" aria-labelledby="modal1Title" aria-describedby="modal1Desc">
	<button data-remodal-action="close" class="remodal-close" aria-label="Close"></button>
		<div class="tbl_frm01">
			<table class="table ">
			    <tbody>
			    	<tr>
			    		<th>장소선택</th>
			    		<td>
			    			<select>
			    				<option>지역1</option>
			    			</select>
			    		</td>
			    		<td>
			    			<select>
			    				<option>지역2</option>
			    			</select>
			    		<div class="block-header bg-primary-dark">
			    		</td>

			    		<td>
			    			<input class="form-control" type="text" id="gym-place" name="gym-place" placeholder="경기장 명칭">
			    			<button id="place_search" onclick="place_search()" class="btn btn-primary push-5-r push-10" type="button"><i class="fa fa-search"></i> 검색</button>
			    		</td>
			    	</tr>
			    </tbody>
			</table>
		</div>
		<div class="tbl_style01">
			<table class="table table-striped" >
                <thead>
                    <tr>
                        <th class="text-center" style="width: 50px;">#</th>
                        <th style="width: 25%;">명칭</th>
                        <th>주소</th>
                        <th class="text-center" style="width:100px;">지도</th>
                        <th class="text-center" style="width: 70px;">코트수</th>
                        <th class="text-center" style="width: 100px;">장소선택</th>
                    </tr>
                </thead>
                <tbody id="gym_list">
                </tbody>
            </table>
		</div>
	<button data-remodal-action="cancel" class="remodal-cancel">취소</button>
</div>
<!-- End page content -->

<script>window.jQuery || document.write('<script src="../libs/jquery/dist/jquery.min.js"><\/script>')</script>
<script src="../dist/remodal.js"></script>


<script>
function place_search () {
	var gym_name = document.getElementById('gym-place').value;
	//alert("aaa");
	$.ajax({
		url:'./gym_search.php?gym_name='+gym_name,
		dataType:'json',
		success:function(data){
			console.log(data);
	    	var str = '';
	    	var i = 1;
	    	if(data.length == 0){
				$('#gym_search_error').html('<i class="glyphicon glyphicon-alert text-danger"></i> "지역 검색" 에서 원하시는 시/군/구의 검색결과가 없으면 해당 지역에 대회장이 없습니다.');
	    		$('#gym_list').html("");
	    	}else{
	    		for(var name in data){
					str +=  '<tr><td class="text-center">'+i+'</td>'+
							'<td>'+data[name]['gym_name']+'</td>'+
							'<td>'+data[name]['gym_address3']+'</td>'+
							'<td>'+'<a href="http://dmaps.kr/xz9t" class="btn btn-xs btn-success" type="button" target="_blank"><i class="fa fa-map-marker"></i> 지도보기</a>'+'</td>'+
							'<td class="text-center">'+data[name]['gym_courts']+'</td>'+
							'<td class="text-center"><button class="btn btn-xs btn-default" data-remodal-action="cancel" onclick="place_add('+data[name]['wr_id']+')" data-dismiss="modal" type="button"><i class="fa fa-check"></i> 선택</button></td>';
					i++;
				}
				$('#gym_list').html(str);
				$('#gym_search_error').html('');
	    	}
	    }
	});
}
var i = 1;
function place_add(wr_id) {
	$.ajax({
		url:'./gym_search_id.php?wr_id='+wr_id,
		dataType:'json',
		success:function(data){
			console.log(data['0']);
	    	var str = '';

    		for(var name in data){
    			var cnt = 1;
    			var courts = data['0']['gym_courts'];
str += '<li id="gym_div_'+i+'" ><input type="hidden" name="chk[]" value="'+i+'"></input><input type="hidden" name="wr_id[]" value="'+data['0']['wr_id']+'"></input>'
+'	<p>'
+'		<span>장소명 :</span>'+data['0']['gym_name']+'<a href="">&nbsp;<i class="flaticon-placeholder"></i></a><br/>'
+'		<span>주소 :</span>'+data['0']['gym_address3']+'<br/>'
+'	</p>'
+'	<div class="text-center place_date_add">'
+'		<a style="" id="gym_plus'+i+'" onclick="gym_time_plus('+i+','+data['0']['wr_id']+','+courts+')"   class="btn_default">'
+'			<span class="color5">일정을 등록하지 않으면 경기기간 동안 해당 체육관을 사용하는 것으로 설정됩니다.</span>'
+'			+ 경기일정 추가'
+'		</a>'
+'	</div>'
+'	<div class="match_day">'
+'		<ul>'
+'			<li>'
+'				경기일 : <input class="frm_input text-center" id="application-period1" required name="application-period[]" placeholder="시작일" type="text">'
+'				&nbsp;/&nbsp;사용코트수 : '
+'				<select id="use-court" name="use-court[]">';
				for( cnt = 1 ; cnt <= courts ; cnt++ ){
str +='					<option value="'+cnt+'">'+cnt+'</option>';
				}
str += '				</select>'
+'			</li>'
+'			<li id="gym_plus_place'+i+'">'
+'			</li>'
+'		</ul>'
+'	</div>'
+'	<div class="text-center">'
+'		<a style="" onclick="delete_gym('+i+')" class="btn_default bkg5">삭제</a>'
+'		</div>'
+'	</li>';
				i++;
			}
			$('#place_add_list').append(str);

	    }
	});
}
</script>
<script>
function delete_gym(j){
	$('#gym_div_'+j).remove("");
}

	function gym_time_plus(j ,wr_id,courts){
		var str = '';
		str += '<input type="hidden" name="chk[]" value="'+j+'"></input><input type="hidden" name="wr_id[]" value="'+wr_id+'"></input><div class="">'
+'				경기일 : <input class="frm_input text-center" id="application-period1" required name="application-period[]"  placeholder="시작일" type="text">'
+'				&nbsp;/&nbsp;사용코트수 : '
+'				<select required id="use-court" name="use-court[]">'
				for(var cnt = 1 ; cnt <= courts ; cnt++ ){
str +='					<option value="'+cnt+'">'+cnt+'</option>';
				}
str += '				</select>'
+'				&nbsp;&nbsp;<a onclick="gym_time_minus('+j+')" class="btn_default bkg5" >삭제</a>'
		$('#gym_plus_place'+j).html(str);
}
function gym_time_minus(i){
	$( "#gym_plus_place"+i ).html('<div id="gym_plus_place'+i+'"></div>');
	//$('#gym_plus_place'+j).html(str);
};
</script>
<script>
function championshipStartDay(i){
	var sub = document.getElementById('championship-start-day1'+i).value;

	document.getElementById('championship-start-day2'+i).value = sub;
};
function championshipEndDay(i){
	var sub = document.getElementById('championship-end-day1'+i).value;

	document.getElementById('championship-end-day2'+i).value = sub;
};


</script>


<?php
include_once(G5_SADM_PATH.'/tail.php');
?>
