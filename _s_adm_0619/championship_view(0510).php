<?php
$menu_cate2 ='2';
$menu_cate3 ='2';
include_once('./_common.php');
include_once(G5_SADM_PATH.'/head.php');

$match = sql_fetch("select * from match_data where code = '$code'");

?>
  <link rel="stylesheet" href="../dist/remodal.css">
  <link rel="stylesheet" href="../dist/remodal-default-theme.css">

<!-- sub hd_section-->
	<div class="tit_area">
		<div class="tit_subject">대회관리</div>
		<div class="tit">대회명 출력</div>
	</div>
<!--//sub_hd_area -->
	
<!-- sub_hd_area -->
<div class="tab_hd tab_style01">
    <a class="active" href="">대회안내</a>
    <a class="" href="">접수현황</a>
    <a class="" href="">대진표</a>
    <a class="" href="">대회운영</a>
    <a class="" href="">경기결과</a>
</div>
<div class="sub_hd">
	<div class="l_area">
        <ul>
        	<li><a href="" class="btn_default active">대회안내</a></li>
        	<li><a href="" class="btn_default ">접수현황</a></li>
        	<li><a href="" class="btn_default">대진표</a></li>
        	<li><a href="" class="btn_default">대회원영</a></li>
        	<li><a href="" class="btn_default">경기결과</a></li>
        </ul>
    </div>

	<!-- 게시판 검색 시작 { -->
	<div class="r_area">
		<ul>
			<li></li>
		</ul>
	</div>
	<!-- } 게시판 검색 끝 -->
</div>
<!--//sub_hd_area -->

<!-- !PAGE CONTENT! -->
<div>
	<!-- 대회정보 필수입력-->
	<div class="tbl_style01 tbl_striped tbl_borderd">
		<table>
			<tbody>
				<tr>
					<td width="100">대회종류</td>
					<td></td>
				</tr>
				<tr>
					<td>대회이름</td>
					<td></td>
				</tr>
				<tr>
					<td>대회기간</td>
					<td></td>
				</tr>
				<tr>
					<td>접수기간</td>
					<td></td>
				</tr>
				<tr>
					<td>주최기관</td>
					<td></td>
				</tr>
				<tr>
					<td>주관기관</td>
					<td></td>
				</tr>
				<tr>
					<td>대회지역</td>
					<td></td>
				</tr>
				<tr>
					<td>대회홈페이지</td>
					<td></td>
				</tr>
				<tr>
					<td>대회장소</td>
					<td></td>
				</tr>
			</tbody>
		</table>
	</div>
	<!-- //대회정보 필수입력-->
	
	<div class="btn_area">
		<!-- 수정할때 출력-->
		<a class="btn btn02 delete fw-600" onclick="">수 정</a>
	</div>

</div>




<?php
include_once(G5_SADM_PATH.'/tail.php');
?>