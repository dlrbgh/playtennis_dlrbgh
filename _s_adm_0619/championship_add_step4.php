<?php
$menu_cate2 ='2';
$menu_cate3 ='2';
include_once('./_common.php');
include_once(G5_SADM_PATH.'/_head.php');

?>
<script src="<?php echo G5_SADM_URL?>/assets/js/autosave"></script>
<!-- sub hd_section-->
	<div class="tit_area">
		<div class="tit_subject">대회관리</div>
		<div class="tit">급수별 편성표&nbsp;</div>
		<div class="r-btn-area">
			<ul>
				<li><a href="" class="btn_default">저장하기</a></li>
				<li><a href="" class="btn_default">불러오기</a></li>
			</ul>
		</div>
	</div>
<!--//sub_hd_area -->

<!-- !PAGE CONTENT! -->
<div>
	<div class="bo_cate_area">
	    <!-- 게시판 카테고리 시작 { -->
	    <nav id="tab_group2">
	        <h2>급수별 편성</h2>
	        <ul id="bo_cate_ul">
	            <li><a href="" id="bo_cate_on">전체</a></li>
	            <?php 
                	$sql_s = "select * from match_gym_data inner join gym_data where match_gym_data.match_id='$code' and gym_data.wr_id = match_gym_data.gym_id group by gym_id";
					$result = sql_query($sql_s);
					while($r = sql_fetch_array($result)){
				?>
	            <li><a href=""><?php echo $r['gym_name'];?></a></li>
			    <?php } ?>
	    </nav>
	    <!-- } 게시판 카테고리 끝 -->
	</div>
   	<form name="frmReview" id="frmReview" action="gym_group_arrangement.php" method="post" onsubmit="return true;">
	<div class="tbl_style01 tbl_borderd">
	<p>예선 리그전 배치 </p>
		<table>
			<thead>
				<tr>
					<th width="50">No</th>
					<th>그룹</th>
					<th>장소및 일자</th>
					<th width="100">편집</th>
				</tr>
			</thead>
			<tbody class="text-center">
				 <?php 
               	 	$i = 0;
                	$sql = "select * from group_data where match_code='$code' order by wr_id";
					$gourp_result = sql_query($sql);
				?>
				<?php while($group = sql_fetch_array($gourp_result)){
						$sqls = "select count(wr_id) as cnt from game_score_data where division='{$group[division]}' and match_code='$code' and series = '$group[series]'";
						$count_result = sql_query($sqls);
						$rs = sql_fetch_array($count_result);
					?>
					<tr>
                    	<input type="hidden" name="division[<?php echo $i;?>]" value="<?php echo $group['division']?>" />
                    	<input type="hidden" name="series[<?php echo $i;?>]" value="<?php echo $group['series']?>" />
                    	<input type="hidden" name="series_sub[<?php echo $i;?>]" value="<?php echo $group['series_sub']?>" />
                    	<input type="hidden" name="num[<?php echo $i;?>]" value="<?php echo $group['num']?>" />
                    	<input type="hidden" name="tournament[<?php echo $i;?>]" value="L" />
                    	<input type="hidden" name="group_code[<?php echo $i;?>]" value="<?php echo $group['code']?>" />

                    	<input type="hidden" name="code" value="<?php echo $code?>" />
						<td><?php echo $i;?></td>
						<td>
							<a href=""><?php echo $group['division'];?> <?php echo $group['series'];?> <?php echo $group['series_sub']?> <?php echo $group['num']+1?>조(<?php echo $rs['cnt'];?>경기)</a>
						</td>
						<td>
							<select class="frm_input" id="val-skill" name="val_skill[<?php echo $i;?>]">
								<?php 
				                	$sql_s = "select * from match_gym_data inner join gym_data where match_gym_data.match_id='$code' and gym_data.wr_id = match_gym_data.gym_id order by match_gym_data.wr_id";
									$result = sql_query($sql_s);
									while($r = sql_fetch_array($result)){
								?>
							    <option value="<?=$r['wr_id'];?>|<?=$r['application_period'];?>" <?php if($group['gym_code'] == $r['wr_id'] && $group['game_date'] == $r['application_period']){echo "selected";}?>><?php echo $r['application_period'];?>&nbsp;/&nbsp;<?php echo $r['gym_name'];?></option>
							    <?php } ?>
	                    	</select>
						</td>
						
						<td>
							<input type="submit" class="btn_default" value="저장" />

							<a href="" </a>
						</td>
					</tr>
				<?php $i++; } ?>
				
			</tbody>
		</table>
	</div>

</div>
<div>
	<div class="tbl_style01 tbl_borderd">
		<p>본선 토너먼트 배치</p>
		<table>
			<thead>
				<tr>
					<th width="50">No</th>
					<th>그룹</th>
					<th>토너먼트생성</th>
					<th width="100">편집</th>
				</tr>
			</thead>
			<tbody class="text-center">
				<?php 
                	$sql = "select * from series_data where match_code='$code' order by wr_id";
					$gourp_result = sql_query($sql);
				?>
				<?php while($group = sql_fetch_array($gourp_result)){
						$sqls = "select count(wr_id) as cnt from game_score_data where division='{$group[division]}' and match_code='$code' and series = '$group[series]'";
						$count_result = sql_query($sqls);
						$rs = sql_fetch_array($count_result);
					?>
					<tr>
                    	<input type="hidden" name="division[<?php echo $i;?>]" value="<?php echo $group['division']?>" />
                    	<input type="hidden" name="series[<?php echo $i;?>]" value="<?php echo $group['series']?>" />
                    	<input type="hidden" name="series_sub[<?php echo $i;?>]" value="<?php echo $group['series_sub']?>" />
                    	<input type="hidden" name="num[<?php echo $i;?>]" value="<?php echo $group['num']?>" />
                    	<input type="hidden" name="tournament[<?php echo $i;?>]" value="T" />
                    	<input type="hidden" name="code" value="<?php echo $code?>" />
						<td><?php echo $i;?></td>
						<td>
							<a href=""><?php echo $group['division'];?> <?php echo $group['series'];?> <?php echo $group['series_sub']?> <?php echo $group['num']+1?>조(<?php echo $rs['cnt'];?>경기)</a>
						</td>
						<td>
							<select class="frm_input" id="val-skill" name="val_skill[<?php echo $i;?>]">
								<?php 
				                	$sql_s = "select * from match_gym_data inner join gym_data where match_gym_data.match_id='$code' and gym_data.wr_id = match_gym_data.gym_id order by match_gym_data.wr_id";
									$result = sql_query($sql_s);
									while($r = sql_fetch_array($result)){
								?>
							    <option value="<?=$r['wr_id'];?>|<?=$r['application_period'];?>" <?php if($group['gym_code'] == $r['wr_id'] && $group['game_date'] == $r['application_period']){echo "selected";}?>><?php echo $r['application_period'];?>&nbsp;/&nbsp;<?php echo $r['gym_name'];?></option>
							    <?php } ?>
	                    	</select>
						</td>
						<td>
							<input type="submit" class="btn_default" value="저장" />

							<a href="" </a>
						</td>
					</tr>
				<?php $i++; } ?>
				
			</tbody>
		</table>
	</div>
	
	<div class="btn_area">
		<a class="btn btn01 fw-600" href="javascript:history.go(-1)">뒤 로</a>
		<a class="btn btn01 fw-600" href="<?php echo G5_SADM_URL?>/championship_add_step5.php?code=<?php echo $code;?>">다 음(skip)</a>
		<!-- <a class="btn btn01 fw-600"  onclick="group_arrangement_check();" >다 음</a> -->
		<input type="submit" class="btn btn01 fw-600"  value="다 음" />
	</div>
	</form>


</div>
<!-- End page content -->

<script>
// $("form#frmReview").submit(function(event){       
    // //disable the default form submission
    // event.preventDefault();
    // var fd = new FormData($(this)[0]); 
    // $.ajax({
        // url: "gym_group_arrangement.php",
        // type: "POST",
        // dataType:'json',
        // data: fd,
        // async: false,
        // cache: false,
        // contentType: false,
        // processData: false,
        // success:  function(data){
        	// console.log(data);
			// $('#success_button').click();
	   			// window.location.href = './championship_add_step5.php?code=<?=$code;?>';
        // }
    // });
    // return false;
// });

function place_search (division,series,series_sub) {
	$.ajax({
		url:'./gym_division.php?division='+division+'&series='+series+'&series_sub='+series_sub+'&wr_id=<?php echo $wr_id?>',
		dataType:'json',
		success:function(data){
	    	var str = '';
	    	var i = 1;
			str =  '<div class="block block-themed block-transparent remove-margin-b">'
					+'<div class="block-header bg-primary-dark">'
+'                    <ul class="block-options">'
+'                        <li>'
+'                            <button data-dismiss="modal" type="button"><i class="si si-close"></i></button>'
+'                        </li>'
+'                    </ul>'
+'                    <h3 class="block-title">조별 장소 선택</h3>'
+'                </div>'
+'                <div class="block-content form-horizontal push-10-t push-10">'
+'                	<h4 class="block-title">20A 조별 장소 선택</h4>'
+'                	<div class="row">'
+'                		<div class="col-lg-12">'
+'				            <div class="block">'
+'				                <div class="block-content">'
+'				                    <table class="table ">'
+'				                        <thead>'
+'				                            <tr>'
+'				                                <th class="text-center" style="width: 50px;">#</th>'
+'				                                <th style="">조이름</th>'
+'				                                <th style="">일정 및 장소 선택</th>'
+'				                            </tr>'
+'				                        </thead>'
+'				                        <tbody>';
										for(var name in data){
											console.log(name);
str+='				                            <tr>'
+'				                                <td class="text-center"></td>'
+'				                                <td>'+data[name]['num']+'조</td>'
+'												<td>'
+'													<select class="form-control" id="val-skill" name="val-skill">'
+'					                                    <option value="">선택해 주세요</option>'
						                                    <?php 
				                            	$sql_s = "select * from match_gym_data inner join gym_data where match_gym_data.match_id='$wr_id' and gym_data.wr_id = match_gym_data.gym_id";
												$result = sql_query($sql_s);
															?>
															<?php while($r = sql_fetch_array($result)){?>
+'	<option value=""><?php echo $r['application_period'];?>&nbsp;/&nbsp;<?php echo $r['gym_name'];?></option>'
						                                    <?php } ?>

+'													</select>'
+'												</td>'
+'				                            </tr>';
										}
str+='				                        </tbody>'
+'				                    </table>'
+'				                </div>'
+'				            </div>'
+'        				</div>'
+'                	</div>'
+'            	</div>'
+'            	<div class="modal-footer">'
+'            	    <button class="btn btn-sm btn-default" type="button" data-dismiss="modal">취소</button>'
+'            	    <button class="btn btn-sm btn-primary" type="button" data-dismiss="modal"><i class="fa fa-check"></i> 적용</button>'
+'	            </div>'
+'        	</div>';
			$('#modal_content').html(str);	
			$('#modal-popin').modal('show');
			
	    	}	
	    });
}
function group_arrangement_check () {
	$.ajax({
		url:'./gym_group_arrangement_check.php?code=<?php echo $code;?>',
		dataType:'json',
		success:function(data){
	   		if(data[0]['result'] == "false"){
	   			
	   		}
	   		if(data[0]['result'] == "success"){
	   			window.location.href = './championship_add_step5.php?code=<?=$code;?>';
	   		}
	   	}
    });
}

</script>


<?php
include_once(G5_SADM_PATH.'/_tail.php');
?>
