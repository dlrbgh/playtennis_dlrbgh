<?php
$menu_cate2 ='2';
$menu_cate3 ='2';
include_once('./_common.php');
include_once('./head.sub.php');

include_once (G5_SADM_PATH.'/lib/tennis.common.php');
include_once (G5_AOS_PATH.'/common_functions/Drawer.php');



$team_field = "team_data";

if($division == "단체전"){
	$team_field = "team_event_data";
}

$sql_team = "select count(wr_id) as cnt from $team_field where match_code='$code' and division='$division' and series ='$series' and series_sub = '$series_sub'";
$team_result = sql_query($sql_team);
$r = sql_fetch_array($team_result);

$sql_teams = "select count(wr_id) as cnt from group_data where match_code='$code' and division='$division' and series ='$series' and series_sub = '$series_sub'";
$team_results = sql_query($sql_teams);
$rs = sql_fetch_array($team_results);

$sql_team = "select count(wr_id) as cnt from $team_field where match_code='$code' and division='$division' and series ='$series' and series_sub = '$series_sub'";
$team_result = sql_query($sql_team);
$r = sql_fetch_array($team_result);
		
?>

<link rel="stylesheet" href="<?php echo G5_SADM_URL?>/assets/css/media_print.css" type="text/css" media="print">
<link rel="stylesheet" href="<?php echo G5_SADM_URL?>/assets/css/media_print.css" type="text/css">

<page size="A4">
<!-- !PAGE CONTENT! -->
<div>
	
	<!-- 그룹명  -->
	<div class="sub_hd">
        <div class="tit"><?php echo $division." ".$series." ".$series_sub;?> - <?php echo $r['cnt'];?>명 - <?php echo $rs['cnt'];?>조</div>
    </div>
    <!-- //그룹명  -->


	<div class="champ_match_result_pnt">
		<ul>
			<?php 
	           	$sql = "select * from group_data where match_code='$code' and division='$division' and series='$series' and series_sub='$series_sub' and tournament	='L' order by num";
				$gourp_result = sql_query($sql);
				$i = 0;
				$j = 1;
				
			?>
			
			<?php while($group = sql_fetch_array($gourp_result)){ ?>
			
			
			<li style="min-height: 270px;">				
				<?php $gym_name = sql_fetch("select gym_name from gym_data where wr_id = '{$group['gym_code']}' ", true);  ?>
				<div class="tit_area">
					<span style="font-size:18px"><?=$group['num']+1;?>조<?php echo "-".$gym_name['gym_name']; ?></span>
					<?php //echo $gym_name['gym_name'];  //여기에 코트 및 경기순서 들어갈 자리 

					$team_field = "team_data";
					if($division == "단체전"){
						$team_field = "team_event_data";
					}
										?> 
				</div>

				
				<div class="text-center">
					<div class="current_rating_tbl">
						<table>
	            			<thead>
	            				<tr>
	            				<th width="25%">구분</th>
	            				<?php
	            					if($group['team_1']!=""){
	                					$team_sql = "select * from $team_field where match_code='$code' and team_code='{$group['team_1']}'";
										$team_result = sql_query($team_sql);
										$r = sql_fetch_array($team_result);
									?>
	                				<th width="25%"><?=$r['club'];?></th>
	                				<?php }	?>
	                				<?php
	            					if($group['team_2']!=""){
	                					$team_sql = "select * from $team_field where match_code='$code' and team_code='{$group['team_2']}'";
										$team_result = sql_query($team_sql);
										$r = sql_fetch_array($team_result);
									?>
	                				<th width="25%"><?=$r['club'];?><br/></th>
	                				<?php }	?>
	                				<?php
	            					if($group['team_3']!=""){
	                					$team_sql = "select * from $team_field where match_code='$code' and team_code='{$group['team_3']}'";
										$team_result = sql_query($team_sql);
										$r = sql_fetch_array($team_result);
									?>
	                				<th width="25%"><?=$r['club'];?><br/></th>
	                				<?php }	?>
	                				<?php
	            					if($group['team_4']!=""){
	                					$team_sql = "select * from team_data where match_code = '$code' and team_code = '$group[team_4]'";
										$team_result = sql_query($team_sql);
										$r = sql_fetch_array($team_result);
									?>
	                				<th class="text-center"><?=$r['club'];?><br/></th>
	                				<?php }	?>
	                				<?php
	            					if($group['team_5']!=""){
	                					$team_sql = "select * from team_data where match_code = '$code' and team_code = '$group[team_5]'";
										$team_result = sql_query($team_sql);
										$r = sql_fetch_array($team_result);
									?>
	                				<th class="text-center"><?=$r['club'];?><br/></th>
	                				<?php }	?>
	            				<tr>
	            				</tr>
	            			</thead>
							<tbody class="text-center">
								<?php
                					if($group['team_1']!=""){
                    					$team_sql = "select * from $team_field where match_code = '$code' and team_code = '$group[team_1]'";
										$team_result = sql_query($team_sql);
										$r = sql_fetch_array($team_result);
										
										$score_sql = "select * from game_score_data where match_code = '$code' and tournament = 'L' and (team_1_code = '$group[team_1]' or team_2_code = '$group[team_1]') and (team_1_code = '$group[team_2]' or team_2_code = '$group[team_2]')";
										$score_result = sql_query($score_sql);
										$score1 = sql_fetch_array($score_result);

										$score_sql = "select * from game_score_data where match_code = '$code' and tournament = 'L' and (team_1_code = '$group[team_1]' or team_2_code = '$group[team_1]') and (team_1_code = '$group[team_3]' or team_2_code = '$group[team_3]')";
										$score_result = sql_query($score_sql);
										$score2 = sql_fetch_array($score_result);
										
										if($group['team_4']!=""){
											$score_sql = "select * from game_score_data where match_code = '$code' and tournament = 'L' and (team_1_code = '$group[team_1]' or team_2_code = '$group[team_1]') and (team_1_code = '$group[team_4]' or team_2_code = '$group[team_4]')";
											$score_result = sql_query($score_sql);
											$score3 = sql_fetch_array($score_result);
										}
										if($group['team_5']!=""){
											$score_sql = "select * from game_score_data where match_code = '$code' and tournament = 'L' and (team_1_code = '$group[team_1]' or team_2_code = '$group[team_1]') and (team_1_code = '$group[team_5]' or team_2_code = '$group[team_5]')";
											$score_result = sql_query($score_sql);
											$score4 = sql_fetch_array($score_result);
										}
								?>
                				<tr>
                					<td class="font-w700 teamA_winner"><?=$r['club'];?><br/></td>
                					<td class="text-center table_slash"></td>
                					
                					<?php if($score1['team_1_code'] == $group['team_1']){ ?>
                					<td class="text-center">
                						<h3>
                						<?php if($score1['team_1_score']=='0' and $score1['team_2_score']=='0')
                						{echo '';}else{echo $score1['team_1_score'];}?>
                						&nbsp;:&nbsp;
                						<?php if($score1['team_1_score']=='0' and $score1['team_2_score']=='0')
                						{echo '';}else{echo $score1['team_2_score'];}?>
                						</h3></td>
                					<?php }?>
                					<?php if($score1['team_2_code'] == $group['team_1']){ ?>
										<?php if($score1['team_2_score']=='0' and $score1['team_1_score']=='0')
                						{echo '';}else{echo $score1['team_2_score'];}?>
                						&nbsp;:&nbsp;
                						<?php if($score1['team_2_score']=='0' and $score1['team_1_score']=='0')
                						{echo '';}else{echo $score1['team_1_score'];}?>
                					</h3></td>
                					<?php }?>
                					
                					<?php if($score2['team_1_code'] == $group['team_1']){ ?>
                					<td class="text-center"><h3>:</h3></td>
                					<?php }?>
                					<?php if($score2['team_2_code'] == $group['team_1']){ ?>
                					<td class="text-center"><h3>:</h3></td>
                					<?php }?>
                					
                					<?php if($group['team_4']!=""){ ?>
                    					<?php if($score3['team_1_code'] == $group['team_1']){?>
                    					<td class="text-center"><h3><?=$score3['team_1_score']?>&nbsp;:&nbsp;<?=$score3['team_2_score']?></h3></td>
                    					<?php }?>
                    					<?php if($score3['team_2_code'] == $group['team_1']){?>
                    					<td class="text-center"><h3><?=$score3['team_2_score']?>&nbsp;:&nbsp;<?=$score3['team_1_score']?></h3></td>
                    					<?php }?>
                					<?php }	?>
                					
                					<?php if($group['team_5']!=""){?>
                    					<?php if($score4['team_1_code'] == $group['team_1']){ ?>
                    					<td class="text-center"><h3><?=$score4['team_1_score']?>&nbsp;:&nbsp;<?=$score4['team_2_score']?></h3></td>
                    					<?php }?>
                    					<?php if($score4['team_2_code'] == $group['team_1']){ ?>
                    					<td class="text-center"><h3><?=$score4['team_2_score']?>&nbsp;:&nbsp;<?=$score4['team_1_score']?></h3></td>
                    					<?php }?>
                					<?php }	?>
                				</tr>
                				<?php
									}
                				?>
								<?php
                					if($group['team_2']!=""){
                    					$team_sql = "select * from $team_field where match_code = '$code' and team_code = '$group[team_2]'";
										$team_result = sql_query($team_sql);
										$r = sql_fetch_array($team_result);
										
										$score_sql = "select * from game_score_data where match_code = '$code' and tournament = 'L' and (team_1_code = '$group[team_2]' or team_2_code = '$group[team_2]') and (team_1_code = '$group[team_1]' or team_2_code = '$group[team_1]')";
										$score_result = sql_query($score_sql);
										$score1 = sql_fetch_array($score_result);
										
										$score_sql = "select * from game_score_data where match_code = '$code' and tournament = 'L' and (team_1_code = '$group[team_2]' or team_2_code = '$group[team_2]') and (team_1_code = '$group[team_3]' or team_2_code = '$group[team_3]')";
										$score_result = sql_query($score_sql);
										$score2 = sql_fetch_array($score_result);
										
										if($group['team_4']!=""){
											$score_sql = "select * from game_score_data where match_code = '$code' and tournament = 'L' and (team_1_code = '$group[team_2]' or team_2_code = '$group[team_2]') and (team_1_code = '$group[team_4]' or team_2_code = '$group[team_4]')";
											$score_result = sql_query($score_sql);
											$score3 = sql_fetch_array($score_result);
										}
										if($group['team_5']!=""){
											$score_sql = "select * from game_score_data where match_code = '$code' and tournament = 'L' and (team_1_code = '$group[team_2]' or team_2_code = '$group[team_2]') and (team_1_code = '$group[team_5]' or team_2_code = '$group[team_5]')";
											$score_result = sql_query($score_sql);
											$score4 = sql_fetch_array($score_result);
										}					
								?>
                				<tr>
                					<td class="font-w700 teamA_winner"><?=$r['club'];?><br/></td>
                					<?php if($score1['team_1_code'] == $group['team_2']){?>
                					<td class="text-center"><h3>:</h3></td>
                					<?php }?>
                					<?php if($score1['team_2_code'] == $group['team_2']){?>
                					<td class="text-center"><h3>:</h3></td>
                					<?php }?>
                					
                					<td class="text-center table_slash"></td>
                					
									<?php if($score2['team_1_code'] == $group['team_2']){?>
                					<td class="text-center"><h3>:</h3></td>
                					<?php }?>
                					<?php if($score2['team_2_code'] == $group['team_2']){?>
                					<td class="text-center"><h3>:</h3></td>
                					<?php }?>		                    					
                					
                					
                					<?php if($group['team_4']!=""){?>
                    					<?php if($score3['team_1_code'] == $group['team_2']){?>
                    					<td class="text-center"><h3>:</h3></td>
                    					<?php }?>
                    					<?php if($score3['team_2_code'] == $group['team_2']){?>
                    					<td class="text-center"><h3>:</h3></td>
                    					<?php }?>
                					<?php }	?>
                					
                					<?php if($group['team_5']!=""){?>
                    					<?php if($score4['team_1_code'] == $group['team_2']){?>
                    					<td class="text-center"><h3>:</h3></td>
                    					<?php }?>
                    					<?php if($score4['team_2_code'] == $group['team_2']){?>
                    					<td class="text-center"><h3>:</h3></td>
                    					<?php }?>
                					<?php }	?>
                				</tr>
                				<?php
									}
                				?>
								<?php
                					if($group['team_3']!=""){
                    					$team_sql = "select * from $team_field where match_code = '$code' and team_code = '$group[team_3]'";
										$team_result = sql_query($team_sql);
										$r = sql_fetch_array($team_result);
										
										$score_sql = "select * from game_score_data where match_code = '$code' and tournament = 'L' and (team_1_code = '$group[team_3]' or team_2_code = '$group[team_3]') and (team_1_code = '$group[team_1]' or team_2_code = '$group[team_1]')";
										$score_result = sql_query($score_sql);
										$score1 = sql_fetch_array($score_result);
										 
										$score_sql = "select * from game_score_data where match_code = '$code' and tournament = 'L' and (team_1_code = '$group[team_3]' or team_2_code = '$group[team_3]') and (team_1_code = '$group[team_2]' or team_2_code = '$group[team_2]')";
										$score_result = sql_query($score_sql);
										$score2 = sql_fetch_array($score_result);
										
										if($group['team_4']!=""){
											$score_sql = "select * from game_score_data where match_code = '$code' and tournament = 'L' and (team_1_code = '$group[team_3]' or team_2_code = '$group[team_3]') and (team_1_code = '$group[team_4]' or team_2_code = '$group[team_4]')";
											$score_result = sql_query($score_sql);
											$score3 = sql_fetch_array($score_result);
										}
										if($group['team_5']!=""){
											$score_sql = "select * from game_score_data where match_code = '$code' and tournament = 'L'  and (team_1_code = '$group[team_3]' or team_2_code = '$group[team_3]') and (team_1_code = '$group[team_5]' or team_2_code = '$group[team_5]')";
											$score_result = sql_query($score_sql);
											$score4 = sql_fetch_array($score_result);
										}
								?>
                				<tr>
                					<td class="font-w700 teamA_winner"><?=$r['club'];?><br/></td>
                					
                					<?php if($score1['team_1_code'] == $group['team_3']){?>
                					<td class="text-center"><h3>:</h3></td>
                					<?php }?>
                					<?php if($score1['team_2_code'] == $group['team_3']){?>
                					<td class="text-center"><h3>:</h3></td>
                					<?php }?>
                					
									<?php if($score2['team_1_code'] == $group['team_3']){?>
                					<td class="text-center"><h3>:</h3></td>
                					<?php }?>
                					<?php if($score2['team_2_code'] == $group['team_3']){?>
                					<td class="text-center"><h3>:</h3></td>
                					<?php }?>		                    					
                					<td class="text-center table_slash"></td>
                					<?php if($group['team_4']!=""){?>
                    					<?php if($score3['team_1_code'] == $group['team_3']){?>
                    					<td class="text-center"><h3><?=$score3['team_1_score']?>&nbsp;:&nbsp;<?=$score3['team_2_score']?></h3></td>
                    					<?php }?>
                    					<?php if($score3['team_2_code'] == $group['team_3']){?>
                    					<td class="text-center"><h3><?=$score3['team_2_score']?>&nbsp;:&nbsp;<?=$score3['team_1_score']?></h3></td>
                    					<?php }?>
                					<?php }	?>
                					<?php if($group['team_5']!=""){?>
                    					<?php if($score4['team_1_code'] == $group['team_3']){?>
                    					<td class="text-center"><h3><?=$score4['team_1_score']?>&nbsp;:&nbsp;<?=$score4['team_2_score']?></h3></td>
                    					<?php }?>
                    					<?php if($score4['team_2_code'] == $group['team_3']){?>
                    					<td class="text-center"><h3><?=$score4['team_2_score']?>&nbsp;:&nbsp;<?=$score4['team_1_score']?></h3></td>
                    					<?php }?>
                					<?php }	?>
                				</tr>
                				<?php
									}
                				?>

								<?php
                					if($group['team_4']!=""){
                    					$team_sql = "select * from team_data where match_code = '$code' and team_code = '$group[team_4]'";
										$team_result = sql_query($team_sql);
										$r = sql_fetch_array($team_result);
										
										$score_sql = "select * from game_score_data where match_code = '$code' and tournament = 'L' and (team_1_code = '$group[team_4]' or team_2_code = '$group[team_4]') and (team_1_code = '$group[team_1]' or team_2_code = '$group[team_1]')";
										$score_result = sql_query($score_sql);
										$score1 = sql_fetch_array($score_result);
										
										$score_sql = "select * from game_score_data where match_code = '$code' and tournament = 'L' and (team_1_code = '$group[team_4]' or team_2_code = '$group[team_4]') and (team_1_code = '$group[team_2]' or team_2_code = '$group[team_2]')";
										$score_result = sql_query($score_sql);
										$score2 = sql_fetch_array($score_result);
										
										if($group['team_4']!=""){
											$score_sql = "select * from game_score_data where match_code = '$code' and tournament = 'L' and (team_1_code = '$group[team_4]' or team_2_code = '$group[team_4]') and (team_1_code = '$group[team_3]' or team_2_code = '$group[team_3]')";
											$score_result = sql_query($score_sql);
											$score3 = sql_fetch_array($score_result);
										}
										if($group['team_5']!=""){
											$score_sql = "select * from game_score_data where match_code = '$code' and tournament = 'L' and (team_1_code = '$group[team_4]' or team_2_code = '$group[team_4]') and (team_1_code = '$group[team_5]' or team_2_code = '$group[team_5]')";
											$score_result = sql_query($score_sql);
											$score4 = sql_fetch_array($score_result);
										}
								?>
                				<tr>
                					<td class="font-w700 teamA_winner"><?=$r['club'];?><br/></td>
                					
                					<?php if($score1['team_1_code'] == $group['team_4']){?>
                					<td class="text-center"><h3><?=$score1['team_1_score']?>&nbsp;:&nbsp;<?=$score1['team_2_score']?></h3></td>
                					<?php }?>
                					<?php if($score1['team_2_code'] == $group['team_4']){?>
                					<td class="text-center"><h3><?=$score1['team_2_score']?>&nbsp;:&nbsp;<?=$score1['team_1_score']?></h3></td>
                					<?php }?>
                					
									<?php if($score2['team_1_code'] == $group['team_4']){?>
                					<td class="text-center"><h3><?=$score2['team_1_score']?>&nbsp;:&nbsp;<?=$score2['team_2_score']?></h3></td>
                					<?php }?>
                					<?php if($score2['team_2_code'] == $group['team_4']){?>
                					<td class="text-center"><h3><?=$score2['team_2_score']?>&nbsp;:&nbsp;<?=$score2['team_1_score']?></h3></td>
                					<?php }?>		                    					

									<?php if($score3['team_1_code'] == $group['team_4']){?>
                					<td class="text-center"><h3><?=$score3['team_1_score']?>&nbsp;:&nbsp;<?=$score3['team_2_score']?></h3></td>
                					<?php }?>
                					<?php if($score3['team_2_code'] == $group['team_4']){?>
                					<td class="text-center"><h3><?=$score3['team_2_score']?>&nbsp;:&nbsp;<?=$score3['team_1_score']?></h3></td>
                					<?php }?>		                    					

                					<?php if($group['team_4']!=""){?>
                					<td class="text-center table_slash"></td>
                					<?php }	?>
                					
                					<?php if($group['team_5']!=""){?>
                    					<?php if($score4['team_1_code'] == $group['team_5']){?>
                    					<td class="text-center"><h3><?=$score4['team_1_score']?>&nbsp;:&nbsp;<?=$score4['team_2_score']?></h3></td>
                    					<?php }?>
                    					<?php if($score4['team_2_code'] == $group['team_5']){?>
                    					<td class="text-center"><h3><?=$score4['team_2_score']?>&nbsp;:&nbsp;<?=$score4['team_1_score']?></h3></td>
                    					<?php }?>
                					<?php }	?>
                				</tr>
                				<?php
									}
                				?>
								<?php
                					if($group['team_5']!=""){
                    					$team_sql = "select * from team_data where match_code = '$code' and team_code = '$group[team_5]'";
										$team_result = sql_query($team_sql);
										$r = sql_fetch_array($team_result);
										
										$score_sql = "select * from game_score_data where match_code = '$code' and tournament = 'L' and (team_1_code = '$group[team_5]' or team_2_code = '$group[team_5]') and (team_1_code = '$group[team_1]' or team_2_code = '$group[team_1]')";
										$score_result = sql_query($score_sql);
										$score1 = sql_fetch_array($score_result);
										
										$score_sql = "select * from game_score_data where match_code = '$code' and tournament = 'L' and (team_1_code = '$group[team_5]' or team_2_code = '$group[team_5]') and (team_1_code = '$group[team_2]' or team_2_code = '$group[team_2]')";
										$score_result = sql_query($score_sql);
										$score2 = sql_fetch_array($score_result);
										
										if($group['team_4']!=""){
											$score_sql = "select * from game_score_data where match_code = '$code' and tournament = 'L' and (team_1_code = '$group[team_5]' or team_2_code = '$group[team_5]') and (team_1_code = '$group[team_3]' or team_2_code = '$group[team_3]')";
											$score_result = sql_query($score_sql);
											$score3 = sql_fetch_array($score_result);
										}
										if($group['team_5']!=""){
											$score_sql = "select * from game_score_data where match_code = '$code' and tournament = 'L' and (team_1_code = '$group[team_5]' or team_2_code = '$group[team_5]') and (team_1_code = '$group[team_4]' or team_2_code = '$group[team_4]')";
											$score_result = sql_query($score_sql);
											$score4 = sql_fetch_array($score_result);
										}
								?>
                				<tr>
                					<td class="font-w700 teamA_winner"><?=$r['club'];?><br/><span class="font-s08"><?=$r['team_1_name'];?>, <?=$r['team_2_name'];?></span></td>		                    					
                					<?php if($score1['team_1_code'] == $group['team_5']){?>
                					<td class="text-center"><h3><?=$score1['team_1_score']?>&nbsp;:&nbsp;<?=$score1['team_2_score']?></h3></td>
                					<?php }?>
                					<?php if($score1['team_2_code'] == $group['team_5']){?>
                					<td class="text-center"><h3><?=$score1['team_2_score']?>&nbsp;:&nbsp;<?=$score1['team_1_score']?></h3></td>
                					<?php }?>
                					
									<?php if($score2['team_1_code'] == $group['team_5']){?>
                					<td class="text-center"><h3><?=$score2['team_1_score']?>&nbsp;:&nbsp;<?=$score2['team_2_score']?></h3></td>
                					<?php }?>
                					<?php if($score2['team_2_code'] == $group['team_5']){?>
                					<td class="text-center"><h3><?=$score2['team_2_score']?>&nbsp;:&nbsp;<?=$score2['team_1_score']?></h3></td>
                					<?php }?>		    
                					      
									<?php if($score3['team_1_code'] == $group['team_5']){?>
                					<td class="text-center"><h3><?=$score3['team_1_score']?>&nbsp;:&nbsp;<?=$score3['team_2_score']?></h3></td>
                					<?php }?>
                					<?php if($score3['team_2_code'] == $group['team_5']){?>
                					<td class="text-center"><h3><?=$score3['team_2_score']?>&nbsp;:&nbsp;<?=$score3['team_1_score']?></h3></td>
                					<?php }?>		          

                					<?php if($group['team_4']!=""){?>
                    					<?php if($score4['team_1_code'] == $group['team_5']){?>
                    					<td class="text-center"><h3><?=$score4['team_1_score']?>&nbsp;:&nbsp;<?=$score4['team_2_score']?></h3></td>
                    					<?php }?>
                    					<?php if($score4['team_2_code'] == $group['team_5']){?>
                    					<td class="text-center"><h3><?=$score4['team_2_score']?>&nbsp;:&nbsp;<?=$score4['team_1_score']?></h3></td>
                    					<?php }?>
                					<?php }	?>
                					
                					<?php if($group['team_5']!=""){?>
                					<td class="text-center table_slash"></td>
                					<?php }	?>
                					

                				</tr>
                				<?php
									}
                				?>
							</tbody>
	                	</table>
	                </div>
	                
				</div>
			</li>
			<?php $cnt++;}  ?>
		</ul>	
		
	</div>


</div>
<!-- End page content -->
</page>