<?php
$menu_cate2 ='2';
$menu_cate3 ='2';
include_once('./_common.php');
include_once(G5_SADM_PATH.'/_head.php');

?>
<script src="<?php echo G5_SADM_URL?>/assets/js/autosave"></script>
<!-- sub hd_section-->
	<div class="tit_area">
		<div class="tit_subject"><?php echo $menu_title; ?>(메인 카테고리영역)</div>
		<div class="tit">경기상황별 자세히보는 화면&nbsp;</div>
		
	</div>
<!--//sub_hd_area -->

<!-- !PAGE CONTENT! -->
<div>

	<div class="bo_cate_area">
	    <!-- 게시판 카테고리 시작 { -->
	    <nav id="tab_group2">
	        <h2>급수별 편성</h2>
	        <ul id="bo_cate_ul">
	            <li><a href="" id="bo_cate_on">전체</a></li>
	            <li><a href="">남성</a></li>
	            <li><a href="">여성</a></li>
	            <li><a href="">혼합</a></li>
	    </nav>
	    <!-- } 게시판 카테고리 끝 -->
	</div>

	<div class="tbl_style01 tbl_borderd">
		<table>
			<thead>
				<tr>
					<th width="50">No</th>
					<th>그룹</th>
					<th>등급명</th>
					<th>팀수</th>
					<th>배정현황</th>
					<th></th>
				</tr>
			</thead>
			<tbody class="text-center">
				<?php 
                	$i = 0;
                	$sql = "select * from series_data where match_code='$code' GROUP BY division,series,series_sub";
					$gourp_result = sql_query($sql);
				?>
				<?php while($group = sql_fetch_array($gourp_result)){?>
                    <tr>
						<td>1</td>
						<td>
							<a href="">개인전 - <?php echo $group['series'];?> - <?php echo $group['series_sub'];?>(30팀)</a>
						</td>
						<td>챌린저</td>
						<?php
		                	$sql_team = "select count(wr_id) as cnt from team_data where match_code='$code' and division='$group[division]' and series ='$group[series]' and series_sub = '$group[series_sub]'";
							$team_result = sql_query($sql_team);
							$r = sql_fetch_array($team_result);
							
		                	$sql_teams = "select count(wr_id) as cnt from team_data where match_code='$code' and division='$group[division]' and series ='$group[series]' and series_sub = '$group[series_sub]' and group_assign	= '0'";
							$team_results = sql_query($sql_teams);
							$rs = sql_fetch_array($team_results);
		                ?>
						<td>
							<?=$r[cnt];?>팀
						</td>
						<td>
							배정(29팀) / <font class="color5">미배정(1팀)</font>
						</td>
						<td>
							<a href="currentstatus_rating_view.php?division=<?php echo $group['division'];?>&series=<?php echo $group['series'];?>&series_sub=<?php echo $group['series_sub'];?>&code=<?php echo $group['match_code'];?>&gym_id=<?php echo $group['gym_id'];?>&application_period=<?php echo $group['application_period'];?>" class="btn_default">보기</a>
						</td>
					</tr>
                    	
                 <?php } ?>
			</tbody>
		</table>
	</div>
	

	<div class="btn_area">
		<a class="btn btn01 fw-600" href="javascript:history.go(-1)">뒤 로</a>
	</div>

</div>
<!-- End page content -->



<?php
include_once(G5_SADM_PATH.'/_tail.php');
?>
