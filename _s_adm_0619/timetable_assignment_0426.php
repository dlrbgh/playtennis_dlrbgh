<?php
$menu_cate2 ='2';
$menu_cate3 ='2';
include_once('./_common.php');
include_once(G5_PATH.'/_head.php');

?>
<script src="<?php echo G5_SADM_URL?>/assets/js/autosave"></script>
<!-- sub hd_section-->
	<div class="tit_area">
		<div class="tit_subject"><?php echo $menu_title; ?>(메인 카테고리영역)</div>
		<div class="tit">대진시간표</div>
		<div class="r-btn-area">
			<ul>
				<li><a href="" class="btn_default">저장하기</a></li>
				<li><a href="" class="btn_default">불러오기</a></li>
			</ul>
		</div>
	</div>
<!--//sub_hd_area -->

<!-- !PAGE CONTENT! -->
<div>

	<div class="bo_cate_area">
	    <!-- 게시판 카테고리 시작 { -->
	    <nav id="tab_group2">
	        <h2>급수별 편성</h2>
	        <ul id="bo_cate_ul">
	            <li><a href="" id="bo_cate_on">춘천호반체육관 테니스장1</a></li>
	            <li><a href="">춘천호반체육관 테니스장1</a></li>
	            <li><a href="">춘천호반체육관 테니스장1</a></li>
	            <li><a href="">춘천호반체육관 테니스장1</a></li>
	    </nav>
	    <!-- } 게시판 카테고리 끝 -->
	</div>

	<div>
		<div class="inline-tab relative">
			<ul class="tab_pd">
				<li class=""><a href="farmer_management_view.php">2017-04-26</a></li>
				<li class="active"><a href="farmer_visit_history.php">2017-04-27</a></li>
			</ul>
			<div class="r-btn-area">
				<!--<a href="" class="btn_b02" style="">수정</a>-->
			</div>
		</div>
		
		<div class="champ_edit">
			
			<div style="">
				<div class="chmap_time_count">
					<ul>
						<li>1경기<br/>asd</li>
						<li>2경기</li>
						<li>3경기</li>
						<li>4경기</li>
					</ul>
				</div>
				<!-- full_court_container -->
				<div class="table-time"  style="width:933px"><!-- 155px*칼럼수 값 + 3px-->
					<ul class="match_court">
						<li>1코트</li>
						<li>1코트</li>
						<li>1코트</li>
						<li>1코트</li>
						<li>1코트</li>
						<li>1코트</li>
					</ul>
					<ul class="team_edit_area">
						
						<!-- 숫자 court  -->
						<li>
							<ul>
								<!-- 0court 0game-->
								<li>
									<div class="btn-group text-center group_area male_play " role="group"> 
									<a class="dropdown-toggle text-black" data-toggle="dropdown" aria-expanded="false" href="">												
										<div class="team_play_bg">
											개나리부 1조
											<div class="champ_code">
												<span>#A0017</span>
												<!-- 경기중일때 표시 -->
												<i class="fa fa-circle text-red playing"></i>									
											</div>
										</div>
										<!-- 경기가 종료되었을때 class 추가 end_matchA , end_matchB -->
										<div class="club_area ">
											<div class="team_area">
												<div class="club_name">백령테니스/백령테니스</div>
												<div class="name">용환영/박광동</div>
											</div>
											<div class="scoreboard_area">
												<div class="win_score">
												</div>
											</div>
											<!--<div class="scoreboard_area">
												<div class="win_score">
													<div id="score_session_18371">
														<div class="a_score">31</div>
														<div class="b_score">0</div>
													</div>
												</div>
											</div>
											-->
											<div class="team_area">
												<div class="club_name">백령테니스/백령테니스</div>
												<div class="name">용환영/박광동</div>
											</div>
											
										</div>
									</a>
									</div>
								</li>
								<!-- // 0 court 0game-->
								
							</ul>
						</li>
						<!-- //숫자 court  -->
						
					</ul>
				</div>	
				<!-- //full_court_container -->
			</div>
		</div>
		
		
	</div>
	

	<div class="btn_area">
		<a class="btn btn01 fw-600" href="javascript:history.go(-1)">뒤 로</a>
		<a class="btn btn01 fw-600" href="<?php echo G5_SADM_URL?>/championship_add_step3.php">대진표 생성</a>
	</div>

</div>
<!-- End page content -->



<?php
include_once(G5_PATH.'/_tail.php');
?>
