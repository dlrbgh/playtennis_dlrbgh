<?php
$menu_cate2 ='2';
$menu_cate3 ='2';
include_once('./_common.php');
include_once(G5_SADM_PATH.'/_head.php');

?>
<script src="<?php echo G5_SADM_URL?>/assets/js/autosave"></script>
<!-- sub hd_section-->
	<div class="tit_area">
		<div class="tit_subject">대회관리</div>
		<div class="tit">접수 현황 관리 Step.4&nbsp;</div>
		<div class="r-btn-area">
			<ul>
				<li><a href="" class="btn_default">저장하기</a></li>
				<li><a href="" class="btn_default">불러오기</a></li>
			</ul>
		</div>
	</div>
<!--//sub_hd_area -->

<!-- !PAGE CONTENT! -->
<?php
$total = sql_fetch("select count(*) as cnt from team_data where match_code = '$code'");
$result = sql_query("select count(*) as cnt,series,division,series_sub from team_data where match_code = '$code' group by division,series,series_sub");
$results = sql_fetch("select count(*) as cnt from team_event_data where match_code = '$code'");
$resulted = sql_query("select count(*) as cnt,series,division,series_sub from team_event_data where match_code = '$code' group by division,series,series_sub");
?>
<div>
	<!-- 접수현황  -->
	<div class="sub_hd reception_area" id="excel_list">
        <div class="reception_count">총 접수 인원 : <strong><?php echo $total['cnt']+$results['cnt'];?></strong> 팀</div>
        <div class="recep_status">
        	<div class="recep_status_group">
        		<div class="group_tit">개인전</div>
	        	<div class="group_info">
		        	<!--<div class="info_tit">ㅇㅇ팀 ㅇ명 </div>-->
		        	<ul>
			        <?php 
		        		while($gruop_total = sql_fetch_array($result)){
		        	?>
	        			<li><a href=""><?php echo $gruop_total['division']?> <?php echo $gruop_total['series']?> <?php echo $gruop_total['series_sub']?>(<?php echo $gruop_total['cnt']?>팀)</a></li>
			        <?php		
		        		}
		        	?>
	        		</ul>
	        	</div>
	        	
	        	<div class="group_tit">단체전</div>
	        	<div class="group_info">
		        	<!--<div class="info_tit"><?php echo $gruop_total['division']?> <?php echo $gruop_total['series']?> <?php echo $gruop_total['series_sub']?>팀 <?php echo $gruop_total['cnt']?>명</div>-->
		        	<ul>
	        	<?php 
	        		while($gruop_total = sql_fetch_array($resulted)){
	        	?>
	        		<li><a href=""><?php echo $gruop_total['division']?> <?php echo $gruop_total['series']?> <?php echo $gruop_total['series_sub']?>(<?php echo $gruop_total['cnt']?>팀)</a></li>
		        <?php		
	        		}
	        	?>
	        	</ul>
	        	</div>
				
				<div class="color5" style="font-size:30px">SAMPLE</div>
				<div class="group_tit">단체전</div>
	        	<div class="group_info">
		        	<div class="info_tit">남자 00팀 00명</div>
		        	<ul>
		        		<li><a href="">어르신 00팀(00명)</a></li>
		        		<li><a href="">어르신 00팀(00명)</a></li>
		        		<li><a href="">어르신 00팀(00명)</a></li>
		        	</ul>
	        	</div>
	        	<div class="group_info">
		        	<div class="info_tit">여자 00팀 00명</div>
		        	<ul>
		        		<li><a href="">어르신 00팀(00명)</a></li>
		        		<li><a href="">어르신 00팀(00명)</a></li>
		        		<li><a href="">어르신 00팀(00명)</a></li>
		        		<li><a href="">어르신 00팀(00명)</a></li>
		        	</ul>
	        	</div>
        	</div>
        	<!--
        	<div class="recep_status_group">
        		<div class="group_tit">개인전</div>
	        	
	        	<div class="group_info">
		        	<div class="info_tit">남자 00팀 00명</div>
		        	<ul>
		        		<li><a href="">어르신 00팀(00명)</a></li>
		        		<li><a href="">어르신 00팀(00명)</a></li>
		        		<li><a href="">어르신 00팀(00명)</a></li>
		        		<li><a href="">어르신 00팀(00명)</a></li>
		        	</ul>
	        	</div>
	        	<div class="group_info">
		        	<div class="info_tit">남자 00팀 00명</div>
		        	<ul>
		        		<li><a href="">어르신 00팀(00명)</a></li>
		        		<li><a href="">어르신 00팀(00명)</a></li>
		        		<li><a href="">어르신 00팀(00명)</a></li>
		        		<li><a href="">어르신 00팀(00명)</a></li>
		        	</ul>
	        	</div>
        	</div>-->
        	
        	
        </div>
        
	</div>
	<!-- 접수현황  -->
	<div class="sub_hd champ champ_upload_area">
		<form method="post" name="frmReview1" action="step1.php" id="frmReview1" enctype="multipart/form-data">
	        <div class="col-lg-6">
	            <!-- DropzoneJS -->
				<div class="tit" style="font-size:15px;margin-bottom:5px">첨부파일 등록</div>
	            <div class="input-group">
	            	<input type="hidden" name="wr_id" value="<?php echo $wr_id?>" />
	            	<input type="hidden" name="code" value="<?php echo $code?>" />
	                <input type="file" id="bf_file" name="bf_file[]" class="form-control">
	                <span class="input-group-btn">
	                    <button class="btn btn-default" type="submit" id="submitBtn">엑셀파일 업로드 하기</button>
	                </span>
	                <!--<form class="dropzone" action="base_forms_pickers_more.php"></form>-->
	            </div>
	            <!-- END DropzoneJS -->
	        </div>
        </form>
		<!-- 업로드 결과물 -->
		<div class="upload_result">
			업로드 파일명 출력 / 업로드 시간출력 2017-04-25 13:00 
		</div>
		<!-- //업로드 결과물 -->
		
		<div class="warning_info">
        	<h2 class="tit">주의사항</h2>
        	<ul>
        		<li>참가자 업로드 시 기존에 있던 엑셀 데이터는 자동으로 덮어쓰기 됩니다.</li>
        		<li>대진표 생성 단계로 넘어가면, 참가자를 수동으로 입력하거나, 참가자 입력 단계로 돌아와 처음부터 진행해야 합니다. <span class="color5 fw-600">주의하시기</span> 바랍니다.</li>
        	</ul>
        </div>
	</div>
	<!-- 접수현황 DB-->
	<div class="tbl_style01 tbl_borderd">
		<table>
			<thead>
				<tr>
					<th>No</th>
					<th>경기구분</th>
					<th>성별</th>
					<th>참가부</th>
					<th>지역</th>
					<th>소속클럽</th>
					<th>이름</th>
					<th>등급</th>
					<th>연령</th>
					<th>확인일</th>
					<th>입금확인</th>	
				</tr>
			</thead>
			<tbody class="text-center">
				<?php
					$i = 1;
					$result = sql_query("select * from team_data where match_code = '$code'");
					while($r = sql_fetch_array($result)){
				?>
				<tr>
					<td><?php echo $i;?></td>
					<td><a href=""><?php echo $r['division']?></a></td>
					<td><a href=""><?php echo $r['series']?></a></td>
					<td><a href=""><?php echo $r['series_sub']?></a></td>
					<td><a href=""><?php echo $r['area_1']?>-<?php echo $r['area_2']?></a></td>
					<td><a href=""><?php echo $r['club']?></a></td>
					<td><?php echo $r['team_1_name']?></td>
					<td><!-- <a href="">6등급</a> --></td>
					<td><!-- 34세 --></td>
					<td>
						<!-- <ul>
							<li>상태 로그 영역</li>
							<li>2017-04-23:11:30 <font class="color5">입금</font></li>
							<li>2017-04-23:11:30 <font class="color5">취소</font></li>
							
						</ul> -->
					</td>
								        
					<td>
						 <!-- <div class="dropdown">
						  <button onclick="statusChange()" class="dropbtn">현재상태출력</button>
						  <div id="statusChange" class="dropdown-content">
						    <a href="#">접수중</a>
						    <a href="#">입금대기</a>
						    <a href="#">입금완료</a>
						    <a href="#">취소</a>
						  </div>
						</div> -->
					</td>
				</tr>
				<?php		
				$i++;
					}
				?>
				<?php
					$result = sql_query("select * from team_event_data where match_code = '$code'");
					while($r = sql_fetch_array($result)){
				?>
				<tr>
					<td><?php echo $i;?></td>
					<td><a href=""><?php echo $r['division']?></a></td>
					<td><a href=""><?php echo $r['series']?></a></td>
					<td><a href=""><?php echo $r['series_sub']?></a></td>
					<td><a href=""><?php echo $r['area_1']?>-<?php echo $r['area_2']?></a></td>
					<td><a href=""><?php echo $r['club']?></a></td>
					<td><?php echo $r['team_1_name']?></td>
					<td><!-- <a href="">6등급</a> --></td>
					<td><!-- 34세 --></td>
					<td>
						<!-- <ul>
							<li>상태 로그 영역</li>
							<li>2017-04-23:11:30 <font class="color5">입금</font></li>
							<li>2017-04-23:11:30 <font class="color5">취소</font></li>
							
						</ul> -->
					</td>
								        
					<td>
						 <!-- <div class="dropdown">
						  <button onclick="statusChange()" class="dropbtn">현재상태출력</button>
						  <div id="statusChange" class="dropdown-content">
						    <a href="#">접수중</a>
						    <a href="#">입금대기</a>
						    <a href="#">입금완료</a>
						    <a href="#">취소</a>
						  </div>
						</div> -->
					</td>
				</tr>
				<?php		
				$i++;
					}
				?>
			</tbody>
		</table>
	</div>
	<!-- //접수현황 DB-->

	<div class="btn_area">
		<!-- <a class="btn btn01 fw-600" href="javascript:history.go(-1)">뒤 로</a> -->
		<a class="btn btn01 fw-600" href="<?php echo G5_SADM_URL?>/championship_add_step2.php?code=<?php echo $code;?>">뒤 로</a>
		<a class="btn btn01 fw-600" href="<?php echo G5_SADM_URL?>/championship_add_step4.php?code=<?php echo $code;?>">다 음(skip)</a>
		<a class="btn btn01 fw-600" href="<?php echo G5_SADM_URL?>/step2.php?code=<?=$code?>">다 음</a>
	</div>

</div>
<!-- End page content -->
<style>
 /* Dropdown Button */
.dropbtn {
	background-color: #fff;
    color: #000;
    padding: 5px;
    border: 1px solid #ccc;
    cursor: pointer;
	background:url('../img/common/svg/down-arrow.svg')no-repeat 8px center;background-size:14px ;
	padding-left:22px;
	
}

/* Dropdown button on hover & focus */
.dropbtn:hover, .dropbtn:focus {
    border-color: #14264a;
	background-color: #fafafa;
}

/* The container <div> - needed to position the dropdown content */
.dropdown {
    position: relative;
    display: inline-block;
    
}

/* Dropdown Content (Hidden by Default) */
.dropdown-content {
    display: none;
    position: absolute;
    background-color: #f9f9f9;
    min-width: 160px;
    box-shadow: 0px 8px 16px 0px rgba(0,0,0,0.2);
    z-index: 1;
}

/* Links inside the dropdown */
.dropdown-content a {
    color: black;
    padding: 5px 5px;
    text-decoration: none;
    display: block;
    border-bottom:1px solid #ccc;
    position:relative;
    border-top:1px solid #fff;
}
.dropdown-content a:first-child {border-top:2px solid #ccc}

/* Change color of dropdown links on hover */
.dropdown-content a:hover {background-color: #f1f1f1}

/* Show the dropdown menu (use JS to add this class to the .dropdown-content container when the user clicks on the dropdown button) */
.show {display:block;}

</style>
<script>
	/* When the user clicks on the button,
toggle between hiding and showing the dropdown content */
function statusChange() {
    document.getElementById("statusChange").classList.toggle("show");
}

// Close the dropdown menu if the user clicks outside of it
window.onclick = function(event) {
  if (!event.target.matches('.dropbtn')) {

    var dropdowns = document.getElementsByClassName("dropdown-content");
    var i;
    for (i = 0; i < dropdowns.length; i++) {
      var openDropdown = dropdowns[i];
      if (openDropdown.classList.contains('show')) {
        openDropdown.classList.remove('show');
      }
    }
  }
}

</script>
<script>
$("form#frmReview").submit(function(event){       
    //disable the default form submission
    event.preventDefault();
    var fd = new FormData($(this)[0]); 
    console.log($(this)[0]);
    $.ajax({
        url: "step1.php",
        type: "POST",
        dataType:'json',
        data: fd,
        async: true,
        cache: false,
        contentType: false,
        processData: false,
        success:  function(data){
        	console.log(data);
        	var str = '';
    		for(var name in data){
    			str ='<tr>'+'<td class="font-w600">'+data['0']['file_name']+'</td>'
    			+'<td class="text-center">'+data['0']['count']+'(팀)</td>'
 				+'<td class="text-center">'+data['0']['datetime']+'</td>'                
 				+'<td class="text-center"><div class="btn-group"><button class="btn btn-xs btn-default" type="button" data-toggle="tooltip" title="Remove"><i class="fa fa-times"></i></button></div></td>'                
				+'</tr>';
    		}
    		$('#excel_list').append(str);	
			$('#success_button').click();
            //alert(data);
        /* alert(data); if json obj. alert(JSON.stringify(data));*/
        }
    });
    return false;

 });

</script>

<?php
include_once(G5_SADM_PATH.'/_tail.php');
?>
