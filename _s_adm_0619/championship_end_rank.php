<?php
$menu_cate2 ='2';
$menu_cate3 ='2';
include_once('./_common.php');
include_once(G5_SADM_PATH.'/_head.php');

?>
<script src="<?php echo G5_SADM_URL?>/assets/js/autosave"></script>
<!-- sub hd_section-->
	<div class="tit_area">
		<div class="tit_subject">대회관리</div>
		<div class="tit">경기명 출력 - 랭킹정보</div>
		<div class="r-btn-area">
			<ul>
				<li><a href="" class="btn_default">대진 상황표 이동</a></li>
				<li><a href="" class="btn_default">대진 상황표 이동</a></li>
				<li><a href="" class="btn_default">대진 상황표 이동</a></li>
				<li><a href="" class="btn_default">대진 상황표 이동</a></li>
			</ul>
		</div>
	</div>
<!--//sub_hd_area -->

<!-- !PAGE CONTENT! -->
<div>

	<div class="bo_cate_area">
	    <!-- 게시판 카테고리 시작 { -->
	    <nav id="tab_group2">
	        <h2>급수별 편성</h2>
	        <ul id="bo_cate_ul">
	            <li><a href="" id="bo_cate_on">전체</a></li>
	            <li><a href="">남성</a></li>
	            <li><a href="">여성</a></li>
	            <li><a href="">혼합</a></li>
	    	</ul>
	    </nav>
	    <!-- } 게시판 카테고리 끝 -->
	</div>	
	<!-- 그룹명  -->
	<div class="sub_hd">
        <div class="tit">시상표 그림 위치</div>
    </div>
    <!-- //그룹명  -->

	<div class="champ_rank_result tbl_striped">
	
		<table>
			<thead>
				<tr>
					<th>등급</th>
					<th>1위</th>
					<th>2위</th>
					<th>3위</th>
					<th>상품권</th>
				</tr>
			</thead>
			<tbody class="text-center">
				<tr>
					<td>개나리부</td>
					<td>
						부곡하와이 - 이우선<br/>
						부곡당나라 - 이상훈
					</td>
					<td>
						부곡하와이 - 이우선<br/>
						부곡당나라 - 이상훈
					</td>
					
					<td>
						부곡하와이 - 이우선<br/>
						부곡당나라 - 이상훈
					</td>
					<td><a href="" class="btn_default">출력</a></td>
				</tr>
				<tr>
					<td>개나리부</td>
					<td>
						부곡하와이 - 이우선<br/>
						부곡당나라 - 이상훈
					</td>
					<td>
						부곡하와이 - 이우선<br/>
						부곡당나라 - 이상훈
					</td>
					
					<td>
						부곡하와이 - 이우선<br/>
						부곡당나라 - 이상훈
					</td>
					<td><a href="" class="btn_default">출력</a></td>
				</tr>
			</tbody>
		</table>
		
	</div>
	

	<div class="btn_area">
		<a class="btn btn01 fw-600" href="javascript:history.go(-1)">뒤 로</a>
		<a class="btn btn01 fw-600" href="<?php echo G5_SADM_URL?>/championship_end_rank.php">대회 종료</a>
	</div>

</div>
<!-- End page content -->



<?php
include_once(G5_SADM_PATH.'/_tail.php');
?>
