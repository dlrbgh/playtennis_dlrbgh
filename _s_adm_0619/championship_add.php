<?php
$menu_cate2 ='2';
$menu_cate3 ='2';
include_once('./_common.php');
include_once(G5_SADM_PATH.'/head.php');

$match = sql_fetch("select * from match_data where code = '$code'");

?>
  <link rel="stylesheet" href="../dist/remodal.css">
  <link rel="stylesheet" href="../dist/remodal-default-theme.css">

<!-- sub hd_section-->
	<div class="tit_area">
		<div class="tit_subject">대회관리</div>
		<div class="tit">Step.1 : 중계대회 등록</div>
		
	</div>
<!--//sub_hd_area -->

<!-- !PAGE CONTENT! -->
<div>
	<!-- 대회정보 필수입력-->
	<form action="championship_update.php" method="post" onsubmit="return true;" enctype="multipart/form-data">
	<input type="hidden" name="divison" value="2" />
	<input type="hidden" name="match_code" value="<?=$code?>" />
	
	<div class="tbl_frm01">
		<table>
			<tbody>
				<tr>
					<th>대회이름</th>
					<td><input value="<?=$match['wr_name'];?>" name="championship_name"  id="" required="" class="frm_input required" size="50" maxlength="255" type="text">
						<br/><input value="<?=$match['sh_name'];?>" name="sh_name"  id="sh_name" required="" class="frm_input required" size="20" maxlength="50" type="text" placeholder="약칭명 입력(최대 8자)">
					</td>
					<th>대회종류</th>
					<td>
						
						<select class="" id="" name="championship-scale" >
                            <option value="">선택</option>
                            <option value="1" <?php if($match['scale'] == 1) echo "selected";?>>KTA</option>
                            <option value="2" <?php if($match['scale'] == 2) echo "selected";?>>KATO</option>
                            <option value="3" <?php if($match['scale'] == 3) echo "selected";?>>KATA</option>
                            <option value="4" <?php if($match['scale'] == 4) echo "selected";?>>KASTA</option>
                            <option value="5" <?php if($match['scale'] == 5) echo "selected";?>>LOCAL</option>
                            <option value="6" <?php if($match['scale'] == 6) echo "selected";?>>비랭킹</option>
                        </select>
					</td>
				</tr>
				
				<tr>
					<th>대회기간</th>
					<td>
						<div class="input-daterange" data-date-format="yyyy-mm-dd">
                            <input class="frm_input text-center required" required="" value="<?=$match['date1'];?>" id="convention-date1" name="convention-date1" placeholder="시작일" type="text">
                            <span class="input-group-addon"><i class="flaticon-right-arrow"></i></span>
                            <input class="frm_input text-center required" required="" value="<?=$match['date2'];?>" id="convention-date2" name="convention-date2" placeholder="종료일" type="text">
                        </div>
					</td>
					
					<th>접수기간</th>
					<td>
						<div class="input-daterange" data-date-format="yyyy-mm-dd">
                            <input class="frm_input text-center" value="<?=$match['period1'];?>" id="application-period1" name="application-period1" placeholder="시작일" type="text">
                            <span class="input-group-addon"><i class="flaticon-right-arrow"></i></span>
                            <input class="frm_input text-center" value="<?=$match['period2'];?>" id="application-period2" name="application-period2" placeholder="종료일" type="text">
                        </div>
                    </td>
					
				</tr>
				
				<tr>
					<th>주최기관</th>
					<td><input  value="<?=$match['organizer'];?>" id="championship-organizer" name="championship-organizer" required="" class="frm_input required" size="50" maxlength="255" type="text"></td>
					<th>주관기관</th>
					<td><input value="<?=$match['supervision'];?>" id="championship-supervision" name="championship-supervision" class="frm_input " size="50" maxlength="255" type="text"></td>
				</tr>

				<tr>
					<th>대회지역1</th>
					<td><input  value="<?=$match['area_1'];?>" id="area_1" name="area_1" required="" class="frm_input" size="10" maxlength="50" type="text"></td>
					<th>대회 홈페이지</th>
					<td><input name="hompage" value="<?=$match['homepage'];?>" id="hompage" required="" class="frm_input required" size="50" maxlength="255" type="text"></td>
				</tr>
				<tr>
					<th>대회지역2</th>
					<td><input  value="<?=$match['area_2'];?>" id="area_2" name="area_2" class="frm_input " size="10" maxlength="50" type="text"></td>
					<th>대회 장소</th>
					<td><input value="<?=$match['place'];?>" id="championship_place" name="championship_place" required="" class="frm_input required" size="50" maxlength="255" type="text"></td>
				</tr>
				<tr>
					<th>추가정보</th>
					<td colspan="3"><textarea name="text1" class="txt_area"><?php echo $match['text1']?></textarea></td>
				</tr>
				
				<!--
				<tr>
					<th>후원</th>
					<td><input value="<?=$match['support'];?>" id="championship-support" name="championship-support" class="frm_input required" size="50" maxlength="255" type="text"></td>
					<th>협찬</th>
					<td><input value="<?=$match['sponsor'];?>" id="championship-sponsor" name="championship-sponsor" class="frm_input required" size="50" maxlength="255" type="text"></td>
				</tr>
				
				<tr>
					<th>개회식 일시</th>
					<td>
						<input value="<?=$match['opening_date'];?>" id="opening_date" name="opening_date" required="" class="frm_input required" size="50" maxlength="255" type="text">
					</td>
					<th></th>
					<td>
					</td>
				</tr>
				
				<tr>
					<th>대회 홈페이지</th>
					<td ><input name="hompage" value="<?=$match['opening_date'];?>" id="hompage" required="" class="frm_input required" size="50" maxlength="255" type="text"></td>
					<th>개회식 장소</th>
					<td>
						<input id="opening_place" value="<?=$match['opening_place'];?>" name="opening_place" required="" class="frm_input required" size="50" maxlength="255" type="text">
					</td>
				</tr>
				
				
				<?php 
					$increse = 0;
				?>
				
				<tr>
					<th>대회장소</th>
					<td colspan="3">

						<a href="#modal" class="btn_default">장소추가</a>
						<?php 
		                	$sql_s = "select * from match_gym_data inner join gym_data where match_gym_data.match_id='$code' and gym_data.wr_id = match_gym_data.gym_id";
							$result = sql_query($sql_s);
							while($r = sql_fetch_array($result)){
						?>
						<ul id="place_add_list"  class="champ_map_list">
							<li id="gym_div_<?php echo $increse;?>" ><input type="hidden" name="chk[]" value="<?php echo $increse;?>"></input><input type="hidden" name="wr_id[]" value="<?php echo $r['wr_id'];?>"></input>
								<p>
									<span>장소명 :</span><?php echo $r['gym_name']?><a href="">&nbsp;<i class="flaticon-placeholder"></i></a><br/>
									<span>주소 :</span><?php echo $r['gym_address3']?><br/>
								</p>
								<div class="match_day">
								<ul>
									<li>
											경기일 : <input class="frm_input text-center" id="application-period1" required name="application-period[]" value="<?php echo $r['application_period']?>" placeholder="시작일" type="text">
											&nbsp;/&nbsp;사용코트수 : 
											<select required id="use-court" name="use-court[]">
												<?php 
													for($i = 1 ; $i <= $r['gym_courts'] ;$i++){
												?>
												<option <?php if($r['use_court'] == $i){ echo "selected"; }?> value="<?php echo $i;?>"><?php echo $i;?></option>
												<?php		
													}
												?>
											</select>
									</li>
									<li id="gym_plus_place<?php echo $increse;?>">
									</li>
								</ul>
								<div class="text-left">
									<a style="" id="gym_plus<?php echo $increse;?>" onclick="gym_time_plus('<?php echo $increse;?>','<?php echo $r['wr_id'];?>','<?php echo $r['gym_courts'];?>')"   class="btn_default bkg3">+ 경기 추가</a>
								</div>
							</div>
							<div class="text-right">
								<a style=""  class="btn_default bkg6">적용</a>
								<a style="" onclick="delete_gym(<?php echo $increse;?>)" class="btn_default bkg5">삭제</a>
								</div>
							</li>

						</ul>
						<?php  } ?>
					</td>
				</tr>
				-->
				
			</tbody>
		</table>
	</div>
	<!-- //대회정보 필수입력-->
	
	<div class="btn_area">
		<a class="btn btn01 fw-600">취 소</a>
		<input class="btn btn01 fw-600" value="다 음" type="submit">
		<a class="btn btn01 fw-600" href="<?php echo G5_SADM_URL?>/championship_add_step1.php?code=<?php echo $code;?>">다 음(skip)</a>
		
		<!-- 수정할때 출력-->
		<!-- <a class="btn btn02 delete fw-600" onclick="">삭 제</a> -->
	</div>
  	</form>
	<div class="remodal" data-remodal-id="modal" role="dialog" aria-labelledby="modal1Title" aria-describedby="modal1Desc">
	<button data-remodal-action="close" class="remodal-close" aria-label="Close"></button>
		<div class="block-header bg-primary-dark">
		    <ul class="block-options">
		        <li>
		            <button data-dismiss="modal" type="button"><i class="si si-close"></i></button>
		        </li>
		    </ul>
		    <h3 class="block-title">대회 장소 정보 검색</h3>
		</div>
                
        <div class="block-content form-horizontal push-10-t push-10">
            <div class="row">
                <div class="col-md-5 pull-right">
            		<label class="col-md-3 control-label" for="win-point-type">명칭검색</label>
            		<div class="col-md-6">
                    	<input class="form-control" type="text" id="gym-place" name="gym-place" placeholder="경기장 명칭">
                	</div>
                	<div class="col-md-2">
                		<button id="place_search" onclick="place_search()" class="btn btn-primary push-5-r push-10" type="button"><i class="fa fa-search"></i> 검색</button>
        			</div>
            	</div>
        	</div>
        	<h4 id="gym_search_error" class="block-title"></h4>
        	<div class="row">
        		<div class="col-lg-12">
    				<!-- Striped Table -->
		            <div class="block">
		                <div class="block-header">
		                    <div class="block-options">
		                        <code>대회장을 선택해 주세요</code>
		                    </div>
		                    <h3 class="block-title">검색 결과</h3>
		                </div>
		                <div class="block-content">
		                    <table class="table table-striped" style="width: 50%;">
		                        <thead>
		                            <tr>
		                                <th class="text-center" style="width: 50px;">#</th>
		                                <th style="width: 25%;">명칭</th>
		                                <th>주소</th>
		                                <th class="text-center" style="width:100px;">지도</th>
		                                <th class="text-center" style="width: 70px;">코트수</th>
		                                <th class="text-center" style="width: 100px;">장소선택</th>
		                            </tr>
		                        </thead>
		                        <tbody id="gym_list">
		                        </tbody>
		                    </table>
		                </div>
		            </div>
    				<!-- END Striped Table -->
				</div>
        	</div>
    	</div>
    <br>
	<button data-remodal-action="cancel" class="remodal-cancel">Cancel</button>
	<button data-remodal-action="confirm" class="remodal-confirm">OK</button>
</div>

<script>window.jQuery || document.write('<script src="../libs/jquery/dist/jquery.min.js"><\/script>')</script>
<script src="../dist/remodal.js"></script>


</div>

<script>
function place_search () {
	var gym_name = document.getElementById('gym-place').value;
	//alert("aaa");
	$.ajax({
		url:'./gym_search.php?gym_name='+gym_name,
		dataType:'json',
		success:function(data){
			console.log(data['0']);
	    	var str = '';
	    	var i = 1;
	    	if(data.length == 0){
				$('#gym_search_error').html('<i class="glyphicon glyphicon-alert text-danger"></i> "지역 검색" 에서 원하시는 시/군/구의 검색결과가 없으면 해당 지역에 대회장이 없습니다.');	
	    		$('#gym_list').html("");	
	    	}else{
	    		for(var name in data){
					str +=  '<tr><td class="text-center">'+i+'</td>'+
							'<td>'+data[name]['gym_name']+'</td>'+
							'<td>'+data[name]['gym_address3']+'</td>'+
							'<td>'+'<a href="http://dmaps.kr/xz9t" class="btn btn-xs btn-success" type="button" target="_blank"><i class="fa fa-map-marker"></i> 지도보기</a>'+'</td>'+
							'<td class="text-center">'+data[name]['gym_courts']+'</td>'+
							'<td class="text-center"><button class="btn btn-xs btn-default" data-remodal-action="cancel" onclick="place_add('+data[name]['wr_id']+')" data-dismiss="modal" type="button"><i class="fa fa-check"></i> 선택</button></td>';
					i++;
				}
				$('#gym_list').html(str);	
				$('#gym_search_error').html('');	
	    	}
	    }
	});
}
var i = 1;
function place_add(wr_id) {
	$.ajax({
		url:'./gym_search_id.php?wr_id='+wr_id,
		dataType:'json',
		success:function(data){
			console.log(data['0']);
	    	var str = '';
	    	
    		for(var name in data){
    			var cnt = 1;
    			var courts = data['0']['gym_courts'];
str += '<li id="gym_div_'+i+'" ><input type="hidden" name="chk[]" value="'+i+'"></input><input type="hidden" name="wr_id[]" value="'+data['0']['wr_id']+'"></input>' 
+'	<p>'
+'		<span>장소명 :</span>'+data['0']['gym_name']+'<a href="">&nbsp;<i class="flaticon-placeholder"></i></a><br/>'
+'		<span>주소 :</span>'+data['0']['gym_address3']+'<br/>'
+'	</p>'
+'	<div class="match_day">'
+'		<ul>'
+'			<li>'
+'				경기일 : <input class="frm_input text-center" id="application-period1" required name="application-period[]" placeholder="시작일" type="text">'
+'				&nbsp;/&nbsp;사용코트수 : '
+'				<select id="use-court" name="use-court[]">';
				for( cnt = 1 ; cnt <= courts ; cnt++ ){
str +='					<option value="'+cnt+'">'+cnt+'</option>';
				}
str += '				</select>'
+'			</li>'
+'			<li id="gym_plus_place'+i+'">'
+'			</li>'
+'		</ul>'
+'		<div class="text-left">'
+'			<a style="" id="gym_plus'+i+'" onclick="gym_time_plus('+i+','+data['0']['wr_id']+','+courts+')"   class="btn_default bkg3">+ 경기 추가</a>'
+'		</div>'
+'	</div>'
+'	<div class="text-right">'
+'		<a style=""  class="btn_default bkg6">적용</a>'
+'		<a style="" onclick="delete_gym('+i+')" class="btn_default bkg5">삭제</a>'
+'		</div>'
+'	</li>';
				i++;
			}
			$('#place_add_list').append(str);	
	    	
	    }	    
	});
}
</script>
<script>
function delete_gym(j){
	$('#gym_div_'+j).remove("");
}

	function gym_time_plus(j ,wr_id,courts){
		var str = '';
		str += '<input type="hidden" name="chk[]" value="'+j+'"></input><input type="hidden" name="wr_id[]" value="'+wr_id+'"></input><div class="row">'
+'				경기일 : <input class="frm_input text-center" id="application-period1" required name="application-period[]"  placeholder="시작일" type="text">'
+'				&nbsp;/&nbsp;사용코트수 : '
+'				<select required id="use-court" name="use-court[]">'
				for(var cnt = 1 ; cnt <= courts ; cnt++ ){
str +='					<option value="'+cnt+'">'+cnt+'</option>';
				}
str += '				</select>'
+'				&nbsp;&nbsp;<a onclick="gym_time_minus('+j+')" ><i class="flaticon-garbage color5"></i></a>'
		$('#gym_plus_place'+j).html(str);	
}
function gym_time_minus(i){
	$( "#gym_plus_place"+i ).html('<div id="gym_plus_place'+i+'"></div>');
	//$('#gym_plus_place'+j).html(str);	
};
</script>
<script>
function championshipStartDay(i){
	var sub = document.getElementById('championship-start-day1'+i).value;
	
	document.getElementById('championship-start-day2'+i).value = sub;
};
function championshipEndDay(i){
	var sub = document.getElementById('championship-end-day1'+i).value;
	
	document.getElementById('championship-end-day2'+i).value = sub;
};


</script>




<?php
include_once(G5_SADM_PATH.'/tail.php');
?>