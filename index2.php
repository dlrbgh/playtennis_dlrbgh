<?php
include_once('./_common.php');

define('_INDEX_', true);
if (!defined('_GNUBOARD_')) exit; // 개별 페이지 접근 불가

if(defined('G5_THEME_PATH')) {
    require_once(G5_THEME_PATH.'/index.php');
    return;
}

if (G5_IS_MOBILE) {
    include_once(G5_MOBILE_PATH.'/index.php');
    return;
}

include_once(G5_PATH.'/head.php');
?>
<!--<link rel="stylesheet" href="<?php echo G5_URL ?>/assets/js_slider/jquery.bxslider.css_">
<script src="<?php echo G5_URL ?>/assets/js_slider/jquery.bxslider.min.js_"></script>-->

<div class="full-width bkg">
	<div class="row">
		
		<div class="m_slide_area">
		  
		  <?php  echo latest("main_slider","m_slide", 4, 35);?>
		  
		  <div class="slide_side">
		    <?php echo outlogin('basic'); // 외부 로그인, 테마의 스킨을 사용하려면 스킨을 theme/basic 과 같이 지정 ?>
		    <?php echo visit('basic'); // 접속자집계, 테마의 스킨을 사용하려면 스킨을 theme/basic 과 같이 지정 ?>
		  </div>
		</div>
		
		
		
		<div class="latest_area">
		  <h2 class="sound_only">최신글</h2>
		  <!-- 최신글 시작 { -->
		
		  <!-- 공지사항 -->
		  <?php  echo latest("basic","notice", 4, 35);?>
		
		  <!-- 공지사항 -->
		  <?php  echo latest("basic","freeboard", 4, 35);?>
		
		  <div class="thumb_gallery">
		 		<!-- 갤러리 -->
		  		<?php  echo latest("m_banner","gallery", 5, 35);?>
		  </div>
		 </div>
	 </div>
</div>
<!-- cate list -->

<div class="tit_subject sound_only">
  메뉴 바로가기
</div>
<div class="site_map_area">
	<div class="sitemap_section ani">
    	<div class="tit">알고갑시다</div>
			<ul>
			  <li><a href="<?php echo G5_BBS_URL?>/content.php?co_id=tip_info">현장 개설시</a></li>
		      <li><a href="<?php echo G5_BBS_URL?>/content.php?co_id=tip_info1">공종별 현장 설명서</a></li>
		      <li><a href="<?php echo G5_BBS_URL?>/content.php?co_id=tip_info2">업체별 기성 서류</a></li>
		      <li><a href="<?php echo G5_BBS_URL?>/content.php?co_id=tip_info3">현장 종료시</a></li>
		      <li><a href="<?php echo G5_BBS_URL?>/content.php?co_id=tip_info4">공종별 준공 및 추가</a></li>
		      <li><a href="<?php echo G5_BBS_URL?>/content.php?co_id=tip_info5">전도금 청구&정산 기준</a></li>
		      <li><a href="<?php echo G5_BBS_URL?>/content.php?co_id=tip_info6">안전 관리 지침</a></li>
			</ul>
		<span class="border_inner"></span>
	</div>
	
	<div class="sitemap_section ani">
    	<div class="tit">각 현장별 현황</div>
		<ul>
	      <li><a href="<?php echo G5_BBS_URL?>/board.php?bo_table=site&wr_2=2">진행 현장</a></li>
	      <li><a href="<?php echo G5_BBS_URL?>/board.php?bo_table=site&wr_2=1">종료 현장</a></li>
	    </ul>
	</div>
	<div class="sitemap_section ani">
    	<div class="tit">정보마당</div>
		<ul>
	      <li><a href="<?php echo G5_BBS_URL?>/board.php?bo_table=site_address">각 현장별 연락처</a></li>
	      <li><a href="<?php echo G5_BBS_URL?>/board.php?bo_table=network_emergency">직원 비상 연락망</a></li>
	      <li><a href="<?php echo G5_SUB_URL?>/info/premises.php">구내 전화번호</a></li>
	      <li><a href="<?php echo G5_BBS_URL?>/board.php?bo_table=workplace_data">공사팀 서류 양식</a></li>
	      <li><a href="<?php echo G5_SUB_URL?>/info/cooperator.php">각 협력업체 현황</a></li>
	      <li><a href="<?php echo G5_BBS_URL?>/board.php?bo_table=quality_target&wr_id=">연간 품질목표</a></li>
	      <li><a href="<?php echo G5_SUB_URL?>/info/excellent_staff.php">이달의 우수사원</a></li>
	    </ul>
	</div>		

</div>
<!-- //cate list -->


<?php
include_once(G5_PATH.'/tail.php');
?>
