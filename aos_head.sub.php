<?php
// 이 파일은 새로운 파일 생성시 반드시 포함되어야 함
if (!defined('_GNUBOARD_')) exit; // 개별 페이지 접근 불가

// 테마 head.sub.php 파일
if(!defined('G5_IS_ADMIN') && defined('G5_THEME_PATH') && is_file(G5_THEME_PATH.'/head.sub.php')) {
    require_once(G5_THEME_PATH.'/head.sub.php');
    return;
}

$begin_time = get_microtime();

if (!isset($g5['title'])) {
    $g5['title'] = $config['cf_title'];
    $g5_head_title = $g5['title'];
}
else {
    $g5_head_title = $g5['title']; // 상태바에 표시될 제목
    $g5_head_title .= " | ".$config['cf_title'];
}


// 현재 접속자
// 게시판 제목에 ' 포함되면 오류 발생
$g5['lo_location'] = addslashes($g5['title']);
if (!$g5['lo_location'])
    $g5['lo_location'] = addslashes(clean_xss_tags($_SERVER['REQUEST_URI']));
$g5['lo_url'] = addslashes(clean_xss_tags($_SERVER['REQUEST_URI']));
if (strstr($g5['lo_url'], '/'.G5_ADMIN_DIR.'/') || $is_admin == 'super') $g5['lo_url'] = '';

/*
// 만료된 페이지로 사용하시는 경우
header("Cache-Control: no-cache"); // HTTP/1.1
header("Expires: 0"); // rfc2616 - Section 14.21
header("Pragma: no-cache"); // HTTP/1.0
*/
?>
<!doctype html>
<html lang="ko">
<head>

<meta charset="utf-8">
<meta name="google" content="notranslate" />
<meta name="description" content="">
<meta name="author" content="">
<meta name="robots" content="">
<meta name="format-detection" content="telephone=no">
<?php
    echo '<meta name="viewport" content="user-scalable=no, initial-scale=1.0, maximum-scale=1.0, width=device-width, target-densityDpi=medium-dpi">'.PHP_EOL;
    echo '<meta name="HandheldFriendly" content="true">'.PHP_EOL;
    echo '<meta name="format-detection" content="telephone=no">'.PHP_EOL;

    if($config['cf_add_meta'])
    echo $config['cf_add_meta'].PHP_EOL;
?>
<title><?php echo $g5_head_title; ?></title>

<!-- Favicon Icons -->
<link rel="shortcut icon" href="<?php echo G5_IMG_URL ?>/favicons/favicon.png">

<link rel="icon" type="image/png" href="<?php echo G5_IMG_URL ?>/favicons/favicon-42x42.png" sizes="42x42">
<link rel="icon" type="image/png" href="<?php echo G5_IMG_URL ?>/favicons/favicon-72x72.png" sizes="72x72">
<link rel="icon" type="image/png" href="<?php echo G5_IMG_URL ?>/favicons/favicon-96x96.png" sizes="96x96">
<link rel="icon" type="image/png" href="<?php echo G5_IMG_URL ?>/favicons/favicon-144x144.png" sizes="144x144">
<link rel="icon" type="image/png" href="<?php echo G5_IMG_URL ?>/favicons/favicon-192x192.png" sizes="192x192">

<link rel="apple-touch-icon" sizes="42x42" href="<?php echo G5_IMG_URL ?>/favicons/apple-touch-icon-42x42.png">
<link rel="apple-touch-icon" sizes="72x72" href="<?php echo G5_IMG_URL ?>/favicons/apple-touch-icon-72x72.png">
<link rel="apple-touch-icon" sizes="96x96" href="<?php echo G5_IMG_URL ?>/favicons/apple-touch-icon-96x96.png">
<link rel="apple-touch-icon" sizes="144x144" href="<?php echo G5_IMG_URL ?>/favicons/apple-touch-icon-144x144.png">
<link rel="apple-touch-icon" sizes="192x192" href="<?php echo G5_IMG_URL ?>/favicons/apple-touch-icon-192x192.png">
<!-- END Icons -->

<?php
if (defined('G5_IS_ADMIN')) {
    if(!defined('_THEME_PREVIEW_'))
        echo '<link rel="stylesheet" href="'.G5_ADMIN_URL.'/css/admin.css?ver='.G5_CSS_VER.'">'.PHP_EOL;
} else {
    echo '<link rel="stylesheet" href="'.G5_AOS_URL.'/assets/css/m_aos.css?ver='.G5_CSS_VER.'">'.PHP_EOL;
    echo '<link rel="stylesheet" href="'.G5_CSS_URL.'/font/flaticon.css?ver='.G5_CSS_VER.'">'.PHP_EOL;/*icon*/
}
?>
<!--[if lte IE 8]>
<script src="<?php echo G5_JS_URL ?>/html5.js"></script>
<![endif]-->
<script>
// 자바스크립트에서 사용하는 전역변수 선언
var g5_url       = "<?php echo G5_URL ?>";
var g5_bbs_url   = "<?php echo G5_BBS_URL ?>";
var g5_is_member = "<?php echo isset($is_member)?$is_member:''; ?>";
var g5_is_admin  = "<?php echo isset($is_admin)?$is_admin:''; ?>";
var g5_is_mobile = "<?php echo G5_IS_MOBILE ?>";
var g5_bo_table  = "<?php echo isset($bo_table)?$bo_table:''; ?>";
var g5_sca       = "<?php echo isset($sca)?$sca:''; ?>";
var g5_editor    = "<?php echo ($config['cf_editor'] && $board['bo_use_dhtml_editor'])?$config['cf_editor']:''; ?>";
var g5_cookie_domain = "<?php echo G5_COOKIE_DOMAIN ?>";
<?php if(defined('G5_IS_ADMIN')) { ?>
var g5_admin_url = "<?php echo G5_ADMIN_URL; ?>";
<?php } ?>
</script>
<script src="<?php echo G5_JS_URL ?>/jquery-1.8.3.min.js"></script>
<!--
<script src="<?php echo G5_JS_URL ?>/common.js?ver=<?php echo G5_JS_VER; ?>"></script>
<script src="<?php echo G5_JS_URL ?>/wrest.js?ver=<?php echo G5_JS_VER; ?>"></script>
-->
<?php
if(G5_IS_MOBILE) {
    echo '<script src="'.G5_JS_URL.'/modernizr.custom.70111.js"></script>'.PHP_EOL; // overflow scroll 감지
}
if(!defined('G5_IS_ADMIN'))
    echo $config['cf_add_script'];
?>
<link rel='stylesheet' href='<?=G5_JS_URL?>/node_modules/fullcalendar/dist/fullcalendar.css' />

<!-- <script src='<?=G5_JS_URL?>/node_modules/fullcalendar/dist/jquery.min.js'></script> -->
</head>
<body>
<?php include_once("analyticstracking.php") ?>