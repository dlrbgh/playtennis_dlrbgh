<?php
include_once('./_common.php');
define('_INDEX_', true);
if (!defined('_GNUBOARD_')) exit; // 개별 페이지 접근 불가

if(defined('G5_THEME_PATH')) {
    require_once(G5_THEME_PATH.'/index.php');
    return;
}

if (G5_IS_MOBILE) {
    include_once(G5_MOBILE_PATH.'/index.php');
    return;
}

include_once(G5_PATH.'/head.php');




?>

<div id="container" class="remove-padding">
 	<div class="latest_area">
		  <h2 class="sound_only">최신글</h2>

		  <!-- 공지사항 -->
		  <?php  echo latest("basic","notice", 4, 35);?>

		  <!-- 공지사항 -->
		  <?php  echo latest("basic","news", 4, 35);?>
	</div>

</div>
<div id="mside">
	<a href="<?php echo G5_URL?>/myfair/exhi_matching.php?wr_id=1203"><img src="<?php echo G5_IMG_URL?>/banner_265_300.jpg"></a>
</div>




<?php
include_once(G5_PATH.'/tail.php');
?>
