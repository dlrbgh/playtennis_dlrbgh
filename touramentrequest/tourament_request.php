<?php
include_once("./_common.php");
include_once("../s_adm/common.lib.php");

//기간을 검색하여 현재 접수 진행중인 대회 리스트 출력
//대회 클릭시 접수하기 팝업
//자신의 정보 입력 로그인 시 자신의 정보 출력 혹은 비로그인시 간의 입력? 파트너 검색 ? 신청목록에 임시 저장후 -> 저장하기?

//점수 검색의 기준점이 어떻게 될것 인가?

function getMatchData(){
	$data = date("Y-m-d");	
	$sql = "select * from match_data where period1 <= '$data' and period2 >= '$data' and app_visible = '2'";
	$result = sql_query($sql);
	$arr = Array();
	while($r = sql_fetch_array($result)){
		$arr[] = $r;
	}
	return $arr;
}

function getArea1($id){
	$sql = "select * from area1 where id = '$id'";
	$result = sql_query($sql);
	$r = sql_fetch_array($result);
	return $r;
}

function getAreaKr1($area1){
	$sql = "select * from area1 where area1 = '$area1'";
	$result = sql_query($sql);
	$r = sql_fetch_array($result);
	return $r;
}

function getArea2($id){
	$sql = "select * from area2 where id = '$id'";
	$result = sql_query($sql);
	$r = sql_fetch_array($result);
	return $r;
}

function getAreaKr2($area2){
	$sql = "select * from area2 where area2 = '$area2'";
	$result = sql_query($sql);
	$r = sql_fetch_array($result);
	return $r;
}

function getclublist($id){
	$sql = "select * from club_list where id = '$id'";
	$result = sql_query($sql);
	$r = sql_fetch_array($result);
	return $r;
}

function getClubListKr($club){
	$sql = "select * from club_list where club LIKE '%$club%'";
	$result = sql_query($sql);
	$r = sql_fetch_array($result);
	return $r;
}

function getMemberList($sql_search){
	$sql = "select * from g5_member $sql_search";
	$result = sql_query($sql);
	$arr = Array();
	while($r = sql_fetch_array($result)){
		if($r['mb_name'] != ""){
			$area1 = getArea1($r['mb_1']);
			$area1 = trim($area1['area1']);
			$area2 = getArea2($r['mb_2']);
			$area2 = trim($area2['area2']);
			$club = getclublist($r['mb_3']);
			$club = trim($club['club']);
			
			$sqls = "select * from memberDB where area1 = '{$area1}' and area2 = '{$area2}' and (club = '{$club}' or club2 = '{$club}' or club3 = '{$club}') and REPLACE(name, '[[:digit:]]', '') LIKE '%$r[mb_name]%'";
			$results = sql_query($sqls);
			$rs = sql_fetch_array($results);
			$r['grade'] = $rs['grade'];
			$r['area1'] = $area1;
			$r['area2'] = $area2;
			$r['club'] = $club;
			$r['club1'] = $rs['club'];
			$r['club2'] = $rs['club2'];
			$r['club3'] = $rs['club3'];
			if($rs['grade'] != ""){
				$arr[] = $r;
			}
				
		}	
	}
	return $arr;
}
//등급이 검색 되지 않는 인원 검사
function getMemberListNotGrade($sql_search){
	$sql = "select * from g5_member $sql_search";
	$result = sql_query($sql);
	$arr = Array();
	while($r = sql_fetch_array($result)){
		if($r['mb_name'] != ""){
			$area1 = getArea1($r['mb_1']);
			$area1 = trim($area1['area1']);
			$area2 = getArea2($r['mb_2']);
			$area2 = trim($area2['area2']);
			$club = getclublist($r['mb_3']);
			$club = trim($club['club']);
			
			$sqls = "select * from memberDB where area1 = '{$area1}' and area2 = '{$area2}' and (club = '{$club}' or club2 = '{$club}' or club3 = '{$club}') and REPLACE(name, '[[:digit:]]', '') LIKE '%$r[mb_name]%'";
			$results = sql_query($sqls);
			$rs = sql_fetch_array($results);
			$r['grade'] = $rs['grade'];
			$r['area1'] = $area1;
			$r['area2'] = $area2;
			$r['club'] = $club;
			$r['club1'] = $rs['club'];
			$r['club2'] = $rs['club2'];
			$r['club3'] = $rs['club3'];
			if($rs['grade'] == ""){
				$arr[] = $r;
			}
				
		}	
	}
	return $arr;
}

function getMemberGrade($mb_id){
	$sql = "select * from g5_member where mb_id = '$mb_id'";
	$result = sql_query($sql);
	$r = sql_fetch_array($result);
	
	$area1 = getArea1($r['mb_1']);
	$area1 = trim($area1['area1']);
	$area2 = getArea2($r['mb_2']);
	$area2 = trim($area2['area2']);
	$club = getclublist($r['mb_3']);
	$club = trim($club['club']);
	
	$sqls = "select * from memberDB where area1 = '{$area1}' and area2 = '{$area2}' and club = '{$club}' and name = '{$r[mb_name]}'";
	$results = sql_query($sqls);
	$rs = sql_fetch_array($results);
	$r['grade'] = $rs['grade'];
	$r['area1'] = $area1;
	$r['area2'] = $area2;
	$r['club'] = $club;
	$r['club1'] = $rs['club'];
	$r['club2'] = $rs['club2'];
	$r['club3'] = $rs['club3'];
	
	return $r;
}

function getGradeData($match_code){
	$sql = "select * from grade_data where match_code = '$match_code'";
	$result = sql_query($sql);
	$arr = Array();
	while($r = sql_fetch_array($result)){
		$arr[] = $r;
	}
	return $arr;
}

//동호회 명단 검색
function getmemberDBSearch($sql_search){
	$sql = "select * from memberDB $sql_search";
	$result = sql_query($sql);
	$arr = Array();
	while($r = sql_fetch_array($result)){
		$arr[] = $r;
	}
	return $arr;
}

//회원 명단중에 설정된 점수중에 참가가 가능한 점수대의 사람만 보여줌
function getMemberGradeByGrade($mb_grade,$max_grade,$sql_search){
	$sql = "select * from g5_member $sql_search";
	$result = sql_query($sql);
	$arr = Array();
	while($r = sql_fetch_array($result)){
		if($r['mb_name'] != ""){
			$area1 = getArea1($r['mb_1']);
			$area1 = trim($area1['area1']);
			$area2 = getArea2($r['mb_2']);
			$area2 = trim($area2['area2']);
			$club = getclublist($r['mb_3']);
			$club = trim($club['club']);
			
			$sqls = "select * from memberDB where area1 = '{$area1}' and area2 = '{$area2}' and (club = '{$club}' or club2 = '{$club}' or club3 = '{$club}') and REPLACE(name, '[[:digit:]]', '') LIKE '%$r[mb_name]%'";
			$results = sql_query($sqls);
			$rs = sql_fetch_array($results);
			$r['grade'] = $rs['grade'];
			$r['area1'] = $area1;
			$r['area2'] = $area2;
			$r['club'] = $club;
			$r['club1'] = $rs['club'];
			$r['club2'] = $rs['club2'];
			$r['club3'] = $rs['club3'];
			if($max_grade >= ($mb_grade+$rs['grade']) && $rs['grade'] != ""){
				$arr[] = $r;
			}
		}	
	}
	return $arr;
}

$mb_id = "rubl01@daum.net";

$matchCode = "3815960IH4ECKT0TZ3";

$matchData = getMatchData();
$gradData = getGradeData($matchCode);
$mb = getMemberGrade($mb_id);
$arr = getMemberList();
$arr2 = getMemberListNotGrade();
?>
<!-- <form method="post" name="frmReview1" action="step1.php" id="frmReview1" enctype="multipart/form-data">
    <div class="col-lg-6">
		<div class="tit" style="font-size:15px;margin-bottom:5px">첨부파일 등록</div>
        <div class="input-group">
        	<input type="hidden" name="wr_id" value="<?php echo $wr_id?>" />
        	<input type="hidden" name="code" value="<?php echo $code?>" />
            <input type="file" id="bf_file" name="bf_file[]" class="form-control">
            <span class="input-group-btn">
                <button class="btn btn-default" type="submit" id="submitBtn">엑셀파일 업로드 하기</button>
            </span>
        </div>
    </div>
</form> -->
        
<table>
	<?php 
	foreach ($matchData as $key => $value) {	
	?>
	<tr>
	<td>
		<?php echo $value['wr_name'];?>
	</td>	
	</tr>
	<?php
	}
	?>
</table>

<table>
	<tr>
		<th>등급명</th><th>최저등급</th><th>최고등급</th><th>합산등급</th>
	</tr>
	<?php 
	foreach ($gradData as $key => $value) {	
	?>
	<tr>
	<td>
		<?php echo $value['grad_title'];?>
	</td>
	<td>
		<?php echo $value['grad_low'];?>
	</td>
	<td>
		<?php echo $value['grad_high'];?>
	</td>
	<td>
		<?php echo $value['grad_average'];?>
	</td>	
	</tr>
	<?php
	}
	?>
</table>


<table cellpadding="5" border="1">
	<tr>
		<th>이름</th><th>지역1</th><th>지역2</th><th>회원가입한클럽</th><th>동호회명단 클럽</th><th>동호회명단 클럽2</th><th>동호회명단 클럽3</th><th>점수</th>
	</tr>
	<?php 
	foreach ($arr as $key => $value) {	
	?>
	<tr>
	<td>
		<?php echo $value['mb_name'];?>
	</td>
	<td>
		<?php echo $value['area1'];?>
	</td>
	<td>
		<?php echo $value['area2'];?>
	</td>
	<td>
		<?php echo $value['club'];?>
	</td>
	<td>
		<?php echo $value['club1'];?>
	</td>	
	<td>
		<?php echo $value['club2'];?>
	</td>	
	<td>
		<?php echo $value['club3'];?>
	</td>		
	<td>
		<?php echo $value['grade'];?>
	</td>	
	</tr>
	<?php
	}
	?>
</table> 


<table cellpadding="5" border="1">
	<tr>
		<th>이름</th><th>지역1</th><th>지역2</th><th>회원가입한클럽</th><th>동호회명단 클럽</th><th>동호회명단 클럽2</th><th>동호회명단 클럽3</th><th>점수</th>
	</tr>
	<?php 
	foreach ($arr2 as $key => $value) {	
	?>
	<tr>
	<td>
		<?php echo $value['mb_name'];?>
	</td>
	<td>
		<?php echo $value['area1'];?>
	</td>
	<td>
		<?php echo $value['area2'];?>
	</td>
	<td>
		<?php echo $value['club'];?>
	</td>
	<td>
		<?php echo $value['club1'];?>
	</td>	
	<td>
		<?php echo $value['club2'];?>
	</td>	
	<td>
		<?php echo $value['club3'];?>
	</td>		
	<td>
		<?php echo $value['grade'];?>
	</td>	
	</tr>
	<?php
	}
	?>
</table>
<?php
	//비회원 참가신청하기
?>
<div>
	이름 : <input type="text" name="mb_name" >
	지역 : <input type="text" name="area1" placeholder="강원도" >
	지역1 : <input type="text" name="area2" placeholder="홍천군" >
	클럽 : <input type="text" name="club" >
	등급 : <input type="text" name="grade" >
</div>

<?php
	//회원과 같은 정보 출력 
	$arr = getMemberList("where mb_1 = '$mb[mb_1]' and mb_2 = '$mb[mb_2]' and mb_3 = '$mb[mb_3]'");
	
	$area1 = getAreaKr1("강원도");
	$area2 = getAreaKr2("태백시");
	$club = getClubListKr("여원클럽");
	
	$arr = getMemberList("where mb_1 = '$area1[id]' and mb_2 = '$area2[id]' and mb_3 = '$club[id]'");
?>
<table cellpadding="5" border="1">
	<tr>
		<th>이름</th><th>지역1</th><th>지역2</th><th>클럽</th><th>클럽2</th><th>클럽3</th><th>점수</th>
	</tr>
	<?php 
	foreach ($arr as $key => $value) {	
	?>
	<tr>
	<td>
		<?php echo $value['mb_name'];?>
	</td>
	<td>
		<?php echo $value['area1'];?>
	</td>
	<td>
		<?php echo $value['area2'];?>
	</td>
	<td>
		<?php echo $value['club'];?>
	</td>
	<td>
		<?php echo $value['club2'];?>
	</td>	
	<td>
		<?php echo $value['club3'];?>
	</td>		
	<td>
		<?php echo $value['grade'];?>
	</td>	
	</tr>
	<?php
	}
	?>
</table>

<?php
	//회원과 같은 정보 출력 
	$arr = getMemberList("where mb_1 = '$mb[mb_1]' and mb_2 = '$mb[mb_2]' and mb_3 = '$mb[mb_3]'");
	
	$area1 = getAreaKr1("강원도");
	$area2 = getAreaKr2("태백시");
	$club = getClubListKr("여원클럽");
	//getMemberGradeByGrade($mb_grade,$max_grade,$sql_search)
	$arr = getMemberGradeByGrade(3,5,"where mb_1 = '$area1[id]' and mb_2 = '$area2[id]' and mb_3 = '$club[id]'");
?>
<table cellpadding="5" border="1">
	<tr>
		<th>이름</th><th>지역1</th><th>지역2</th><th>클럽</th><th>클럽2</th><th>클럽3</th><th>점수</th>
	</tr>
	<?php 
	foreach ($arr as $key => $value) {	
	?>
	<tr>
	<td>
		<?php echo $value['mb_name'];?>
	</td>
	<td>
		<?php echo $value['area1'];?>
	</td>
	<td>
		<?php echo $value['area2'];?>
	</td>
	<td>
		<?php echo $value['club'];?>
	</td>
	<td>
		<?php echo $value['club2'];?>
	</td>	
	<td>
		<?php echo $value['club3'];?>
	</td>		
	<td>
		<?php echo $value['grade'];?>
	</td>	
	</tr>
	<?php
	}
	?>
</table>
