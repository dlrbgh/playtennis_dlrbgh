<?php
include_once('./_common.php');

error_reporting(E_ALL);

ini_set("display_errors", 1);


$wr_id = $_REQUEST['wr_id'];
$code = $_REQUEST['code'];

sql_query("DELETE FROM team_data WHERE match_code = '$code'" );
sql_query("DELETE FROM team_event_data WHERE match_code = '$code'" );

sql_query("update match_data set step = '2' WHERE code = '$code'" );

$bo_table = "memberdb";
// 디렉토리가 없다면 생성합니다. (퍼미션도 변경하구요.)
@mkdir(G5_DATA_PATH.'/file/'.$bo_table, G5_DIR_PERMISSION);
@chmod(G5_DATA_PATH.'/file/'.$bo_table, G5_DIR_PERMISSION);

$chars_array = array_merge(range(0,9), range('a','z'), range('A','Z'));

// 가변 파일 업로드
$file_upload_msg = '';
$upload = array();
//print_r($_FILES['bf_file']);
for ($i=0; $i<count($_FILES['bf_file']['name']); $i++) {
	
    $upload[$i]['file']     = '';
    $upload[$i]['source']   = '';
    $upload[$i]['filesize'] = 0;
    $upload[$i]['image']    = array();
    $upload[$i]['image'][0] = '';
    $upload[$i]['image'][1] = '';
    $upload[$i]['image'][2] = '';

    // 삭제에 체크가 되어있다면 파일을 삭제합니다.
    if (isset($_POST['bf_file_del'][$i]) && $_POST['bf_file_del'][$i]) {
        $upload[$i]['del_check'] = true;

        $row = sql_fetch(" select bf_file from {$g5['board_file_table']} where bo_table = '{$bo_table}' and wr_id = '{$wr_id}' and bf_no = '{$i}' ");
        @unlink(G5_DATA_PATH.'/file/'.$bo_table.'/'.$row['bf_file']);
        // 썸네일삭제
        if(preg_match("/\.({$config['cf_image_extension']})$/i", $row['bf_file'])) {
            delete_board_thumbnail($bo_table, $row['bf_file']);
        }
    }
    else
        $upload[$i]['del_check'] = false;

    $tmp_file  = $_FILES['bf_file']['tmp_name'][$i];
	//echo $tmp_file;
    $filesize  = $_FILES['bf_file']['size'][$i];
    $filename  = $_FILES['bf_file']['name'][$i];
    $filename  = get_safe_filename($filename);

    // 서버에 설정된 값보다 큰파일을 업로드 한다면

    if (is_uploaded_file($tmp_file)) {
        // 관리자가 아니면서 설정한 업로드 사이즈보다 크다면 건너뜀

        //=================================================================\
        // 090714
        // 이미지나 플래시 파일에 악성코드를 심어 업로드 하는 경우를 방지
        // 에러메세지는 출력하지 않는다.
        //-----------------------------------------------------------------
        $timg = @getimagesize($tmp_file);
        // image type
        if ( preg_match("/\.({$config['cf_image_extension']})$/i", $filename) ||
             preg_match("/\.({$config['cf_flash_extension']})$/i", $filename) ) {
            if ($timg['2'] < 1 || $timg['2'] > 16)
                continue;
        }
        //=================================================================

        $upload[$i]['image'] = $timg;

        // 4.00.11 - 글답변에서 파일 업로드시 원글의 파일이 삭제되는 오류를 수정
        if ($w == 'u') {
            // 존재하는 파일이 있다면 삭제합니다.
            $row = sql_fetch(" select bf_file from {$g5['board_file_table']} where bo_table = '$bo_table' and wr_id = '$wr_id' and bf_no = '$i' ");
            @unlink(G5_DATA_PATH.'/file/'.$bo_table.'/'.$row['bf_file']);
            // 이미지파일이면 썸네일삭제
            if(preg_match("/\.({$config['cf_image_extension']})$/i", $row['bf_file'])) {
                delete_board_thumbnail($bo_table, $row['bf_file']);
            }
        }

        // 프로그램 원래 파일명
        $upload[$i]['source'] = $filename;
        $upload[$i]['filesize'] = $filesize;

        // 아래의 문자열이 들어간 파일은 -x 를 붙여서 웹경로를 알더라도 실행을 하지 못하도록 함
        $filename = preg_replace("/\.(php|phtm|htm|cgi|pl|exe|jsp|asp|inc)/i", "$0-x", $filename);

        shuffle($chars_array);
        $shuffle = implode('', $chars_array);

        // 첨부파일 첨부시 첨부파일명에 공백이 포함되어 있으면 일부 PC에서 보이지 않거나 다운로드 되지 않는 현상이 있습니다. (길상여의 님 090925)
        $upload[$i]['file'] = abs(ip2long($_SERVER['REMOTE_ADDR'])).'_'.substr($shuffle,0,8).'_'.replace_filename($filename);

        $dest_file = G5_DATA_PATH.'/file/'.$bo_table.'/'.$upload[$i]['file'];

        // 업로드가 안된다면 에러메세지 출력하고 죽어버립니다.
        $error_code = move_uploaded_file($tmp_file, $dest_file) or die($_FILES['bf_file']['error'][$i]);

        // 올라간 파일의 퍼미션을 변경합니다.
        chmod($dest_file, G5_FILE_PERMISSION);
    }
}

// 나중에 테이블에 저장하는 이유는 $wr_id 값을 저장해야 하기 때문입니다.
for ($i=0; $i<count($upload); $i++)
{
    if (!get_magic_quotes_gpc()) {
        $upload[$i]['source'] = addslashes($upload[$i]['source']);
    }

    $row = sql_fetch(" select count(*) as cnt from {$g5['board_file_table']} where bo_table = '{$bo_table}' and wr_id = '{$wr_id}' and bf_no = '{$i}' ");
    if ($row['cnt'])
    {
        // 삭제에 체크가 있거나 파일이 있다면 업데이트를 합니다.
        // 그렇지 않다면 내용만 업데이트 합니다.
        if ($upload[$i]['del_check'] || $upload[$i]['file'])
        {
            $sql = " update {$g5['board_file_table']}
                        set bf_source = '{$upload[$i]['source']}',
                             bf_file = '{$upload[$i]['file']}',
                             bf_content = 'excel',
                             bf_filesize = '{$upload[$i]['filesize']}',
                             bf_width = '{$upload[$i]['image']['0']}',
                             bf_height = '{$upload[$i]['image']['1']}',
                             bf_type = '{$upload[$i]['image']['2']}',
                             bf_datetime = '".G5_TIME_YMDHIS."'
                      where bo_table = '{$bo_table}'
                                and wr_id = '{$wr_id}'
                                and bf_no = '{$i}' ";
            sql_query($sql);
        }
        else
        {
            $sql = " update {$g5['board_file_table']}
                        set bf_content = '{$bf_content[$i]}'
                        where bo_table = '{$bo_table}'
                                  and wr_id = '{$wr_id}'
                                  and bf_no = '{$i}' ";
            sql_query($sql);
        }
    }
    else
    {
        $sql = " insert into {$g5['board_file_table']}
                    set bo_table = '{$bo_table}',
                         wr_id = '{$wr_id}',
                         bf_no = '{$i}',
                         bf_source = '{$upload[$i]['source']}',
                         bf_file = '{$upload[$i]['file']}',
                         bf_content = 'excel',
                         bf_download = 0,
                         bf_filesize = '{$upload[$i]['filesize']}',
                         bf_width = '{$upload[$i]['image']['0']}',
                         bf_height = '{$upload[$i]['image']['1']}',
                         bf_type = '{$upload[$i]['image']['2']}',
                         bf_datetime = '".G5_TIME_YMDHIS."' ";
        sql_query($sql);
    }
}

// 업로드된 파일 내용에서 가장 큰 번호를 얻어 거꾸로 확인해 가면서
// 파일 정보가 없다면 테이블의 내용을 삭제합니다.
$row = sql_fetch(" select max(bf_no) as max_bf_no from {$g5['board_file_table']} where bo_table = '{$bo_table}' and wr_id = '{$wr_id}' ");
for ($i=(int)$row['max_bf_no']; $i>=0; $i--)
{
    $row2 = sql_fetch(" select bf_file from {$g5['board_file_table']} where bo_table = '{$bo_table}' and wr_id = '{$wr_id}' and bf_no = '{$i}' ");

    // 정보가 있다면 빠집니다.
    if ($row2['bf_file']) break;

    // 그렇지 않다면 정보를 삭제합니다.
    sql_query(" delete from {$g5['board_file_table']} where bo_table = '{$bo_table}' and wr_id = '{$wr_id}' and bf_no = '{$i}' ");
}

$sql = "select * from g5_board_file where wr_id = '$wr_id' and bf_content = 'excel' and bo_table = '$bo_table'";
$result = sql_query($sql);
$r = sql_fetch_array($result);

$bf_source = $r['bf_source'];
$file_name = $r['bf_file'];

include_once '../Excel/reader.php';
$data = new Spreadsheet_Excel_Reader();
$data->setOutputEncoding('utf-8');
$data->read('../data/file/memberdb/'.$file_name);

$count = 0;
echo "br".$data->sheets[0]['numRows'];
for ($i = 0; $i <= $data->sheets[0]['numRows']; $i++) {
	
	$value1 = $data->sheets[0]['cells'][$i][2];
	$value1 = trim($value1);
	$value2 = $data->sheets[0]['cells'][$i][3];
	$value2 = trim($value2);
	$value3 = $data->sheets[0]['cells'][$i][4];
	$value3 = trim($value3);
	$value4 = $data->sheets[0]['cells'][$i][5];
	$value4 = trim($value4);
	$value5 = $data->sheets[0]['cells'][$i][6];
	$value5 = trim($value5);
	$value6 = $data->sheets[0]['cells'][$i][7];
	$value6 = trim($value6);
	$value7 = $data->sheets[0]['cells'][$i][8];
	$value7 = trim($value7);
	$value8 = $data->sheets[0]['cells'][$i][9];
	$value8 = trim($value8);
	$value9 = $data->sheets[0]['cells'][$i][10];
	$value9 = trim($value9);
	$value10 = $data->sheets[0]['cells'][$i][11];
	$value10 = trim($value10);
	$value11 = $data->sheets[0]['cells'][$i][12];
	$value11 = trim($value11);
	$value12 = $data->sheets[0]['cells'][$i][13];
	$value12 = trim($value12);
	$value13 = $data->sheets[0]['cells'][$i][14];
	$value13 = trim($value13);
	if($value1 != ""){
		$sql = "insert into memberDB set
	       area1			='$value1',
	       area2			='$value2',
	       club				='$value3',
		   name 	 		='$value4',
	       gender			='$value5',
	       grade			='$value6',
	       division			='$value7',
	       rank				='$value8',
	       etc 				='$value9',
	       club2    		='$value10',
	       club3			='$value11',
	       club4			='$value12',
	       club5			='$value13',
	       wr_datetime 		= '".G5_TIME_YMDHIS."',
	       wr_lasttime 		= '".G5_TIME_YMDHIS."',
			wr_ip = '{$_SERVER['REMOTE_ADDR']}'";
		echo $sql."<br>";
		sql_query($sql);
	}	
}

if(is_file('../data/file/memberdb/'.$file_name)==true){
	unlink('../data/file/memberdb/'.$file_name);
}

// $rows = array();
// 
// $rows['0']['count'] = $count;	
// $rows['0']['file_name'] = $bf_source;		
// $rows['0']['datetime'] = G5_TIME_YMDHIS;
// $rows['0']['code'] = $code;		
// 	
// echo json_encode($rows);

//goto_url("championship_add_step3.php?code=$code");

?>
