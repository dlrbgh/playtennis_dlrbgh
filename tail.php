<?php
if (!defined('_GNUBOARD_')) exit; // 개별 페이지 접근 불가

if(defined('G5_THEME_PATH')) {
    require_once(G5_THEME_PATH.'/tail.php');
    return;
}

if (G5_IS_MOBILE) {
    include_once(G5_MOBILE_PATH.'/tail.php');
    return;
}
?>

       
</div>

<!-- } 콘텐츠 끝 -->

<hr>

<!-- 하단 시작 { -->
<div id="ft">
    <div id="ft_company">
    	<div class="ft_cs_area">
    		<div class="tit">고객센터</div>
    		<div class="call_number">02.6959.5040</div>
    	</div>
    	<ul class="ft_cs_info">
		   <li><strong>MON-FRI</strong> AM 10:00 ~ PM 6:00 / <strong>LUNCH</strong> PM 12:00 ~ PM 1:00</li>
		   <li>고객센터가 종료된 오후 7시 이후와 공휴일에는 1:1 문의하기 게시판을 이용해주세요</li>
		</ul> 
    </div>
    <div id="ft_copy">
        <div class="ft_navi_menu">
        	<a href="<?php echo G5_URL; ?>/company" target="_blank">회사소개</a>|&nbsp;&nbsp;
            <a href="<?php echo G5_BBS_URL; ?>/content.php?co_id=privacy">개인정보처리방침</a>|&nbsp;&nbsp;
            <a href="<?php echo G5_BBS_URL; ?>/content.php?co_id=provision">서비스이용약관</a>
            <a href="#hd" id="ft_totop">상단으로</a>
        </div>
        <div class="ft_info">
        	<ul>
        		<li>상호명 : 주식회사 레이즈업 / 대표자: 이상훈</li>
        		<li>주소 : 서울시 중랑구 망우동 515-39 3층</li>
        		<li>전화 : 02.6959.5040 / 이메일 : inventor@raizup.kr / 사업자 등록번호 : 221-81-38363</li>
        	</ul>
        	Copyright &copy; <b>RAIZUP.</b> All rights reserved.<br>
        </div>
    </div>
</div>

<?php
if ($config['cf_analytics']) {
    echo $config['cf_analytics'];
}
?>

<!-- } main 하단 끝 -->



<?php
include_once(G5_PATH."/tail.sub.php");
?>
