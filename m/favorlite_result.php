<?php
include_once('../common.php');
$user_code	= $_REQUEST['user_code'];
$user_club	= $_REQUEST['user_club'];
$user_name	= $_REQUEST['user_name'];

?>
<?php require 'inc/config.php'; ?>
<?php require 'inc/views/template_head_start.php'; ?>

<!-- Page JS Plugins CSS -->
<link rel="stylesheet" href="<?php echo $one->assets_folder; ?>/js/plugins/slick/slick.min.css">
<link rel="stylesheet" href="<?php echo $one->assets_folder; ?>/js/plugins/slick/slick-theme.min.css">
<link rel="stylesheet" href="<?php echo $one->assets_folder; ?>/js/plugins/select2/select2.min.css">
<link rel="stylesheet" href="<?php echo $one->assets_folder; ?>/js/plugins/select2/select2-bootstrap.min.css">
<?php require 'inc/views/template_head_end.php'; ?>
	
	<!-- Contents Area -->
	<div class="content">
	    	    
	    <!-- 접수현황 -->
	    <div class="row">
	    	<div class="col-lg-6">
	        	<div class="block">
			        <div class="block-content_">
			            <table class="js-table-sections table table-mobile">
			                <!-- 그룹 경기 조회 타이틀 -->
			                <tbody class="js-table-sections-header open">
			                    <tr>
			                        <td class="font-w600" width="85%">중계예정</td>
			                        <td class="text-center">
			                            <i class="fa fa-angle-right"></i>
			                        </td>
			                    </tr>
			                </tbody>
			                <!-- end 그룹 경기 조회 타이틀 -->
			                <tbody>
			                    <!-- 참가 선수 정보 및 대진 점수 -->
			                    <tr>
			                    	<td colspan="2" class="favorite">
			                    		<div class="row tournament_match">
			                    		<?php 
			                    			$fav_sql = "select * from app_user_favor where user_code = '$user_code'";
											$fav_result = sql_query($fav_sql);
											while($fav = sql_fetch_array($fav_result)){
												$game_sql = "select * from game_score_data as a inner join match_data as b where (a.team_1_code = '$fav[team_code]' or a.team_2_code = '$fav[team_code]')and a.game_assign = '1' and a.match_code = b.code and ( b.end_game > '1' and b.end_game < '5') order by a.game_date,a.game_time" ;
												$game_result = sql_query($game_sql);
												$i = 1;
											while($game = sql_fetch_array($game_result)){
												$sql_team1 = "select * from team_data where team_code = '$game[team_1_code]'";
												$team1_result = sql_query($sql_team1);
												$team1 = sql_fetch_array($team1_result);
												
												$sql_team2 = "select * from team_data where team_code = '$game[team_2_code]'";
												$team2_result = sql_query($sql_team2);
												$team2 = sql_fetch_array($team2_result);
												
												$sql_gym = "select * from gym_data where wr_id = '$game[gym_code]'";
												$gym_result = sql_query($sql_gym);
												$gym = sql_fetch_array($gym_result);
												
												$sql_match = "select * from match_data where code = '$game[match_code]'";
												$match_result = sql_query($sql_match);
												$match = sql_fetch_array($match_result);
									
											if($i == 1){
											?>
												<div class="title" style="margin-bottom:12px; float:left;width: 100%;padding:0 15px">
													<div style="background-color:#fff;padding:10px; position:relative;">
						                    			<span class="font-w700"><?=$game['wr_name']?></span>
						                    			<br/><?=$gym['gym_name'];?>&nbsp;&nbsp;&nbsp;<?=$game['game_date'];?>
						                    			<span class="push-10-l"><?=$match_gym['game_date']?>&nbsp;&nbsp;&nbsp;<?=$team['team_1_name']?> <?=$team['team_2_name']?></span>
						                    			<span class="pull-right" style="position:absolute;right:10px;top:50%;margin-top:-7px">
						                    				<a href="champ_info_match_view.php?wr_id=<?=$game['wr_id']?>&user_code=<?=$user_code;?>&user_club=<?=$user_club;?>&user_name=<?=$user_name;?>&division=<?=$game['division']?>&series=<?=$game['series']?>&series_sub=<?=$game['series_sub']?>">바로가기</a>
						                    			</span>
					                    			</div>
					                    		</div>
					                    		<?php	
												}
												$i++;
												?>
			                    		
			                    			<!-- 참가선수 정보 -->
			                    			<div class="col-xs-12 col-sm-6 col-md-6">
									            <div class="block block-rounded">
									                <div class="block-options push-10-t push-10-r">
										                <li><?=$game['division'];?> <?=$game['series'];?> <?=$game['series_sub'];?></li>
										            </div>
										            <div class="tournament_header">
									                    <h3 class="block-title">
									                    	<span><?=$game['game_court'];?>코트 <?=$game['court_array_num'];?>번 경기</span>
									                    	<span class="push-10-l"><?=$game['game_time'];?></span>
								                    	   	<?php if($game['tournament'] == "T"){?>
										                    <span class="push-10-l">토너먼트<?=$game['tournament_count']*2;?>강</span>
											               	<?php }?>
											               	<?php if($game['tournament'] == "C"){?>
										                    <span class="push-10-l">결승</span>
											               	<?php }?>
											                
									                    </h3>
									                </div>
									                <div class="tournament_content clearfix">
									                	<div class="pull-left text-center push-5-l">
															<?php
									                			$favor_sql = "select count(*) as cnt from app_user_favor where user_code = '$user_code' and match_code = '$game[match_code]' and team_code = '$game[team_1_code]'";
									                			$favor_result = sql_query($favor_sql);
																$favor = sql_fetch_array($favor_result);
																if($favor['cnt'] == 0){
															?>
															
									                    		<button class="btn_fav" onclick="favor('<?=$user_code?>','<?=$game['team_1_code']?>','<?=$game['match_code']?>')">
									                    	<?php
																}else{
															?>
																<button class="btn_fav active" onclick="favor('<?=$user_code?>','<?=$game['team_1_code']?>','<?=$game['match_code']?>')">
															<?php	
																}
									                		?>										                    	
									                			<div class="left-star">
											                		<i class="fa fa-star"></i>
											                	</div>
										                        <div class="font-w600"><?=$team1['club'];?></div>
										                        <div class=""><?=$team1['team_1_name'];?> <?=$team1['team_2_name'];?></div>
										                    </button>
									                    </div>
									                	<div class="tournament_point">
									                		<span class="left_team <?php if($game['team_1_score'] > $game['team_2_score']) echo "win";?>"><?=$game['team_1_score'];?></span>
									                		<span>vs</span>
									                		<span class="right_team <?php if($game['team_1_score'] < $game['team_2_score']) echo "win";?>"><?=$game['team_2_score'];?></span>
									                    </div>
									                	
									                    <div class="pull-right text-center push-5-r">
									                		<?php
									                			$favor_sql = "select count(*) as cnt from app_user_favor where user_code = '$user_code' and match_code = '$game[match_code]' and team_code = '$game[team_2_code]'";
									                			$favor_result = sql_query($favor_sql);
																$favor = sql_fetch_array($favor_result);
																if($favor['cnt'] == 0){
															?>
															
									                    		<button class="btn_fav" onclick="favor('<?=$user_code?>','<?=$game['team_2_code']?>','<?=$game['match_code']?>')">
									                    	<?php
																}else{
															?>
																<button class="btn_fav active" onclick="favor('<?=$user_code?>','<?=$game['team_2_code']?>','<?=$game['match_code']?>')">
															<?php	
																}
									                		?>
										                		<div class="right-star">
											                		<i class="fa fa-star"></i>
											                	</div>
										                        <div class="font-w600"><?=$team2['club'];?></div>
										                        <div class=""><?=$team2['team_1_name'];?> <?=$team2['team_2_name'];?></div>
									                        </button>
									                    </div>
									                </div>
									               
									            </div>
									        </div>
									        <!-- end 참가선수 정보 -->
									        <?php } } ?>	 
										</div>
										
			                    	</td>
			                    </tr>
			                </tbody>
			            </table>
			        </div>
			    </div>
	        	
	        </div>
	    </div>
	    <div class="row">
	    	<div class="col-lg-6">
	        	<div class="block">
			        <div class="block-content_">
			            <table class="js-table-sections table table-mobile">
			                <!-- 그룹 경기 조회 타이틀 -->
			                <tbody class="js-table-sections-header open">
			                    <tr>
			                        <td class="font-w600" width="85%">종료경기</td>
			                        <td class="text-center">
			                            <i class="fa fa-angle-right"></i>
			                        </td>
			                    </tr>
			                </tbody>
			                <!-- end 그룹 경기 조회 타이틀 -->
			                <tbody>
			                    <!-- 참가 선수 정보 및 대진 점수 -->
			                    <tr>
			                    	<td colspan="2" class="favorite">
			                    		<div class="row tournament_match">
			                    		<?php 
				                    		$fav_sql = "select * from app_user_favor where user_code = '$user_code'";
											$fav_result = sql_query($fav_sql);
											while($fav = sql_fetch_array($fav_result)){
												$i = 0;
												$game_sql = "select *,b.wr_id from game_score_data as a inner join match_data as b where ( a.team_1_code = '$fav[team_code]' or a.team_2_code = '$fav[team_code]' ) and a.game_assign = '1'  and a.match_code = b.code and b.end_game = '5' order by b.period1,a.game_time desc";
												
												$game_result = sql_query($game_sql);
												while($game = sql_fetch_array($game_result)){
													$sql_team1 = "select * from team_data where team_code = '$game[team_1_code]'";
													$team1_result = sql_query($sql_team1);
													$team1 = sql_fetch_array($team1_result);
													
													$sql_team2 = "select * from team_data where team_code = '$game[team_2_code]'";
													$team2_result = sql_query($sql_team2);
													$team2 = sql_fetch_array($team2_result);
													
													$sql_gym = "select * from gym_data where wr_id = '$game[gym_code]'";
													$gym_result = sql_query($sql_gym);
													$gym = sql_fetch_array($gym_result);
													
													$sql_team = "select * from team_data where team_code = '$fav[team_code]' and match_code = '$game[match_code]'";
													$team_result = sql_query($sql_team);
													$team = sql_fetch_array($team_result);
													
													if($i == 0){
													?>
													<div class="title" style="margin-bottom:12px; float:left;width: 100%;padding:0 15px">
														<div style="background-color:#fff;padding:10px; position:relative;">
							                    			<span class="font-w700"><?=$game['wr_name']?></span>
							                    			<br/><?=$gym['gym_name'];?>&nbsp;&nbsp;&nbsp;<?=$game['game_date'];?>
							                    			<span class="push-10-l"><?=$match_gym['game_date']?>&nbsp;&nbsp;&nbsp;<?=$team['team_1_name']?> <?=$team['team_2_name']?></span>
							                    			<span class="pull-right" style="position:absolute;right:10px;top:50%;margin-top:-7px">
							                    				<a href="champ_info_match_view.php?wr_id=<?=$game['wr_id']?>&user_code=<?=$user_code;?>&user_club=<?=$user_club;?>&user_name=<?=$user_name;?>&division=<?=$game['division']?>&series=<?=$game['series']?>&series_sub=<?=$game['series_sub']?>">바로가기</a>
							                    			</span>
						                    			</div>
						                    		</div>
						                    		<?php	
													}
													$i++;
													?>
			                    		
			                    			<!-- 참가선수 정보 -->
			                    			<div class="col-xs-12 col-sm-6 col-md-6">
									            <div class="block block-rounded">
									                <div class="block-options push-10-t push-10-r">
										                <li></li>
										            </div>
										            <div class="tournament_header">
									                    <h3 class="block-title"><?=$game['game_court'];?>코트 <?=$game['court_array_num'];?>경기<span class="push-10-l"><?=$game['game_time'];?></span>
									                    	<span class="pull-right"></span>
									                    </h3>
									                </div>
									                <div class="tournament_content clearfix">
									                	<div class="pull-left text-center push-5-l">
															<?php
									                			$favor_sql = "select count(*) as cnt from app_user_favor where user_code = '$user_code' and match_code = '$game[match_code]' and team_code = '$game[team_1_code]'";
									                			$favor_result = sql_query($favor_sql);
																$favor = sql_fetch_array($favor_result);
																if($favor['cnt'] == 0){
															?>
															
									                    		<button class="btn_fav" onclick="favor('<?=$user_code?>','<?=$game['team_1_code']?>','<?=$game['match_code']?>')">
									                    	<?php
																}else{
															?>
																<button class="btn_fav active" onclick="favor('<?=$user_code?>','<?=$game['team_1_code']?>','<?=$game['match_code']?>')">
															<?php	
																}
									                		?>										                    	
									                			<div class="left-star">
											                		<i class="fa fa-star"></i>
											                	</div>
										                        <div class="font-w600"><?=$team1['club'];?></div>
										                        <div class=""><?=$team1['team_1_name'];?> <?=$team1['team_2_name'];?></div>
										                    </button>
									                    </div>
									                	<div class="tournament_point">
									                		<span class="left_team <?php if($game['team_1_score'] > $game['team_2_score']) echo "win";?>"><?=$game['team_1_score'];?></span>
									                		<span>vs</span>
									                		<span class="right_team <?php if($game['team_1_score'] < $game['team_2_score']) echo "win";?>"><?=$game['team_2_score'];?></span>
									                    </div>
									                	
									                    <div class="pull-right text-center push-5-r">
									                		<?php
									                			$favor_sql = "select count(*) as cnt from app_user_favor where user_code = '$user_code' and match_code = '$game[match_code]' and team_code = '$game[team_2_code]'";
									                			$favor_result = sql_query($favor_sql);
																$favor = sql_fetch_array($favor_result);
																if($favor['cnt'] == 0){
															?>
															
									                    		<button class="btn_fav" onclick="favor('<?=$user_code?>','<?=$game['team_2_code']?>','<?=$game['match_code']?>')">
									                    	<?php
																}else{
															?>
																<button class="btn_fav active" onclick="favor('<?=$user_code?>','<?=$game['team_2_code']?>','<?=$game['match_code']?>')">
															<?php	
																}
									                		?>
										                		<div class="right-star">
											                		<i class="fa fa-star"></i>
											                	</div>
										                        <div class="font-w600"><?=$team2['club'];?></div>
										                        <div class=""><?=$team2['team_1_name'];?> <?=$team2['team_2_name'];?></div>
									                        </button>
									                    </div>
									                </div>
									               
									            </div>
									        </div>
									        <!-- end 참가선수 정보 -->
									        <?php } } ?>	 
										</div>
										
			                    	</td>
			                    </tr>
			                    
			                    
			                    
			                </tbody>
			                
			            </table>
			            
			        </div>
			    </div>
	        	
	        </div>
	    </div>
	</div>
<!-- end Contents Area -->


<?php require 'inc/views/base_footer.php'; ?>
<?php require 'inc/views/template_footer_start.php'; ?>
<script>
function favor(user_code,team_code,match_code){
	$.ajax({
		type: 'post',
		url:'./favorite_add.php',
		dataType:'json',
		async: false,
		data: {user_code:user_code,team_code:team_code,code:match_code},
		success:function(data){
			console.log(data);
			location.reload(true);
			if(data[0]["result"] == "success"){
				window.AJInterface.callAndroid();
			}else if(data[0]["result"] == "cancel"){
				window.AJInterface.cancelAndroid();
			}
			// var str = '';
			// for(var sub in data){
				// console.log(sub);
				// str += '<option value='+data[sub]['series_sub']+'>'+data[sub]['series_sub']+'</option>';
			// }
			// console.log(str);
			
	   	}
    });
}

</script>

<!-- Page JS Plugins -->
<script src="<?php echo $one->assets_folder; ?>/js/plugins/select2/select2.full.min.js"></script>
<script src="<?php echo $one->assets_folder; ?>/js/plugins/datatables/jquery.dataTables.min.js"></script>


<!-- Page JS Code -->
<script src="<?php echo $one->assets_folder; ?>/js/pages/base_tables_datatables.js"></script>
<script>
    jQuery(function(){
        // Init page helpers (BS Datepicker + BS Datetimepicker + BS Colorpicker + BS Maxlength + Select2 + Masked Input + Range Sliders + Tags Inputs plugins)
        App.initHelpers(['table-tools','datepicker', 'datetimepicker', 'colorpicker', 'maxlength', 'select2', 'masked-inputs', 'rangeslider', 'tags-inputs']);
    });
</script>

<?php require 'inc/views/template_footer_end.php'; ?>

