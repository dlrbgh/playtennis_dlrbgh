<?php
include_once('../common.php');

$user_code	= $_REQUEST['user_code'];
$club	= $_REQUEST['user_club'];
$name	= $_REQUEST['user_name'];
$act = 'current_my';

$sql = "select * from match_data where wr_id = '$wr_id' order by wr_id desc";
$result = sql_query($sql);
$i = 1;
$r = sql_fetch_array($result);
$code = $r['code'];
?>
<?php require 'inc/config.php'; ?>
<?php require 'inc/views/template_head_start.php'; ?>


<?php require 'inc/views/template_head_end.php'; ?>

<!-- sub nav -->
<?php include_once('./app_sub_nav.php'); ?>
<!-- end sub nav -->

<!-- Contents Area -->
	
	<div class="content" style="margin-top: 42px;">
	    <div class="btn-group btn-group-justified push-10">
	        <div class="btn-group">
	            <a href="current_my_info.php?wr_id=<?=$wr_id;?>&user_code=<?=$user_code;?>&user_club=<?=$club;?>&user_name=<?=$name;?>"><button class="btn btn-lg btn-white active" type="button">경기 정보</button></a>
	        </div>
	        <div class="btn-group">
	            <a href="current_my_tm.php?wr_id=<?=$wr_id;?>&user_code=<?=$user_code;?>&user_club=<?=$club;?>&user_name=<?=$name;?>"><button class="btn btn-lg btn-white" type="button">토너먼트 정보</button></a>
	        </div>
		</div>
		
	    <!-- 등록된 선수정보가 없을경우 -->
	    <?php if($club == "" || $name == ""){?>
	    <div class="row text-center" style="height:100%;padding:50px 0 0 0">
	    	<div class="col-md-12">
	    		<h1 class="font-s128 font-w300 animated flipInX"><i class="fa fa-exclamation-circle"></i></h1>
	    		<div class="h4 font-w700 push-10">
		    		등록된 선수정보가 없습니다.
		    	</div>
		    	<div class="h4 font-w700 push-10">
		    		메뉴 > 설정 > 선수등록
		    	</div>
		    	<div class="h4 font-w700">
		    		선수 정보를 등록해 주세요.
		    	</div>
		    </div>
	    	<div class="col-md-12 push-20-t">
	    		<button class="btn btn-block btn-mobile push-10" onclick="insert_data()" type="button"><i class="fa fa-exclamation-circle pull-left"></i>선수정보 등록하기</button>
	    	</div>
	    	
	    </div>
	    
	    <?php }else{?>
		<?php
			$gym_sql = "select count(*) as cnt from team_data as a inner join match_data as b where a.club = '$club' and (a.team_1_name = '$name' or a.team_2_name = '$name') and a.match_code = b.code and b.end_game > '2' and a.match_code='$code'  group by code";
			$gym_result = sql_query($gym_sql);
			$gym = sql_fetch_array($gym_result);
			if($gym['cnt'] == 0){
		?>
		<!-- 대회에 참가 하지 않은경우 -->
	    <div class="row text-center" style="height:100%;padding:50px 0 0 0">
	    	<div class="col-md-12">
    		<h1 class="font-s128 font-w300 animated flipInX"><i class="fa fa-exclamation-circle"></i></h1>
    		<div class="h4 font-w700 push-10">
	    		<?=$r['wr_name']?>에 
	    	</div>
	    	<div class="h4 font-w700 push-10">
	    		<?=$name;?>님은 참가신청을 하지 않으셨습니다.
	    	</div>
	    </div>
	    <?php		
			}
		?>	    
		<?php }?>
	    <!-- end 대회에 참가 하지 않은경우 -->
	    
	    <!-- 웹으로 접속한경우 -->
	    <!-- <div class="row text-center">
	    	<div>
	    		내경기 서비스는 모바일 웹에서 지원하지 않습니다.
	    	</div>
	    	<div>
	    		콕선생 어플리케이션을 설치해 주시기 바랍니다.
	    	</div>
	    	<div>
	    		<a href="">구글플레이</a>
	    		<a href="">앱스토어</a>
	    	</div>
	    </div> -->
	    <!-- end 웹으로 접속한경우 -->
	    
	    <?php
			$gym_sql = "select * from team_data as a inner join match_data as b where a.club = '$club' and (a.team_1_name = '$name' or a.team_2_name = '$name') and a.match_code = b.code and b.end_game > '2' and a.match_code='$code'  group by code";
			$gym_result = sql_query($gym_sql);
			while($gym = sql_fetch_array($gym_result)){
		?>
		<div class="push-10">
			<h5><i class="fa fa-calendar-check-o push-5-r"></i><span class="text-primary font-w700"><?=$name;?></span>님이 출전하는 경기목록 입니다.</h5>
		</div>
	    <!-- 접수현황 -->
	    <div class="row">
	    	<div class="col-lg-6">
	        	<div class="block">
			            <table class="table table-mobile">
			                <!-- 그룹 경기 조회 타이틀 -->
			                <tbody class="js-table-sections-header open">
			                    <tr>
			                        <td class="font-w600" width="85%"><?=$gym['wr_name'];?></td>
			                    </tr>
			                </tbody>
			                <!-- end 그룹 경기 조회 타이틀 -->
			                <tbody>
			                	<?php 
			                		$team_sql = "select * from team_data as a where a.club = '$club' and (a.team_1_name = '$name' or a.team_2_name = '$name') and a.match_code = '$gym[code]'";
									$team_result = sql_query($team_sql);
									while($team = sql_fetch_array($team_result)){
										$match_gym_sql = "select * from game_score_data as a inner join gym_data as b where (a.team_1_code = '$team[team_code]' or a.team_2_code = '$team[team_code]') and a.match_code = '$team[match_code]' and a.gym_code = b.wr_id group by a.game_date";
										$match_gym_result = sql_query($match_gym_sql);
										$match_gym = sql_fetch_array($match_gym_result);
			                	?>
			                    <tr>
			                    	<td class="mychamp_list">
			                    		<div class="title">
			                    			<span class=""><?=$team['division'];?> <?=$team['series'];?> <?=$team['series_sub'];?></span><span class="push-10-l">(<?=$match_gym['gym_name']?>)</span>
			                    			<span class="pull-right"><a href="champ_info_match_view.php?wr_id=<?=$gym['wr_id']?>&user_code=<?=$user_code;?>&user_club=<?=$user_club;?>&user_name=<?=$user_name;?>&division=<?=$team['division']?>&series=<?=$team['series']?>&series_sub=<?=$team['series_sub']?>">바로가기</a></span>
			                    		</div>
			                    		
			                    		<div class="row tournament_match">
			                    			<!-- 참가선수 정보 -->
			                    			<?php
							            		$score_sql = "select * from game_score_data where (team_1_code = '$team[team_code]' or team_2_code = '$team[team_code]') and match_code = '$team[match_code]' order by game_increase";
												$score_result = sql_query($score_sql);
												while($score = sql_fetch_array($score_result)){
															
													$sql_team1 = "select * from team_data where team_code = '$score[team_1_code]'";
													$team1_result = sql_query($sql_team1);
													$team1 = sql_fetch_array($team1_result);
													
													$sql_team2 = "select * from team_data where team_code = '$score[team_2_code]'";
													$team2_result = sql_query($sql_team2);
													$team2 = sql_fetch_array($team2_result);
								            ?>
			                    			<div class="col-xs-12 col-sm-6 col-md-6">
									            <div class="block block-rounded">
									                <div class="block-options push-10-t push-10-r">
										                <li>
										                   <?=$score['game_date'];?>
										                </li>
										            </div>
										            <div class="tournament_header">
									                    <h3 class="block-title">
									                    	<span><?=$score['game_court'];?>코트 <?=$score['court_array_num'];?>번 경기</span>
									                    	<span class="push-10-l"><?=$score['game_time'];?></span>
								                    	   	<?php if($score['tournament'] == "T"){?>
										                    <span class="push-10-l">토너먼트<?=$score['tournament_count']*2;?>강</span>
											               	<?php }?>
											               	<?php if($score['tournament'] == "C"){?>
										                    <span class="push-10-l">결승</span>
											               	<?php }?>
											                <span class="pull-right"></span>
									                    </h3>
									                </div>
									                <div class="tournament_content clearfix">
									                	<div class="pull-left text-center push-5-l">
															    <div class="font-w600"><?=$team1['club'];?></div>
										                        <div class=""><?=$team1['team_1_name'];?> <?=$team1['team_2_name'];?></div>
										                </div>
									                	<div class="tournament_point">
									                		<span class="left_team <?php if($score['team_1_score'] > $score['team_2_score']) echo "win";?>"><?=$score['team_1_score'];?></span>
									                		<span>vs</span>
									                		<span class="right_team <?php if($score['team_1_score'] < $score['team_2_score']) echo "win";?>"><?=$score['team_2_score'];?></span>
									                    </div>
									                    <div class="pull-right text-center push-5-r">
									                		    <div class="font-w600"><?=$team2['club'];?></div>
										                        <div class=""><?=$team2['team_1_name'];?> <?=$team2['team_2_name'];?></div>
									                    </div>
									                </div>
									               
									            </div>
									        </div>
									        <!-- end 참가선수 정보 -->
									         <?php } ?>
										</div>
			                    	</td>
			                    </tr>
			                    <?php } ?>
			                </tbody>
			            </table>
			    </div>
	        	
	        </div>
	    </div>
	<?php } ?>
	</div>
<!-- end Contents Area -->


<?php require 'inc/views/base_footer.php'; ?>
<?php require 'inc/views/template_footer_start.php'; ?>

<script>
	function insert_data(){
		window.AJInterface.insert_info();
	}
</script>


<?php require 'inc/views/template_footer_end.php'; ?>

