<?php
include_once('../common.php');

$wr_id 		= $_REQUEST['wr_id'];
$division   = $_REQUEST['division'];
$series 	= $_REQUEST['series'];
$series_sub = $_REQUEST['series_sub'];
$user_code	= $_REQUEST['user_code'];

?>
<?php require 'inc/config.php'; ?>
<?php require 'inc/views/template_head_start.php'; ?>

<!-- Page JS Plugins CSS -->
<link rel="stylesheet" href="<?php echo $one->assets_folder; ?>/js/plugins/slick/slick.min.css">
<link rel="stylesheet" href="<?php echo $one->assets_folder; ?>/js/plugins/slick/slick-theme.min.css">
<link rel="stylesheet" href="<?php echo $one->assets_folder; ?>/js/plugins/select2/select2.min.css">
<link rel="stylesheet" href="<?php echo $one->assets_folder; ?>/js/plugins/select2/select2-bootstrap.min.css">
<?php require 'inc/views/template_head_end.php'; ?>

<?php 
	$sql = "select * from match_data where wr_id = '$wr_id' order by wr_id desc";
	$result = sql_query($sql);
	$i = 1;
	$r = sql_fetch_array($result);
    $code = $r['code'];
?>

<div class="menu_snav top_nav" id="home_tabs"  style="top: 0;">
    <a href="index.php?user_code=<?=$user_code;?>">홈</a></li> 
    <a class="" href="championship_info.php?wr_id=<?=$wr_id?>&user_code=<?=$user_code;?>">정보</a> 
    <a class="" href="champ_info_apply.php?wr_id=<?=$wr_id?>&user_code=<?=$user_code;?>">접수현황</a>
    <a class="active" href="champ_info_matchtbl_level.php?wr_id=<?=$wr_id?>&user_code=<?=$user_code;?>">대진표</a> 
    <a href="champ_info_match.php?wr_id=<?=$wr_id?>&user_code=<?=$user_code;?>">경기진행</a> 
    <a href="champ_info_endmatch.php?wr_id=<?=$wr_id?>&user_code=<?=$user_code;?>">결과</a> 
</div>
<!-- Contents Area -->
	
	<div class="content" style="margin-top: 42px;">
	
		<div class="btn-group btn-group-justified push-10">
	        <div class="btn-group">
	            <a href="champ_info_matchtbl_level.php?wr_id=<?=$wr_id;?>&user_code=<?=$user_code;?>"><button class="btn btn-lg btn-white active" type="button">급수별 편성표</button></a>
	        </div>
	        <div class="btn-group">
	            <a href="champ_info_matchtbl_time.php?wr_id=<?=$wr_id;?>&user_code=<?=$user_code;?>"><button class="btn btn-lg btn-white" type="button">대진 시간표</button></a>
	        </div>
		</div>
	    
	    <div style="height:100%;">
		    <div style="padding:10px;text-align:center;background-color:#2c343f;color:#fff">
	        	남성복식
	        </div>
	        <div style="padding:10px;text-align:center;background-color:#2c343f;color:#fff">
	        	여성복식
	        </div>
	        <div style="padding:10px;text-align:center;background-color:#2c343f;color:#fff">
	        	혼합복식
	        </div>
        </div>
	    <!-- 대진 시간표 조회 -->
	    <div class="row">
	    	<!-- 조회(급수별 조회) -->
	    	<div class="col-lg-6">
				<div class="block block-themed">
	                테스트
	            </div>
			</div>
	    	<!-- end 조회(급수별 조회) -->
	    </div>
	    <!-- end 대진 시간표 조회 -->
	    
	    <!-- 접수현황 -->
	    
	</div>
<!-- end Contents Area -->


<?php require 'inc/views/base_footer.php'; ?>
<?php require 'inc/views/template_footer_start.php'; ?>


<!-- Page JS Plugins -->
<script src="<?php echo $one->assets_folder; ?>/js/plugins/datatables/jquery.dataTables.min.js"></script>


<!-- Page JS Code -->
<script src="<?php echo $one->assets_folder; ?>/js/pages/base_tables_datatables.js"></script>
<script>
    jQuery(function(){
        App.initHelpers(['table-tools']);
    });
</script>

<?php require 'inc/views/template_footer_end.php'; ?>

