
<!DOCTYPE html>
<html>
<head>
<title>Page Title</title>

<style>
body{
  display:table-cell;
}
  #imade{
    vertical-align: top;
    display:inline-block;
    width:40%;
  }
  #test{
    vertical-align: top;
    width:40%;
    display:inline-block;
    margin-left:10px
  }
</style>

<script src="//ajax.googleapis.com/ajax/libs/jquery/1/jquery.min.js"></script>
<script type="text/javascript" src="../js/tournament/jquery.bracket.min.print.js"></script>
<link rel="stylesheet" type="text/css" href="../js/tournament/jquery.bracket.min.print.css" />



</head>
<body>
<?php
  $round ;
  $round[2] = "['1- 1', '1- 2']";
  $round[4] = "['1- 1', '1- 2'], ['2- 1', '1- 2']";
  $round[6] = "['1- 1', null], ['2- 2', '3- 2']
         ,['3- 1', '1- 2'], [null, '2- 1']";
  $round[8] = "['1- 1', '2- 2'], ['4- 1', '3- 2']
    ,['3- 1', '4- 2	'], ['1- 2	', '2- 1']";
  $round[10] = " ['1- 1', null], ['2- 2', '3- 2']
        , ['5- 1',null], ['4- 1',null]
        , ['3- 1', null], ['4- 2',null]
        , ['5- 2','1- 2'], ['2- 1',null]";
  $round[12] = " ['1- 1', null], ['2- 2', '6- 2']
        , ['5- 1','3- 2'], ['4- 1',null]
        , ['3- 1', null], ['4- 2','6- 1']
        , ['5- 2','1- 2'], ['2- 1',null]";
  $round[14] = " ['1- 1',null], ['7- 2','2- 2	']
         , ['5- 1','6- 2'], ['3- 2','4- 1']
         , ['3- 1','4- 2'], ['5- 2','6- 1']
         , ['7- 1','1- 2'], ['2- 1',null]";
   $round[16] = " ['1- 1','2- 2'], ['7- 2','8- 1']
         , ['5- 1','6- 2'], ['3- 2','4- 1']
         , ['3- 1','4- 2'], ['5- 2','6- 1']
         , ['7- 1','8- 2'], ['2- 1','1- 2']";

   $round[18] = " ['1- 1',null], ['2- 2','7- 2']
         , ['9- 1',null], [null,'8- 1']
         , ['5- 1',null], [null,'6- 2']
         , ['3- 2',null], [null,'4- 1']
         , ['3- 1',null], [null,'4- 2']
         , ['5- 2',null], [null,'6- 1']
         , ['7- 1',null], [null,'8- 2']
         , ['9- 2','1- 2'], [null,'2- 1']";


  $round[20] = "['1- 1',null], ['2- 2','10- 2']
        , ['9- 1',null], ['8- 1',null]
        , ['5- 1',null], ['7- 2',null]
        , ['6- 2','3- 2'], ['4- 1',null]
        , ['3- 1',null], ['4- 2','5- 2']
        , ['8- 2',null], ['6- 1',null]
        , ['7- 1',null], ['10- 1',null]
        , ['9- 2','1- 2'], ['2- 1',null]";

  $round[24] = "['1- 1',null], ['2- 2','10- 2']
        , ['9- 1','7- 2'], ['8- 1',null]
        , ['5- 1',null], ['6- 2','12- 1']
        , ['11- 2','3- 2'], ['4- 1',null]
        , ['3- 1',null], ['4- 2','12- 2']
        , ['11- 1','5- 2'], ['6- 1',null]
        , ['7- 1',null], ['8- 2','10- 1']
        , ['9- 2','1- 2'], ['2- 1',null]";

  $round[28] = "['1- 1',null], ['2- 2','10- 2']
        , ['9- 1','7- 2'], ['8- 1','6- 2']
        , ['5- 1','11- 2'], ['14- 2','12- 1']
        , ['13- 1','3- 2'], ['4- 1',null]
        , ['3- 1',null], ['4- 2','14- 1']
        , ['11- 1','13- 2'], ['12- 2','6- 1']
        , ['7- 1','5- 2'], ['8- 2','10- 1']
        , ['9- 2','1- 2'], ['2- 1',null]";

  $round[32] = "['1- 1','2- 2'], ['15- 2','16- 1']
        , ['9- 1','10- 2'], ['8- 1','7- 2']
        , ['5- 1','6- 2'], ['11- 2','12- 1']
        , ['13- 1','14- 2'], ['3- 2','4- 1']
        , ['3- 1','4- 2'], ['13- 2','14- 1']
        , ['11- 1','12- 2'], ['5- 2','6- 1']
        , ['7- 1','8- 2'], ['9- 2','10- 1']
        , ['15- 1','16- 2'], ['1- 2','2- 1']";


        //
        // teamWidth: 60,
        //   : 20,
        //   matchMargin: 10,
        //   : 50,

$teamWidth = (!isset($_REQUEST['teamWidth']) || $_REQUEST['teamWidth'] == '') ? 170 : $_REQUEST['teamWidth'];
$scoreWidth = (!isset($_REQUEST['scoreWidth']) || $_REQUEST['scoreWidth'] == '') ? 60 : $_REQUEST['scoreWidth'];
$matchMargin = (!isset($_REQUEST['matchMargin']) || $_REQUEST['matchMargin'] == '') ? 156 : $_REQUEST['matchMargin'];
$roundMargin = (!isset($_REQUEST['roundMargin']) || $_REQUEST['roundMargin'] == '') ? 50 : $_REQUEST['roundMargin'];



 ?>

 <div id="test">
 </div>
<script>


var singleElimination = {
  "teams": [
    <?=$round[$_REQUEST['teams']]?>
  ]
}

$('#test').bracket({
  teamWidth:<?php print $teamWidth?>
  , scoreWidth:<?php print $scoreWidth?>
  , matchMargin:<?php print $matchMargin?>
  , roundMargin:<?php print $roundMargin?>
  ,init:singleElimination})

</script>
