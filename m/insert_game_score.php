<?php
include_once('../common.php');
include_once('./lib/common.lib.php');

$rows = array();

$wr_id 				= $_REQUEST['wr_id'];
$division			= $_REQUEST['division'];
$series 			= $_REQUEST['series'];
$series_sub			= $_REQUEST['series_sub'];
$match_code			= $_REQUEST['match_code'];
$modify				= $_REQUEST['modify'];

$tournament			= $_REQUEST['tournament'];
$tournament_count	= $_REQUEST['tournament_count'];

$team_1_score		= (int)$_REQUEST['team_1_score'];
$team_2_score		= (int)$_REQUEST['team_2_score'];

$team_1_score_before	= $_REQUEST['team_1_score_before'];
$team_2_score_before	= $_REQUEST['team_2_score_before'];
$require_url	= $_REQUEST['rd'];

if($wr_id == ""){
	$rows = array();
	$rows[]['result'] = "false";
	echo json_encode($rows);
	return;
}

if($division == ""){
	$rows = array();
	$rows[]['result'] = "false";
	echo json_encode($rows);
	return;
}
if($series == ""){
	$rows = array();
	$rows[]['result'] = "false";
	echo json_encode($rows);
	return;
}
if($match_code == ""){
	$rows = array();
	$rows[]['result'] = "false";
	echo json_encode($rows);
	return;
}
if($modify == ""){
	$rows = array();
	$rows[]['result'] = "false";
	echo json_encode($rows);
	return;
}

if($tournament == ""){
	$rows = array();
	$rows[]['result'] = "false";
	echo json_encode($rows);
	return;
}

if($tournament_count == ""){
	$rows = array();
	$rows[]['result'] = "false";
	echo json_encode($rows);
	return;
}

//점수 삽입
$team_field = "team_data";

if($division == "단체전"){
	$team_field = "team_event_data";
}

if($team_1_dis == "1" || $team_1_dis == "2" ){
	$sql = " update game_score_data
            set
                 team_1_score 	= '0',
                 team_2_score 	= '6',
                 end_game 		= 'Y',
                 team_1_dis 	= '$team_1_dis'
            where wr_id = '$wr_id' and match_code = '$match_code'";
	sql_query($sql);
}
else if($team_2_dis == "1" || $team_2_dis == "2" ){
	$sql = " update game_score_data
            set
                 team_1_score 	= '6',
                 team_2_score 	= '0',
                 end_game 		= 'Y',
                 team_2_dis 	= '$team_2_dis'
            where wr_id = '$wr_id' and match_code = '$match_code'";
	sql_query($sql);
}
else{
	$sql = " update game_score_data
            set
                 team_1_score 	= '$team_1_score',
                 team_2_score 	= '$team_2_score',
                 end_game 		= 'Y',
                 team_1_dis 	= '$team_1_dis',
                 team_2_dis 	= '$team_2_dis'

            where wr_id = '$wr_id' and match_code = '$match_code'";
	sql_query($sql);
}

$group_code_sql  = "select * from game_score_data where wr_id = '$wr_id' and match_code = '$match_code'";
$group_code_result = sql_query($group_code_sql);
$group_code = sql_fetch_array($group_code_result);

if($wr_text != ""){
	$sql = " insert into game_score_data_history
            set
                 wr_text 	= '$wr_text',
                 game_code 	= '$group_code[code]',
                 match_code = '$match_code',
                 wr_datetime = '".G5_TIME_YMDHIS."',
				mb_id = '$member[mb_id]',
				wr_ip = '{$_SERVER['REMOTE_ADDR']}'";
	sql_query($sql);
}


/**
 * 퓌쉬전송 시작
 */
//
// //관심 경기 푸쉬 전송 이전 경기가 시작되면 푸쉬를 전송한다.
// $three_cournt = $group_code['court_array_num']+3;
// $two_cournt = $group_code['court_array_num']+2;
// $one_cournt = $group_code['court_array_num']+1;
// $gym_code = $group_code['gym_code'];
// $game_date = $group_code['game_date'];
// $game_court = $group_code['game_court'];
// $court_array_num = $group_code['court_array_num'];
//
// $three_push_sql = "select * from game_score_data where match_code = '$match_code' and game_court = '$game_court' and court_array_num = '$one_cournt' and gym_code = '$gym_code' and game_date = '$game_date'";
// $three_push_result = sql_query($three_push_sql);
// $three_push_code = sql_fetch_array($three_push_result);
//
// $ios_array = array();
// $andorid_array = array();
//
// $team_1_code = $three_push_code['team_1_code'];
// $team_2_code = $three_push_code['team_2_code'];
// $game_time 	 = $three_push_code['game_time'];
// $push_court  = $three_push_code['game_court'];
//
// //아이폰 푸쉬전송
//
// // ";
// $push_user_sqls = "select * from app_user_favor where match_code = '$match_code' and ( team_code = '$team_1_code' or team_code = '$team_2_code' )";
// $push_user_result = sql_query($push_user_sqls);
// while($push_user_code = sql_fetch_array($push_user_result)){
	// $users_code  = $push_user_code['user_code'];
//
	// $team_sql = "select * from $team_field where match_code = '$match_code' and team_code = '$push_user_code[team_code]'";
	// $team_result = sql_query($team_sql);
	// $team = sql_fetch_array($team_result);
//
	// $user_sql = "select * from app_user_token where user_code = '$users_code'";
	// $user_result = sql_query($user_sql);
	// while($code = sql_fetch_array($user_result)){
		// if($code['device'] == "iphone"){
			// $ios_array = $code['pushtoken'];
			// //아이폰 푸쉬 전송
			// $deviceToken = $ios_array; // 디바이스토큰ID
			// $message = "$team[club]"."클럽 $team[team_1_name]"."/"."$team[team_2_name]"."팀의 경기가  $push_court"."코트에서 시작됩니다.";
//
			// ios_push($deviceToken,$message);
		// }
		// if($code['device'] == "android"){
			// $andorid_array = array();
			// $andorid_array[] = $code['pushtoken'];
//
			// //안드로이드 푸쉬전송
			// $message = "$team[club]"."클럽 $team[team_1_name]"."/"."$team[team_2_name]"."팀의 경기가  $push_court"."코트에서 시작됩니다.";
//
			// android_push($andorid_array,$message);
		// }
	// }
// }

//나의 경기 푸쉬 전송
//-3경기시 푸쉬전송
//아이폰 푸쉬전송

//-3경기시 푸쉬전송
//아이폰 푸쉬전송
// $two_push_sql = "select * from game_score_data where match_code = '$match_code' and game_court = '$game_court' and court_array_num = '$two_cournt' and gym_code = '$gym_code' and game_date = '$game_date'";
// $two_push_result = sql_query($two_push_sql);
// $two_push_code = sql_fetch_array($two_push_result);
//
// $ios_array = array();
// $andorid_array = array();
//
// $team_1_code = $two_push_code['team_1_code'];
// $team_2_code = $two_push_code['team_2_code'];
// $game_court  = $two_push_code['game_court'];
//
// $sql_team1 = "select * from team_data where team_code = '$team_1_code' and match_code = '$match_code'";
// $team1_result = sql_query($sql_team1);
// $team1 = sql_fetch_array($team1_result);
//
// $sql_team2 = "select * from team_data where team_code = '$team_2_code' and match_code = '$match_code'";
// $team2_result = sql_query($sql_team2);
// $team2 = sql_fetch_array($team2_result);
//
// $user_sql = "select * from app_user_token where user_club = '$team1[club]' and user_club != '' and user_name = '$team1[team_1_name]' and user_name != ''";
// $user_result = sql_query($user_sql);
//
// while($code = sql_fetch_array($user_result)){
	// if($code['device'] == "iphone"){
		// $ios_array = $code['pushtoken'];
		// //아이폰 푸쉬 전송
		// $deviceToken = $ios_array; // 디바이스토큰ID
		// $message = "$team1[team_1_name]님의 경기가 20분 이내에 시작합니다. $game_court"."번 코트에서 준비해주시기 바랍니다.";
		// ios_push($deviceToken,$message);
	// }
	// if($code['device'] == "android"){
		// $andorid_array = array();
		// $andorid_array[] = $code['pushtoken'];
//
		// $message = "$team1[team_1_name]님의 경기가 20분 이내에 시작합니다. $game_court"."번 코트에서 준비해주시기 바랍니다.";
		// android_push($andorid_array,$message);
	// }
// }
//
// $user_sql = "select * from app_user_token where user_club = '$team1[club]' and user_club != '' and user_name = '$team1[team_2_name]' and user_name != ''";
// $user_result = sql_query($user_sql);
//
// while($code = sql_fetch_array($user_result)){
	// if($code['device'] == "iphone"){
		// $ios_array = $code['pushtoken'];
		// //아이폰 푸쉬 전송
		// $deviceToken = $ios_array; // 디바이스토큰ID
		// $message = "$team1[team_2_name]님의 경기가 20분 이내에 시작합니다. $game_court"."번 코트에서 준비해주시기 바랍니다.";
		// ios_push($deviceToken,$message);
	// }
	// if($code['device'] == "android"){
		// $andorid_array = array();
		// $andorid_array[] = $code['pushtoken'];
//
		// $message = "$team1[team_2_name]님의 경기가 20분 이내에 시작합니다. $game_court"."번 코트에서 준비해주시기 바랍니다.";
		// android_push($andorid_array,$message);
	// }
// }
//
// $user_sql = "select * from app_user_token where user_club = '$team2[club]' and user_club != '' and user_name = '$team2[team_1_name]' and user_name != ''";
// $user_result = sql_query($user_sql);
// while($code = sql_fetch_array($user_result)){
	// if($code['device'] == "iphone"){
		// $ios_array = $code['pushtoken'];
		// //아이폰 푸쉬 전송
		// $deviceToken = $ios_array; // 디바이스토큰ID
		// $message = "$team2[team_1_name]님의 경기가 20분 이내에 시작합니다. $game_court"."번 코트에서 준비해주시기 바랍니다.";
//
		// ios_push($deviceToken,$message);
	// }
	// if($code['device'] == "android"){
		// $andorid_array = array();
		// $andorid_array[] = $code['pushtoken'];
//
		// $message = "$team2[team_1_name]님의 경기가 20분 이내에 시작합니다. $game_court"."번 코트에서 준비해주시기 바랍니다.";
		// android_push($andorid_array,$message);
	// }
// }
//
// $user_sql = "select * from app_user_token where user_club = '$team2[club]' and user_club != '' and user_name = '$team2[team_2_name]' and user_name != ''";
// $user_result = sql_query($user_sql);
// while($code = sql_fetch_array($user_result)){
	// if($code['device'] == "iphone"){
		// $ios_array = $code['pushtoken'];
		// //아이폰 푸쉬 전송
		// $deviceToken = $ios_array; // 디바이스토큰ID
		// $message = "$team2[team_2_name]님의 경기가 20분 이내에 시작합니다. $game_court"."번 코트에서 준비해주시기 바랍니다.";
//
		// ios_push($deviceToken,$message);
	// }
	// if($code['device'] == "android"){
		// $andorid_array = array();
		// $andorid_array[] = $code['pushtoken'];
//
		// $message = "$team2[team_2_name]님의 경기가 20분 이내에 시작합니다. $game_court"."번 코트에서 준비해주시기 바랍니다.";
		// android_push($andorid_array,$message);
	// }
// }

/**
 * 퓌쉬전송 완료 수정 및 토너먼트 입력 개발
 */

//수정시에 토너먼트 데이터 초기화 후 다시 토너먼트 배치
if($modify == 1){
	if($tournament == "L"){
		$tournament_reset_sql = "update game_score_data set
                 team_1_score = '',
                 team_2_score = '',
                 team_1_code = '',
                 team_2_code = '',
                 end_game = 'N'
                 where match_code = '$match_code' and tournament != 'L' and division = '$division' and series = '$series' and series_sub = '$series_sub' ";
		sql_query($tournament_reset_sql);
	}else if($tournament == "T" && $tournament_count == "16"){

		$tournament_reset_sql = "update game_score_data set
                 team_1_score = '',
                 team_2_score = '',
                 team_1_code = '',
                 team_2_code = ''
                 where match_code = '$match_code' and tournament = 'c' and tournament_count = '0' and division = '$division' and series = '$series' and series_sub = '$series_sub'";
		sql_query($tournament_reset_sql);

	}else if($tournament == "T" && $tournament_count == "8"){

		$tournament_reset_sql = "update game_score_data set
                 team_1_score = '',
                 team_2_score = '',
                 team_1_code = '',
                 team_2_code = '',
                 end_game = 'N'
                 where match_code = '$match_code' and tournament = 'c' and tournament_count = '0' and division = '$division' and series = '$series' and series_sub = '$series_sub'";
		sql_query($tournament_reset_sql);

	}else if($tournament == "T" && $tournament_count == "4"){
		// $tournament_reset_sql = "update game_score_data set
                 // team_1_score = '',
                 // team_2_score = '',
                 // team_1_code = '',
                 // team_2_code = '',
                 // end_game = 'N'
                 // where match_code = '$match_code' and tournament = 'T' and tournament_count = '2' and division = '$division' and series = '$series' and series_sub = '$series_sub'";
		// sql_query($tournament_reset_sql);

		$tournament_reset_sql = "update game_score_data set
                 team_1_score = '',
                 team_2_score = '',
                 team_1_code = '',
                 team_2_code = '',
                 end_game = 'N'
                 where match_code = '$match_code' and tournament = 'c' and tournament_count = '0' and division = '$division' and series = '$series' and series_sub = '$series_sub'";
		sql_query($tournament_reset_sql);

	}else if($tournament == "T" && $tournament_count == "2"){

		$tournament_reset_sql = "update game_score_data set
                 team_1_score = '',
                 team_2_score = '',
                 team_1_code = '',
                 team_2_code = '',
                 end_game = 'N'
                 where match_code = '$match_code' and tournament = 'c' and tournament_count = '0' and division = '$division' and series = '$series' and series_sub = '$series_sub'";
		sql_query($tournament_reset_sql);
	}

	if($tournament == "L"){
		if($team_1_score_before > $team_2_score_before){
			$sql  = "select * from game_score_data where wr_id = '$wr_id' and match_code = '$match_code'";
			$result = sql_query($sql);
			$r = sql_fetch_array($result);
			$team1 = $r['team_1_code'];
			$update = " update $team_field
		            set
		            	team_league_point	 = team_league_point - 1,
		                team_total_match_point =  team_total_match_point - '$team_1_score_before',
		                team_total_match_minus_point = team_total_match_minus_point - '$team_2_score_before'
		            where team_code = '$team1' and match_code = '$match_code'";
			sql_query($update);
			$team2 = $r['team_2_code'];
			$update = " update $team_field
		            set
		            	team_league_lose_count	 = team_league_lose_count - 1,
		                team_total_match_point = team_total_match_point - '$team_2_score_before',
		                team_total_match_minus_point = team_total_match_minus_point - '$team_1_score_before'
		            where team_code = '$team2' and match_code = '$match_code'";
			sql_query($update);
		}if($team_1_score_before < $team_2_score_before){
			$sql  = "select * from game_score_data where wr_id = '$wr_id' and match_code = '$match_code'";
			$result = sql_query($sql);
			$r = sql_fetch_array($result);

			$team1 = $r['team_1_code'];

			$updatedd = " update $team_field
		            set
		            	team_league_lose_count	 = team_league_lose_count - 1,
		                team_total_match_point = team_total_match_point - '$team_1_score_before',
		                team_total_match_minus_point = team_total_match_minus_point - '$team_2_score_before'
		            where team_code = '$team1' and match_code = '$match_code'";
			sql_query($updatedd);

			$team2 = $r['team_2_code'];

			$updateee = " update $team_field
		            set
		            	team_league_point	 = team_league_point - 1,
		                team_total_match_point = team_total_match_point - '$team_2_score_before',
		                team_total_match_minus_point =  team_total_match_minus_point - '$team_1_score_before'
		            where team_code = '$team2' and match_code = '$match_code'";
			sql_query($updateee);
		}
	}
}

if($tournament == "L"){
	if($team_1_score > $team_2_score){
		$sql  = "select * from game_score_data where wr_id = '$wr_id' and match_code = '$match_code'";
		$result = sql_query($sql);
		$r = sql_fetch_array($result);

		$team1 = $r['team_1_code'];
		$update = " update $team_field
	            set
	            	team_league_point	 = team_league_point + 1,
	                team_total_match_point =  team_total_match_point + '$team_1_score',
	                team_total_match_minus_point = team_total_match_minus_point + '$team_2_score'
	            where team_code = '$team1' and match_code = '$match_code'";
		sql_query($update);
		$team2 = $r['team_2_code'];
		$update = " update $team_field
	            set
	            	team_league_lose_count	 = team_league_lose_count + 1,
	                team_total_match_point = team_total_match_point + '$team_2_score',
	                team_total_match_minus_point = team_total_match_minus_point + '$team_1_score'
	            where team_code = '$team2' and match_code = '$match_code'";
		sql_query($update);
	}

	if($team_1_score < $team_2_score){
		$sql  = "select * from game_score_data where wr_id = '$wr_id' and match_code = '$match_code'";
		$result = sql_query($sql);
		$r = sql_fetch_array($result);

		$team1 = $r['team_1_code'];

		$update = " update $team_field
	            set
	            	team_league_lose_count	 = team_league_lose_count + 1,
	                team_total_match_point = team_total_match_point + '$team_1_score',
	                team_total_match_minus_point = team_total_match_minus_point + '$team_2_score'
	            where team_code = '$team1' and match_code = '$match_code'";
		sql_query($update);

		$team2 = $r['team_2_code'];
		$update = " update $team_field
	            set
	            	team_league_point	 = team_league_point + 1,
	                team_total_match_point = team_total_match_point + '$team_2_score',
	                team_total_match_minus_point =  team_total_match_minus_point + '$team_1_score'
	            where team_code = '$team2' and match_code = '$match_code'";
		sql_query($update);
	}
}

//그룹코드 로 game_score_data에 대이터를 검색한다. 모든 데이터가 입력되었다면 해당 그룹네 팀을 검색하여서 그룹내 순위를 결정한다.
if($tournament == "L"){
	$tournament_create = '1';

	//현재 등급의 모든 점수가 입력되었는지 검사한다.
	$sql_d = "select count(wr_id) as cnt from game_score_data where and team_1_score = '' and team_2_score = '' and match_code = '$match_code' and group_code = '$group_code[group_code]'";
	$result = sql_query($sql_d);
	$cnt = sql_fetch_array($result);

	if($cnt['cnt'] == 0){

		$group_sql = "select * from group_data where match_code = '$match_code' and code = '$group_code[group_code]'";
		$result = sql_query($group_sql);
		$group_data = sql_fetch_array($result);

		//검색된 팀의 승률(winning_rate)과 득실차(gains_losses_point)를 계산한다.
		$group_sqls = "select * from $team_field where match_code = '$match_code' and (team_code = '$group_data[team_1]' OR team_code = '$group_data[team_2]' OR team_code = '$group_data[team_3]') ORDER BY team_league_point desc ,winning_rate desc	,gains_losses_point desc, team_total_match_point DESC";
		$result = sql_query($group_sqls);
		//검색된 순위대로 그룹 랭크를 매긴다.
		while($team = sql_fetch_array($result)){

			$win_rate = $team['team_league_point']/($team['team_league_point']+$team['team_league_lose_count']);
			$gains_losses_point = $team['team_total_match_point'] - $team['team_total_match_minus_point'];
			$wr_ids = $team['wr_id'];
			$sqls = " update team_data
			            set
			                 winning_rate = '$win_rate',
			                 gains_losses_point = '$gains_losses_point'
			            where wr_id = '$wr_ids' and match_code = '$match_code'";
			sql_query($sqls);
		}
		//그룹내 팀의 등수를 설정하는데 승수, 승률, 득실차, 총 득점수를 기준으로 정렬한다.
		$group_sqls = "select * from $team_field where match_code = '$match_code' and (team_code = '$group_data[team_1]' OR team_code = '$group_data[team_2]' OR team_code = '$group_data[team_3]' OR team_code = '$group_data[team_4]' OR team_code = '$group_data[team_5]') ORDER BY team_league_point desc ,winning_rate desc ,gains_losses_point desc, team_total_match_point DESC";
		$rank_result = sql_query($group_sqls);
		//검색된 순위대로 그룹 랭크를 매긴다.
		$rank = 1;
		//승자승 체크기능 필요
		$chk_array = array();
		while($team = sql_fetch_array($rank_result)){
			$chk_array[] = $team;
		}

		// //1위와 2위가 모든게 같다면
		// if($chk_array[0]['team_league_point'] == $chk_array[1]['team_league_point'] && $chk_array[0]['winning_rate'] == $chk_array[1]['winning_rate'] ){
			// //1위2위3위가 같다면
			// //1위와 2위가 한 경기를 검색하여서 1위와 2위를 정한다.
				// $sql_d = "select * from game_score_data where ( team_1_code = '{$chk_array[0][team_code]}' and team_2_code = '{$chk_array[1][team_code]}') and match_code = '$match_code' and group_code = '$group_code[group_code]'";
				// $result = sql_query($sql_d);
				// $chk = sql_fetch_array($result);
				// if($chk == ""){
					// $sql_d = "select * from game_score_data where ( team_1_code = '{$chk_array[1][team_code]}' and team_2_code = '{$chk_array[0][team_code]}') and match_code = '$match_code' and group_code = '$group_code[group_code]'";
					// $result = sql_query($sql_d);
					// $chk = sql_fetch_array($result);
//
					// if($chk['team_1_score'] > $chk['team_2_score']){
						// $team1 = $chk_array['1'];
						// $team2 = $chk_array['0'];
//
						// $chk_array['0'] = $team1;
						// $chk_array['1'] = $team2;
//
						// $sqlsssdd = $sql_d."{$team1['team_code']}"."{$team2['team_code']}";
//
//
					// }
					// if($chk['team_1_score'] < $chk['team_2_score']){
						// $team1 = $chk_array['1'];
						// $team2 = $chk_array['0'];
//
						// $chk_array['0'] = $team2;
						// $chk_array['1'] = $team1;
//
						// //$sqlsssdd = $sql_d."{$team1['team_code']}"."{$team2['team_code']}";
//
					// }
//
				// }else{
					// if($chk['team_1_score'] > $chk['team_2_score']){
						// $team1 = $chk_array['0'];
						// $team2 = $chk_array['1'];
//
						// $chk_array['0'] = $team1;
						// $chk_array['1'] = $team2;
//
						// $sqlsssdd = $sql_d."{$team1['team_code']}"."{$team2['team_code']}";
//
//
					// }
					// if($chk['team_1_score'] < $chk['team_2_score']){
						// $team1 = $chk_array['0'];
						// $team2 = $chk_array['1'];
//
						// $chk_array['0'] = $team2;
						// $chk_array['1'] = $team1;
//
						// //$sqlsssdd = $sql_d."{$team1['team_code']}"."{$team2['team_code']}";
//
					// }
				// }
		// }

		for($i = 0 ; $i < count($chk_array) ;$i++){
			$wr_ids = $chk_array[$i]['wr_id'];
			$sqlsss = " update $team_field
			            set
			                 group_rank = '$rank'
			            where wr_id = '$wr_ids' and match_code = '$match_code'";
			sql_query($sqlsss);
			$rank++;

		}

		// $group_sqls = "select * from team_data where match_code = '$match_code' and (team_code = '$group_data[team_1]' OR team_code = '$group_data[team_2]' OR team_code = '$group_data[team_3]' OR team_code = '$group_data[team_4]' OR team_code = '$group_data[team_5]') ORDER BY team_league_point desc ,winning_rate desc ,gains_losses_point desc, team_total_match_point DESC";
		// $rank_result = sql_query($group_sqls);
//
		// while($team = sql_fetch_array($rank_result)){
//
		// }
	}
}


//모든 그룹에서 종료된었는지 검사한다.
//모든 그룹에서 종료되었을경우 팀성적에서 랭킹 1위와 득실차를 이용하여서 팀 성적을 나열하고 성적이 나쁜 순서대로 부전승에 집어 넣는다.
//end경기인지 확인한다.
$sql = "select count(wr_id) as cnt from game_score_data where end_game = 'N' and tournament = 'L' and match_code = '$match_code' and division = '$division' and series = '$series' and series_sub = '$series_sub'";
$result = sql_query($sql);
$cnt = sql_fetch_array($result);
//그룹에 모든 경기가 종료되었다면
if($cnt['cnt'] == 0){

	$group_cnt_sql = "select count(wr_id) as cnt from group_data where match_code = '$match_code' and  division = '$division' and series = '$series' and series_sub = '$series_sub'";
	$result = sql_query($group_cnt_sql);
	$group_data = sql_fetch_array($result);
	$group_cnt = $group_data['cnt']*2;
	$gravity = 1;
	if($division == "단체전")
		$gravity = 3;
	//해당 그룹이 2개만 있을 경우 각 그룹에 1위와 2위를 4강전에 올린다.
	if($group_cnt == 2){

		// $sql = "select count(wr_id) as cnt from game_score_data where tournament = 'T' and tournament_count = '2' and end_game = 'N' and match_code = '$match_code' and division = '$division' and series = '$series' and series_sub = '$series_sub'";
		// $result = sql_query($sql);
		// $r = sql_fetch_array($result);
		$tournamet_count = get_tourament_N_count(2,$match_code,$division,$series,$series_sub);
		if($tournamet_count == 2){

			$tournament_create = 'T4';

			$group_sql = "select * from team_data where match_code = '$match_code' and  division = '$division' and series = '$series' and series_sub = '$series_sub'  ORDER BY group_rank,team_league_point desc ,winning_rate desc	,gains_losses_point desc, team_total_match_point DESC Limit 4";
			$rows[1]["sql"] = $group_sql;
			$result = sql_query($group_sql);
			//검색된 순위대로 그룹 랭크를 매긴다.
			$team_array = array();
			$team_array1 = array();
			$cnt = 0;
			$cnt1 = 0;
			while($team = sql_fetch_array($result)){
				$gourp_sql = "select num from group_data where (team_1  = '$team[team_code]' or team_2  = '$team[team_code]' or team_3  = '$team[team_code]' or team_4  = '$team[team_code]'  or team_5  = '$team[team_code]') and match_code = '$match_code' and division = '$division' and series = '$series' and series_sub = '$series_sub' and tournament = 'L' order by num asc";
				$rows[1][] = $gourp_sql;
				$group_result = sql_query($gourp_sql);
				$group = sql_fetch_array($group_result);
				if($group['num'] == 0){
					$team_array[$cnt] = $team;
					$cnt++;
				}
				if($group['num'] == 1){
					$team_array1[$cnt1] = $team;
					$cnt1++;
				}
			}


			$sql = "select * from game_score_data where tournament = 'T' and tournament_count = '2' and end_game = 'N' and match_code = '$match_code' and division = '$division' and series = '$series' and series_sub = '$series_sub'";
			$result = sql_query($sql);
			$count = 0;
			while($r = sql_fetch_array($result)){
				if($count == 0){
					$team_1 = $team_array['0']['team_code'];
					$team_2 = $team_array1['1']['team_code'];
					$sql = " update game_score_data
			            set
			                 team_1_code = '$team_1',
			                 team_2_code = '$team_2'
			            where wr_id = '$r[wr_id]' and match_code = '$match_code'";
					sql_query($sql);
				}
				if($count == 1){
					$team_1 = $team_array['1']['team_code'];
					$team_2 = $team_array1['0']['team_code'];
					$sql = " update game_score_data
			            set
			                 team_1_code = '$team_1',
			                 team_2_code = '$team_2'
			            where wr_id = '$r[wr_id]' and match_code = '$match_code'";
					sql_query($sql);
				}
				$count++;
			}
		}else{
			set_championship($match_code,$division,$series,$series_sub);
		}
	}
	else if($group_cnt == 4){
		// $sql = "select count(wr_id) as cnt from game_score_data where tournament = 'T' and tournament_count = '2' and  end_game = 'N' and match_code = '$match_code' and division = '$division' and series = '$series' and series_sub = '$series_sub'";
		// $result = sql_query($sql);
		// $r = sql_fetch_array($result);
		$tournamet_count = get_tourament_N_count(2,$match_code,$division,$series,$series_sub);
		if($tournamet_count == 2*$gravity){
			$tournament_create = 'T4';
			$group_sql = "select * from $team_field where match_code = '$match_code' and division = '$division' and series = '$series' and series_sub = '$series_sub' and (group_rank = '1' or group_rank = '2')  ORDER BY group_rank,team_league_point desc ,winning_rate desc	,gains_losses_point desc, team_total_match_point DESC";
			$result = sql_query($group_sql);
			//검색된 순위대로 그룹 랭크를 매긴다.
			$cnt = 0;
			$team_array = array();
			while($team = sql_fetch_array($result)){
				$team_array[$cnt] = $team;
				$cnt++;
			}
			$sql = "select * from game_score_data where tournament = 'T' and tournament_count = '2' and team_1_score = '' and team_2_score = '' and match_code = '$match_code' and division = '$division' and series = '$series' and series_sub = '$series_sub' group by tournament_array_num";
			$result = sql_query($sql);
			$count = 0;
			$counts = 3;

			while($r = sql_fetch_array($result)){
				$team_1_code = $team_array[$count]['team_code'];
				$team_2_code = $team_array[$counts]['team_code'];

				$tournament_array_num = $r['tournament_array_num'];
				if($division == "단체전"){
					$sql = " update game_score_data
			            set
			                 team_1_code = '$team_1_code',
			                 team_2_code = '$team_2_code'
			            where tournament_array_num = '$tournament_array_num' and match_code = '$match_code'";
					sql_query($sql);
				}else{
					$sql = " update game_score_data
			            set
			                 team_1_code = '$team_1_code',
			                 team_2_code = '$team_2_code'
			            where wr_id = '$r[wr_id]' and match_code = '$match_code'";
					sql_query($sql);
				}
				$count = $count + 1;
				$counts = $counts - 1;
			}
		}else if($tournamet_count == 0){
			set_championship($match_code,$division,$series,$series_sub);
		}
	}else if($group_cnt > 4 && $group_cnt < 8){
		//2016.03.06 그룹 5개에서 7개까지 만듬
		// $sql = "select count(wr_id) as cnt from game_score_data where tournament = 'T' and tournament_count = '4' and end_game = 'N' and match_code = '$match_code' and division = '$division' and series = '$series' and series_sub = '$series_sub'";
		// $result = sql_query($sql);
		// $r = sql_fetch_array($result);
		//8강전이 종료되었는지 를 검사한다.
		$tournamet_count = get_tourament_N_count(4,$match_code,$division,$series,$series_sub);
		echo $tournamet_count."<br>";
		echo ($group_cnt-4)*$gravity;
		if($tournamet_count == ($group_cnt-4)*$gravity){
			$tournament_create = 'T8';
			// $group_sql = "select * from $team_field where match_code = '$match_code' and division = '$division' and series = '$series' and series_sub = '$series_sub' and (group_rank = '1' or group_rank = '2') ORDER BY group_rank,team_league_point desc ,winning_rate desc	,gains_losses_point desc, team_total_match_point DESC";
			// $result = sql_query($group_sql);
			// //검색된 순위대로 그룹 랭크를 매긴다.
			// $cnt = 0;
			// $team_array = array();
			// while($team = sql_fetch_array($result)){
				// $team_array[$cnt] = $team;
				// $cnt++;
			// }
			$team_array = get_team_rank($team_field,$match_code,$division,$series,$series_sub);
			print_r($team_array);
			$sql = "select * from game_score_data where tournament = 'T' and tournament_count = '2' and end_game = 'N' and match_code = '$match_code' and division = '$division' and series = '$series' and series_sub = '$series_sub' group by tournament_array_num";
			$result = sql_query($sql);
			$cnt = 0;
			$group_array = array();
			while($r = sql_fetch_array($result)){
				$group_array[$cnt] = $r;
				$cnt++;
			}
			for($c = 0 ; $c < (8-$group_cnt); $c++){
				if($c == 0){
					$team_1_code = $team_array[$c]['team_code'];
					$wr_ids = $group_array['0']['wr_id'];
					$tournament_array_num = $group_array['0']['tournament_array_num'];
					if($division == "단체전"){
						$sql = " update game_score_data
			            set
			                 team_1_code = '$team_1_code'
			            where tournament_array_num = '$tournament_array_num' and match_code = '$match_code'";
						sql_query($sql);
					}else{
						$sql = " update game_score_data
			            set
			                 team_1_code = '$team_1_code'
			            where wr_id = '$wr_ids' and match_code = '$match_code'";
						sql_query($sql);
					}
				}
				if($c == 1){
					$team_1_code = $team_array[$c]['team_code'];
					$wr_ids = $group_array['1']['wr_id'];
					$tournament_array_num = $group_array['1']['tournament_array_num'];
					if($division == "단체전"){
						$sql = " update game_score_data
			            set
			                 team_1_code = '$team_1_code'
			            where tournament_array_num = '$tournament_array_num' and match_code = '$match_code'";
						sql_query($sql);
					}else{
						$sql = " update game_score_data
			            set
			                 team_1_code = '$team_1_code'
			            where wr_id = '$wr_ids' and match_code = '$match_code'";
						sql_query($sql);
					}

				}
				if($c == 2){
					$team_1_code = $team_array[$c]['team_code'];
					$wr_ids = $group_array['1']['wr_id'];
					$tournament_array_num = $group_array['1']['tournament_array_num'];
					if($division == "단체전"){
						$sql = " update game_score_data
			            set
			                 team_2_code = '$team_1_code'
			            where tournament_array_num = '$tournament_array_num' and match_code = '$match_code'";
						sql_query($sql);
					}else{
						$sql = " update game_score_data
			            set
			                 team_2_code = '$team_1_code'
			            where wr_id = '$wr_ids' and match_code = '$match_code'";
						sql_query($sql);
					}
				}
			}
			$count = 0;
			$sqlsss = "select * from game_score_data where tournament = 'T' and tournament_count = '4' and end_game = 'N' and match_code = '$match_code' and division = '$division' and series = '$series' and series_sub = '$series_sub' group by tournament_array_num";
			$result = sql_query($sqlsss);
			$cnt = 0;
			$group_array = array();
			while($r = sql_fetch_array($result)){
				$group_array[$cnt] = $r;
				$cnt++;
			}

			for(; $c < count($team_array); $c++){
				if($count == 0){
					$team_1_code = $team_array[3]['team_code'];
					$team_2_code = $team_array[4]['team_code'];
					$wr_ids = $group_array['0']['wr_id'];
					$tournament_array_num = $group_array['0']['tournament_array_num'];
					if($division == "단체전"){
						$sql = " update game_score_data
			            set
			                 team_1_code = '$team_1_code',
			                 team_2_code = '$team_2_code'
			            where tournament_array_num = '$tournament_array_num' and match_code = '$match_code'";
						sql_query($sql);
					}else{
						$sql = " update game_score_data
			            set
			                 team_1_code = '$team_1_code',
			                 team_2_code = '$team_2_code'
			            where wr_id = '$wr_ids' and match_code = '$match_code'";
						sql_query($sql);
					}
				}
				if($count == 1){
					$team_1_code = $team_array[2]['team_code'];
					$team_2_code = $team_array[5]['team_code'];
					$wr_ids = $group_array['1']['wr_id'];
					$tournament_array_num = $group_array['1']['tournament_array_num'];
					if($division == "단체전"){
						$sql = " update game_score_data
			            set
			                 team_1_code = '$team_1_code',
			                 team_2_code = '$team_2_code'
			            where tournament_array_num = '$tournament_array_num' and match_code = '$match_code'";
						sql_query($sql);
					}else{
						$sql = " update game_score_data
			            set
			                 team_1_code = '$team_1_code',
			                 team_2_code = '$team_2_code'
			            where wr_id = '$wr_ids' and match_code = '$match_code'";
						sql_query($sql);
					}
				}
				if($count == 2){
					$team_1_code = $team_array[6]['team_code'];
					$team_2_code = $team_array[1]['team_code'];
					$tournament_array_num = $group_array['2']['tournament_array_num'];
					if($division == "단체전"){
						$sql = " update game_score_data
			            set
			                 team_1_code = '$team_1_code',
			                 team_2_code = '$team_2_code'
			            where tournament_array_num = '$tournament_array_num' and match_code = '$match_code'";
						sql_query($sql);
					}else{
						$sql = " update game_score_data
			            set
			                 team_1_code = '$team_1_code',
			                 team_2_code = '$team_2_code'
			            where wr_id = '$wr_ids' and match_code = '$match_code'";
						sql_query($sql);
					}
				}
				$count++;
			}
		}else if($tournamet_count == 0){
			//8강전이 종료되었다면 8강전에 승자를 4강전에 배치한다.
			// $sql = "select count(wr_id) as cnt from game_score_data where tournament = 'T' and tournament_count = '2' and end_game = 'N' and match_code = '$match_code' and division = '$division' and series = '$series' and series_sub = '$series_sub'";
			// $result = sql_query($sql);
			// $r = sql_fetch_array($result);

			$tournamet_count = get_tourament_N_count(2,$match_code,$division,$series,$series_sub);
			if($tournamet_count == 2*$gravity){
				$tournament_create = 'T4';
				//검색된 순위대로 그룹 랭크를 매긴다.
				$sql = "select * from game_score_data where tournament = 'T' and tournament_count = '4' and match_code = '$match_code' and division = '$division' and series = '$series' and series_sub = '$series_sub' group by tournament_array_num";
				$result = sql_query($sql);
				$sqls = "select * from game_score_data where tournament = 'T' and tournament_count = '2' and end_game = 'N' and match_code = '$match_code' and division = '$division' and series = '$series' and series_sub = '$series_sub' group by tournament_array_num";
				$results = sql_query($sqls);

				$cnt = 0;
				$team_array = array();
				$team_1_code = 0;
				while($team = sql_fetch_array($result)){
					// if($team['team_1_score'] > $team['team_2_score']){
						// $team_1_code = $team['team_1_code'];
					// }else{
						// $team_1_code = $team['team_2_code'];
					// }
					$team_1_code = team_win_code($team);
					if($cnt == 0){
						$r = sql_fetch_array($results);
						if($division == "단체전"){
							$sql = " update game_score_data
				            set
				                 team_2_code = '$team_1_code'
				            where tournament_array_num = '$r[tournament_array_num]' and match_code = '$match_code'";
							sql_query($sql);
							echo $sql;
						}else{
							$sql = " update game_score_data
				            set
				                 team_2_code = '$team_1_code'
				            where wr_id = '$r[wr_id]' and match_code = '$match_code'";

							sql_query($sql);
						}
					}
					if($cnt == 1){
						$r = sql_fetch_array($results);
						if($division == "단체전"){
							$sql = " update game_score_data
				            set
				                 team_2_code = '$team_1_code'
				            where tournament_array_num = '$r[tournament_array_num]' and match_code = '$match_code'";
							sql_query($sql);
						}else{
							$sql = " update game_score_data
				            set
				                 team_2_code = '$team_1_code'
				            where wr_id = '$r[wr_id]' and match_code = '$match_code'";

							sql_query($sql);
						}

					}
					if($cnt == 2){
						$r = sql_fetch_array($results);
						if($division == "단체전"){
							$sql = " update game_score_data
				            set
				                 team_1_code = '$team_1_code'
				            where tournament_array_num = '$r[tournament_array_num]' and match_code = '$match_code'";
							sql_query($sql);
						}else{
							$sql = " update game_score_data
				            set
				                 team_1_code = '$team_1_code'
				            where wr_id = '$r[wr_id]' and match_code = '$match_code'";

							sql_query($sql);
						}
					}
					$cnt++;

				}
			}else if($tournamet_count == 0){
				set_championship($match_code,$division,$series,$series_sub);
			}
		}
	}else if($group_cnt == 8){
		//2016.03.07 여기부터 제작 8갈 부터 정렬해서 삽입
		// $sql = "select count(wr_id) as cnt from game_score_data where tournament = 'T' and tournament_count = '4' and end_game = 'N' and match_code = '$match_code' and division = '$division' and series = '$series' and series_sub = '$series_sub'";
		// $result = sql_query($sql);
		// $r = sql_fetch_array($result);
		$tournamet_count = get_tourament_N_count(4,$match_code,$division,$series,$series_sub);
		if($tournamet_count == 4*$gravity){
			$tournament_create = 'T8';
			// $group_sql = "select * from team_data where match_code = '$match_code' and division = '$division' and series = '$series' and series_sub = '$series_sub' and group_rank = '1' ORDER BY group_rank,team_league_point desc ,winning_rate desc	,gains_losses_point desc, team_total_match_point DESC";
			// $result = sql_query($group_sql);
			// //검색된 순위대로 그룹 랭크를 매긴다.
			// $cnt = 0;
			// $team_array = array();
			// while($team = sql_fetch_array($result)){
				// $team_array[$cnt] = $team;
				// $cnt++;
			// }
			$team_array = get_team_rank($team_field,$match_code,$division,$series,$series_sub);
			$sql = "select * from game_score_data where tournament = 'T' and tournament_count = '4' and team_1_score = '' and team_2_score = '' and match_code = '$match_code' and division = '$division' and series = '$series' and series_sub = '$series_sub' group by tournament_array_num";
			$result = sql_query($sql);
			$count = 0;
			while($r = sql_fetch_array($result)){
				if($count == 0){
					$team_1_code = $team_array[0]['team_code'];
					$team_2_code = $team_array[7]['team_code'];
				}
				if($count == 1){
					$team_1_code = $team_array[3]['team_code'];
					$team_2_code = $team_array[4]['team_code'];
				}
				if($count == 2){
					$team_1_code = $team_array[2]['team_code'];
					$team_2_code = $team_array[5]['team_code'];
				}
				if($count == 3){
					$team_1_code = $team_array[1]['team_code'];
					$team_2_code = $team_array[6]['team_code'];
				}

				if($division == "단체전"){
					$tournament_array_num = $r['tournament_array_num'];
					$sql = " update game_score_data
		            set
		                 team_1_code = '$team_1_code',
		                 team_2_code = '$team_2_code'
		            where tournament_array_num = '$tournament_array_num' and match_code = '$match_code'";
					sql_query($sql);
				}else{
					$sql = " update game_score_data
		            set
		                 team_1_code = '$team_1_code',
		                 team_2_code = '$team_2_code'
		            where wr_id = '$r[wr_id]' and match_code = '$match_code'";
					sql_query($sql);
				}
				$count++;
			}
		}else if($tournamet_count == 0){
			// $sql = "select count(wr_id) as cnt from game_score_data where tournament = 'T' and tournament_count = '2' and end_game = 'N' and match_code = '$match_code' and division = '$division' and series = '$series' and series_sub = '$series_sub'";
			// $result = sql_query($sql);
			// $r = sql_fetch_array($result);
			$tournamet_count = get_tourament_count(2,$match_code,$division,$series,$series_sub);
			if($tournamet_count == 2*$gravity){
				$tournament_create = 'T4';
				//검색된 순위대로 그룹 랭크를 매긴다.
				$sql = "select * from game_score_data where tournament = 'T' and tournament_count = '4' and match_code = '$match_code' and division = '$division' and series = '$series' and series_sub = '$series_sub' group by tournament_array_num";
				$result = sql_query($sql);

				$sqls = "select * from game_score_data where tournament = 'T' and tournament_count = '2' and team_1_score = '' and team_2_score = '' and match_code = '$match_code' and division = '$division' and series = '$series' and series_sub = '$series_sub' group by tournament_array_num";
				$results = sql_query($sqls);
//
				$cnt = 0;
				$team_array = array();
				$team_1_code = 0;
				while($team = sql_fetch_array($result)){
					// if($team['team_1_score'] > $team['team_2_score']){
						// $team_1_code = $team['team_1_code'];
					// }else{
						// $team_1_code = $team['team_2_code'];
					// }
					$team_1_code = team_win_code($team);
					if($cnt == 0){
						$r = sql_fetch_array($results);
						if($division == "단체전"){
							$tournament_array_num = $r['tournament_array_num'];
							$sql = " update game_score_data
				            set
				                 team_1_code = '$team_1_code'
				            where tournament_array_num = '$tournament_array_num' and match_code = '$match_code'";
							sql_query($sql);
						}else{
							$sql = " update game_score_data
				            set
				                 team_1_code = '$team_1_code'
				            where wr_id = '$r[wr_id]' and match_code = '$match_code'";

							sql_query($sql);
						}
					}
					if($cnt == 1){
						if($division == "단체전"){
							$tournament_array_num = $r['tournament_array_num'];
							$sql = " update game_score_data
				            set
				                 team_2_code = '$team_1_code'
				            where tournament_array_num = '$tournament_array_num' and match_code = '$match_code'";
							sql_query($sql);
						}else{
							$sql = " update game_score_data
				            set
				                 team_2_code = '$team_1_code'
				            where wr_id = '$r[wr_id]' and match_code = '$match_code'";

							sql_query($sql);
						}
					}
					if($cnt == 2){
						$r = sql_fetch_array($results);
						if($division == "단체전"){
							$tournament_array_num = $r['tournament_array_num'];
							$sql = " update game_score_data
				            set
				                 team_1_code = '$team_1_code'
				            where tournament_array_num = '$tournament_array_num' and match_code = '$match_code'";
							sql_query($sql);
						}else{
							$sql = " update game_score_data
				            set
				                 team_1_code = '$team_1_code'
				            where wr_id = '$r[wr_id]' and match_code = '$match_code'";

							sql_query($sql);
						}
					}
					if($cnt == 3){
						if($division == "단체전"){
							$tournament_array_num = $r['tournament_array_num'];
							$sql = " update game_score_data
				            set
				                 team_2_code = '$team_1_code'
				            where tournament_array_num = '$tournament_array_num' and match_code = '$match_code'";
							sql_query($sql);
						}else{
							$sql = " update game_score_data
				            set
				                 team_2_code = '$team_1_code'
				            where wr_id = '$r[wr_id]' and match_code = '$match_code'";

							sql_query($sql);
						}
					}
					$cnt++;

				}
			}else if($tournamet_count == 0){
				set_championship($match_code,$division,$series,$series_sub);
			}
		}
	}else if($group_cnt> 8 && $group_cnt < 16){
		//2016.03.07 그룹 9개에서 15개까지 만듬
		//$sql = "select count(wr_id) as cnt from game_score_data where tournament = 'T' and tournament_count = '8' and end_game = 'N' and match_code = '$match_code' and division = '$division' and series = '$series' and series_sub = '$series_sub'";
		//$result = sql_query($sql);
		//$r = sql_fetch_array($result);
		//8강전이 종료되었는지 를 검사한다.
		$tournamet_count = get_tourament_N_count(8,$match_code,$division,$series,$series_sub);
		if($tournamet_count == ($group_cnt-8)*$gravity){
			$tournament_create = 'T16';
			// $group_sql = "select * from team_data where match_code = '$match_code' and division = '$division' and series = '$series' and series_sub = '$series_sub' and group_rank = '1' ORDER BY group_rank, team_league_point desc ,winning_rate desc	,gains_losses_point desc , team_total_match_point DESC";
			// $result = sql_query($group_sql);
			// //검색된 순위대로 그룹 랭크를 매긴다.
			// $cnt = 0;
			// $team_array = array();
			// while($team = sql_fetch_array($result)){
				// $team_array[$cnt] = $team;
				// $cnt++;
			// }
			$team_array = get_team_rank($team_field,$match_code,$division,$series,$series_sub);
			$sql = "select * from game_score_data where tournament = 'T' and tournament_count = '4' and end_game = 'N' and match_code = '$match_code' and division = '$division' and series = '$series' and series_sub = '$series_sub' group by tournament_array_num order by game_increase desc ";
			$result = sql_query($sql);
			$cnt = 0;
			$group_array = array();
			while($r = sql_fetch_array($result)){
				$group_array[$cnt] = $r;
				$cnt++;
			}
			for($c = 0 ; $c < (16-$group_cnt); $c++){

				if($c == 0){
					$team_1_code = $team_array[$c]['team_code'];
					if($division == "단체전"){
						$tournament_array_num = $group_array['0']['tournament_array_num'];
						$sql = " update game_score_data
			            set
			                 team_2_code = '$team_1_code'
			            where tournament_array_num = '$tournament_array_num' and match_code = '$match_code'";
						sql_query($sql);
					}else{
						$wr_ids = $group_array['0']['wr_id'];
						$sql = " update game_score_data
			            set
			                 team_1_code = '$team_1_code'
			            where wr_id = '$wr_ids' and match_code = '$match_code'";
						sql_query($sql);
					}
				}
				if($c == 1){
					$team_1_code = $team_array[$c]['team_code'];
					if($division == "단체전"){
						$tournament_array_num = $group_array['3']['tournament_array_num'];
						$sql = " update game_score_data
			            set
			                 team_1_code = '$team_1_code'
			            where tournament_array_num = '$tournament_array_num' and match_code = '$match_code'";
						sql_query($sql);
					}else{
						$wr_ids = $group_array['3']['wr_id'];
						$sql = " update game_score_data
			            set
			                 team_1_code = '$team_1_code'
			            where wr_id = '$wr_ids' and match_code = '$match_code'";
						sql_query($sql);
					}
				}
				if($c == 2){
					$team_1_code = $team_array[$c]['team_code'];
					if($division == "단체전"){
						$tournament_array_num = $group_array['2']['tournament_array_num'];
						$sql = " update game_score_data
			            set
			                 team_1_code = '$team_1_code'
			            where tournament_array_num = '$tournament_array_num' and match_code = '$match_code'";
						sql_query($sql);
					}else{
						$wr_ids = $group_array['2']['wr_id'];
						$sql = " update game_score_data
			            set
			                 team_1_code = '$team_1_code'
			            where wr_id = '$wr_ids' and match_code = '$match_code'";
						sql_query($sql);
					}
				}
				if($c == 3){
					$team_1_code = $team_array[$c]['team_code'];
					if($division == "단체전"){
						$tournament_array_num = $group_array['1']['tournament_array_num'];
						$sql = " update game_score_data
			            set
			                 team_1_code = '$team_1_code'
			            where tournament_array_num = '$tournament_array_num' and match_code = '$match_code'";
						sql_query($sql);
					}else{
						$wr_ids = $group_array['1']['wr_id'];
						$sql = " update game_score_data
			            set
			                 team_1_code = '$team_1_code'
			            where wr_id = '$wr_ids' and match_code = '$match_code'";
						sql_query($sql);
					}
				}
				if($c == 4){
					$team_1_code = $team_array[$c]['team_code'];
					if($division == "단체전"){
						$tournament_array_num = $group_array['1']['tournament_array_num'];
						$sql = " update game_score_data
			            set
			                 team_2_code = '$team_1_code'
			            where tournament_array_num = '$tournament_array_num' and match_code = '$match_code'";
						sql_query($sql);
					}else{
						$wr_ids = $group_array['1']['wr_id'];
						$sql = " update game_score_data
			            set
			                 team_2_code = '$team_1_code'
			            where wr_id = '$wr_ids' and match_code = '$match_code'";
						sql_query($sql);
					}
				}
				if($c == 5){
					$team_1_code = $team_array[$c]['team_code'];
					if($division == "단체전"){
						$tournament_array_num = $group_array['2']['tournament_array_num'];
						$sql = " update game_score_data
			            set
			                 team_2_code = '$team_1_code'
			            where tournament_array_num = '$tournament_array_num' and match_code = '$match_code'";
						sql_query($sql);
					}else{
						$wr_ids = $group_array['2']['wr_id'];
						$sql = " update game_score_data
			            set
			                 team_2_code = '$team_1_code'
			            where wr_id = '$wr_ids' and match_code = '$match_code'";
						sql_query($sql);
					}
				}
				if($c == 6){
					$team_1_code = $team_array[$c]['team_code'];
					if($division == "단체전"){
						$tournament_array_num = $group_array['3']['tournament_array_num'];
						$sql = " update game_score_data
			            set
			                 team_2_code = '$team_1_code'
			            where tournament_array_num = '$tournament_array_num' and match_code = '$match_code'";
						sql_query($sql);
					}else{
						$wr_ids = $group_array['3']['wr_id'];
						$sql = " update game_score_data
			            set
			                 team_2_code = '$team_1_code'
			            where wr_id = '$wr_ids' and match_code = '$match_code'";
						sql_query($sql);
					}
				}
			}

			$count = 0;
			$sql = "select * from game_score_data where tournament = 'T' and tournament_count = '8' and end_game = 'N' and match_code = '$match_code' and division = '$division' and series = '$series' and series_sub = '$series_sub' group by tournament_array_num order by game_increase desc ";
			$result = sql_query($sql);
			$cnt = 0;
			$group_array = array();
			while($r = sql_fetch_array($result)){
				$group_array[$cnt] = $r;
				$cnt++;
			}

			$sqlsss = "select * from game_score_data where tournament = 'T' and tournament_count = '4' and end_game = 'N' and match_code = '$match_code' and division = '$division' and series = '$series' and series_sub = '$series_sub' group by tournament_array_num order by game_increase desc ";
			$result1 = sql_query($sqlsss);
			$cnt = 0;

			$group_count = 0;

			$group_array1 = array("1","4","3","2");
			$group_array2 = array("8","5","6","7");

			while($r = sql_fetch_array($result1)){
				if($r['team_1_code'] == ""){
					$i = $group_array1[$cnt]-1;
					$j = 15 - $i;

					$team_1_code = $team_array[$i]['team_code'];
					$team_2_code = $team_array[$j]['team_code'];
					if($division == "단체전"){
						$tournament_array_num = $group_array[$group_count]['tournament_array_num'];
						$sql1 = " update game_score_data
			            set
			                 team_1_code = '$team_1_code',
			                 team_2_code = '$team_2_code'
			            where tournament_array_num = '$tournament_array_num' and match_code = '$match_code'";
						sql_query($sql1);
					}else{
						$wr_ids = $group_array[$group_count]['wr_id'];
						$sql1 = " update game_score_data
			            set
			                 team_1_code = '$team_1_code',
			                 team_2_code = '$team_2_code'
			            where wr_id = '$wr_ids' and match_code = '$match_code'";
						sql_query($sql1);
					}
					$group_count++;
				}
				if($r['team_2_code'] == ""){
					$i = $group_array2[$cnt]-1;
					$j = 15 - $i;

					$team_1_code = $team_array[$i]['team_code'];
					$team_2_code = $team_array[$j]['team_code'];
					if($division == "단체전"){
						$tournament_array_num = $group_array[$group_count]['tournament_array_num'];
						$sql1 = " update game_score_data
			            set
			                 team_1_code = '$team_1_code',
			                 team_2_code = '$team_2_code'
			            where tournament_array_num = '$tournament_array_num' and match_code = '$match_code'";
						sql_query($sql1);
					}else{
						$wr_ids = $group_array[$group_count]['wr_id'];
						$sql2 = " update game_score_data
			            set
			                 team_1_code = '$team_1_code',
			                 team_2_code = '$team_2_code'
			            where wr_id = '$wr_ids' and match_code = '$match_code'";
						sql_query($sql2);
					}
					$group_count++;
				}
				$cnt++;
			}

			$count++;

		}else if($tournamet_count == 0){
			//8강 끝났는지 검사한다.
			$sql = "select count(wr_id) as cnt from game_score_data where tournament = 'T' and tournament_count = '4' and end_game = 'N' and match_code = '$match_code' and division = '$division' and series = '$series' and series_sub = '$series_sub'";
			$result = sql_query($sql);
			$r = sql_fetch_array($result);
			$tournamet_count = get_tourament_N_count(4,$match_code,$division,$series,$series_sub);
			$count = ($group_cnt-8);
			if($tournamet_count == 4*$gravity){
				$tournament_create = 'T8';
				//검색된 순위대로 그룹 랭크를 매긴다.
				$sql = "select * from game_score_data where tournament = 'T' and tournament_count = '8' and match_code = '$match_code' and division = '$division' and series = '$series' and series_sub = '$series_sub' group by tournament_array_num order by game_increase desc ";
				$result = sql_query($sql);

				$sqls = "select * from game_score_data where tournament = 'T' and tournament_count = '4' and (team_1_code = '' or team_2_code = '') and match_code = '$match_code' and division = '$division' and series = '$series' and series_sub = '$series_sub' group by tournament_array_num order by game_increase desc ";
				$resultss = sql_query($sqls);
 				$rs = sql_fetch_array($resultss);

				$cnt = 0;
				$team_array = array();
				$team_1_code = 0;
				$evennumber = 0;
				while($team = sql_fetch_array($result)){
					// if($team['team_1_score'] > $team['team_2_score']){
						// $team_1_code = $team['team_1_code'];
					// }else{
						// $team_1_code = $team['team_2_code'];
					// }
					$team_1_code = team_win_code($team);

					if($rs['team_1_code'] == "" && $evennumber == 0){

						if($division == "단체전"){
							$tournament_array_num = $rs['tournament_array_num'];
							$sql1 = " update game_score_data
				            set
				                 team_1_code = '$team_1_code'
				            where tournament_array_num = '$tournament_array_num' and match_code = '$match_code'";

							sql_query($sql1);
						}else{
							$wr_ids = $rs['wr_id'];
							$sqles = " update game_score_data
				            set
				                 team_1_code = '$team_1_code'
				            where wr_id = '$wr_ids' and match_code = '$match_code'";

							sql_query($sqles);
						}
						$evennumber = 1;
					}
					else {
						if($division == "단체전"){
							$tournament_array_num = $rs['tournament_array_num'];
							$sql1 = " update game_score_data
				            set
				                 team_2_code = '$team_1_code'
				            where tournament_array_num = '$tournament_array_num' and match_code = '$match_code'";

							sql_query($sql1);
						}else{
							$wr_ids = $rs['wr_id'];
							$sqled = " update game_score_data
				            set
				                 team_2_code = '$team_1_code'
				            where wr_id = '$wr_ids' and match_code = '$match_code'";

							sql_query($sqled);
						}
						$rs = sql_fetch_array($resultss);
						$evennumber = 0;
					}
					//$chksss = "123123";
					$cnt++;
				}


			}else{
				// $sql = "select count(wr_id) as cnt from game_score_data where tournament = 'T' and tournament_count = '4' and end_game = 'Y' and match_code = '$match_code' and division = '$division' and series = '$series' and series_sub = '$series_sub'";
				// $result = sql_query($sql);
				// $r = sql_fetch_array($result);
				$tournamet_count = get_tourament_Y_count(4,$match_code,$division,$series,$series_sub);
				if($tournamet_count == 4*$gravity){
					$sql = "select count(wr_id) as cnt from game_score_data where tournament = 'T' and tournament_count = '2' and end_game = 'N' and match_code = '$match_code' and division = '$division' and series = '$series' and series_sub = '$series_sub'";
					$result = sql_query($sql);
					$r = sql_fetch_array($result);
					$tournamet_count = get_tourament_N_count(2,$match_code,$division,$series,$series_sub);
					if($tournamet_count == 2*$gravity){
						$tournament_create = 'T4';
						//검색된 순위대로 그룹 랭크를 매긴다.
						$sql = "select * from game_score_data where tournament = 'T' and tournament_count = '4' and match_code = '$match_code' and division = '$division' and series = '$series' and series_sub = '$series_sub' group by tournament_array_num order by game_increase desc ";
						$result = sql_query($sql);

						$sqls = "select * from game_score_data where tournament = 'T' and tournament_count = '2' and (team_1_code = '' or team_2_code = '') and match_code = '$match_code' and division = '$division' and series = '$series' and series_sub = '$series_sub' group by tournament_array_num order by game_increase desc ";
						$results = sql_query($sqls);

						$cnt = 0;
						$team_array = array();
						$team_1_code = 0;
						while($team = sql_fetch_array($result)){
							// if($team['team_1_score'] > $team['team_2_score']){
								// $team_1_code = $team['team_1_code'];
							// }else{
								// $team_1_code = $team['team_2_code'];
							// }
							$team_1_code = team_win_code($team);
							if($cnt == 0){
								$r = sql_fetch_array($results);
								if($division == "단체전"){
									$tournament_array_num = $r['tournament_array_num'];
									$sql1 = " update game_score_data
						            set
						                 team_1_code = '$team_1_code'
						            where tournament_array_num = '$tournament_array_num' and match_code = '$match_code'";

									sql_query($sql1);
								}else{
									$wr_ids = $r['wr_id'];
									$sql = " update game_score_data
						            set
						                 team_1_code = '$team_1_code'
						            where wr_id = '$r[wr_id]' and match_code = '$match_code'";

									sql_query($sql);
								}
							}
							if($cnt == 1){
								if($division == "단체전"){
									$tournament_array_num = $r['tournament_array_num'];
									$sql1 = " update game_score_data
						            set
						                 team_2_code = '$team_1_code'
						            where tournament_array_num = '$tournament_array_num' and match_code = '$match_code'";

									sql_query($sql1);
								}else{
									$wr_ids = $r['wr_id'];
									$sql = " update game_score_data
						            set
						                 team_2_code = '$team_1_code'
						            where wr_id = '$wr_ids' and match_code = '$match_code'";

									sql_query($sql);
								}
							}
							if($cnt == 2){
								$r = sql_fetch_array($results);
								if($division == "단체전"){
									$tournament_array_num = $r['tournament_array_num'];
									$sql1 = " update game_score_data
						            set
						                 team_1_code = '$team_1_code'
						            where tournament_array_num = '$tournament_array_num' and match_code = '$match_code'";

									sql_query($sql1);
								}else{
									$wr_ids = $r['wr_id'];
									$sql = " update game_score_data
						            set
						                 team_1_code = '$team_1_code'
						            where wr_id = '$wr_ids' and match_code = '$match_code'";

									sql_query($sql);
								}
							}
							if($cnt == 3){
								if($division == "단체전"){
									$tournament_array_num = $r['tournament_array_num'];
									$sql1 = " update game_score_data
						            set
						                 team_2_code = '$team_1_code'
						            where tournament_array_num = '$tournament_array_num' and match_code = '$match_code'";

									sql_query($sql1);
								}else{
									$wr_ids = $r['wr_id'];
									$sql = " update game_score_data
						            set
						                 team_2_code = '$team_1_code'
						            where wr_id = '$wr_ids' and match_code = '$match_code'";

									sql_query($sql);
								}
							}
							$cnt++;

						}
					}else{
						set_championship($match_code,$division,$series,$series_sub);
						//결승전을 넣는다.
					}
				}
			}
		}
	}else if($group_cnt == 16){
		//2016.03.07 여기부터 제작 8갈 부터 정렬해서 삽입
		$sql = "select count(wr_id) as cnt from game_score_data where tournament = 'T' and tournament_count = '8' and team_1_code = '' and team_2_code = '' and match_code = '$match_code' and division = '$division' and series = '$series' and series_sub = '$series_sub'";
		$result = sql_query($sql);
		$r = sql_fetch_array($result);

		$tournamet_count = get_tourament_N_count(4,$match_code,$division,$series,$series_sub);

		if($r['cnt'] == 4*$gravity){
			$tournament_create = 'T16';
			// $group_sql = "select * from team_data where match_code = '$match_code' and division = '$division' and series = '$series' and series_sub = '$series_sub' and group_rank = '1' ORDER BY group_rank, team_league_point desc ,winning_rate desc	,gains_losses_point desc , team_total_match_point DESC";
			// $result = sql_query($group_sql);
			// //검색된 순위대로 그룹 랭크를 매긴다.
			// $cnt = 0;
			// $team_array = array();
			// while($team = sql_fetch_array($result)){
				// $team_array[$cnt] = $team;
				// $cnt++;
			// }
			$team_array = get_team_rank($team_field,$match_code,$division,$series,$series_sub);
			$sql = "select * from game_score_data where tournament = 'T' and tournament_count = '8' and team_1_score = '' and team_2_score = '' and match_code = '$match_code' and division = '$division' and series = '$series' and series_sub = '$series_sub' group by tournament_array_num order by game_increase desc ";
			$result = sql_query($sql);
			$count = 0;
			while($r = sql_fetch_array($result)){
				if($count == 0){
					$team_1_code = $team_array[0]['team_code'];
					$team_2_code = $team_array[15]['team_code'];
				}
				if($count == 1){
					$team_1_code = $team_array[7]['team_code'];
					$team_2_code = $team_array[8]['team_code'];
				}
				if($count == 2){
					$team_1_code = $team_array[4]['team_code'];
					$team_2_code = $team_array[11]['team_code'];
				}
				if($count == 3){
					$team_1_code = $team_array[3]['team_code'];
					$team_2_code = $team_array[12]['team_code'];
				}
				if($count == 4){
					$team_1_code = $team_array[2]['team_code'];
					$team_2_code = $team_array[13]['team_code'];
				}
				if($count == 5){
					$team_1_code = $team_array[6]['team_code'];
					$team_2_code = $team_array[9]['team_code'];
				}
				if($count == 6){
					$team_1_code = $team_array[5]['team_code'];
					$team_2_code = $team_array[10]['team_code'];
				}
				if($count == 7){
					$team_1_code = $team_array[1]['team_code'];
					$team_2_code = $team_array[14]['team_code'];
				}
				if($division == "단체전"){
					$tournament_array_num = $r['tournament_array_num'];
					$sql = " update game_score_data
			            set
			                 team_1_code = '$team_1_code',
			                 team_2_code = '$team_2_code'
			            where tournament_array_num = '$tournament_array_num' and match_code = '$match_code'";
					sql_query($sql);
				}else{
					$wr_ids = $r['wr_id'];
					$sql = " update game_score_data
			            set
			                 team_1_code = '$team_1_code',
			                 team_2_code = '$team_2_code'
			            where wr_id = '$wr_ids' and match_code = '$match_code'";
					sql_query($sql);
				}
				$count++;
			}
		}else if($r['cnt'] == 0){
			//8강전이 종료되었다면 16강전에 승자를 8강전에 배치한다.
			$sql = "select count(wr_id) as cnt from game_score_data where tournament = 'T' and tournament_count = '4' and team_1_score = '' and team_2_score = '' and match_code = '$match_code' and division = '$division' and series = '$series' and series_sub = '$series_sub'";
			$result = sql_query($sql);
			$r = sql_fetch_array($result);
			if($r['cnt'] == 4*$gravity){
				$tournament_create = 'T8';
				//검색된 순위대로 그룹 랭크를 매긴다.
				$sql = "select * from game_score_data where tournament = 'T' and tournament_count = '8' and match_code = '$match_code' and division = '$division' and series = '$series' and series_sub = '$series_sub' group by tournament_array_num order by game_increase desc ";
				$result = sql_query($sql);

				$sqls = "select * from game_score_data where tournament = 'T' and tournament_count = '4' and team_1_score = '' and team_2_score = '' and match_code = '$match_code' and division = '$division' and series = '$series' and series_sub = '$series_sub' group by tournament_array_num order by game_increase desc ";
				$results = sql_query($sqls);
 				$r = sql_fetch_array($results);

				$cnt = 0;
				$team_array = array();
				$team_1_code = 0;
				$evennumber = 0;
				while($team = sql_fetch_array($result)){
					// if($team['team_1_score'] > $team['team_2_score']){
						// $team_1_code = $team['team_1_code'];
					// }else{
						// $team_1_code = $team['team_2_code'];
					// }
					$team_1_code = team_win_code($team);


					if($r['team_1_code'] == "" && $evennumber == 0){
						if($division == "단체전"){
							$tournament_array_num = $r['tournament_array_num'];
							$sql = " update game_score_data
					            set
					                 team_1_code = '$team_1_code'
					            where tournament_array_num = '$tournament_array_num' and match_code = '$match_code'";
							sql_query($sql);
						}else{
							$wr_ids = $r['wr_id'];
							$sql = " update game_score_data
				            set
				                 team_1_code = '$team_1_code'
				            where wr_id = '$wr_ids' and match_code = '$match_code'";

							sql_query($sql);
						}
						$evennumber = 1;
					}
					else{
						if($division == "단체전"){
							$tournament_array_num = $r['tournament_array_num'];
							$sql = " update game_score_data
					            set
					                 team_2_code = '$team_1_code'
					            where tournament_array_num = '$tournament_array_num' and match_code = '$match_code'";
							sql_query($sql);
						}else{
							$wr_ids = $r['wr_id'];
							$sql = " update game_score_data
				            set
				                 team_2_code = '$team_1_code'
				            where wr_id = '$wr_ids' and match_code = '$match_code'";

							sql_query($sql);
						}
						$r = sql_fetch_array($results);
						$evennumber = 0;
					}
					$cnt++;

				}
			}else{
				//update`game_score_data` set team_1_score = '20' and team_2_score = '10' WHERE `match_code` LIKE 'WBVKRJJSBEQ' AND `division` LIKE '여복' AND `series` LIKE '40' AND `series_sub` LIKE 'D1' AND`tournament` = 'L'

				$sql = "select count(wr_id) as cnt from game_score_data where tournament = 'T' and tournament_count = '2' and team_1_score = '' and team_2_score = '' and match_code = '$match_code' and division = '$division' and series = '$series' and series_sub = '$series_sub'";
				$result = sql_query($sql);
				$r = sql_fetch_array($result);
				if($r['cnt'] == 2*$gravity){
					$tournament_create = 'T4';
					//검색된 순위대로 그룹 랭크를 매긴다.
					$sql = "select * from game_score_data where tournament = 'T' and tournament_count = '4' and match_code = '$match_code' and division = '$division' and series = '$series' and series_sub = '$series_sub' group by tournament_array_num order by game_increase desc ";
					$result = sql_query($sql);

					$sqls = "select * from game_score_data where tournament = 'T' and tournament_count = '2' and team_1_score = '' and team_2_score = '' and match_code = '$match_code' and division = '$division' and series = '$series' and series_sub = '$series_sub' group by tournament_array_num order by game_increase desc ";
					$results = sql_query($sqls);
	//
					$cnt = 0;
					$team_array = array();
					$team_1_code = 0;
					while($team = sql_fetch_array($result)){
						// if($team['team_1_score'] > $team['team_2_score']){
							// $team_1_code = $team['team_1_code'];
						// }else{
							// $team_1_code = $team['team_2_code'];
						// }
						$team_1_code = team_win_code($team);

						if($cnt == 0){
							$r = sql_fetch_array($results);

							if($division == "단체전"){
								$tournament_array_num = $r['tournament_array_num'];
								$sql = " update game_score_data
						            set
						                 team_1_code = '$team_1_code'
						            where tournament_array_num = '$tournament_array_num' and match_code = '$match_code'";
								sql_query($sql);
							}else{
								$wr_ids = $r['wr_id'];
								$sql = " update game_score_data
					            set
					                 team_1_code = '$team_1_code'
					            where wr_id = '$r[wr_id]' and match_code = '$match_code'";

								sql_query($sql);
							}
						}
						if($cnt == 1){
							if($division == "단체전"){
								$tournament_array_num = $r['tournament_array_num'];
								$sql = " update game_score_data
						            set
						                 team_2_code = '$team_1_code'
						            where tournament_array_num = '$tournament_array_num' and match_code = '$match_code'";
								sql_query($sql);
							}else{
								$wr_ids = $r['wr_id'];
								$sql = " update game_score_data
					            set
					                 team_2_code = '$team_1_code'
					            where wr_id = '$r[wr_id]' and match_code = '$match_code'";

								sql_query($sql);
							}
						}
						if($cnt == 3){
							$r = sql_fetch_array($results);
							if($division == "단체전"){
								$tournament_array_num = $r['tournament_array_num'];
								$sql = " update game_score_data
						            set
						                 team_1_code = '$team_1_code'
						            where tournament_array_num = '$tournament_array_num' and match_code = '$match_code'";
								sql_query($sql);
							}else{
								$wr_ids = $r['wr_id'];
								$sql = " update game_score_data
					            set
					                 team_1_code = '$team_1_code'
					            where wr_id = '$r[wr_id]' and match_code = '$match_code'";

								sql_query($sql);
							}
						}
						if($cnt == 4){
							// $sql = " update game_score_data
				            // set
				                 // team_2_code = '$team_1_code'
				            // where wr_id = '$r[wr_id]' and match_code = '$match_code'";
//
							// sql_query($sql);
							if($division == "단체전"){
								$tournament_array_num = $r['tournament_array_num'];
								$sql = " update game_score_data
						            set
						                 team_2_code = '$team_1_code'
						            where tournament_array_num = '$tournament_array_num' and match_code = '$match_code'";
								sql_query($sql);
							}else{
								$wr_ids = $r['wr_id'];
								$sql = " update game_score_data
					            set
					                 team_2_code = '$team_1_code'
					            where wr_id = '$r[wr_id]' and match_code = '$match_code'";

								sql_query($sql);
							}
						}
						$cnt++;

					}
				}else{
					//결승전을 넣는다.
					set_championship($match_code,$division,$series,$series_sub);

				}
			}
		}
	}else if($group_cnt > 16 && $group_cnt <= 32){
		//2016.03.07 그룹 9개에서 15개까지 만듬
        $sql = "select count(wr_id) as cnt from game_score_data where tournament = 'T' and tournament_count = '16' and team_1_code = ''  and team_2_code = '' and end_game = 'N' and match_code = '$match_code' and division = '$division' and series = '$series' and series_sub = '$series_sub'";
        echo $sql."<br>"."<br>";
        $result = sql_query($sql);
        $r = sql_fetch_array($result);
        //8강전이 종료되었는지 를 검사한다.
        if($r['cnt'] == ($group_cnt-16)*$gravity){
			$tournament_create = 'T32';
            // $group_sql = "select * from team_data where match_code = '$match_code' and division = '$division' and series = '$series' and series_sub = '$series_sub' and group_rank = '1' ORDER BY group_rank, team_league_point desc ,winning_rate desc ,gains_losses_point desc , team_total_match_point DESC";
			// $result = sql_query($group_sql);
			// //검색된 순위대로 그룹 랭크를 매긴다.
			// $cnt = 0;
			// $team_array = array();
			// while($team = sql_fetch_array($result)){
				// $team_array[$cnt] = $team;
				// $cnt++;
			// }
			$team_array = get_team_rank($team_field,$match_code,$division,$series,$series_sub);
			$sql = "select * from game_score_data where tournament = 'T' and tournament_count = '8' and team_1_score = '' and team_2_score = '' and match_code = '$match_code' and division = '$division' and series = '$series' and series_sub = '$series_sub' group by tournament_array_num order by game_increase desc ";
			echo $sql."<br>"."<br>";
			$result = sql_query($sql);
			$cnt = 0;
			$group_array = array();
			while($r = sql_fetch_array($result)){
				$group_array[$cnt] = $r;
				$cnt++;
			}

			for($c = 0 ; $c < (32-$group_cnt); $c++){

				if($c == 0){
					$team_1_code = $team_array[$c]['team_code'];
					if($division == "단체전"){
						$tournament_array_num = $group_array['0']['tournament_array_num'];
						$sql = " update game_score_data
				            set
			                	team_1_code = '$team_1_code'
				            where tournament_array_num = '$tournament_array_num' and match_code = '$match_code'";
						sql_query($sql);
					}else{
						$wr_ids = $group_array['0']['wr_id'];
						$sql = " update game_score_data
			            set
			                 team_1_code = '$team_1_code'
			            where wr_id = '$wr_ids' and match_code = '$match_code'";
						sql_query($sql);
					}
				}
				if($c == 1){
					$team_1_code = $team_array[$c]['team_code'];
					if($division == "단체전"){
						$tournament_array_num = $group_array['7']['tournament_array_num'];
						$sql = " update game_score_data
				            set
			                	team_1_code = '$team_1_code'
				            where tournament_array_num = '$tournament_array_num' and match_code = '$match_code'";
						sql_query($sql);
					}else{
						$wr_ids = $group_array['7']['wr_id'];
						$sql = " update game_score_data
			            set
			                 team_1_code = '$team_1_code'
			            where wr_id = '$wr_ids' and match_code = '$match_code'";
						sql_query($sql);
					}
				}
				if($c == 2){
					$team_1_code = $team_array[$c]['team_code'];
					if($division == "단체전"){
						$tournament_array_num = $group_array['4']['tournament_array_num'];
						$sql = " update game_score_data
				            set
			                	team_1_code = '$team_1_code'
				            where tournament_array_num = '$tournament_array_num' and match_code = '$match_code'";
						sql_query($sql);
					}else{
						$wr_ids = $group_array['4']['wr_id'];
						$sql = " update game_score_data
			            set
			                 team_1_code = '$team_1_code'
			            where wr_id = '$wr_ids' and match_code = '$match_code'";
						sql_query($sql);
					}
				}
				if($c == 3){
					$team_1_code = $team_array[$c]['team_code'];
					if($division == "단체전"){
						$tournament_array_num = $group_array['3']['tournament_array_num'];
						$sql = " update game_score_data
				            set
			                	team_1_code = '$team_1_code'
				            where tournament_array_num = '$tournament_array_num' and match_code = '$match_code'";
						sql_query($sql);
					}else{
						$wr_ids = $group_array['3']['wr_id'];
						$sql = " update game_score_data
			            set
			                 team_1_code = '$team_1_code'
			            where wr_id = '$wr_ids' and match_code = '$match_code'";
						sql_query($sql);
					}
				}
				if($c == 4){
					$team_1_code = $team_array[$c]['team_code'];
					if($division == "단체전"){
						$tournament_array_num = $group_array['2']['tournament_array_num'];
						$sql = " update game_score_data
				            set
			                	team_1_code = '$team_1_code'
				            where tournament_array_num = '$tournament_array_num' and match_code = '$match_code'";
						sql_query($sql);
					}else{
						$wr_ids = $group_array['2']['wr_id'];
						$sql = " update game_score_data
			            set
			                 team_1_code = '$team_1_code'
			            where wr_id = '$wr_ids' and match_code = '$match_code'";
						sql_query($sql);
					}
				}
				if($c == 5){
					$team_1_code = $team_array[$c]['team_code'];
					if($division == "단체전"){
						$tournament_array_num = $group_array['6']['tournament_array_num'];
						$sql = " update game_score_data
				            set
			                	team_1_code = '$team_1_code'
				            where tournament_array_num = '$tournament_array_num' and match_code = '$match_code'";
						sql_query($sql);
					}else{
						$wr_ids = $group_array['6']['wr_id'];
						$sql = " update game_score_data
			            set
			                 team_1_code = '$team_1_code'
			            where wr_id = '$wr_ids' and match_code = '$match_code'";
						sql_query($sql);
					}
				}
				if($c == 6){
					$team_1_code = $team_array[$c]['team_code'];
					if($division == "단체전"){
						$tournament_array_num = $group_array['5']['tournament_array_num'];
						$sql = " update game_score_data
				            set
			                	team_1_code = '$team_1_code'
				            where tournament_array_num = '$tournament_array_num' and match_code = '$match_code'";
						sql_query($sql);
					}else{
						$wr_ids = $group_array['5']['wr_id'];
						$sql = " update game_score_data
			            set
			                 team_1_code = '$team_1_code'
			            where wr_id = '$wr_ids' and match_code = '$match_code'";
						sql_query($sql);
					}
				}
				if($c == 7){
					$team_1_code = $team_array[$c]['team_code'];
					if($division == "단체전"){
						$tournament_array_num = $group_array['1']['tournament_array_num'];
						$sql = " update game_score_data
				            set
			                	team_1_code = '$team_1_code'
				            where tournament_array_num = '$tournament_array_num' and match_code = '$match_code'";
						sql_query($sql);
					}else{
						$wr_ids = $group_array['1']['wr_id'];
						$sql = " update game_score_data
			            set
			                 team_1_code = '$team_1_code'
			            where wr_id = '$wr_ids' and match_code = '$match_code'";
						sql_query($sql);
					}
				}
				if($c == 8){
					$team_1_code = $team_array[$c]['team_code'];
					if($division == "단체전"){
						$tournament_array_num = $group_array['1']['tournament_array_num'];
						$sql = " update game_score_data
				            set
			                	team_2_code = '$team_1_code'
				            where tournament_array_num = '$tournament_array_num' and match_code = '$match_code'";
						sql_query($sql);
					}else{
						$wr_ids = $group_array['1']['wr_id'];
						$sql = " update game_score_data
			            set
			                 team_2_code = '$team_1_code'
			            where wr_id = '$wr_ids' and match_code = '$match_code'";
						sql_query($sql);
					}
				}
				if($c == 9){
					$team_1_code = $team_array[$c]['team_code'];
					if($division == "단체전"){
						$tournament_array_num = $group_array['5']['tournament_array_num'];
						$sql = " update game_score_data
				            set
			                	team_2_code = '$team_1_code'
				            where tournament_array_num = '$tournament_array_num' and match_code = '$match_code'";
						sql_query($sql);
					}else{
						$wr_ids = $group_array['5']['wr_id'];
						$sql = " update game_score_data
			            set
			                 team_2_code = '$team_1_code'
			            where wr_id = '$wr_ids' and match_code = '$match_code'";
						sql_query($sql);
					}
				}
				if($c == 10){
					$team_1_code = $team_array[$c]['team_code'];
					if($division == "단체전"){
						$tournament_array_num = $group_array['6']['tournament_array_num'];
						$sql = " update game_score_data
				            set
			                	team_2_code = '$team_1_code'
				            where tournament_array_num = '$tournament_array_num' and match_code = '$match_code'";
						sql_query($sql);
					}else{
						$wr_ids = $group_array['6']['wr_id'];
						$sql = " update game_score_data
			            set
			                 team_2_code = '$team_1_code'
			            where wr_id = '$wr_ids' and match_code = '$match_code'";
						sql_query($sql);
					}
				}
				if($c == 11){
					$team_1_code = $team_array[$c]['team_code'];
					if($division == "단체전"){
						$tournament_array_num = $group_array['2']['tournament_array_num'];
						$sql = " update game_score_data
				            set
			                	team_2_code = '$team_1_code'
				            where tournament_array_num = '$tournament_array_num' and match_code = '$match_code'";
						sql_query($sql);
					}else{
						$wr_ids = $group_array['2']['wr_id'];
						$sql = " update game_score_data
			            set
			                 team_2_code = '$team_1_code'
			            where wr_id = '$wr_ids' and match_code = '$match_code'";
						sql_query($sql);
					}
				}
				if($c == 12){
					$team_1_code = $team_array[$c]['team_code'];
					if($division == "단체전"){
						$tournament_array_num = $group_array['3']['tournament_array_num'];
						$sql = " update game_score_data
				            set
			                	team_2_code = '$team_1_code'
				            where tournament_array_num = '$tournament_array_num' and match_code = '$match_code'";
						sql_query($sql);
					}else{
						$wr_ids = $group_array['3']['wr_id'];
						$sql = " update game_score_data
			            set
			                 team_2_code = '$team_1_code'
			            where wr_id = '$wr_ids' and match_code = '$match_code'";
						sql_query($sql);
					}
				}
				if($c == 13){
					$team_1_code = $team_array[$c]['team_code'];
					if($division == "단체전"){
						$tournament_array_num = $group_array['4']['tournament_array_num'];
						$sql = " update game_score_data
				            set
			                	team_2_code = '$team_1_code'
				            where tournament_array_num = '$tournament_array_num' and match_code = '$match_code'";
						sql_query($sql);
					}else{
						$wr_ids = $group_array['4']['wr_id'];
						$sql = " update game_score_data
			            set
			                 team_2_code = '$team_1_code'
			            where wr_id = '$wr_ids' and match_code = '$match_code'";
						sql_query($sql);
					}
				}
				if($c == 14){
					$team_1_code = $team_array[$c]['team_code'];
					if($division == "단체전"){
						$tournament_array_num = $group_array['7']['tournament_array_num'];
						$sql = " update game_score_data
				            set
			                	team_2_code = '$team_1_code'
				            where tournament_array_num = '$tournament_array_num' and match_code = '$match_code'";
						sql_query($sql);
					}else{
						$wr_ids = $group_array['7']['wr_id'];
						$sql = " update game_score_data
			            set
			                 team_2_code = '$team_1_code'
			            where wr_id = '$wr_ids' and match_code = '$match_code'";
						sql_query($sql);
					}
				}
			}
			$count = 0;
			$sql = "select * from game_score_data where tournament = 'T' and tournament_count = '16' and team_1_score = '' and team_2_score = '' and match_code = '$match_code' and division = '$division' and series = '$series' and series_sub = '$series_sub' group by tournament_array_num order by game_increase desc ";
			$result = sql_query($sql);
			$cnt = 0;
			$group_array = array();
			while($r = sql_fetch_array($result)){
				$group_array[$cnt] = $r;
				$cnt++;
			}

			$sqlsss = "select * from game_score_data where tournament = 'T' and tournament_count = '8' and team_1_score = '' and team_2_score = '' and match_code = '$match_code' and division = '$division' and series = '$series' and series_sub = '$series_sub' group by tournament_array_num order by game_increase desc ";
			$result1 = sql_query($sqlsss);
			$cnt = 0;

			$group_count = 0;

			$group_array1 = array("1","8","5","4","3","7","6","2");
			$group_array2 = array("16","9","12","13","14","10","11","15");
			echo "<br>";
			echo count($team_array);
			echo "<br>";

			while($r = sql_fetch_array($result1)){
				if($r['team_1_code'] == ""){
					$i = $group_array1[$cnt]-1;
					$j = 31 - $i;

					$team_1_code = $team_array[$i]['team_code'];
					$team_2_code = $team_array[$j]['team_code'];
					if($division == "단체전"){
						$tournament_array_num = $group_array[$group_count]['tournament_array_num'];

						$sql = " update game_score_data
				            set
			                 team_1_code = '$team_1_code',
			                 team_2_code = '$team_2_code'
				            where tournament_array_num = '$tournament_array_num' and match_code = '$match_code'";
						sql_query($sql);

					}else{
						$wr_ids = $group_array[$group_count]['wr_id'];

						$sql1 = " update game_score_data
			            set
			                 team_1_code = '$team_1_code',
			                 team_2_code = '$team_2_code'
			            where wr_id = '$wr_ids' and match_code = '$match_code'";

						sql_query($sql1);
					}

					$group_count++;
				}
				if($r['team_2_code'] == ""){
					$i = $group_array2[$cnt]-1;
					$j = 31 - $i;

					$team_1_code = $team_array[$i]['team_code'];
					$team_2_code = $team_array[$j]['team_code'];
					if($division == "단체전"){
						$tournament_array_num = $group_array[$group_count]['tournament_array_num'];

						$sql = " update game_score_data
				            set
			                 team_1_code = '$team_1_code',
			                 team_2_code = '$team_2_code'
				            where tournament_array_num = '$tournament_array_num' and match_code = '$match_code'";
						sql_query($sql);
					}else{
						$wr_ids = $group_array[$group_count]['wr_id'];

						$sql1 = " update game_score_data
			            set
			                 team_1_code = '$team_1_code',
			                 team_2_code = '$team_2_code'
			            where wr_id = '$wr_ids' and match_code = '$match_code'";

						sql_query($sql1);
					}
					$group_count++;
					echo "<br>".$sql."<br>";
				}
				echo $group_count;
				$cnt++;
			}
		}else if($r['cnt'] == 0){
			//8강전이 종료되었다면 16강전에 승자를 8강전에 배치한다.
			$count_32 = "select count(wr_id) as cnt from game_score_data where tournament = 'T' and tournament_count = '16' and match_code = '$match_code' and division = '$division' and series = '$series' and series_sub = '$series_sub'";
            $result_count_32 = sql_query($count_32);
            $count_32 = sql_fetch_array($result_count_32);

            $sql_32 = "select count(wr_id) as cnt from game_score_data where tournament = 'T' and tournament_count = '16' and end_game = 'Y' and match_code = '$match_code' and division = '$division' and series = '$series' and series_sub = '$series_sub'";
            $result_32 = sql_query($sql_32);
            $r_32 = sql_fetch_array($result_32);

            $sql = "select count(wr_id) as cnt from game_score_data where tournament = 'T' and tournament_count = '8' and end_game = 'N' and match_code = '$match_code' and division = '$division' and series = '$series' and series_sub = '$series_sub'";

            $result = sql_query($sql);
            $r = sql_fetch_array($result);
            $count = ($group_cnt-16);
			//if($r['cnt'] == 8*$gravity && $r_32 == 0){
            if($r['cnt'] > 0 && $r_32['cnt'] == $count_32['cnt']){
				$tournament_create = 'T16';
				//검색된 순위대로 그룹 랭크를 매긴다.
				$sql = "select * from game_score_data where tournament = 'T' and tournament_count = '16' and match_code = '$match_code' and division = '$division' and series = '$series' and series_sub = '$series_sub' order by game_increase desc";
				$result = sql_query($sql);

				$sqls = "select * from game_score_data where tournament = 'T' and tournament_count = '8' and (team_1_code = '' or team_2_code = '') and match_code = '$match_code' and division = '$division' and series = '$series' and series_sub = '$series_sub' order by game_increase desc";
				$results = sql_query($sqls);
//

				$cnt = 0;
				$team_array = array();
				$team_1_code = 0;
				$evennumber = 0;
				$r = sql_fetch_array($results);
				while($team = sql_fetch_array($result)){

					if($team['team_1_score'] > $team['team_2_score']){
						$team_1_code = $team['team_1_code'];
					}else{
						$team_1_code = $team['team_2_code'];
					}

					if($r['team_1_code'] == "" && $evennumber == 0){
						$sql1 = " update game_score_data
			            set
			                 team_1_code = '$team_1_code'
			            where wr_id = '$r[wr_id]' and match_code = '$match_code'";

						sql_query($sql1);
						$evennumber = 1;
					}
					else{
						$sql2 = " update game_score_data
			            set
			                 team_2_code = '$team_1_code'
			            where wr_id = '$r[wr_id]' and match_code = '$match_code'";

						sql_query($sql2);
						$r = sql_fetch_array($results);
						$evennumber = 0;
					}
					$cnt++;

				}
			}else if($r['cnt'] == 0){
                $sql = "select count(wr_id) as cnt from game_score_data where tournament = 'T' and tournament_count = '4' and end_game = 'N' and match_code = '$match_code' and division = '$division' and series = '$series' and series_sub = '$series_sub'";
                $result = sql_query($sql);
                $r = sql_fetch_array($result);
                $count = ($group_cnt-8);
                if($r['cnt'] == 4*$gravity){
					$tournament_create = 'T8';
					//검색된 순위대로 그룹 랭크를 매긴다.
					$sql = "select * from game_score_data where tournament = 'T' and tournament_count = '8' and match_code = '$match_code' and division = '$division' and series = '$series' and series_sub = '$series_sub' group by tournament_array_num  order by game_increase desc";
					$result = sql_query($sql);

					$sqls = "select * from game_score_data where tournament = 'T' and tournament_count = '4' and team_1_score = '' and team_2_score = '' and match_code = '$match_code' and division = '$division' and series = '$series' and series_sub = '$series_sub' group by tournament_array_num  order by game_increase desc";
					$results = sql_query($sqls);
					$cnt = 0;
					$team_array = array();
					$team_1_code = 0;
					while($team = sql_fetch_array($result)){

						$team_1_code = team_win_code($team);

						if($cnt == 0){
							$r = sql_fetch_array($results);
							if($division == "단체전"){
								$tournament_array_num = $r['tournament_array_num'];
								$sql = " update game_score_data
						            set
						                 team_1_code = '$team_1_code'
						            where tournament_array_num = '$tournament_array_num' and match_code = '$match_code'";
								sql_query($sql);
							}else{
								$wr_ids = $r['wr_id'];
								$sql = " update game_score_data
					            set
					                 team_1_code = '$team_1_code'
					            where wr_id = '$wr_ids' and match_code = '$match_code'";

								sql_query($sql);
							}
						}
						if($cnt == 1){
							if($division == "단체전"){
								$tournament_array_num = $r['tournament_array_num'];
								$sql = " update game_score_data
						            set
						                 team_2_code = '$team_1_code'
						            where tournament_array_num = '$tournament_array_num' and match_code = '$match_code'";
								sql_query($sql);
							}else{
								$wr_ids = $r['wr_id'];
								$sql = " update game_score_data
					            set
					                 team_2_code = '$team_1_code'
					            where wr_id = '$wr_ids' and match_code = '$match_code'";

								sql_query($sql);
							}
						}
						if($cnt == 2){
							$r = sql_fetch_array($results);
							if($division == "단체전"){
								$tournament_array_num = $r['tournament_array_num'];
								$sql = " update game_score_data
						            set
						                 team_1_code = '$team_1_code'
						            where tournament_array_num = '$tournament_array_num' and match_code = '$match_code'";
								sql_query($sql);
							}else{
								$wr_ids = $r['wr_id'];
								$sql = " update game_score_data
					            set
					                 team_1_code = '$team_1_code'
					            where wr_id = '$r[wr_id]' and match_code = '$match_code'";

								sql_query($sql);
							}
						}
						if($cnt == 3){
							if($division == "단체전"){
								$tournament_array_num = $r['tournament_array_num'];
								$sql = " update game_score_data
						            set
						                 team_2_code = '$team_1_code'
						            where tournament_array_num = '$tournament_array_num' and match_code = '$match_code'";
								sql_query($sql);
							}else{
								$wr_ids = $r['wr_id'];
								$sql = " update game_score_data
					            set
					                 team_2_code = '$team_1_code'
					            where wr_id = '$r[wr_id]' and match_code = '$match_code'";

								sql_query($sql);
							}
						}
						if($cnt == 4){
							$r = sql_fetch_array($results);
							if($division == "단체전"){
								$tournament_array_num = $r['tournament_array_num'];
								$sql = " update game_score_data
						            set
						                 team_1_code = '$team_1_code'
						            where tournament_array_num = '$tournament_array_num' and match_code = '$match_code'";
								sql_query($sql);
							}else{
								$wr_ids = $r['wr_id'];
								$sql = " update game_score_data
					            set
					                 team_1_code = '$team_1_code'
					            where wr_id = '$r[wr_id]' and match_code = '$match_code'";

								sql_query($sql);
							}
						}
						if($cnt == 5){
							if($division == "단체전"){
								$tournament_array_num = $r['tournament_array_num'];
								$sql = " update game_score_data
						            set
						                 team_2_code = '$team_1_code'
						            where tournament_array_num = '$tournament_array_num' and match_code = '$match_code'";
								sql_query($sql);
							}else{
								$wr_ids = $r['wr_id'];
								$sql = " update game_score_data
					            set
					                 team_2_code = '$team_1_code'
					            where wr_id = '$r[wr_id]' and match_code = '$match_code'";

								sql_query($sql);
							}
						}
						if($cnt == 6){
							$r = sql_fetch_array($results);
							if($division == "단체전"){
								$tournament_array_num = $r['tournament_array_num'];
								$sql = " update game_score_data
						            set
						                 team_1_code = '$team_1_code'
						            where tournament_array_num = '$tournament_array_num' and match_code = '$match_code'";
								sql_query($sql);
							}else{
								$wr_ids = $r['wr_id'];
								$sql = " update game_score_data
					            set
					                 team_1_code = '$team_1_code'
					            where wr_id = '$r[wr_id]' and match_code = '$match_code'";

								sql_query($sql);
							}
						}
						if($cnt == 7){
							if($division == "단체전"){
								$tournament_array_num = $r['tournament_array_num'];
								$sql = " update game_score_data
						            set
						                 team_2_code = '$team_1_code'
						            where tournament_array_num = '$tournament_array_num' and match_code = '$match_code'";
								sql_query($sql);
							}else{
								$wr_ids = $r['wr_id'];
								$sql = " update game_score_data
					            set
					                 team_2_code = '$team_1_code'
					            where wr_id = '$r[wr_id]' and match_code = '$match_code'";

								sql_query($sql);
							}
						}
						echo $sql."<br>";
						$cnt++;
					}
				}else if($r['cnt'] == 0){
  		            $sql = "select count(wr_id) as cnt from game_score_data where tournament = 'T' and tournament_count = '2' and end_game = 'N' and match_code = '$match_code' and division = '$division' and series = '$series' and series_sub = '$series_sub'";
                    $result = sql_query($sql);
                    $r = sql_fetch_array($result);
                    $count = ($group_cnt-4);
                    if($r['cnt'] == 2*$gravity){
						$tournament_create = 'T4';
						//검색된 순위대로 그룹 랭크를 매긴다.
						$sql = "select * from game_score_data where tournament = 'T' and tournament_count = '4' and match_code = '$match_code' and division = '$division' and series = '$series' and series_sub = '$series_sub' group by tournament_array_num";
						$result = sql_query($sql);

						$sqls = "select * from game_score_data where tournament = 'T' and tournament_count = '2' and team_1_score = '' and team_2_score = '' and match_code = '$match_code' and division = '$division' and series = '$series' and series_sub = '$series_sub' group by tournament_array_num";
						$results = sql_query($sqls);
		//
						$cnt = 0;
						$team_array = array();
						$team_1_code = 0;
						while($team = sql_fetch_array($result)){
							// if($team['team_1_score'] > $team['team_2_score']){
								// $team_1_code = $team['team_1_code'];
							// }else{
								// $team_1_code = $team['team_2_code'];
							// }
							$team_1_code = team_win_code($team);
							echo $team_1_code."<br>";
							if($cnt == 0){
								$r = sql_fetch_array($results);

								if($division == "단체전"){
									$tournament_array_num = $r['tournament_array_num'];
									$sql = " update game_score_data
							            set
							                 team_1_code = '$team_1_code'
							            where tournament_array_num = '$tournament_array_num' and match_code = '$match_code'";
									sql_query($sql);
								}else{
									$wr_ids = $r['wr_id'];
									$sql = " update game_score_data
						            set
						                 team_1_code = '$team_1_code'
						            where wr_id = '$r[wr_id]' and match_code = '$match_code'";

									sql_query($sql);
								}
							}
							if($cnt == 1){
								if($division == "단체전"){
									$tournament_array_num = $r['tournament_array_num'];
									$sql = " update game_score_data
							            set
							                 team_2_code = '$team_1_code'
							            where tournament_array_num = '$tournament_array_num' and match_code = '$match_code'";
									sql_query($sql);
								}else{
									$wr_ids = $r['wr_id'];
									$sql = " update game_score_data
						            set
						                 team_2_code = '$team_1_code'
						            where wr_id = '$r[wr_id]' and match_code = '$match_code'";

									sql_query($sql);
								}
							}
							if($cnt == 2){
								$r = sql_fetch_array($results);
								if($division == "단체전"){
									$tournament_array_num = $r['tournament_array_num'];
									$sql = " update game_score_data
							            set
							                 team_1_code = '$team_1_code'
							            where tournament_array_num = '$tournament_array_num' and match_code = '$match_code'";
									sql_query($sql);
								}else{
									$wr_ids = $r['wr_id'];
									$sql = " update game_score_data
						            set
						                 team_1_code = '$team_1_code'
						            where wr_id = '$r[wr_id]' and match_code = '$match_code'";

									sql_query($sql);
								}
							}
							if($cnt == 3){
								if($division == "단체전"){
									$tournament_array_num = $r['tournament_array_num'];
									$sql = " update game_score_data
							            set
							                 team_2_code = '$team_1_code'
							            where tournament_array_num = '$tournament_array_num' and match_code = '$match_code'";
									sql_query($sql);
								}else{
									$wr_ids = $r['wr_id'];
									$sql = " update game_score_data
						            set
						                 team_2_code = '$team_1_code'
						            where wr_id = '$r[wr_id]' and match_code = '$match_code'";

									sql_query($sql);
								}
							}
							echo $sql."<br>";
							$cnt++;
						}
					}else{
                        //결승전을 넣는다.
						$tournament_create = set_championship($match_code,$division,$series,$series_sub);
    				}
				}
			}
		}
	}
}
echo $require_url;

goto_url($require_url);



//$rows[0]['sql'] = "$sqlsssdd";
//echo json_encode($rows);
?>
<!-- <script>
	opener.parent.location.reload();
	window.close();
</script> -->
