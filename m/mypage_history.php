<?php
include_once('../common.php');
if (!defined('_GNUBOARD_')) exit; // 개별 페이지 접근 불가
include_once(G5_AOS_PATH.'/head.php');
$menu_cate2 ='mypage';
$menu_cate3 ='1';

$mb_id = get_session('ss_mb_id');
$mb = get_member($mb_id);
?>

<!-- sub nav -->
<?php include_once('./app_sub_nav.php'); ?>
<!-- end sub nav -->


<!-- Contents Area -->
<div class="pop_container">
		<!-- 준비중 -->
		<div class="empty_waiting">
			<div class="img_area">
				<img src="<?php echo G5_IMG_URL.'/common/intro_logo1.png';?>">
			</div>
			<div class="cmt ani02">
				전적이 없습니다.
			</div>
		</div>
		<!-- //준비중 -->
</div>
<!-- end Contents Area -->


<?php
include_once(G5_AOS_PATH.'/tail.php');
?>
