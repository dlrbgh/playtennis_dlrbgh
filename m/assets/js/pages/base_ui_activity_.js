/*
 *  Document   : base_ui_activity.js
 *  Author     : pixelcave
 *  Description: Custom JS code used in Activity Page
 */

var BaseUIActivity = function() {
    // Randomize progress bars values
    var barsRandomize = function(){
        jQuery('.js-bar-randomize').on('click', function(){
            jQuery(this)
                .parents('.block')
                .find('.progress-bar')
                .each(function() {
                    var $this   = jQuery(this);
                    var $random = Math.floor((Math.random() * 91) + 10)  + '%';

                    $this.css('width', $random);

                    if ( ! $this.parent().hasClass('progress-mini')) {
                        $this.html($random);
                    }
                });
            });
    };

    // SweetAlert, for more examples you can check out https://github.com/t4t5/sweetalert
    var sweetAlert = function(){
        // Init a simple alert on button click
        jQuery('.js-swal-alert').on('click', function(){
            swal('Hi, this is a simple alert!');
        });

        // Init an success alert on button click
        jQuery('.js-excel-success').on('click', function(){
            swal('Success', '참가자 입력파일이 성공적으로 업로드 되었습니다.', 'success');
        });
		
		// Init an success alert on button click
        jQuery('.js-excel-error').on('click', function(){
            swal({
                title: '파일 업로드 실패',
                text: '참가자 입력 파일의 업로드가 실패하였습니다.'+'<br/>'+'엑셀파일이 참가자 입력 양식에 맞는지 확인해 주세요.',
                type: 'error',
                confirmButtonColor: '#d26a5c',
                confirmButtonText: '확인',
                closeOnConfirm: false,
                html: true
            });
        });

        // Init an error alert on button click
        jQuery('.js-swal-error').on('click', function(){
            swal('Oops...', '시스템이 정상적으로 처리되지 않았습니다!', 'error');
        });

        // Init an example confirm alert on button click
        jQuery('.js-swal-confirm').on('click', function(){
            swal({
                title: 'Are you sure?',
                text: 'You will not be able to recover this imaginary file!',
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#d26a5c',
                confirmButtonText: 'Yes, delete it!',
                closeOnConfirm: false,
                html: false
            }, function () {
                swal('Deleted!', 'Your imaginary file has been deleted.', 'success');
            });
        });
        // 장소 및 일자 취소
        jQuery('.js-swal-confirm').on('click', function(){
            swal({
                title: 'Are you sure?',
                text: 'You will not be able to recover this imaginary file!',
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#d26a5c',
                confirmButtonText: 'Yes, delete it!',
                closeOnConfirm: false,
                html: false
            }, function () {
                swal('Deleted!', 'Your imaginary file has been deleted.', 'success');
            });
        });
    };

    return {
        init: function() {
            // Init randomize bar values
            barsRandomize();

            // Init SweetAlert
            sweetAlert();
        }
    };
}();

// Initialize when page loads
jQuery(function(){ BaseUIActivity.init(); });