/*
 *  Document   : base_ui_activity.js
 *  Author     : pixelcave
 *  Description: Custom JS code used in Activity Page
 */

var BaseUIActivity = function() {
    // Randomize progress bars values
    var barsRandomize = function(){
        jQuery('.js-bar-randomize').on('click', function(){
            jQuery(this)
                .parents('.block')
                .find('.progress-bar')
                .each(function() {
                    var $this   = jQuery(this);
                    var $random = Math.floor((Math.random() * 91) + 10)  + '%';

                    $this.css('width', $random);

                    if ( ! $this.parent().hasClass('progress-mini')) {
                        $this.html($random);
                    }
                });
            });
    };

    // SweetAlert, for more examples you can check out https://github.com/t4t5/sweetalert
    var sweetAlert = function(){
        // Init a simple alert on button click
        jQuery('.js-swal-alert').on('click', function(){
            swal('Hi, this is a simple alert!');
        });

        // Init an success alert on button click
        jQuery('.js-excel-success').on('click', function(){
            swal('Success', '참가자 입력파일이 성공적으로 업로드 되었습니다.', 'success');
        });
		
		// 엑셀파일 업로드 실패
        jQuery('.js-excel-error').on('click', function(){
            swal({
                title: '파일 업로드 실패',
                text: '참가자 입력 파일의 업로드가 실패하였습니다.'+'<br/>'+'엑셀파일이 참가자 입력 양식에 맞는지 확인해 주세요.',
                type: 'error',
                confirmButtonColor: '#d26a5c',
                confirmButtonText: '확인',
                closeOnConfirm: false,
                html: true
            });
        });

		// 조벌장소배치 설정 취소
        jQuery('.js-group-cancle').on('click', function(){
            swal({
                title: '장소 배치 취소',
                text: '조별 장소 배치를 취소하시겠습니까?.'+'<br/>'+'취소한 데이터는 복구 할 수 없습니다.',
                type: 'warning',
                showCancelButton: true,
                cancelButtonText: '취소',
                confirmButtonColor: '#d26a5c',
                confirmButtonText: '확인',
                closeOnConfirm: false,
                html: true
            });
        });

		// 대진시간표 생성 확인
        


		// 대진시간표 생성 확인
        jQuery('.js-team-delete').on('click', function(){
            swal({
                title: '팀 삭제',
                text: '해당 팀을 삭제 하시겠습니까?.'+'<br/>'+'삭제한 팀은 복구할 수 없습니다.',
                type: 'warning',
                showCancelButton: true,
                cancelButtonText: '취소',
                confirmButtonColor: '#d26a5c',
                confirmButtonText: '확인',
                closeOnConfirm: false,
                html: true
            });
        });

		// 팀그룹 미배정 팀 배정하기
        // jQuery('.js-teamgroup-confirm').on('click', function(){
            // swal({
                // title: '팀 배정',
                // text: '선택한 팀을 해당 조에 편성합니다.'+'<br/>'+'실행 하시겠습니까?',
                // type: 'warning',
                // showCancelButton: true,
                // cancelButtonText: '취소',
                // confirmButtonColor: '#d26a5c',
                // confirmButtonText: '확인',
                // closeOnConfirm: false,
                // html: true
            // });
        // });

		// 단체전 점수 입력 오류
        jQuery('.js-point-error').on('click', function(){
            swal({
                title: '점수 재확인 필요',
                text: '1위 점수값이 잘못 입력되었습니다.'+'<br/>'+'자동 설정을 하기위해, 다시 확인 후 입력해 주시기 바랍니다.',
                type: 'warning',
                confirmButtonColor: '#d26a5c',
                confirmButtonText: '확인',
                closeOnConfirm: false,
                html: true
            });
        });

		

		// 대진시간표 빈공간 채우기
        // jQuery('.js-timetable-confirm').on('click', function(){
            // swal({
                // title: '대진시간표 확정',
                // text: '경기 진행단계로 넘어가면 대진시간표를 수정할 수 없습니다.'+'<br/>'+'대진시간표를 이대로 확정하시겠습니까?',
                // type: 'warning',
                // showCancelButton: true,
                // cancelButtonText: '취소',
                // confirmButtonColor: '#d26a5c',
                // confirmButtonText: '확인',
                // closeOnConfirm: false,
                // html: true
            // });
        // });
        
        // Init an error alert on button click
        jQuery('.js-swal-error').on('click', function(){
            swal('Oops...', '시스템이 정상적으로 처리되지 않았습니다!', 'error');
        });

        // Init an example confirm alert on button click
        jQuery('.js-swal-confirm').on('click', function(){
            swal({
                title: 'Are you sure?',
                text: 'You will not be able to recover this imaginary file!',
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#d26a5c',
                confirmButtonText: 'Yes, delete it!',
                closeOnConfirm: false,
                html: false
            }, function () {
                swal('Deleted!', 'Your imaginary file has been deleted.', 'success');
            });
        });
        // 장소 및 일자 취소
        jQuery('.js-swal-confirm').on('click', function(){
            swal({
                title: 'Are you sure?',
                text: 'You will not be able to recover this imaginary file!',
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#d26a5c',
                confirmButtonText: 'Yes, delete it!',
                closeOnConfirm: false,
                html: false
            }, function () {
                swal('Deleted!', 'Your imaginary file has been deleted.', 'success');
            });
        });
    };

    return {
        init: function() {
            // Init randomize bar values
            barsRandomize();

            // Init SweetAlert
            sweetAlert();
        }
    };
}();

// Initialize when page loads
jQuery(function(){ BaseUIActivity.init(); });