<?php
include_once('../common.php');
if (!defined('_GNUBOARD_')) exit; // 개별 페이지 접근 불가
include_once(G5_AOS_PATH.'/head.php');
$menu_cate2 ='competition_view';
$menu_cate3 ='5';
?>

<!-- sub nav -->
<?php include_once('./app_sub_nav.php'); ?>
<!-- end sub nav -->


<?php
	$defaultQstr = "competition_time_tbl.php?c={$c}&d={$d}&s={$s}&ss={$ss}&gi={$gi}&t={$t}";
	$gi = isset($gi) && $gi!='' ? $gi : 0;
	$t = isset($t) && $t!='' ? $t : "L";
?>

<!-- Contents Area -->
<div class="pop_container">
	<?php if(false &&! $competition['opening_date']){?>
		<div class="empty_waiting">
			<div class="img_area">
				<img src="<?php echo G5_IMG_URL.'/common/intro_logo1.png';?>">
			</div>
			<div class="cmt ani02">
				경기가 준비중입니다.
			</div>
		</div>
	<?php }else {?>
	<!-- 탭-코트-->
	<section class="section2">
		<div class="pop_hd">
			<div class="tit">그룹 선택</div>
		</div>
		<div class="content ">
			<div class="btn-group" role="group">

				<ul class="btn-list list3">
					<?php
						$group_sql = "select division, series, series_sub from group_data where match_code = '{$c}'
						group by division, series, series_sub
						order by division, series, series_sub";
						$groups = sql_query($group_sql);
						$is_first = $d!='';
						$active_class = 'class="active"';

						$index = 0;
						while($row = sql_fetch_array($groups)){
							if($d==''){
								$d = $row['division'];
								$s = $row['series'];
								$ss = $row['series_sub'];
							}
							$groupqstr = "?c={$c}&d={$row['division']}&s={$row['series']}&ss={$row['series_sub']}&gi={$index}&t={$t}";
							?>
							<li><a <?=($index."" == $gi? $active_class : "")?> href="<?=$groupqstr?>">
								<?=$row['division']?> <?=$row['series']?> <?=$row['series_sub']?></a></li>

						<?php $index++;
					}?>
				</ul>
	        </div>
	        <!-- 예선 or 본선-->
	        <div class="btn-group" role="group">
				<ul class="btn-list list2">
					<?php
						$tQstr = "competition_time_tbl.php?c={$c}&d={$d}&s={$s}&ss={$ss}&gi={$gi}";
					 ?>
					<li><a <?=($t == 'L' || $t == '' ? $active_class : "")?> href="<?=$tQstr?>&t=L">예선</a></li>
					<li><a <?=($t == 'C' ? $active_class : "")?> href="<?=$tQstr?>&t=C">본선</a></li>
				</ul>
	        </div>
	        <!-- //예선 or 본선-->
		</div>
	</section>
</div>
	<!-- //탭-코트-->


	<!-- 예선 ㅇㅇ ㅇ그룹 ㅇ조 0코트 경기결과 -->
	<?php if($t == 'L'){?>
					<?php
					if($d != '' && $t != ''){
					$winloseSql = "
					SELECT * FROM
					( (SELECT
						code AS match_code, gym_id, gym_name
						FROM match_data JOIN match_gym_data JOIN gym_data
						ON match_data.code = match_gym_data.match_id
						AND match_gym_data.gym_id = gym_data.wr_id
						WHERE match_data.code = '{$c}') match_gym
					JOIN
						(SELECT code AS group_code ,match_code ,num
						,(select concat(club,' ', team_1_name, ' ',team_2_name,' ', team_league_point,' ',team_league_lose_count) from team_data where team_1 = team_data.team_code) as team_1
						,(select concat(club,' ', team_1_name, ' ',team_2_name,' ', team_league_point,' ',team_league_lose_count) from team_data where team_2 = team_data.team_code) as team_2
						,(select concat(club,' ', team_1_name, ' ',team_2_name,' ', team_league_point,' ',team_league_lose_count) from team_data where team_3 = team_data.team_code) as team_3
						,game_court
						FROM tennis2.group_data
						WHERE division = '{$d}' AND series = '{$s}'
						AND series_sub = '{$ss}'
						AND tournament = '{$t}'
						AND match_code = '{$c}') group_data
						ON group_data.match_code = match_gym.match_code )
						order by num";

						$matchProcessSql = "SELECT
					    game_score_data.division,
					    game_score_data.series,
					    game_score_data.series_sub,
					    group_data.code as group_code,
					    group_data.num, team_1_code,
					    (select concat(club,'-', team_1_name,'-',team_2_name) from team_data where team_code = team_1_code) as team_1,
					    (select concat(club,'-', team_1_name,'-',team_2_name) from team_data where team_code = team_2_code) as team_2,
					    team_1_score,
					    team_2_score,
					    end_game
					FROM
					    game_score_data
					        JOIN
					    group_data ON game_score_data.group_code = group_data.code
					WHERE
					    game_score_data.match_code = '{$c}'
							and group_data.division = '{$d}'
							and group_data.series = '{$s}'
							and group_data.series_sub = '{$ss}'
							and group_data.tournament = '{$t}'
					        AND team_1_code <> ''
					ORDER BY game_score_data.division , game_score_data.series , game_score_data.series_sub , group_data.num";

						$matchProcess = sql_query($matchProcessSql, true);
						$matchIndex = 0;
						$match_group_code = '';
						$match_group_text = '';

						$winloseTable = sql_query($winloseSql, true);

						while($row = sql_fetch_array($winloseTable)){
							$sorted = array(explode(' ',$row['team_1']),explode(' ',$row['team_2']),explode(' ',$row['team_3']));


							//맨아래에 cmp선언되있음
							usort($sorted, "cmp"); ?>

							<section>
								<div class="content">

							<div class="con_tit_area clear">
								<div class="tit"><?="$d $s $ss {$row['num']}"?>조</div>
								<div class="r-area">
									<ul>
										<li class="color5 fw-700"><?=$row['gym_name']?> <?=$row['game_court']?>코트 <!--진행중 or 종료--></li>
									</ul>
								</div>
							</div>
							<div class="tbl_style01 tbl_striped mb-20">
								<table>
									<thead>
										<tr>
											<th>순위</th>
											<th>클럽</th>
											<th>선수</th>
											<th>승</th>
											<th>패</th>
										</tr>
									</thead>
									<tbody>
									<?php

									for($index = 0; $index < count($sorted); $index++){?>
										<tr>
											<td class="text-center"><?=$index+1?>위</td>
											<td class="text-center">
												<?=$sorted[$index][0]?><br/>
												<?=$sorted[$index][0]?>
											</td>
											<td class="text-center">
												<?=$sorted[$index][1]?><br/>
												<?=$sorted[$index][2]?>
											</td>
											<td class="text-center">
												<?=$sorted[$index][3]?>승
											</td>
											<td class="text-center">
												<?=$sorted[$index][4]?>패
											</td>
										</tr>
									<?php	}?>
								</tbody>
								</table>
</div>
								<div class="con_tit_area clear">
									<div class="tit">경기결과</div>
								</div>
								<div class="tbl_style02 tbl_striped">
									<table>
										<thead>
											<tr>
												<th>경기</th>
												<th>클럽</th>
												<th>이름</th>
												<th>점수</th>
												<th>이름</th>
												<th>클럽</th>
											</tr>
										</thead>
										<tbody>
											<?php
											$fetch_limit = count($sorted) + $match_index;
											for($j = $match_index; $j < $fetch_limit; $j++){
												$match_row = sql_fetch_array($matchProcess);
												$team1 = explode('-',$match_row['team_1']);
												$team2 = explode('-',$match_row['team_2']);
												?>
												<tr>
													<td class="text-center"><?=$j+1?><br/>경기</td>
													<td class="text-center"><?=$team1['0']?><br/><?=$team1['0']?></td>
													<td class="text-center"><?=$team1['1']?><br/><?=$team1['2']?></td>
													<td class="match_point text-center"><span><?=$match_row['team_1_score']?></span>&nbsp;:&nbsp;<span><?=$match_row['team_2_score']?></span></td>
													<td class="text-center"><?=$team2['1']?><br/><?=$team2['2']?></td>
													<td class="text-center"><?=$team2['0']?><br/><?=$team2['0']?></td>
												</tr>
												<?php }?>
										</tbody>
									</table>
								</div>
							</div>



						</div>
				</section>

			<?php }?>

	<?php }?>

	<!-- //예선 ㅇㅇ ㅇ그룹 ㅇ조 0코트 경기결과 -->
	<?php }else if($t = 'C'){?>


	<!-- 본선 토너먼트 ㅇ그룹 경기 -->

	<!--토너먼트 대진표 -->




	<?php

	  $is_league_end = "SELECT
	        end_game , count(*) as count
	FROM
	    game_score_data
	        JOIN
	    group_data ON game_score_data.group_code = group_data.code
	WHERE
	    game_score_data.match_code = '{$c}'
			AND team_1_code <> ''
      and game_score_data.tournament = 'L'
      and end_game = 'N'
	        GROUP BY end_game
	ORDER BY game_score_data.division , game_score_data.series , game_score_data.series_sub , group_data.num";

	$count_n = sql_fetch($is_league_end)['count'];


	if($count_n == 0){?>
		<div class="content">
			<div>
				<div class="con_tit_area clear">
					<div class="tit text-center">예선 진행중입니다.</div>
				</div>
				<div>
					<!-- <iframe src="http://www.aropupu.fi/bracket/" style="width:100%;height:100%"></iframe> -->
				</div>
			</div>
		</div>
	<?php }else {?>
			<?php
				$tournament_sql = "select
						wr_id
						,team_1_code
						,(select concat(club,' ',team_1_name, ' ',team_2_name ) from team_data where team_data.team_code = team_1_code ) as team_1
						,team_1_score
						,team_2_code
						,(select concat(club,' ',team_1_name, ' ',team_2_name ) from team_data where team_data.team_code = team_2_code ) as team_2
						,team_2_score
						,concat(game_court,'코트') as game_court
						,game_increase
						,tournament
						,tournament_count
						,case when tournament_count = 0 then '결승'
						else concat(tournament_count * 2,'강') end as count_for_display
						,tournament_num
						,end_game
						,case when end_game = 'Y' then '종료'
						else '경기중' end as end_game_display
						,date_format(wr_datetime,get_format(date,'iso')) as datetime_display
						,(select gym_name from gym_data where wr_id = gym_code) as gym_name
						from game_score_data
						where match_code = '{$c}'
						and division = '{$d}'
						and series = '{$s}'
						and series_sub = '{$ss}'
						and tournament != 'L'
						and game_assign = '1' order by wr_id desc ";
						// print $tournament_sql;
						$tournament = sql_query($tournament_sql, true);
						$rount_index = "";
						$round_html;
						$rount_detail = "";
						while($row = sql_fetch_array($tournament)){
							if($round_index == ""){
								$round_index = $row['count_for_display'];
								$round_html .="<a class=\"round_tab active\" data-for=\"{$row['count_for_display']}\">{$row['count_for_display']}</a>";
							}else if($round_index != $row['count_for_display']){
								$round_index = $row['count_for_display'];
								$round_html .="<a class=\"round_tab\" data-for=\"{$round_index}\">{$round_index}</a>";
							}
							$tournament_list[$row['count_for_display']]['body'][] = $row;
						}
			?>
	<!-- <section class="section2">

		<div class="content">
			<div>
				<div class="con_tit_area clear">
					<div class="tit">토너먼트 대진표</div>
				</div>
				<div >

						<div class="tab_group1 mb-0">
							<?= $round_html;?>
						</div>
						<div class="mb-0">
							<ul class="tournament_list" >
								<li><table style="width:100%;">
									<tr>
										<td>8강 1</td>
										<td></td>
									</tr>
									<tr>
										<td></td>
										<td>4강1</td>
									</tr>
									<tr>
										<td>8강 2</td>
										<td></td>
									</tr>
								</table></li>
							</ul>

						</div>
					<?php //<!-- <iframe src="http://www.aropupu.fi/bracket/" style="width:100%;height:100%"></iframe> -->?>
				</div>
			</div>
		</div>
	</section> -->
	<!--//토너먼트 대진표 -->

	<section>
		<div class="content">
			<div class="con_tit_area clear">
				<div class="tit">본선진행 중계</div>
				<div class="r-area">
					<ul>
						<li class="color5 fw-700">8강 진행중</li>
					</ul>
				</div>
			</div>


			<div class="tab_group1 mb-0">
				<?= $round_html;?>
			</div>

			<div class="mb-20">
				<?= tournament_list_html_render($tournament_list)?>
			</div>
		</div>
	</section>


	<?php }?>
<?php }?>
	<!-- //본선 토너먼트 ㅇ그룹 경기 -->
</div>
<!-- end Contents Area -->
<script>
	$('.round_tab').click(function(event){
		$('.round_tab.active').removeClass('active');
		$(this).addClass('active');
		var target = $(this).data('for');
		$('.tournament_list').hide();
		$('#'+target).show();
	})
</script>


<?php if(false){?>
<div class="pop_container">

	<!-- 탭-코트-->
	<section class="section2">
		<div class="pop_hd">
			<div class="tit">경기장 선택</div>
		</div>
		<div class="content ">
			<div class="btn-group" role="group">

				<ul class="btn-list list3">
					<li><a class="active" href="champ_info_matchtbl_level.php?wr_id=236&amp;division=남복&amp;series=20/30&amp;series_sub=A">3그룹 단식</a></li>
					<li><a href="champ_info_matchtbl_level.php?wr_id=236&amp;division=남복&amp;series=20/30&amp;series_sub=A">그룹 타이틀</a></li>
					<li><a href="champ_info_matchtbl_level.php?wr_id=236&amp;division=남복&amp;series=20/30&amp;series_sub=A">2코트</a></li>
				</ul>
	        </div>
	        <!-- 예선 or 본선-->
	        <div class="btn-group" role="group">
				<ul class="btn-list list2">
					<li><a class="active" href="">예선</a></li>
					<li><a href="">본선</a></li>
				</ul>
	        </div>
	        <!-- //예선 or 본선-->
		</div>
	</section>
	<!-- //탭-코트-->


	<!-- 예선 ㅇㅇ ㅇ그룹 ㅇ조 0코트 경기결과 -->
	<section>
		<div class="content">
				<div class="con_tit_area clear">
					<div class="tit">복식 3그룹 1조</div>
					<div class="r-area">
						<ul>
							<li class="color5 fw-700">춘천 송암 1코트 진행중 or 종료</li>
						</ul>
					</div>
				</div>

				<div class="tbl_style01 tbl_striped mb-20">
					<table>
						<thead>
							<tr>
								<th>순위</th>
								<th>클럽</th>
								<th>선수</th>
								<th>승</th>
								<th>패</th>
							</tr>
						</thead>
						<tbody class="text-center">
							<tr>
								<td>1위</td>
								<td>
									레이즈업<br/>
									레이즈업
								</td>
								<td>
									이우선<br/>
									이상훈
								</td>
								<td>
									3승
								</td>
								<td>
									1패
								</td>
							</tr>
							<tr>
								<td>1위</td>
								<td>
									레이즈업<br/>
									레이즈업
								</td>
								<td>
									이우선<br/>
									이상훈
								</td>
								<td>
									3승
								</td>
								<td>
									1패
								</td>
							</tr>
							<tr>
								<td>1위</td>
								<td>
									레이즈업<br/>
									레이즈업
								</td>
								<td>
									이우선<br/>
									이상훈
								</td>
								<td>
									3승
								</td>
								<td>
									1패
								</td>
							</tr>
						</tbody>
					</table>
				</div>

				<div class="con_tit_area clear">
					<div class="tit">경기결과</div>
				</div>
				<div class="tbl_style02 tbl_striped">
					<table>
						<thead>
							<tr>
								<th>경기</th>
								<th>클럽</th>
								<th>이름</th>
								<th>점수</th>
								<th>이름</th>
								<th>클럽</th>
							</tr>
						</thead>
						<tbody class="text-center">
							<tr>
								<td>1<br/>경기</td>
								<td>
									레이즈업<br/>
									레이즈업
								</td>
								<td>
									이우선<br/>
									이상훈
								</td>
								<td class="match_point"><span>6</span>&nbsp;:&nbsp;<span>0</span></td>
								<td>
									이우선<br/>
									이상훈
								</td>
								<td>
									레이즈업<br/>
									레이즈업
								</td>
							</tr>
							<tr>
								<td>1<br/>경기</td>
								<td>
									레이즈업<br/>
									레이즈업
								</td>
								<td>
									이우선<br/>
									이상훈
								</td>
								<td class="match_point">
									<div class="playing ani01">진행중</div>
								</td>
								<td>
									이우선<br/>
									이상훈
								</td>
								<td>
									레이즈업<br/>
									레이즈업
								</td>
							</tr>
						</tbody>
					</table>
				</div>


		</div>
	</section>
	<!-- //예선 ㅇㅇ ㅇ그룹 ㅇ조 0코트 경기결과 -->

	<!-- 본선 토너먼트 ㅇ그룹 경기 -->

	<!--토너먼트 대진표 -->
	<section class="section2">
		<div class="content">
			<div>
				<div class="con_tit_area clear">
					<div class="tit">토너먼트 대진표</div>
				</div>
				<div >
					토너먼트 대진표 출력영역<!--<iframe src="http://www.aropupu.fi/bracket/" style="width:100%;height:100%"></iframe>-->
				</div>
			</div>
		</div>
	</section>
	<!--//토너먼트 대진표 -->

	<section>
		<div class="content">
			<div class="con_tit_area clear">
				<div class="tit">본선진행 중계</div>
				<div class="r-area">
					<ul>
						<li class="color5 fw-700">8강 진행중</li>
					</ul>
				</div>
			</div>

			<div class="tab_group1 mb-0">
    			<a class="active" href="">128강</a>
    			<a class="" href="">64강</a>
    			<a href="">32강</a>
    			<a href="">16강</a>
    			<a href="">8강</a>
    			<a href="">준결승</a>
    			<a href="">결승</a>
        	</div>

			<div class="mb-20">
				<ul class="tournament_list">
					 <div class="status_banner">
					 	3그룹 단식 토너먼트 64강 종료
					 </div>
					<li>
						<div class="tournament_match">
				            <div class="tournament_hd">
			                    <span>64강 4코트 5경기</span>
			                    <span class="r-side-area"></span>
			                </div>

			                <div class="tournament_content">
			                	<div class="l-area">
			                        <div class="teamA">자연별곡 - 이우선가</div>
			                        <div class="teamA">자연자연 - 홍길동</div>
			                    </div>
			                    <div class="tournament_point">
			                    	<!-- 점수입력관리자 기능 (경기시작 > 점수입력 >점수입력 선택 수정팝업 )
			                    		<a href="" class="btn">경기시작</a>
			                    		<a href="" class="btn bkg6">점수입력</a>
			                    		 -->
									<a href="" class="btn">경기시작</a>
								</div>
			                    <div class="r-area">
			                        <div class="teamB">정겨운 - 정겨정겨</div>
			                        <div class="teamB">어경민 - 잠자는바나나</div>
			                    </div>
			                </div>

			                <div class="tournament_ft">
			                	<span>송암체육관</span>
			                	<span class="r-side-area">이히2017-10-17</span>
			                </div>
				        </div>
					</li>
					<li>
						<div class="tournament_match">
				            <div class="tournament_hd">
			                    <span>64강 4코트 5경기</span>
			                    <span class="r-side-area"></span>
			                </div>

			                <div class="tournament_content">
			                	<div class="l-area">
			                        <div class="teamA">자연별곡 - 이우선가</div>
			                        <div class="teamA">자연자연 - 홍길동</div>
			                    </div>
			                    <div class="tournament_point">
			                    	<a href="" class="btn bkg6">점수입력</a>

			                    </div>
			                    <div class="r-area">
			                        <div class="teamB">정겨운 - 정겨정겨</div>
			                        <div class="teamB">어경민 - 잠자는바나나</div>
			                    </div>
			                </div>

			                <div class="tournament_ft">
			                	<span>송암체육관</span>
			                	<span class="r-side-area">이히2017-10-17</span>
			                </div>
				        </div>
					</li>
					<li>
						<div class="tournament_match">
				            <div class="tournament_hd">
			                    <span>64강 4코트 5경기</span>
			                    <span class="r-side-area"></span>
			                </div>

			                <div class="tournament_content">
			                	<div class="l-area">
			                        <div class="teamA">자연별곡 - 이우선가</div>
			                        <div class="teamA">자연자연 - 홍길동</div>
			                    </div>
			                    <div class="tournament_point">
			                    	<!-- 사용자 뷰 경기예정 or 경기중 -->
			                    	<a class="btn">경기예정</a>
			                    </div>
			                    <div class="r-area">
			                        <div class="teamB">정겨운 - 정겨정겨</div>
			                        <div class="teamB">어경민 - 잠자는바나나</div>
			                    </div>
			                </div>

			                <div class="tournament_ft">
			                	<span>송암체육관</span>
			                	<span class="r-side-area">이히2017-10-17</span>
			                </div>
				        </div>
					</li>
					<li>
						<div class="tournament_match">
				            <div class="tournament_hd">
			                    <span>64강 4코트 5경기</span>
			                    <span class="r-side-area"></span>
			                </div>

			                <div class="tournament_content">
			                	<div class="l-area">
			                        <div class="teamA">자연별곡 - 이우선가</div>
			                        <div class="teamA">자연자연 - 홍길동</div>
			                    </div>
			                    <div class="tournament_point">
			                    	<!--
			                    	<a href="">점수입력</a>
			                    	-->
			                    	<div class="result_point">
			                    		<div><span></span>&nbsp;-&nbsp;<span></span></div><!-- 준비중-->
	                    			<div class="playing ani01">진행중</div><!-- 진행중 -->
	                    			<div><span class="win">6</span>&nbsp;:&nbsp;<span>0</span></div><!-- 종료 -->
			                    	</div>

			                    </div>
			                    <div class="r-area">
			                        <div class="teamB">정겨운 - 정겨정겨</div>
			                        <div class="teamB">어경민 - 잠자는바나나</div>
			                    </div>
			                </div>

			                <div class="tournament_ft">
			                	<span>송암체육관</span>
			                	<span class="r-side-area">이히2017-10-17</span>
			                </div>
				        </div>
					</li>
				</ul>

			</div>
		</div>
	</section>
	<!-- //본선 토너먼트 ㅇ그룹 경기 -->
<?php }?>
	<?php }?>
</div>
<?php

	function cmp($a, $b){
		return ($b[3]-0) -($a[3]-0);
	}
	function tournament_table_html_render(){
		$table_template = '<ul class="tournament_list" >
			<li><table style="width:100%;">
				<tr>
					<td>8강 1</td>
					<td></td>
				</tr>
				<tr>
					<td></td>
					<td>4강1</td>
				</tr>
				<tr>
					<td>8강 2</td>
					<td></td>
				</tr>
			</table></li>
		</ul>';

	}

	function tournament_list_html_render($list){
		$wrapper = '<ul class="tournament_list {:is_active}" id="{:id}"><div class="status_banner">{:header}</div>{:list}</ul>';
		$html = '';
		$round = '';
		$tmp_html = '';
		$tmp_wrapper = '';
		$list_stack = '';
		$tmp_list_template = $list_template;
		$is_first = true;
		$is_active = 'active';
		foreach ($list as $key => $value) {
			$round = $key;
			$ttt = 0;
			$list_stack = '';
			$tmp_list_template = $list_template;
			foreach($list[$round]['body'] as $match_key => $match_value){

				$list_stack .= set_list_template($match_value);
			}
			$tmp_wrapper = $wrapper;
			$tmp_wrapper = str_replace("{:is_active}", $is_active, $tmp_wrapper);
			$tmp_wrapper = str_replace("{:id}", "{$key}", $tmp_wrapper);
			$tmp_wrapper = str_replace("{:header}", "{$key} 토너먼트 상태", $tmp_wrapper);
			$html .=str_replace("{:list}", $list_stack, $tmp_wrapper);
			$is_active = "hide";
		}
		return $html;

	}
	function set_list_template($data, $common){
		$list_template= '
<li>
	<div class="tournament_match">
		<div class="tournament_hd">
			<span>{:round} {:court} {:match}</span>
			<span class="r-side-area"></span>
		</div>

		<div class="tournament_content">
			<div class="l-area">
				<div class="teamA">{:club1} - {:team_1_name_1}</div>
				<div class="teamA">{:club1} - {:team_1_name_2}</div>
		</div>
		<div class="tournament_point">
			<div class="result_point">
			{:status}
			</div>
		</div>
		<div class="r-area">
			<div class="teamB">{:team_2_name_1} - {:club2}</div>
			<div class="teamB">{:team_2_name_2} - {:club2}</div>
		</div>
		</div>
		<div class="tournament_ft">
			<span>{:gym_name}</span>
			<span class="r-side-area">{:date}</span>
		</div>
	</div>
</li>';
		// print_r( $data);
		//0			1				2
		//club name1, name2

		$team_1 = explode(' ', $data['team_1']);
		$team_2 = explode(' ', $data['team_2']);

		if($data['end_game'] == 'N'){
			if($data['시간'] == ''){
				$status = '<div><span></span>준비중<span></span></div>';
			}else{
				$status = '<div class="playing ani01">진행중</div>';
			}
		}else{
			$status = '<div><span class="win">'.$data['team_1_score'].'</span>&nbsp;:&nbsp;<span>'.$data['team_2_score'].'</span></div>';
		}

		$list_template = str_replace('{:club1}',$team_1[0], $list_template);
		$list_template = str_replace('{:team_1_name_1}',$team_1[1], $list_template);
		$list_template = str_replace('{:team_1_name_2}',$team_1[2], $list_template);
		$list_template = str_replace('{:status}', $status, $list_template);
		$list_template = str_replace('{:club2}',$team_2[0], $list_template);
		$list_template = str_replace('{:team_2_name_1}',$team_2[1], $list_template);
		$list_template = str_replace('{:team_2_name_2}',$team_2[2], $list_template);
		$list_template = str_replace('{:court}',$data['game_court'], $list_template);
		$list_template = str_replace('{:round}',$data['count_for_display'], $list_template);
		$list_template = str_replace('{:match}','', $list_template);
		$list_template = str_replace('{:gym_name}',$data['gym_name'], $list_template);
		$list_template = str_replace('{:date}',$data['datetime_display'], $list_template);
		// $list_template = str_replace('{:status}',$data['end_game_display'], $list_template);
		// $list_template = str_replace('',$team_1[0], $list_template);
		// $list_template = str_replace('',$team_1[0], $list_template);


		return $list_template;
	}
 ?>
<?php
include_once(G5_AOS_PATH.'/tail.php');
?>
