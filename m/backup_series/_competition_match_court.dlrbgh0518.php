<?php
include_once('../common.php');
if (!defined('_GNUBOARD_')) exit; // 개별 페이지 접근 불가
include_once(G5_AOS_PATH.'/head.php');
$menu_cate2 ='competition_view';
$menu_cate3 ='4';
?>

<!-- sub nav -->
<?php include_once('./app_sub_nav.php'); ?>
<!-- end sub nav -->

<?php

	// $gym_sql = "select *, date_format(wr_datetime, '%Y.%m.%d') as format_wr_datetime
	// from gym_data join match_data join match_gym_data
	// on gym_data.wr_id = match_gym_data.gym_id and match_gym_data.match_id = match_data.code
	// where match_code = '{$c}'";

	$gym_sql = "select gym_data.*, date_format(gym_data.wr_datetime, '%Y.%m.%d') as format_wr_datetime
	from gym_data join match_data join match_gym_data
	on gym_data.wr_id = match_gym_data.gym_id and match_gym_data.match_id = match_data.code
	where match_data.code = '{$c}'";
	$gym = sql_query($gym_sql);
 ?>
<!-- Contents Area -->
<div class="pop_container">
	<?php if(false && ! $competition['opening_date']){?>
		<div class="empty_waiting">
			<div class="img_area">
				<img src="<?php echo G5_IMG_URL.'/common/intro_logo1.png';?>">
			</div>
			<div class="cmt ani02">
				경기가 준비중입니다.
			</div>
		</div>
	<?php }else {?>

	<!-- 탭-코트-->
	<section class="section2">
		<div class="pop_hd">
			<div class="tit">경기장 선택</div>
		</div>
		<div class="content ">
			<select class="full-width form-control" id="gym">
				<option value="-1">경기장 선택</option>
				<?php
					while($row = sql_fetch_array($gym)){?>
						<option value="<?=$row['wr_id']?>"><?=$row['gym_name']?> <?=$row['format_wr_datetime']?></option>
					<?php } ?>
			</select>
			<div id="court_list" class="btn-group" role="group">
				<ul class="btn-list">
					<!-- <li><a class="active" href="champ_info_matchtbl_level.php?wr_id=236&amp;division=남복&amp;series=20/30&amp;series_sub=A">11코트</a></li>
					<li><a href="champ_info_matchtbl_level.php?wr_id=236&amp;division=남복&amp;series=20/30&amp;series_sub=A">22코트</a></li>
					<li><a href="champ_info_matchtbl_level.php?wr_id=236&amp;division=남복&amp;series=20/30&amp;series_sub=A">2코트</a></li>
					<li><a href="champ_info_matchtbl_level.php?wr_id=236&amp;division=남복&amp;series=20/30&amp;series_sub=A">2코트</a></li>
					<li><a href="champ_info_matchtbl_level.php?wr_id=236&amp;division=남복&amp;series=20/30&amp;series_sub=A">2코트</a></li> -->
				</ul>
	        </div>
		</div>
	</section>
	<!-- //탭-코트-->

	<!-- 탭-코트 상황 출력-->
	<section id="scoreSection" class="hide">
		<div class="content">
			<div class="con_tit_area clear">
				<div class="tit">경기진행상황</div>
				<div class="r-area">
					<ul>
						<li class="color5 fw-700" id="process_status">0번경기 진행중</li>
					</ul>
				</div>
			</div>

			<div class="tbl_style02 tbl_striped">
				<table id="process_match_table">
					<thead>
						<tr>
							<th>경기</th>
							<th>클럽</th>
							<th>이름</th>
							<th>점수</th>
							<th>이름</th>
							<th>클럽</th>
						</tr>
					</thead>
					<tbody class="text-center"
					id="process_match_table">
					</tbody>
				</table>
			</div>
		</div>
	</section>
	<!-- //탭-코트 상황 출력-->

	<!--진행완료 승패-->
	<section id="winloseSection" class="hide">>
		<div class="content">
			<!-- <div class="con_tit_area clear">
				<div class="tit">복식 3그룹 1조</div>
			</div> -->

			<div class="tbl_style01 tbl_striped" id="winlose">
			</div>
		</div>
	</section>
	<!--//진행완료 승패-->
	<?php }?>

</div>
<!-- end Contents Area -->

<script>
var c = '<?=$c?>';
var gym_id;
var court_id;
	$('#gym').change(getGymCourts)
	function getGymCourts(event){
		gym_id = $(event.currentTarget).val();
		$.ajax({
			url:g5_url+'/m/ajax/match_court/gym_courts_list.php'
			,data:{c :c, gym_id: gym_id}
			,dataType:'json'
			,type:'post'
			,success:function(data){

				var _html = '';
				var is_active = "active";
				for(var i = 1; i <= data[0].use_court; i++){
					if(i > 1) is_active = '';
					_html += '<li><a class="'+is_active+'" data-court="'+i+'" href="">'+(i)+'코트</a></li>';
				}
				$('#court_list ul').empty().append(_html);
			}
		})
	}
	$('#court_list').on('click', 'ul li a', function(event){
		$('#court_list ul li a').removeClass('active');
		$(event.currentTarget).addClass('active');
		court_id = $(event.currentTarget).data('court');;
		event.preventDefault();
		matchPass(event);
		match_process(event);
		getWinLoseTable(event);
	})
	function matchPass(event){

		$.ajax({
			url:g5_url+'/m/ajax/match_court/matchPass.php'
			,data:{c :c, gym_id: gym_id, court_id:court_id}
			,dataType:'json'
			,type:'post'
			,success:function(data){
				try {
					total = data['N'].count + data['Y'].count;
					$('#process_status').text(data['Y'].count+'째 경기 진행중')
				} catch (e) {

				} finally {

				}

			}
		})

	}
	function match_process(event){

		$.ajax({
			url:g5_url+'/m/ajax/match_court/getMatchProcessData.php'
			,data:{c :c, gym_id: gym_id, court_id:court_id}
			,dataType:'json'
			,type:'post'
			,success:function(data){
				var _html = '';
				if(data.length < 1){
					_html = '<tr><td colspan="6" class="text-center">경기가 없습니다</tr>';
				}else {
					var index = 1;
					var group;
					var group
					for(var i in data){
						if(group == '' || group != data[i].group_code){
							group = data[i].group_code;
							_html += '<tr><td colspan="6">'+data[i].division+data[i].series+data[i].series_sub+' '+data[i].num+' 조</td></tr>'
					}
					var team1 = data[i].team_1.split('-');
					var team2 = data[i].team_2.split('-');
					_html += '<tr>';
					_html += '<td>'+(index++)+'</td>'
					_html += '<td>'+team1[0]+'<br/>'+team1[0]+'</td>';
					_html += '<td>'+team1[1]+'<br/>'+team1[2]+'</td>';
					_html += '<td class="match_point"><span>'+data[i].team_1_score+'</span>&nbsp;:&nbsp;<span>'+data[i].team_2_score+'</span></td>';
					_html += '<td>'+team2[1]+'<br/>'+team2[2]+'</td>';
					_html += '<td>'+team2[0]+'<br/>'+team2[0]+'</td>';
					_html += '</tr>';
					}
				}

				$('#process_match_table tbody').empty().append(_html);
				$('#scoreSection').show();
			}
		})
	}

	function getWinLoseTable(event){

		$.ajax({
			url:g5_url+'/m/ajax/match_court/getWinLoseTable.php'
			,data:{c :c, gym_id: gym_id, court_id:court_id}
			,dataType:'json'
			,type:'post'
			,success:function(data){
				// console.log(data);
				var _html = '';
				if(data.length  < 1){
					// _html = '<tr><td colspan="4" class="text-center">'
				}else{
					var index = 1;
					var group;
					var group
					for(var i in data){
						if(group == '' || group != data[i].group_code){
							group = data[i].group_code;
						}
						var team1 = data[i].team_1.split(' ');
						var team2 = data[i].team_2.split(' ');
						var team3 = data[i].team_3.split(' ');

						var sorted = sort(team1, team2, team3);
						_html +='<table><tr><td colspan="5" class="text-center">'+data[i].division+data[i].series+data[i].series_sub+' '+data[i].num+'조</td></tr><tr><th>순위</th><th>클럽</th><th>선수</th><th>승</th><th>패</th></tr>';

						for(var j = 0; j < sorted.length ; j++){
							_html += '<tr>';
							_html += '<td class="text-center">'+(j+1)+'위</td>'
							_html += '<td class="text-center">'+sorted[j][0]+'<br/>'+sorted[j][0]+'</td>';
							_html += '<td class="text-center">'+sorted[j][1]+'<br/>'+sorted[j][2]+'</td>'
							_html += '<td class="text-center">'+sorted[j][3]+'</td><td class="text-center">'+sorted[j][4]+'</td>'
							_html += '</tr>'
						}
						_html += '</table>'
					}
					$('#winlose').empty().append(_html);
					$('#winloseSection').show();
				}
			}
		})
	}
	function sort(t1, t2, t3){
		var t =[t1, t2, t3];
		t.sort(function(a,b){
			// console.log(a)
			return b[3]-a[3];
		})

		return t;
	}
</script>
<?php
include_once(G5_AOS_PATH.'/tail.php');
?>
