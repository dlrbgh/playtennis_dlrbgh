<?php
include_once('../common.php');
if (!defined('_GNUBOARD_')) exit; // 개별 페이지 접근 불가
include_once(G5_AOS_PATH.'/head.php');
$menu_cate2 ='competition_view';
$menu_cate3 ='4';
?>

<!-- sub nav -->
<?php include_once('./app_sub_nav.php'); ?>
<!-- end sub nav -->
<div class="pop_container">
	<?php if(false && ! $competition['opening_date']){?>
		<div class="empty_waiting">
			<div class="img_area">
				<img src="<?php echo G5_IMG_URL.'/common/intro_logo1.png';?>">
			</div>
			<div class="cmt ani02">
				경기가 준비중입니다.
			</div>
		</div>
	<?php }else {?>

		<?php

			$getGymdateSql = "select date_format(match_gym_data.application_period, get_format(date, 'iso'))
									as format_wr_datetime from match_data join match_gym_data
									on match_data.code = match_gym_data.match_id
									where code = '{$c}' group by application_period";
			$date = sql_query($getGymdateSql);?>
			<section>
				<div class="pop_hd sound_only">
					<div class="tit">
						선택
					</div>
				</div>
				<div class="content">
					<div id="court_list" class="btn-group" role="group">
					<ul class="btn-list">
						<?php while($row = sql_fetch_array($date)){
							$d == '' ? $d = $row['format_wr_datetime'] : $d;
							?>
							<li><a href="?c=<?=$c?>&d=<?=urlencode($row['format_wr_datetime'])?>"
								<?=$d == $row['format_wr_datetime'] ? 'class="active"':""?>
								>
								<?=$row['format_wr_datetime']?></a></li>
						<?php }?>
	 				</ul>
				</div>

<?php
	$gym_sql = "select * from match_data join match_gym_data join gym_data
					on match_data.code = match_gym_data.match_id
					and match_gym_data.gym_id = gym_data.wr_id
					where code = '{$c}' and date_format(match_gym_data.application_period, get_format(date, 'iso')) = '{$d}'
					";

	// print $gym_sql;
	$gymdata = sql_query($gym_sql);



	$use_court = 1;

 ?>

	<div class="pop_hd sound_only">
		<div class="tit">경기장 선택</div>
	</div>

		<!-- <?php print_r($gym_data)?> -->

		<select class="full-width form-control" id="gym">
			<option value="">경기장 선택</option>
			<?php
				while($row = sql_fetch_array($gymdata)){
					$g == '' ? $g = $row['wr_id'] : $g;
					?>
					<option value="<?=$row['wr_id']?>"
						<?php if($g == $row['wr_id']){
							print 'selected="selected"';
							$use_court = $row['use_court'];
						}?>
						><?=$row['gym_name']?></option>
				<?php }
			?>
		</select>
		<script>
		$('#gym').change(function(event){
			window.location.href='?c=<?=$c?>&d='+encodeURIComponent('<?=$d?>')+'&g='+$(this).val();});
		</script>

		<div id="court_list" class="btn-group" role="group">
			<ul class="btn-list">
				<?php
					$ct = $ct == '' ? 1 : $ct;
					for($i = 0; $i < $use_court ; $i++){
						?>
						<li><a href="?c=<?=$c?>&d=<?=$d?>&g=<?=$g?>&ct=<?=$i+1?>"
							<?=$ct == $i+1 ? 'class="active"' : ''?>
							><?=$i+1?>코트</a></li>
					<?php }?>
			</ul>
		</div>
	</div>
</section>

<!-- 토너먼트 -->
<?php
$is_league_finish = false;
// 이번 경기(6월초)는 코트페이지에서 본선경기를 보여주지않도록
// 코트배정이 동적이고 사람들마다 보여줘야하니까 일단 뺍니다
// 5.29 이규호

if($is_league_finish ){?>
	<section>
		<div class="pop_hd">
			<div class="tit">본선</div>
		</div>
		<div class="content ">
			<div class="tbl_style02 tbl_striped">

			<table>
					<thead>
						<tr>
							<th>경기</th>
							<th>클럽</th>
							<th width="60">이름</th>
							<th>점수</th>
							<th width="60">이름</th>
							<th>클럽</th>
						</tr>
					</thead>
					<tbody>
						<?php

							$tournament_sql  = "select *
													,division, series, series_sub
													,case when division = '단체전' then team_1_event_code
													else team_1_code end as team_1_code
													,case when division = '단체전' then team_2_event_code
													else team_2_code  end as team_2_code
													,case when division = '단체전'
													then (select concat(club,'-',team_1_name,'-', team_2_name) from team_data where team_code	= team_1_event_code)
													else (select concat(club,'-',team_1_name,'-', team_2_name) from team_data where team_code	= team_1_code) end as team_1
													,case when division = '단체전'
													then (select concat(club,'-',team_1_name,'-', team_2_name) from team_data where team_code	= team_2_event_code)
													else (select concat(club,'-',team_1_name,'-', team_2_name) from team_data where team_code	= team_2_code) end as team_2

													from game_score_data
													where match_code = '{$c}'
													and gym_code = {$g}
													and tournament <> 'L'
													and game_date = '{$d}'
													and game_court = {$ct}
													order by court_array_num";

							// and gym_code = {$g}
							// and date_format(game_date, get_format(date, 'iso')) = '{$d}'
							// and game_court = {$ct}
							// print $tournament_sql;
							$tournament_court_list = sql_query($tournament_sql, true);

							if($tournament_court_list->num_rows < 1){?>
								<tr>
									<td colspan="6" class="text-center">경기가 없습니다</td>
								</tr>
							<?php }else
							while($row = sql_fetch_array( $tournament_court_list)){
								// print_r($row);
								$team_1_info = explode('-',$row['team_1']);
								$team_2_info = explode('-',$row['team_2']);
								// print_r($)
								if($row['team_1_code'] == ''){?>
									<tr>
										<td class="text-center"><?=$row['court_array_num']?>경기</td>
										<td colspan="5" class="text-center">준비중입니다</td>
									</tr>
								<?php }else{?>
									<tr>
										<td><?=$row['court_array_num']?>경기</td>
										<td><?=$team_1_info[0]?><br /><?=$team_1_info[0]?></td>
										<td><?=$team_1_info[1]?><br /><?=$team_1_info[2]?></td>
										<td class="match_point text-center">
										<?php if($row['team_1_dis'] > 0){
											print '<div class="skip"><span>ㅡ</span> : <span>승</span></div>';
										}else if($row['team_2_dis'] > 0){
											print '<div class="skip"><span>승</span> : <span>ㅡ</span></div>';
										}else{
												 if($row['is_on'] == 'N' && $row['end_game'] == 'N'){?>
												<!-- <div><span></span>&nbsp;준비중&nbsp;<span></span></div> -->
												<div class="">
													<span style="font-size:16px">준비중</span>
												</div>
											<?php }else if($row['end_game'] == 'Y'){?>
												<div><span class="win"><?=$row['team_1_score']?></span>&nbsp;:&nbsp;<span><?=$row['team_2_score']?></span></div>
											<?php }else if($row['is_on'] == 'Y' && $row['game_start_time'] != ''){?>
												<div class="playing ani01">진행중</div>
											<?php }

											}?>
											<!-- <?php if($row['is_on'] == 'N' && $row['end_game'] == 'N'){?>
												<span></span>
												준비중
												<span></span>
											<?php }else if($row['end_game'] == 'Y'){?>
												<?=$row['team_1_score']?> : <?=$row['team_2_score']?>
											<?php }else if($row['is_on'] == 'Y' && $row['game_start_time'] != ''){?>
												<span></span>
												진행중
												<span></span>
											<?php }?> -->
										</td>
										<td><?=$team_2_info[1]?><br /><?=$team_2_info[2]?></td>
										<td><?=$team_2_info[0]?><br /><?=$team_2_info[0]?></td>
									</tr>
							<?php }
						}?>
					</tbody>
				</table>
			</div>
		</div>
	</section>
<?php }?>









<?php
	$i = 0;
$sql = "select * from group_data
			where match_code='$c'
			and tournament	='L'
			and gym_code = {$g}
			and game_court = {$ct}
			and date_format(game_date, '%Y-%m-%d') = '{$d}'
			order by court_array_num desc";
$game_score ="select code from game_score_data as a
		where a.match_code='{$c}' and a.tournament = 'L' and a.game_court = '{$ct}' and
		a.gym_code = '{$g}' and a.game_date = '{$d}' order by a.court_array_num desc";
$game_result = sql_query($game_score);
// print $sql;

$game_index = 1;
$list;
	// while($row = sql_fetch_array($game_result)){
	// 	$game_result_list[$row['code']] = $game_index++ ;
	// }
	$group_result = sql_query($sql);

	$targetTeam_data = "team_data";

	$match_index = 1;



	while($group = sql_fetch_array($group_result)){
		if($group['division'] == '단체전') $targetTeam_data = "team_event_data";
		$group_code =	$group['code'];
		$game_status = is_on($group_code)?>
		<section>
			<div class="content">
				<div class="con_tit_area clear">
					<div class="tit"><?=$group['division']?> <?=$group['series']?> <?=$group['series_sub']?>-<?php echo $group['num']+1?>조</div>
						<div class="r-area">
							<ul>
								<li class="color5 fw-700"><?=$game_status['status_display']?></li>
							</ul>
						</div>
					</div>
					<!-- 임시 주석 0601 이우선
					<div class="tbl_style01 tbl_striped mb-20">
						<table>
							<thead>
								<tr>
									<th><?php
											if($game_status['status']==3){
												echo "순위";
											}else{
												echo "번호";
											}
											?></th>
									<th>클럽</th>
									<th>선수</th>
									<th>승</th>
									<th>패</th>
								</tr>
							</thead>
							<tbody class="text-center">
								<?php
									$team_data = array();
									for($team_index = 1; $team_index < 6; $team_index ++){
										if($group['team_'.+$team_index] == '') continue;
										$getWinLoseDataSql = "select * from {$targetTeam_data}
										where team_code = '{$group['team_'.+$team_index]}'";
										$team_data[] = sql_fetch($getWinLoseDataSql);}
										usort($team_data, "cmp");
										$ranking = 1;
										$this_tbody = '';
										foreach($team_data as $key => $value){
											$end_game[$value['end_game']] =$end_game[$value['end_game']]+1;
											$go_tournament = '';
											if($game_status['status'] == 3 && $ranking == 1){
												// $go_tournament = '<br />토너먼트 진출';
											}
											$this_tbody .="<tr>";
											$this_tbody .="<td>{$ranking}";
															if($game_status['status']==3){
																echo "위";
															}else{
																echo "";
															}
																"{$go_tournament}</td>";
											$this_tbody .="<td>{$value['club']}<br />{$value['club']}</td>";
											$this_tbody .="<td>{$value['team_1_name']}<br />{$value['team_2_name']}</td>";
											$this_tbody .="<td>{$value['team_league_point']}승</td>";
											$this_tbody .="<td>{$value['team_league_lose_count']}패</td>";
											$this_tbody .="</tr>";
											$ranking ++;
								 }
								 ?>
								 <?=$this_tbody?>
							</tbody>
						</table>
				</div>
				<div class="con_tit_area clear">
					<div class="tit">경기결과</div>
				</div>
				//임시 주석 0601 이우선 -->
				<div class="tbl_style02 tbl_striped">
				<table>
					<thead>
						<tr>
							<th>경기</th>
							<th>클럽</th>
							<th width="60">이름</th>
							<th>점수</th>
							<th width="60">이름</th>
							<th>클럽</th>
						</tr>
					</thead>
					<tbody>
						<?php
							$game_score_sql = "select * from game_score_data
							where group_code = '{$group_code}' and tournament = 'L' order by court_array_num desc, term desc";
							$game_score = sql_query($game_score_sql);
							while($row = sql_fetch_array($game_score)){
								if($group['division'] == '단체전'){
									$team_1_info = sql_fetch("select * from team_event_data where team_code = '{$row['team_1_event_code']}'");
									$team_2_info = sql_fetch("select * from team_event_data where team_code = '{$row['team_2_event_code']}'");
								}else{
									$team_1_info = sql_fetch("select * from team_data where team_code = '{$row['team_1_code']}'");
									$team_2_info = sql_fetch("select * from team_data where team_code = '{$row['team_2_code']}'");
								}
								?>
								<tr>
									<!-- <td class="text-center"><?=$game_result_list[$row['code']]?><br>경기</td> -->
									<td class="text-center"><?=($match_index++)?><br>경</br>기</td>
									<td class="text-center">
										<?= $team_1_info['area_2']."-".$team_1_info['club']?><br><?= $team_1_info['area_2']."-".$team_1_info['club']?>
									</td>
									<td class="text-center "><?= $team_1_info['team_1_name']."(".$team_1_info['team_1_grade'].")"?><br><?= $team_1_info['team_2_name']."(".$team_1_info['team_2_grade'].")"?></td>
									<td class="match_point text-center">
										<?php if($row['team_1_dis'] > 0){
											print '<div class="skip"><span>ㅡ</span> : <span>승</span></div>';
										}else if($row['team_2_dis'] > 0){
											print '<div class="skip"><span>승</span> : <span>ㅡ</span></div>';
										}else{
											 if($row['is_on'] == 'N' && $row['end_game'] == 'N'){?>
											<!-- <div><span></span>&nbsp;준비중&nbsp;<span></span></div> -->
											<div class="">
												<span style="font-size:16px">준비중</span>
											</div>
										<?php }else if($row['end_game'] == 'Y'){?>
											<div><span class="win"><?=$row['team_1_score']?></span>&nbsp;:&nbsp;<span><?=$row['team_2_score']?></span></div>
										<?php }else if($row['is_on'] == 'Y' && $row['game_start_time'] != ''){?>
											<div class="playing ani01">진행중</div>
										<?php }

										}?>
										<!-- <span><?= $row['team_1_score']?></span>&nbsp;:&nbsp;
										<span><?= $row['team_2_score']?></span> -->
									</td>
									<td class="text-center "><?= $team_2_info['team_1_name']."(".$team_2_info['team_1_grade'].")"?><br><?= $team_2_info['team_2_name']."(".$team_2_info['team_2_grade'].")"?></td>
									<td class="text-center"><?= $team_2_info['area_2']."(".$team_2_info['club'].")"?><br><?= $team_2_info['area_2']."(".$team_2_info['club'].")"?></td>
								</tr>
							<?php }?>
						</tbody>
				</table>
			</div>
			</div>
		</section>
	<?php $i++;} ?>
<?php $i++;} ?>

</div>
<?php
function cmp($a, $b){
	return $b['team_league_point'] - $a['team_league_point'];
}
function is_on($group_code, $tournament_sql = " and tournament = 'L'"){

	$game_state_array = array(1=>'예정', 2=>'진행중', 3=>'종료');
	$sql = "select end_game, is_on, ifnull(game_start_time,'') as game_start_time
	,  ifnull(game_end_time,'') as game_end_time from game_score_data where group_code = '{$group_code}' $tournament_sql";
	// print $sql;
	$result = sql_query($sql,true);
	$game = $result->num_rows;
	$end_game = array('N'=>0, 'Y'=>0);
	$is_on  = array('N'=>0, 'Y'=>0);
	$game_start_time;
	$game_end_time;
	while($row = sql_fetch_array($result)){
		$end_game[$row['end_game']] =$end_game[$row['end_game']] +1;
		$is_on[$row['is_on']] = $is_on[$row['is_on']] +1;
		$game_start_time[$row['game_start_time']]
		= $game_start_time[$row['game_start_time']] == '' ? 1 :  ($game_start_time[$row['game_start_time']]-0 )+1;

	}
	if($end_game['N'] == $game){
		if($game_start_time[''] == $game){
			return array('status'=>1, 'status_display'=>$game_state_array[1]);
		}else if($game_start_time[''] < $game){
			return array('status'=>2, 'status_display'=>$game_state_array[2]);
		}
	}
	if($end_game['Y'] == $game) {
		return array('status'=>3, 'status_display'=>$game_state_array[3]);
	}
}
function is_league_finish($c, $d, $g, $ct){
	$sql = "select end_game, count(*) as count
	from game_score_data
	where match_code = '{$c}' and tournament = 'L' group by end_game";
	$result = sql_query($sql);
	$r;
	while($row = sql_fetch_array($result)){
		$r[$row['end_game']] = $row['count'];
	}
	if(isset($r['N']) && $r['N'] == 0){
		return true;
	}else{
		return false;
	}
	// if($r
}
?>

<?php
include_once(G5_AOS_PATH.'/tail.php');
?>
