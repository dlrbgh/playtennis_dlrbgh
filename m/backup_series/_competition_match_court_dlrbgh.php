<?php
include_once('../common.php');
if (!defined('_GNUBOARD_')) exit; // 개별 페이지 접근 불가
include_once(G5_AOS_PATH.'/head.php');
$menu_cate2 ='competition_view';
$menu_cate3 ='4';
?>

<!-- sub nav -->
<?php include_once('./app_sub_nav.php'); ?>
<!-- end sub nav -->

<?php

	// $gym_sql = "select *, date_format(wr_datetime, '%Y.%m.%d') as format_wr_datetime
	// from gym_data join match_data join match_gym_data
	// on gym_data.wr_id = match_gym_data.gym_id and match_gym_data.match_id = match_data.code
	// where match_code = '{$c}'";

	$gym_sql = "select gym_data.*, date_format(gym_data.wr_datetime, '%Y.%m.%d') as format_wr_datetime
	,use_court
	from gym_data join match_data join match_gym_data
	on gym_data.wr_id = match_gym_data.gym_id and match_gym_data.match_id = match_data.code
	where match_data.code = '{$c}'";
	$gym = sql_query($gym_sql);
	$date_data;
	$gym_data;


	while ($row = sql_fetch_array($gym)) {
		$date_data[$row['format_wr_datetime']] = $row;
		$gym_data[$row['format_wr_datetime']][$row['wr_id']] = $row;
	}

 ?>
<!-- Contents Area -->
<div class="pop_container">
	<?php if(false && ! $competition['opening_date']){?>
		<div class="empty_waiting">
			<div class="img_area">
				<img src="<?php echo G5_IMG_URL.'/common/intro_logo1.png';?>">
			</div>
			<div class="cmt ani02">
				경기가 준비중입니다.
			</div>
		</div>
	<?php }else {?>

			<?php
			if (count($date_data) > 1) {?>
				<section>
					<div class="pop_hd">
						<div class="tit">
							날짜 선택
						</div>
					</div>
					<div class="content">
						<div id="court_list" class="btn-group" role="group">
						<ul class="btn-list">
				<?php
						foreach($date_data as $date){
							?>
							<li><a href="?c=<?=$c?>&d=<?=$date['format_wr_datetime']?>"
								<?= $d == $date['format_wr_datetime'] ? 'class="active"' : ''?>
								>
								<?=$date['format_wr_datetime']?></a></li>
						<?php }?>
						</ul>
					</div>
					</div>
				</section>
				<?php }else{
					$dt = array_keys($date_data)[0];
					// $d = $date_date[0]['format_wr_datetime'];
				}?>
	<!-- 탭-코트-->
	<section class="section2">
		<div class="pop_hd">
			<div class="tit">경기장 선택</div>
		</div>
		<div class="content ">
			<!-- <?php print_r($gym_data)?> -->

			<select class="full-width form-control" id="gym">
				<option value="">경기장 선택</option>
				<?php

					foreach($gym_data[$dt] as $gym_key => $value){?>
						<option value="<?=$value['wr_id']?>"
							<?=  $g == '' || $g == $value['wr_id'] ? "selected=\"selected\"" :""?>>
							<?=$value['gym_name']?></option>
					<?php
					if($g == '') $g = $value['wr_id'];
				}?>

			</select>
			<div id="court_list" class="btn-group" role="group">
				<ul class="btn-list">
					<?php
					$court = $gym_data[$dt][$g]['use_court'];
					if($ct == ''){$ct = 1;}
					for ($i=0; $i < $court; $i++) {?>
						<li><a <?=$ct ==  $i+1? 'class="active"':""?>
							href="?c=<?=$c?>&dt=<?=$dt?>&g=<?=$g?>&ct=<?=$i+1?>">
							<?=$i+1?>코트</a></li>
					<?php

					}?>
				</ul>
	        </div>
		</div>
	</section>
	<!-- //탭-코트-->
	<?php
		if($dt != '' && $g != ''  && $ct != ''){
		$winloseSql = "
		SELECT * FROM
		( (SELECT
			code AS match_code, gym_id, gym_name
			FROM match_data JOIN match_gym_data JOIN gym_data
			ON match_data.code = match_gym_data.match_id
			AND match_gym_data.gym_id = gym_data.wr_id
			WHERE match_data.code = '{$c}'
			and date_format(gym_data.wr_datetime, '%Y.%m.%d') = '{$dt}'
			and gym_data.wr_id = {$g}
			) match_gym
		JOIN
			(SELECT code AS group_code ,match_code ,num
			,(select concat(club,' ', team_1_name, ' ',team_2_name,' ', team_league_point,' ',team_league_lose_count) from team_data where team_1 = team_data.team_code) as team_1
			,(select concat(club,' ', team_1_name, ' ',team_2_name,' ', team_league_point,' ',team_league_lose_count) from team_data where team_2 = team_data.team_code) as team_2
			,(select concat(club,' ', team_1_name, ' ',team_2_name,' ', team_league_point,' ',team_league_lose_count) from team_data where team_3 = team_data.team_code) as team_3
			,game_court
			FROM tennis2.group_data
			WHERE  match_code = '{$c}') group_data
			ON group_data.match_code = match_gym.match_code )
			where game_court = {$ct}
			order by num";

			// print $winloseSql;
			$matchProcessSql = "SELECT
				group_data.code as group_code,
				group_data.num, team_1_code,
				(select concat(club,'-', team_1_name,'-',team_2_name) from team_data where team_code = team_1_code) as team_1,
				(select concat(club,'-', team_1_name,'-',team_2_name) from team_data where team_code = team_2_code) as team_2,
				team_1_score,
				team_2_score,
				end_game
		FROM
				game_score_data
						JOIN
				group_data ON game_score_data.group_code = group_data.code
		WHERE
				game_score_data.match_code = '{$c}'
				and group_data.game_court = {$ct}
				AND team_1_code <> ''
		ORDER BY group_code, num";
			$matchProcess = sql_query($matchProcessSql, true);
			$matchIndex = 0;
			$match_group_code = '';
			$match_group_text = '';

			$winloseTable = sql_query($winloseSql, true);

			while($row = sql_fetch_array($winloseTable)){
				$sorted = array(explode(' ',$row['team_1']),explode(' ',$row['team_2']),explode(' ',$row['team_3']));


				//맨아래에 cmp선언되있음
				usort($sorted, "cmp"); ?>

				<section>
					<div class="content">

				<div class="con_tit_area clear">
					<div class="tit"><?="$d $s $ss {$row['num']}"?>조</div>
					<div class="r-area">
						<ul>
							<li class="color5 fw-700"><?=$row['gym_name']?> <?=$row['game_court']?>코트 <!--진행중 or 종료--></li>
						</ul>
					</div>
				</div>
				<div class="tbl_style01 tbl_striped mb-20">
					<table>
						<thead>
							<tr>
								<th>순위</th>
								<th>클럽</th>
								<th>선수</th>
								<th>승</th>
								<th>패</th>
							</tr>
						</thead>
						<tbody>
						<?php

						for($index = 0; $index < count($sorted); $index++){?>
							<tr>
								<td class="text-center"><?=$index+1?>위</td>
								<td class="text-center">
									<?=$sorted[$index][0]?><br/>
									<?=$sorted[$index][0]?>
								</td>
								<td class="text-center">
									<?=$sorted[$index][1]?><br/>
									<?=$sorted[$index][2]?>
								</td>
								<td class="text-center">
									<?=$sorted[$index][3]?>승
								</td>
								<td class="text-center">
									<?=$sorted[$index][4]?>패
								</td>
							</tr>
						<?php	}?>
					</tbody>
					</table>
		</div>
					<div class="con_tit_area clear">
						<div class="tit">경기결과</div>
					</div>
					<div class="tbl_style02 tbl_striped">
						<table>
							<thead>
								<tr>
									<th>경기</th>
									<th>클럽</th>
									<th>이름</th>
									<th>점수</th>
									<th>이름</th>
									<th>클럽</th>
								</tr>
							</thead>
							<tbody>
								<?php
								//$row['num']
								$fetch_limit = count($sorted) + $match_index;
								for($j = $match_index; $j < $fetch_limit; $j++){
									$match_row = sql_fetch_array($matchProcess);
									$team1 = explode('-',$match_row['team_1']);
									$team2 = explode('-',$match_row['team_2']);
									?>
									<tr>
										<td class="text-center"><?=$j+1?><br/>경기</td>
										<td class="text-center"><?=$team1['0']?><br/><?=$team1['0']?></td>
										<td class="text-center"><?=$team1['1']?><br/><?=$team1['2']?></td>
										<td class="match_point text-center"><span><?=$match_row['team_1_score']?></span>&nbsp;:&nbsp;<span><?=$match_row['team_2_score']?></span></td>
										<td class="text-center"><?=$team2['1']?><br/><?=$team2['2']?></td>
										<td class="text-center"><?=$team2['0']?><br/><?=$team2['0']?></td>
									</tr>
									<?php }?>
							</tbody>
						</table>
					</div>
				</div>



			</div>
		</section>

		<?php }?>

		<?php }?>

	<?php }?>

</div>
<!-- end Contents Area -->

<script>
$('#gym').change(function(event){
	window.location.href='?c=<?=$c?>&d='+encodeURIComponent('<?=$d?>')+'&g='+$(this).val();
});
// var c = '<?=$c?>';
// var gym_id;
// var court_id;
// 	$('#gym').change(getGymCourts)
// 	function getGymCourts(event){
// 		gym_id = $(event.currentTarget).val();
// 		$.ajax({
// 			url:g5_url+'/m/ajax/match_court/gym_courts_list.php'
// 			,data:{c :c, gym_id: gym_id}
// 			,dataType:'json'
// 			,type:'post'
// 			,success:function(data){
//
// 				var _html = '';
// 				var is_active = "active";
// 				for(var i = 1; i <= data[0].use_court; i++){
// 					if(i > 1) is_active = '';
// 					_html += '<li><a class="'+is_active+'" data-court="'+i+'" href="">'+(i)+'코트</a></li>';
// 				}
// 				$('#court_list ul').empty().append(_html);
// 			}
// 		})
// 	}
// 	$('#court_list').on('click', 'ul li a', function(event){
// 		$('#court_list ul li a').removeClass('active');
// 		$(event.currentTarget).addClass('active');
// 		court_id = $(event.currentTarget).data('court');;
// 		event.preventDefault();
// 		matchPass(event);
// 		match_process(event);
// 		getWinLoseTable(event);
// 	})
// 	function matchPass(event){
//
// 		$.ajax({
// 			url:g5_url+'/m/ajax/match_court/matchPass.php'
// 			,data:{c :c, gym_id: gym_id, court_id:court_id}
// 			,dataType:'json'
// 			,type:'post'
// 			,success:function(data){
// 				try {
// 					total = data['N'].count + data['Y'].count;
// 					$('#process_status').text(data['Y'].count+'째 경기 진행중')
// 				} catch (e) {
//
// 				} finally {
//
// 				}
//
// 			}
// 		})
//
// 	}
// 	function match_process(event){
//
// 		$.ajax({
// 			url:g5_url+'/m/ajax/match_court/getMatchProcessData.php'
// 			,data:{c :c, gym_id: gym_id, court_id:court_id}
// 			,dataType:'json'
// 			,type:'post'
// 			,success:function(data){
// 				var _html = '';
// 				if(data.length < 1){
// 					_html = '<tr><td colspan="6" class="text-center">경기가 없습니다</tr>';
// 				}else {
// 					var index = 1;
// 					var group;
// 					var group
// 					for(var i in data){
// 						if(group == '' || group != data[i].group_code){
// 							group = data[i].group_code;
// 							_html += '<tr><td colspan="6">'+data[i].division+data[i].series+data[i].series_sub+' '+data[i].num+' 조</td></tr>'
// 					}
// 					var team1 = data[i].team_1.split('-');
// 					var team2 = data[i].team_2.split('-');
// 					_html += '<tr>';
// 					_html += '<td>'+(index++)+'</td>'
// 					_html += '<td>'+team1[0]+'<br/>'+team1[0]+'</td>';
// 					_html += '<td>'+team1[1]+'<br/>'+team1[2]+'</td>';
// 					_html += '<td class="match_point"><span>'+data[i].team_1_score+'</span>&nbsp;:&nbsp;<span>'+data[i].team_2_score+'</span></td>';
// 					_html += '<td>'+team2[1]+'<br/>'+team2[2]+'</td>';
// 					_html += '<td>'+team2[0]+'<br/>'+team2[0]+'</td>';
// 					_html += '</tr>';
// 					}
// 				}
//
// 				$('#process_match_table tbody').empty().append(_html);
// 				$('#scoreSection').show();
// 			}
// 		})
// 	}
//
// 	function getWinLoseTable(event){
//
// 		$.ajax({
// 			url:g5_url+'/m/ajax/match_court/getWinLoseTable.php'
// 			,data:{c :c, gym_id: gym_id, court_id:court_id}
// 			,dataType:'json'
// 			,type:'post'
// 			,success:function(data){
// 				// console.log(data);
// 				var _html = '';
// 				if(data.length  < 1){
// 					// _html = '<tr><td colspan="4" class="text-center">'
// 				}else{
// 					var index = 1;
// 					var group;
// 					var group
// 					for(var i in data){
// 						if(group == '' || group != data[i].group_code){
// 							group = data[i].group_code;
// 						}
// 						var team1 = data[i].team_1.split(' ');
// 						var team2 = data[i].team_2.split(' ');
// 						var team3 = data[i].team_3.split(' ');
//
// 						var sorted = sort(team1, team2, team3);
// 						_html +='<table><tr><td colspan="5" class="text-center">'+data[i].division+data[i].series+data[i].series_sub+' '+data[i].num+'조</td></tr><tr><th>순위</th><th>클럽</th><th>선수</th><th>승</th><th>패</th></tr>';
//
// 						for(var j = 0; j < sorted.length ; j++){
// 							_html += '<tr>';
// 							_html += '<td class="text-center">'+(j+1)+'위</td>'
// 							_html += '<td class="text-center">'+sorted[j][0]+'<br/>'+sorted[j][0]+'</td>';
// 							_html += '<td class="text-center">'+sorted[j][1]+'<br/>'+sorted[j][2]+'</td>'
// 							_html += '<td class="text-center">'+sorted[j][3]+'</td><td class="text-center">'+sorted[j][4]+'</td>'
// 							_html += '</tr>'
// 						}
// 						_html += '</table>'
// 					}
// 					$('#winlose').empty().append(_html);
// 					$('#winloseSection').show();
// 				}
// 			}
// 		})
// 	}
// 	function sort(t1, t2, t3){
// 		var t =[t1, t2, t3];
// 		t.sort(function(a,b){
// 			// console.log(a)
// 			return b[3]-a[3];
// 		})
//
// 		return t;
// 	}
</script>
<?php
include_once(G5_AOS_PATH.'/tail.php');
?>
