<?php
include_once('../common.php');
if (!defined('_GNUBOARD_')) exit; // 개별 페이지 접근 불가
include_once(G5_AOS_PATH.'/head.php');
$menu_cate2 ='management';
$menu_cate3 ='3';
?>

<!-- sub nav -->
<?php include_once('./app_sub_nav.php'); ?>
<!-- end sub nav -->
<?php
	$match_sql = "select * from match_data";
	$match_result = sql_query($match_sql);
?>

<?php

	$gym_sql = "select gym_data.*, date_format(gym_data.wr_datetime, '%Y.%m.%d') as format_wr_datetime
	,use_court
	from gym_data join match_data join match_gym_data
	on gym_data.wr_id = match_gym_data.gym_id and match_gym_data.match_id = match_data.code
	where match_data.code = '{$c}'";
	
	$gym = sql_query($gym_sql);
	$date_data;
	$gym_data;


	while ($row = sql_fetch_array($gym)) {
		$date_data[$row['format_wr_datetime']] = $row;
	}
 ?>
<!-- Contents Area -->
<div class="pop_container">
	<?php if(false && ! $competition['opening_date']){?>
		<div class="empty_waiting">
			<div class="img_area">
				<img src="<?php echo G5_IMG_URL.'/common/intro_logo1.png';?>">
			</div>
			<div class="cmt ani02">
				경기가 준비중입니다.
			</div>
		</div>
	<?php }else {?>

			<section>
				<div class="pop_hd">
					대회선택
				</div>
				<div class="content">
					<select class="full-width form-control" id="match_list" class="" name="m">
						<option value="">경기를 선택해주세요</option>
						<?php
							while($row = sql_fetch_array($match_result)){?>
								<option <?=$c == $row['code'] ? 'selected="selected"' : ""?> value="<?=$row['code']?>"><?=$row['wr_name']?></option>
							<?php }?>
					</select>
				</div>
			</section>
			<script>$('#match_list').change(function(){window.location.href='?c='+$(this).val();});</script>

			<?php
			
			if (count($date_data) > 1) {?>
				<section>
					<div class="pop_hd">
						<div class="tit">
							날짜 선택
						</div>
					</div>
					<div class="content">
						<div id="court_list" class="btn-group" role="group">
						<ul class="btn-list">
						<?php
							$sql = "select * from match_gym_data where match_id = '$c' group by application_period";
							$result = sql_query($sql);
							while( $value = sql_fetch_array($result)){
						?>
							<li><a <?=$value['application_period'] ==  $dt? 'class="active"':""?> href="?c=<?php echo $c;?>&dt=<?php echo $value['application_period']?>"><?php echo $value['application_period']?></a></li>
						<?php }?>
						</ul>
					</div>
					</div>
				</section>
				<?php }else{
					//print_r($date_date);
					$dt = array_keys($date_data);
					print_r($dt);
				}?>
	<!-- 탭-코트-->
	<section class="section2">
		<div class="pop_hd">
			<div class="tit">경기장 선택</div>
		</div>
		<div class="content ">
				<ul class="btn-list">
					<?php
						$sql = "select * from game_score_data where match_code = '$c' and game_date = '$dt' and tournament = 'T' group by series_sub ";
						$result = sql_query($sql);
						while( $value = sql_fetch_array($result)){
					?>
					<li><a <?=$value['series_sub'] ==  $g? 'class="active"':""?>
							href="?c=<?=$c?>&dt=<?=$dt?>&g=<?=$value['series_sub'];?>&ct=0">
							<?php echo $value['series_sub'];?></a></li>
					<?php
					}?>
				</ul>
				
			<div id="court_list" class="btn-group" role="group">
				<ul class="btn-list">
					<?php
					$sql = "select * from game_score_data where match_code = '$c' and series_sub = '$g' and tournament = 'T' group by tournament_count order by tournament_count desc";
					$result = sql_query($sql);
					while($value = sql_fetch_array($result)){
					?>
						<li><a <?=$ct == $value['tournament_count']? 'class="active"':""?>
							href="?c=<?=$c?>&dt=<?=$dt?>&g=<?=$g?>&ct=<?=$value['tournament_count']?>">
							<?=$value['tournament_count']*2?>강</a></li>
					<?php
					}?>
					<li><a <?=$ct ==  0? 'class="active"':""?>
							href="?c=<?=$c?>&dt=<?=$dt?>&g=<?=$g?>&ct=0">
							결승</a>
					</li>
				</ul>
	        </div>
		</div>
	</section>
	<!-- //탭-코트-->
	<?php
		if($dt != '' && $g != ''  && $ct != ''){

			$redirectUrl = urlencode (G5_AOS_URL."/competion_mnt_list1.php?c=$c&dt=$dt&g=$g&ct=$ct");

			// $matchProcessSql = "SELECT
			// game_score_data.wr_id as game_score_id,
				// group_data.code as group_code,
				// group_data.num, team_1_code,
				// (select concat(club,'-', team_1_name,'-',team_2_name) from team_data where team_code = team_1_code) as team_1,
				// (select concat(club,'-', team_1_name,'-',team_2_name) from team_data where team_code = team_2_code) as team_2,
				// team_1_score,
				// team_2_score,
				// end_game,
				// game_start_time,
				// game_end_time,
				// is_on,
				// game_score_data.court_array_num,
				// game_score_data.wr_id
		// FROM
				// game_score_data
						// JOIN
				// group_data ON game_score_data.group_code = group_data.code
		// WHERE
				// game_score_data.match_code = '{$c}'
				// and game_score_data.game_court = {$ct}
				// AND team_1_code <> ''
		// ORDER BY court_array_num desc, wr_id desc";
		
			$matchProcessSql = "select a.*
					from game_score_data as a
					where a.match_code='$c' and a.tournament != 'L' and end_game = 'N'  and a.game_date = '$dt' and a.tournament_count = '$ct' and a.series_sub = '$g' 
					order by a.court_array_num desc, a.wr_id desc";
			
			$matchProcess = sql_query($matchProcessSql);
			$matchIndex = 0;
			$match_group_code = '';
			$match_group_text = '';


			while($match_row = sql_fetch_array($matchProcess)){
				$matchIndex++;
				if($match_group_code == '' || $match_group_code != $match_row['group_code'] ){?>

					<?php if($match_group_code != '' && $match_group_code != $match_row['group_code']){?>
					</tbody></table></div></div></section>
					<?php }?>
					<section>
						<div class="content">
						<div class="con_tit_area clear">
							<div class="tit">경기결과</div>
						</div>
						
						<div class="tbl_style02 tbl_striped">
							<table>
								<thead>
									<tr>
										<th>경기</th>
										<th>경기장</th>
										<th>클럽</th>
										<th>이름</th>
										<th>점수</th>
										<th>이름</th>
										<th>클럽</th>
									</tr>
								</thead>
								<tbody>
							<?php
								$match_group_code = $match_row['group_code'];
							} ?>
							<?php
							
								$team_field = "team_data";
								
								$team_1 = $match_row['team_1_code'];
								if($match_row['division'] == "단체전")
									$team_1 = $match_row['team_1_event_code'];
								
								$team_2 = $match_row['team_1_code'];
								if($match_row['division'] == "단체전")
									$team_2 = $match_row['team_2_event_code'];
																
								$team1 = sql_fetch("select * from $team_field where match_code = '$c' and team_code='$team_1'");
								$team2 = sql_fetch("select * from $team_field where match_code = '$c' and team_code='$team_2'");
								
								$gym = sql_fetch("select * from gym_data where wr_id = '$match_row[gym_code]'");
							?>
							
								<tr>
									<td class="text-center"><?=$matchIndex?><br/>경기</td>
									<td class="text-center"><?php echo $gym['gym_name'];?></td>
									<td class="text-center">
										<?php 
										if($match_row['division'] == "단체전")
											if($match_row['team_1_event_code'] == ""){
										?>
											<a class="score_btn" href="popup_team_event_set.php?game_code=<?php echo $match_row['code'];?>&team=1&rd=<?=$redirectUrl?>">경기배정</a>
										<?
											}
										?>
										<?=$team1['club']?><br/><?=$team1['team_2_club']?>
									</td>
									<td class="text-center"><?=$team1['team_1_name']?><br/><?=$team1['team_2_name']?></td>
									<td class="match_point text-center">
										<div class="btn_group">
										<?php
											if($match_row['end_game'] == 'N'){
												if($match_row['is_on'] == 'N'){?>
													<div class="tournament_point">
														<a href="./start_game.php?c=<?=$c?>&dt=<?=$dt?>&g=<?=$g?>&ct=<?=$ct?>&gid=<?=$match_row['code']?>&rd=<?=$redirectUrl?>" class="btn ready">경기시작</a>
													</div>
												<?php }else{?>
													<div class="tournament_point">
									                	<a href="popup_insert_score.php?game_code=<?=$match_row['code']?>&rd=<?=$redirectUrl?>" target="_blank" class="btn mpoint">점수입력</a>
									                </div>
												<?php }
											}else{?>
									                <a href="popup_insert_score.php?game_code=<?=$match_row['code']?>&rd=<?=$redirectUrl?>" target="_blank"><span><?=$match_row['team_1_score']?></span>&nbsp;:&nbsp;<span><?=$match_row['team_2_score']?></span></a>
											<?php }?>
										</div>
									</td>
									<td class="text-center">
										<?=$team2['club']?><br/><?=$team2['team_2_club']?>
									</td>
									<td class="text-center">
										<?php 
										if($match_row['division'] == "단체전")
											if($match_row['team_2_event_code'] == ""){
										?>
											<a class="score_btn" href="popup_team_event_set.php?game_code=<?php echo $match_row['code'];?>&team=2&rd=<?=$redirectUrl?>">경기배정</a>
										<?
											}
										?>
										<?=$team2['team_1_name']?><br/><?=$team2['team_2_name']?>
									</td>
								</tr>
							<?php }?>
						<?php }?>
					<?php } ?>
</div>
<!-- end Contents Area -->
					</tbody></table></div></div></section>

<script>
$('#gym').change(function(event){
	window.location.href='?c=<?=$c?>&dt='+encodeURIComponent('<?=$dt?>')+'&g='+$(this).val();
});

</script>
<?php
include_once(G5_AOS_PATH.'/tail.php');
?>
