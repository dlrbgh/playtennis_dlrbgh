<?php
include_once('../common.php');
if (!defined('_GNUBOARD_')) exit; // 개별 페이지 접근 불가
include_once(G5_AOS_PATH.'/head.php');
?>

<!-- sub nav -->
<?php include_once(G5_AOS_PATH.'/inc/sub_cate.inc'); ?>
<!-- end sub nav -->
<?php



$match_code = $c;
$competition_sql = "SELECT * FROM match_data";
$competition = sql_fetch($competition_sql, true);?>



<div class="sub_container">
	<!-- bo_sch -->
	<div class="quick_sch">
		<div class="sch_area">
			<input name="title" value="" type="text">
			<button class=""><i class="flaticon-search"></i></button>

		</div>
	</div>
	<!-- bo_sch -->

	<!-- 현재 탭 이름  -->
	<section>
		<div class="hd">
			<div class="tit"><i class="flaticon-heart"></i>&nbsp;타이틀</div>
			<div class="r-btn-area">
				<ul>
					<li><a href="<?=G5_AOS_URL?>/competition_list.php" class="more_info">더보기</a></li>
				</ul>
			</div>
		</div>
		<!-- list board -->
		<div class="bo_list_ul">
			<ul>
				<li>
					<a href="<?=G5_AOS_URL?>/competition_view.php" class="list_a">
						<div class="champ_status">
							<span class="champ_ico champ_ico1"></span>
							<span class="limit_status">접수중</span>
						</div>
						<span class="list_r_area">
							<i class="flaticon-right-arrow"></i>
						</span>
						<div class="subject">
							<div class="m_subject">플레이테니스배 강원도 춘천시 테니스 예선 대회 입니다 </div>
							<div class="s_subject">2017.04.14 ~ 04.16&nbsp;&nbsp;강원도,춘천 시</div>
						</div>
					</a>
				</li>
				<li>
					<a href="<?=G5_AOS_URL?>/competition_view.php" class="list_a">
						<div class="champ_status">
							<span class="champ_ico champ_ico2"></span>
							<span class="limit_status">접수중</span>
						</div>
						<span class="list_r_area">
							<i class="flaticon-right-arrow"></i>
						</span>
						<div class="subject">
							<div class="m_subject">플레이테니스배 강원도 춘천시 테니스 예선 대회 입니다 </div>
							<div class="s_subject">2017.04.14 ~ 04.16&nbsp;&nbsp;강원도,춘천 시</div>
						</div>
					</a>
				</li>
				<li>
					<a href="<?=G5_AOS_URL?>/competition_view.php" class="list_a">
						<div class="champ_status">
							<span class="champ_ico champ_ico3"></span>
							<span class="limit_status">접수중</span>
						</div>
						<span class="list_r_area">
							<i class="flaticon-right-arrow"></i>
						</span>
						<div class="subject">
							<div class="m_subject">플레이테니스배 강원도 춘천시 테니스 예선 대회 입니다 </div>
							<div class="s_subject">2017.04.14 ~ 04.16&nbsp;&nbsp;강원도,춘천 시</div>
						</div>
					</a>
				</li>
				<li>
					<a href="<?=G5_AOS_URL?>/competition_view.php" class="list_a">
						<div class="champ_status">
							<span class="champ_ico champ_ico4"></span>
							<span class="limit_status">접수중</span>
						</div>
						<span class="list_r_area">
							<i class="flaticon-right-arrow"></i>
						</span>
						<div class="subject">
							<div class="m_subject">플레이테니스배 강원도 춘천시 테니스 예선 대회 입니다 </div>
							<div class="s_subject">2017.04.14 ~ 04.16&nbsp;&nbsp;강원도,춘천 시</div>
						</div>
					</a>
				</li>
				<li>
					<a href="<?=G5_AOS_URL?>/competition_view.php" class="list_a">
						<div class="champ_status">
							<span class="champ_ico champ_ico4"></span>
							<span class="limit_status">접수중</span>
						</div>
						<span class="list_r_area">
							<i class="flaticon-right-arrow"></i>
						</span>
						<div class="subject">
							<div class="m_subject">플레이테니스배 강원도 춘천시 테니스 예선 대회 입니다 </div>
							<div class="s_subject">2017.04.14 ~ 04.16&nbsp;&nbsp;강원도,춘천 시</div>
						</div>
					</a>
				</li>
				<li>
					<a href="<?=G5_AOS_URL?>/competition_view.php" class="list_a">
						<div class="champ_status">
							<span class="champ_ico champ_ico4"></span>
							<span class="limit_status">접수중</span>
						</div>
						<span class="list_r_area">
							<i class="flaticon-right-arrow"></i>
						</span>
						<div class="subject">
							<div class="m_subject">플레이테니스배 강원도 춘천시 테니스 예선 대회 입니다 </div>
							<div class="s_subject">2017.04.14 ~ 04.16&nbsp;&nbsp;강원도,춘천 시</div>
						</div>
					</a>
				</li>
				<li>
					<a href="<?=G5_AOS_URL?>/competition_view.php" class="list_a">
						<div class="champ_status">
							<span class="champ_ico champ_ico4"></span>
							<span class="limit_status">접수중</span>
						</div>
						<span class="list_r_area">
							<i class="flaticon-right-arrow"></i>
						</span>
						<div class="subject">
							<div class="m_subject">플레이테니스배 강원도 춘천시 테니스 예선 대회 입니다 </div>
							<div class="s_subject">2017.04.14 ~ 04.16&nbsp;&nbsp;강원도,춘천 시</div>
						</div>
					</a>
				</li>
			</ul>
			<div class="quick_view">
				<a href="">
					더보기
				</a>
			</div>

		</div>
		<!-- //list board -->
	</section>
	<!-- //현재 탭 이름  -->

	<!-- calander section--
	<section>
		<div>
			캘린더 출력
		</div>
	</section>
	<!-- //calander section-->
</div>
<!-- champ_list -->


<?php
include_once(G5_AOS_PATH.'/tail.php');
?>
