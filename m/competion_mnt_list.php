<?php

include_once('../common.php');
if (!defined('_GNUBOARD_')) exit; // 개별 페이지 접근 불가
include_once(G5_AOS_PATH.'/head.php');
$menu_cate2 ='management';
$menu_cate3 ='2';

?>
<?php

$big_division = sql_fetch("select division from series_data where match_code = '{$c}' ");


if($big_division['division'] == '단체전'){?>

	<!-- sub nav -->
	<?php include_once('./app_sub_nav.php'); ?>
	<!-- end sub nav -->
	<?php
	$mb_id = get_session('ss_mb_id');
		if($is_admin){
			$match_sql = "select * from match_data where 1 order by app_visible desc , date1 desc";
		}else{
			// $match_sql = "select * from match_data where code in
			// 						(SELECT match_id FROM match_gym_data
			// 						where  (charger1 = '{$mb_id}' or charger2 = '{$mb_id}'
			// 						or charger3 = '{$mb_id}' or charger4 = '{$mb_id}'
			// 						or charger5 = '{$mb_id}') group by match_id)
			// 					and app_visible = 2
			// 					order by date1 desc";
			$match_sql = "select * from match_data where code in
									(SELECT match_id FROM match_gym_data
									where  (charger1 = '{$mb_id}' or charger2 = '{$mb_id}'
									or charger3 = '{$mb_id}' or charger4 = '{$mb_id}'
									or charger5 = '{$mb_id}') group by match_id)
								order by date1 desc";
		}
		$match_result = sql_query($match_sql);
	?>

	<!-- Contents Area -->
	<div class="pop_container">
		<?php if(false && ! $competition['opening_date']){?>
			<div class="empty_waiting">
				<div class="img_area">
					<img src="<?php echo G5_IMG_URL.'/common/intro_logo1.png';?>">
				</div>
				<div class="cmt ani02">
					경기가 준비중입니다.
				</div>
			</div>
		<?php }else {?>
			<?php if($is_admin){?>
				<section>

					<div class="pop_hd sound_only">
						대회선택
					</div>
					<div class="content">
						<select class="full-width form-control" id="match_list" class="" name="m">
							<option value="">경기를 선택해주세요</option>
							<?php
								while($row = sql_fetch_array($match_result)){
									if($c == '') {$c = $row['code'];}
									?>
									<option <?=$c == $row['code'] ? 'selected="selected"' : ""?>
										value="<?=$row['code']?>">
										<?php
											if($is_admin){
												if($row['app_visible'] == 2){
													print "(공개)";
												}else{
													print "(비공개)";
												}
											}
										 ?>
										<?=$row['wr_name']?>

									</option>
								<?php }?>
						</select>


						<script>$('#match_list').change(function(){window.location.href='?c='+$(this).val();});</script>

						<div class="pop_hd sound_only">
							<div class="tit">
								날짜 선택
							</div>
						</div>

						<div id="court_list" class="btn-group" role="group">
							<ul class="btn-list">
							<?php
								$sql = "select * from match_gym_data where match_id = '$c' group by application_period";
								$result = sql_query($sql);
								while( $value = sql_fetch_array($result)){
									 $dt == '' ? $dt = $value['application_period'] : $dt;
							?>
								<li><a <?=$value['application_period'] ==  $dt? 'class="active"':""?> href="competion_mnt_list.php?c=<?php echo $c;?>&dt=<?php echo $value['application_period']?>"><?php echo $value['application_period']?></a></li>
							<?php }?>
							</ul>
						</div>
						<!-- 탭-코트-->

						<div class="pop_hd sound_only">
							<div class="tit">경기장 선택</div>
						</div>


						<select class="full-width form-control" id="gym">

							<option value="">경기장 선택</option>

							<?php
								$sql = "select *,b.gym_name from match_gym_data as a
								inner join gym_data as b
								where 1
								and match_id = '{$c}'
								and a.gym_id = b.wr_id ";
								if(!$is_admin){
									$sql .=" and (a.charger1 ='$mb_id' or a.charger2 ='$mb_id'
									or a.charger3 ='$mb_id' or a.charger4 ='$mb_id'
									or a.charger5 ='$mb_id') ";
								}
								$sql .=" order by application_period desc ";
								$result = sql_query($sql);
								while( $value = sql_fetch_array($result)){
									if($c == ''){$c = $value['match_id'];}
									if($g == ''){$g = $value['gym_id'];}
									// print_r($value);
							?>
									<option value="<?php echo $value['wr_id']?>"
										<?php echo  $g == '' || $g == $value['wr_id'] ? "selected=\"selected\"" :""?>>
										<?php echo $value['application_period']?> <?php echo $value['gym_name'];?></option>
							<?php


							}?>

						</select>
						<div id="court_list" class="btn-group" role="group">
							<ul class="btn-list">
								<?php
								$sql = "select * from match_gym_data where
								match_id = '$c' and gym_id = '$g'";
								$result = sql_query($sql);
								$value = sql_fetch_array($result);
								for($i = 0 ; $i < $value['use_court'] ; $i++){
									if($ct=='') $ct = $i+1;
								?>
									<li>
										<a
										<?=$ct ==  $i+1? 'class="active"':""
										?>
										href="?c=<?=$c?>&g=<?=$g?>&ct=<?=$i+1?>">
										<?=$i+1?>코트</a></li>
								<?php

								}?>
							</ul>
								</div>
						<!-- //탭-코트-->
					</div>
				</section>




			<?php }else{?>

				<section>

					<div class="pop_hd sound_only">
						대회선택
					</div>
					<div class="content">
						<!-- <select class="full-width form-control" id="match_list" class="" name="m">
							<option value="">경기를 선택해주세요</option>
							<?php
								while($row = sql_fetch_array($match_result)){
									if($c == '') {$c = $row['code'];}
									?>
									<option <?=$c == $row['code'] ? 'selected="selected"' : ""?>
										value="<?=$row['code']?>">
										<?php
											if($is_admin){
												if($row['app_visible'] == 2){
													print "(공개)&nbsp;&nbsp;&nbsp;";
												}else{
													print "(비공개)";
												}
											}
										 ?>
										<?=$row['wr_name']?>

									</option>
								<?php }?>
						</select> -->


						<script>$('#match_list').change(function(){window.location.href='?c='+$(this).val();});</script>

						<div class="pop_hd sound_only">
							<div class="tit">
								날짜 선택
							</div>
						</div>

						<!-- <div id="court_list" class="btn-group" role="group">
							<ul class="btn-list">
							<?php
								$sql = "select * from match_gym_data where match_id = '$c' group by application_period";
								$result = sql_query($sql);
								while( $value = sql_fetch_array($result)){
									 $dt == '' ? $dt = $value['application_period'] : $dt;
							?>
								<li><a <?=$value['application_period'] ==  $dt? 'class="active"':""?> href="competion_mnt_list.php?c=<?php echo $c;?>&dt=<?php echo $value['application_period']?>"><?php echo $value['application_period']?></a></li>
							<?php }?>
							</ul>
						</div> -->
						<!-- 탭-코트-->

						<div class="pop_hd sound_only">
							<div class="tit">경기장 선택</div>
						</div>


						<select class="full-width form-control" id="gym">

							<option value="">경기장 선택</option>

							<?php
								$sql = "select *,b.gym_name from match_gym_data as a
								inner join gym_data as b
								where 1
								and match_id = '{$c}'
								and a.gym_id = b.wr_id ";
								if(!$is_admin){
									$sql .=" and (a.charger1 ='$mb_id' or a.charger2 ='$mb_id'
									or a.charger3 ='$mb_id' or a.charger4 ='$mb_id'
									or a.charger5 ='$mb_id') ";
								}
								$sql .=" order by application_period desc ";
								$result = sql_query($sql);
								while( $value = sql_fetch_array($result)){
									if($c == ''){$c = $value['match_id'];}
									if($g == ''){$g = $value['gym_id'];}
									// print_r($value);
							?>
									<option value="<?php echo $value['wr_id']?>"
										<?php echo  $g == '' || $g == $value['wr_id'] ? "selected=\"selected\"" :""?>>
										<?php echo $value['application_period']?> <?php echo $value['gym_name'];?></option>
							<?php


							}?>

						</select>
						<div id="court_list" class="btn-group" role="group">
							<ul class="btn-list">
								<?php
								$sql = "select * from match_gym_data where
								match_id = '$c' and gym_id = '$g'";
								$result = sql_query($sql);
								$value = sql_fetch_array($result);
								for($i = 0 ; $i < $value['use_court'] ; $i++){
									if($ct=='') $ct = $i+1;
								?>
									<li>
										<a
										<?=$ct ==  $i+1? 'class="active"':""
										?>
										href="?c=<?=$c?>&g=<?=$g?>&ct=<?=$i+1?>">
										<?=$i+1?>코트</a></li>
								<?php

								}?>
							</ul>
								</div>
						<!-- //탭-코트-->
					</div>
				</section>

			<?php } ?>


		<?php
			if( $g != ''  && $ct != ''){

				$redirectUrl = urlencode (G5_AOS_URL."/competion_mnt_list.php?c=$c&dt=$dt&g=$g&ct=$ct");

				// $matchProcessSql = "SELECT
				// game_score_data.wr_id as game_score_id,
					// group_data.code as group_code,
					// group_data.num, team_1_code,
					// (select concat(club,'-', team_1_name,'-',team_2_name) from team_data where team_code = team_1_code) as team_1,
					// (select concat(club,'-', team_1_name,'-',team_2_name) from team_data where team_code = team_2_code) as team_2,
					// team_1_score,
					// team_2_score,
					// end_game,
					// game_start_time,
					// game_end_time,
					// is_on,
					// game_score_data.court_array_num,
					// game_score_data.wr_id
			// FROM
					// game_score_data
							// JOIN
					// group_data ON game_score_data.group_code = group_data.code
			// WHERE
					// game_score_data.match_code = '{$c}'
					// and game_score_data.game_court = {$ct}
					// AND team_1_code <> ''
			// ORDER BY court_array_num desc, wr_id desc";

				$matchProcessSql = "select a.*,b.num
						from game_score_data as a inner join group_data as b
						where a.match_code='$c' and a.tournament = 'L' and a.game_court = '$ct' and a.gym_code = '$g' and a.group_code = b.code
						order by a.court_array_num , a.wr_id desc";
					// print $matchProcessSql;
				$matchProcess = sql_query($matchProcessSql);
				$matchIndex = 0;
				$match_group_code = '';
				$match_group_text = '';


				while($match_row = sql_fetch_array($matchProcess)){

					$matchIndex++;

					if($match_group_code == '' || $match_group_code != $match_row['group_code'] ){?>

						<?php if($match_group_code != '' && $match_group_code != $match_row['group_code']){?>
						</tbody></table></div></div></section>
						<?php }?>
						<section>
							<div class="content">
							<div class="con_tit_area clear">
								<div class="tit"> <?=$match_row['division']?> <?=$match_row['series']?> <?=$match_row['series_sub']?> <?=$match_row['num']+1?>조</div>
							</div>
							<div class="tbl_style02 tbl_striped">
								<table>
									<thead>
										<tr>
											<th>경기</th>
											<th>클럽</th>
											<th>이름</th>
											<th>점수</th>
											<th>이름</th>
											<th>클럽</th>
										</tr>
									</thead>
									<tbody>
								<?php
									$match_group_code = $match_row['group_code'];
								} ?>
								<?php

									$team_field = "team_data";

									$team_1 = $match_row['team_1_code'];
									$team_left = $match_row['team_1_code'];
									if($match_row['division'] == "단체전")
										$team_1 = $match_row['team_1_event_code'];

									$team_2 = $match_row['team_2_code'];
									$team_right = $match_row['team_2_code'];
									if($match_row['division'] == "단체전"){
										$team_2 = $match_row['team_2_event_code'];
										}
									// print "<br />".$match_row['team_2_code']." ".$match_row['team_2_event_code']."<br />";

									$team1 = sql_fetch("select * from $team_field where match_code = '$c' and team_code='$team_1'");
									$team2 = sql_fetch("select * from $team_field where match_code = '$c' and team_code='$team_2'");
								?>
									<tr>
										<td class="text-center"><?=$matchIndex?><br/>경기</td>
										<td class="text-center">
											<?php
											if($match_row['division'] == "단체전")
												if($match_row['team_1_event_code'] == ""){
												$team_left_name = sql_fetch("select * from team_event_data where team_code='$team_left'");
											?>
												<a class="score_btn" href="popup_team_event_set.php?game_code=<?php echo $match_row['code'];?>&team=1&rd=<?=$redirectUrl?>" target="_blank"><?=$team_left_name['club']; ?></a>
											<?php
												}
											?>
											<?=$team1['club']?><br/><?=$team1['team_2_club']?>
										</td>
										<td class="text-center">
											<?=$team1['team_1_name']."(".$team1['team_1_grade'].")"?><br/>
											<?=$team1['team_2_name']."(".$team1['team_2_grade'].")"?></td>
										<td class="match_point text-center">
											<div class="btn_group">
											<?php
												if($match_row['end_game'] == 'N'){
													if($match_row['is_on'] == 'N'){?>
														<div class="tournament_point">
															<a class="btn ready start_game" data-c="<?=$c?>" data-dt="<?=$dt?>"
																data-g="<?=$g?>" data-ct="<?=$ct?>" data-gid="<?=$match_row['code']?>"
																data-gamecode="<?=$match_row['code']?>">경기시작</a>
															<!-- <a href="./start_game.php?c=<?=$c?>&dt=<?=$dt?>&g=<?=$g?>&ct=<?=$ct?>&gid=<?=$match_row['code']?>&rd=<?=$redirectUrl?>" class="btn ready">경기시작</a> -->
														</div>
													<?php }else{?>
														<div class="tournament_point">
										                	<a href="popup_insert_score.php?game_code=<?=$match_row['code']?>&rd=<?=$redirectUrl?>" target="_blank" class="btn mpoint">점수입력</a>
										                </div>
													<?php }
												}else{?>

			               								 <a href="popup_insert_score.php?game_code=<?=$match_row['code']?>&rd=<?=$redirectUrl?>" target="_blank">
				                						<?php
															if($match_row['team_1_dis']=="1"){?>
															<div class="point_box">
																<div class="skip"><span>기권</span><span>&nbsp;&nbsp;&nbsp;&nbsp;<?=$match_row['team_2_score']?>&nbsp;&nbsp;&nbsp;&nbsp;</span></div>
															</div>
														<?php }else if($match_row['team_1_dis']=="2"){?>
															<div class="point_box">
																<div class="skip"><span>실격</span><span style="width:50px">&nbsp;&nbsp;&nbsp;&nbsp;<?=$match_row['team_2_score']?>&nbsp;&nbsp;&nbsp;&nbsp;</span></div>
															</div>
														<?php }else if($match_row['team_1_dis']=="3"){?>
			                									<div class="skip"><span>경기안함</span></div>
			                							<?php }else if($match_row['team_2_dis']=="1"){?>
															<div class="point_box">
																<div class="skip"><span>&nbsp;&nbsp;&nbsp;&nbsp;<?=$match_row['team_1_score']?>&nbsp;&nbsp;&nbsp;&nbsp;</span><span>기권</span></div>
															</div>
													 	<?php }else if($match_row['team_2_dis']=="2"){?>
															<div class="point_box">
																<div class="skip"><span>&nbsp;&nbsp;&nbsp;&nbsp;<?=$match_row['team_1_score']?>&nbsp;&nbsp;&nbsp;&nbsp;</span><span>실격</span></div>
															</div>
														<?php }else if($match_row['team_2_dis']=="3"){?>
															<span>경기안함</span>
		                								<?php }else{ ?>
										                	<div class="point_box">
										                		<span><?=$match_row['team_1_score']?></span>&nbsp;&nbsp;<span><?=$match_row['team_2_score']?></span>
										                	</div>
								                		</a>
														<?php } ?>

												<?php }?>

											</div>
										</td>
										<td class="text-center">
											<?=$team2['team_1_name']."(".$team2['team_1_grade'].")"?><br/>
											<?=$team2['team_2_name']."(".$team2['team_1_grade'].")"?>
										</td>
										<td class="text-center">
											<?php
											if($match_row['division'] == "단체전")
												$team_right_name = sql_fetch("select * from team_event_data where team_code='$team_right'");
											?>
												<a class="score_btn" href="popup_team_event_set.php?game_code=<?php echo $match_row['code'];?>&team=2&rd=<?=$redirectUrl?>" target="_blank"><?=$team_right_name['club']; ?></a>
											<?php
	//170602sh											}
											?>
											<?//=$team2['club']?><br/><?=$team2['team_2_club']?>

										</td>
									</tr>
								<?php }?>
							<?php }?>
						<?php } ?>

	<!-- end Contents Area -->
						</tbody></table></div></div></section>

	<script>
	$('#gym').change(function(event){
		window.location.href='?c=<?=$c?>&dt='+encodeURIComponent('<?=$dt?>')+'&g='+$(this).val();
	});
	$('.start_game').click(function(event){
		event.preventDefault();
		var dataset = $(event.currentTarget).data();
		$.ajax({
			url:'./start_game.php'
			,data:dataset
			,dataType:'json'
			,success:function(data){
				console.log(data);
				if(data.state == 'ok'){
					window.location.reload();
					// gamecode = dataset.gamecode;
					// var html = '<a href="popup_insert_score.php?game_code='+(dataset.gamecode)+'&rd='+dataset.rd+'" target="_blank" class="btn mpoint">점수입력</a>'
					// console.log(html);
					// $(event.currentTarget).replaceWith(html);
				}
			}
		})
	})
	</script>
	<?php
	include_once(G5_AOS_PATH.'/tail.php');
	?>


	<?php }else{?>
		<!-- sub nav -->

		<?php include_once('./app_sub_nav.php'); ?>
		<!-- end sub nav -->
		<?php
		$mb_id = get_session('ss_mb_id');
			if($is_admin){
				$match_sql = "select * from match_data where 1 order by app_visible desc , date1 desc";
			}else{
				// $match_sql = "select * from match_data where code in
				// 						(SELECT match_id FROM match_gym_data
				// 						where  (charger1 = '{$mb_id}' or charger2 = '{$mb_id}'
				// 						or charger3 = '{$mb_id}' or charger4 = '{$mb_id}'
				// 						or charger5 = '{$mb_id}') group by match_id)
				// 					and app_visible = 2
				// 					order by date1 desc";
				$match_sql = "select * from match_data where code in
										(SELECT match_id FROM match_gym_data
										where  (charger1 = '{$mb_id}' or charger2 = '{$mb_id}'
										or charger3 = '{$mb_id}' or charger4 = '{$mb_id}'
										or charger5 = '{$mb_id}') group by match_id)
									order by date1 desc";
			}
			$match_result = sql_query($match_sql);
		?>

		<!-- Contents Area -->
		<div class="pop_container">
			<?php if(false && ! $competition['opening_date']){?>
				<div class="empty_waiting">
					<div class="img_area">
						<img src="<?php echo G5_IMG_URL.'/common/intro_logo1.png';?>">
					</div>
					<div class="cmt ani02">
						경기가 준비중입니다.
					</div>
				</div>
			<?php }else {?>
				<?php if($is_admin){?>
					<section>

						<div class="pop_hd sound_only">
							대회선택
						</div>
						<div class="content">
							<select class="full-width form-control" id="match_list" class="" name="m">
								<option value="">경기를 선택해주세요</option>
								<?php
									while($row = sql_fetch_array($match_result)){
										if($c == '') {$c = $row['code'];}
										?>
										<option <?=$c == $row['code'] ? 'selected="selected"' : ""?>
											value="<?=$row['code']?>">
											<?php
												if($is_admin){
													if($row['app_visible'] == 2){
														print "(공개)";
													}else{
														print "(비공개)";
													}
												}
											 ?>
											<?=$row['wr_name']?>

										</option>
									<?php }?>
							</select>


							<script>$('#match_list').change(function(){window.location.href='?c='+$(this).val();});</script>

							<div class="pop_hd sound_only">
								<div class="tit">
									날짜 선택
								</div>
							</div>

							<div id="court_list" class="btn-group" role="group">
								<ul class="btn-list">
								<?php
									$sql = "select * from match_gym_data where match_id = '$c' group by application_period";
									$result = sql_query($sql);
									while( $value = sql_fetch_array($result)){
										 $dt == '' ? $dt = $value['application_period'] : $dt;
								?>
									<li><a <?=$value['application_period'] ==  $dt? 'class="active"':""?> href="competion_mnt_list.php?c=<?php echo $c;?>&dt=<?php echo $value['application_period']?>"><?php echo $value['application_period']?></a></li>
								<?php }?>
								</ul>
							</div>
							<!-- 탭-코트-->

							<div class="pop_hd sound_only">
								<div class="tit">경기장 선택</div>
							</div>


							<select class="full-width form-control" id="gym">

								<option value="">경기장 선택</option>

								<?php
									$sql = "select *,b.gym_name from match_gym_data as a
									inner join gym_data as b
									where 1
									and match_id = '{$c}'
									and a.gym_id = b.wr_id ";
									if(!$is_admin){
										$sql .=" and (a.charger1 ='$mb_id' or a.charger2 ='$mb_id'
										or a.charger3 ='$mb_id' or a.charger4 ='$mb_id'
										or a.charger5 ='$mb_id') ";
									}
									$sql .=" order by application_period desc ";
									$result = sql_query($sql);
									while( $value = sql_fetch_array($result)){
										if($c == ''){$c = $value['match_id'];}
										if($g == ''){$g = $value['gym_id'];}
										// print_r($value);
								?>
										<option value="<?php echo $value['wr_id']?>"
											<?php echo  $g == '' || $g == $value['wr_id'] ? "selected=\"selected\"" :""?>>
											<?php echo $value['application_period']?> <?php echo $value['gym_name'];?></option>
								<?php


								}?>

							</select>
							<div id="court_list" class="btn-group" role="group">
								<ul class="btn-list">
									<?php
									$sql = "select * from match_gym_data where
									match_id = '$c' and gym_id = '$g'";
									$result = sql_query($sql);
									$value = sql_fetch_array($result);
									for($i = 0 ; $i < $value['use_court'] ; $i++){
										if($ct=='') $ct = $i+1;
									?>
										<li>
											<a
											<?=$ct ==  $i+1? 'class="active"':""
											?>
											href="?c=<?=$c?>&g=<?=$g?>&ct=<?=$i+1?>">
											<?=$i+1?>코트</a></li>
									<?php

									}?>
								</ul>
									</div>
							<!-- //탭-코트-->
						</div>
					</section>




				<?php }else{?>

					<section>

						<div class="pop_hd sound_only">
							대회선택
						</div>
						<div class="content">
							<!-- <select class="full-width form-control" id="match_list" class="" name="m">
								<option value="">경기를 선택해주세요</option>
								<?php
									while($row = sql_fetch_array($match_result)){
										if($c == '') {$c = $row['code'];}
										?>
										<option <?=$c == $row['code'] ? 'selected="selected"' : ""?>
											value="<?=$row['code']?>">
											<?php
												if($is_admin){
													if($row['app_visible'] == 2){
														print "(공개)&nbsp;&nbsp;&nbsp;";
													}else{
														print "(비공개)";
													}
												}
											 ?>
											<?=$row['wr_name']?>

										</option>
									<?php }?>
							</select> -->


							<script>$('#match_list').change(function(){window.location.href='?c='+$(this).val();});</script>

							<div class="pop_hd sound_only">
								<div class="tit">
									날짜 선택
								</div>
							</div>

							<!-- <div id="court_list" class="btn-group" role="group">
								<ul class="btn-list">
								<?php
									$sql = "select * from match_gym_data where match_id = '$c' group by application_period";
									$result = sql_query($sql);
									while( $value = sql_fetch_array($result)){
										 $dt == '' ? $dt = $value['application_period'] : $dt;
								?>
									<li><a <?=$value['application_period'] ==  $dt? 'class="active"':""?> href="competion_mnt_list.php?c=<?php echo $c;?>&dt=<?php echo $value['application_period']?>"><?php echo $value['application_period']?></a></li>
								<?php }?>
								</ul>
							</div> -->
							<!-- 탭-코트-->

							<div class="pop_hd sound_only">
								<div class="tit">경기장 선택</div>
							</div>


							<select class="full-width form-control" id="gym">

								<option value="">경기장 선택</option>

								<?php
									$sql = "select *,b.gym_name from match_gym_data as a
									inner join gym_data as b
									where 1
									and match_id = '{$c}'
									and a.gym_id = b.wr_id ";
									if(!$is_admin){
										$sql .=" and (a.charger1 ='$mb_id' or a.charger2 ='$mb_id'
										or a.charger3 ='$mb_id' or a.charger4 ='$mb_id'
										or a.charger5 ='$mb_id') ";
									}
									$sql .=" order by application_period desc ";
									$result = sql_query($sql);
									while( $value = sql_fetch_array($result)){
										if($c == ''){$c = $value['match_id'];}
										if($g == ''){$g = $value['gym_id'];}
										// print_r($value);
								?>
										<option value="<?php echo $value['wr_id']?>"
											<?php echo  $g == '' || $g == $value['wr_id'] ? "selected=\"selected\"" :""?>>
											<?php echo $value['application_period']?> <?php echo $value['gym_name'];?></option>
								<?php


								}?>

							</select>
							<div id="court_list" class="btn-group" role="group">
								<ul class="btn-list">
									<?php
									$sql = "select * from match_gym_data where
									match_id = '$c' and gym_id = '$g'";
									$result = sql_query($sql);
									$value = sql_fetch_array($result);
									for($i = 0 ; $i < $value['use_court'] ; $i++){
										if($ct=='') $ct = $i+1;
									?>
										<li>
											<a
											<?=$ct ==  $i+1? 'class="active"':""
											?>
											href="?c=<?=$c?>&g=<?=$g?>&ct=<?=$i+1?>">
											<?=$i+1?>코트</a></li>
									<?php

									}?>
								</ul>
									</div>
							<!-- //탭-코트-->
						</div>
					</section>

				<?php } ?>


			<?php
				if( $g != ''  && $ct != ''){

					$redirectUrl = urlencode (G5_AOS_URL."/competion_mnt_list.php?c=$c&dt=$dt&g=$g&ct=$ct");

					// $matchProcessSql = "SELECT
					// game_score_data.wr_id as game_score_id,
						// group_data.code as group_code,
						// group_data.num, team_1_code,
						// (select concat(club,'-', team_1_name,'-',team_2_name) from team_data where team_code = team_1_code) as team_1,
						// (select concat(club,'-', team_1_name,'-',team_2_name) from team_data where team_code = team_2_code) as team_2,
						// team_1_score,
						// team_2_score,
						// end_game,
						// game_start_time,
						// game_end_time,
						// is_on,
						// game_score_data.court_array_num,
						// game_score_data.wr_id
				// FROM
						// game_score_data
								// JOIN
						// group_data ON game_score_data.group_code = group_data.code
				// WHERE
						// game_score_data.match_code = '{$c}'
						// and game_score_data.game_court = {$ct}
						// AND team_1_code <> ''
				// ORDER BY court_array_num desc, wr_id desc";

					$matchProcessSql = "select a.*,b.num
							from game_score_data as a inner join group_data as b
							where a.match_code='$c' and a.tournament = 'L' and a.game_court = '$ct' and a.gym_code = '$g' and a.group_code = b.code
							order by a.court_array_num, a.wr_id desc";
						// print $matchProcessSql;
					$matchProcess = sql_query($matchProcessSql);
					$matchIndex = 0;
					$match_group_code = '';
					$match_group_text = '';


					while($match_row = sql_fetch_array($matchProcess)){

						$matchIndex++;

						if($match_group_code == '' || $match_group_code != $match_row['group_code'] ){?>

							<?php if($match_group_code != '' && $match_group_code != $match_row['group_code']){?>
							</tbody></table></div></div></section>
							<?php }?>
							<section>
								<div class="content">
								<div class="con_tit_area clear">
									<div class="tit"> <?=$match_row['division']?> <?=$match_row['series']?> <?=$match_row['series_sub']?> <?=$match_row['num']+1?>조</div>
								</div>
								<div class="tbl_style02 tbl_striped">
									<table>
										<thead>
											<tr>
												<th>경기</th>
												<th>클럽</th>
												<th>이름</th>
												<th>점수</th>
												<th>이름</th>
												<th>클럽</th>
											</tr>
										</thead>
										<tbody>
									<?php
										$match_group_code = $match_row['group_code'];
									} ?>
									<?php

										$team_field = "team_data";

										$team_1 = $match_row['team_1_code'];
										$team_left = $match_row['team_1_code'];
										if($match_row['division'] == "단체전")
											$team_1 = $match_row['team_1_event_code'];

										$team_2 = $match_row['team_2_code'];
										$team_right = $match_row['team_2_code'];
										if($match_row['division'] == "단체전"){
											$team_2 = $match_row['team_2_event_code'];
											}
										// print "<br />".$match_row['team_2_code']." ".$match_row['team_2_event_code']."<br />";

										$team1 = sql_fetch("select * from $team_field where match_code = '$c' and team_code='$team_1'");
										$team2 = sql_fetch("select * from $team_field where match_code = '$c' and team_code='$team_2'");
									?>
										<tr>
											<td class="text-center"><?=$matchIndex?><br/>경기</td>
											<td class="text-center">
												<?php
												if($match_row['division'] == "단체전")
													if($match_row['team_1_event_code'] == ""){
													$team_left_name = sql_fetch("select * from team_event_data where team_code='$team_left'");
												?>
													<a class="score_btn" href="popup_team_event_set.php?game_code=<?php echo $match_row['code'];?>&team=1&rd=<?=$redirectUrl?>" target="_blank"><?=$team_left_name['club']; ?></a>
												<?php
													}
												?>
												<?=$team1['club']?><br/><?=$team1['team_2_club']?>
											</td>
											<td class="text-center">
												<?=$team1['team_1_name']."(".$team1['team_1_grade'].")"?><br/>
												<?=$team1['team_2_name']."(".$team1['team_2_grade'].")"?></td>
											<td class="match_point text-center">
												<div class="btn_group">
												<?php
													if($match_row['end_game'] == 'N'){
														if($match_row['is_on'] == 'N'){?>
															<div class="tournament_point">
																<a class="btn ready start_game" data-c="<?=$c?>" data-dt="<?=$dt?>"
																	data-g="<?=$g?>" data-ct="<?=$ct?>" data-gid="<?=$match_row['code']?>"
																	data-gamecode="<?=$match_row['code']?>">경기시작</a>
																<!-- <a href="./start_game.php?c=<?=$c?>&dt=<?=$dt?>&g=<?=$g?>&ct=<?=$ct?>&gid=<?=$match_row['code']?>&rd=<?=$redirectUrl?>" class="btn ready">경기시작</a> -->
															</div>
														<?php }else{?>
															<div class="tournament_point">
											                	<a href="popup_insert_score.php?game_code=<?=$match_row['code']?>&rd=<?=$redirectUrl?>" target="_blank" class="btn mpoint">점수입력</a>
											                </div>
														<?php }
													}else{?>

				               								 <a href="popup_insert_score.php?game_code=<?=$match_row['code']?>&rd=<?=$redirectUrl?>" target="_blank">
					                						<?php
																if($match_row['team_1_dis']=="1"){?>
																<div class="point_box">
																	<div class="skip"><span>기권</span><span>&nbsp;&nbsp;&nbsp;&nbsp;<?=$match_row['team_2_score']?>&nbsp;&nbsp;&nbsp;&nbsp;</span></div>
																</div>
															<?php }else if($match_row['team_1_dis']=="2"){?>
																<div class="point_box">
																	<div class="skip"><span>실격</span><span style="width:50px">&nbsp;&nbsp;&nbsp;&nbsp;<?=$match_row['team_2_score']?>&nbsp;&nbsp;&nbsp;&nbsp;</span></div>
																</div>
															<?php }else if($match_row['team_1_dis']=="3"){?>
				                									<div class="skip"><span>경기안함</span></div>
				                							<?php }else if($match_row['team_2_dis']=="1"){?>
																<div class="point_box">
																	<div class="skip"><span>&nbsp;&nbsp;&nbsp;&nbsp;<?=$match_row['team_1_score']?>&nbsp;&nbsp;&nbsp;&nbsp;</span><span>기권</span></div>
																</div>
														 	<?php }else if($match_row['team_2_dis']=="2"){?>
																<div class="point_box">
																	<div class="skip"><span>&nbsp;&nbsp;&nbsp;&nbsp;<?=$match_row['team_1_score']?>&nbsp;&nbsp;&nbsp;&nbsp;</span><span>실격</span></div>
																</div>
															<?php }else if($match_row['team_2_dis']=="3"){?>
																<span>경기안함</span>
			                								<?php }else{ ?>
											                	<div class="point_box">
											                		<span><?=$match_row['team_1_score']?></span>&nbsp;&nbsp;<span><?=$match_row['team_2_score']?></span>
											                	</div>
									                		</a>
															<?php } ?>

													<?php }?>

												</div>
											</td>
											<td class="text-center">
												<?=$team2['team_1_name']."(".$team2['team_1_grade'].")"?><br/>
												<?=$team2['team_2_name']."(".$team2['team_2_grade'].")"?>
											</td>
											<td class="text-center">
												<?php
												if($match_row['division'] == "단체전")
													$team_right_name = sql_fetch("select * from team_event_data where team_code='$team_right'");
												?>
													<a class="score_btn" href="popup_team_event_set.php?game_code=<?php echo $match_row['code'];?>&team=2&rd=<?=$redirectUrl?>" target="_blank"><?=$team_right_name['club']; ?></a>
												<?php
		//170602sh											}
												?>
												<?=$team2['club']?><br/><?=$team2['team_2_club']?>

											</td>
										</tr>
									<?php }?>
								<?php }?>
							<?php } ?>

		<!-- end Contents Area -->
							</tbody></table></div></div></section>

		<script>
		$('#gym').change(function(event){
			window.location.href='?c=<?=$c?>&dt='+encodeURIComponent('<?=$dt?>')+'&g='+$(this).val();
		});
		$('.start_game').click(function(event){
			event.preventDefault();
			var dataset = $(event.currentTarget).data();
			$.ajax({
				url:'./start_game.php'
				,data:dataset
				,dataType:'json'
				,success:function(data){
					console.log(data);
					if(data.state == 'ok'){
						window.location.reload();
						// gamecode = dataset.gamecode;
						// var html = '<a href="popup_insert_score.php?game_code='+(dataset.gamecode)+'&rd='+dataset.rd+'" target="_blank" class="btn mpoint">점수입력</a>'
						// console.log(html);
						// $(event.currentTarget).replaceWith(html);
					}
				}
			})
		})
		</script>
		<?php
		include_once(G5_AOS_PATH.'/tail.php');
		?>

<?php } ?>
