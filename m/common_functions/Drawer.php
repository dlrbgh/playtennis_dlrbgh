<?php



  function get_tournament_from_db($c, $d, $s, $ss, $reverse = false){
    $sql = "SELECT * FROM tennis3.game_score_data
        where match_code = '{$c}'
        and division = '{$d}' and series = '{$s}'
        and series_sub = '{$ss}'
        and tournament <> 'L'";
        if($reverse)
          $sql .="order by tournament desc , tournament_count desc , tournament_num desc";
        else
          $sql .="order by tournament, tournament_count, tournament_num";
    $return = array();
    $result = sql_query($sql);
    while($row = sql_fetch_array($result)){
      $return[] = $row;
    }

    return $return;
  }

  function is_tournament_made($c, $d, $s, $ss){

    // $sql = "select
    // case when team_1_code = '' then assigned_group_name1
    // else (select concat(club,' ', team_1_name,' ',team_2_name) from team_data where team_code = team_1_code)
    // end as team_1
    // ,case when team_2_code = '' then assigned_group_name2
    // else (select concat(club,' ', team_1_name,' ',team_2_name) from team_data where team_code = team_2_code)
    // end as team_2
    //
    // from game_score_data
    // where match_code = '$c' and division = '{$d}' and series = '{$s}' and series_sub = '{$ss}'
    // and tournament <> 'L'
    // order by tournament, tournament_count, tournament_num";

    //반영은 되지만 조이름만 올라가도록
    $sql = "select
    assigned_group_name1 as team_1
    ,assigned_group_name2 as team_2
    from game_score_data

    where match_code = '$c' and division = '{$d}' and series = '{$s}' and series_sub = '{$ss}'
    and tournament <> 'L'
    order by tournament, tournament_count, tournament_num";
    print $sql;
    $result = sql_query($sql);
    $full;
    $half;
    while($row = sql_fetch_array($result)){
      if($row['team_1'] == '' && $row['team_2'] == '')
        continue;
      else if($row['team_1'] != '' && $row['team_2'] != ''){
        $full[] = $row;
      }else if($row['team_1'] == '' || $row['team_2'] == ''){
        $half[] = $row;
      }
    }
    print_r($full);
    if(count($full) > count($half)){
      for($i = 0; $i < count($full); $i++){
        $return[] = $full[$i];
        if($half){
          $ele = array_shift($half);
          $return [] = $ele;
        }
      }
    }else{
      for($i = 0; $i < count($half); $i++){
        $return[] = $half[$i];
        if($full){
          $ele = array_shift($full);
          $return [] = $ele;
        }

      }
    }
    $return;
    return $return;
  }
  function get_drawer_example($teams){
    return make_tournament($teams);
  }
  function get_drawer($c, $d, $s, $ss, $teams){
    $tournament = is_tournament_made($c, $d, $s, $ss);
    if($tournament){
      // print">>>>";
      return $tournament;
    }else{
      return make_tournament($teams);
    }
  }

  function make_tournament($teams , $is_print=false){
    if($is_print){?>
      <style media="screen">
        ul li{
          display:inline-block;
          vertical-align: top;
          text-align:center;
          font-size:11px;
          padding:4px;
        }
      </style>
    <?php }
      $tournament;

        $is_full_tournament = is_full_tournament($teams);

        $rounds = get_rounds($teams);

        for($tmp_rounds = 0; $tmp_rounds <=$rounds;){
          if($tmp_rounds == 0){
            $tmp_rounds = 2;
          }
          $tournament[$tmp_rounds] = '';
          $tmp_rounds = $tmp_rounds * 2;
        }

        if($rounds * 2 < $teams){
          $tournament [($rounds*2)] = '';
        }

        $team_index = 0;

        $groups = get_groups($teams);
        $team_order = 0;


       ?>
    <?php

      $first_seed;
      $first_more;

      $second_seed;
      $second_more;


      $by = $teams - $rounds;
      $half_1 = $by == 1 ? 1 : $by/2 ;
      $half_2 = $by == 1 ? 1 : $by/2 ;



      $first_team_distribute = 2;
      $team = 0;

      for($round = 0; $round < $rounds/2; $round++){
        if($first_team_distribute == 2){
        }

        if($first_team_distribute == 1){
          $first_seed[] = $team;
        }else if($first_team_distribute == 2){
          $first_more[] = array($team, $team+1);
        }
        for($team = $team_index; $team < $team_index+$first_team_distribute && $team < $teams; $team++){

        }

        $half_1--;
        $team_index = $team_index +$first_team_distribute;
        if($half_1 == 0){
          $first_team_distribute = 1;
        }

      }
      $second_team_distribute = 2;
      for($round = $rounds/2; $round < $rounds; $round++){
          if($second_team_distribute == 2){
          }

          if($second_team_distribute == 1){
            $second_seed[] = $team;
          }else if($second_team_distribute == 2){
            $second_more[] = array($team, $team+1);
          }
        for($team = $team_index; $team < $team_index+$second_team_distribute && $team < $teams; $team++){

        }
        $half_2--;
        $team_index = $team_index +$second_team_distribute;
        if($half_2 == 0){
          $second_team_distribute = 1;
        }

      }


      if($is_print){
        print "<ul id=\"imade\">";
        print "<li>";
        print "{$rounds}강";
        if($half_1){
          $next = $rounds *2;
          print "<br>{$next}강";
        }
      }




      $group_first_ranking_index = 0;
      $group_second_ranking_index = count($groups);

      $seed_shift_index = 0;
      $seed_shift_max = ceil($team/4);


      if(count($first_seed) >= count($first_more)){

        while($first_seed){
          $seed = array_pop($first_seed);
          $seed_shift_index ++;
          if($seed_shift_index <= $seed_shift_max){
              $who_distribute = array_shift($groups);
          }else{
            $who_distribute = array_pop($groups);
          }

          if($is_print){print "<li>".$who_distribute."</li>";}
          $tournament[$round][] = array($who_distribute);
          if($first_more){
            $more = array_pop($first_more);
            $who_distribute1 = array_pop($groups);
            $who_distribute2 = array_pop($groups);
            if($is_print){print "<li>승자조<br>{$who_distribute1}-{$who_distribute2}</li>";}
            $tournament[$round][] = count($tournament[$round*2]);
            $tournament[$round*2][] =array($who_distribute1,$who_distribute2);
          }
        }
      }else{
        while($first_more){
          if($first_seed){
            $seed = array_pop($first_seed);
            $who_distribute = array_shift($groups);
            if($is_print){print "<li>".$who_distribute."</li>";}

            $tournament[$round][] = array($who_distribute);
          }

          $more = array_pop($first_more);
          $who_distribute1 = array_pop($groups);
          $who_distribute2 = array_pop($groups);
          if($is_print){print "<li>승자조<br>{$who_distribute1}-{$who_distribute2}</li>";}

          $tournament[$round][] = count($tournament[$round*2]);
          $tournament[$round*2][] =array($who_distribute1,$who_distribute2);
        }
      }

      if($is_print){print '<li> half </li>';}
      if(count($second_seed) >= count($second_more)){

        while($second_seed){

          $who_distribute = array_shift($groups);
          $seed = array_pop($second_seed);
          // print "<li>{$seed}</li>";
          if($is_print){print "<li>".$who_distribute."</li>";}
          $tournament[$round][] = array($who_distribute);
          if($second_more){
            $more = array_pop($second_more);

            $who_distribute1 = array_pop($groups);
            $who_distribute2 = array_pop($groups);
            // print "<li>승자조<br>{$more[0]}-{$more[1]}</li>";
            if($is_print){print "<li>승자조<br>{$who_distribute1}-{$who_distribute2}</li>";}
            $tournament[$round][] = count($tournament[$round*2]);
            $tournament[$round*2][] =array($who_distribute1,$who_distribute2);
            // $tournament[$round*2][] =$who_distribute1.' '.$who_distribute2;
          }

        }
      }else{
        while($second_more){
          if($second_seed){
            $who_distribute = array_shift($groups);
            $seed = array_pop($second_seed);
            if($is_print){print "<li>".$who_distribute."</li>";}
                  $tournament[$round][] = array($who_distribute);
          }
          $more = array_pop($second_more);

          $who_distribute1 = array_pop($groups);
          $who_distribute2 = array_pop($groups);

          if($is_print){print "<li>승자조<br>{$who_distribute1}-{$who_distribute2}</li>";}

          $tournament[$round][] = count($tournament[$round*2]);
          $tournament[$round*2][] =array($who_distribute1,$who_distribute2);
        }
      }

      if($is_print){print "</ul>";}

      return $tournament;
  }
  function get_groups($teams){





    // $groups;
    // for($group = 0; $group < $teams; $group++){
    //   $team_order ++;
    //   if($group < $teams/2){
    //     $groups[$group] = ($team_order)."조 1위";
    //   }else{
    //     $groups[$group] = ($team_order)."조 2위";
    //   }
    //
    //   if($team_order >= $teams/2){
    //     $team_order = 0;
    //   }
    // }
    return $groups;
  }



  function is_full_tournament($teams){
    if($teams == 4 )
      return 2;
    else if($teams == 8)
      return 4;
    else if($teams == 16)
      return 8;
    else if($teams == 32)
      return 16;
    else if($teams == 64)
      return 32;
    else if($teams == 128)
      return 64;
    else if($teams == 256)
      return 128;
    else if($teams == 512)
      return 256;
    else
      return false;
  }

   function get_rounds($teams) {
     if($teams <= 4 )
       return 2;
     else if($teams <= 8)
       return 4;
     else if($teams <= 16)
       return 8;
     else if($teams <= 32)
       return 16;
     else if($teams <= 64)
       return 32;
     else if($teams <= 128)
       return 64;
     else if($teams <= 256)
       return 128;
     else if($teams <= 512)
       return 256;
   }


   function get_rounds_example($teams){
     $round[2] = "['1조 1위', '1조 2위']";
     $round[4] = "['1조 1위', '2조 2위'], ['2조 1위', '1조 2위']";
     $round[6] = "['1조 1위', null], ['2조 2위', '3조 2위']
            ,['3조 1위', '1조 2위'], [null, '2조 1위']";
    $round[8] = "['1조 1위', '2조 2위'], ['4조 1위', '3조 2위']
       ,['3조 1위', '4조 2위	'], ['1조 2위	', '2조 1위']";
     $round[10] = " ['1조 1위', null], ['2조 2위', '3조 2위']
           , ['5조 1위',null], ['4조 1위',null]
           , ['3조 1위', null], ['4조 2위',null]
           , ['5조 2위','1조 2위'], ['2조 1위',null]";
     $round[12] = " ['1조 1위', null], ['2조 2위', '6조 2위']
           , ['5조 1위','3조 2위'], ['4조 1위',null]
           , ['3조 1위', null], ['4조 2위','6조 1위']
           , ['5조 2위','1조 2위'], ['2조 1위',null]";
     $round[14] = " ['1조 1위',null], ['7조 2위','2조 2위	']
            , ['5조 1위','6조 2위'], ['3조 2위','4조 1위']
            , ['3조 1위','4조 2위'], ['5조 2위','6조 1위']
            , ['7조 1위','1조 2위'], ['2조 1위',null]";
      $round[16] = "['1조 1위','2조 2위'], ['7조 2위','8조 1위']
            , ['5조 1위','6조 2위'], ['3조 2위','4조 1위']
            , ['3조 1위','4조 2위'], ['5조 2위','6조 1위']
            , ['7조 1위','8조 2위'], ['2조 1위','1조 2위']";
    $round[18] = "['1조 1위',null], ['2조 2위','7조 2위']
                , ['9조 1위',null], ['8조 1위',null]
                , ['5조 1위',null], ['6조 2위',null]
                , ['3조 2위',null], ['4조 1위',null]
                , ['3조 1위',null], ['4조 2위',null]
                , ['5조 2위',null], ['6조 1위',null]
                , ['7조 1위',null], ['8조 2위',null]
                , ['9조 2위','1조 2위'], ['2조 1위',null]";
     $round[20] = "['1조 1위',null], ['2조 2위','10조 2위']
           , ['9조 1위',null], ['8조 1위',null]
           , ['5조 1위',null], ['7조 2위',null]
           , ['6조 2위','3조 2위'], ['4조 1위',null]
           , ['3조 1위',null], ['4조 2위','5조 2위']
           , ['8조 2위',null], ['6조 1위',null]
           , ['7조 1위',null], ['10조 1위',null]
           , ['9조 2위','1조 2위'], ['2조 1위',null]";

     $round[22] = "['1조 1위',null], ['2조 2위','10조 2위']
           , ['9조 1위',null], ['8조 1위',null]
           , ['5조 1위',null], ['7조 2위','6조 2위']
           , ['11조 2위','3조 2위'], ['4조 1위',null]
           , ['3조 1위',null], ['4조 2위','5조 2위']
           , ['8조 2위','11조 1위'], ['6조 1위',null]
           , ['7조 1위',null], ['10조 1위',null]
           , ['9조 2위','1조 2위'], ['2조 1위',null]";

     $round[24] = "['1조 1위',null], ['2조 2위','10조 2위']
           , ['9조 1위','7조 2위'], ['8조 1위',null]
           , ['5조 1위',null], ['6조 2위','12조 1위']
           , ['11조 2위','3조 2위'], ['4조 1위',null]
           , ['3조 1위',null], ['4조 2위','12조 2위']
           , ['11조 1위','5조 2위'], ['6조 1위',null]
           , ['7조 1위',null], ['8조 2위','10조 1위']
           , ['9조 2위','1조 2위'], ['2조 1위',null]";

     $round[26] = "['1조 1위',null], ['2조 2위','10조 2위']
           , ['9조 1위','7조 2위'], ['8조 1위','6조2위']
           , ['5조 1위',null], ['11조 2위','12조 1위']
           , ['13조 1위','3조 2위'], ['4조 1위',null]
           , ['3조 1위',null], ['4조 2위','13조 2위']
           , ['11조 1위','12조 2위'], ['6조 1위',null]
           , ['7조 1위','5조 2위'], ['8조 2위','10조 1위']
           , ['9조 2위','1조 2위'], ['2조 1위',null]";

           $round[28] = "['1조 1위',null], ['2조 2위','10조 2위']
                 , ['9조 1위','7조 2위'], ['8조 1위','6조 2위']
                 , ['5조 1위','11조 2위'], ['14조 2위','12조 1위']
                 , ['13조 1위','3조 2위'], ['4조 1위',null]
                 , ['3조 1위',null], ['4조 2위','14조 1위']
                 , ['11조 1위','13조 2위'], ['12조 2위','6조 1위']
                 , ['7조 1위','5조 2위'], ['8조 2위','10조 1위']
                 , ['9조 2위','1조 2위'], ['2조 1위',null]";
     $round[30] = "['1조 1위',null], ['2조 2위','15조 2위']
           , ['9조 1위','10조 2위'], ['7조 2위','8조 1위']
           , ['5조 1위','6조 2위'], ['11조 2위','12조 1위']
           , ['13조 1위','14조 2위'], ['4조 1위','3조 2위']
           , ['3조 1위','4조 2위'], ['13조 2위','14조 1위']
           , ['11조 1위','12조 2위'], ['5조 2위','6조 1위']
           , ['7조 1위','8조 2위'], ['9조 2위','10조 1위']
           , ['15조 1위','1조 2위'], ['2조 1위',null]";

     $round[32] = "['1조 1위','2조 2위'], ['15조 2위','16조 1위']
           , ['9조 1위','10조 2위'], ['8조 1위','7조 2위']
           , ['5조 1위','6조 2위'], ['11조 2위','12조 1위']
           , ['13조 1위','14조 2위'], ['3조 2위','4조 1위']
           , ['3조 1위','4조 2위'], ['13조 2위','14조 1위']
           , ['11조 1위','12조 2위'], ['5조 2위','6조 1위']
           , ['7조 1위','8조 2위'], ['9조 2위','10조 1위']
           , ['15조 1위','16조 2위'], ['1조 2위','2조 1위']";
           return $round[$teams];
   }
 ?>
