<?php
  include_once('../../../common.php');
  // $query = "select use_court from match_gym_data where match_id = '{$c}' and gym_id = {$gym_id}";

      $is_league_end = "SELECT
            end_game , count(*) as count
    FROM
        game_score_data
            JOIN
        group_data ON game_score_data.group_code = group_data.code
    WHERE
        game_score_data.match_code = '{$c}'
            AND gym_code = ${gym_id}
            AND game_score_data.game_court = ${court_id}
            AND team_1_code <> ''
            and game_score_data.tournament = 'L'
            and end_game = 'N'
            GROUP BY end_game
    ORDER BY game_score_data.division , game_score_data.series , game_score_data.series_sub , group_data.num";
    $count_n = sql_fetch($is_league_end)['count'];
    $tournament = "and game_score_data.tournament = 'L'";
if($count_n == 0){
      $tournament = "and game_score_data.tournament != 'L'";
    }

  $query="SELECT
    game_score_data.division,
    game_score_data.series,
    game_score_data.series_sub,
    group_data.code as group_code,
    group_data.num, team_1_code,
    (select concat(club,'-', team_1_name,'-',team_2_name) from team_data where team_code = team_1_code)as team_1,
    (select concat(club,'-', team_1_name,'-',team_2_name) from team_data where team_code = team_2_code) as team_2,
    team_1_score,
    team_2_score,
    end_game
FROM
    game_score_data
        JOIN
    group_data ON game_score_data.group_code = group_data.code
WHERE
    game_score_data.match_code = '{$c}'
        AND gym_code = ${gym_id}
        AND game_score_data.game_court = ${court_id}
        AND team_1_code <> ''
        {$tournament}
ORDER BY game_score_data.division , game_score_data.series , game_score_data.series_sub , group_data.num";
  $result = sql_query($query);
  $json_result = array();
  while($row = sql_fetch_array($result)){
    $json_result [] = $row;
  }

  die(json_encode($json_result));


?>
