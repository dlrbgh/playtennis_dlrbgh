<?php
  include_once('../../../common.php');


  $is_league_end = "SELECT
        end_game , count(*) as count
FROM
    game_score_data
        JOIN
    group_data ON game_score_data.group_code = group_data.code
WHERE
    game_score_data.match_code = '{$c}'
        AND gym_code = ${gym_id}
        AND game_score_data.game_court = ${court_id}
        AND team_1_code <> ''
        and game_score_data.tournament = 'L'
        and end_game = 'N'
        GROUP BY end_game
ORDER BY game_score_data.division , game_score_data.series , game_score_data.series_sub , group_data.num";
$count_n_result = sql_fetch($is_league_end);
$count_n = $count_n_result['count'];
$tournament = "and game_score_data.tournament = 'L'";
if($count_n == 0){
  $tournament = "and game_score_data.tournament != 'L'";
}



  $query="SELECT
        end_game , count(*) as count
FROM
    game_score_data
        JOIN
    group_data ON game_score_data.group_code = group_data.code
WHERE
    game_score_data.match_code = '{$c}'
        AND gym_code = ${gym_id}
        AND game_score_data.game_court = ${court_id}
        AND team_1_code <> ''
        {$tournament }

ORDER BY game_score_data.division , game_score_data.series , game_score_data.series_sub , group_data.num";
// print $query;
$result = sql_query($query, true);
$json_result = array();
while($row = sql_fetch_array($result)){
  $json_result [$row['end_game']] = $row;
}

die(json_encode($json_result));

?>
