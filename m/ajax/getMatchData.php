<?php


  include_once('../../common.php');

$searchquery = $searchText == '' ? "" : "and wr_name like '%{$searchText}%'";
$sql = '';
if($type=='favor'){
  $sql = "

      SELECT * ,
      CASE
        WHEN date_format(date1, '%Y') = date_format(date2, '%Y')
        THEN concat(date_format(date1, '%Y-%m-%d'),' ~ ', date_format(date2, '%m-%d'))
        ELSE concat(date_format(date1, '%Y-%m-%d'),' ~ ', date_format(date2, '%Y-%m-%d'))
      END AS period
      FROM match_data join favorite_match
      on match_data.code = favorite_match.match_code

       where favorite_match.mb_id = '".get_session('ss_mb_id')."'
      {$searchquery}
      limit $a, $limit;
      ";
}else{
  $sql = "
      SELECT * ,
      CASE
        WHEN date_format(date1, '%Y') = date_format(date2, '%Y')
        THEN concat(date_format(date1, '%Y-%m-%d'),' ~ ', date_format(date2, '%m-%d'))
        ELSE concat(date_format(date1, '%Y-%m-%d'),' ~ ', date_format(date2, '%Y-%m-%d'))
      END AS period
      FROM match_data
      where 1
      {$searchquery}
      limit $a, $limit;
      ";
}
// print $sql;
$result = sql_query($sql);
$json = array();
while($row = sql_fetch_array($result)){
  $json[] = $row;
}

die(json_encode($json));


 ?>
