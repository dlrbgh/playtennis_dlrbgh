<?php
if (!defined('_GNUBOARD_')) exit; // 개별 페이지 접근 불가

if(defined('G5_THEME_PATH')) {
    require_once(G5_THEME_PATH.'/tail.php');
    return;
}
?>


<style>
/*푸터 뒤로가기*/
.footer_btn_area{height:70px;}
.footer_btn_area ul{position:fixed;bottom:0;right:0;list-style:none;z-index:999;}
.footer_btn_area ul li{float:left}
.footer_btn{margin:0 10px;width:42px;height:42px;display:inline-block;}
.footer_btn img{width:100%}
</style>
<?php if (!defined("_INDEX_")) { ?> 
<div class="footer_btn_area">
	<ul>
		<!--
		<li>
			<a href="<?php echo G5_URL ?>/m/index.php" onclick="" class="footer_btn">
				<img class="img-responsive" src="<?php echo G5_AOS_URL?>/assets/img/home_icon.png" />
			</a>
		</li>
		-->
		<li>
			<a href="javascript:history.go(0)" onclick="" class="footer_btn">
				<img class="img-responsive" src="<?php echo G5_AOS_URL?>/assets/img/refresh_icon.png" />
			</a>
		</li>
		<!--
		<li>
			<a href="javascript:history.go(-1)" class="footer_btn">
				<img class="img-responsive" src="<?php echo G5_AOS_URL?>/assets/img/back_icon.png" />
			</a>
		</li>-->
	</ul>
</div>
<?php } ?>
    </main>
    <!-- END Main Container -->
</div>
<!-- END Page Container -->


<?php


if ($config['cf_analytics']) {
    echo $config['cf_analytics'];
}
?>

<script>
// $(function() {
//     // 폰트 리사이즈 쿠키있으면 실행
//     font_resize("container", get_cookie("ck_font_resize_rmv_class"), get_cookie("ck_font_resize_add_class"));
// });
</script>

<?php
include_once(G5_PATH."/aos_tail.sub.php");
?>
