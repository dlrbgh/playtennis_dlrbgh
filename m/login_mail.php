<?php
define('_INDEX_', true);
include_once('../common.php');



if (!defined('_GNUBOARD_')) exit; // 개별 페이지 접근 불가
include_once(G5_AOS_PATH.'/head.php');
?>
<style>
	body{background-color:#fff}
</style>
<div class="login_hd_area">
	<div class="hd_lnb"><!--<a href="javascript:history.go(-1)"><i class="flaticon-left-arrow"></i></a>--></div>
	<a class="title">이메일 로그인</a>
	<div class="hd_right"><a>&nbsp;</a></div>
</div>

<!-- 회원가입 -->
<form class="" action="./login_check.php" method="post" onsubmit="return verify()">
<div class="login_container">
	<div class="tbl_frm02">
		<table>
			<tbody>
				<tr>
					<th width="90">이메일</th>
					<td><input type="text" id="mb_id" name="mb_id" value="<?=$mb_id?>" placeholder="이메일 주소를 입력하세요" /></td>
				</tr>

				<tr>
					<th>비밀번호</th>
					<td><input type="password" id="mb_password" name="mb_password" placeholder="비밀번호를 입력하세요" /></td>
				</tr>
			</tbody>
		</table>
	</div>

	<div class="register_area">
		<div class="tip mb-10" style="padding:0; text-align:center">
		<a href="register.php" style="padding:15px;display:block; height:100%;">회원가입하기</a>
		</div>
	</div>
	<div class="con_btn_area">
		<button type="submit">로그인</a>
	</div>
	<style>
		/*회원가입 콘텐츠 버튼 */
.con_btn_area{position:relative;width:100%;display:block}
.con_btn_area button{background-color:#14264a;color:#fff;padding:20px 0;display:block;text-align:center;width:100%;border:1px solid #14264a}
.con_btn_area button:active{background-color:#bdbdbd;color:#000}
	</style>
</div>
<!-- //회원가입 -->
<div class="ft_area">
	<!-- <div class="ft_hd text-center">
		<a href="">비밀번호 찾기</a>
	</div> 
	<div class="btn_area">
		<button type="submit">로그인</a>
	</div>
	-->
</div>
</form>
<script>
	function verify(){
		var mb_id = $('#mb_id').val();
		var mb_password = $('#mb_password').val();
		var msg = $('#msg');
		// return false;

		$('input').removeClass('border_color5')
		if(mb_id == ''){
			$('#mb_id').attr('placeholder', '이메일을 입력하세요').addClass('border_color5')
			$('#mb_id').focus();
			return false;
		}else if(mb_password == ''){
			$('#mb_password').attr('placeholder', '비밀번호를 입력하세요').addClass('border_color5')
			$('#mb_password').focus();
			// $(msg).find('td').text('비밀번호를 입력하세요');
			return false;
		}
		return true;
	}

			<?php if($type == 'not_exsist'){?>
				$('.tbl_frm02').before('<div style="text-align:center;">\
				등록된 회원정보가 없습니다.<br />회원가입하시겠습니까?\
				<div class="tip mb-10" style="padding:0; text-align:center">\
				<a href="register.php" style="padding:15px;display:block; height:100%;">회원가입하기</a>\
				</div>\
				</div>');
			<?php }
			if($type == 'wrong'){?>
				$('.tbl_frm02').before('<div style="text-align:center;">\
				정보를 잘못 입력하셨습니다. 다시 입력하세요\
				</div>');
			<?php }?>

</script>
<?php
include_once(G5_AOS_PATH.'/tail.php');
?>
