<?php
include_once('../common.php');

$user_code	= $_REQUEST['user_code'];
$club	= $_REQUEST['user_club'];
$name	= $_REQUEST['user_name'];
$act = 'current_my';

$sql = "select * from match_data where wr_id = '$wr_id' order by wr_id desc";
$result = sql_query($sql);
$i = 1;
$r = sql_fetch_array($result);
$code = $r['code'];

?>
<?php require 'inc/config.php'; ?>
<?php require 'inc/views/template_head_start.php'; ?>


<?php require 'inc/views/template_head_end.php'; ?>
<!-- sub nav -->
<?php include_once('./app_sub_nav.php'); ?>
<!-- end sub nav -->

<!-- Contents Area -->
	
	<div class="content" style="margin-top: 42px;">
	    <div class="btn-group btn-group-justified push-10">
	        <div class="btn-group">
	            <a href="current_my_info.php?wr_id=<?=$wr_id;?>&user_code=<?=$user_code;?>&user_club=<?=$club;?>&user_name=<?=$name;?>"><button class="btn btn-lg btn-white " type="button">경기 정보</button></a>
	        </div>
	        <div class="btn-group">
	            <a href="current_my_tm.php?wr_id=<?=$wr_id;?>&user_code=<?=$user_code;?>&user_club=<?=$club;?>&user_name=<?=$name;?>"><button class="btn btn-lg btn-white active" type="button">토너먼트 정보</button></a>
	        </div>
		</div>
		
	   <!-- 등록된 선수정보가 없을경우 -->
	    <?php if($club == "" || $name == ""){?>
	    <div class="row text-center" style="height:100%;padding:50px 0 0 0">
	    	<div class="col-md-12">
	    		<h1 class="font-s128 font-w300 animated flipInX"><i class="fa fa-exclamation-circle"></i></h1>
	    		<div class="h4 font-w700 push-10">
		    		등록된 선수정보가 없습니다.
		    	</div>
		    	<div class="h4 font-w700 push-10">
		    		메뉴 > 설정 > 선수등록
		    	</div>
		    	<div class="h4 font-w700">
		    		선수 정보를 등록해 주세요.
		    	</div>
		    </div>
	    	<div class="col-md-12 push-20-t">
	    		<button class="btn btn-block btn-mobile push-10" onclick="insert_data()" type="button"><i class="fa fa-exclamation-circle pull-left"></i>선수정보 등록하기</button>
	    	</div>
	    </div>
	    
	    <?php }else{?>
		<?php
			$gym_sql = "select count(*) as cnt from team_data as a inner join match_data as b where a.club = '$club' and (a.team_1_name = '$name' or a.team_2_name = '$name') and a.match_code = b.code and b.end_game > '2' and a.match_code='$code'  group by code";
			$gym_result = sql_query($gym_sql);
			$gym = sql_fetch_array($gym_result);
			if($gym['cnt'] == 0){
		?>
		<!-- 대회에 참가 하지 않은경우 -->
	    <div class="row text-center" style="height:100%;padding:50px 0 0 0">
	    	<div class="col-md-12">
    		<h1 class="font-s128 font-w300 animated flipInX"><i class="fa fa-exclamation-circle"></i></h1>
    		<div class="h4 font-w700 push-10">
	    		<?=$r['wr_name']?>에 
	    	</div>
	    	<div class="h4 font-w700 push-10">
	    		<?=$name;?>님은 참가신청을 하지 않으셨습니다.
	    	</div>
	    </div>
	    <?php		
			}
		?>	    
		<?php }?>
	    
	    
	    <!-- end 등록된 선수정보가 없을경우 -->
	    
	    <!-- end 대회에 참가 하지 않은경우 -->
	    
	    <!-- 웹으로 접속한경우 -->
	    <!-- <div class="row text-center">
	    	<div>
	    		내경기 서비스는 모바일 웹에서 지원하지 않습니다.
	    	</div>
	    	<div>
	    		콕선생 어플리케이션을 설치해 주시기 바랍니다.
	    	</div>
	    	<div>
	    		<a href="">구글플레이</a>
	    		<a href="">앱스토어</a>
	    	</div>
	    </div> -->
	    <!-- end 웹으로 접속한경우 -->
	    
	    <?php
			$gym_sql = "select * from team_data as a inner join match_data as b where a.club = '$club' and (a.team_1_name = '$name' or a.team_2_name = '$name') and a.match_code = b.code and b.end_game > '2' and a.match_code='$code'   group by code";
			$gym_result = sql_query($gym_sql);
			while($gym = sql_fetch_array($gym_result)){
		?>
		<div class="push-10">
			<h5><i class="fa fa-calendar-check-o push-5-r"></i><span class="text-primary font-w700"><?=$name;?></span>님이 출전하는 토너먼트 목록 입니다.</h5>
		</div>    
	    <!-- 접수현황 -->
	    <div class="row">
	    	<div class="col-lg-6">
	        	<div class="block">
			            <table class="table table-mobile">
			                <!-- 그룹 경기 조회 타이틀 -->
			                <tbody class="js-table-sections-header open">
			                    <tr>
			                        <td class="font-w600" width="85%"><?=$gym['wr_name'];?></td>
			                    </tr>
			                </tbody>
			                <!-- end 그룹 경기 조회 타이틀 -->
			                <tbody>
			                	<?php 
			                		$team_sql = "select * from team_data as a where a.club = '$club' and (a.team_1_name = '$name' or a.team_2_name = '$name') and a.match_code = '$gym[code]'";
									$team_result = sql_query($team_sql);
									while($team = sql_fetch_array($team_result)){
											
										$division = $team['division'];
										$series = $team['series'];
										$series_sub = $team['series_sub'];

										$match_gym_sql = "select * from game_score_data as a inner join gym_data as b where (a.team_1_code = '$team[team_code]' or a.team_2_code = '$team[team_code]') and a.match_code = '$team[match_code]' and a.gym_code = b.wr_id group by a.game_date";
										$match_gym_result = sql_query($match_gym_sql);
										$match_gym = sql_fetch_array($match_gym_result);
			                	?>
			                    <tr>
			                    	<td class="mychamp_list">
			                    		<div class="title">
			                    			<span class=""><?=$team['division'];?> <?=$team['series'];?> <?=$team['series_sub'];?></span><span class="push-10-l">(<?=$match_gym['gym_name']?>)</span>
			                    			<span class="pull-right"><a href="champ_info_match_view.php?wr_id=<?=$gym['wr_id']?>&user_code=<?=$user_code;?>&user_club=<?=$user_club;?>&user_name=<?=$user_name;?>&division=<?=$team['division']?>&series=<?=$team['series']?>&series_sub=<?=$team['series_sub']?>">바로가기</a></span>
			                    		</div>
			                    		
			                    		<!-- 토너먼트 대진표 출력 -->
								        	<?php
									        	$sql = "select count(*) as cnt from group_data where match_code = '$code' and division = '$division' and series = '$series' and series_sub = '$series_sub' and tournament = 'L'";
												$result = sql_query($sql);
												$r = sql_fetch_array($result);
												if($r['cnt'] > 1){
											?>
									    	<div class="col-lg-12 push-10 bg-white" style="padding-bottom:10px;">
									    		<img class="img-responsive" src="<?php echo G5_IMG_URL?>/tour_seed/<?=$r['cnt'];?>.gif">
										        <div class="text-center push-10-t">
									    			<h5>위 토너먼트는 예선전 순위에따라 배정됩니다</h5>
									    		</div>
									        </div>
								        	<?php		
												}				
											?>
										<!-- end 토너먼트 대진표 출력 -->
										
										<!--본인 급수의 예선전 순위 -->
										<div class="row">
											<div class="col-md-12">
									        	<div class="block">
											        <div class="block-tit" >
									                    <ul class="block-options">
									                        <li>
									                            <button type="button" data-toggle="block-option" data-action="content_toggle"><i class="si si-arrow-down"></i></button>
									                        </li>
									                    </ul>
									                    <h3 class="block-title">토너먼트 시드배정 순위</h3>
									                </div>
											        <div class="block-content_list">
											        	<!-- 경기 결과표 출력 -->
									                    <table class="table champ_result">
								                    		<thead>
								                    			<th class="text-center">순위</th>
								                    			<th class="text-center">클럽</th>
								                    			<th class="text-center">선수</th>
								                    			<th class="text-center">승</th>
								                    			<th class="text-center">패</th>
								                    			<th class="text-center">득점</th>
								                    			<th class="text-center">실점</th>
								                    			<th class="text-center">득실</th>
								                    			<th class="text-center">그룹순위</th>
								                    		</thead>
								                    		<tbody class="text-center" >
								                    			<?php 
																$group_cnt_sql = "select count(wr_id) as cnt from group_data where match_code = '$code' and  division = '$division' and series = '$series' and series_sub = '$series_sub'";
																$result = sql_query($group_cnt_sql);
																$group_data = sql_fetch_array($result);
																
																if($group_data['cnt'] == 2 ){
																	$group_count = 4;
																}
																if($group_data['cnt'] == 3 ){
																	$group_count = 6;
																}
																if($group_data['cnt'] == 4 ){
																	$group_count = 4;
																}
																if($group_data['cnt'] > 4 && $group_data['cnt'] < 8  ){
																	$group_count = (8-$group_data['cnt'])+($group_data['cnt']-(8-$group_data['cnt']));
																}
																if($group_data['cnt'] == 8  ){
																	$group_count = 8;
																}
																if($group_data['cnt'] > 8 && $group_data['cnt'] < 16 ){
																	$group_count = (16-$group_data['cnt'])+($group_data['cnt']-(16-$group_data['cnt']));
																}
																if( $group_data['cnt'] == 16 ){
																	$group_count = 16;
																}
																
								                    			$score_sql = "select * from team_data where match_code = '$code' and division = '$division' and series = '$series' and series_sub = '$series_sub'  ORDER BY group_rank,team_league_point desc ,winning_rate desc	,gains_losses_point desc, team_total_match_point DESC Limit 0,$group_count";
																
																$score_result = sql_query($score_sql);
																$i = 1;
																while($score = sql_fetch_array($score_result)){
																	
																	$gourp_sql = "select num from group_data where (team_1  = '$score[team_code]' or team_2  = '$score[team_code]' or team_3  = '$score[team_code]' or team_4  = '$score[team_code]'  or team_5  = '$score[team_code]') and match_code = '$code' and division = '$division' and series = '$series' and series_sub = '$series_sub' and tournament = 'L' order by num asc";
																	$group_result = sql_query($gourp_sql);
																	$group = sql_fetch_array($group_result);
																?>
								                    			<tr>
								                    				<td><?=$i;?></td>
								                    				<td><?=$score['club'];?></td>
								                    				<td><?=$score['team_1_name'];?>&nbsp;<?=$score['team_2_name'];?></td>
								                    				<td><?=$score['team_league_point'];?></td>
								                    				<td><?=$score['team_league_lose_count'];?></td>
								                    				<td><?=$score['team_total_match_point'];?></td>
								                    				<td><?=$score['team_total_match_minus_point'];?></td>
								                    				<td><?=$score['gains_losses_point'];?></td>
								                    				<td><?=$group['num']+1?>그룹<?=$score['group_rank'];?>위</td>
								                    			</tr>
								                    			<?php $i++; } ?>
								                    			
								                    		</tbody>
								                    	</table>
								                    	<div class="push-10-t font-w700">순위 선정 기준 : 다승 > 승률 > 득실차 > 다득점</div>
									                    <!-- end 경기 결과표 출력 --> 
										            </div>
									            </div>
								            </div>
							            </div>
										<!--본인 급수의 예선전 순위 -->
			                    		
			                    	</td>
			                    </tr>
			                    <?php } ?>
			                </tbody>
			            </table>
			    </div>
	        	
	        </div>
	    </div>
	<?php } ?>
	</div>
<!-- end Contents Area -->


<?php require 'inc/views/base_footer.php'; ?>
<?php require 'inc/views/template_footer_start.php'; ?>

<script>
	function insert_data(){
		window.AJInterface.insert_info();
	}
</script>



<?php require 'inc/views/template_footer_end.php'; ?>

