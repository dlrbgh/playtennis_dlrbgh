<?php
include_once('../common.php');
include_once('./lib/common.lib.php');

$rows = array();

$wr_id 				= $_REQUEST['wr_id'];
$division			= $_REQUEST['division'];
$series 			= $_REQUEST['series'];
$series_sub			= $_REQUEST['series_sub'];
$match_code			= $_REQUEST['match_code'];
$modify				= $_REQUEST['modify'];

$tournament			= $_REQUEST['tournament'];
$tournament_count	= $_REQUEST['tournament_count'];

$team_1_score		= (int)$_REQUEST['team_1_score'];
$team_2_score		= (int)$_REQUEST['team_2_score'];

$team_1_score_before	= $_REQUEST['team_1_score_before'];
$team_2_score_before	= $_REQUEST['team_2_score_before'];
$require_url	= $_REQUEST['rd'];

if($wr_id == ""){
	$rows = array();
	$rows[]['result'] = "false";		
	echo json_encode($rows);
	return;
}

if($division == ""){
	$rows = array();
	$rows[]['result'] = "false";		
	echo json_encode($rows);
	return;
}
if($series == ""){
	$rows = array();
	$rows[]['result'] = "false";		
	echo json_encode($rows);
	return;
}
if($match_code == ""){
	$rows = array();
	$rows[]['result'] = "false";		
	echo json_encode($rows);
	return;
}
if($modify == ""){
	$rows = array();
	$rows[]['result'] = "false";		
	echo json_encode($rows);
	return;
}

if($tournament == ""){
	$rows = array();
	$rows[]['result'] = "false";		
	echo json_encode($rows);
	return;
}

if($tournament_count == ""){
	$rows = array();
	$rows[]['result'] = "false";		
	echo json_encode($rows);
	return;
}

//점수 삽입
$team_field = "team_data";

if($division == "단체전"){
	$team_field = "team_event_data";	
}

if($team_1_dis == "1" || $team_1_dis == "2" ){
	$sql = " update game_score_data
            set 
                 team_1_score 	= '0',
                 team_2_score 	= '6',
                 end_game 		= 'Y',
                 team_1_dis 	= '$team_1_dis'     
            where wr_id = '$wr_id' and match_code = '$match_code'";
	sql_query($sql);
}
else if($team_2_dis == "1" || $team_2_dis == "2" ){
	$sql = " update game_score_data
            set 
                 team_1_score 	= '6',
                 team_2_score 	= '0',
                 end_game 		= 'Y',
                 team_2_dis 	= '$team_2_dis'
            where wr_id = '$wr_id' and match_code = '$match_code'";
	sql_query($sql);
}
else{
	$sql = " update game_score_data
            set 
                 team_1_score 	= '$team_1_score',
                 team_2_score 	= '$team_2_score',
                 end_game 		= 'Y',
                 team_1_dis 	= '$team_1_dis',
                 team_2_dis 	= '$team_2_dis'     
                 
            where wr_id = '$wr_id' and match_code = '$match_code'";
	sql_query($sql);	
}

$group_code_sql  = "select * from game_score_data where wr_id = '$wr_id' and match_code = '$match_code'";
$group_code_result = sql_query($group_code_sql);
$group_code = sql_fetch_array($group_code_result);

if($wr_text != ""){
	$sql = " insert into game_score_data_history
            set 
                 wr_text 	= '$wr_text',
                 game_code 	= '$group_code[code]',
                 match_code = '$match_code',
                 wr_datetime = '".G5_TIME_YMDHIS."',
				mb_id = '$member[mb_id]',
				wr_ip = '{$_SERVER['REMOTE_ADDR']}'";
	sql_query($sql);
	
}



/**
 * 퓌쉬전송 완료 수정 및 토너먼트 입력 개발
 */

//수정시에 토너먼트 데이터 초기화 후 다시 토너먼트 배치
if($modify == 1){
	if($tournament == "L"){
		
		$tournament_reset_sql = "update game_score_data set 
                 team_1_score = '',
                 team_2_score = '',
                 team_1_code = '',
                 team_2_code = '',
                 end_game = 'N'
                 where match_code = '$match_code' and gym_code = '$group_code[gym_code]'  and tournament != 'L' and division = '$division' and series = '$series' and series_sub = '$series_sub' ";
		sql_query($tournament_reset_sql);
	}else if($tournament == "T" && $tournament_count == "16"){
		
		$tournament_reset_sql = "update game_score_data set 
                 team_1_score = '',
                 team_2_score = '',
                 team_1_code = '',
                 team_2_code = ''  
                 where match_code = '$match_code' and tournament = 'c' and tournament_count = '0' and division = '$division' and series = '$series' and series_sub = '$series_sub'";
		sql_query($tournament_reset_sql);
				
	}else if($tournament == "T" && $tournament_count == "8"){
		
		$tournament_reset_sql = "update game_score_data set 
                 team_1_score = '',
                 team_2_score = '',
                 team_1_code = '',
                 team_2_code = '',
                 end_game = 'N'
                 where match_code = '$match_code' and tournament = 'c' and tournament_count = '0' and division = '$division' and series = '$series' and series_sub = '$series_sub'";
		sql_query($tournament_reset_sql);
		
	}else if($tournament == "T" && $tournament_count == "4"){
		// $tournament_reset_sql = "update game_score_data set 
                 // team_1_score = '',
                 // team_2_score = '',
                 // team_1_code = '',
                 // team_2_code = '',
                 // end_game = 'N'
                 // where match_code = '$match_code' and tournament = 'T' and tournament_count = '2' and division = '$division' and series = '$series' and series_sub = '$series_sub'";
		// sql_query($tournament_reset_sql);
				
		$tournament_reset_sql = "update game_score_data set 
                 team_1_score = '',
                 team_2_score = '',
                 team_1_code = '',
                 team_2_code = '',
                 end_game = 'N'
                 where match_code = '$match_code' and tournament = 'c' and tournament_count = '0' and division = '$division' and series = '$series' and series_sub = '$series_sub'";
		sql_query($tournament_reset_sql);
				
	}else if($tournament == "T" && $tournament_count == "2"){
						
		$tournament_reset_sql = "update game_score_data set 
                 team_1_score = '',
                 team_2_score = '',
                 team_1_code = '',
                 team_2_code = '',
                 end_game = 'N'  
                 where match_code = '$match_code' and tournament = 'c' and tournament_count = '0' and division = '$division' and series = '$series' and series_sub = '$series_sub'";
		sql_query($tournament_reset_sql);
	}
	
	if($tournament == "L"){
		if($team_1_score_before > $team_2_score_before){
			$sql  = "select * from game_score_data where wr_id = '$wr_id' and match_code = '$match_code'";
			$result = sql_query($sql);
			$r = sql_fetch_array($result);
			$team1 = $r['team_1_code'];
			$update = " update $team_field
		            set 
		            	team_league_point	 = team_league_point - 1,
		                team_total_match_point =  team_total_match_point - '$team_1_score_before',
		                team_total_match_minus_point = team_total_match_minus_point - '$team_2_score_before'     
		            where team_code = '$team1' and match_code = '$match_code'";
			sql_query($update);
			$team2 = $r['team_2_code'];
			$update = " update $team_field
		            set 
		            	team_league_lose_count	 = team_league_lose_count - 1,
		                team_total_match_point = team_total_match_point - '$team_2_score_before',
		                team_total_match_minus_point = team_total_match_minus_point - '$team_1_score_before'     
		            where team_code = '$team2' and match_code = '$match_code'";
			sql_query($update);
		}if($team_1_score_before < $team_2_score_before){
			$sql  = "select * from game_score_data where wr_id = '$wr_id' and match_code = '$match_code'";
			$result = sql_query($sql);
			$r = sql_fetch_array($result);
			
			$team1 = $r['team_1_code'];
			
			$updatedd = " update $team_field
		            set 
		            	team_league_lose_count	 = team_league_lose_count - 1,
		                team_total_match_point = team_total_match_point - '$team_1_score_before',
		                team_total_match_minus_point = team_total_match_minus_point - '$team_2_score_before'     
		            where team_code = '$team1' and match_code = '$match_code'";
			sql_query($updatedd);
			
			$team2 = $r['team_2_code'];
			
			$updateee = " update $team_field
		            set 
		            	team_league_point	 = team_league_point - 1,
		                team_total_match_point = team_total_match_point - '$team_2_score_before',
		                team_total_match_minus_point =  team_total_match_minus_point - '$team_1_score_before'     
		            where team_code = '$team2' and match_code = '$match_code'";
			sql_query($updateee);
		}
	}
}

if($tournament == "L"){
	if($team_1_score > $team_2_score){
		$sql  = "select * from game_score_data where wr_id = '$wr_id' and match_code = '$match_code'";
		$result = sql_query($sql);
		$r = sql_fetch_array($result);
		
		$team1 = $r['team_1_code'];
		$update = " update $team_field
	            set 
	            	team_league_point	 = team_league_point + 1,
	                team_total_match_point =  team_total_match_point + '$team_1_score',
	                team_total_match_minus_point = team_total_match_minus_point + '$team_2_score'     
	            where team_code = '$team1' and match_code = '$match_code'";
		sql_query($update);
		$team2 = $r['team_2_code'];
		$update = " update $team_field
	            set 
	            	team_league_lose_count	 = team_league_lose_count + 1,
	                team_total_match_point = team_total_match_point + '$team_2_score',
	                team_total_match_minus_point = team_total_match_minus_point + '$team_1_score'     
	            where team_code = '$team2' and match_code = '$match_code'";
		sql_query($update);
	}
	
	if($team_1_score < $team_2_score){
		$sql  = "select * from game_score_data where wr_id = '$wr_id' and match_code = '$match_code'";
		$result = sql_query($sql);
		$r = sql_fetch_array($result);
		
		$team1 = $r['team_1_code'];
		
		$update = " update $team_field
	            set 
	            	team_league_lose_count	 = team_league_lose_count + 1,
	                team_total_match_point = team_total_match_point + '$team_1_score',
	                team_total_match_minus_point = team_total_match_minus_point + '$team_2_score'     
	            where team_code = '$team1' and match_code = '$match_code'";
		sql_query($update);

		$team2 = $r['team_2_code'];
		$update = " update $team_field
	            set 
	            	team_league_point	 = team_league_point + 1,
	                team_total_match_point = team_total_match_point + '$team_2_score',
	                team_total_match_minus_point =  team_total_match_minus_point + '$team_1_score'     
	            where team_code = '$team2' and match_code = '$match_code'";
		sql_query($update);
	}
}

//그룹코드 로 game_score_data에 대이터를 검색한다. 모든 데이터가 입력되었다면 해당 그룹네 팀을 검색하여서 그룹내 순위를 결정한다.
if($tournament == "L"){
	$tournament_create = '1';
	
	//현재 등급의 모든 점수가 입력되었는지 검사한다.
	$sql_d = "select count(wr_id) as cnt from game_score_data where and team_1_score = '' and team_2_score = '' and match_code = '$match_code' and group_code = '$group_code[group_code]'";
	echo "$sql_d";
	$result = sql_query($sql_d);
	$cnt = sql_fetch_array($result);
	
	if($cnt['cnt'] == 0){
		
		$group_sql = "select * from group_data where match_code = '$match_code' and code = '$group_code[group_code]'";
		$result = sql_query($group_sql);
		$group_data = sql_fetch_array($result);
		
		//검색된 팀의 승률(winning_rate)과 득실차(gains_losses_point)를 계산한다.
		$group_sqls = "select * from $team_field where match_code = '$match_code' and (team_code = '$group_data[team_1]' OR team_code = '$group_data[team_2]' OR team_code = '$group_data[team_3]') ORDER BY team_league_point desc ,winning_rate desc	,gains_losses_point desc, team_total_match_point DESC";
		$result = sql_query($group_sqls);
		//검색된 순위대로 그룹 랭크를 매긴다.
		while($team = sql_fetch_array($result)){
			
			$win_rate = $team['team_league_point']/($team['team_league_point']+$team['team_league_lose_count']);
			$gains_losses_point = $team['team_total_match_point'] - $team['team_total_match_minus_point'];
			$wr_ids = $team['wr_id'];			
			$sqls = " update team_data
			            set 
			                 winning_rate = '$win_rate',
			                 gains_losses_point = '$gains_losses_point'
			            where wr_id = '$wr_ids' and match_code = '$match_code'";
			sql_query($sqls);
		}
		//그룹내 팀의 등수를 설정하는데 승수, 승률, 득실차, 총 득점수를 기준으로 정렬한다.
		$group_sqls = "select * from $team_field where match_code = '$match_code' and (team_code = '$group_data[team_1]' OR team_code = '$group_data[team_2]' OR team_code = '$group_data[team_3]' OR team_code = '$group_data[team_4]' OR team_code = '$group_data[team_5]') ORDER BY team_league_point desc ,winning_rate desc ,gains_losses_point desc, team_total_match_point DESC";
		$rank_result = sql_query($group_sqls);
		//검색된 순위대로 그룹 랭크를 매긴다.
		$rank = 1;
		//승자승 체크기능 필요
		$chk_array = array();
		while($team = sql_fetch_array($rank_result)){	
			$chk_array[] = $team;
		}

		for($i = 0 ; $i < count($chk_array) ;$i++){
			$wr_ids = $chk_array[$i]['wr_id'];		
			$sqlsss = " update $team_field
			            set 
			                 group_rank = '$rank'
			            where wr_id = '$wr_ids' and match_code = '$match_code'";
			sql_query($sqlsss);
			$rank++;

		}
	}	
}


//모든 그룹에서 종료된었는지 검사한다.
//모든 그룹에서 종료되었을경우 팀성적에서 랭킹 1위와 득실차를 이용하여서 팀 성적을 나열하고 성적이 나쁜 순서대로 부전승에 집어 넣는다.
//end경기인지 확인한다.
$sql = "select count(wr_id) as cnt from game_score_data where end_game = 'N' and gym_code = '$group_code[gym_code]' and tournament = 'L' and match_code = '$match_code' and division = '$division' and series = '$series' and series_sub = '$series_sub'";
echo $sql;
$result = sql_query($sql);
$cnt = sql_fetch_array($result);
//그룹에 모든 경기가 종료되었다면
if($cnt['cnt'] == 0){

	$group_cnt_sql = "select count(wr_id) as cnt from group_data where match_code = '$match_code' and  division = '$division' and series = '$series' and series_sub = '$series_sub'";
	$result = sql_query($group_cnt_sql);
	$group_data = sql_fetch_array($result);
	$group_cnt = $group_data['cnt']*2;
	$gravity = 1;
	if($division == "단체전")
		$gravity = 3;
	//해당 그룹이 2개만 있을 경우 각 그룹에 1위와 2위를 4강전에 올린다.
	
	if($group_cnt > 32 && $group_cnt <= 64){
		//64강 토너먼트 제
        $sql = "select count(wr_id) as cnt from game_score_data where tournament = 'T' and tournament_count = '32' and gym_code = '$group_code[gym_code]'  and end_game = 'N' and match_code = '$match_code' and division = '$division' and series = '$series' and series_sub = '$series_sub'";
        $result = sql_query($sql);
        $r = sql_fetch_array($result);
		print_r($r);
		
		$sql = "select count(wr_id) as cnt from game_score_data where tournament = 'T' and tournament_count = '32' and gym_code = '$group_code[gym_code]'  and match_code = '$match_code' and division = '$division' and series = '$series' and series_sub = '$series_sub'";
        $result = sql_query($sql);
        $rs = sql_fetch_array($result);
		
		
        //8강전이 종료되었는지 를 검사한다.
        if($r['cnt'] == $rs['cnt']){
			$tournament_create = 'T32';

			
			//현재 경기의 $group_code[gym_id]의 group_id를 불러온다.
			$t_group_sql = "select * from group_data where gym_code = '$group_code[gym_code]' and match_code = '$match_code' and  game_date = '$group_code[game_date]'";
			$t_result = sql_query($t_group_sql);
			while($t_data = sql_fetch_array($t_result)){
				$t_team_lank = "select * from team_data where (team_code = '$t_data[team_1]' or team_code = '$t_data[team_2]' or team_code = '$t_data[team_3]') and match_code = '$match_code'  and group_rank = '1'";
				$t_team_result = sql_query($t_team_lank);
				while($teams = sql_fetch_array($t_team_result)){
				 	$team_array[] = $teams;
				}
			}
			$t_group_sql = "select * from group_data where gym_code = '$group_code[gym_code]' and match_code = '$match_code' and  game_date = '$group_code[game_date]'";
			$t_result = sql_query($t_group_sql);
			while($t_data = sql_fetch_array($t_result)){
				$t_team_lank = "select * from team_data where (team_code = '$t_data[team_1]' or team_code = '$t_data[team_2]' or team_code = '$t_data[team_3]') and match_code = '$match_code'  and group_rank = '2' ";
				$t_team_result = sql_query($t_team_lank);
				while($teams = sql_fetch_array($t_team_result)){
				 	$team_array[] = $teams;
				}
			}
			echo count($team_array);
			//해당 경기장의 ranking 1위와 2위를 불러서 저장한다.
			//해당 경기장의 배정된 64강과 32강을 불러서 배치한다.
			
			//$team_array = get_team_rank($team_field,$match_code,$division,$series,$series_sub);
			
			$sql = "select * from game_score_data where tournament = 'T' and tournament_count = '16' and gym_code = '$group_code[gym_code]' and team_1_score = '' and team_2_score = '' and match_code = '$match_code' and division = '$division' and series = '$series' and series_sub = '$series_sub' group by tournament_array_num order by game_increase desc ";
			$result = sql_query($sql);
			$cnt = 0;
			$group_array_16 = array();
			while($r = sql_fetch_array($result)){
				$group_array_16[$cnt] = $r; 
				$cnt++;
			}
			
			$sql = "select * from game_score_data where tournament = 'T' and tournament_count = '32' and gym_code = '$group_code[gym_code]' and team_1_score = '' and team_2_score = '' and match_code = '$match_code' and division = '$division' and series = '$series' and series_sub = '$series_sub' group by tournament_array_num order by game_increase desc ";
			$result = sql_query($sql);
			$cnt = 0;
			$group_array_32 = array();
			while($r = sql_fetch_array($result)){
				$group_array_32[$cnt] = $r;
				$cnt++;
			}
			 
			//gym_code = 89 일때
			//gym_code = 93 일때 gym_code = 92 일때 
			if($group_code['gym_code'] == 89){
				for($c = 0 ; $c < 22; $c++){
					if($c == 0){
						$team_1_code = $team_array[$c]['team_code'];
							
						$wr_ids = $group_array_16['0']['wr_id'];
						$sql = " update game_score_data
			            set 
			                 team_1_code = '$team_1_code'
			            where wr_id = '$wr_ids' and match_code = '$match_code'";
						sql_query($sql);
					}
					if($c == 1){
						$team_1_code = $team_array[$c]['team_code'];
							
						$wr_ids = $group_array_16['7']['wr_id'];
						$sql = " update game_score_data
			            set 
			                 team_2_code = '$team_1_code'
			            where wr_id = '$wr_ids' and match_code = '$match_code'";
						sql_query($sql);
					}
					if($c == 2){
						$team_1_code = $team_array[$c]['team_code'];
							
						$wr_ids = $group_array_16['4']['wr_id'];
						$sql = " update game_score_data
			            set 
			                 team_1_code = '$team_1_code'
			            where wr_id = '$wr_ids' and match_code = '$match_code'";
						sql_query($sql);
					}
					if($c == 3){
						$team_1_code = $team_array[$c]['team_code'];
							
						$wr_ids = $group_array_16['3']['wr_id'];
						$sql = " update game_score_data
			            set 
			                 team_2_code = '$team_1_code'
			            where wr_id = '$wr_ids' and match_code = '$match_code'";
						sql_query($sql);
					}
					if($c == 4){
						$team_1_code = $team_array[$c]['team_code'];
							
						$wr_ids = $group_array_16['2']['wr_id'];
						$sql = " update game_score_data
			            set 
			                 team_2_code = '$team_1_code'
			            where wr_id = '$wr_ids' and match_code = '$match_code'";
						sql_query($sql);
					}
					if($c == 5){
						$team_1_code = $team_array[$c]['team_code'];
							
						$wr_ids = $group_array_16['5']['wr_id'];
						$sql = " update game_score_data
			            set 
			                 team_1_code = '$team_1_code'
			            where wr_id = '$wr_ids' and match_code = '$match_code'";
						sql_query($sql);
					}
					if($c == 6){
						$team_1_code = $team_array[$c]['team_code'];
							
						$wr_ids = $group_array_16['1']['wr_id'];
						$sql = " update game_score_data
			            set 
			                 team_1_code = '$team_1_code'
			            where wr_id = '$wr_ids' and match_code = '$match_code'";
						sql_query($sql);
					}
					if($c == 7){
						$team_1_code = $team_array[$c]['team_code'];
							
						$wr_ids = $group_array_16['5']['wr_id'];
						$sql = " update game_score_data
			            set 
			                 team_2_code = '$team_1_code'
			            where wr_id = '$wr_ids' and match_code = '$match_code'";
						sql_query($sql);
					}
					if($c == 8){
						$team_1_code = $team_array[$c]['team_code'];
							
						$wr_ids = $group_array_16['2']['wr_id'];
						$sql = " update game_score_data
			            set 
			                 team_1_code = '$team_1_code'
			            where wr_id = '$wr_ids' and match_code = '$match_code'";
						sql_query($sql);
					}
					if($c == 9){
						$team_1_code = $team_array[$c]['team_code'];
							
						$wr_ids = $group_array_16['6']['wr_id'];
						$sql = " update game_score_data
			            set 
			                 team_2_code = '$team_1_code'
			            where wr_id = '$wr_ids' and match_code = '$match_code'";
						sql_query($sql);
					}
					if($c == 10){
						$team_1_code = $team_array[$c]['team_code'];
							
						$wr_ids = $group_array_32['0']['wr_id'];
						$sql = " update game_score_data
			            set 
			                 team_1_code = '$team_1_code'
			            where wr_id = '$wr_ids' and match_code = '$match_code'";
						sql_query($sql);
					}
					if($c == 11){
						$team_1_code = $team_array[$c]['team_code'];
							
						$wr_ids = $group_array_32['5']['wr_id'];
						$sql = " update game_score_data
			            set 
			                 team_1_code = '$team_1_code'
			            where wr_id = '$wr_ids' and match_code = '$match_code'";
						sql_query($sql);
					}
					if($c == 12){
						$team_1_code = $team_array[$c]['team_code'];
							
						$wr_ids = $group_array_32['2']['wr_id'];
						$sql = " update game_score_data
			            set 
			                 team_2_code = '$team_1_code'
			            where wr_id = '$wr_ids' and match_code = '$match_code'";
						sql_query($sql);
					}
					
					if($c == 13){
						$team_1_code = $team_array[$c]['team_code'];
							
						$wr_ids = $group_array_32['1']['wr_id'];
						$sql = " update game_score_data
			            set 
			                 team_2_code = '$team_1_code'
			            where wr_id = '$wr_ids' and match_code = '$match_code'";
						sql_query($sql);
					}
					if($c == 14){
						$team_1_code = $team_array[$c]['team_code'];
							
						$wr_ids = $group_array_32['3']['wr_id'];
						$sql = " update game_score_data
			            set 
			                 team_2_code = '$team_1_code'
			            where wr_id = '$wr_ids' and match_code = '$match_code'";
						sql_query($sql);
					}
					if($c == 15){
						$team_1_code = $team_array[$c]['team_code'];
							
						$wr_ids = $group_array_32['4']['wr_id'];
						$sql = " update game_score_data
			            set 
			                 team_1_code = '$team_1_code'
			            where wr_id = '$wr_ids' and match_code = '$match_code'";
						sql_query($sql);
					}
					if($c == 16){
						$team_1_code = $team_array[$c]['team_code'];
							
						$wr_ids = $group_array_32['2']['wr_id'];
						$sql = " update game_score_data
			            set 
			                 team_1_code = '$team_1_code'
			            where wr_id = '$wr_ids' and match_code = '$match_code'";
						sql_query($sql);
					}
					if($c == 17){
						$team_1_code = $team_array[$c]['team_code'];
							
						$wr_ids = $group_array_32['5']['wr_id'];
						$sql = " update game_score_data
			            set 
			                 team_2_code = '$team_1_code'
			            where wr_id = '$wr_ids' and match_code = '$match_code'";
						sql_query($sql);
					}
					if($c == 18){
						$team_1_code = $team_array[$c]['team_code'];
							
						$wr_ids = $group_array_32['1']['wr_id'];
						$sql = " update game_score_data
			            set 
			                 team_1_code = '$team_1_code'
			            where wr_id = '$wr_ids' and match_code = '$match_code'";
						sql_query($sql);
					}
					if($c == 19){
						$team_1_code = $team_array[$c]['team_code'];
							
						$wr_ids = $group_array_32['3']['wr_id'];
						$sql = " update game_score_data
			            set 
			                 team_1_code = '$team_1_code'
			            where wr_id = '$wr_ids' and match_code = '$match_code'";
						sql_query($sql);
					}
					if($c == 20){
						$team_1_code = $team_array[$c]['team_code'];
							
						$wr_ids = $group_array_32['0']['wr_id'];
						$sql = " update game_score_data
			            set 
			                 team_2_code = '$team_1_code'
			            where wr_id = '$wr_ids' and match_code = '$match_code'";
						sql_query($sql);
					}
					if($c == 21){
						$team_1_code = $team_array[$c]['team_code'];
							
						$wr_ids = $group_array_32['4']['wr_id'];
						$sql = " update game_score_data
			            set 
			                 team_2_code = '$team_1_code'
			            where wr_id = '$wr_ids' and match_code = '$match_code'";
						sql_query($sql);
					}
				}
			}
			if($group_code['gym_code'] == 93 || $group_code['gym_code'] == 92){
				for($c = 0 ; $c < 12; $c++){
					if($c == 0){
						$team_1_code = $team_array[$c]['team_code'];
							
						$wr_ids = $group_array_16['0']['wr_id'];
						$sql = " update game_score_data
			            set 
			                 team_1_code = '$team_1_code'
			            where wr_id = '$wr_ids' and match_code = '$match_code'";
						sql_query($sql);
					}
					if($c == 1){
						$team_1_code = $team_array[$c]['team_code'];
							
						$wr_ids = $group_array_16['3']['wr_id'];
						$sql = " update game_score_data
			            set 
			                 team_2_code = '$team_1_code'
			            where wr_id = '$wr_ids' and match_code = '$match_code'";
						sql_query($sql);
					}
					if($c == 2){
						$team_1_code = $team_array[$c]['team_code'];
							
						$wr_ids = $group_array_16['1']['wr_id'];
						$sql = " update game_score_data
			            set 
			                 team_2_code = '$team_1_code'
			            where wr_id = '$wr_ids' and match_code = '$match_code'";
						sql_query($sql);
					}
					if($c == 3){
						$team_1_code = $team_array[$c]['team_code'];
							
						$wr_ids = $group_array_16['2']['wr_id'];
						$sql = " update game_score_data
			            set 
			                 team_1_code = '$team_1_code'
			            where wr_id = '$wr_ids' and match_code = '$match_code'";
						sql_query($sql);
					}
					if($c == 4){
						$team_1_code = $team_array[$c]['team_code'];
							
						$wr_ids = $group_array_32['0']['wr_id'];
						$sql = " update game_score_data
			            set 
			                 team_2_code = '$team_1_code'
			            where wr_id = '$wr_ids' and match_code = '$match_code'";
						sql_query($sql);
					}
					if($c == 5){
						$team_1_code = $team_array[$c]['team_code'];
							
						$wr_ids = $group_array_32['3']['wr_id'];
						$sql = " update game_score_data
			            set 
			                 team_1_code = '$team_1_code'
			            where wr_id = '$wr_ids' and match_code = '$match_code'";
						sql_query($sql);
					}
					if($c == 6){
						$team_1_code = $team_array[$c]['team_code'];
							
						$wr_ids = $group_array_32['3']['wr_id'];
						$sql = " update game_score_data
			            set 
			                 team_2_code = '$team_1_code'
			            where wr_id = '$wr_ids' and match_code = '$match_code'";
						sql_query($sql);
					}
					if($c == 7){
						$team_1_code = $team_array[$c]['team_code'];
							
						$wr_ids = $group_array_32['0']['wr_id'];
						$sql = " update game_score_data
			            set 
			                 team_1_code = '$team_1_code'
			            where wr_id = '$wr_ids' and match_code = '$match_code'";
						sql_query($sql);
					}
					if($c == 8){
						$team_1_code = $team_array[$c]['team_code'];
							
						$wr_ids = $group_array_32['2']['wr_id'];
						$sql = " update game_score_data
			            set 
			                 team_2_code = '$team_1_code'
			            where wr_id = '$wr_ids' and match_code = '$match_code'";
						sql_query($sql);
					}
					if($c == 9){
						$team_1_code = $team_array[$c]['team_code'];
							
						$wr_ids = $group_array_32['1']['wr_id'];
						$sql = " update game_score_data
			            set 
			                 team_1_code = '$team_1_code'
			            where wr_id = '$wr_ids' and match_code = '$match_code'";
						sql_query($sql);
					}
					if($c == 10){
						$team_1_code = $team_array[$c]['team_code'];
							
						$wr_ids = $group_array_32['2']['wr_id'];
						$sql = " update game_score_data
			            set 
			                 team_1_code = '$team_1_code'
			            where wr_id = '$wr_ids' and match_code = '$match_code'";
						sql_query($sql);
					}
					if($c == 11){
						$team_1_code = $team_array[$c]['team_code'];
							
						$wr_ids = $group_array_32['1']['wr_id'];
						$sql = " update game_score_data
			            set 
			                 team_2_code = '$team_1_code'
			            where wr_id = '$wr_ids' and match_code = '$match_code'";
						sql_query($sql);
					}
					
				}
			}
		}else if($r['cnt'] == 0){
			//8강전이 종료되었다면 16강전에 승자를 8강전에 배치한다.
			$count_32 = "select count(wr_id) as cnt from game_score_data where tournament = 'T' and tournament_count = '32' and gym_code = '$group_code[gym_code]'  and match_code = '$match_code' and division = '$division' and series = '$series' and series_sub = '$series_sub'";
            $result_count_32 = sql_query($count_32);
            $count_32 = sql_fetch_array($result_count_32);

            $sql_32 = "select count(wr_id) as cnt from game_score_data where tournament = 'T' and tournament_count = '32' and gym_code = '$group_code[gym_code]'  and end_game = 'Y' and match_code = '$match_code' and division = '$division' and series = '$series' and series_sub = '$series_sub'";
            $result_32 = sql_query($sql_32);
            $r_32 = sql_fetch_array($result_32);

            $sql = "select count(wr_id) as cnt from game_score_data where tournament = 'T' and tournament_count = '16' and gym_code = '$group_code[gym_code]'  and end_game = 'N' and match_code = '$match_code' and division = '$division' and series = '$series' and series_sub = '$series_sub'";

            $result = sql_query($sql);
            $r = sql_fetch_array($result);
            $count = ($group_cnt-16);
			//if($r['cnt'] == 8*$gravity && $r_32 == 0){
            if($r['cnt'] > 0 && $r_32['cnt'] == $count_32['cnt']){
				$tournament_create = 'T16';
				//검색된 순위대로 그룹 랭크를 매긴다.
				$sql = "select * from game_score_data where tournament = 'T' and tournament_count = '32' and gym_code = '$group_code[gym_code]'  and match_code = '$match_code' and division = '$division' and series = '$series' and series_sub = '$series_sub' order by game_increase desc";
				$result = sql_query($sql);
				
				$sqls = "select * from game_score_data where tournament = 'T' and tournament_count = '16' and gym_code = '$group_code[gym_code]'  and (team_1_code = '' or team_2_code = '') and match_code = '$match_code' and division = '$division' and series = '$series' and series_sub = '$series_sub' order by game_increase desc";
				echo $sqls;
				$results = sql_query($sqls);
// 				
				
				$cnt = 0;
				$team_array = array();
				$team_1_code = 0;
				$evennumber = 0;
				$r = sql_fetch_array($results);
				while($team = sql_fetch_array($result)){
				 
					if($team['team_1_score'] > $team['team_2_score']){
						$team_1_code = $team['team_1_code'];
					}else{
						$team_1_code = $team['team_2_code'];
					}
					echo "1tean".$r['team_1_code'] ."<br>";
					echo "2tean".$r['team_2_code'] ."<br>";
					echo $evennumber ."<br>";
					
					if($r['team_1_code'] == ""){
						$sql1 = " update game_score_data
			            set 
			                 team_1_code = '$team_1_code'
			            where wr_id = '$r[wr_id]' and match_code = '$match_code'";
			            echo $sql1."<br>";
			            
						sql_query($sql1);
												$r = sql_fetch_array($results);
						$evennumber = 1;
					}
					else if($r['team_2_code'] == ""){
						$sql2 = " update game_score_data
			            set 
			                 team_2_code = '$team_1_code'
			            where wr_id = '$r[wr_id]' and match_code = '$match_code'";
			            echo $sql2."<br>";
						sql_query($sql2);
						$r = sql_fetch_array($results);
						$evennumber = 0;
					}
					$cnt++;
					
				}
			}else if($r['cnt'] == 0){
                $sql = "select count(wr_id) as cnt from game_score_data where tournament = 'T' and gym_code = '$group_code[gym_code]'  and tournament_count = '8' and end_game = 'N' and match_code = '$match_code' and division = '$division' and series = '$series' and series_sub = '$series_sub'";
                $result = sql_query($sql);
                $r = sql_fetch_array($result);
                
                $sql = "select count(wr_id) as cnt from game_score_data where tournament = 'T' and gym_code = '$group_code[gym_code]'  and tournament_count = '8' and match_code = '$match_code' and division = '$division' and series = '$series' and series_sub = '$series_sub'";
                $result = sql_query($sql);
                $rs = sql_fetch_array($result);
                
                $count = ($group_cnt-16);
                if($r['cnt'] == $rs['cnt']){
					$tournament_create = 'T8';
					//검색된 순위대로 그룹 랭크를 매긴다.
					$sql = "select * from game_score_data where tournament = 'T' and tournament_count = '16' and gym_code = '$group_code[gym_code]'  and match_code = '$match_code' and division = '$division' and series = '$series' and series_sub = '$series_sub' group by tournament_array_num  order by game_increase desc";
					$result = sql_query($sql);
					
					$sqls = "select * from game_score_data where tournament = 'T' and tournament_count = '8' and gym_code = '$group_code[gym_code]'  and team_1_score = '' and team_2_score = '' and match_code = '$match_code' and division = '$division' and series = '$series' and series_sub = '$series_sub' group by tournament_array_num  order by game_increase desc";
					$results = sql_query($sqls);			
					$cnt = 0;
					$team_array = array();
					$team_1_code = 0;
					while($team = sql_fetch_array($result)){
						
						$team_1_code = team_win_code($team);
						
						if($cnt == 0){
							$r = sql_fetch_array($results);
							if($division == "단체전"){
								$tournament_array_num = $r['tournament_array_num'];
								$sql = " update game_score_data
						            set 
						                 team_1_code = '$team_1_code'
						            where tournament_array_num = '$tournament_array_num' and match_code = '$match_code'";
								sql_query($sql);
							}else{
								$wr_ids = $r['wr_id'];
								$sql = " update game_score_data
					            set 
					                 team_1_code = '$team_1_code'
					            where wr_id = '$wr_ids' and match_code = '$match_code'";
					            
								sql_query($sql);
							}
						}
						if($cnt == 1){
							if($division == "단체전"){
								$tournament_array_num = $r['tournament_array_num'];
								$sql = " update game_score_data
						            set 
						                 team_2_code = '$team_1_code'
						            where tournament_array_num = '$tournament_array_num' and match_code = '$match_code'";
								sql_query($sql);
							}else{
								$wr_ids = $r['wr_id'];
								$sql = " update game_score_data
					            set 
					                 team_2_code = '$team_1_code'
					            where wr_id = '$wr_ids' and match_code = '$match_code'";
					            
								sql_query($sql);
							}
						}
						if($cnt == 2){
							$r = sql_fetch_array($results);
							if($division == "단체전"){
								$tournament_array_num = $r['tournament_array_num'];
								$sql = " update game_score_data
						            set 
						                 team_1_code = '$team_1_code'
						            where tournament_array_num = '$tournament_array_num' and match_code = '$match_code'";
								sql_query($sql);
							}else{
								$wr_ids = $r['wr_id'];
								$sql = " update game_score_data
					            set 
					                 team_1_code = '$team_1_code'
					            where wr_id = '$r[wr_id]' and match_code = '$match_code'";
					            
								sql_query($sql);
							}
						}
						if($cnt == 3){
							if($division == "단체전"){
								$tournament_array_num = $r['tournament_array_num'];
								$sql = " update game_score_data
						            set 
						                 team_2_code = '$team_1_code'
						            where tournament_array_num = '$tournament_array_num' and match_code = '$match_code'";
								sql_query($sql);
							}else{
								$wr_ids = $r['wr_id'];
								$sql = " update game_score_data
					            set 
					                 team_2_code = '$team_1_code'
					            where wr_id = '$r[wr_id]' and match_code = '$match_code'";
					            
								sql_query($sql);
							}
						}
						if($cnt == 4){
							$r = sql_fetch_array($results);
							if($division == "단체전"){
								$tournament_array_num = $r['tournament_array_num'];
								$sql = " update game_score_data
						            set 
						                 team_1_code = '$team_1_code'
						            where tournament_array_num = '$tournament_array_num' and match_code = '$match_code'";
								sql_query($sql);
							}else{
								$wr_ids = $r['wr_id'];
								$sql = " update game_score_data
					            set 
					                 team_1_code = '$team_1_code'
					            where wr_id = '$r[wr_id]' and match_code = '$match_code'";
					            
								sql_query($sql);
							}
						}
						if($cnt == 5){
							if($division == "단체전"){
								$tournament_array_num = $r['tournament_array_num'];
								$sql = " update game_score_data
						            set 
						                 team_2_code = '$team_1_code'
						            where tournament_array_num = '$tournament_array_num' and match_code = '$match_code'";
								sql_query($sql);
							}else{
								$wr_ids = $r['wr_id'];
								$sql = " update game_score_data
					            set 
					                 team_2_code = '$team_1_code'
					            where wr_id = '$r[wr_id]' and match_code = '$match_code'";
					            
								sql_query($sql);
							}
						}
						if($cnt == 6){
							$r = sql_fetch_array($results);
							if($division == "단체전"){
								$tournament_array_num = $r['tournament_array_num'];
								$sql = " update game_score_data
						            set 
						                 team_1_code = '$team_1_code'
						            where tournament_array_num = '$tournament_array_num' and match_code = '$match_code'";
								sql_query($sql);
							}else{
								$wr_ids = $r['wr_id'];
								$sql = " update game_score_data
					            set 
					                 team_1_code = '$team_1_code'
					            where wr_id = '$r[wr_id]' and match_code = '$match_code'";
					            
								sql_query($sql);
							}
						}
						if($cnt == 7){
							if($division == "단체전"){
								$tournament_array_num = $r['tournament_array_num'];
								$sql = " update game_score_data
						            set 
						                 team_2_code = '$team_1_code'
						            where tournament_array_num = '$tournament_array_num' and match_code = '$match_code'";
								sql_query($sql);
							}else{
								$wr_ids = $r['wr_id'];
								$sql = " update game_score_data
					            set 
					                 team_2_code = '$team_1_code'
					            where wr_id = '$r[wr_id]' and match_code = '$match_code'";
					            
								sql_query($sql);
							}
						}
						if($cnt == 8){
							$r = sql_fetch_array($results);
							if($division == "단체전"){
								$tournament_array_num = $r['tournament_array_num'];
								$sql = " update game_score_data
						            set 
						                 team_1_code = '$team_1_code'
						            where tournament_array_num = '$tournament_array_num' and match_code = '$match_code'";
								sql_query($sql);
							}else{
								$wr_ids = $r['wr_id'];
								$sql = " update game_score_data
					            set 
					                 team_1_code = '$team_1_code'
					            where wr_id = '$wr_ids' and match_code = '$match_code'";
					            
								sql_query($sql);
							}
						}
						if($cnt == 9){
							if($division == "단체전"){
								$tournament_array_num = $r['tournament_array_num'];
								$sql = " update game_score_data
						            set 
						                 team_2_code = '$team_1_code'
						            where tournament_array_num = '$tournament_array_num' and match_code = '$match_code'";
								sql_query($sql);
							}else{
								$wr_ids = $r['wr_id'];
								$sql = " update game_score_data
					            set 
					                 team_2_code = '$team_1_code'
					            where wr_id = '$wr_ids' and match_code = '$match_code'";
					            
								sql_query($sql);
							}
						}
						if($cnt == 10){
							$r = sql_fetch_array($results);
							if($division == "단체전"){
								$tournament_array_num = $r['tournament_array_num'];
								$sql = " update game_score_data
						            set 
						                 team_1_code = '$team_1_code'
						            where tournament_array_num = '$tournament_array_num' and match_code = '$match_code'";
								sql_query($sql);
							}else{
								$wr_ids = $r['wr_id'];
								$sql = " update game_score_data
					            set 
					                 team_1_code = '$team_1_code'
					            where wr_id = '$r[wr_id]' and match_code = '$match_code'";
					            
								sql_query($sql);
							}
						}
						if($cnt == 11){
							if($division == "단체전"){
								$tournament_array_num = $r['tournament_array_num'];
								$sql = " update game_score_data
						            set 
						                 team_2_code = '$team_1_code'
						            where tournament_array_num = '$tournament_array_num' and match_code = '$match_code'";
								sql_query($sql);
							}else{
								$wr_ids = $r['wr_id'];
								$sql = " update game_score_data
					            set 
					                 team_2_code = '$team_1_code'
					            where wr_id = '$r[wr_id]' and match_code = '$match_code'";
					            
								sql_query($sql);
							}
						}
						if($cnt == 12){
							$r = sql_fetch_array($results);
							if($division == "단체전"){
								$tournament_array_num = $r['tournament_array_num'];
								$sql = " update game_score_data
						            set 
						                 team_1_code = '$team_1_code'
						            where tournament_array_num = '$tournament_array_num' and match_code = '$match_code'";
								sql_query($sql);
							}else{
								$wr_ids = $r['wr_id'];
								$sql = " update game_score_data
					            set 
					                 team_1_code = '$team_1_code'
					            where wr_id = '$r[wr_id]' and match_code = '$match_code'";
					            
								sql_query($sql);
							}
						}
						if($cnt == 13){
							if($division == "단체전"){
								$tournament_array_num = $r['tournament_array_num'];
								$sql = " update game_score_data
						            set 
						                 team_2_code = '$team_1_code'
						            where tournament_array_num = '$tournament_array_num' and match_code = '$match_code'";
								sql_query($sql);
							}else{
								$wr_ids = $r['wr_id'];
								$sql = " update game_score_data
					            set 
					                 team_2_code = '$team_1_code'
					            where wr_id = '$r[wr_id]' and match_code = '$match_code'";
					            
								sql_query($sql);
							}
						}
						if($cnt == 14){
							$r = sql_fetch_array($results);
							if($division == "단체전"){
								$tournament_array_num = $r['tournament_array_num'];
								$sql = " update game_score_data
						            set 
						                 team_1_code = '$team_1_code'
						            where tournament_array_num = '$tournament_array_num' and match_code = '$match_code'";
								sql_query($sql);
							}else{
								$wr_ids = $r['wr_id'];
								$sql = " update game_score_data
					            set 
					                 team_1_code = '$team_1_code'
					            where wr_id = '$r[wr_id]' and match_code = '$match_code'";
					            
								sql_query($sql);
							}
						}
						if($cnt == 15){
							if($division == "단체전"){
								$tournament_array_num = $r['tournament_array_num'];
								$sql = " update game_score_data
						            set 
						                 team_2_code = '$team_1_code'
						            where tournament_array_num = '$tournament_array_num' and match_code = '$match_code'";
								sql_query($sql);
							}else{
								$wr_ids = $r['wr_id'];
								$sql = " update game_score_data
					            set 
					                 team_2_code = '$team_1_code'
					            where wr_id = '$r[wr_id]' and match_code = '$match_code'";
					            
								sql_query($sql);
							}
						}
						echo $sql."<br>";
						$cnt++;
					}	
				}
				else if($r['cnt'] == 0){
					$sql = "select count(wr_id) as cnt from game_score_data where tournament = 'T' and tournament_count = '4' and gym_code = '$group_code[gym_code]'  and end_game = 'N' and match_code = '$match_code' and division = '$division' and series = '$series' and series_sub = '$series_sub'";
	                $result = sql_query($sql);
	                $r = sql_fetch_array($result);
					
					$sql = "select count(wr_id) as cnt from game_score_data where tournament = 'T' and tournament_count = '4' and gym_code = '$group_code[gym_code]' and match_code = '$match_code' and division = '$division' and series = '$series' and series_sub = '$series_sub'";
	                $result = sql_query($sql);
	                $rs = sql_fetch_array($result);
					
	                $count = ($group_cnt-8);
	                if($r['cnt'] == $rs['cnt']){
						$tournament_create = 'T8';
						//검색된 순위대로 그룹 랭크를 매긴다.
						$sql = "select * from game_score_data where tournament = 'T' and tournament_count = '8' and gym_code = '$group_code[gym_code]'  and match_code = '$match_code' and division = '$division' and series = '$series' and series_sub = '$series_sub' group by tournament_array_num  order by game_increase desc";
						$result = sql_query($sql);
						
						$sqls = "select * from game_score_data where tournament = 'T' and tournament_count = '4' and gym_code = '$group_code[gym_code]'  and team_1_score = '' and team_2_score = '' and match_code = '$match_code' and division = '$division' and series = '$series' and series_sub = '$series_sub' group by tournament_array_num  order by game_increase desc";
						$results = sql_query($sqls);			
						$cnt = 0;
						$team_array = array();
						$team_1_code = 0;
						while($team = sql_fetch_array($result)){
							
							$team_1_code = team_win_code($team);
							
							if($cnt == 0){
								$r = sql_fetch_array($results);
								if($division == "단체전"){
									$tournament_array_num = $r['tournament_array_num'];
									$sql = " update game_score_data
							            set 
							                 team_1_code = '$team_1_code'
							            where tournament_array_num = '$tournament_array_num' and match_code = '$match_code'";
									sql_query($sql);
								}else{
									$wr_ids = $r['wr_id'];
									$sql = " update game_score_data
						            set 
						                 team_1_code = '$team_1_code'
						            where wr_id = '$wr_ids' and match_code = '$match_code'";
						            
									sql_query($sql);
								}
							}
							if($cnt == 1){
								if($division == "단체전"){
									$tournament_array_num = $r['tournament_array_num'];
									$sql = " update game_score_data
							            set 
							                 team_2_code = '$team_1_code'
							            where tournament_array_num = '$tournament_array_num' and match_code = '$match_code'";
									sql_query($sql);
								}else{
									$wr_ids = $r['wr_id'];
									$sql = " update game_score_data
						            set 
						                 team_2_code = '$team_1_code'
						            where wr_id = '$wr_ids' and match_code = '$match_code'";
						            
									sql_query($sql);
								}
							}
							if($cnt == 2){
								$r = sql_fetch_array($results);
								if($division == "단체전"){
									$tournament_array_num = $r['tournament_array_num'];
									$sql = " update game_score_data
							            set 
							                 team_1_code = '$team_1_code'
							            where tournament_array_num = '$tournament_array_num' and match_code = '$match_code'";
									sql_query($sql);
								}else{
									$wr_ids = $r['wr_id'];
									$sql = " update game_score_data
						            set 
						                 team_1_code = '$team_1_code'
						            where wr_id = '$r[wr_id]' and match_code = '$match_code'";
						            
									sql_query($sql);
								}
							}
							if($cnt == 3){
								if($division == "단체전"){
									$tournament_array_num = $r['tournament_array_num'];
									$sql = " update game_score_data
							            set 
							                 team_2_code = '$team_1_code'
							            where tournament_array_num = '$tournament_array_num' and match_code = '$match_code'";
									sql_query($sql);
								}else{
									$wr_ids = $r['wr_id'];
									$sql = " update game_score_data
						            set 
						                 team_2_code = '$team_1_code'
						            where wr_id = '$r[wr_id]' and match_code = '$match_code'";
						            
									sql_query($sql);
								}
							}
							if($cnt == 4){
								$r = sql_fetch_array($results);
								if($division == "단체전"){
									$tournament_array_num = $r['tournament_array_num'];
									$sql = " update game_score_data
							            set 
							                 team_1_code = '$team_1_code'
							            where tournament_array_num = '$tournament_array_num' and match_code = '$match_code'";
									sql_query($sql);
								}else{
									$wr_ids = $r['wr_id'];
									$sql = " update game_score_data
						            set 
						                 team_1_code = '$team_1_code'
						            where wr_id = '$r[wr_id]' and match_code = '$match_code'";
						            
									sql_query($sql);
								}
							}
							if($cnt == 5){
								if($division == "단체전"){
									$tournament_array_num = $r['tournament_array_num'];
									$sql = " update game_score_data
							            set 
							                 team_2_code = '$team_1_code'
							            where tournament_array_num = '$tournament_array_num' and match_code = '$match_code'";
									sql_query($sql);
								}else{
									$wr_ids = $r['wr_id'];
									$sql = " update game_score_data
						            set 
						                 team_2_code = '$team_1_code'
						            where wr_id = '$r[wr_id]' and match_code = '$match_code'";
						            
									sql_query($sql);
								}
							}
							if($cnt == 6){
								$r = sql_fetch_array($results);
								if($division == "단체전"){
									$tournament_array_num = $r['tournament_array_num'];
									$sql = " update game_score_data
							            set 
							                 team_1_code = '$team_1_code'
							            where tournament_array_num = '$tournament_array_num' and match_code = '$match_code'";
									sql_query($sql);
								}else{
									$wr_ids = $r['wr_id'];
									$sql = " update game_score_data
						            set 
						                 team_1_code = '$team_1_code'
						            where wr_id = '$r[wr_id]' and match_code = '$match_code'";
						            
									sql_query($sql);
								}
							}
							if($cnt == 7){
								if($division == "단체전"){
									$tournament_array_num = $r['tournament_array_num'];
									$sql = " update game_score_data
							            set 
							                 team_2_code = '$team_1_code'
							            where tournament_array_num = '$tournament_array_num' and match_code = '$match_code'";
									sql_query($sql);
								}else{
									$wr_ids = $r['wr_id'];
									$sql = " update game_score_data
						            set 
						                 team_2_code = '$team_1_code'
						            where wr_id = '$r[wr_id]' and match_code = '$match_code'";
						            
									sql_query($sql);
								}
							}
							echo $sql."<br>";
							$cnt++;
						}
					}else if($r['cnt'] == 0){
						$sql = "select count(wr_id) as cnt from game_score_data where tournament = 'T' and tournament_count = '4' and end_game = 'N' and match_code = '$match_code' and division = '$division' and series = '$series' and series_sub = '$series_sub'";
		                $result = sql_query($sql);
		                $r = sql_fetch_array($result);
						if($r['cnt'] == 0){
		  		            $sql = "select count(wr_id) as cnt from game_score_data where tournament = 'T' and tournament_count = '2' and end_game = 'N' and match_code = '$match_code' and division = '$division' and series = '$series' and series_sub = '$series_sub'";
		                    $result = sql_query($sql);
		                    $r = sql_fetch_array($result);
		                    $count = ($group_cnt-4);
		                    if($r['cnt'] == 2*$gravity){
								$tournament_create = 'T4';
								//검색된 순위대로 그룹 랭크를 매긴다.
								$sql = "select * from game_score_data where tournament = 'T' and tournament_count = '4' and match_code = '$match_code' and division = '$division' and series = '$series' and series_sub = '$series_sub' group by tournament_array_num";
								$result = sql_query($sql);
								
								$sqls = "select * from game_score_data where tournament = 'T' and tournament_count = '2' and team_1_score = '' and team_2_score = '' and match_code = '$match_code' and division = '$division' and series = '$series' and series_sub = '$series_sub' group by tournament_array_num";
								$results = sql_query($sqls);
				// 				
								$cnt = 0;
								$team_array = array();
								$team_1_code = 0;
								while($team = sql_fetch_array($result)){
									// if($team['team_1_score'] > $team['team_2_score']){
										// $team_1_code = $team['team_1_code'];
									// }else{
										// $team_1_code = $team['team_2_code'];
									// }
									$team_1_code = team_win_code($team);
									echo $team_1_code."<br>";
									if($cnt == 0){
										$r = sql_fetch_array($results);
										
										if($division == "단체전"){
											$tournament_array_num = $r['tournament_array_num'];
											$sql = " update game_score_data
									            set 
									                 team_1_code = '$team_1_code'
									            where tournament_array_num = '$tournament_array_num' and match_code = '$match_code'";
											sql_query($sql);
										}else{
											$wr_ids = $r['wr_id'];
											$sql = " update game_score_data
								            set 
								                 team_1_code = '$team_1_code'
								            where wr_id = '$r[wr_id]' and match_code = '$match_code'";
								            
											sql_query($sql);
										}
									}
									if($cnt == 1){
										if($division == "단체전"){
											$tournament_array_num = $r['tournament_array_num'];
											$sql = " update game_score_data
									            set 
									                 team_2_code = '$team_1_code'
									            where tournament_array_num = '$tournament_array_num' and match_code = '$match_code'";
											sql_query($sql);
										}else{
											$wr_ids = $r['wr_id'];
											$sql = " update game_score_data
								            set 
								                 team_2_code = '$team_1_code'
								            where wr_id = '$r[wr_id]' and match_code = '$match_code'";
								            
											sql_query($sql);
										}
									}
									if($cnt == 2){
										$r = sql_fetch_array($results);
										if($division == "단체전"){
											$tournament_array_num = $r['tournament_array_num'];
											$sql = " update game_score_data
									            set 
									                 team_1_code = '$team_1_code'
									            where tournament_array_num = '$tournament_array_num' and match_code = '$match_code'";
											sql_query($sql);
										}else{
											$wr_ids = $r['wr_id'];
											$sql = " update game_score_data
								            set 
								                 team_1_code = '$team_1_code'
								            where wr_id = '$r[wr_id]' and match_code = '$match_code'";
								            
											sql_query($sql);
										}
									}
									if($cnt == 3){
										if($division == "단체전"){
											$tournament_array_num = $r['tournament_array_num'];
											$sql = " update game_score_data
									            set 
									                 team_2_code = '$team_1_code'
									            where tournament_array_num = '$tournament_array_num' and match_code = '$match_code'";
											sql_query($sql);
										}else{
											$wr_ids = $r['wr_id'];
											$sql = " update game_score_data
								            set 
								                 team_2_code = '$team_1_code'
								            where wr_id = '$r[wr_id]' and match_code = '$match_code'";
								            
											sql_query($sql);
										}
									}
									echo $sql."<br>";
									$cnt++;
								}
							}else{
		                        //결승전을 넣는다.
								$tournament_create = set_championship($match_code,$division,$series,$series_sub);
		    				}	
						}	
					}
				}
			}
		}
	}
}

$rows[0]['result'] = "success";		
$rows[0]['wr_id']  = "$wr_id";		
$rows[0]['team_1_score'] = "$team_1_score";
$rows[0]['team_2_score'] = "$team_2_score";
$rows[0]['tournament'] 	 = "$tournament";
$rows[0]['tournament_count'] 	= "$tournament_count";
$rows[0]['tournament_create'] 	= "$tournament_create";
$rows[0]['division'] 	= "$division";
$rows[0]['series'] 		= "$series";
$rows[0]['series_sub'] 	= "$series_sub";


$rows[0]['sql'] = "$sqlsssdd";
echo json_encode($rows);

//goto_url($require_url);

?>
<!-- <script>
	opener.parent.location.reload();
	window.close();
</script> -->