<?php
include_once('../common.php');
if (!defined('_GNUBOARD_')) exit; // 개별 페이지 접근 불가
include_once(G5_AOS_PATH.'/head.php');
$menu_cate2 ='management';
$menu_cate3 ='3';
?>

<!-- sub nav -->
<?php include_once('./app_sub_nav.php'); ?>
<!-- end sub nav -->
<?php
	$match_sql = "select * from match_data";
	$match_result = sql_query($match_sql);
?>

<?php

	// $gym_sql = "select gym_data.*, date_format(gym_data.wr_datetime, '%Y.%m.%d') as format_wr_datetime
	// ,use_court
	// from gym_data join match_data join match_gym_data
	// on gym_data.wr_id = match_gym_data.gym_id and match_gym_data.match_id = match_data.code
	// where match_data.code = '{$c}'";
	//
	// $gym = sql_query($gym_sql);
	// $date_data;
	// $gym_data;
	//
	//
	// while ($row = sql_fetch_array($gym)) {
	// 	$date_data[$row['format_wr_datetime']] = $row;
	// }
	$mb_id = get_session('ss_mb_id');
	if($is_admin){
		$match_sql = "select * from match_data where 1 order by app_visible desc , date1 desc";
	}else{
		$match_sql = "select * from match_data where code in
								(SELECT match_id FROM match_gym_data
								where  (charger1 = '{$mb_id}' or charger2 = '{$mb_id}'
								or charger3 = '{$mb_id}' or charger4 = '{$mb_id}'
								or charger5 = '{$mb_id}') group by match_id)
							and app_visible = 2
							order by date1 desc";
	}
	$match_result = sql_query($match_sql);
	// print_r($match_sql);
 ?>
<!-- Contents Area -->
<div class="pop_container">
	<?php if(false && ! $competition['opening_date']){?>
		<div class="empty_waiting">
			<div class="img_area">
				<img src="<?php echo G5_IMG_URL.'/common/intro_logo1.png';?>">
			</div>
			<div class="cmt ani02">
				경기가 준비중입니다.
			</div>
		</div>
	<?php }else {?>

		<section>

			<div class="pop_hd sound_only">
				대회선택
			</div>
			<div class="content">
				<select class="full-width form-control" id="match_list" class="" name="m">
					<option value="">경기를 선택해주세요</option>
					<?php
						while($row = sql_fetch_array($match_result)){
							if($c == '') {$c = $row['code'];}
							?>
							<option <?=$c == $row['code'] ? 'selected="selected"' : ""?>
								value="<?=$row['code']?>">
								<?php
									if($is_admin){
										if($row['app_visible'] == 2){
											print "(공개)&nbsp;&nbsp;&nbsp;";
										}else{
											print "(비공개)";
										}
									}
								 ?>
								<?=$row['wr_name']?>

							</option>
						<?php }?>
				</select>


				<script>$('#match_list').change(function(){window.location.href='?c='+$(this).val();});</script>

				<div class="pop_hd sound_only">
					<div class="tit">
						날짜 선택
					</div>
				</div>



				<br />
				<select class="full-width form-control" id="gym">
					<option value="">경기장 선택</option>
					<?php
						$sql = "select *,b.gym_name from match_gym_data as a
						inner join gym_data as b
						where 1
						and match_id = '{$c}'
						and a.gym_id = b.wr_id ";
						if(!$is_admin){
							$sql .=" and (a.charger1 ='$mb_id' or a.charger2 ='$mb_id'
							or a.charger3 ='$mb_id' or a.charger4 ='$mb_id'
							or a.charger5 ='$mb_id') ";
						}
						$sql .=" order by application_period desc ";
						$result = sql_query($sql);
						print $g;
						while( $value = sql_fetch_array($result)){
							if($g == ''){$g = $value['wr_id'] ; print '444444';}
					?>
							<option value="<?php echo $value['wr_id']?>"
								<?php echo  $g == $value['wr_id'] ? "selected=\"selected\"" :""?>>
								<?php echo $value['gym_name'];?></option>
					<?php


					}?>

				</select>
				<br />
				<!-- 탭-코트-->
				<div class="pop_hd sound_only">
					<div class="tit"></div>
				</div>
				<ul class="btn-list list2" style="padding:4px 0px">
					<?php
						// $sql = "select * from game_score_data where match_code = '$c' and tournament = 'T' group by series_sub ";
						// $result = sql_query($sql);
						// print $sql;
						$sql = "select * from series_data
						where match_code = '{$c}'";
						$result = sql_query($sql, true);
						// print $sql;
						while( $value = sql_fetch_array($result)){
								if($d == ''){$d = $value['division'];
													$s = $value['series'];
													$ss = $value['series_sub'];
								}
								// if($s == ''){$s = $value['series'];}
								// if($ss == ''){$ss = $value['series_sub'];}
							?>
					<li><a <?= $value['division'] ==  $d && $value['series'] ==  $s && $value['series_sub'] ==  $ss? 'class="active"':""?>
							href="?c=<?=$c?>&dt=<?=$dt?>&d=<?=$value['division']?>&s=<?=$value['series'];?>&ss=<?=$value['series_sub'];?>">
							<?php echo $value['series'].$value['series_sub'];?></a></li>
					<?php
					}?>
				</ul>

				<?php if($c !='' && $d != '' && $ss != ''){?>
				<div id="court_list" class="btn-group" role="group">
					<ul class="btn-list">
						<?php
						$sql = "select tournament_count, tournament_num,
						case when tournament_count >0 then concat(tournament_count*2,'강')
						else '결승' end as display_name
						from game_score_data
						where match_code = '$c'
						and division = '{$d}'
						and series = '{$s}'
						and series_sub = '{$ss}'
						and tournament <> 'L'
						group by tournament_count order by tournament_count desc";
						// print ">>>>>>>>".$sql;
						$result = sql_query($sql, true);

						// print $sql;
						while($value = sql_fetch_array($result)){
							if($ct == '') $ct = $value['tournament_count'];
						?>
							<li><a <?=$ct == $value['tournament_count']? 'class="active"':""?>
								href="?c=<?=$c?>&dt=<?=$dt?>&d=<?=$d?>&s=<?=$s?>&ss=<?=$ss?>&ct=<?=$value['tournament_count']?>">
								<?=$value['display_name']?></a></li>

						<?php
						}?>
						<!-- 결승이 선택안했는데 튀어나와서 -->


					</ul>
		        </div>
						<?php }?>
		        <!-- //탭-코트-->
			</div>
		</section>

	<?php
		if($c !='' && $d != '' && $ss != ''){
			$redirectUrl = urlencode (G5_AOS_URL."/competion_mnt_list1.php?c=$c&dt=$dt&d=$d&s=$s&ss=$ss&ct=$ct");

			$matchProcessSql = "select a.*
					from game_score_data as a
					where a.match_code='$c' and a.tournament != 'L'

					and a.division = '{$d}' and a.series = '{$s}'
					and a.series_sub = '{$ss}'
					and tournament_count={$ct}
					order by a.game_increase desc, a.wr_id desc";
					// print $matchProcessSql;
			$matchProcess = sql_query($matchProcessSql);
			$matchIndex = 0;
			$match_group_code = '';
			$match_group_text = '';

			end_game_check($c, $dt, $ct, $g);


			while($match_row = sql_fetch_array($matchProcess)){
				$matchIndex++;
				if($match_group_code == '' || $match_group_code != $match_row['group_code'] ){?>

					<?php if($match_group_code != '' && $match_group_code != $match_row['group_code']){?>
					</tbody></table></div></div></section>
					<?php }?>
					<section>
						<div class="content">
						<div class="con_tit_area clear">
							<div class="tit">경기상황</div>
						</div>
						<!-- tab 토너먼트 출력 -->
						<!-- <div class="tab_group1 mb-0">
							<a href="?c=<?=$c?>&d=<?=$d?>&s=<?=$s?>&ss=<?=$ss?>&t=T&rd=<?=$row['tournament_count']?>" class="round_tab
								<?=($rd == $row['tournament_count'] ? 'active' : '')?>"
								 data-for="<?=$row['tournament_count']?>" >
							 <?=$row['round']?></a>
							 <a >ㅇ강</a>
							 <a >ㅇ강</a>
							 <a >ㅇ강</a>
						</div> -->
						<!-- tab 토너먼트 출력 -->

						<ul class="tournament_list active" id="8강" >
							<!-- <div class="status_banner"><?=$value['tournament_count']*2?>강 토너먼트 상태</div> -->
							<div class="status_banner">
								<?=$ct == 0 ? '결승' : $ct *2 ."강"?>
								토너먼트 <!--상태--></div>
							<?php
								$match_group_code = $match_row['group_code'];
							} ?>
							<?php

								$team_field = "team_data";
								$team_1 = $match_row['team_1_code'];
								$team_2 = $match_row['team_2_code'];
								if($d == "단체전"){
									$team_field = "team_event_data";
									$team_1 = $match_row['team_1_event_code'];
									$team_2 = $match_row['team_2_event_code'];
									}

								$team1 = sql_fetch("select * from $team_field where match_code = '$c' and team_code='$team_1'");
								$team2 = sql_fetch("select * from $team_field where match_code = '$c' and team_code='$team_2'");
								$gym = sql_fetch("select * from gym_data where wr_id = '$match_row[gym_code]'");
							?>

							<li>
								<div class="tournament_match">
									<?php print $match_row['wr_id']?>
									<div class="tournament_hd">
										<span><?php echo $gym['gym_name'];?> <?=$match_row['tournament_count']*2?>강 <?=$match_row['game_court'];?>코트 <?=$matchIndex?>경기</span>
										<span class="r-side-area"></span>
									</div>

									<div class="tournament_content">
										<div class="l-area">
											<?php

											if($match_row['division'] == "단체전"){
												if($match_row['team_1_event_code'] == ""){
													print "<!--";
													print_r($match_row);
													print "--!>";
											?>

												<a class="score_btn" href="popup_team_event_set.php?game_code=<?php echo $match_row['code'];?>&team=1&t=T&rd=<?=$redirectUrl?>">
													<?php print $match_row['club']?>
													경기배정1212</a>
											<?php
												}
											}
											?>
											<?php
											$team_1_code = 'team_1_code';
											$team_2_code = 'team_2_code';
											if($match_row['division'] == '단체전'){
												$team_1_code = 'team_1_event_code';
												$team_2_code = 'team_2_event_code';
											}

											if($match_row[$team_1_code] == ''){?>
												<div class="teamA"><?php print $match_row['assigned_group_name1']?></div>
											<?php }else{	?>

											<div class="teamA"><?=$team1['club']?>-<?=$team1['team_1_name']?></div>
											<div class="teamA"><?=$team1['team_2_club']?>-<?=$team1['team_2_name']?></div>
											<?php }?>
										</div>
									<div class="tournament_point match_point">
										<div class="btn_group">
										<?php
											if($match_row['end_game'] == 'N'){
												if($match_row['is_on'] == 'N'){?>
													<div class="tournament_point">
														<a href="./start_game.php?c=<?=$c?>&dt=<?=$dt?>&g=<?=$g?>&ct=<?=$ct?>&gid=<?=$match_row['code']?>&rd=<?=$redirectUrl?>" class="btn ready">경기시작</a>
													</div>
												<?php }else{?>
													<div class="tournament_point">
		                	<a href="popup_insert_score.php?game_code=<?=$match_row['code']?>&rd=<?=$redirectUrl?>" target="_blank" class="btn mpoint">점수입력</a>
		                </div>
												<?php }
											}else{?>
		                <a href="popup_insert_score.php?game_code=<?=$match_row['code']?>&rd=<?=$redirectUrl?>" target="_blank"><span><?=$match_row['team_1_score']?></span>&nbsp;:&nbsp;<span><?=$match_row['team_2_score']?></span></a>
											<?php }?>
										</div>
									</div>
									<div class="r-area">
										<?php
										if($match_row['division'] == "단체전")
											if($match_row['team_2_event_code'] == ""){
										?>
											<a class="score_btn" href="popup_team_event_set.php?game_code=<?php echo $match_row['code'];?>&team=2&t=T&rd=<?=$redirectUrl?>">경기배정</a>
										<?php
											}
										?>
										<?php if($match_row[$team_2_code] == ''){?>
											<div class="teamB"><?php print $match_row['assigned_group_name2']?></div>
										<?php }else{?>
										<div class="teamB"><?=$team2['team_1_name']?>-<?=$team2['club']?></div>
										<div class="teamB"><?=$team2['team_2_name']?>-<?=$team2['team_2_club']?></div>
										<?php }?>
									</div>
									</div>
									<div class="tournament_ft">
										<span></span>
										<span class="r-side-area"><?php print $match_row['game_date']?></span>
									</div>
								</div>
							</li>

							<?php }?>
						<?php }?>
					<?php } ?>
</div>
<!-- end Contents Area -->
					</ul></div></div></section>

<script>
$('#gym').change(function(event){
	window.location.href='?c=<?=$c?>&dt='+encodeURIComponent('<?=$dt?>')+'&g='+$(this).val();
});

</script>


<?php

	function end_game_check($c, $dt, $ct, $g){
		$sql = "select a.end_game, count(end_game) as count from game_score_data as a where a.match_code='6131400ZFZJAJWGABF' and a.tournament != 'L' and a.game_date = '2017-05-21' and a.tournament_count = '32' and a.series_sub = '신인부' group by end_game order by a.game_increase desc, a.wr_id desc";

		$result = sql_query($sql);
		$end_game;
		while($row = sql_fetch_array($result)){
			$end_game[$row['end_game']] = $row['count'];
		}
		//N컬럼이 없을경우
		if(!isset($end_game['N'])){
			// print "종료";
			return false;
		}else {
			// print "진행중";
			return true;
		}

	}

 ?>
<?php
include_once(G5_AOS_PATH.'/tail.php');
?>
