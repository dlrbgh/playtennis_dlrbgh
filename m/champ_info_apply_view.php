<?php
include_once('../common.php');

$wr_id = $_REQUEST['wr_id'];
$division = $_REQUEST['division'];
$series = $_REQUEST['series'];
$series_sub = $_REQUEST['series_sub'];

$user_code	= $_REQUEST['user_code'];
$act = 'champ_info_apply'
?>
<?php require 'inc/config.php'; ?>
<?php require 'inc/views/template_head_start.php'; ?>

<!-- Page JS Plugins CSS -->
<link rel="stylesheet" href="../<?php echo $one->assets_folder; ?>/js/plugins/slick/slick.min.css">
<link rel="stylesheet" href="../<?php echo $one->assets_folder; ?>/js/plugins/slick/slick-theme.min.css">
<link rel="stylesheet" href="../<?php echo $one->assets_folder; ?>/js/plugins/select2/select2.min.css">
<link rel="stylesheet" href="../<?php echo $one->assets_folder; ?>/js/plugins/select2/select2-bootstrap.min.css">
<?php require 'inc/views/template_head_end.php'; ?>

<?php 
	$sql = "select * from match_data where wr_id = '$wr_id' order by wr_id desc";
	$result = sql_query($sql);
	$r = sql_fetch_array($result);           
	$code = $r['code'];       
?>
    
<!-- sub nav -->
<?php include_once('./app_sub_nav.php'); ?>
<!-- end sub nav -->

<!-- Contents Area -->
	
	<div class="content" style="margin-top: 42px;">
	
		<div class="btn-group btn-group-justified push-10">
	        <div class="btn-group">
	            <a href="champ_info_apply.php?wr_id=<?=$wr_id;?>&user_code=<?=$user_code;?>&user_club=<?=$user_club;?>&user_name=<?=$user_name;?>"><button class="btn btn-lg btn-white active" type="button">급수별</button></a>
	        </div>
	        <div class="btn-group">
	            <a href="champ_info_area.php?wr_id=<?=$wr_id;?>&user_code=<?=$user_code;?>&user_club=<?=$user_club;?>&user_name=<?=$user_name;?>"><button class="btn btn-lg btn-white " type="button">지역별</button></a>
	        </div>
		</div>
	    
	    <div class="row">
	    	<!-- 토너먼트 대진표 출력 -->
	    	<div class="col-xs-12">
		    	<div class="block block-themed block-rounded">
			    	<div class="block-header bg-smooth-dark" style="padding:13px 20px;">
		                <h3 class="block-title"><?=$division;?> / <?=$series;?> / <?=$series_sub;?></h3>
		            </div>
		            <?php
		        	$sql = "select count(*) as cnt from group_data where match_code = '$code' and division = '$division' and series = '$series' and series_sub = '$series_sub' and tournament = 'L'";
					$result = sql_query($sql);
					$r = sql_fetch_array($result);
					if($r['cnt'] > 1){
					?>
					<div class="col-lg-6 push-10 bg-white" style="padding-bottom:10px;">
			    		<img class="img-responsive" src="<?php echo G5_IMG_URL?>/tour_seed/<?=$r['cnt'];?>.gif">
				        <div class="text-center push-10-t">
			    			<h5>위 토너먼트는 예선전 순위에따라 배정됩니다</h5>
			    		</div>
			        </div>
			    	<?php		
						}				
					?>
		        </div>
		    </div>
	        <!-- end 토너먼트 대진표 출력 -->
	        <?php
        			$sql = "select * from group_data where match_code = '$code' and division = '$division' and series = '$series' and series_sub = '$series_sub' and tournament = 'L' order by num asc";
					
					$result = sql_query($sql);
					while($r = sql_fetch_array($result)){						
			?>
            <?php
        		$sql_team1 = "select * from team_data where team_code = '$r[team_1]'";
				$team1_result = sql_query($sql_team1);
				$team1 = sql_fetch_array($team1_result);
				
				$sql_team2 = "select * from team_data where team_code = '$r[team_2]'";
				$team2_result = sql_query($sql_team2);
				$team2 = sql_fetch_array($team2_result);
				
				$sql_team3 = "select * from team_data where team_code = '$r[team_3]'";
				$team3_result = sql_query($sql_team3);
				$team3 = sql_fetch_array($team3_result);
				
				$sql_team4 = "select * from team_data where team_code = '$r[team_4]'";
				$team4_result = sql_query($sql_team4);
				$team4 = sql_fetch_array($team4_result);
				
				$sql_team5 = "select * from team_data where team_code = '$r[team_5]'";
				$team5_result = sql_query($sql_team5);
				$team5 = sql_fetch_array($team5_result);
				
			?>

	        <div class="col-md-6 push-20">
	        	<div class="font-w700 push-5"><?=$r['num']+1?>그룹</div>
	        	<table class="table champ_result bg-white">
            		<thead>
            			<th class="text-center">클럽명</th>
            			<th colspan="2" class="text-center">지역</th>
            			<th class="text-center">참가자</th>
            		</thead>
            		<tbody>
            			<tr>
            				<td><?=$team1['club'];?></td>
            				<td><?=$team1['area_1'];?></td>
            				<td><?=$team1['area_2'];?></td>
            				<td><?=$team1['team_1_name'];?> / <?=$team1['team_2_name'];?></td>
            			</tr>
            			<tr>
            				<td><?=$team2['club'];?></td>
            				<td><?=$team2['area_1'];?></td>
            				<td><?=$team2['area_2'];?></td>
            				<td><?=$team2['team_1_name'];?> / <?=$team2['team_2_name'];?></td>
            			</tr>
            			<?php if($team3['club'] != ""){?>
            			<tr>
            				<td><?=$team3['club'];?></td>
            				<td><?=$team3['area_1'];?></td>
            				<td><?=$team3['area_2'];?></td>
            				<td><?=$team3['team_1_name'];?> / <?=$team3['team_2_name'];?></td>
            			</tr>
            			<?php }?>
            			<?php if($team4['club'] != ""){?>
            			<tr>
            				<td><?=$team4['club'];?></td>
            				<td><?=$team4['area_1'];?></td>
            				<td><?=$team4['area_2'];?></td>
            				<td><?=$team4['team_1_name'];?> / <?=$team4['team_2_name'];?></td>
            			</tr>
            			<?php }?>
            			<?php if($team5['club'] != ""){?>
            			<tr>
            				<td><?=$team5['club'];?></td>
            				<td><?=$team5['area_1'];?></td>
            				<td><?=$team5['area_2'];?></td>
            				<td><?=$team5['team_1_name'];?> / <?=$team5['team_2_name'];?></td>
            			</tr>
            			<?php }?>

            		</tbody>
            	</table>
	        </div>
	          <?php		
						}				
					?>
	        
	    </div>
	    
	</div>
<!-- end Contents Area -->


<?php require 'inc/views/base_footer.php'; ?>
<?php require 'inc/views/template_footer_start.php'; ?>


<!-- Page JS Plugins -->

<script src="<?php echo $one->assets_folder; ?>/js/plugins/datatables/jquery.dataTables.min.js"></script>
<script src="<?php echo $one->assets_folder; ?>/js/plugins/select2/select2.full.min.js"></script>

<!-- Page JS Code -->
<script src="<?php echo $one->assets_folder; ?>/js/pages/base_tables_datatables.js"></script>

<script>
    jQuery(function(){
        // Init page helpers (BS Datepicker + BS Datetimepicker + BS Colorpicker + BS Maxlength + Select2 + Masked Input + Range Sliders + Tags Inputs plugins)
        App.initHelpers(['datepicker', 'datetimepicker', 'colorpicker', 'maxlength', 'select2', 'masked-inputs', 'rangeslider', 'tags-inputs']);
    });
</script>
<?php require 'inc/views/template_footer_end.php'; ?>

