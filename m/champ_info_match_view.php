<?php
include_once('../common.php');

$wr_id 		= $_REQUEST['wr_id'];
$division   = $_REQUEST['division'];
$series 	= $_REQUEST['series'];
$series_sub = $_REQUEST['series_sub'];
$user_code	= $_REQUEST['user_code'];
$act = 'champ_info_match'
?>
<?php require 'inc/config.php'; ?>
<?php require 'inc/views/template_head_start.php'; ?>

<!-- Page JS Plugins CSS -->
<link rel="stylesheet" href="<?php echo $one->assets_folder; ?>/js/plugins/slick/slick.min.css">
<link rel="stylesheet" href="<?php echo $one->assets_folder; ?>/js/plugins/slick/slick-theme.min.css">
<link rel="stylesheet" href="<?php echo $one->assets_folder; ?>/js/plugins/select2/select2.min.css">
<link rel="stylesheet" href="<?php echo $one->assets_folder; ?>/js/plugins/select2/select2-bootstrap.min.css">
<?php require 'inc/views/template_head_end.php'; ?>

<?php 
	$sql = "select * from match_data where wr_id = '$wr_id' order by wr_id desc";
	$result = sql_query($sql);
	$i = 1;
	$r = sql_fetch_array($result);
    $code = $r['code'];
	
	$sql = "select * from match_gym_data where match_id = '$code' group by gym_id";
	$result = sql_query($sql);
	$gym_id = sql_fetch_array($result);
?>

<!-- sub nav -->
<?php include_once('./app_sub_nav.php'); ?>
<!-- end sub nav -->

<!-- Contents Area -->
<script language="JavaScript">
function readCookie(name){
return(document.cookie.match('(^|; )'+name+'=([^;]*)')||0)[2]
}
</script>
<body onScroll="document.cookie='ypos=' + window.pageYOffset" onLoad="window.scrollTo(0,readCookie('ypos'))">
	<div class="content" style="margin-top: 42px;" >
	
		<div class="btn-group btn-group-justified push-10">
	        <div class="btn-group">
	            <a href="champ_info_match.php?wr_id=<?=$wr_id;?>&user_club=<?=$user_club;?>&user_name=<?=$user_name;?>&user_code=<?=$user_code;?>&division=남복"><button class="btn btn-lg btn-white active" type="button">급수별 편성표</button></a>
	        </div>
	        <div class="btn-group">
	            <a href="champ_info_match_time.php?wr_id=<?=$wr_id;?>&user_club=<?=$user_club;?>&user_name=<?=$user_name;?>&user_code=<?=$user_code;?>&gym_court=<?=$gym_id['wr_id']?>&application_period=<?=$gym_id['application_period']?>&gym_game=1"><button class="btn btn-lg btn-white" type="button">대진 시간표</button></a>
	        </div>
		</div>
		<div class="row">
			<div class="col-md-6">
				<!-- 1일차 기본 선택 / 1일 경기 일경우 출력 안함 -->
				<div class="champ-data push-10">
					<?php 
						$sqls = "select * from series_data where match_code = '$code' group by division";
						$results = sql_query($sqls);
						while($seriess = sql_fetch_array($results)){
							
					?>
					<a class="<?php if(strcmp(trim($division), trim($seriess['division'])) == "0") echo "active"; ?> btn2" href="champ_info_match.php?wr_id=<?=$wr_id?>&user_code=<?=$user_code;?>&division=<?=$seriess['division']?>&user_club=<?=$user_club;?>&user_name=<?=$user_name;?>"><?=$seriess['division']?></a>
					<?php			
						}
					?>
					<!-- <button class="btn2">남자 복식</button>
					<button class="active btn2">여자 복식</button>
					<button class="btn2">혼합 복식</button> -->				
				</div>
			</div>
		</div>
	    
	    <div class="row">
	    	<!-- 토너먼트 대진표 출력 -->
	        	<?php
		        	$sql = "select count(*) as cnt from group_data where match_code = '$code' and division = '$division' and series = '$series' and series_sub = '$series_sub' and tournament = 'L'";
					$result = sql_query($sql);
					$r = sql_fetch_array($result);
					if($r['cnt'] > 1){
				?>
		    	<div class="col-lg-12 push-10 bg-white" style="padding-bottom:10px;">
		    		<img class="img-responsive" src="<?php echo G5_IMG_URL?>/tour_seed/<?=$r['cnt'];?>.gif">
			        <div class="text-center push-10-t">
		    			<h5>위 토너먼트는 예선전 순위에따라 배정됩니다</h5>
		    		</div>
		        </div>
	        	<?php		
					}				
				?>
				
			<!-- end 토너먼트 대진표 출력 -->
			
           <?php
				$group_sql = "select count(*) as cnt from group_data where match_code = '$code' and division = '$division' and series = '$series' and series_sub = '$series_sub'";
				$group_result = sql_query($group_sql);
            	$group = sql_fetch_array($group_result);
				if($group['cnt'] > 1){
			?>
			<!-- 테스트 -->
			<style>
				
			</style>
			<div class="col-lg-12">
	            <div class="block block-opt-hidden">
	                <div class="block-tit" >
	                    <ul class="block-options">
	                        <li>
	                            <button type="button" data-toggle="block-option" data-action="content_toggle"><i class="si si-arrow-down"></i></button>
	                        </li>
	                    </ul>
	                    <h3 class="block-title">토너먼트 대진</h3>
	                </div>
	                <div class="block-content_list">
	                    <div class="row tournament_match">
                			<?php 
                    			$score_sql = "select * from game_score_data where match_code = '$code' and division = '$division' and series = '$series' and series_sub = '$series_sub' and tournament != 'L' and game_assign = '1' order by wr_id ";
								
								$score_result = sql_query($score_sql);
								$i = 1;
								while($score = sql_fetch_array($score_result)){
									
									$sql_team1 = "select * from team_data where team_code = '$score[team_1_code]'";
									$team1_result = sql_query($sql_team1);
									$team1 = sql_fetch_array($team1_result);
									
									$sql_team2 = "select * from team_data where team_code = '$score[team_2_code]'";
									$team2_result = sql_query($sql_team2);
									$team2 = sql_fetch_array($team2_result);
									
									$sql_gym = "select * from gym_data where wr_id = '$score[gym_code]'";
									$gym_result = sql_query($sql_gym);
									$gym = sql_fetch_array($gym_result);
									
								?>
							<!-- 참가선수 정보 -->
                			<div class="col-xs-12 col-sm-6 col-md-6">
					            <a class="block block-rounded" href="javascript:void(0)">
					                <div class="block-options push-10-t push-10-r">
						                <li>
						                    <?=$score['game_court'];?>코트 <?=$score['court_array_num'];?>경기
						                </li>
						            </div>
					                <div class="tournament_header">
						               	<?php if($score['tournament'] == "T"){?>
					                    <h3 class="block-title">토너먼트 <?=$score['tournament_count']*2;?>강</h3>
						               	<?php }?>
						               	<?php if($score['tournament'] == "C"){?>
					                    <h3 class="block-title">결승</h3>
						               	<?php }?>
					                </div>
					                <div class="tournament_content clearfix">
					                	
					                	<div class="pull-left text-center push-5-l">
					                		<?php
					                			$favor_sql = "select count(*) as cnt from app_user_favor where user_code = '$user_code' and match_code = '$code' and team_code = '$score[team_1_code]'";
					                			$favor_result = sql_query($favor_sql);
												$favor = sql_fetch_array($favor_result);
												if($favor['cnt'] == 0){
											?>
											
					                    		<button class="btn_fav" onclick="favor('<?=$user_code?>','<?=$score['team_1_code']?>')">
					                    	<?php
												}else{
											?>
												<button class="btn_fav active" onclick="favor('<?=$user_code?>','<?=$score['team_1_code']?>')">
											<?php	
												}
					                		?>
						                    	<div class="left-star">
							                		<i class="fa fa-star"></i>
							                	</div>
						                        <div class="font-w600"><?=$team1['club'];?></div>
						                        <div class=""><?=$team1['team_1_name'];?> <?=$team1['team_2_name'];?></div>
						                    </button>
					                    </div>
					                	<div class="tournament_point">
					                		<span class="left_team <?php if($score['team_1_score'] > $score['team_2_score']) echo "win";?>"><?=$score['team_1_score'];?></span>
					                		<span>vs</span>
					                		<span class="right_team <?php if($score['team_1_score'] < $score['team_2_score']) echo "win";?>"><?=$score['team_2_score'];?></span>
					                    </div>
					                    <div class="pull-right text-center push-5-r">
											<?php
					                			$favor_sql = "select count(*) as cnt from app_user_favor where user_code = '$user_code' and match_code = '$code' and team_code = '$score[team_2_code]'";
					                			$favor_result = sql_query($favor_sql);
												$favor = sql_fetch_array($favor_result);
												if($favor['cnt'] == 0){
											?>
											
					                    		<button class="btn_fav" onclick="favor('<?=$user_code?>','<?=$score['team_2_code']?>')">
					                    	<?php
												}else{
											?>
												<button class="btn_fav active" onclick="favor('<?=$user_code?>','<?=$score['team_2_code']?>')">
											<?php	
												}
					                		?>										                		
					                			<div class="right-star">
							                		<i class="fa fa-star"></i>
							                	</div>
						                        <div class="font-w600"><?=$team2['club'];?></div>
						                        <div class=""><?=$team2['team_1_name'];?> <?=$team2['team_2_name'];?></div>
					                        </button>
					                    </div>
					                </div>
					                <div class="coat_tit">
					                	<span class="pull-left"><?=$score['game_date'];?> <?=$score['game_time'];?></span>
					                	<span class="pull-right"><?=$gym['gym_name'];?>&nbsp;&nbsp;&nbsp;<?=$score['game_code'];?></span>
					                </div>
					            </a>
					        </div>
					        <!-- end 참가선수 정보 -->				                    			
					        <?php
					        	$i++;
								}
                    		?>			                    			
                		</div>
	                </div>
	            </div>
	        </div>
	        <!-- end 테스트 -->
	        <?php		
				}
            ?>
			
            <!-- 리그전 종합 순위 -->
            <div class="col-lg-12">
	            <div class="block block-opt-hidden">
	                <div class="block-tit" >
	                    <ul class="block-options">
	                        <li>
	                            <button type="button" data-toggle="block-option" data-action="content_toggle"><i class="si si-arrow-down"></i></button>
	                        </li>
	                    </ul>
	                    <h3 class="block-title">토너먼트 시드배정 순위</h3>
	                </div>
	                <div class="block-content_list">
	                    <!-- 경기 결과표 출력 -->
	                    <table class="table champ_result table-mobile bg-white">
                    		<thead>
                    			<th class="text-center">순위</th>
                    			<th class="text-center">클럽</th>
                    			<th class="text-center">선수</th>
                    			<th class="text-center">승</th>
                    			<th class="text-center">패</th>
                    			<th class="text-center">득</th>
                    			<th class="text-center">실</th>
                    			<th class="text-center">차</th>
                    			<th class="text-center">순위</th>
                    		</thead>
                    		<tbody class="text-center">
                    			<?php 
								$group_cnt_sql = "select count(wr_id) as cnt from group_data where match_code = '$code' and  division = '$division' and series = '$series' and series_sub = '$series_sub'";
								$result = sql_query($group_cnt_sql);
								$group_data = sql_fetch_array($result);
								
								if($group_data['cnt'] == 2 ){
									$group_count = 4;
								}
								if($group_data['cnt'] == 3 ){
									$group_count = 6;
								}
								if($group_data['cnt'] == 4 ){
									$group_count = 4;
								}
								if($group_data['cnt'] > 4 && $group_data['cnt'] < 8  ){
									$group_count = (8-$group_data['cnt'])+($group_data['cnt']-(8-$group_data['cnt']));
								}
								if($group_data['cnt'] == 8  ){
									$group_count = 8;
								}
								if($group_data['cnt'] > 8 && $group_data['cnt'] < 16 ){
									$group_count = (16-$group_data['cnt'])+($group_data['cnt']-(16-$group_data['cnt']));
								}
								if( $group_data['cnt'] == 16 ){
									$group_count = 16;
								}
								
                    			$score_sql = "select * from team_data where match_code = '$code' and division = '$division' and series = '$series' and series_sub = '$series_sub'  ORDER BY group_rank,team_league_point desc ,winning_rate desc	,gains_losses_point desc, team_total_match_point DESC Limit 0,$group_count";
								
								$score_result = sql_query($score_sql);
								$i = 1;
								while($score = sql_fetch_array($score_result)){
									
									$gourp_sql = "select num from group_data where (team_1  = '$score[team_code]' or team_2  = '$score[team_code]' or team_3  = '$score[team_code]' or team_4  = '$score[team_code]'  or team_5  = '$score[team_code]') and match_code = '$code' and division = '$division' and series = '$series' and series_sub = '$series_sub' and tournament = 'L' order by num asc";
									$group_result = sql_query($gourp_sql);
									$group = sql_fetch_array($group_result);
								?>
                    			<tr>
                    				<td><?=$i;?></td>
                    				<td><?=$score['club'];?></td>
                    				<td><?=$score['team_1_name'];?>&nbsp;<?=$score['team_2_name'];?></td>
                    				<td><?=$score['team_league_point'];?></td>
                    				<td><?=$score['team_league_lose_count'];?></td>
                    				<td><?=$score['team_total_match_point'];?></td>
                    				<td><?=$score['team_total_match_minus_point'];?></td>
                    				<td><?=$score['gains_losses_point'];?></td>
                    				<td><?=$group['num']+1?>그룹<?=$score['group_rank'];?>위</td>
                    			</tr>
                    			<?php $i++; } ?>
                    			
                    		</tbody>
                    	</table>
                    	<!-- END 경기 결과표 출력 -->
                    	<div class="push-10-t font-w700">순위 선정 기준 : 다승 > 승률 > 득실차 > 다득점</div>
                    	<!-- 참가 선수 정보 및 대진 점수 -->
                		<div class="row tournament_match">
                			<?php 
                    			$score_sql = "select * from game_score_data where group_code = '$r[code]' and match_code = '$code' and division = '$division' and series = '$series' and series_sub = '$series_sub' and tournament = 'L' order by wr_id desc";
								$score_result = sql_query($score_sql);
								$i = 1;
								while($score = sql_fetch_array($score_result)){
									
									$sql_team1 = "select * from team_data where team_code = '$score[team_1_code]'";
									$team1_result = sql_query($sql_team1);
									$team1 = sql_fetch_array($team1_result);
									
									$sql_team2 = "select * from team_data where team_code = '$score[team_2_code]'";
									$team2_result = sql_query($sql_team2);
									$team2 = sql_fetch_array($team2_result);
									
									$sql_gym = "select * from gym_data where wr_id = '$score[gym_code]'";
									$gym_result = sql_query($sql_gym);
									$gym = sql_fetch_array($gym_result);
									
								?>
							<!-- 참가선수 정보 -->
                			<div class="col-xs-12 col-sm-6 col-md-6">
					            <a class="block block-rounded" href="javascript:void(0)">
					                <div class="block-options push-10-t push-10-r">
						                <li>
						                    <?=$score['game_court'];?>코트 <?=$score['court_array_num'];?>경기
						                </li>
						            </div>
					                <div class="tournament_header">
					                    <h3 class="block-title">조별리그 <?=$i;?>경기</h3>
					                </div>
					                <div class="tournament_content clearfix">
					                	<div class="pull-left text-center push-5-l">
					                    	<?php
					                			$favor_sql = "select count(*) as cnt from app_user_favor where user_code = '$user_code' and match_code = '$code' and team_code = '$score[team_1_code]'";
					                			$favor_result = sql_query($favor_sql);
												$favor = sql_fetch_array($favor_result);
												if($favor['cnt'] == 0){
											?>
											
					                    		<button class="btn_fav" onclick="favor('<?=$user_code?>','<?=$score['team_1_code']?>')">
					                    	<?php
												}else{
											?>
												<button class="btn_fav active" onclick="favor('<?=$user_code?>','<?=$score['team_1_code']?>')">
											<?php	
												}
					                		?>
						                    	<div class="left-star">
							                		<i class="fa fa-star"></i>
							                	</div>
						                        <div class="font-w600"><?=$team1['club'];?></div>
						                        <div class=""><?=$team1['team_1_name'];?> <?=$team1['team_2_name'];?></div>
						                    </button>
					                    </div>
					                	<div class="tournament_point">
					                		<span class="left_team <?php if($score['team_1_score'] > $score['team_2_score']) echo "win";?>"><?=$score['team_1_score'];?></span>
					                		<span>vs</span>
					                		<span class="right_team <?php if($score['team_1_score'] < $score['team_2_score']) echo "win";?>"><?=$score['team_2_score'];?></span>
					                    </div>
					                    <div class="pull-right text-center push-5-r">
					                    	<?php
					                			$favor_sql = "select count(*) as cnt from app_user_favor where user_code = '$user_code' and match_code = '$code' and team_code = '$score[team_2_code]'";
					                			$favor_result = sql_query($favor_sql);
												$favor = sql_fetch_array($favor_result);
												if($favor['cnt'] == 0){
											?>
											
					                    		<button class="btn_fav" onclick="favor('<?=$user_code?>','<?=$score['team_2_code']?>')">
					                    	<?php
												}else{
											?>
												<button class="btn_fav active" onclick="favor('<?=$user_code?>','<?=$score['team_2_code']?>')">
											<?php	
												}
					                		?>
						                		<div class="right-star">
							                		<i class="fa fa-star"></i>
							                	</div>
						                        <div class="font-w600"><?=$team2['club'];?></div>
						                        <div class=""><?=$team2['team_1_name'];?> <?=$team2['team_2_name'];?></div>
					                        </button>
					                    </div>
					                </div>
					                <div class="coat_tit">
					                	<span class="pull-left"><?=$score['game_date'];?> <?=$score['game_time'];?></span>
					                	<span class="pull-right"><?=$gym['gym_name'];?>&nbsp;&nbsp;&nbsp;<?=$score['game_code'];?></span>
					                </div>
					            </a>
					        </div>
					        <!-- end 참가선수 정보 -->				                    			
					        <?php
					        	$i++;
								}
                    		?>			                    			
                		</div>
                		<!-- end 참가 선수 정보 및 대진 점수 -->
	                </div>
	            </div>
	        </div>
	        <!-- end 리그전 종합 순위 -->
            
            <?php
        			$sql = "select * from group_data where match_code = '$code' and division = '$division' and series = '$series' and series_sub = '$series_sub' and tournament = 'L' order by num asc";
					$result = sql_query($sql);
					while($r = sql_fetch_array($result)){
			?>
	        
	        <div class="col-lg-12">
	            <div class="block block-opt-hidden">
	                <div class="block-tit" >
	                    <ul class="block-options">
	                        <li>
	                            <button type="button" data-toggle="block-option" data-action="content_toggle"><i class="si si-arrow-down"></i></button>
	                        </li>
	                    </ul>
	                    <h3 class="block-title"><?=$r['num']+1;?>그룹 대진</h3>
	                </div>
	                <div class="block-content_list">
	                    <table class="table champ_result table-mobile bg-white">
                    		<thead>
                    			<th class="text-center">순위</th>
                    			<th class="text-center">클럽</th>
                    			<th class="text-center">선수</th>
                    			<th class="text-center">승</th>
                    			<th class="text-center">패</th>
                    			<th class="text-center">득점</th>
                    			<th class="text-center">실점</th>
                    			<th class="text-center">득실</th>
                    		</thead>
                    		<tbody>
                    			<?php 
                    			$rank_sql = "select * from team_data where (team_code = '$r[team_1]' or team_code = '$r[team_2]' or team_code = '$r[team_3]' or team_code = '$r[team_4]' or team_code = '$r[team_5]') and match_code = '$code' and division = '$division' and series = '$series' and series_sub = '$series_sub' order by group_rank";
								$rank_result = sql_query($rank_sql);
								while($rank = sql_fetch_array($rank_result)){
								?>
                    			<tr>
                    				<td><?=$rank['group_rank'];?></td>
                    				<td><?=$rank['club'];?></td>
                    				<td><?=$rank['team_1_name'];?>&nbsp;<?=$rank['team_2_name'];?></td>
                    				<td><?=$rank['team_league_point'];?></td>
                    				<td><?=$rank['team_league_lose_count'];?></td>
                    				<td><?=$rank['team_total_match_point'];?></td>
                    				<td><?=$rank['team_total_match_minus_point'];?></td>
                    				<td><?=$rank['team_total_match_point'] - $rank['team_total_match_minus_point'];?></td>
                    			</tr>
                    			<?php
								}
                    			?>
                    		</tbody>
                    	</table>
                    	<div class="push-10-t font-w700">순위 선정 기준 : 다승 > 승률 > 득실차 > 다득점</div>
                    	<div class="row tournament_match">
                			<?php 
                    			$score_sql = "select * from game_score_data where group_code = '$r[code]' and match_code = '$code' and division = '$division' and series = '$series' and series_sub = '$series_sub' and tournament = 'L' order by wr_id desc";
								$score_result = sql_query($score_sql);
								$i = 1;
								while($score = sql_fetch_array($score_result)){
									
									$sql_team1 = "select * from team_data where team_code = '$score[team_1_code]'";
									$team1_result = sql_query($sql_team1);
									$team1 = sql_fetch_array($team1_result);
									
									$sql_team2 = "select * from team_data where team_code = '$score[team_2_code]'";
									$team2_result = sql_query($sql_team2);
									$team2 = sql_fetch_array($team2_result);
									
									$sql_gym = "select * from gym_data where wr_id = '$score[gym_code]'";
									$gym_result = sql_query($sql_gym);
									$gym = sql_fetch_array($gym_result);
									
								?>
							<!-- 참가선수 정보 -->
                			<div class="col-xs-12 col-sm-6 col-md-6">
					            <a class="block block-rounded" href="javascript:void(0)">
					                <div class="block-options push-10-t push-10-r">
						                <li>
						                    <?=$score['game_court'];?>코트 <?=$score['court_array_num'];?>경기
						                </li>
						            </div>
					                <div class="tournament_header">
					                    <h3 class="block-title"><?=$r['num']+1;?>그룹 예선&nbsp;(<?=$i;?>)</h3>
					                </div>
					                <div class="tournament_content clearfix">
					                	<div class="pull-left text-center push-5-l">
					                    	<?php
					                			$favor_sql = "select count(*) as cnt from app_user_favor where user_code = '$user_code' and match_code = '$code' and team_code = '$score[team_1_code]'";
					                			$favor_result = sql_query($favor_sql);
												$favor = sql_fetch_array($favor_result);
												if($favor['cnt'] == 0){
											?>
											
					                    		<button class="btn_fav" onclick="favor('<?=$user_code?>','<?=$score['team_1_code']?>')">
					                    	<?php
												}else{
											?>
												<button class="btn_fav active" onclick="favor('<?=$user_code?>','<?=$score['team_1_code']?>')">
											<?php	
												}
					                		?>
						                    	<div class="left-star">
							                		<i class="fa fa-star"></i>
							                	</div>
						                        <div class="font-w600"><?=$team1['club'];?></div>
						                        <div class=""><?=$team1['team_1_name'];?> <?=$team1['team_2_name'];?></div>
						                    </button>
					                    </div>
					                	<div class="tournament_point">
					                		<span class="left_team <?php if($score['team_1_score'] > $score['team_2_score']) echo "win";?>"><?=$score['team_1_score'];?></span>
					                		<span>vs</span>
					                		<span class="right_team <?php if($score['team_1_score'] < $score['team_2_score']) echo "win";?>"><?=$score['team_2_score'];?></span>
					                    </div>
					                    <div class="pull-right text-center push-5-r">
					                    	<?php
					                			$favor_sql = "select count(*) as cnt from app_user_favor where user_code = '$user_code' and match_code = '$code' and team_code = '$score[team_2_code]'";
					                			$favor_result = sql_query($favor_sql);
												$favor = sql_fetch_array($favor_result);
												if($favor['cnt'] == 0){
											?>
											
					                    		<button class="btn_fav" onclick="favor('<?=$user_code?>','<?=$score['team_2_code']?>')">
					                    	<?php
												}else{
											?>
												<button class="btn_fav active" onclick="favor('<?=$user_code?>','<?=$score['team_2_code']?>')">
											<?php	
												}
					                		?>
						                		<div class="right-star">
							                		<i class="fa fa-star"></i>
							                	</div>
						                        <div class="font-w600"><?=$team2['club'];?></div>
						                        <div class=""><?=$team2['team_1_name'];?> <?=$team2['team_2_name'];?></div>
					                        </button>
					                    </div>
					                </div>
					                <div class="coat_tit">
					                	<span class="pull-left"><?=$score['game_date'];?> <?=$score['game_time'];?></span>
					                	<span class="pull-right"><?=$gym['gym_name'];?>&nbsp;&nbsp;&nbsp;<?=$score['game_code'];?></span>
					                </div>
					            </a>
					        </div>
					        <!-- end 참가선수 정보 -->				                    			
					        <?php
					        	$i++;
								}
                    		?>			                    			
                		</div>
                    	
	                </div>
	            </div>
	        </div>
	        
            <?php
					}
        	?>
            
            
 
	        
	    </div>
	    
	</div>
<!-- end Contents Area -->
</body>

<?php require 'inc/views/base_footer.php'; ?>
<?php require 'inc/views/template_footer_start.php'; ?>

<script>
function favor(user_code,team_code){
	$.ajax({
		type: 'post',
		url:'./favorite_add.php',
		dataType:'json',
		data: {user_code:user_code,team_code:team_code,code:'<?php echo $code;?>'},
		success:function(data){
			console.log(data);
			location.reload(true);
			if(data[0]["result"] == "success"){
				window.AJInterface.callAndroid();
			}else if(data[0]["result"] == "cancel"){
				window.AJInterface.cancelAndroid();
			}

			// var str = '';
			// for(var sub in data){
				// console.log(sub);
				// str += '<option value='+data[sub]['series_sub']+'>'+data[sub]['series_sub']+'</option>';
			// }
			// console.log(str);
			
	   	}
    });
}
</script>

<!-- Page JS Plugins -->
<script src="<?php echo $one->assets_folder; ?>/js/plugins/select2/select2.full.min.js"></script>
<script src="<?php echo $one->assets_folder; ?>/js/plugins/datatables/jquery.dataTables.min.js"></script>


<!-- Page JS Code -->
<script src="<?php echo $one->assets_folder; ?>/js/pages/base_tables_datatables.js"></script>
<script>
    jQuery(function(){
        // Init page helpers (BS Datepicker + BS Datetimepicker + BS Colorpicker + BS Maxlength + Select2 + Masked Input + Range Sliders + Tags Inputs plugins)
        App.initHelpers(['table-tools','datepicker', 'datetimepicker', 'colorpicker', 'maxlength', 'select2', 'masked-inputs', 'rangeslider', 'tags-inputs']);
    });
</script>

<?php require 'inc/views/template_footer_end.php'; ?>

