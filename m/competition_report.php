<?php
include_once('../common.php');
if (!defined('_GNUBOARD_')) exit; // 개별 페이지 접근 불가
include_once(G5_AOS_PATH.'/head.php');
$menu_cate2 ='competition_view';
$menu_cate3 ='7';
?>

<?php
//개인전
// if($c="4443620KKDWMYYUS3C"){
// 	$man_master_2 = "마스터2, 마스터2";
// 	$man_new_1 = "신인1, 신인1";
// 	$man_new_2 = "신인2, 신인2";
// 	$man_old = "어르신, 어르신";
// 	$man_open = "오픈, 오픈";
// 	$man_chall_1 = "챌린저1, 챌린저1";
// 	$man_chall_2= "챌린저2, 챌린저2";
// 	$wom_master = "마스터_여, 마스터_여";
// 	$wom_new_1 = "신인1_여, 신인1_여";
// 	$wom_new_2 = "신인2_여, 신인2_여";
// 	$wom_chall_1 = "챌린저1_여, 챌린저1_여";
// 	$wom_chall_2 = "챌린저2_여, 챌린저2_여";
// }
// //단체전
// if($c="4169710EFS4DQY4HJ0"){
// 	$man_master_1 = "마스터1, 마스터1";
// }
?>
<!-- sub nav -->
<?php include_once('./app_sub_nav.php'); ?>
<!-- end sub nav -->
<?php
	$defaultQstr = "competition_time_tbl.php?c={$c}&d={$d}&s={$s}&ss={$ss}&gi={$gi}&t={$t}";
	$gi = isset($gi) && $gi!='' ? $gi : 0;
	$t = isset($t) && $t!='' ? $t : "L";



	print $d;
?>


<!-- Contents Area -->
<div class="pop_container">
	<?php if(false &&! $competition['opening_date']){?>
		<div class="empty_waiting">
			<div class="img_area">
				<img src="<?php echo G5_IMG_URL.'/common/intro_logo1.png';?>">
			</div>
			<div class="cmt ani02">
				경기가 준비중입니다.
			</div>
		</div>
	<?php }else {?>
	<!-- 탭-코트-->
	<section class="section2">
		<div class="pop_hd">
			<div class="tit">경기 선택</div>
		</div>
		<div class="content ">
			<div class="btn-group" role="group">

				<ul class="btn-list list2">
					<?php
						$group_sql = "select division, series, series_sub from group_data where match_code = '{$c}'
						group by division, series, series_sub
						order by division, series, series_sub
						";
						$groups = sql_query($group_sql);
						$is_first = $d!='';
						$active_class = 'class="active"';

						$index = 0;
						while($row = sql_fetch_array($groups)){
							if($d==''){
								$d = $row['division'];
								$s = $row['series'];
								$ss = $row['series_sub'];
							}
							$groupqstr = "?c={$c}&d={$row['division']}&s={$row['series']}&ss={$row['series_sub']}&gi={$index}&t=L";

							?>
							<li><a <?=($index."" == $gi? $active_class : "")?> href="<?=$groupqstr?>">
								<?=$row['division']?> <?=$row['series']?> <?=$row['series_sub']?></a></li>
						<?php
						$index++;
					}?>
				</ul>
	        </div>
	    </div>
	</section>
	<!-- //탭-코트-->
	<?php if($d != ''){?>
	<section>
		<div class="content">
			<div class="con_tit_area clear">
				<div class="tit mb-5 fw-700">
					<?=$competition['wr_name']?>
				</div>
				<div class="tit">
					<?=$competition['date1']?> ~ <?=$competition['date2']?> <?=$competition['gym_name']?>
				</div>
				<div class="r-area">
					<ul>
						<li class="color5 fw-700"></li>
					</ul>
				</div>
			</div>

			<div class="tbl_style01 tbl_striped mb-20">
				<table>
					<thead>
						<tr>
							<th>순위</th>
							<th>클럽</th>
							<th>선수</th>
						</tr>
					</thead>
					<tbody class="text-center">
						<?php

							$getFinal_sql  = "select
							    (select concat(club,'-',team_1_name, '-',team_2_name ) from team_data where team_data.team_code = team_1_code ) as team_1
							    ,team_1_score
							    ,(select concat(club,'-',team_1_name, '-',team_2_name ) from team_data where team_data.team_code = team_2_code ) as team_2
							    ,team_2_score
									from game_score_data
									where match_code = '{$c}'
									and division = '{$d}'
									and series = '{$s}'
									and series_sub = '{$ss}'
									and tournament = 'C'
									and game_assign = '1' order by wr_id desc ";



									$result = sql_fetch($getFinal_sql, true);
									$first;
									$second;
									if($result['team_1_score'] > $result['team_2_score']){
										$first = explode('-',$result['team_1']);
										$second =  explode('-',$result['team_2']);
									}else{
										$second =  explode('-',$result['team_1']);
										$first =  explode('-',$result['team_2']);
									}

									print_R($first);
						 ?>
						<tr>
							<td>우승</td>
							<td><?=$first[0]?><br><?=$first[0]?></td>
							<td><?=$first[1]?><br><?=$first[2]?></td>
						</tr>
						<tr>
							<td>준우승</td>
							<td><?=$second[0]?><br><?=$second[0]?></td>
							<td><?=$second[1]?><br><?=$second[2]?></td>
						</tr>

					</tbody>
				</table>
			</div>


		</div>
	</section>

	<?php }?>

	<!-- //본선 토너먼트 ㅇ그룹 경기 결과-->
	<script>
		$('.round_tab').click(function(event){
			$('.round_tab.active').removeClass('active');
			$(this).addClass('active');
			var target = $(this).data('for');
			$('.tournament_list').hide();
			$('#'+target).show();
		})
	</script>

	 <?php }?>
</div>
<!-- end Contents Area -->

<?php
include_once(G5_AOS_PATH.'/tail.php');
?>
