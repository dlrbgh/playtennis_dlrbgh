<?php
include_once('../common.php');
include_once('./update_grade.php');

set_session('ss_mb_id', '');
$mb_id       = trim($_POST['mb_id']);
$mb_password = trim($_POST['mb_password']);
// $authToken = trim($POST['authToken']);

if($type == "tokenLogin"){
  // die(json_encode($_REQUEST));
  $mb = get_member_auth($authToken);
  update_member_grade($mb['area2'],$mb['mb_3'], $mb['mb_name']);
  $mb = get_member_auth($authToken);

  if(!$mb['mb_id']){
    die(json_encode(array("state"=>"failure")));
  }else{
    $process = "0002";
    if($mb['mb_1'] == '' || $mb['mb_2'] == '' || $mb['mb_3'] == '' || $mb['mb_4'] == ''){
      $process = '0003';
    }
    $new_token = createRandomPassword();
    // 회원아이디 세션 생성
    set_session('ss_mb_id', $mb['mb_id']);
    // FLASH XSS 공격에 대응하기 위하여 회원의 고유키를 생성해 놓는다. 관리자에서 검사함 - 110106
    set_session('ss_mb_key', md5($mb['mb_datetime'] . $_SERVER['REMOTE_ADDR'] . $_SERVER['HTTP_USER_AGENT']));

    sql_query("update g5_member set mb_10 = '{$new_token}' where mb_id = '{$mb_id}'");
    die(json_encode(array("state"=>"ok", "process"=>$process,'authToken'=>$new_token, "mb"=> $mb)));
  }
}else if($type == "googleLogin"){
  $process = "0000";
  // $mb = sql_fetch("select mb_id, mb_email, mb_name
  // ,mb_1, mb_2, mb_3
  // , (select area1 from area1 where id = mb_1) as area1
  // , (select area2 from area2 where id = mb_2) as area2
  // , (select club from club_list where id = mb_3) as club
  // , mb_4, mb_5
  // from g5_member where mb_id = '{$mb_id}'", true);

  $mb = get_member_auth($mb_id, true);
  update_member_grade($mb['area2'],$mb['mb_3'], $mb['mb_name']);
  $mb = get_member_auth($mb_id, true);
  $new_token = createRandomPassword();
  if(!$mb['mb_id']){
    regist($mb_id, $mb_name);
      $mb = get_member_auth($mb_id, true);
      update_member_grade($mb['area2'],$mb['mb_3'], $mb['mb_name']);
      $mb = get_member_auth($mb_id, true);

  }else{
    $process = "0002";
    if($mb['mb_1'] == '' || $mb['mb_2'] == '' || $mb['mb_3'] == '' || $mb['mb_4'] == ''|| $mb['mb_tel'] == ''){
      $process = '0003';
    }
  }
  // 회원아이디 세션 생성
  set_session('ss_mb_id', $mb['mb_id']);
  // FLASH XSS 공격에 대응하기 위하여 회원의 고유키를 생성해 놓는다. 관리자에서 검사함 - 110106
  set_session('ss_mb_key', md5($mb['mb_datetime'] . $_SERVER['REMOTE_ADDR'] . $_SERVER['HTTP_USER_AGENT']));

  sql_query("update g5_member set mb_10 = '{$new_token}' where mb_id = '{$mb_id}'");
  // die(json_encode($_SESSION));
  die(json_encode(array("state"=>"ok", "process"=>$process,'authToken'=>$new_token, "mb"=> $mb,"sessinoid"=>session_id())));

}else if($type == "emailLogin"){
if($mb_id != '' && $mb_password != ''){
  if (!$mb_id || !$mb_password)
    die(json_encode(array('state'=>'failure','msg'=>'회원아이디나 비밀번호가 공백이면 안됩니다.','mb_id'=>$mb_id)));
      // alert('회원아이디나 비밀번호가 공백이면 안됩니다.');


  // $mb = get_member($mb_id);

  // $mb = sql_fetch("select mb_id, mb_email, mb_name
  // ,mb_1, mb_2, mb_3
  // , (select area1 from area1 where id = mb_1) as area1
  // , (select area2 from area2 where id = mb_2) as area2
  // , (select club from club_list where id = mb_3) as club
  // , mb_4
  // from g5_member where mb_id = '{$mb_id}'", true);

  $mb = get_member_auth($mb_id, true);
  if(!$mb['mb_id']){
    // die(json_encode(array('state'=>'failure','msg'=>'아이디가 존재하지않습니다.','mb_id'=>$mb_id)));
    die(json_encode(array('state'=>'failure','errorcode'=>0000, 'msg'=>'일치하는 아이디 혹은 비밀번호가 없습니다.','mb_id'=>$mb_id)));
  }


  if( !check_password($mb_password, $mb['mb_password']) ){
    die(json_encode(array('state'=>'failure','errorcode'=>0001, 'msg'=>'일치하는 아이디 혹은 비밀번호가 없습니다.','mb_id'=>$mb_id)));
  }

  // 회원아이디 세션 생성
  set_session('ss_mb_id', $mb['mb_id']);
  // FLASH XSS 공격에 대응하기 위하여 회원의 고유키를 생성해 놓는다. 관리자에서 검사함 - 110106
  set_session('ss_mb_key', md5($mb['mb_datetime'] . $_SERVER['REMOTE_ADDR'] . $_SERVER['HTTP_USER_AGENT']));

  set_session('emailLogined', 'emailLogined');
  //0601 지금은 웹에서 이메일로그인하므로 필요없지만 나중에 사용하게된다.
  //이메일 로그인됬을때 해당 세션의 토큰정보랑 정보를 넘겨주려고 만들었음
  //메인페이지에서 체크해서 앱에서 get_Member_info을 호출한 뒤 삭제됨

  // set_session('ss_mb_id', $mb['mb_id']);

  $process = "0004";
  if($mb['mb_1'] == '' || $mb['mb_2'] == '' || $mb['mb_3'] == '' || $mb['mb_4'] == ''){
    $process = '0003';
  }

  $token = createRandomPassword();
  // die($token);
  sql_query("update g5_member set mb_10 = '{$token}' where mb_id = '{$mb_id}'");


  die(json_encode(array('state'=>'ok', 'authToken'=>$token, "mb"=> $mb)));

}else{
}


}


function get_member_auth($condition,$is_id = false){
  if($is_id){
    $sql = "select mb_id, mb_email, mb_name
    ,mb_1, mb_2, mb_3, mb_tel
    , (select area1 from area1 where id = mb_1) as area1
    , (select area2 from area2 where id = mb_2) as area2
    , ifnull((select club from club_list where id = mb_3), '') as club
    , mb_4, mb_5
    from g5_member where mb_id= '{$condition}'";
  }else{
    $sql = "select mb_id, mb_email, mb_name
    ,mb_1, mb_2, mb_3, mb_tel
    , (select area1 from area1 where id = mb_1) as area1
    , (select area2 from area2 where id = mb_2) as area2
    , ifnull((select club from club_list where id = mb_3), '') as club
    , mb_4, mb_5
    from g5_member where  mb_10  = '{$condition}'";
  }
  return sql_fetch($sql);
}


function regist($mb_id, $mb_name){

  $mb_password    = trim($_POST['mb_password']);
  $mb_password_re = trim($_POST['mb_password_re']);
  $mb_name        = trim($_POST['mb_name']);
  $mb_nick        = trim($_POST['mb_nick']);
  $mb_email       = trim($_POST['mb_email']);
  $mb_sex         = isset($_POST['mb_sex'])           ? trim($_POST['mb_sex'])         : "";
  $mb_birth       = isset($_POST['mb_birth'])         ? trim($_POST['mb_birth'])       : "";
  $mb_homepage    = isset($_POST['mb_homepage'])      ? trim($_POST['mb_homepage'])    : "";
  $mb_tel         = isset($_POST['mb_tel'])           ? trim($_POST['mb_tel'])         : "";
  $mb_hp          = isset($_POST['mb_hp'])            ? trim($_POST['mb_hp'])          : "";
  $mb_zip1        = isset($_POST['mb_zip'])           ? substr(trim($_POST['mb_zip']), 0, 3) : "";
  $mb_zip2        = isset($_POST['mb_zip'])           ? substr(trim($_POST['mb_zip']), 3)    : "";
  $mb_addr1       = isset($_POST['mb_addr1'])         ? trim($_POST['mb_addr1'])       : "";
  $mb_addr2       = isset($_POST['mb_addr2'])         ? trim($_POST['mb_addr2'])       : "";
  $mb_addr3       = isset($_POST['mb_addr3'])         ? trim($_POST['mb_addr3'])       : "";
  $mb_addr_jibeon = isset($_POST['mb_addr_jibeon'])   ? trim($_POST['mb_addr_jibeon']) : "";
  $mb_signature   = isset($_POST['mb_signature'])     ? trim($_POST['mb_signature'])   : "";
  $mb_profile     = isset($_POST['mb_profile'])       ? trim($_POST['mb_profile'])     : "";
  $mb_recommend   = isset($_POST['mb_recommend'])     ? trim($_POST['mb_recommend'])   : "";
  $mb_mailling    = isset($_POST['mb_mailling'])      ? trim($_POST['mb_mailling'])    : "";
  $mb_sms         = isset($_POST['mb_sms'])           ? trim($_POST['mb_sms'])         : "";
  $mb_1           = isset($_POST['mb_1'])             ? trim($_POST['mb_1'])           : "";
  $mb_2           = isset($_POST['mb_2'])             ? trim($_POST['mb_2'])           : "";
  $mb_3           = isset($_POST['mb_3'])             ? trim($_POST['mb_3'])           : "";
  $mb_4           = isset($_POST['mb_4'])             ? trim($_POST['mb_4'])           : "";
  $mb_5           = isset($_POST['mb_5'])             ? trim($_POST['mb_5'])           : "";
  $mb_6           = isset($_POST['mb_6'])             ? trim($_POST['mb_6'])           : "";
  $mb_7           = isset($_POST['mb_7'])             ? trim($_POST['mb_7'])           : "";
  $mb_8           = isset($_POST['mb_8'])             ? trim($_POST['mb_8'])           : "";
  $mb_9           = isset($_POST['mb_9'])             ? trim($_POST['mb_9'])           : "";
  $mb_10          = isset($_POST['mb_10'])            ? trim($_POST['mb_10'])          : "";

  $mb_name        = clean_xss_tags($mb_name);
  $mb_email       = get_email_address($mb_email);
  $mb_homepage    = clean_xss_tags($mb_homepage);
  $mb_tel         = clean_xss_tags($mb_tel);
  $mb_zip1        = preg_replace('/[^0-9]/', '', $mb_zip1);
  $mb_zip2        = preg_replace('/[^0-9]/', '', $mb_zip2);
  $mb_addr1       = clean_xss_tags($mb_addr1);
  $mb_addr2       = clean_xss_tags($mb_addr2);
  $mb_addr3       = clean_xss_tags($mb_addr3);
  $mb_addr_jibeon = preg_match("/^(N|R)$/", $mb_addr_jibeon) ? $mb_addr_jibeon : '';

  $mb_email = $mb_id;
  $sql = " insert into g5_member
              set mb_id = '{$mb_id}',
                   mb_password = '".get_encrypt_string($mb_password)."',
                   mb_name = '{$mb_name}',
                   mb_nick = '{$mb_nick}',
                   mb_nick_date = '".G5_TIME_YMD."',
                   mb_email = '{$mb_email}',
                   mb_homepage = '{$mb_homepage}',
                   mb_tel = '{$mb_tel}',
                   mb_zip1 = '{$mb_zip1}',
                   mb_zip2 = '{$mb_zip2}',
                   mb_addr1 = '{$mb_addr1}',
                   mb_addr2 = '{$mb_addr2}',
                   mb_addr3 = '{$mb_addr3}',
                   mb_addr_jibeon = '{$mb_addr_jibeon}',
                   mb_signature = '{$mb_signature}',
                   mb_profile = '{$mb_profile}',
                   mb_today_login = '".G5_TIME_YMDHIS."',
                   mb_datetime = '".G5_TIME_YMDHIS."',
                   mb_ip = '{$_SERVER['REMOTE_ADDR']}',
                   mb_level = '{$config['cf_register_level']}',
                   mb_recommend = '{$mb_recommend}',
                   mb_login_ip = '{$_SERVER['REMOTE_ADDR']}',
                   mb_mailling = '{$mb_mailling}',
                   mb_sms = '{$mb_sms}',
                   mb_open = '{$mb_open}',
                   mb_open_date = '".G5_TIME_YMD."',
                   mb_1 = '{$mb_1}',
                   mb_2 = '{$mb_2}',
                   mb_3 = '{$mb_3}',
                   mb_4 = '{$mb_4}',
                   mb_5 = '{$mb_5}',
                   mb_6 = '{$mb_6}',
                   mb_7 = '{$mb_7}',
                   mb_8 = '{$mb_8}',
                   mb_9 = '{$mb_9}',
                   mb_10 = '{$mb_10}' ";

  sql_query($sql, true);
  return $sql;
}
?>
