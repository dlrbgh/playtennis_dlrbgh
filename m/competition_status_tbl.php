<?php
include_once('../common.php');
if (!defined('_GNUBOARD_')) exit; // 개별 페이지 접근 불가
include_once(G5_AOS_PATH.'/head.php');
$menu_cate2 ='competition_view';
$menu_cate3 ='3';

?>

<!-- sub nav -->
<?php include_once('./app_sub_nav.php'); ?>
<!-- end sub nav -->

<!-- Contents Area -->
<div class="pop_container">

	<!-- 참가 접수정보-->
	<section>
		<div class="pop_hd">
			<div class="tit">내접수 현황</div>
		</div>
		<div class="content">
			<?php

				$mystatusSql = "
				select division, series, series_sub, club
				,case when team_1_name = '{$mb['mb_name']}' then team_2_name
        else team_1_name end as coPlayer
				from team_data
				where
				match_code = '{$c}'
				and ( team_1_name like '%{$mb['mb_name']}%'
				or team_2_name like '%{$mb['mb_name']}%')
				and area_1 like (select concat('%',area1,'%') from area1 where id = '{$mb['mb_1']}')
				";

				// and area_1 =(select area1 from area1 where id = '{$mb['mb_1']}')
				// and area_2 = (select area1 from area1 where id = '{$mb['mb_2']}')
				// and club = (select club from club_list where id = {$mb['mb_3']})";

				// print $mystatusSql;

				$status_result = sql_query($mystatusSql);

			 ?>
			 <ul class="info-list">
 				<?php if($status_result->num_rows <1 ){?>
 					<li style="padding:0;" class="text-center">접수내용이 없습니다</li>
 				<?php }else{?>
 				<li><span>소속클럽</span>
 					<?php
 						if($status_result->num_rows < 1){
 								print "-";

 						}?>
 				</li>
 				<li><span>참가자-1</span><?=$mb['mb_name']?></li>
 				<li><span>참가자-2</span>
 					<?php

 						if($status_result->num_rows < 1){
 							print "-";
 						}else{?>
 						<div class="tbl_style01 tbl_striped">
 						<table style="width:100%">
 							<tr>
 								<th class="text-center" style="padding:5px 3px">클럽</th>
 								<th class="text-center" style="padding:5px 3px">성별</th>
 								<th class="text-center" style="padding:5px 3px">이름</th>
 							</tr>
 							<?php

 							while($row = sql_fetch_array($status_result)){?>
 								<tr>
 									<td class="text-center"><?=$row['club']?></td>
 									<td class="text-center"><?=$row['']?>성별?</td>
 									<td class="text-center"><?=$row['coPlayer']?></td>
 								</tr>
 							<?php } ?>
 						</table>
 					</div><?php }?>
 				</li>
 				<li><span>접수현황</span>
 					<div class="tbl_style01 tbl_striped">
 						<?php
 							if($status_result->num_rows < 1){
 								print "-";
 							}else{
 						?>
 					<table style="width:100%">
 						<tr>
 							<th class="text-center" style="padding:5px 3px">그룹</th>
 							<th class="text-center" style="padding:5px 3px">상태</th>
 						</tr>
 						<?php
 						// mysql_data_seek($status_result, 0);
 						$status_result = sql_query($mystatusSql);
 						while($row = sql_fetch_array($status_result)){?>
 							<tr>
 								<td class="text-center"><?=$row['division']?> <?=$row['series']?> <?=$row['series_sub']?></td>
 								<td class="text-center">상태</td>

 							</tr>
 						<?php } ?>
 					</table>
 						<?php }?>
 				</div>
 				</li>
 				<li><span>대회장소</span>
 					<?=$competition['gym_name'] == "" ? "-" : $competition['gym_name']?></li>
 				<li><span>대회일시</span>
 					<?=$competition['date1']?> ~ <?=$competition['date2']?>
 				</li>
 				<?php }?>
 			</ul>
		</div>
	</section>
	<!-- //참가 접수정보-->

	<!-- 전체접수현황-->
	<?php
	$series_data = array();
	$series_sql = "
	SELECT *,
		DATE_FORMAT(wr_last, '%Y-%m-%d') AS format_last_date,
			CASE
				WHEN DATEDIFF(wr_last, NOW()) < 0 THEN 'end'
				ELSE 'accept'
			END is_over,
			count
			FROM
		tennis2.series_data s
			join (select division, series, series_sub, count(*)  as count
			from team_data WHERE match_code = '{$c}'
			group by division, series, series_sub) c
			on s.division = c.division and s.series = c.series and s.series_sub = c.series_sub
			WHERE
					match_code = '{$c}'
			ORDER BY s.division , s.series";
		// print $series_sql;
		$series_list = sql_query($series_sql, true);
		if($series_list->num_rows > 0){?>
	<section>
		<div class="pop_hd">
			<div class="tit">전체접수현황</div>
		</div>

		<div class="content">
			<div class="tbl_style01 tbl_striped mb-50">
				<table>
					<thead>
						<tr>
							<th>접수종류</th>
							<th>참가팀수</th>
							<th>마감일시</th>
							<!-- <th>입금여부</th> -->
						</tr>
					</thead>
					<tbody class="text-center">

						<?php 	while($row = sql_fetch_array($series_list)){
								$series_data[] = array("division"=>$row['division'],"series"=>$row['series'], "series_sub"=>$row['series_sub']);
							?>
								<tr>
									<td><?=$row['division']?> <?=$row['series']?> <?=$row['series_sub']?></td>
									<td><?=$row['format_last_date']?></td>
									<td><?=$row['count']?></td>
									<!-- <td> -->
										<?php if($row['is_over'] == 'over'){
											// print '마감';
										}else{
											// print "<a href=\"\">접수하기</a>";
										}?>

									<!-- </td> -->
								</tr>
						 <?php }?>
					</tbody>
				</table>
			</div>



			<!-- apply user status-- 0516 ghokyo 삭제
			<div class="bo_cate_area">
			    <nav id="champ_apply_user">
							<ul id="bo_cate_ul">
								<li class="active">
									<a href="#all" data-division="total" data-series="" data-series_sub="" data-a = "0" id="bo_cate_on">전체</a></li>
								<?php
									for($i = 0; $i < count($series_data); $i++){
										?>
										<li><a href="#<?=$series_data[$i]['division']?><?=$series_data[$i]['series']?><?=$series_data[$i]['series_sub']?>"
											data-division="<?=$series_data[$i]['division']?>"
											data-series="<?=$series_data[$i]['series']?>"
											data-series_sub="<?=$series_data[$i]['series_sub']?>"
											data-a = "0">
											<?=$series_data[$i]['division']?> <?=$series_data[$i]['series']?> <?=$series_data[$i]['series_sub']?></a></li>
									<?php }?>
			    		</ul>
					</nav>
			</div>-->

			<div id="court_list" class="btn-group" role="group">
				<ul id="champ_apply_user" class="btn-list list2">
					<li class="active"><a  data-division="total" data-series="" data-series_sub="" data-a = "0" href="#all">전체</a></li>
					<?php
						for($i = 0; $i < count($series_data); $i++){
							?>
					<li><a href="#<?=$series_data[$i]['division']?><?=$series_data[$i]['series']?><?=$series_data[$i]['series_sub']?>"
							data-division="<?=$series_data[$i]['division']?>"
							data-series="<?=$series_data[$i]['series']?>"
							data-series_sub="<?=$series_data[$i]['series_sub']?>"
							data-a = "0">
							<?=$series_data[$i]['division']?> <?=$series_data[$i]['series']?> <?=$series_data[$i]['series_sub']?></a></li>
					<?php }?>
				</ul>
	        </div>

			<div class="tbl_style01 tbl_striped receiptList" id="total">
				<table>
					<thead>
						<tr>
							<th>번호</th>
							<th>소속클럽</th>
							<th>신청자</th>
						</tr>
					</thead>
					<tbody></tbody>
					<tfoot class="page"></tfoot>
				</table>
			</div>

			<?php
				for($i = 0; $i < count($series_data); $i++){?>
					<div style="display:none;min-height:334px" class="tbl_style01 tbl_striped receiptList" id="<?=$series_data[$i]['division']?><?=$series_data[$i]['series']?><?=$series_data[$i]['series_sub']?>">
							<table>
								<thead>
									<tr>
										<th>번호</th>
										<th>소속클럽</th>
										<th>신청자</th>
									</tr>
								</thead>
								<tbody></tbody>
								<tfoot class="page"></tfoot>
							</table>

					</div>
				<?php }?>
			<!-- //apply user status-->
		</div>
	</section>
	<?php }?>

	<!-- //전체접수현황-->
</div>
<!-- end Contents Area -->


<script>
	var limit = 10;
	// getMatchReceiptList({division:'total',series:'', series_sub:'', c:'<?=$c?>', a : 0, limit : 10})


	$('.receiptList tfoot ').on('click', 'span', function(event){
		var dataset = $(this).data();
		var target;
		if(dataset.division == 'total'){
			target = '#total';
		}else{
			target = '#'+dataset.division+dataset.series+dataset.series_sub
		}
		getMatchReceiptList($(this).data(), target);
		$(this).addClass('active');
	});
	$('#champ_apply_user li a').click(function(event){
		event.preventDefault();
		var dataset = $(this).data();
		$(this).data('a');

		var target;
		if(dataset.division == 'total'){
			target = '#total';
		}else{
			target = '#'+dataset.division+dataset.series+dataset.series_sub
		}
		var loadmore_dataset = $(target+' tfoot span').data();
		if(loadmore_dataset == null || (loadmore_dataset != null && $(this).data('a') != 0) ){
			dataset['c'] = '<?=$c?>';
			dataset['a'] = 0;
			dataset['limit'] = 10;
			getMatchReceiptList(dataset, target);
		}else{

			$('.receiptList').hide();
			$(target).show();
		}
		$('#champ_apply_user li').removeClass('active');
		$(this).parent().addClass('active');
	});
	function getMatchReceiptList(dataset, target){
		$.ajax({
			url:g5_url+'/m/ajax/getMatchReceptUserList.php'
			,type:'post'
			,dataType:'json'
			,data:dataset
			,success:function(data){
				var list_data = data.list_data;
				var _html = '';
				for(var i in list_data){
					_html +='<tr>'
					_html +='<td class="text-center">'+list_data[i].wr_id+'</td>'
					_html +='<td class="text-center">'+list_data[i].club+'</td>'
					_html +='<td class="text-center">'+list_data[i].team_1_name+'</td>'
					_html +='</tr>'
				}
				$(target+' table tbody').append(_html);
				$(target+' table tfoot').empty().append(generatePage(dataset, data.page));

							$('.receiptList').hide();
							$(target).show();
			}
		});
	}
	function generatePage(dataset , page){

		var _html;
		var end = Math.floor(page.total/limit);
		_html = '<tr><td colspan="3"><span data-division="'+dataset.division+'" data-series="'+dataset.series+'" data-series_sub="'+dataset.series_sub+'"';
		_html += 'data-c="<?=$c?>" data-a="'+((page.cur-0)+1)+'" data-limit="'+limit+'">더보기</span></td></tr>';



		return _html;
	}

		$('#champ_apply_user li a:eq(0)').click();
</script>


<?php
include_once(G5_AOS_PATH.'/tail.php');
?>
