<?php
include_once('../common.php');
if (!defined('_GNUBOARD_')) exit; // 개별 페이지 접근 불가
include_once(G5_AOS_PATH.'/head.php');
$menu_cate2 ='competition_view';
$menu_cate3 ='2';
?>

<!-- sub nav -->
<?php include_once('./app_sub_nav.php'); ?>
<!-- end sub nav -->


<!-- Contents Area -->
<div class="pop_container">
	<!-- 참가 접수정보-->
	<section>
		<div class="pop_hd">
			<div class="tit">내접수 현황</div>
		</div>
		<div class="content">
			<?php
				$mystatusSql = "
				select division, series, series_sub, club
				,case when team_1_name = '{$mb['mb_name']}' then team_2_name
				else team_1_name end as coPlayer
				from team_data
				where
				match_code = '{$c}'
				and ( team_1_name = '{$mb['mb_name']}'
				or team_2_name = '{$mb['mb_name']}')
				and area_1 = '{$mb['mb_1']}'
				and area_2 = '{$mb['mb_2']}'
				and club = '{$mb['mb_3']}'";
				$status_result = sql_query($mystatusSql);?>
			<ul class="info-list">
				<?php if($status_result->num_rows <1 ){?>
					<li>접수내용이 없습니다</li>
				<?php }else{?>
				<li><span>소속클럽</span>
					<?php
						if($status_result->num_rows < 1){
								print "-";

						}?>
				</li>
				<li><span>참가자-1</span><?=$mb['mb_name']?></li>
				<li><span>참가자-2</span>
					<?php

						if($status_result->num_rows < 1){
							print "-";
						}else{?>
						<div class="tbl_style01 tbl_striped">
						<table style="width:100%">
							<tr>
								<th class="text-center" style="padding:5px 3px">클럽</th>
								<th class="text-center" style="padding:5px 3px">성별</th>
								<th class="text-center" style="padding:5px 3px">이름</th>
							</tr>
							<?php

							while($row = sql_fetch_array($status_result)){?>
								<tr>
									<td class="text-center"><?=$row['club']?></td>
									<td class="text-center"><?=$row['']?>성별?</td>
									<td class="text-center"><?=$row['coPlayer']?></td>
								</tr>
							<?php } ?>
						</table>
					</div><?php }?>
				</li>
				<li><span>접수현황</span>
					<div class="tbl_style01 tbl_striped">
						<?php
							if($status_result->num_rows < 1){
								print "-";
							}else{
						?>
					<table style="width:100%">
						<tr>
							<th class="text-center" style="padding:5px 3px">그룹</th>
							<th class="text-center" style="padding:5px 3px">상태</th>
						</tr>
						<?php
						// mysql_data_seek($status_result, 0);
						$status_result = sql_query($mystatusSql);
						while($row = sql_fetch_array($status_result)){?>
							<tr>
								<td class="text-center"><?=$row['division']?> <?=$row['series']?> <?=$row['series_sub']?></td>
								<td class="text-center">상태</td>

							</tr>
						<?php } ?>
					</table>
						<?php }?>
				</div>
				</li>
				<li><span>대회장소</span>
					<?=$competition['gym_name'] == "" ? "-" : $competition['gym_name']?></li>
				<li><span>대회일시</span>
					<?=$competition['date1']?> ~ <?=$competition['date2']?>
				</li>
				<?php }?>
			</ul>
		</div>
	</section>
	<!-- //참가 접수정보-->

	<!-- 전체접수현황-->


		<?php
			$series_data = array();
		$series_sql = "
		SELECT *,
			DATE_FORMAT(wr_last, '%Y-%m-%d') AS format_last_date,
				CASE
					WHEN DATEDIFF(wr_last, NOW()) < 0 THEN 'end'
					ELSE 'accept'
				END is_over,
				count
				FROM
			tennis2.series_data s
				join (select division, series, series_sub, count(*)  as count
				from team_data WHERE match_code = '{$c}'
				group by division, series, series_sub) c
				on s.division = c.division and s.series = c.series and s.series_sub = c.series_sub
				WHERE
						match_code = '{$c}'
				ORDER BY s.division , s.series";
			$series_list = sql_query($series_sql);

			if($series_list->num_rows > 0){
			?>
	<section>
		<section>
			<div class="pop_hd">
				<div class="tit">전체접수현황</div>
			</div>

			<div class="content">
				<div class="tbl_style01 tbl_striped mb-50">
					<table>
						<thead>
							<tr>
								<th>접수종류</th>
								<th>참가팀수</th>
								<th>마감일시</th>
								<!-- <th>입금여부</th> -->
							</tr>
						</thead>
						<tbody class="text-center">
							<?php
								while($row = sql_fetch_array($series_list)){
									$series_data[] = array("division"=>$row['division'],"series"=>$row['series'], "series_sub"=>$row['series_sub']);
								?>
									<tr>
										<td><?=$row['division']?> <?=$row['series']?> <?=$row['series_sub']?></td>
										<td><?=$row['count']?></td>
										<td><?=$row['format_last_date']?></td>
										<!-- <td> -->
											<?php if($row['is_over'] == 'over'){
												// print '마감';
											}else{
												// print "<a href=\"\">접수하기</a>";
											}?>

										<!-- </td> -->
									</tr>
							 <?php }?>
						</tbody>
					</table>
				</div>

	</section>
	<?php }?>
	<!-- //전체접수현황-->


</div>
<!-- end Contents Area -->

<?php
include_once(G5_AOS_PATH.'/tail.php');
?>
