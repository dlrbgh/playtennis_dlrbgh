<?php
define('_INDEX_', true);
include_once('../common.php');
if (!defined('_GNUBOARD_')) exit; // 개별 페이지 접근 불가
include_once(G5_AOS_PATH.'/head.php');
$act = "competition_relay_list";

?>

<!-- sub nav -->
<?php include_once(G5_AOS_PATH.'/inc/sub_cate.inc'); ?>
<!-- end sub nav -->
<?php
	$searchquery = $searchText == '' ? "" : "and wr_name like '%{$searchText}%'";

	$compatition_list_sql = "
			SELECT * ,
			CASE
				WHEN date_format(date1, '%Y') = date_format(date2, '%Y')
				THEN concat(date_format(date1, '%Y-%m-%d'),' ~ ', date_format(date2, '%m-%d'))
				ELSE concat(date_format(date1, '%Y-%m-%d'),' ~ ', date_format(date2, '%Y-%m-%d'))
			END AS period
			FROM match_data
			where 1 and division = 2 and app_visible = 2
			order by date1 desc
			{$searchquery}
			";
	$competition_list = sql_query($compatition_list_sql);?>



<div class="sub_container">
	<!-- bo_sch --
	<div class="quick_sch">
		<div class="sch_area">
			<form class="" action="" method="get">
				<input id="searchText" name="searchText" type="text" value="<?=$searchText?>">
				<button class="" type="submit"><i class="flaticon-search"></i></button>
			</form>
		</div>
	</div>
	<!-- bo_sch -->

	<!-- 현재 탭 이름  -->
	<section>

		<!-- list board -->
		<div class="bo_list_ul">
			<ul id="competition_list">
				<?php
				if($l=='T' || $competition_list->num_rows < 1){?>
					<li class="text-center">
						<div class="empty_waiting">
							<div class="img_area">
								<img src="<?php echo G5_IMG_URL.'/common/intro_logo1.png';?>">
							</div>
							<div class="cmt ani02">
								등록된 중계대회가 없습니다.
							</div>
						</div>
					</li>
				<?php }else{
					 while($row = sql_fetch_array($competition_list)){?>
				<li>
					<a href="<?=G5_AOS_URL?>/competition_view.php?c=<?=$row['code']?>" class="list_a">
						<div class="champ_status">
							<span class="champ_ico champ_ico<?=$row['scale']?>"></span>
							<!--<span class="limit_status">접수중</span>-->
						</div>
						<span class="list_r_area">
							<i class="flaticon-right-arrow"></i>
						</span>
						<div class="subject">
							<div class="m_subject"><?=$row['wr_name']?></div>
							<div class="s_subject"><?=$row['period']?>&nbsp;&nbsp;<?=$row['opening_place']?></div>
						</div>
					</a>
				</li>
				<?php }}?>
			</ul>
			<div class="quick_view">
				<!-- <a href="" id="more">
					더보기
				</a> -->
			</div>

		</div>
		<!-- //list board -->
	</section>
	<!-- //현재 탭 이름  -->

</div>
<!-- champ_list -->

<script>

$('ul#competition_list a').click(function(event){
	event.preventDefault();
	var link = $(this).attr('href');

	try {
										//link, title
										// console.log(link);
		window.Android.openLayer(link, '타이틀명');
	} catch (e) {
		console.log('web');
		location.href=link;
	} finally {

	}
});
	var more = {
		index : 0
		,limit : 15
		,request:function(event){
			event.preventDefault();
	        $.ajax({
	          url:g5_url+'/m/ajax/getMatchData.php'
	          ,type:'post'
						,data:{type : '',a:more.index, limit:more.limit,searchText : document.getElementById('searchText').value }
	          ,dataType:'json'
	          ,cache:false
	          ,success:more.append})
	          }
	,append:function(data){
	  var _html = '';
	  for(var i in data){
			_html += '<li>'
			_html += '<a href="./competition_view.php?c='+data[i].code+'" class="list_a">'
			_html += '<div class="champ_status">'
			_html += '<span class="champ_ico champ_ico6"></span>'
			_html += '<span class="limit_status">접수중</span>'
			_html += '</div>'
			_html += '<span class="list_r_area">'
			_html += '<i class="flaticon-right-arrow"></i>'
			_html += '</span>'
			_html += '<div class="subject">'
			_html += '<div class="m_subject">'+data[i].wr_name+'</div>'
			_html += '<div class="s_subject">'+data[i].period+'&nbsp;&nbsp;'+data[i].opening_place+'</div>'
			_html += '</div>'
			_html += '</a>'
			_html += '</li>'
	  }
	  if(data.length<more.limit){
	    _html +='<li>목록이 더 없습니다.</li>';
	    $('#more').hide();
	  }
	  $('#competition_list').append(_html);
		more.index ++;
	}}

	$('#more').click(more.request);
</script>
<?php
include_once(G5_AOS_PATH.'/tail.php');
?>
