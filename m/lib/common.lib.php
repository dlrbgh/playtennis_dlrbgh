<?php

function create_group($code,$division,$series,$series_sub,$i){
	$team = createRandomPassword();
	$sql = "insert into group_data set
			match_code	= '$code',
			code = '$team',
			num = '$i',
			division 	= '$division',
			series 	= '$series',
			series_sub 	= '$series_sub',
			tournament = 'L',
			wr_datetime = '".G5_TIME_YMDHIS."',
			mb_id = '$member[mb_id]',
			wr_ip = '{$_SERVER['REMOTE_ADDR']}',
			wr_last = '".G5_TIME_YMDHIS."'";
			sql_query($sql);
}

function create_series($code,$division,$series,$series_sub,$color){
	$sql = "insert into series_data set
			division 	= '$division',
			series 	= '$series',
			series_sub 	= '$series_sub',
			match_code	= '$code',
			color = '$color',
			wr_datetime = '".G5_TIME_YMDHIS."',
			mb_id = '$member[mb_id]',
			wr_ip = '{$_SERVER['REMOTE_ADDR']}',
			wr_last = '".G5_TIME_YMDHIS."'";
	sql_query($sql);
}

function create_game($code,$group,$tournament,$tournament_count,$tournament_num,$NO_Full_League,$tournament_array_num){
	$team = createRandomPassword();
	$sql = "insert into game_score_data set
			match_code	= '$code',
			group_code = '$group[code]',
			code = '$team',
			term = '$NO_Full_League',
			tournament	= '$tournament',
			tournament_count	= '$tournament_count',
			tournament_num = '$tournament_num',
			tournament_array_num = '$tournament_array_num',
			Full_League = '$NO_Full_League',
			division 	= '$group[division]',
			series	 	= '$group[series]',
			series_sub	= '$group[series_sub]',
			wr_datetime = '".G5_TIME_YMDHIS."',
			mb_id = '$member[mb_id]',
			wr_last = '".G5_TIME_YMDHIS."'";
	sql_query($sql);
	//단체전일 경우 동일한 경기를 2개 더 생성한다.
	if($group[division] == "단체전"){
		$team = createRandomPassword();
		$sql = "insert into game_score_data set
				match_code	= '$code',
				group_code = '$group[code]',
				code = '$team',
				term = '$NO_Full_League',
				tournament	= '$tournament',
				tournament_count	= '$tournament_count',
				tournament_num = '$tournament_num',
				tournament_array_num = '$tournament_array_num',
				Full_League = '$NO_Full_League',
				division 	= '$group[division]',
				series	 	= '$group[series]',
				series_sub	= '$group[series_sub]',
				wr_datetime = '".G5_TIME_YMDHIS."',
				mb_id = '$member[mb_id]',
				wr_last = '".G5_TIME_YMDHIS."'";
		sql_query($sql);
		$team = createRandomPassword();
		$sql = "insert into game_score_data set
				match_code	= '$code',
				group_code = '$group[code]',
				code = '$team',
				term = '$NO_Full_League',
				tournament	= '$tournament',
				tournament_count	= '$tournament_count',
				tournament_num = '$tournament_num',
				tournament_array_num = '$tournament_array_num',
				Full_League = '$NO_Full_League',
				division 	= '$group[division]',
				series	 	= '$group[series]',
				series_sub	= '$group[series_sub]',
				wr_datetime = '".G5_TIME_YMDHIS."',
				mb_id = '$member[mb_id]',
				wr_last = '".G5_TIME_YMDHIS."'";
		sql_query($sql);
	}
}

function create_league_game($code,$group,$team_1,$team_2,$tournament,$tournament_count,$tournament_num,$NO_Full_League,$term){
	if($group[division] == "개인전"){
		$team = createRandomPassword();
		$sql = "insert into game_score_data set
			team_1_code = '$team_1',
			team_2_code = '$team_2',
			match_code	= '$code',
			group_code = '$group[code]',
			code = '$team',
			term = '$term',
			tournament	= 'L',
			tournament_count = '0',
			tournament_num = '3',
			Full_League = '$NO_Full_League',
			division 	= '$group[division]',
			series	 	= '$group[series]',
			series_sub	= '$group[series_sub]',
			wr_datetime = '".G5_TIME_YMDHIS."',
			mb_id = '$member[mb_id]',
			wr_last = '".G5_TIME_YMDHIS."'";
		sql_query($sql);
	}

	//단체전일 경우 동일한 팀이 배정되지 않는 경기를 3경기 생성한다.
	if($group[division] == "단체전"){
		$team = createRandomPassword();
		$sql = "insert into game_score_data set
			team_1_code = '$team_1',
			team_2_code = '$team_2',
			match_code	= '$code',
			group_code = '$group[code]',
			code = '$team',
			term = '$term',
			tournament	= 'L',
			tournament_count = '0',
			tournament_num = '3',
			Full_League = '$NO_Full_League',
			division 	= '$group[division]',
			series	 	= '$group[series]',
			series_sub	= '$group[series_sub]',
			wr_datetime = '".G5_TIME_YMDHIS."',
			mb_id = '$member[mb_id]',
			wr_last = '".G5_TIME_YMDHIS."'";
		sql_query($sql);
		$team = createRandomPassword();
		$sql = "insert into game_score_data set
			team_1_code = '$team_1',
			team_2_code = '$team_2',
			match_code	= '$code',
			group_code = '$group[code]',
			code = '$team',
			term = '$term',
			tournament	= 'L',
			tournament_count = '0',
			tournament_num = '3',
			Full_League = '$NO_Full_League',
			division 	= '$group[division]',
			series	 	= '$group[series]',
			series_sub	= '$group[series_sub]',
			wr_datetime = '".G5_TIME_YMDHIS."',
			mb_id = '$member[mb_id]',
			wr_last = '".G5_TIME_YMDHIS."'";
		sql_query($sql);
		$team = createRandomPassword();
		$sql = "insert into game_score_data set
			team_1_code = '$team_1',
			team_2_code = '$team_2',
			match_code	= '$code',
			group_code = '$group[code]',
			code = '$team',
			term = '$term',
			tournament	= 'L',
			tournament_count = '0',
			tournament_num = '3',
			Full_League = '$NO_Full_League',
			division 	= '$group[division]',
			series	 	= '$group[series]',
			series_sub	= '$group[series_sub]',
			wr_datetime = '".G5_TIME_YMDHIS."',
			mb_id = '$member[mb_id]',
			wr_last = '".G5_TIME_YMDHIS."'";
		sql_query($sql);
	}
}

function ios_push($deviceToken,$message){

	// 개발용
	// $apnsHost = 'gateway.sandbox.push.apple.com';
	// $apnsCert = 'apns-dev.pem';

	// 실서비스용
	$apnsHost = 'gateway.push.apple.com';
	$apnsCert = 'apns.pem';

	$apnsPort = 2195;

	$payload = array('aps' => array('alert' => $message, 'badge' => 0, 'sound' => 'default'));
	$payload = json_encode($payload);

	$streamContext = stream_context_create();
	stream_context_set_option($streamContext, 'ssl', 'local_cert', $apnsCert);

	$apns = stream_socket_client('ssl://'.$apnsHost.':'.$apnsPort, $error, $errorString, 2, STREAM_CLIENT_CONNECT, $streamContext);

	if($apns)
	{
	  $apnsMessage = chr(0).chr(0).chr(32).pack('H*', str_replace(' ', '', $deviceToken)).chr(0).chr(strlen($payload)).$payload;
	  fwrite($apns, $apnsMessage);
	  fclose($apns);
	}
}
function android_push($andorid_array,$message){
	$title = "콕선생 관심경기 알림";

	// URL to POST to
	$gcm_url = 'https://android.googleapis.com/gcm/send';

	// data to be posted
	$fields = array(
		'registration_ids'  => $andorid_array,
		'data'              => array( "message" => $message,"title" => $title ),
	);

	// headers for the request
	$headers = array(
	    'Authorization:key=AIzaSyBSwFTUalt1RBrfITM0MbyQ5Ac3Q21YVGc',
	    'Content-Type: application/json'
	);

	$curl_handle = curl_init();

	// set CURL optionsa
	curl_setopt($curl_handle, CURLOPT_URL, 'https://android.googleapis.com/gcm/send');
	curl_setopt($curl_handle, CURLOPT_HTTPHEADER,  $headers);
	curl_setopt($curl_handle, CURLOPT_POST,    true);
	curl_setopt($curl_handle, CURLOPT_RETURNTRANSFER, true);
	curl_setopt($curl_handle, CURLOPT_SSL_VERIFYPEER, false);
	curl_setopt($curl_handle, CURLOPT_POSTFIELDS,json_encode($fields));
	$response = curl_exec($curl_handle);

	curl_close($curl_handle);

}
function insert_two_team($team_1_code,$team_2_code,$wr_ids,$match_code){
	$sql = " update game_score_data
		    set
		         team_1_code = '$team_1_code',
		         team_2_code = '$team_2_code'

		    where wr_id = '$wr_ids' and match_code = '$match_code'";
	sql_query($sql);
}
function insert_team_1(){
	$sql = " update game_score_data
            set
                 team_1_code = '$team_code'
            where wr_id = '$wr_ids' and match_code = '$match_code'";

	sql_query($sql);
}

function insert_team_2($team_code,$wr_ids,$match_code){
	$sql = " update game_score_data
            set
                 team_2_code = '$team_code'
            where wr_id = '$wr_ids' and match_code = '$match_code'";

	sql_query($sql);
}

function team_win_code($team){
	if($team['division'] == "단체전"){
		$sql = "select * from game_score_data where match_code = '$team[match_code]' and tournament_array_num = '$team[tournament_array_num]' and division = '$team[division]' and series = '$team[series]' and series_sub = '$team[series_sub]'";
		$result = sql_query($sql);
		$chk1 = 0;
		$chk2 = 0;
		while($team = sql_fetch_array($result)){
			if($team['team_1_score'] > $team['team_2_score']){
				$chk1++;
				$team_2_code = $team['team_1_code'];
			}else{
				$chk2++;
				$team_1_code = $team['team_2_code'];
			}
		}
		if($chk1 > $chk2){
			return $team_2_code;
		}
		if($chk2 > $chk1){
			return $team_1_code;
		}
	}else{
		if($team['team_1_score'] > $team['team_2_score']){
			$team_1_code = $team['team_1_code'];
		}else{
			$team_1_code = $team['team_2_code'];
		}
		return $team_1_code;
	}

}

function get_tourament_N_count($tournament_count,$match_code,$division,$series,$series_sub){
	$sql = "select count(wr_id) as cnt from game_score_data where tournament = 'T' and tournament_count = '$tournament_count' and end_game = 'N' and match_code = '$match_code' and division = '$division' and series = '$series' and series_sub = '$series_sub'";
	$result = sql_query($sql);
	$r = sql_fetch_array($result);
	$cnt = $r['cnt'];
	return $cnt;
}
function get_tourament_Y_count($tournament_count,$match_code,$division,$series,$series_sub){
	$sql = "select count(wr_id) as cnt from game_score_data where tournament = 'T' and tournament_count = '$tournament_count' and end_game = 'N' and match_code = '$match_code' and division = '$division' and series = '$series' and series_sub = '$series_sub'";
	$result = sql_query($sql);
	$r = sql_fetch_array($result);
	$cnt = $r['cnt'];
	return $cnt;
}

function set_championship($match_code,$division,$series,$series_sub){
	$gravity = 1;
	if($division == "단체전")
		$gravity = 3;

	$sql = "select count(wr_id) as cnt from game_score_data where tournament = 'T' and tournament_count = '2' and end_game = 'Y' and match_code = '$match_code' and division = '$division' and series = '$series' and series_sub = '$series_sub'";
	$result = sql_query($sql);
	$r = sql_fetch_array($result);
	if($r['cnt'] == 2*$gravity){
		$sql = "select count(wr_id) as cnt from game_score_data where tournament = 'C' and tournament_count = '0' and end_game = 'N' and match_code = '$match_code' and division = '$division' and series = '$series' and series_sub = '$series_sub'";
		$result = sql_query($sql);
		$r = sql_fetch_array($result);
		if($r['cnt'] == 1*$gravity){
			$tournament_create = 'C';
			$group_sql = "select * from game_score_data where tournament = 'T' and tournament_count = '2' and division = '$division' and series = '$series' and series_sub = '$series_sub' and match_code = '$match_code'";
			$result = sql_query($group_sql);
			//검색된 순위대로 그룹 랭크를 매긴다.
			$cnt = 0;
			$team_array = array();
			$team_1_code = 0;
			$team_2_code = 0;
			while($team = sql_fetch_array($result)){

				if($cnt == 0){
					$team_1_code = team_win_code($team);
				}else{
					$team_2_code = team_win_code($team);
				}
				$cnt++;
			}
			$sqls = "select * from game_score_data where tournament = 'C' and tournament_count = '0' and team_1_score = '' and team_2_score = '' and match_code = '$match_code' and division = '$division' and series = '$series' and series_sub = '$series_sub'";
			$result = sql_query($sqls);
			$r = sql_fetch_array($result);
			$tournament_array_num = $r['tournament_array_num'];
			if($division == "단체전"){
				$sql = " update game_score_data
	            set
	                 team_1_code = '$team_1_code',
	                 team_2_code = '$team_2_code'
	            where tournament_array_num = '$tournament_array_num' and match_code = '$match_code'";
				sql_query($sql);
			}else{
				$wr_ids  = $r['wr_id'];
				$sql = " update game_score_data
	            set
	                 team_1_code = '$team_1_code',
	                 team_2_code = '$team_2_code'
	            where wr_id = '$wr_ids' and match_code = '$match_code'";
				sql_query($sql);
			}
			
		}
	}
	echo $tournament_create;
}

function get_team_rank($team_field,$match_code,$division,$series,$series_sub){
	$group_sql = "select * from $team_field where match_code = '$match_code' and division = '$division' and series = '$series' and series_sub = '$series_sub' and (group_rank = '1' or group_rank = '2') ORDER BY group_rank,team_league_point desc ,winning_rate desc	,gains_losses_point desc, team_total_match_point DESC";
	$result = sql_query($group_sql);
	//검색된 순위대로 그룹 랭크를 매긴다.
	$cnt = 0;
	$team_array = array();
	while($team = sql_fetch_array($result)){
		$team_array[$cnt] = $team;
		$cnt++;
	}
	return $team_array;
}


?>
