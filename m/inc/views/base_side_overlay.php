<?php
include_once('../common.php');

$user_code = $_REQUEST['user_code'];
?>
<?php require 'inc/config.php'; ?>
<?php require 'inc/views/template_head_start.php'; ?>
<!-- Page JS Plugins CSS -->
<link rel="stylesheet" href="<?php echo $one->assets_folder; ?>/js/plugins/slick/slick.min.css">
<link rel="stylesheet" href="<?php echo $one->assets_folder; ?>/js/plugins/slick/slick-theme.min.css">
<link rel="stylesheet" href="<?php echo $one->assets_folder; ?>/js/plugins/select2/select2.min.css">
<link rel="stylesheet" href="<?php echo $one->assets_folder; ?>/js/plugins/select2/select2-bootstrap.min.css">
<?php require 'inc/views/template_head_end.php'; ?>

<!-- Side Overlay-->
<aside id="side-overlay">
    <!-- Side Overlay Scroll Container -->
    <div id="side-overlay-scroll">
        <!-- Side Header -->
        <div class="side-header side-content">
            <!-- Layout API, functionality initialized in App() -> uiLayoutApi() -->
            <button class="btn btn-default pull-right" type="button" data-toggle="layout" data-action="side_overlay_close">
                <i class="fa fa-times"></i>
            </button>
            <span>
                <!--검색-->
            </span>
        </div>
        <!-- END Side Header -->

        <!-- Side Content -->
        <form action="./search_result.php" method="Get">
        	<input type="hidden" name="user_code" value="<?=$user_code;?>">
	        <div class="side-content remove-padding-t">
	            <div class="block pull-r-l">
	                <div class="block-header bg-gray-lighter">
	                    <h3 class="block-title">검색</h3>
	                </div>
	                <div class="block-content">
	                    <!-- search -->
	                    <form class="form-horizontal" action="base_forms_pickers_more.php" method="post" onsubmit="return false;">
							<div class="form-group push-10">
				                <div class="col-md-12">
				                    <select class="js-select2 form-control" id="search_type" name="search_type" style="width:100%;height:44px;" data-placeholder="검색">
				                        <option>전체</option>
				                        <option value="1">이름</option>
				                        <option value="2">대회명</option>
				                    </select>
				                </div>
				            </div>
				            <div class="form-group push-10">
				            	<div class="col-md-12">
	                                <input class="form-control" type="text" id="search_name" name="search_name" placeholder="검색어를 입력하세요." autocomplete="off">
	                            </div>
				            </div>
				            <div class="form-group push-10">
				            	<div class="col-md-12">
	                            	<button class="btn btn-block btn-mobile push-10">확 인</button>
	                            </div>
				            </div>
				        </form>
	                    <!-- END search -->
	                </div>
	            </div>
	        </div>
        </form>
        <!-- END Side Content -->
    </div>
    <!-- END Side Overlay Scroll Container -->
</aside>
<!-- END Side Overlay -->

<?php require 'inc/views/base_footer.php'; ?>
<?php require 'inc/views/template_footer_start.php'; ?>


<!-- Page JS Plugins -->

<script src="<?php echo $one->assets_folder; ?>/js/plugins/datatables/jquery.dataTables.min.js"></script>
<script src="<?php echo $one->assets_folder; ?>/js/plugins/select2/select2.full.min.js"></script>

<!-- Page JS Code -->
<script src="<?php echo $one->assets_folder; ?>/js/pages/base_tables_datatables.js"></script>

<script>
    jQuery(function(){
        // Init page helpers (BS Datepicker + BS Datetimepicker + BS Colorpicker + BS Maxlength + Select2 + Masked Input + Range Sliders + Tags Inputs plugins)
        App.initHelpers(['datepicker', 'datetimepicker', 'colorpicker', 'maxlength', 'select2', 'masked-inputs', 'rangeslider', 'tags-inputs']);
    });
</script>
<?php require 'inc/views/template_footer_end.php'; ?>
