<?php
include_once('../common.php');

$wr_id = $_REQUEST['wr_id'];
?>
<?php require 'inc/config.php'; ?>
<?php require 'inc/views/template_head_start.php'; ?>


<?php require 'inc/views/template_head_end.php'; ?>


<?php 
	$sql = "select * from match_data where wr_id = '$wr_id' order by wr_id desc";
	$result = sql_query($sql);
	$i = 1;
	$r = sql_fetch_array($result);
    $code = $r['code'];
?>

<div class="push-10 top_nav" style="top: 0;">
	<ul>
		<li>
			<a href="index.php">홈</a>
		</li>
		<li>
			<a  href="championship_info.php?wr_id=<?=$wr_id?>">정보</a>
		</li>
		<li>
			<a  href="champ_info_apply.php?wr_id=<?=$wr_id?>">접수현황</a>
		</li>
		<li>
			<a href="champ_info_matchtbl_level.php?wr_id=<?=$wr_id?>&user_code=<?=$user_code;?>">대진표</a>
		</li>
		<li>
			<a href="champ_info_match.php?wr_id=<?=$wr_id?>">경기진행</a>
		</li>
		<li>
			<a class="active" href="champ_info_endmatch.php?wr_id=<?=$wr_id?>">결과</a>
		</li>
	</ul>
</div>

<!-- Contents Area -->
	<div class="content" style="margin-top: 42px;">
<!-- 카테고리 -->
		<div class="btn-group btn-group-justified push-10">
	        <div class="btn-group">
	            <a href="champ_info_endmatch.php?wr_id=<?=$wr_id;?>"><button class="btn btn-lg btn-white remove-padding " type="button">남자복식</button></a>
	        </div>
	        <div class="btn-group">
	            <a href="champ_info_endmatch_woman.php?wr_id=<?=$wr_id;?>"><button class="btn btn-lg btn-white remove-padding" type="button">여자복식</button></a>
	        </div>
	        <div class="btn-group">
	            <a href="champ_info_endmatch_mix.php?wr_id=<?=$wr_id;?>"><button class="btn btn-lg btn-white remove-padding" type="button">혼합복식</button></a>
	        </div>
	        <div class="btn-group">
	            <a href="champ_info_endmatch_point.php?wr_id=<?=$wr_id;?>"><button class="btn btn-lg btn-white remove-padding active" type="button">단체전</button></a>
	        </div>
		</div>
	    <!-- end 카테고리 -->
	    <!-- 접수현황 -->
	    <div class="row push-20">
	        <div class="col-lg-12">
	            <!-- 경기 결과 우승자 출력 -->
	            <div class="block">
	                <div class="block-content remove-padding">
	                    <table class="table  champ_result table-mobile">
	                        <thead>
	                        	<th class="text-center">순위</th>
	                        	<th class="text-center">클럽명</th>
	                        	<th class="text-center">점수</th>
	                        </thead>
	                        <tbody>
			                	 <tr>
	                            	<td>1위</td>
	                            	<td>클럽명</td>
	                            	<td>합계점수</td>
	                            </tr>
								
	                        </tbody>
	                    </table>
	                </div>
	            </div>
	            <!-- end 경기 결과 우승자 출력 -->
	        </div>
	        
	    </div>
	</div>
<!-- end Contents Area -->


<?php require 'inc/views/base_footer.php'; ?>
<?php require 'inc/views/template_footer_start.php'; ?>







<?php require 'inc/views/template_footer_end.php'; ?>

