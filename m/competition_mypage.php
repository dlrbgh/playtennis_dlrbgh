<?php
include_once('../common.php');
if (!defined('_GNUBOARD_')) exit; // 개별 페이지 접근 불가
include_once(G5_AOS_PATH.'/head.php');
$menu_cate2 ='competition_view';
$menu_cate3 ='6';

$team_field = "team_data";

if($division == "단체전"){
	$team_field = "team_event_data";
}


$mb_id = get_session('ss_mb_id');
$mb_id = 'iflikesomthiing@gmail.com';

$mb = get_member($mb_id);
// print_r($mb);

$match_data = sql_fetch("select * from match_data where code = '$c'");

$area_1s =sql_fetch("select area1 from area1 where id = '$mb[mb_1]'");
$area_1 = trim($area_1s['area1']);

$area_2s =sql_fetch("select area2 from area2 where id = '$mb[mb_2]'");
$area_2 = trim($area_2s['area2']);

// print $area_2;
// print_r($mb);
$club_lists =sql_fetch("select club from club_list where id = '$mb[mb_3]'");
$club = trim($club_lists['club']);

$team_data = sql_fetch("select team_code from $team_field where
match_code='$c' and
(area_2 = '$area_2' and team_1_name='{$mb['mb_name']}'
or team_2_area = '$area_2' and team_2_name = '{$mb['mb_name']}'
)");

$team_code = trim($team_data['team_code']);


//단체전 대응 필요함. 현재 개인전/참가안함은 동작하는 것으로 보임


?>


<!-- sub nav -->
<?php include_once('./app_sub_nav.php'); ?>
<!-- end sub nav -->


<!-- Contents Area -->
<div class="pop_container">
<?php if($team_code==''){  ?>
		<!-- 준비중 -->
		<div class="empty_waiting">
			<div class="img_area">
				<img src="<?php echo G5_IMG_URL.'/common/intro_logo1.png';?>">
			</div>
			<div class="cmt ani02">
				참가신청 내역이 없습니다.
			</div>
		</div>
		<!-- //준비중 -->
<?php }else{?>

	<section>
		<div class="pop_hd">
			<div class="tit"><?=$mb['mb_name']?>님 경기 정보</div>
		</div>
		<div class="content">
			<!-- <?php
				$mystatusSql = "
				select division, series, series_sub, club
				,case when team_1_name = '{$mb['mb_name']}' then team_2_name
				else team_1_name end as coPlayer
				from team_data
				where
				match_code = '{$c}'
				and ( team_1_name = '{$mb['mb_name']}'
				or team_2_name = '{$mb['mb_name']}')
				and area_1 like '%{$area_1}%'
				and area_2 like '%{$area_2}%'
				and club like '%{$club}%'";
				//print $mystatusSql;
				$status_result = sql_query($mystatusSql);
				$row = sql_fetch_array($status_result);
				print_r ($row);
			 ?> -->

			<div class="con_tit_area clear">
				<div class="tit mb-15 fw-700">
					<!-- 개인전 남자 챌린저부-->
					<?php echo $row['division'].'-'.$row['series'].'-'.$row['series_sub']?>
				</div>
				<div class="tit color5">
				<?php
					if($row['division'] == "단체전"){
					$game_info = sql_fetch("select * from game_score_data where match_code='$c' and (team_1_event_code = '{$team_code}' or team_2_event_code = '{$team_code}') order by court_array_num asc");
						echo $game_info;
					}else{
					$game_info = sql_fetch("select * from game_score_data where match_code='$c' and (team_1_code='{$team_code}' or team_2_code='{$team_code}') order by court_array_num asc");
					}

					if($ $row['division']== '단체전'){
						echo "단체전";
						echo $game_info['game_date'];
						$gym = sql_fetch("select * from gym_data where wr_id = '{$game_info['gym_code']}'");
						echo "<br />첫경기 : ".$gym['gym_name']." ".$game_info['game_court']."코트  ".$game_info['court_array_num']."번째 경기";
					}else{
						echo $game_info['game_date'];
						$gym = sql_fetch("select * from gym_data where wr_id = '{$game_info['gym_code']}'");
						echo "<br />첫경기 : ".$gym['gym_name']." ".$game_info['game_court']."코트  ".$game_info['court_array_num']."번째 경기";
					}
				?>
					<br/>
				</div>
			</div>

			<!-- 단체전 선수배정-->
			<!-- <div class="group_match_area">
				<ul>
					<li>단체전1경기 <span class="color5">선수를 배정하세요</span> <a href="popup_team_event_set.php" target="_blank">선수배정</a></li>
				</ul>
			</div> -->
			<!-- //단체전 선수배정-->
		</div>


	</section>


<style>
.group_match_area ul li{position:relative;display:block;line-height:20px}
.group_match_area ul li a{position:absolute;right:0;padding:5px 15px;border:1px solid #ccc;bottom:-3px;width:100px;text-align:center}
.group_match_area ul li a:active{background-color:#14264a;color:#fff}
.group_match_area ul li span{font-size:13px}
</style>

<?php

	$i = 0;
	$sql = "select * from group_data where match_code='$c'
	and (team_1 = '$team_code' or team_2 = '$team_code' or team_3 = '$team_code')
	and tournament	='L' order by num";
	$gourp_result = sql_query($sql);

	while($group = sql_fetch_array($gourp_result)){
		$group_code = $group['code'];
		// print $group_code;
		$gym_name = sql_fetch("select gym_name from gym_data where wr_id = (SELECT gym_code FROM game_score_data where group_code = '{$group_code}' group by gym_code limit 1)");
		//$game_status = is_on($group_code);

?>
	<section>
		<div class="content">
			<div class="con_tit_area clear">
				<div class="tit"><?=$group['division']?> <?=$group['series']?> <?=$group['series_sub']?>-<?php echo $group['num']+1?>조</div>
				<div class="r-area">
					<ul>
						<li class="color5 fw-700"><?=$gym_name['gym_name']?> <?=$group['game_court']?>코트 <?=$game_status['status_display']?></li>
					</ul>
				</div>
			</div>
			<div class="tbl_style01 tbl_striped mb-20">
				<table>
					<?php
						$team_index = 1;
						$team_result_data = array();
						while($group['team_'.$team_index] != ''){
							$group_team_code = $group['team_'.$team_index];
							$team_sql = "select * from $team_field
							where match_code = '$c'
							and team_code = '{$group_team_code}'";
							$team_result = sql_query($team_sql);
							$r = sql_fetch_array($team_result);
							$team_result_data[] =$r;
							$team_index++;}
					?>
					<thead>
						<tr>
							<th>
								<?php
								if($game_status['status']==3){
									echo "순위";
								}else{
									echo "번호";
								}
								?>

							</th>
								<th>클럽</th>
								<th>선수</th>
								<th>승</th>
								<th>패</th>
							</tr>
					</thead>
					<tbody>
						<?php
							usort($team_result_data, "cmp");
							$ranking  = 1;
							foreach($team_result_data as $key => $value){

								$go_tournament = '';
								if($game_status['status'] == 3 && $ranking == 1){
									// $go_tournament = '<br />토너먼트 진출';
								}
						?>
						<tr>
							<td><?=$ranking++?>
								<?php
								if($game_status['status']==3){
									echo "위";
								}else{
									echo "";
								}
								?>
								<?=$go_tournament?>
							</td>
							<td><?=$value['area_2'] ?>-<?=$value['club']?></td>
							<td>
								<?=$value['team_1_name'];?>(<?=$value['team_1_grade'];?>)<br>
								<?=$value['team_2_name'];?>(<?=$value['team_2_grade'];?>)<br>
								<?php if($d == "단체전"){ ?>
									<?=$value['team_3_name'];?>(<?=$value['team_3_grade'];?>)<br>
									<?=$value['team_4_name'];?>(<?=$value['team_4_grade'];?>)<br>
									<?=$value['team_5_name'];?>(<?=$value['team_5_grade'];?>)<br>
									<?=$value['team_6_name'];?>(<?=$value['team_6_grade'];?>)<br>
									<?php if($value['team_7_name']!=""){echo $value['team_7_name'];echo "(";echo $value['team_7_grade'];echo ")";}?><br>
									<?php if($value['team_8_name']!=""){echo $value['team_8_name'];echo "(";echo $value['team_8_grade'];echo ")";}?><br>
									<?php } ?>
							</td>
							<td><?=$value['team_league_point']?>승</td>
							<td><?=$value['team_league_lose_count']?>패</td>
						</tr>
						<?php $team_index++;}?>
					</tbody>
				</table>
			</div>
		</div>
	</section>
<?php } ?>


	<section>
		<div class="con_tit_area clear">
			<div class="tit">내경기 현황</div>
		</div>
		<div class="tbl_style02 tbl_striped">
			<table>
				<thead>
					<tr>
						<th>경기</th>
						<th>클럽</th>
						<th>이름</th>
						<th>점수</th>
						<th>이름</th>
						<th>클럽</th>
					</tr>
				</thead>
				<tbody>
					<?php
					$game_score_sql = "select * from game_score_data
					where (team_1_code = '{$team_code}' or team_2_code = '{$team_code}')
					and match_code='$c'
					and tournament = 'L' ";

					$game_score = sql_query($game_score_sql);

					while($row = sql_fetch_array($game_score)){

						if($row['division'] == '단체전'){
							$team_1_info = sql_fetch("select * from $team_field where team_code = '{$row['team_1_event_code']}'");
							$team_2_info = sql_fetch("select * from $team_field where team_code = '{$row['team_2_event_code']}'");
						}else{
							$team_1_info = sql_fetch("select * from team_data where team_code = '{$row['team_1_code']}'");
							$team_2_info = sql_fetch("select * from team_data where team_code = '{$row['team_2_code']}'");
						}

					?>
					<tr>
						<td class="text-center"><?=$game_result_list[$row['code']]?><br>경기</td>
						<td class="text-center"><?= $team_1_info['club']?><br><?= $team_1_info['team_2_club']?></td>
						<td class="text-center">
							<?php if($team_1_info['team_1_name']!=""){echo $team_1_info['team_1_name'];echo "(";echo $team_1_info['team_1_grade'];echo ")";}?><br>
							<?php if($team_1_info['team_2_name']!=""){echo $team_1_info['team_2_name'];echo "(";echo $team_1_info['team_2_grade'];echo ")";}?><br>
							<!-- <?=$team_1_info['team_2_name'];?>(<?=$team_1_info['team_2_grade'];?>)<br> -->
							<?php if($d == "단체전"){ ?>
								<!-- <?=$team_1_info['team_3_name'];?><br>
								<?=$team_1_info['team_4_name'];?><br>
								<?=$team_1_info['team_5_name'];?><br>
								<?=$team_1_info['team_6_name'];?><br>
								<?=$team_1_info['team_7_name'];?><br>
								<?=$team_1_info['team_8_name'];?><br> -->
							<?php } ?>
						</td>
						<td class="match_point text-center">
							<?php if($row['team_1_dis'] > 0){
								print '<span>기권</span> : 6';
							}else if($row['team_2_dis'] > 0){
								print '<span>6</span> :
	<span>기권</span>';
							}else{
								 if($row['is_on'] == 'N' && $row['end_game'] == 'N'){?>
								<!-- <div><span></span>&nbsp;준비중&nbsp;<span></span></div> -->
								<div class="">
									<span style="font-size:16px">준비중</span>
								</div>
							<?php }else if($row['end_game'] == 'Y'){?>
								<div><span class="win"><?=$row['team_1_score']?></span>&nbsp;:&nbsp;<span><?=$row['team_2_score']?></span></div>
								<?php }else if($row['is_on'] == 'Y' && $row['game_start_time'] != '')
														{?>
								<div class="playing ani01">진행중</div>
							<?php }

							}?>
							<!-- <span><?= $row['team_1_score']?></span>&nbsp;:&nbsp;
							<span><?= $row['team_2_score']?></span> -->
						</td>
						<td class="text-center">
							<?php if($team_2_info['team_1_name']!=""){echo $team_2_info['team_1_name'];echo "(";echo $team_2_info['team_1_grade'];echo ")";;}?><br>
							<?php if($team_2_info['team_2_name']!=""){echo $team_2_info['team_2_name'];echo "(";echo $team_2_info['team_2_grade'];echo ")";}?><br>
							<!-- <?=$team_2_info['team_1_name'];?>(<?=$team_2_info['team_1_grade'];?>)<br>
							<?=$team_2_info['team_2_name'];?>(<?=$team_2_info['team_2_grade'];?>)<br> -->
							<?php if($d == "단체전"){ ?>
								<!-- <?=$team_2_info['team_3_name'];?><br>
								<?=$team_2_info['team_4_name'];?><br>
								<?=$team_2_info['team_5_name'];?><br>
								<?=$team_2_info['team_6_name'];?><br>
								<?=$team_2_info['team_7_name'];?><br>
								<?=$team_2_info['team_8_name'];?><br> -->
								<?php } ?>
						</td>
						<td class="text-center"><?= $team_2_info['club']?><br><?= $team_2_info['club']?></td>
					</tr>
					<?php }?>
				</tbody>
			</table>
		</div>
	</section>
<?php }?>
</div>
<!-- end Contents Area -->

<?php
function cmp($a, $b){
	return ($b[3]-0) -($a[3]-0);
}
include_once(G5_AOS_PATH.'/tail.php');
?>
