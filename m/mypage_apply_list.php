<?php
include_once('../common.php');
if (!defined('_GNUBOARD_')) exit; // 개별 페이지 접근 불가
include_once(G5_AOS_PATH.'/head.php');
$menu_cate2 ='mypage';
$menu_cate3 ='2';
?>

<!-- sub nav -->
<?php include_once('./app_sub_nav.php'); ?>
<!-- end sub nav -->

<div class="pop_container">
		<!-- 준비중 -->
		<div class="empty_waiting">
			<div class="img_area">
				<img src="<?php echo G5_IMG_URL.'/common/intro_logo1.png';?>">
			</div>
			<div class="cmt ani02">
				참가기록이 없습니다.
			</div>
		</div>
		<!-- //준비중 -->
</div>
<!-- champ_list -->

<script>

	var more = {
		index : 0
		,limit : 15
		,request:function(event){
			event.preventDefault();
	        $.ajax({
	          url:g5_url+'/m/test.php'
	          ,type:'post'
						,data:{index:more.index}
	          ,dataType:'json'
	          ,cache:false
	          ,success:more.append})
	          }
	,append:function(data){
	  var _html = '';
	  for(var i in data){
			_html += '<li>'
			_html += '<a href="./competition_view.php?c='+data[i].code+'" class="list_a">'
			_html += '<div class="champ_status">'
			_html += '<span class="champ_ico champ_ico6"></span>'
			_html += '<span class="limit_status">접수중</span>'
			_html += '</div>'
			_html += '<span class="list_r_area">'
			_html += '<i class="flaticon-right-arrow"></i>'
			_html += '</span>'
			_html += '<div class="subject">'
			_html += '<div class="m_subject">'+data[i].wr_name+'</div>'
			_html += '<div class="s_subject">'+data[i].period+'&nbsp;&nbsp;'+data[i].opening_place+'</div>'
			_html += '</div>'
			_html += '</a>'
			_html += '</li>'
	  }
	  if(data.length<more.limit){
	    _html +='<li>목록이 더 없습니다.</li>';
	    $('#more').hide();
	  }
	  $('#competition_list').append(_html);
		more.index ++;
	}}

	$('#more').click(more.request);
</script>
<?php
include_once(G5_AOS_PATH.'/tail.php');
?>
