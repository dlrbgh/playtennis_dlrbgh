<?php
include_once('../common.php');

$user_code = $_REQUEST['user_code'];
$user_club = $_REQUEST['user_club'];
$user_name = $_REQUEST['user_name'];

?>

<?php require 'inc/config.php'; ?>
<?php require 'inc/views/template_head_start.php'; ?>

<!-- Page JS Plugins CSS -->
<link rel="stylesheet" href="<?php echo $one->assets_folder; ?>/js/plugins/slick/slick.min.css">
<link rel="stylesheet" href="<?php echo $one->assets_folder; ?>/js/plugins/slick/slick-theme.min.css">

<?php require 'inc/views/template_head_end.php'; ?>

<div class="menu_snav top_nav" id="home_tabs"  style="top: 0;">
	<a class=""  href="index.php?user_code=<?=$user_code;?>&user_club=<?=$user_club;?>&user_name=<?=$user_name;?>">진행중 대회</a>
	<a class="active" href="<?php echo G5_MOBILE_URL ?>/match_end.php?user_code=<?=$user_code;?>&user_club=<?=$user_club;?>&user_name=<?=$user_name;?>">종료된 대회</a>
</div>

<!-- end 현재 위치 -->
<!-- champ_list -->
<div class="main_content" style="margin-top: 42px;">	
	<ul class="main_list">
		<?php 
        	$sql = "select * from match_data where end_game = '5' and app_visible = '1' order by wr_id desc";
			$result = sql_query($sql);
			$i = 1;
        ?>
        <?php while($r = sql_fetch_array($result)) { 
       		$match_sql = "select * from match_gym_data where match_id='$r[code]' order by application_period";
			$match_result = sql_query($match_sql);
			$match = sql_fetch_array($match_result);
            $urls = "currentstatus_table.php?code=$r[code]&gym_id=$match[gym_id]&application_period=$match[application_period]";

			$img_sql = "select * from g5_board_file where wr_id = '$r[wr_id]' and bf_no = '1'";
			$img_result = sql_query($img_sql);
			$img = sql_fetch_array($img_result);      

			$sqled = "select * from series_data where match_code = '$r[code]' group by division";
			$resulted = sql_query($sqled);
			$seriesss = sql_fetch_array($resulted);
        ?>
        <a href="<?php echo G5_URL?>/m/champ_info_endmatch.php?wr_id=<?=$r['wr_id'];?>&user_code=<?=$user_code;?>&user_club=<?=$user_club;?>&user_name=<?=$user_name;?>&division=<?=$seriesss['division']?>">
		<li class="push-30">
			<!-- img area -->
			<div class="ribbon ribbon-modern ribbon-crystal ribbon-top">
				<div class="ribbon-box font-w600 text-uppercase">대회종료</div>
					<img class="img-responsive" src="../data/file/match/<?=$img['bf_file']?>" />
			</div>
            <!-- end img area -->
			<!-- champ_info area -->
			<div class="m_champ_info">
			 	<div class="font-w700">
			 		<?=$r['wr_name']?>
			 	</div>
			 	<div class="period">
			 		<?=$r['date1']?> ~ <?=$r['date2']?>
			 	</div>
			</div>
			</a>
			<!-- end champ_info area -->
		</li>

    	<?php $i++;} ?>
	</ul>
</div>
<!-- end champ_list -->

<?php require 'inc/views/base_footer.php'; ?>
<?php require 'inc/views/template_footer_start.php'; ?>


<!-- Page JS Plugins -->
<script src="<?php echo $one->assets_folder; ?>/js/plugins/datatables/jquery.dataTables.min.js"></script>

<!-- Page JS Code -->
<script src="<?php echo $one->assets_folder; ?>/js/pages/base_tables_datatables.js"></script>
<?php require 'inc/views/template_footer_end.php'; ?>

