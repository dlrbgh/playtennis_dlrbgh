<?php
include_once('../common.php');

$user_code = $_REQUEST['user_code'];
$user_club = $_REQUEST['user_club'];
$user_name = $_REQUEST['user_name'];

?>
<?php require 'inc/config.php'; ?>
<?php require 'inc/views/template_head_start.php'; ?>

<!-- Page JS Plugins CSS -->
<link rel="stylesheet" href="./assets/js/plugins/slick/slick.min.css">
<link rel="stylesheet" href="./assets/js/plugins/slick/slick-theme.min.css">
<link rel="stylesheet" href="./assets/js/plugins/select2/select2.min.css">
<link rel="stylesheet" href="./assets/js/plugins/select2/select2-bootstrap.min.css">
<?php require 'inc/views/template_head_end.php'; ?>

<!-- Side Overlay-->
    <!-- Side Overlay Scroll Container -->
    <div id="side-overlay-scroll" style="background-color:#fff;height:100%;">
        <!-- Side Header -->
        
        <!-- END Side Header -->

        <!-- Side Content -->
        <form action="./search_result.php" method="Get">
	        <div class="side-content remove-padding-t">
	        	<input type="hidden" name="user_code" value="<?=$user_code;?>" />
	        	<input type="hidden" name="user_club" value="<?=$user_club;?>" />
	        	<input type="hidden" name="user_name" value="<?=$user_name;?>" />

	            <div class="block pull-r-l">
	                <div class="block-header bg-gray-lighter">
	                    <h3 class="block-title">검색</h3>
	                </div>
	                <div class="block-content">
	                    <!-- search -->
	                    <form class="form-horizontal" action="base_forms_pickers_more.php" method="post" onsubmit="return false;">
							<div class="col-md-12 push-10">
			                    <div class="push-5 h5 font-w700">이름 또는 대회명을 선택하세요<br/></div>
			                </div>
							<div class="form-group push-10">
				                <div class="col-md-12">
				                    <select class="js-select2 form-control" id="search_type" name="search_type" style="width:100%;height:44px;" data-placeholder="검색">
				                        <option value="1">이름</option>
				                        <option value="2">대회명</option>
				                    </select>
				                </div>
				            </div>
				            <div class="form-group push-10">
				            	<div class="col-md-12">
	                                <input class="form-control" type="text" id="search_name" name="search_name" placeholder="검색어를 입력해 주세요" autocomplete="off">
	                            </div>
				            </div>
				            <div class="form-group push-10">
				            	<div class="col-md-12">
	                            	<button class="btn btn-block btn-mobile push-10">확 인</button>
	                            </div>
				            </div>
				        </form>
	                    <!-- END search -->
	                    
	                </div>
	            </div>
	        </div>
        </form>
        <!-- END Side Content -->
    </div>
    <!-- END Side Overlay Scroll Container -->
<!-- END Side Overlay -->
