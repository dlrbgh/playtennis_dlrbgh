<?php
include_once('../common.php');

$wr_id = $_REQUEST['wr_id'];
$user_code	= $_REQUEST['user_code'];
$act = 'champ_info_apply'
?>
<?php require 'inc/config.php'; ?>
<?php require 'inc/views/template_head_start.php'; ?>

<!-- Page JS Plugins CSS -->
<link rel="stylesheet" href="<?php echo $one->assets_folder; ?>/js/plugins/slick/slick.min.css">
<link rel="stylesheet" href="<?php echo $one->assets_folder; ?>/js/plugins/slick/slick-theme.min.css">
<link rel="stylesheet" href="<?php echo $one->assets_folder; ?>/js/plugins/select2/select2.min.css">
<link rel="stylesheet" href="<?php echo $one->assets_folder; ?>/js/plugins/select2/select2-bootstrap.min.css">
<?php require 'inc/views/template_head_end.php'; ?>

<?php 
	$sql = "select * from match_data where wr_id = '$wr_id' order by wr_id desc";
	$result = sql_query($sql);
	$r = sql_fetch_array($result);
	$code = $r['code'];                    
?>

<!-- sub nav -->
<?php include_once('./app_sub_nav.php'); ?>
<!-- end sub nav -->

<!-- Contents Area -->
	
	<div class="content" style="margin-top: 42px;">
	
		<div class="btn-group btn-group-justified push-10">
	        <div class="btn-group">
	            <a href="champ_info_apply.php?wr_id=<?=$wr_id;?>&user_code=<?=$user_code;?>&user_club=<?=$user_club;?>&user_name=<?=$user_name;?>"><button class="btn btn-lg btn-white " type="button">급수별</button></a>
	        </div>
	        <div class="btn-group">
	            <a href="champ_info_area.php?wr_id=<?=$wr_id;?>&user_code=<?=$user_code;?>&user_club=<?=$user_club;?>&user_name=<?=$user_name;?>"><button class="btn btn-lg btn-white active" type="button">지역별</button></a>
	        </div>
		</div>
	    
	    <!-- 접수현황 -->
	    <div class="row">
	        <div class="col-lg-6">
	            <!-- 지역별 접수 현황 Table -->
	            <div class="block">
	                <div class="block-content remove-padding">
	                    <table class="table table-vcenter champ_result table-mobile">
	                        <thead>
	                            <tr>
	                                <th class="text-center" style="">광역시/도</th>
	                                <th class="text-center" style="">시/군/구</th>
	                                <th class="text-center" style="">남복</th>
	                                <th class="text-center" style="">여복</th>
	                                <th class="text-center" style="">혼복</th>
	                                <th class="text-center" style="">합계</th>
	                            </tr>
	                        </thead>
	                        <tbody>
	                        <?php 
							$sql = "select area_1 from team_data where match_code = '$r[code]' GROUP BY area_1";
							$division_result = sql_query($sql);
							$a_sum = 0;
							$b_sum = 0;
							$c_sum = 0;
							while($area_1 = sql_fetch_array($division_result)){
								$series_sql = "select area_2 from team_data where area_1 = '$area_1[area_1]' and match_code = '$r[code]' GROUP BY area_2";
								$series_result = sql_query($series_sql);				
								$i = 0;					
									while($area_2 = sql_fetch_array($series_result)){
										$a_sql = "select count(wr_id) as cnt from team_data where match_code = '$r[code]' and area_1 = '$area_1[area_1]' and area_2 = '$area_2[area_2]' and division = '남복'";
	                        			$a_result = sql_query($a_sql);
										$a = sql_fetch_array($a_result);
										
										$b_sql = "select count(wr_id) as cnt from team_data where match_code = '$r[code]' and area_1 = '$area_1[area_1]' and area_2 = '$area_2[area_2]' and division = '여복'";
	                        			$b_result = sql_query($b_sql);
										$b = sql_fetch_array($b_result);
										
										$c_sql = "select count(wr_id) as cnt from team_data where match_code = '$r[code]' and area_1 = '$area_1[area_1]' and area_2 = '$area_2[area_2]' and division = '혼복'";
	                        			$c_result = sql_query($c_sql);
										$c = sql_fetch_array($c_result);
										
										$a_sum = $a_sum + $a['cnt'];
										$b_sum = $b_sum + $b['cnt'];
										$c_sum = $c_sum + $c['cnt'];
										
																		
										if($i == 0){
											$cnt_sql = "select wr_id as cnt from team_data where area_1 = '$area_1[area_1]' and match_code = '$r[code]'  group by area_2";
				                        	$cnt_result = sql_query($cnt_sql);
				                        	$cnt = 0;
											while($sub = sql_fetch_array($cnt_result)){
												$cnt++;
											}		
									?>
								 	<tr class="text-center">
		                        		<td rowspan="<?=$cnt;?>"><?=$area_1['area_1'];?></td>
		                                <td><?=$area_2['area_2'];?></td>
		                                <td><?=$a['cnt'];?></td>
		                                <td><?=$b['cnt'];?></td>
		                                <td><?=$c['cnt'];?></td>
		                                <td><?=$a['cnt']+$b['cnt']+$c['cnt'];?></td>
		                            </tr>
									<?php
										}else{
									?>
								 	<tr class="text-center">
		                                <td><?=$area_2['area_2'];?></td>
		                                <td><?=$a['cnt'];?></td>
		                                <td><?=$b['cnt'];?></td>
		                                <td><?=$c['cnt'];?></td>
		                                <td><?=$a['cnt']+$b['cnt']+$c['cnt'];?></td>
		                            </tr>
									<?php
										}
										$i++;	
									?>

								<?php
										}
									}
	                        	?>
	                        	<tr class="text-center">
	                                <td colspan="2">합계</td>
	                                <td><?=$a_sum;?></td>
	                                <td><?=$b_sum;?></td>
	                                <td><?=$c_sum;?></td>
	                                <td><?=$a_sum+$b_sum+$c_sum;?></td>
	                            </tr>
                            </tbody>
	                    </table>
	                </div>
	            </div>
	            <!-- end 지역별 접수 현황 Table -->
	        </div>
	    </div>
	</div>
<!-- end Contents Area -->


<?php require 'inc/views/base_footer.php'; ?>
<?php require 'inc/views/template_footer_start.php'; ?>


<!-- Page JS Plugins -->

<script src="<?php echo $one->assets_folder; ?>/js/plugins/datatables/jquery.dataTables.min.js"></script>
<script src="<?php echo $one->assets_folder; ?>/js/plugins/select2/select2.full.min.js"></script>

<!-- Page JS Code -->
<script src="<?php echo $one->assets_folder; ?>/js/pages/base_tables_datatables.js"></script>

<script>
    jQuery(function(){
        // Init page helpers (BS Datepicker + BS Datetimepicker + BS Colorpicker + BS Maxlength + Select2 + Masked Input + Range Sliders + Tags Inputs plugins)
        App.initHelpers(['datepicker', 'datetimepicker', 'colorpicker', 'maxlength', 'select2', 'masked-inputs', 'rangeslider', 'tags-inputs']);
    });
</script>
<?php require 'inc/views/template_footer_end.php'; ?>

