<?php
include_once('../common.php');
if (!defined('_GNUBOARD_')) exit; // 개별 페이지 접근 불가
include_once(G5_AOS_PATH.'/head.php');
$menu_cate2 ='mypage';
$menu_cate3 ='1';

$mb_id = get_session('ss_mb_id');
$mb = get_member($mb_id);
?>

<!-- sub nav -->
<?php include_once('./app_sub_nav.php'); ?>
<!-- end sub nav -->


<!-- Contents Area -->
<div class="pop_container">
	<!-- 선수정보 -->
	<!-- <section>
		<div class="pop_hd">
			<div class="tit">선수정보</div>
		</div>
<!-- <a href="clearSession.php"> 로그아웃</a> -->
		<!-- <div class="content">
			<ul class="info-list">

				<li><span>이름</span><?=$mb['mb_name']?></li>
				<li><span>소속</span>강원도 춘천 레이즈업</li>
				<li><span>등급</span><?=$mb['mb_5']?></li>
				<li><span>최고성적</span>
					<ul>
						<li>2017. 본선8강 <a class="color4">(소양강배 춘천 테니스 대회)</a></li>
					</ul>
				</li>
			</ul>
		</div>
	</section>  -->
	<!-- //선수정보 -->

	<!-- 대회별전적 -->
	<!-- <section>
		<div class="pop_hd">
			<div class="tit">대회별 전적</div>
		</div>

		<div class="content">
			<ul class="info-list01">
				<li><span>2017.01</span>
					<a href="">
						<div class="tit">제10회 춘천 오픈 전국동호인 복식 테니스 대회</div>
						<div>레이즈업 이우선, 레이즈업 어경민</div>
						<div>최고성적 : 본선 16강 (예선전적 : 3전 2승 1패)</div>
					</a>
				</li>
				<li><span>2017.01</span>
					<a href="">
						<div class="tit">제10회 춘천 오픈 전국동호인 복식 테니스 대회</div>
						<div>레이즈업 이우선, 레이즈업 어경민</div>
						<div>최고성적 : 본선 16강 (예선전적 : 3전 2승 1패)</div>
					</a>
				</li>
				<li><span>2017.01</span>
					<a href="">
						<div class="tit">제10회 춘천 오픈 전국동호인 복식 테니스 대회</div>
						<div>레이즈업 이우선, 레이즈업 어경민</div>
						<div>최고성적 : 본선 16강 (예선전적 : 3전 2승 1패)</div>
					</a>
				</li>
			</ul>
		</div>
		<div class="quick_view">
			<a href="">
				더 보 기
			</a>
		</div>
	</section> -->
	<!-- //대회별전적 -->

	<!-- <section>
		<div class="pop_hd">
			<div class="tit">페어별 전적</div>
		</div>

		<div class="content">
			<div class="tbl_style02 tbl_striped">
				<table>
					<thead>
						<tr>
							<th>팀원</th>
							<th>참가횟수</th>
							<th>최고성적</th>
							<th>예선통합전적</th>
							<th>예선승률</th>
						</tr>
					</thead>
					<tbody class="text-center">
						<tr>
							<td>레이즈업<br/>이우선</td>
							<td>
								5
							</td>
							<td>
								8강<br>
								(제22회 춘천 소양강배)
							</td>
							<td>
								10전8승2패
							</td>
							<td>
								80%
							</td>
						</tr>


					</tbody>
				</table>
			</div>

		</div>

	</section> -->
</div>
<!-- end Contents Area -->


<?php
include_once(G5_AOS_PATH.'/tail.php');
?>
