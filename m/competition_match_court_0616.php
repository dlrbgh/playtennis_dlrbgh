<?php
include_once('../common.php');
if (!defined('_GNUBOARD_')) exit; // 개별 페이지 접근 불가
include_once(G5_AOS_PATH.'/head.php');
$menu_cate2 ='competition_view';
$menu_cate3 ='4';
?>

<!-- sub nav -->
<?php include_once('./app_sub_nav.php'); ?>
<!-- end sub nav -->
<div class="pop_container">
	<?php if(false && ! $competition['opening_date']){?>
		<div class="empty_waiting">
			<div class="img_area">
				<img src="<?php echo G5_IMG_URL.'/common/intro_logo1.png';?>">
			</div>
			<div class="cmt ani02">
				경기가 준비중입니다.
			</div>
		</div>
	<?php }else {?>

		<?php

			$getGymdateSql = "select date_format(match_gym_data.application_period, get_format(date, 'iso'))
									as format_wr_datetime from match_data join match_gym_data
									on match_data.code = match_gym_data.match_id
									where code = '{$c}' group by application_period";
			$date = sql_query($getGymdateSql);?>
			<section>
				<div class="pop_hd sound_only">
					<div class="tit">
						선택
					</div>
				</div>
				<div class="content">
					<div id="court_list" class="btn-group" role="group">
					<ul class="btn-list">
						<?php while($row = sql_fetch_array($date)){
							$d == '' ? $d = $row['format_wr_datetime'] : $d;
							?>
							<li><a href="?c=<?=$c?>&d=<?=urlencode($row['format_wr_datetime'])?>"
								<?=$d == $row['format_wr_datetime'] ? 'class="active"':""?>
								>
								<?=$row['format_wr_datetime']?></a></li>
						<?php }?>
	 				</ul>
				</div>

<?php
	$gym_sql = "select * from match_data join match_gym_data join gym_data
					on match_data.code = match_gym_data.match_id
					and match_gym_data.gym_id = gym_data.wr_id
					where code = '{$c}' and date_format(match_gym_data.application_period, get_format(date, 'iso')) = '{$d}'
					";

	// print $gym_sql;
	$gymdata = sql_query($gym_sql);



	$use_court = 1;

 ?>

	<div class="pop_hd sound_only">
		<div class="tit">경기장 선택</div>
	</div>

		<!-- <?php print_r($gym_data)?> -->

		<select class="full-width form-control" id="gym">
			<option value="">경기장 선택</option>
			<?php
				while($row = sql_fetch_array($gymdata)){
					$g == '' ? $g = $row['wr_id'] : $g;
					?>
					<option value="<?=$row['wr_id']?>"
						<?php if($g == $row['wr_id']){
							print 'selected="selected"';
							$use_court = $row['use_court'];
						}?>
						><?=$row['gym_name']?></option>
				<?php }
			?>
		</select>
		<script>
		$('#gym').change(function(event){
			window.location.href='?c=<?=$c?>&d='+encodeURIComponent('<?=$d?>')+'&g='+$(this).val();});
		</script>

		<div id="court_list" class="btn-group" role="group">
			<ul class="btn-list">
				<?php
					$ct = $ct == '' ? 1 : $ct;
					for($i = 0; $i < $use_court ; $i++){?>
						<li><a href="?c=<?=$c?>&d=<?=$d?>&g=<?=$g?>&ct=<?=$i+1?>"
							<?=$ct == $i+1 ? 'class="active"' : ''?>
							><?=$i+1?>코트</a></li>
					<?php }?>
			</ul>
		</div>

	</div>
</section>

<section>
  <div style="width:100%; height:22px; background-color:#929292;position:relative;">
  <?php

    // c=719666017N3HCA3EM4&d=2017-06-03&g=84&ct=1
    $game_progress_sql = "
    select end_game, count(*) as cnt from game_score_data
    where
    match_code = '{$c}'
    and game_date = '{$d}'
    and gym_code = {$g}
    and game_court = {$ct}
		and tournament = 'L'
    group by end_game";

    // print $game_progress_sql;
    $game_progress = sql_query($game_progress_sql);
    $game;
    $total_game = 0;
    $game = array('N'=>0, 'Y'=>0);
    while($row = sql_fetch_array($game_progress)){
      $game[$row['end_game']] = $row['cnt'];
      $total_game = $total_game+ ($row['cnt']-0);
    }
    // print_r($game);
    $end_game_percentage = $game['Y']/$total_game * 100;
    // $end_game_percentage = 40;
    // print $end_game_percentage;
    print $total_game." ".$game['Y'];
   ?>

   <div id="progress" style="width:<?php print $end_game_percentage?>%; background-color:rgb(20, 38, 74);height:22px;position:absolute;top:0;left:0;">
   </div>
   <div style="width:100%;min-width:90px;height:22px;position:absolute;top:0;left:0;text-align:center;color:white;"><?php print $game['Y'];?>/<?php print $total_game;?>진행중</div>

 </div>

</section>

<!-- 토너먼트 -->
<?php
$is_league_finish = false;
// 이번 경기(6월초)는 코트페이지에서 본선경기를 보여주지않도록
// 코트배정이 동적이고 사람들마다 보여줘야하니까 일단 뺍니다
// 5.29 이규호

if($is_league_finish ){?>
	<section>
		<div class="pop_hd">
			<div class="tit">본선</div>
		</div>
		<div class="content ">
			<div class="tbl_style02 tbl_striped">

			<table>
					<thead>
						<tr>
							<th>경기</th>
							<th>클럽</th>
							<th>이름</th>
							<th>점수</th>
							<th>이름</th>
							<th>클럽</th>
						</tr>
					</thead>
					<tbody>
						<?php

							$tournament_sql  = "select *
													,division, series, series_sub
													,case when division = '단체전' then team_1_event_code
													else team_1_code end as team_1_code
													,case when division = '단체전' then team_2_event_code
													else team_2_code  end as team_2_code
													,case when division = '단체전'
													then (select concat(club,'-',team_1_name,'-', team_2_name) from team_data where team_code	= team_1_event_code)
													else (select concat(club,'-',team_1_name,'-', team_2_name) from team_data where team_code	= team_1_code) end as team_1
													,case when division = '단체전'
													then (select concat(club,'-',team_1_name,'-', team_2_name) from team_data where team_code	= team_2_event_code)
													else (select concat(club,'-',team_1_name,'-', team_2_name) from team_data where team_code	= team_2_code) end as team_2

													from game_score_data
													where match_code = '{$c}'
													and gym_code = {$g}
													and tournament <> 'L'
													and game_date = '{$d}'
													and game_court = {$ct}
													order by court_array_num";

							// and gym_code = {$g}
							// and date_format(game_date, get_format(date, 'iso')) = '{$d}'
							// and game_court = {$ct}
							$tournament_court_list = sql_query($tournament_sql, true);

							if($tournament_court_list->num_rows < 1){?>
								<tr>
									<td colspan="6" class="text-center">경기가 없습니다</td>
								</tr>
							<?php }else
							while($row = sql_fetch_array( $tournament_court_list)){
								// print_r($row);
								$team_1_info = explode('-',$row['team_1']);
								$team_2_info = explode('-',$row['team_2']);
								// print_r($)
								if($row['team_1_code'] == ''){?>
									<tr>
										<td class="text-center"><?=$row['court_array_num']?>경기</td>
										<td colspan="5" class="text-center">준비중입니다</td>
									</tr>
								<?php }else{?>
									<tr>
										<td><?=$row['court_array_num']?>경기</td>

										<td><?=$team_1_info[0]?><br /><?=$team_1_info[0]?></td>
										<td><?=$team_1_info[1]?><br /><?=$team_1_info[2]?></td>
										<td class="match_point text-center">
											<?php if($row['team_1_dis'] > 0){
												print '<div class="skip"><span>ㅡ</span> : <span>승</span></div>';
											}else if($row['team_2_dis'] > 0){
												print '<div class="skip"><span>승</span> : <span>ㅡ</span></div>';
											}else{
												 if($row['is_on'] == 'N' && $row['end_game'] == 'N'){?>
												<!-- <div><span></span>&nbsp;준비중&nbsp;<span></span></div> -->
												<div class="">
													<span style="font-size:16px">준비중</span>
												</div>
											<?php }else if($row['end_game'] == 'Y'){?>
												<div><span class="win"><?=$row['team_1_score']?></span>&nbsp;:&nbsp;<span><?=$row['team_2_score']?></span></div>
											<?php }else if($row['is_on'] == 'Y' && $row['game_start_time'] != ''){?>
												<div class="playing ani01"><?=$row['game_court']?>진행중</div>
											<?php }

											}?>
											<!-- <?php if($row['is_on'] == 'N' && $row['end_game'] == 'N'){?>
												<span></span>
												준비중
												<span></span>
											<?php }else if($row['end_game'] == 'Y'){?>
												<?=$row['team_1_score']?> : <?=$row['team_2_score']?>
											<?php }else if($row['is_on'] == 'Y' && $row['game_start_time'] != ''){?>
												<span></span>
												진행중
												<span></span>
											<?php }?> -->
										</td>
										<td><?=$team_2_info[1]?><br /><?=$team_2_info[2]?></td>
										<td><?=$team_2_info[0]?><br /><?=$team_2_info[0]?></td>
									</tr>
							<?php }
						}?>
					</tbody>
				</table>
				1111
			</div>
		</div>
	</section>
<?php }?>


<?php

	$game_score_data_sql = "select *, game_score_data.game_court as use_game_court from game_score_data join group_data
	on game_score_data.group_code = group_data.code
	where game_score_data.match_code = '{$c}'
	and game_score_data.gym_code = $g
	and game_score_data.game_court = {$ct}
	and game_score_data.tournament = 'L'
	order by game_score_data.court_array_num";
	?>

	<?php

	$game_score_list = sql_query($game_score_data_sql);

	$match_index = 1;
	$group_d;
	$group_s;
	$group_ss;
	while($game_score = sql_fetch_array($game_score_list)){
		$d = $game_score['division'];
		$s = $game_score['series'];
		$ss = $game_score['series_sub'];
		$team_1_code = $game_score['team_1_code'];
		$team_2_code = $game_score['team_2_code'];
		if($group_d == ''){
			$group_d = $game_score['division'];
			$group_s = $game_score['series'];
			$group_ss = $game_score['series_sub'];
			?>
			<section>
				<div class="content">
					<div class="con_tit_area clear">
						<div class="tit"><?=$game_score['division']?> <?=$game_score['series']?> <?=$game_score['series_sub']?>-<?php echo $game_score['num']+1?>조</div>
							<div class="r-area">
								<ul>
									<li class="color5 fw-700"><?=$game_status['status_display']?></li>
								</ul>
							</div>
					</div>
					<div class="tbl_style02 tbl_striped">
					<table>
						<thead>
							<tr>
								<th>경기</th>
								<th>참가팀</th>
								<th>점수</th>
								<th>참가팀</th>
							</tr>
						</thead>
		<?php }
		if($group_d.$group_s.$group_ss != $game_score['division'].$game_score['series'].$game_score['series_sub']){
			$group_d = $game_score['division'];
			$group_s = $game_score['series'];
			$group_ss = $game_score['series_sub'];
			?>

					</tbody>
				</table>
			</div>
			</section>
			<section>
				<div class="content">
					<div class="con_tit_area clear">
						<div class="tit"><?=$game_score['division']?> <?=$game_score['series']?> <?=$game_score['series_sub']?>-<?php echo $game_score['num']+1?>조</div>
							<div class="r-area">
								<ul>
									<li class="color5 fw-700"><?=$game_status['status_display']?></li>
								</ul>
							</div>
					</div>
					<div class="tbl_style02 tbl_striped">
					<table>
						<thead>
							<tr>
								<th>경기</th>
								<th>참가팀</th>
								<th>점수</th>
								<th>참가팀</th>
							</tr>
						</thead>

		<?php } ?>

		<?php
			$team_1_info = sql_fetch("select * from team_data where team_code = '{$team_1_code}'");
			$team_2_info = sql_fetch("select * from team_data where team_code = '{$team_2_code}'");
		 ?>
		 <tr>
			<td class="text-center"><?=($match_index++)?><br>경기</td>
			<td class="text-center">
				<div class="match_member_area">
					<div class="team_area">
						<div class="club"><?php print "{$team_1_info['area_2']}-{$team_1_info['club']}"?></div>
						<div class="name"><?php print "{$team_1_info['team_1_name']}({$team_1_info['team_1_grade']})"?></div>
					</div>
					<div class="team_area">
						<div class="club"><?php print "{$team_1_info['team_2_area']}-{$team_1_info['team_2_club']}"?></div>
						<div class="name"><?php print "{$team_1_info['team_2_name']}({$team_1_info['team_2_grade']})"?></div>
					</div>
				</div>
			</td>
			<td class="match_point text-center">
						<?php if($game_score['team_1_dis'] == "1"){
							print '<div class="skip point_box"><span>ㅡ</span>&nbsp;&nbsp;&nbsp;&nbsp;<span>승</span></div>';
						}else if($game_score['team_2_dis'] == "1"){
							print '<div class="skip point_box"><span>승</span>&nbsp;&nbsp;&nbsp;&nbsp;<span>ㅡ</span></div>';
						}else if($game_score['team_1_dis'] == "2"){
							print '<div class="skip point_box"><span>실격</span><span>ㅡ</span></div>';
						}else if($game_score['team_2_dis'] == "2"){
							print '<div class="skip point_box"><span>&nbsp;&nbsp;&nbsp;&nbsp;-&nbsp;&nbsp;&nbsp;&nbsp;</span><span>실격</span></div>';
						}else if($game_score['team_1_dis'] == "3"){
							print '<div class="skip"><span>경기안함</span></div>';
						}else{
						 if($game_score['is_on'] == 'N' && $game_score['end_game'] == 'N'){?>
						<div class="">
							<span style="font-size:16px">준비중</span>
						</div>
					<?php }else if($game_score['end_game'] == 'Y'){?>
						<div class="point_box"><span class="win"><?=$game_score['team_1_score']?></span>&nbsp;&nbsp;<span><?=$game_score['team_2_score']?></span></div>
					<?php }else if($game_score['is_on'] == 'Y' && $game_score['game_start_time'] != ''){?>
						<div class="playing ani01 color6"><?=$game_score['use_game_court']?>코트 진행중</div>
					<?php }
					}?>
				</td>
			<td class="text-center">
				<div class="match_member_area">
					<div class="team_area">
						<div class="club"><?php print "{$team_2_info['area_2']}-{$team_2_info['club']}"?></div>
						<div class="name"><?php print "{$team_2_info['team_1_name']}({$team_2_info['team_1_grade']})"?></div>
					</div>
					<div class="team_area">
						<div class="club"><?php print "{$team_2_info['team_2_area']}-{$team_2_info['team_2_club']}"?></div>
						<div class="name"><?php print "{$team_2_info['team_2_name']}({$team_2_info['team_2_grade']})"?></div>
					</div>
				</div>
			</td>
		</tr>


	<?php }?>
<?php } ?>

					</tbody>
				</table>
			</div>
			</section>
</div>
<?php
function cmp($a, $b){
	return $b['team_league_point'] - $a['team_league_point'];
}
function is_on($group_code, $tournament_sql = " and tournament = 'L'"){

	$game_state_array = array(1=>'예정', 2=>'진행중', 3=>'종료');
	$sql = "select end_game, is_on, ifnull(game_start_time,'') as game_start_time
	,  ifnull(game_end_time,'') as game_end_time from game_score_data where group_code = '{$group_code}' $tournament_sql";
	// print $sql;
	$result = sql_query($sql,true);
	$game = $result->num_rows;
	$end_game = array('N'=>0, 'Y'=>0);
	$is_on  = array('N'=>0, 'Y'=>0);
	$game_start_time;
	$game_end_time;
	while($row = sql_fetch_array($result)){
		$end_game[$row['end_game']] =$end_game[$row['end_game']] +1;
		$is_on[$row['is_on']] = $is_on[$row['is_on']] +1;
		$game_start_time[$row['game_start_time']]
		= $game_start_time[$row['game_start_time']] == '' ? 1 :  ($game_start_time[$row['game_start_time']]-0 )+1;

	}
	if($end_game['N'] == $game){
		if($game_start_time[''] == $game){
			return array('status'=>1, 'status_display'=>$game_state_array[1]);
		}else if($game_start_time[''] < $game){
			return array('status'=>2, 'status_display'=>$game_state_array[2]);
		}
	}
	if($end_game['Y'] == $game) {
		return array('status'=>3, 'status_display'=>$game_state_array[3]);
	}
}
function is_league_finish($c, $d, $g, $ct){
	$sql = "select end_game, count(*) as count
	from game_score_data
	where match_code = '{$c}' and tournament = 'L' group by end_game";
	$result = sql_query($sql);
	$r;
	while($row = sql_fetch_array($result)){
		$r[$row['end_game']] = $row['count'];
	}
	if(isset($r['N']) && $r['N'] == 0){
		return true;
	}else{
		return false;
	}
	// if($r
}
?>

<?php
include_once(G5_AOS_PATH.'/tail.php');
?>
