<?php
if (!defined('_GNUBOARD_')) exit; // 개별 페이지 접근 불가

if(defined('G5_THEME_PATH')) {
    require_once(G5_THEME_PATH.'/index.php');
    return;
}

include_once(G5_MOBILE_PATH.'/head.php');
?>


 <!-- 공지사항 -->
		  <?php  echo latest("basic","notice", 4, 28);?>

<?php
include_once(G5_MOBILE_PATH.'/tail.php');
?>