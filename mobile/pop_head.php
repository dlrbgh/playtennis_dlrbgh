<style>
	/*
@import url(//cdn.jsdelivr.net/nanumsquare/1.0/nanumsquare.css);
*/
/*default mobile settiong*/
*, *::before, *::after {
    box-sizing: inherit;}
html {
    /* 화면을 길게 누르고 있을때 뜨는 팝업이나 액션시트를 제어 */
    -webkit-touch-callout:none;
    /* 텍스트나 이미지를 선택할 수 있게 하는 여부를 제어 */
    -webkit-user-select:none;
    /* 링크를 터치했을때 나오는 기본 영역의 색상을 제어 */
    -webkit-tap-highlight-color:rgba(0, 0, 0, 0);
}
body{padding:0;margin:0;font-family:'nanumsquare','Malgun Gothic','맑은 고딕',sans-serif;background-color:#efefef}
ul,li{list-style-type:none;padding:0;margin:0}
ul::after{content:"";display:block;clear:both}
a{text-decoration:none;color:#14264a}

table {
    border-collapse: collapse;
    border-spacing: 0;
}
select,input{box-sizing: border-box;}
.form-control {display: block;width: 100%;height: 40px;padding: 6px 12px;font-size: 14px;line-height: 1.32857143;
    color: #555;background-color: #fff;background-image: none;border: 1px solid #ccc;
    -webkit-box-shadow: inset 0 1px 1px rgba(0,0,0,.075);
    box-shadow: inset 0 1px 1px rgba(0,0,0,.075);
    -webkit-transition: border-color ease-in-out .15s , -webkit-box-shadow ease-in-out .15s;
    -o-transition: border-color ease-in-out .15s , box-shadow ease-in-out .15s;
    transition: border-color ease-in-out .15s , box-shadow ease-in-out .15s;}

.sound_only{visibility:hidden;font-size:0px}
.img-responsive{width:100%}
/*margin-padding*/
.ml-30{margin-left:30px;}
.mb-50{margin-bottom:50px!important}
.mb-45{margin-bottom:45px!important}
.mb-40{margin-bottom:40px!important}
.mb-35{margin-bottom:35px!important}
.mb-30{margin-bottom:30px!important}
.mb-25{margin-bottom:25px!important}
.mb-20{margin-bottom:20px!important}
.mb-15{margin-bottom:15px!important}
.mb-10{margin-bottom:10px!important}
.mb-5{margin-bottom:5px!important}
.mb-0{margin-bottom:0px!important}
/*padding-padding*/
.pt-50{padding-top:50px!important}
.pb-0{padding-bottom:0px!important}
.pr-5{padding-right:5px!important}

/*fontweight*/
.fw-700{font-weight:700!important}
.fw-600{font-weight:600!important}
.fw-500{font-weight:500!important}
.fw-400{font-weight:400!important}
.fw-300{font-weight:300!important}
.fw-200{font-weight:200!important}
.fw-100{font-weight:100!important}

.text-left{text-align:left}
.text-right{text-align:right}
.text-center{text-align:center}

/*background-image*/
.bk-img1{background-image:url('../img/common/bkg01.jpg');background-position-x:50%;background-repeat:no-repeat;}

/*icon_모음*/
.quick_view a:after,.competion_tab a i{background:url('../img/common/icons.png') no-repeat;background-size:130px 43px;}
.ico-star{background-image:url("../img/common/icon_favorite.png")}
.ico-home{background-image:url("../img/common/icon_home.png")}

/* color-setting */
.bkg, .bkg1,.bkg2,.bkg3,.bkg4,.bkg5,.bkg6,.bkg7{border:none}
.bkg{background-color:#efefef}
.bkg1, a.bkg1{background-color:#efefef!important;color:#fff}
.bkg1, a.bkg1:hover{}

.bkg2, a.bkg2{background-color:#777777!important;color:#fff}
.bkg3, a.bkg3{background-color:#999999!important;color:#fff}
.bkg3, a.bkg3:hover{background-color:#292929!important;color:#fff}

.bkg4, a.bkg4{background-color:#bbbbbb!important;color:#fff}
.bkg4, a.bkg4:hover{background-color:#292929!important;color:#fff}
.bkg5, a.bkg5{background-color:#ff0000!important;color:#fff}
.bkg5, a.bkg5:hover{background-color:#e8180c!important;border:1px solid #e8180c;color:#fff}
.bkg6, a.bkg6{background-color:#f29803!important;color:#fff}
.bkg6, a.bkg6:hover{background-color:#292929!important;color:#fff}
.bkg7, a.bkg7{background-color:#fff!important;color:#292929}

.bkg10, a.bkg10{background-color:#14264a!important;color:#fff}
.bkg10, a.bkg10:hover{background-color:#292929!important;color:#fff}


.color1, a.color1{color:#292929!important}
.color2, a.color2{color:#777777!important}
.color3, a.color3{color:#999999!important}
.color4, a.color4{color:#bbbbbb!important}
.color5, a.color5{color:#ff0000!important}
.color6, a.color6{color:#f29803!important}
.color7, a.color7{color:#fff!important}
/* dlrbgh 0516*/
.border_color5{border:1px #ff0000 solid!important}

/*--------------------------table setting-----------------------*/
.tbl_style01 table {margin:0 0 10px;width:100%;font-size:1em}
.tbl_style01 table caption {padding:0;font-size:0;line-height:0;overflow:hidden}
.tbl_style01 table thead th {padding:7px 0;border-top:2px solid #000000;border-bottom:1px solid #000000;background:#fff;color:#777;font-size:0.8em;text-align:center;letter-spacing:-0.1em}
.tbl_style01 table thead a {color:#383838}
.tbl_style01 table thead th input {vertical-align:top} /* middle 로 하면 게시판 읽기에서 목록 사용시 체크박스 라인 깨짐 */
.tbl_style01 table tfoot th, .tbl_head01 tfoot td {padding:10px 0;border-top:1px solid #c1d1d5;border-bottom:1px solid #c1d1d5;background:#d7e0e2;text-align:center}
.tbl_style01 table tbody th {padding:13px 0;border-top:1px solid #e9e9e9;border-bottom:1px solid #e9e9e9;font-size:0.8em}
.tbl_style01 table td {padding:5px 3px;border-top:1px solid #e9e9e9;border-bottom:1px solid #e9e9e9;line-height:1.5em;word-break:break-all;font-size:0.8em;}
.tbl_style01 table tfoot.page{text-align:center}
.tbl_style01 table tfoot.page tr td{padding:0}
.tbl_style01 table tfoot.page tr td span{font-size:14px;font-weight:600;padding:15px 0; display:inline-block; width:100%;cursor:pointer}
.tbl_style01 table tfoot.page span.active{background-color:#14264a;color:#fff;}
.tbl_style01 table tfoot.page span:active{background-color:#14264a;color:#fff}


.tbl_style02 table {margin:0 0 10px;width:100%;font-size:1em}
.tbl_style02 table caption {padding:0;font-size:0;line-height:0;overflow:hidden}
.tbl_style02 table thead th {padding:7px 0;border-top:2px solid #000000;border-bottom:1px solid #000000;background:#fff;color:#777;font-size:0.8em;text-align:center;letter-spacing:-0.1em}
.tbl_style02 table thead a {color:#383838}
.tbl_style02 table thead th input {vertical-align:top} /* middle 로 하면 게시판 읽기에서 목록 사용시 체크박스 라인 깨짐 */
.tbl_style02 table tfoot th, .tbl_head01 tfoot td {padding:10px 0;border-top:1px solid #c1d1d5;border-bottom:1px solid #c1d1d5;background:#d7e0e2;text-align:center}
.tbl_style02 table tbody th {padding:13px 0;border-top:1px solid #e9e9e9;border-bottom:1px solid #e9e9e9;font-size:0.8em}
.tbl_style02 table td {padding:5px 3px;border-top:1px solid #e9e9e9;border-bottom:1px solid #e9e9e9;line-height:1.5em;word-break:break-all;font-size:0.8em;}
.tbl_style02 table td.match_point {color:#aaa;font-size:2.5em}
.tbl_style02 table td.match_point .playing{font-size:16px;}
/*ect setting*/
.tbl_striped>table > tbody > tr:nth-of-type(odd) {background-color: #f9f9f9;}
.tbl_borderd>table>thead>tr>th{border-right: 1px solid #cccccc;}
.tbl_borderd>table{border-collapse: collapse;}
.tbl_borderd>table>tbody>tr>td{border-right: 1px solid #cccccc;border-bottom: 1px solid #cccccc;border-top:none}
.tbl_borderd>table>thead>tr>th:first-child, .tbl_borderd>table>tbody>tr>td:first-child{border-left:none}
.tbl_borderd>table>thead>tr>th:last-child, .tbl_borderd>table>tbody>tr>td:last-child{border-right:none}

.tbl_small tr th,.tbl_small tr td{padding:5px}

/*------------------------------login_area--------------------------------*/
/*login css*/
.logo_area{position:relative;margin:50px;height:200px}
.logo_area img{width:173px;position:absolute;left:50%;margin-left:-100px}

.login_area{margin: 0 auto;text-align:center;position:relative;padding:30px}
.login_hd{background-color: #d5d7dc;height: 1px;
    position: relative;height: 1px;width: 100%;margin-bottom:30px}
.login_hd span{background-color:#fff;position:absolute;left:50%;margin-left:-50px;top:-6px;color:#d5d7dc;font-size:13px;width:100px;text-align:center}
.login_list {
    background-color: #fff;
    color: #fff;
    bottom: 60px;
    z-index: 1030;
    -webkit-backface-visibility: hidden;
backface-visibility: hidden;
display: box;
display: -webkit-box;
display: -moz-box;
box-orient: horizontal;
-webkit-box-orient: horizontal;
-moz-box-orient: horizontal;
/*0516 임시추가 - 단일 로그인*/
width:100px;
margin:0 auto;
}
.login_list a {
display: block;
box-flex: 1;
-webkit-box-flex: 1;
-moz-box-flex: 1;
color: #bbbbbb;
font-size: 14px;
letter-spacing: -1px;
text-align: center;
line-height: 2em;
white-space: nowrap;
padding:10px;
}
.login_list a img{width:100%;}
.login_area .login_ft{text-align:center;margin-top:20px}
.login_area .login_ft img{width:50%;}
.ft_logo{position:fixed;bottom:0;width:64px;left:50%;bottom:20px;margin-left:-32px;}

.register_area{margin-top:30px}
.register_area .btn_regi
{background-color: #bdbdbd;
color: #fff;
padding: 20px 0;
display: block;
text-align: center;
width: 100%;
border: 1px solid #bdbdbd;
font-size:0.8em}



.login_hd_area{background-color:#fff;color:#fff;padding:12px 0;text-align:center;box-shadow:1px 1px 3px grey;position:fixed;display:inline-block;width:100%;z-index:9999;top:0}
.login_hd_area a{color:#292929;padding:12px}
.login_hd_area a.title{font-size:16px;font-weight:600;}

.login_container{padding-top:90px;margin:0 20px }

/* 회원가입 폼 테이블 */
.tbl_frm02 {margin:0 0 20px;color:#9d9d9d}
.tbl_frm02 table {width:100%;border-collapse:collapse;border-spacing:0}
.tbl_frm02 thead th {box-sizing: content-box;width:80px;padding:7px 13px;background:#f7f7f7;text-align:left}
.tbl_frm02 tbody th {padding:15px 0 5px 0;background:transparent;text-align:left;font-size:13px;font-weight:300}
.tbl_frm02 tbody td {padding:15px 0 5px 0;background:transparent;border-bottom:1px solid #ccc}
.tbl_frm02 tbody td input{border:0;width:100%;padding:5px;font-weight:700;color:#aaa}


.ft_area{position:absolute;bottom:0;width: 100%;}
.ft_area .ft_hd{position:relative;padding:15px 0;}
.ft_area .ft_hd a{font-size:12px;text-decoration:underline;color:#aaa;font-weight:700}
.ft_area .btn_area{position:relative;width:100%;display:block}
.ft_area .btn_area a{background-color:#bdbdbd;color:#fff;padding:20px 0;display:block;text-align:center;width:100%}
.ft_area .btn_area a:active{background-color:#14264a}

.login_hd_area{background-color:#fff;color:#fff;padding:12px 0;text-align:center;box-shadow:1px 1px 3px grey;position:fixed;display:inline-block;width:100%;z-index:9999;top:0}
.login_hd_area a{color:#292929;padding:12px}
.login_hd_area a.title{font-size:16px;font-weight:600;}

.login_container{padding-top:90px;margin:0 20px }

/* 회원가입 폼 테이블 */
.tbl_frm02 {margin:0 0 20px;color:#9d9d9d}
.tbl_frm02 table {width:100%;border-collapse:collapse;border-spacing:0}
.tbl_frm02 thead th {box-sizing: content-box;width:80px;padding:7px 13px;background:#f7f7f7;text-align:left}
.tbl_frm02 tbody th {padding:15px 0 5px 0;background:transparent;text-align:left;font-size:13px;font-weight:300}
.tbl_frm02 tbody td {padding:15px 0 5px 0;background:transparent;border-bottom:1px solid #ccc}
.tbl_frm02 tbody td input{border:0;width:100%;padding:5px;font-weight:700;color:#aaa}


.ft_area{position:absolute;bottom:0;width: 100%;}
.ft_area .ft_hd{position:relative;padding:15px 0;}
.ft_area .ft_hd a{font-size:12px;text-decoration:underline;color:#aaa;font-weight:700}
.ft_area .btn_area{position:relative;width:100%;display:block}
.ft_area .btn_area a{background-color:#bdbdbd;color:#fff;padding:20px 0;display:block;text-align:center;width:100%}
.ft_area .btn_area a:active{background-color:#14264a}

/*경고안내*/
.login_container .warning{padding:8px 8px;background-color:#ffcccc;border:1px #ff1212 solid; color:#ff1212}

/*--------------------------main-------------------------------*/
.m_container{padding-top:51px}

/*main_hd*/
.m_hd_area{padding:5px 0;text-align:center;border-bottom:1px solid #efefef;position:fixed;display:inline-block;width:100%;z-index:9999;background-color:#fff}
.m_hd_area a.logo{color:#fff;width:145px;display:inline-block;padding-top:8px}
.m_hd_area a.title{font-size:16px;font-weight:600}
.m_hd_area a.menu{padding:10px 12px;width:28px;height:21px;background-color:red;display:inline-block;background:url('../img/common/menu.png') no-repeat center center;background-size:47px}
.m_hd_area a.calander{padding:10px 12px;width:28px;height:21px;background-color:red;display:inline-block;background:url('../img/common/edit_42.png') no-repeat center center;background-size:28px}
.m_hd_area a.menu i,.m_hd_area a.calander i{font-size:0;line-height:0;}

.hd_lnb{float:left;}
.hd_right{float:right;}
/*popup_hd*/
.pop_hd_area{background-color:#14264a;color:#fff;padding:12px 0;text-align:center;box-shadow:1px 1px 3px grey;position:fixed;display:inline-block;width:100%;z-index:9999;top:0;height:26px;line-height:26px}
.pop_hd_area a{color:#fff;padding:10px;}
.pop_hd_area a.title{font-size:16px;font-weight:600;}
/*
#home_tabs{
    margin-top:49px;
    padding:0px;
    background-color:white;

}
#home_tabs a{
    display: inline-block;
    padding: 10px 6px 11px 6px;
    color:grey;
}
#home_tabs a.active{
  border-bottom:2px #14264a solid;
  color:#14264a;
  font-weight:bolder;
}*/

.menu_snav {
    display: box;
    display: -webkit-box;
    display: -moz-box;
    box-orient: horizontal;
    -webkit-box-orient: horizontal;
    -moz-box-orient: horizontal;
    background: #2c343f;
}
.top_nav {
    background-color: #fff;
    color: #fff;
    position: fixed;
    top: 40px;
    right: 0;
    left: 0;
    z-index: 1030;
    min-width: 320px;
    -webkit-backface-visibility: hidden;
    backface-visibility: hidden;
    box-shadow:1px 1px 3px grey;
}
	.menu_snav a.active {
    position: relative;
    color: #14264a;
    font-weight: 700;
}
.menu_snav a {
    display: block;
    box-flex: 1;
    -webkit-box-flex: 1;
    -moz-box-flex: 1;
    height: 43px;
    color: #bbbbbb;
    font-size: 15px;
    letter-spacing: -1px;
    text-align: center;
    line-height: 2.8em;
    white-space: nowrap;
    font-weight:600;
}
.menu_snav a.active:after {
    content: '';
    display: block;
    position: absolute;
    bottom: 0;
    left: 0;
    right: 0;
    height: 3px;
    background: #14264a;
    z-index: 10;
}



/*section setting*/
section{background-color:#fff;}
section.section2{background-color:#efefef;}

section:after{content:"";background-color:#efefef;padding-bottom:15px;clear:both;display:block}
section .hd{position: relative;padding:15px 5px;font-size:18px;font-weight:600}
section .hd .tit .ico{background-position:left center;background-repeat:no-repeat;background-size:18px;width:18px;padding:9px;margin-right:5px}

section .hd .r-btn-area{position:absolute;right:5px;top:20px;z-index:10;background-color:#fff;font-size:12px;}
section .hd .r-btn-area ul li{float:left;padding-left:5px;}
section .hd .r-btn-area ul li a {font-weight:600;padding:10px;}
section .hd .r-btn-area ul li a.more_info:before {content:"+";}
section .hd .r-btn-area ul li a img{margin-bottom:5px}
section .hd .r-btn-area ul li a span{font-size:0.8em}
/*-----------------sub_container-------------------------*/
.sub_container{padding-top:96px}



/*-----------------popup_container----------------------*/
/*popup_section setting*/
.pop_container{padding-top:90px}
.pop_container section{margin-bottom:20px}
.pop_container section:last-child{margin-bottom:0}
.pop_container section:after{content:"";background-color:#efefef;clear:both;display:block;padding-bottom:0}
.pop_container section .pop_hd{position: relative;padding:10px 10px;font-size:16px;font-weight:600;background-color:#efefef;color:#666666}
.pop_container section .pop_hd .r-btn-area{position:absolute;right:5px;top:10px;z-index:10;background-color:#transparent;font-size:12px;}
.pop_container section .pop_hd .r-btn-area ul li{float:left;padding-left:5px;}
.pop_container section .pop_hd .r-btn-area ul li a {font-weight:600;padding:10px;}
.pop_container section .pop_hd .r-btn-area ul li a.more_info:before {content:"+";}
.pop_container section .pop_hd .r-btn-area ul li a img{margin-bottom:5px}
.pop_container section .pop_hd .r-btn-area ul li a span{font-size:0.8em}

.pop_container section .pop_hd .r-btn-area ul li a.add_favo:before{content:"+";padding-right:3px}
.pop_container section .pop_hd .r-btn-area ul li a.add_favo{border:1px solid #14264a;background-color:#14264a;color:#fff;padding:5px 10px;}
.pop_container section .pop_hd .r-btn-area ul li a.remove_favo:before{content:"-";padding-right:3px}
.pop_container section .pop_hd .r-btn-area ul li a.remove_favo{border:1px solid #14264a;background-color:#fff;color:#14264a;padding:5px 10px}

/*section content_area*/
section .content{padding:10px;background-color:#fff;border-bottom:1px solid #ddd;border-top:1px solid #ddd;}
section .content ul.info-list{font-weight:700}
section .content ul.info-list li{padding-left:80px;position:relative;color:#181818;margin:10px 0;line-height:18px;}
section .content ul.info-list li span{position:absolute;left:0;color:#777777}
section .content ul.info-list li ul li{margin:0;padding:0}/*2depth*/

section .content ul.info-list01{font-weight:700}
section .content ul.info-list01 li{padding-left:80px;position:relative;color:#181818;margin:10px 0;line-height:18px;padding-bottom:10px;border-bottom:1px solid #ccc}
section .content ul.info-list01 li:last-child{border-bottom:none;padding-bottom:0}
section .content ul.info-list01 li a{font-size:0.9em}
section .content ul.info-list01 li a .tit{padding-bottom:5px}
section .content ul.info-list01 li span{position:absolute;left:0;color:#777777}
section .content ul.info-list01 li ul li{margin:0;padding:0}/*2depth*/

section .content p{}
section.section2 .content{padding:0 10px;background-color:#efefef;border-bottom:1px solid #efefef;border-top:1px solid #efefef;}
/*sub_content*/
.content select.full-width{width:100%;padding:10px 15px;}
.content .con_tit_area{position: relative;margin-bottom:10px}
.content .con_tit_area::after {display: block;visibility: hidden;clear: both;content: "";}
.content .con_tit_area .tit{font-size: 14px;color:#666666;    line-height: 14px;}
.content .con_tit_area .r-area{position:absolute;right:0px;bottom:0px;font-size:11px;line-height:13px}
.content .con_tit_area .r-area ul li{float:left;padding-left:5px;}
.content .con_tit_area .r-area ul li a img{margin-bottom:5px}
.content .con_tit_area .r-area ul li a span{font-size:0.8em}
/*게시물 리스트*/
.bo_list_ul li:active,.bo_list_ul li:hover,section a:active{background-color:rgba(18,38,75,0.1);}
.bo_list_ul ul li{overflow: hidden;border-top: 1px solid #ededed;}
.bo_list_ul ul li:last-child{border-bottom: 1px solid #ededed;}

.bo_list_ul .list_a {overflow: hidden;display:block;max-width: 100%;-webkit-box-sizing: border-box;box-sizing: border-box;}
.bo_list_ul .list_a .champ_status {float: left;position: relative;padding: 10px 5px 9px 5px;color: #222;white-space: nowrap;letter-spacing: -1px;text-align:center}
.bo_list_ul .list_a .subject {display: block;overflow: hidden;padding: 10px 0 9px 0;color: #222;letter-spacing: -.5px;white-space: nowrap;text-overflow: ellipsis;}
.bo_list_ul .subject .m_subject{font-size:14px;font-weight:700;color:#14264a;white-space: nowrap;overflow: hidden;text-overflow: ellipsis;padding-bottom:2px;}
.bo_list_ul .subject .s_subject{font-size:13px;color:#999999;font-weight:600;}
.bo_list_ul .list_a .list_r_area {float: right;position: relative;padding: 14px 10px 14px 10px;color: #222;white-space: nowrap;letter-spacing: -1px;}
.bo_list_ul .list_a .list_r_area .btn01{background-color:#eb0414;font-size:14px;padding:3px;color:#fff;font-weight:600}
.bo_list_ul .list_a .list_r_area .btn02{background-color:#14264a;font-size:14px;padding:3px;color:#fff;font-weight:600}

/*quick_line_button*/
section .quick_view {position:relative}
section .quick_view a{font-weight:700;text-align:center;padding:15px;display:block;font-size:14px}
section .quick_view a:after{content:"";position: absolute;
background-position:-50px -17px;
width:30px;height:18px;margin-left:10px;top:50%;margin-top:-9px;}

/*quick_sch*/
.quick_sch .sch_bg{background-color:#fafafa;padding:45px 97px 15px 97px;}
.quick_sch .sch_area {background-color:#fff;margin:7px auto;text-align:center;position:relative;width:90%;box-sizing: border-box;}
.quick_sch .sch_area input{padding:10px 0px 10px 15px;border:2px solid #aaa;width:100%;color:#aaa}
.quick_sch .sch_area button{padding:2px 15px;border:none;position:absolute;right:2px;top:2px;cursor:pointer;}
.quick_sch .sch_area button i{font-size:26.5px;font-weight:600;color:#aaa}
.quick_sch .sch_area button:active i{color:#ccc;}


/*icon style*/
.champ_ico{border:1px solid #292929;border-radius:5px;font-size:11px;padding:0 5px;font-weight:600;display:block}
.champ_ico1:before{content:"KTA"}.champ_ico1{background-color:#41fdcf;}
.champ_ico2:before{content:"KATO"}.champ_ico2{background-color:#69fd41;}
.champ_ico3:before{content:"KATA"}.champ_ico3{background-color:#fdc141;}
.champ_ico4:before{content:"KASTA"}.champ_ico4{background-color:#41e9fd;}
.champ_ico5:before{content:"LOCAL"}.champ_ico5{background-color:#41fdcf;}
.champ_ico6:before{content:"비랭킹"}.champ_ico6{background-color:#41fdcf;}

.champ_status .limit_status{color:#eb0414;font-size:11px;font-weight:600;padding-top:5px;display:block}


/*inline-tab-style2*/
#tab_group2{
  height: 100%;width: 100%;overflow: hidden;}
#tab_group2 h2 {position:absolute;font-size:0;line-height:0;overflow:hidden}
/*#tab_group2 ul {margin-bottom:10px;padding-left:1px;zoom:1;display:inline-block;font-size:0.8em}*/
#tab_group2 ul{width: 100%;
height: 100%;
overflow-y: scroll;
padding-bottom: 17px; white-space:nowrap };
#tab_group2 ul:after {display:block;visibility:hidden;clear:both;content:""}
/*#tab_group2 li:first-child{border-left: 1px solid #000;}*/
#tab_group2 li.active a{background-color:#292929;color:#fff;font-weight:bold}
#tab_group2 li {display: inline-block; 1px solid #000;}
/*#tab_group2 li {float:left;padding:0 4px 0 5px;border-right: 1px solid #000;}*/
#tab_group2 li:last-child{margin-right:-1px}
#tab_group2 a {display:block;position:relative;margin-left:-1px;padding:6px 10px 5px;background:#fff;color:#888;text-align:center;letter-spacing:0.1em;line-height:1.2em;cursor:pointer;}
#tab_group2 a:focus, #bo_cate a:hover, #bo_cate a:active {text-decoration:none}
/*#tab_group2 #bo_cate_on {z-index:2;background:#292929;color:#fff;font-weight:bold}*/

.tab_group1 {display: box;display: -webkit-box;display: -moz-box;box-orient: horizontal;-webkit-box-orient: horizontal;-moz-box-orient: horizontal;
    background: #fff;margin-bottom:10px;padding:3px 0 4px 0;position:relative;margin-bottom:10px;}
.tab_group1 {
    background-color: #fff;color: #fff;
    min-width: 320px;
    -webkit-backface-visibility: hidden;
    backface-visibility: hidden;
    border-top:2px solid #14264a;
    border-bottom:1px solid #636363;
}
.tab_group1::after {content: '';
display: block;
position: absolute;
bottom: 0;
left: 0;
right: 0;
height: 1px;
background: #818181;}
.tab_group1 a {
display: block;
box-flex: 1;
-webkit-box-flex: 1;
-moz-box-flex: 1;
height: 30px;
color: #bbbbbb;
font-size: 14px;
letter-spacing: -1px;
text-align: center;
line-height: 2em;
white-space: nowrap;
}
.tab_group1 a.active {position: relative;color: #fff;background-color:#14264a;}



/*btn-group-list*/
.btn-group{margin:10px 0}
.btn-group .tit{font-size:14px;margin-bottom:5px}

ul.btn-list {list-style-type:none;margin:0;padding:0;font-size:0.8em;border-top:1px solid #ccc;border-left:1px solid #ccc;line-height:1.4em}
.btn-list li{background-color:#fff;float:left;width:20%;text-align:center;border-right:1px solid #ccc;border-bottom:1px solid #ccc;-webkit-box-sizing: border-box;box-sizing: border-box; }
.btn-list li.active a{background-color:#14264a;font-weight:700;color:#fff}
.btn-list li a{display:block;padding:6px 10px;color:#555555;}
.btn-list li a.active{background-color:#14264a;font-weight:700;color:#fff}
/*list3*/
.btn-list.list2 li{width:50%}
/*list3*/
.btn-list.list3 li{width:33.33333%}

/*토너먼트 상황 리스트*/
ul.tournament_list li:nth-of-type(even){background-color:#f9f9f9}
.tournament_list .status_banner{background-color:#efefef;padding:15px;text-align:center;color:#666666;font-weight:700;line-height:16px;border-bottom:1px solid #ddd}
.tournament_match{font-size:0.8em;border-bottom:1px solid #ccc}
.tournament_match .r-side-area{position: absolute;right:0;}
.tournament_match .tournament_hd{position:relative;padding:5px;font-weight:700;}
.tournament_match .tournament_content{height:40px;position:relative;}
.tournament_match .tournament_content .l-area{position:absolute;right:50%;text-align:right;margin-right:50px}
.tournament_match .tournament_content .r-area{position:absolute;left:50%;text-align:left;margin-left:50px}
.tournament_match .tournament_content .tournament_point{position:absolute;left:50%;}
.tournament_match .tournament_content .tournament_point .result_point{color: red;font-size: 3em;text-align:center;width:80px;margin-left:-40px;line-height:1em}
.tournament_match .tournament_content .tournament_point .result_point .playing{font-size:16px;}
.tournament_match .tournament_content .tournament_point .btn{width:76px;display:inline-block;padding:9px 0;font-size: 16px;text-align:center;width:80px;margin-left:-40px;line-height:1em;border:2px solid #14264a;border-radius:3px}
.tournament_match .tournament_ft{position:relative;padding:5px;line-height:11px}
.tournament_content::after{clear:both;content:"";display:block}

/*내경기*/
tr.my_team_sts{background-color:rgba(255,255,0,0.3);border:2px solid brown;color:brown;font-weight:700;}


.btn-apply{border:1px solid #ccc;padding:5px;}


/*animation setting*/
.ani{animation-name: color-change;animation-duration: 3s;
  -webkit-animation-direction: alternate;
  animation-direction: alternate;
  -webkit-animation-iteration-count: infinite;
  animation-iteration-count: infinite;
}
.ani01{animation-name: color-change01;
  animation-duration: 4s;
  animation-delay: 4s;
  animation-iteration-count: infinite;
  animation-timing-function: linear;
  animation-direction: alternate;
}
/* animation */
@keyframes color-change {
from{
background-color:blue;color:#fff
}
to{
background-color:yellow;color:#000
}
}
@keyframes color-change01 {
  from, 50%, to {
    opacity: 1;
  }

  25%, 75% {
    opacity: 0;
  }
}


/*conpetion_management_page*/
/*competion_mnt_list*/
.tbl_match_status table{width:100%;font-size:13px;}
.tbl_match_status table thead tr th{padding:3px;border:1px solid #ccc;border-bottom:2px solid #aaa;word-break: break-all;}
.tbl_match_status table tbody tr th{padding:3px;border:1px solid #ccc;}
.tbl_match_status table tbody tr td{border:1px solid #ccc;}
.tbl_match_status table tbody tr td::before{content:""}

.tbl_match_status table tbody tr td.end{background-color:#eee}
.tbl_match_status table tbody tr td.playing{background-color:#000}

 .match_manager_area{font-size:13px;}
 .match_manager_area ul{border:1px solid #ccc;}
 .match_manager_area ul li{border-bottom:1px solid #ccc;position:relative;padding-left:5px}
 .match_manager_area ul li:last-child{border-bottom:none}
 .match_manager_area ul li.active{background-color:yellow}
 .match_manager_area ul li a{background-color:#fff;display:block;padding:5px;border-left:1px solid #ccc}
 .match_manager_area ul li .ground_area{color:#777;}
 .match_manager_area ul li .m_name{color:#aaa}



/*--------------------------회원가입 -----------------------------------*/
.form-control2 {display: block;width: 100%;height: 27px;line-height: 1.32857143;
    background-color: #fff;background-image: none;
border:0px;color:#aaa;font-weight:700;padding:0;font-size:13px;
	}
.login_container .tip {zoom:1;border:1px solid #d9d9d9;padding:15px;position:relative;color:#696566;font-size:14px;line-height:17px}
.login_container .tip::before {content: "";position: absolute;bottom: -4px;left: -1px;display: block;width: 100%;height: 3px;background: #f1f1f1;}
.login_container .tip:after {display:block;visibility:hidden;clear:both;content:"";}


.login_hd_area{background-color:#fff;color:#fff;padding:12px 0;text-align:center;box-shadow:1px 1px 3px grey;position:fixed;display:inline-block;width:100%;z-index:9999;top:0}
.login_hd_area a{color:#292929;padding:12px}
.login_hd_area a.title{font-size:16px;font-weight:600;}

.login_container{padding-top:90px;margin:0 20px }

/* 회원가입 폼 테이블 */
.tbl_frm02 {margin:0 0 20px;color:#9d9d9d}
.tbl_frm02 table {width:100%;border-collapse:collapse;border-spacing:0}
.tbl_frm02 thead th {box-sizing: content-box;width:80px;padding:7px 13px;background:#f7f7f7;text-align:left}
.tbl_frm02 tbody th {padding:15px 0 5px 0;background:transparent;text-align:left;font-size:13px;font-weight:300}
.tbl_frm02 tbody td {padding:15px 0 5px 0;background:transparent;border-bottom:1px solid #ccc}
.tbl_frm02 tbody td input{border:0;width:100%;padding:5px;font-weight:700;color:#aaa}


.ft_area{position:fixed;bottom:0;width: 100%;}
.ft_area .ft_hd{position:relative;padding:15px 0;}
.ft_area .ft_hd a{font-size:12px;text-decoration:underline;color:#aaa;font-weight:700}
.ft_area .btn_area{position:relative;width:100%;display:block}
.ft_area .btn_area a,.ft_area .btn_area button,.ft_area .btn_area input{background-color:#bdbdbd;color:#fff;padding:20px 0;display:block;text-align:center;width:100%;border:1px solid #bdbdbd}
.ft_area .btn_area a:active,.ft_area .btn_area button:active,.ft_area .btn_area input:active{background-color:#14264a}

/*회원정보 수정*/
.profile_update .tip {zoom:1;border:1px solid #d9d9d9;padding:15px;position:relative;color:#696566;font-size:14px;line-height:17px}
.profile_update .tip::before {content: "";position: absolute;bottom: -4px;left: -1px;display: block;width: 100%;height: 3px;background: #f1f1f1;}
.profile_update .tip:after {display:block;visibility:hidden;clear:both;content:"";}
.profile_update{margin:0 20px }




/*점수입력 팝업*/
.tbl_style03 table {margin:0 0 10px;width:100%;font-size:1em}
.tbl_style03 table caption {padding:0;font-size:0;line-height:0;overflow:hidden}
.tbl_style03 table thead th {padding:7px 0;border-top:2px solid #000000;border-bottom:1px solid #000000;background:#fff;color:#777;font-size:0.8em;text-align:center;letter-spacing:-0.1em}
.tbl_style03 table thead a {color:#383838}
.tbl_style03 table thead th input {vertical-align:top} /* middle 로 하면 게시판 읽기에서 목록 사용시 체크박스 라인 깨짐 */
.tbl_style03 table tfoot th, .tbl_head01 tfoot td {padding:10px 0;border-top:1px solid #c1d1d5;border-bottom:1px solid #c1d1d5;background:#d7e0e2;text-align:center}
.tbl_style03 table tbody th {padding:13px 0;border-top:1px solid #e9e9e9;border-bottom:1px solid #e9e9e9;font-size:0.8em}
.tbl_style03 table td {text-align: center;padding:5px 3px;border-top:1px solid #e9e9e9;border-bottom:1px solid #e9e9e9;line-height:1.5em;word-break:break-all;font-size:0.8em;}
.tbl_style03 table td.match_point {color:#aaa;font-size:2.5em}
.tbl_style03 table td.match_point input{width:30px;text-align:center;font-size:30px}
.tbl_style03 table td.match_point span:first-child{padding-right:5px}
.tbl_style03 table td.match_point span:last-child{padding-left:5px}
.tbl_style03 table td.match_point .playing{font-size:16px;}


.insert_score_area .hd_tit{margin-bottom:5px;margin-top:15px}
.insert_score_area .btn_area input{width:100%;padding:5px}
.insert_score_area .btn_area .btn_submit{box-sizing: border-box;padding:15px;width:100%;display:block;margin-top:5px;background-color:#fafafa;color:#292929;border:1px solid #ccc;cursor:pointer}
.insert_score_area .btn_area .btn_submit:hover{background-color:#14264a;color:#fff;font-weight:700}

.insert_score_area .history_log_area {font-size:13px;margin-top:20px}
.insert_score_area .history_log_area .tit{font-weight:700;margin-bottom:5px;font-size:13px}
.insert_score_area .history_log_area ul{margin:0;padding:0;list-style-type:none;border:1px solid #ccc;padding:5px }

.hide{display:none;}
</style>

