<?php
include_once('../common.php');

?>

	<!-- exhi_qa_board-->
	<div class="section">
		<!-- content_subhead_tit-->
		<div class="sub_tit_type1">
			<span>1:1 문의 게시판</span> 
		</div>
		<!-- content_subhead_tit-->
		
		<!-- board list area -->
		<div class="tbl-default mb-20">
			<table>
				<thead>
					<tr>
						<th width="100">번호</th>
						<th width="100">분류</th>
						<th>제목</th>
						<th width="100">상태</th>
						<th width="100">등록일</th>
					</tr>
				</thead>
				<tbody>
					<tr>
						<td class="text-center">1</td>
						<td class="text-center">분류</td>
						<td><a href="<?php echo G5_URL ?>/inc/qa_board_view.php" id="qa_board_view">질문 제목</a></td>
						<td class="text-center">
							<!-- 답변 상태에 따른 출력 
							<span>답변대기</span>-->
							<span class="color5"><a href="<?php echo G5_URL ?>/inc/qa_board_view.php" id="qa_board_write">답변완료</a></span>
						</td>
						<td class="text-center">01-19</td>
					</tr>
				</tbody>
			</table>
		</div>
		
		<!-- //board list area -->
		<div class="btn_step section text-right">
			<a href="<?php echo G5_URL ?>/inc/qa_board_write.php" id="qa_board_write" class="btn bkg1 btn-radius fw-600">질문하기</a>	
		</div>
	</div>
	<!-- //exhi_qa_board-->
 


<script>
var popup_list3 = function(href) {
    window.open(href, "", "left=50, top=50, width=1000, height=570, scrollbars=1");
}
 
$(document).ready(function(){
    $("#qa_board_write, #qa_board_view").click(function(){
        popup_list3(this.href);
        return false;
    });
});
</script>



