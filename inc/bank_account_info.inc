<!-- bank_account_info -->
<div class="section">
	<div class="sub_tit_type1">
		<span>입금 계좌 안내</span> 
		<small>전시회 신청을 위한 입금 계좌 안내 입니다.</small>
		<div class="right_cmt">
			<ul>
				<li class="text_underline">관련문의 : <a >02 - 565 - 7288</a></li>
			</ul>
			
		</div>
	</div>
	<div class="tbl-style01 bank_info_area">
		<table>
			<tbody>
				<tr>
					<td width="300">
						<div class="account_info">
							<div class="bank_logo"><img src="<?php echo G5_IMG_URL?>/common/bank_woori.png"> <span class="right_cmt">예금주 : 마이페어</span></div>
							<div class="bank_account">1005.103.049786 </div>
						</div>
					</td> 
					<td>
						<div class="account_caution">
							주의사항
							<p class="xsmall-txt">무통장 입금시, 예금주(마이페어) 명을 꼭 확인해주세요.</p>
							
						</div>
					</td>
				</tr>
			</tbody>
		</table>
	</div>
</div>
<!-- //bank_account_info -->