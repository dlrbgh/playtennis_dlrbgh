<?php
include_once('../common.php');
include_once(G5_PATH.'/head.sub.php');
if (!defined('_GNUBOARD_')) exit; // 개별 페이지 접근 불가
?>

<div class="sub_container ">
	<div class="sub_tit">
		<span>문의하기</span> 
	</div>
	<!--  register_form-->
	<form action="" method="post">
	
	<div class=" bkg-white">
		
		<div class="regi_area">
			<div class="regi-style02 mb-20">
				<table>
					<tbody>
						<tr>
							<td><label for="">항목</label></td>
							<td >
								<select>
									<option>마이페어 서비스 관련</option>
									<option>참가 전시회 관련</option>
									<option>기타</option>
								</select>
							</td>
						</tr>
						<tr>
							<td><label for="">제목</label></td>
							<td ><input type="text" value="" id="" name="" ></td>
						</tr>
						<tr>
							<td class="vertical-top"><label for="">내용</label></td>
							<td >
								<textarea></textarea>
							</td>
						</tr>
					</tbody>
				</table>
			</div>
			<div class="regi-cmt2">
				<div class="tit">TIP</div>
				<ul>
					<li>·&nbsp;마이페어 관련
						<ul>
							<li>마이페어 서비스 관련 문의사항이 있을때 선택해 주세요</li>
						</ul>
					</li>
					<li>·&nbsp;참가전시회 관련
						<ul>
							<li>참가하시는 전시회에 문의사항이 있을때 선택해주세요</li>
						</ul>	
					</li>
					<li>·&nbsp;기타 질문
						<ul>
							<li>기타 질문 사항 입니다</li>
						</ul>	
					</li>
				</ul>
			</div>
			<div class="btn_step">
				<a href="javascript:window.close()" class="btn bkg3 btn-radius fw-600">취 소</a>
				<input class="btn bkg1 btn-radius fw-600" value="등 록" type="submit">
			</div>
		</div>
		
		
	</div>

	</form>
	<!--  end register_form-->
</div>

<script>
$(function() {
    var sw = screen.width;
    var sh = screen.height;
    var cw = document.body.clientWidth;
    var ch = document.body.clientHeight;
    var top  = sh / 2 - ch / 2 - 200;
    var left = sw / 2 - cw / 2;
    moveTo(left, top);
});
</script>
<?php
include_once(G5_PATH.'/tail.sub.php');
?>