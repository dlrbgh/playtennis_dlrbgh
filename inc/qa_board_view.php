<?php
include_once('../common.php');
include_once(G5_PATH.'/head.sub.php');
if (!defined('_GNUBOARD_')) exit; // 개별 페이지 접근 불가
?>

<div class="sub_container ">
	<div class="sub_tit">
		<span>문의하기</span> 
	</div>
	<!--  register_form-->
	<form action="" method="post">
	
	<div class=" bkg-white">
		
		<div class="regi_area">
			<div class="regi-style02 mb-20">
				<table>
					<tbody>
						<tr>
							<td><label for="">항목</label></td>
							<td>
								마이페어 서비스 관련
							</td>
						</tr>
						<tr>
							<td><label for="">제목</label></td>
							<td >질문 제목 출력</td>
						</tr>
						<tr>
							<td class="vertical-top"><label for="">내용</label></td>
							<td>
								<p>
									내용 출력 내용 출력 내용 출력 내용 출력 내용 출력 내용 출력
									내용 출력 내용 출력 내용 출력 내용 출력 내용 출력 내용 출력
									내용 출력 내용 출력 내용 출력 내용 출력 내용 출력 내용 출력
									내용 출력 내용 출력 내용 출력 내용 출력 내용 출력 내용 출력
									내용 출력 내용 출력 내용 출력 내용 출력 내용 출력 내용 출력
									내용 출력 내용 출력 내용 출력 내용 출력 내용 출력 내용 출력
									내용 출력 내용 출력 내용 출력 내용 출력 내용 출력 내용 출력
									<br/>
									내용 출력 내용 출력 내용 출력 내용 출력 내용 출력 내용 출력
								</p>
								
							</td>
						</tr>
						<tr class="color5">
							<td class="vertical-top"><label for="">답변내용</label>
								<br/>
								2016.05.30 답변
							</td>
							<td>
								<p>
									내용 출력 내용 출력 내용 출력 내용 출력 내용 출력 내용 출력
									내용 출력 내용 출력 내용 출력 내용 출력 내용 출력 내용 출력
									내용 출력 내용 출력 내용 출력 내용 출력 내용 출력 내용 출력
									내용 출력 내용 출력 내용 출력 내용 출력 내용 출력 내용 출력
									내용 출력 내용 출력 내용 출력 내용 출력 내용 출력 내용 출력
									내용 출력 내용 출력 내용 출력 내용 출력 내용 출력 내용 출력
									내용 출력 내용 출력 내용 출력 내용 출력 내용 출력 내용 출력
									<br/>
									내용 출력 내용 출력 내용 출력 내용 출력 내용 출력 내용 출력
								</p>
							</td>
						</tr>
					</tbody>
				</table>
			</div>
			<div class="regi-cmt2">
				<div class="tit">TIP</div>
				<ul>
					<li>·&nbsp;마이페어에서 알려 드립니다.
						<ul>
							<li>질문해 주셔서 감사합니다</li>
							<li>성심 성의껏 답변해 드리겠습니다</li>
						</ul>
					</li>
					
				</ul>
			</div>
			<div class="btn_step">
				<a href="javascript:window.close()" class="btn bkg3 btn-radius fw-600">닫 기</a>
				<!--
				<input class="btn bkg1 btn-radius fw-600" value="추가 질문하기" type="submit">
				-->
			</div>
		</div>
	</div>

	</form>
	<!--  end register_form-->
</div>

<script>
$(function() {
    var sw = screen.width;
    var sh = screen.height;
    var cw = document.body.clientWidth;
    var ch = document.body.clientHeight;
    var top  = sh / 2 - ch / 2 - 200;
    var left = sw / 2 - cw / 2;
    moveTo(left, top);
});
</script>
<?php
include_once(G5_PATH.'/tail.sub.php');
?>